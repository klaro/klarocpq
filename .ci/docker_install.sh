#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

apt-get update -yqq
apt-get install git libfreetype6-dev libgd-dev libjpeg62-turbo-dev libpng-dev libtidy-dev libzip-dev zlib1g-dev -yqq

# Install project dependencies
docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
docker-php-ext-install -j$(nproc) bcmath gd pdo pdo_mysql tidy zip

pecl channel-update pecl.php.net

# Install composer
curl -sS https://getcomposer.org/installer | php
php composer.phar install --prefer-dist --no-progress --no-scripts
