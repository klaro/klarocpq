<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\SecurityBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('klaro_security');

        $this->addJWTSection($rootNode);

        return $treeBuilder;
    }

    private function addJWTSection(ArrayNodeDefinition $rootNode)
    {
        $supportedAlgorithms = ['HS256', 'HS384', 'HS512', 'RS256'];

        $rootNode
            ->children()
                ->arrayNode('jwt')
                    ->validate()
                        ->ifTrue(static function ($v) {
                            return !is_array($v);
                        })
                        ->thenInvalid('The klaro_security.jwt config has to be an array.')
                    ->end()
                    ->children()
                        ->scalarNode('token_name')->isRequired()->cannotBeEmpty()->end()
                        ->arrayNode('public_key')
                            ->beforeNormalization()
                                ->ifString()
                                ->then(static function ($v) {
                                    return ['secret' => $v];
                                })
                            ->end()
                            ->children()
                                ->scalarNode('secret')->defaultNull()->end()
                                ->scalarNode('path')->defaultNull()->end()
                                ->scalarNode('filename')->defaultNull()->end()
                                ->scalarNode('algorithm')
                                    ->validate()
                                        ->ifNotInArray($supportedAlgorithms)
                                        ->thenInvalid('The algorithm %s is not supported. Please choose one of '.json_encode($supportedAlgorithms))
                                    ->end()
                                    ->defaultNull()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
