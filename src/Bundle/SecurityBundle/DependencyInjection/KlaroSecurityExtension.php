<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\SecurityBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class KlaroSecurityExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $this->loadJWTSection($config, $container);
    }

    private function loadJWTSection(array $config, ContainerBuilder $container): void
    {
        if (isset($config['jwt']) ? true === empty($config['jwt']) : true) {
            return;
        }

        $container->setParameter('klaro_security.jwt.token_name', $config['jwt']['token_name']);

        // Check if we have secret or public key
        $publicKeyData = $config['jwt']['public_key'];
        if (null !== $publicKeyData['secret']) {
            $container->setParameter('klaro_security.jwt.public_key.secret', $publicKeyData['secret']);
        } else {
            $container->setParameter('klaro_security.jwt.public_key.filename', $publicKeyData['filename']);
            $container->setParameter('klaro_security.jwt.public_key.path', $publicKeyData['path']);
            $container->setParameter(
                'klaro_security.jwt.public_key.algorithm',
                null !== $publicKeyData['algorithm'] ? [$publicKeyData['algorithm']] : null
            );
        }
    }
}
