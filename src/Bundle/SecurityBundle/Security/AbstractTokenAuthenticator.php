<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\SecurityBundle\Security;

use Exception;
use Firebase\JWT\JWT;
use Klaro\UserBundle\Doctrine\UserManager;
use Klaro\UserBundle\Model\UserManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

abstract class AbstractTokenAuthenticator extends AbstractGuardAuthenticator
{
    use TargetPathTrait;

    /** @var Container */
    private $container;

    /** @var UserManager */
    private $userManager;

    public function __construct(ContainerInterface $container, UserManagerInterface $userManager)
    {
        $this->container = $container;
        $this->userManager = $userManager;
    }

    /**
     * @param  Request $request
     *
     * @return bool
     */
    public function supports(Request $request): bool
    {
        $tokenName = $this->container->getParameter('klaro_security.jwt.token_name');

        return $request->headers->has($tokenName) || $request->cookies->has($tokenName) || $request->query->has($tokenName);
    }

    /**
     * @param  Request $request
     *
     * @return array
     */
    public function getCredentials(Request $request): array
    {
        $tokenName = $this->container->getParameter('klaro_security.jwt.token_name');
        $token = $request->cookies->get($tokenName) ?? $request->get($tokenName);

        // We have to return both decoded object and token as credentials since further implementations may require token
        // for additional data requests
        return ['user' => $this->decodeToken($token), 'token' => $token];
    }

    /**
     * @return Container
     */
    public function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * Loads user and updates its realtime role information
     *
     * @param  mixed                 $credentials
     * @param  UserProviderInterface $userProvider
     *
     * @return UserInterface
     *
     * @throws Exception
     */
    abstract public function getUser($credentials, UserProviderInterface $userProvider): UserInterface;

    /**
     * @param  mixed         $credentials
     * @param  UserInterface $user
     *
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return true;
    }

    /**
     * @param  Request        $request
     * @param  TokenInterface $token
     * @param  string         $providerKey
     *
     * @return RedirectResponse|Response|null
     *
     * @throws Exception
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * @param  Request                 $request
     * @param  AuthenticationException $exception
     *
     * @return Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        return new JsonResponse(
            [
                'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),
            ],
            Response::HTTP_FORBIDDEN
        );
    }

    /**
     * @param  Request                      $request
     * @param  AuthenticationException|null $authException
     *
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return new JsonResponse(['message' => 'Authentication Required'], Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe(): bool
    {
        return false;
    }

    /**
     * @return UserManager
     */
    public function getUserManager(): UserManager
    {
        return $this->userManager;
    }

    /**
     * Decodes jwt stored in cookie and checks for its validity
     *
     * @param  string $jwt
     *
     * @return array
     */
    protected function decodeToken(string $jwt): array
    {
        if ($filename = $this->getContainer()->getParameter('klaro_security.jwt.public_key.filename')) {
            $algorithm = $this->getContainer()->getParameter('klaro_security.jwt.public_key.algorithm');
            $path = $this->getContainer()->getParameter('klaro_security.jwt.public_key.path');
            $secret = file_get_contents($path.'/'.$filename);
        }

        $secret = $secret ?? $this->getContainer()->getParameter('klaro_user.jwt.public_key.secret');

        return (array) JWT::decode($jwt, $secret, $algorithm ?? []);
    }
}
