<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ProductManagerBundle\DependencyInjection;

use Klaro\Component\ProductPhase\PhaseNode;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        //@formatter:off
        $rootNode = $treeBuilder->root('klaro_product_manager')
            ->children()
                ->scalarNode('manager')
                ->defaultNull()
                ->end()
            ->end();
        //@formatter:on
        $this->addPhasesSection($rootNode);
        $this->addProductLineSection($rootNode);
        $this->addProductsSection($rootNode);

        return $treeBuilder;
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addPhasesSection(ArrayNodeDefinition $node)
    {
        //@formatter:off
        $node
            ->fixXmlConfig('phase')
            ->children()
                ->arrayNode('phases')
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->useAttributeAsKey('phase')
                    ->prototype('scalar')->end()
                ->end()
            ->end();
        //@formatter:on
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addProductsSection(ArrayNodeDefinition $node)
    {
        //@formatter:off
        $node
            ->fixXmlConfig('product')
            ->children()
                ->arrayNode('products')
                    ->useAttributeAsKey('product')
                    ->prototype('array')
                    ->children()
                        ->scalarNode('title')->isRequired()->end()
                        ->variableNode('roles')
                            ->defaultValue([])
                            ->validate()
                                ->ifTrue(function ($v) {
                                    return !is_array($v);
                                })
                                ->thenInvalid('Product roles has be array.')
                            ->end()
                        ->end()
                        ->scalarNode('disabled')
                            ->defaultFalse()
                        ->end()
                        ->arrayNode('phases')
                            ->isRequired()
                            ->requiresAtLeastOneElement()
                            ->prototype('scalar')->end()
                        ->end()
                        ->variableNode('structure')
                            ->validate()
                                ->ifTrue(function ($v) {
                                    return !is_string($v) && !is_array($v);
                                })
                                ->thenInvalid('Sales structure must be either string or array.')
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
        //@formatter:on
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addProductLineSection(ArrayNodeDefinition $node)
    {
        //@formatter:off
        $node
            ->fixXmlConfig('product_line')
            ->children()
                ->arrayNode('product_lines')
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->useAttributeAsKey('product_line')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('title')->isRequired()->end()
                            ->scalarNode('image')->defaultNull()->end()
                            ->arrayNode('phases')
                                ->prototype('array')
                                    ->children()
                                        ->scalarNode('type')
                                            ->validate()
                                                ->ifTrue(function ($v) {
                                                    return !in_array($v, [PhaseNode::PRODUCT_LIST, PhaseNode::PRODUCT, PhaseNode::GROUP, PhaseNode::SINGLE]);
                                                })
                                                ->thenInvalid('Invalid phase type.')
                                            ->end()
                                        ->end()
                                        ->scalarNode('title')->defaultNull()->end()
                                        ->variableNode('roles')
                                            ->defaultValue([])
                                            ->validate()
                                                ->ifTrue(function ($v) {
                                                    return !is_array($v);
                                                })
                                                ->thenInvalid('Phase roles has be array.')
                                            ->end()
                                        ->end()
                                        ->scalarNode('disabled')
                                            ->defaultFalse()
                                        ->end()
                                        ->variableNode('ref')
                                            ->validate()
                                                ->ifTrue(function ($v) {
                                                    return !is_string($v) && !is_array($v);
                                                })
                                                ->thenInvalid('Phase ref must be either string or array.')
                                            ->end()
                                            ->validate()
                                                ->ifTrue(function ($v) {
                                                    return is_array($v) && count($v) < 1;
                                                })
                                                ->thenInvalid('Phase ref requires at least one element.')
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->variableNode('sales_structure')
                                ->validate()
                                    ->ifTrue(function ($v) {
                                        return !is_string($v) && !is_array($v);
                                    })
                                    ->thenInvalid('Sales structure must be either string or array.')
                                ->end()
                            ->end()
                            ->variableNode('structure')
                                ->validate()
                                    ->ifTrue(function ($v) {
                                        return !is_string($v) && !is_array($v);
                                    })
                                    ->thenInvalid('Sales structure must be either string or array.')
                                ->end()
                            ->end()
                            ->variableNode('product_items')
                                ->validate()
                                    ->ifTrue(function ($v) {
                                        return !is_string($v) && !is_array($v);
                                    })
                                    ->thenInvalid('Product items must be either string or array.')
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
        //@formatter:on
    }
}
