<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ProductManagerBundle\DependencyInjection;

use Klaro\Component\ProductPhase\PhaseNode;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class KlaroProductManagerExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        // Set product manager implementation
        $container->setParameter('klaro_product_manager.manager_provider', $config['manager']);

        $allPhases = $config['phases'];
        $allProducts = $config['products'];

        // Validate products
        foreach ($allProducts as $productId => $productData) {
            foreach ($productData['phases'] as $productPhaseId) {
                if (!array_key_exists($productPhaseId, $allPhases)) {
                    throw new \UnexpectedValueException(
                        sprintf('The phase "%s" is not defined under klaro_quotation.phases', $productPhaseId)
                    );
                }
            }
        }

        foreach ($config['products'] as $productRef => $productData) {
            if (!isset($productData['structure']) || !is_array($productData['structure'])) {
                $config['products'][$productRef]['structure'] = [];
            }
        }

        foreach ($config['product_lines'] as $productLine => $productLineDefinition) {
            // Read the phase definitions from the YAML config file.
            foreach ($productLineDefinition['phases'] as $phaseId => $phaseConfig) {
                $refs = is_string($phaseConfig['ref']) ? [$phaseConfig['ref']] : $phaseConfig['ref'];

                foreach ($refs as $ref) {
                    if (in_array($phaseConfig['type'], [PhaseNode::PRODUCT_LIST, PhaseNode::GROUP]) && empty($phaseConfig['title'])) {
                        throw new \InvalidArgumentException(
                            sprintf('A title is needed for phases of type "%s" and "%s".', PhaseNode::PRODUCT_LIST, PhaseNode::GROUP)
                        );
                    }

                    // Product must exist
                    if (in_array($phaseConfig['type'], [PhaseNode::PRODUCT_LIST, PhaseNode::PRODUCT]) && !array_key_exists($ref, $allProducts)) {
                        throw new \UnexpectedValueException(
                            sprintf('The product "%s" is not defined under klaro_quotation.products', $ref)
                        );
                    }

                    // Phase must exist
                    if ($phaseConfig['type'] == PhaseNode::SINGLE && !array_key_exists($ref, $allPhases)) {
                        throw new \UnexpectedValueException(
                            sprintf('The phase "%s" is not defined under klaro_quotation.phases', $ref)
                        );
                    }
                }
            }
        }

        $container->setParameter('klaro_product_manager.product_lines', $config['product_lines']);
        $container->setParameter('klaro_product_manager.phases', $config['phases']);
        $container->setParameter('klaro_product_manager.products', $config['products']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
