<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ProductManagerBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SetProductManagerProviderCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     *
     * @throws \Exception
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_product_manager.manager')) {
            return;
        }

        // Check that the selected alias exists.
        $selectedProvider = $container->getParameter('klaro_product_manager.manager_provider');

        if (!is_null($selectedProvider)) {
            if (!$container->hasDefinition($selectedProvider)) {
                throw new \Exception('Tried to set service "'.$selectedProvider.'" as product manager but the service does not exist.');
            }

            // Set the selected service as the formatter implementation.
            $productManager = $container->getDefinition('klaro_product_manager.manager');

            $productManager->replaceArgument(0, $container->getDefinition($selectedProvider));
        }
    }
}
