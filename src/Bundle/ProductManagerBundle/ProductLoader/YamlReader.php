<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ProductManagerBundle\ProductLoader;

use Klaro\Component\Common\Exception\ExceptionFactory;
use Klaro\Component\Util\ArrayUtil;
use Symfony\Component\Yaml\Yaml;

class YamlReader
{
    /** @var string */
    protected $rootDir;

    /** @var \stdClass|null */
    protected $metaSchema;

    /**
     * YamlReader constructor.
     * @param $rootDir
     */
    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir.'/../';
        $this->metaSchema = null;
    }

    /**
     * @return array
     */
    public function getPhaseConfigs($phaseFiles)
    {
        $phases = [];

        foreach ($phaseFiles as $phaseId => $source) {
            $phases[$phaseId] = $this->readPhaseConfig($source, true);
        }

        return $phases;
    }

    /**
     * @param $source
     * @param bool   $includeItems
     *
     * @return array
     *
     * @throws \Exception
     */
    private function readPhaseConfig($source, $includeItems = false)
    {
        $realConfigPath = $this->rootDir.$source;

        $defaults = array(
            'definition'    => $source,
            'template'      => null,
            'extends'       => null,
            'title'         => null,
            'evaluateTitle' => false,
            'model'         => null,
            'aliases'       => [],
            'icon'          => null,
            'phases'        => null,
            'repeat'        => null,
            'extra'         => null,
            'disabled'      => false,
            'editor'        => 'phase',
            'ordering'      => null,
            'validation'    => null,
            'decorator'     => null,
            'schema'        => null,
            'roles'         => [],
        );

        if ($includeItems === true) {
            $defaults['items'] = null;
        }

        if (file_exists($realConfigPath)) {
            $phaseConfig = file_get_contents($realConfigPath);
            $phaseConfig = Yaml::parse($phaseConfig);

            $phaseDefinition = array();

            // Read basic fields (ones defined in defaults).
            if (isset($phaseConfig['phaseDefinition'])) {
                foreach ($phaseConfig['phaseDefinition'] as $key => $field) {
                    if (array_key_exists($key, $defaults)) {
                        $phaseDefinition[$key] = $field;
                    }
                }
            }
        } else {
            throw new \Exception("Phase file".$realConfigPath." does not exist!");
        }

        if (isset($phaseDefinition['roles']) && !is_array($phaseDefinition['roles'])) {
            throw new \Exception("Roles attribute must be an array!");
        }

        // Fix extended config path.
        if (isset($phaseDefinition['extends']) && $phaseDefinition['extends'] !== null) {
            $extendedDefinition = $this->readPhaseConfig($phaseDefinition['extends'], $includeItems);

            unset($extendedDefinition['definition']);

            // Fix null item array.
            if (array_key_exists('items', $phaseDefinition) && !is_array($phaseDefinition['items'])) {
                $phaseDefinition['items'] = [];
            }

            $phaseDefinition = ArrayUtil::arrayMergeRecursive($extendedDefinition, $phaseDefinition);
        }

        // Handle reordering or filtering of items.
        if (isset($phaseDefinition['items']) && is_array($phaseDefinition['items'])) {
            // Load item definitions from file if specified.
            foreach ($phaseDefinition['items'] as $itemId => &$itemConfig) {
                if (isset($itemConfig['definition'])) {
                    $loadedConfig = $this->readPhaseConfig($itemConfig['definition'], true);

                    $itemConfig = ArrayUtil::arrayMergeRecursive($loadedConfig, $itemConfig);

                    unset($itemConfig['definition']);
                    unset($itemConfig['validation']);
                    unset($itemConfig['decorator']);
                }
            }

            if (isset($phaseDefinition['ordering']) && is_array($phaseDefinition['ordering'])) {
                $allowedKeys = array_flip($phaseDefinition['ordering']);

                // Filter unwanted keys & order according to the given keys.
                $phaseDefinition['items'] = array_merge($allowedKeys, array_intersect_key($phaseDefinition['items'], $allowedKeys));
            }
        }

        // Validate schema
        if (isset($phaseDefinition['schema']) && is_array($phaseDefinition['schema'])) {
            $schema = ArrayUtil::arrayToObject($phaseDefinition['schema']);

            $validator = new \League\JsonGuard\Validator($schema, $this->getMetaSchema());

            if ($validator->passes()) {
                $phaseDefinition['schema'] = $schema;
            } else {
                throw ExceptionFactory::invalidSchema();
            }
        }

        return $phaseDefinition + $defaults;
    }

    /**
     * @return null|object|\stdClass
     */
    private function getMetaSchema()
    {
        if (!$this->metaSchema) {
            $schemaFile = realpath(__DIR__.'/../Resources/config/schema-draft-06.json');

            $this->metaSchema = \League\JsonReference\Dereferencer::draft6()->dereference('file://'.$schemaFile);
        }

        return $this->metaSchema;
    }
}
