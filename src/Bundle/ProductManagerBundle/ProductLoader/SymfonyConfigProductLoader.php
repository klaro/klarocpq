<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ProductManagerBundle\ProductLoader;

use Klaro\Component\ProductLoader\ArrayProductConfigLoader;
use Symfony\Component\Config\ConfigCache;

class SymfonyConfigProductLoader extends ArrayProductConfigLoader
{
    /** @var string */
    protected $rootDir;

    /** @var array */
    protected $phaseFiles;

    /**
     * SymfonyConfigProductLoader constructor.
     * @param array $rootDir
     * @param array $cacheDir
     * @param array $phaseFiles
     * @param array $products
     * @param array $productLines
     * @param bool  $debug
     */
    public function __construct(
        $rootDir,
        $cacheDir,
        array $phaseFiles,
        array $products,
        array $productLines,
        $debug = false
    ) {
        $cache = new ConfigCache($this->getCachePath($cacheDir), $debug);

        // Store data that is read from YAML files to config cache to make loading faster.
        if (/*!$cache->isFresh()*/!is_file($cache->getPath())) {
            $reader = new YamlReader($rootDir);

            $data = [
                'productLines' => $productLines,
                'products' => $products,
                'phases' => $reader->getPhaseConfigs($phaseFiles),
            ];

            // Dump to cache.
            $cache->write(sprintf('<?php return %s;', var_export($data, true)));
        } else {
            $data = require $cache->getPath();
        }

        parent::__construct($data['phases'], $data['products'], $data['productLines']);
    }

    /**
     * @param $cacheDir
     *
     * @return string
     */
    private function getCachePath($cacheDir)
    {
        $cachePath = $cacheDir.'/'.'klaro_product_manager';

        if (!file_exists($cachePath)) {
            if (!mkdir($cachePath) && !is_dir($cachePath)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $cachePath));
            }
        }

        return $cachePath.'/product_config.php.cache';
    }
}
