<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ProductManagerBundle\ProductManager;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Klaro\Component\ProductLoader\ProductConfigLoaderInterface;
use Klaro\Component\ProductManager\ProductManagerInterface;
use Klaro\Component\ProductPhase\PhaseFactory;
use Klaro\Component\ProductPhase\ProductLineDefinition;
use Klaro\ProductManagerBundle\Model\VersionedConfig;
use Klaro\Component\ProductLoader\ArrayProductConfigLoader;
use Klaro\ProductManagerBundle\ProductLoader\SymfonyConfigProductLoader;

class VersionedProductManager implements ProductManagerInterface
{
    /** @var EntityManager */
    protected $em;

    /** @var SymfonyConfigProductLoader */
    protected $productLoader;

    /** @var ProductConfigLoaderInterface[] */
    protected $loaders;

    /**
     * VersionedProductManager constructor.
     * @param ManagerRegistry            $managerRegistry
     * @param SymfonyConfigProductLoader $productLoader
     */
    public function __construct(ManagerRegistry $managerRegistry, SymfonyConfigProductLoader $productLoader)
    {
        $this->em = $managerRegistry->getManagerForClass(VersionedConfig::class);
        $this->productLoader = $productLoader;
        $this->loaders = [];
    }

    /**
     * {@inheritdoc}
     */
    public function getProductLineDefinition($ref, $version = self::VERSION_LATEST, $fetchMode = self::FETCH_CACHED)
    {
        $loader = $this->getProductLoader($ref, $version, $fetchMode);

        return PhaseFactory::create($loader)->createProductLineDefinition($ref);
    }

    /**
     * {@inheritdoc}
     */
    public function getProductLine(ProductLineDefinition $productLineDefinition)
    {
        $loader = $this->getProductLoader($productLineDefinition->getId(), $productLineDefinition->getVersion());

        return PhaseFactory::create($loader)->createProductLinePhase($productLineDefinition);
    }

    /**
     * @return array
     */
    public function getProductLines()
    {
        $productLines = [];

        foreach ($this->getProductLineNames() as $ref => $title) {
            $productLines[$ref] = $this->getProductLineDefinition($ref);
        }

        return $productLines;
    }

    /**
     * @return array
     */
    public function getProductLineNames()
    {
        return $this->productLoader->getAllProductLineRefs();
    }

    /**
     * {@inheritdoc}
     */
    public function hasProductLine($productLine)
    {
        return $this->productLoader->hasProductLineRef($productLine);
    }

    /**
     * @param $ref
     * @param null      $version
     * @param $fetchMode
     *
     * @return ArrayProductConfigLoader|ProductConfigLoaderInterface
     */
    private function getProductLoader($ref, $version = self::VERSION_LATEST, $fetchMode = self::FETCH_CACHED)
    {
        $cacheId = $ref.'/'.$version;

        if ($fetchMode !== self::FETCH_CACHED || $version === ProductManagerInterface::VERSION_LATEST || !isset($this->loaders[$cacheId])) {
            $versionedConfig = $this->findOrCreateVersionedConfig($ref, $version, $fetchMode);

            $versionedData = $versionedConfig->getData();

            $cacheId = $ref.'/'.$versionedConfig->getVersion();

            $this->loaders[$cacheId] = new ArrayProductConfigLoader(
                $versionedData['refs']['phases'],
                $versionedData['refs']['products'],
                [$ref => $versionedData],
                $versionedConfig->getVersion()
            );
        }

        return $this->loaders[$cacheId];
    }

    /**
     * @param $ref
     * @param null $version
     *
     * @return null|object
     */
    private function getVersionedConfig($ref, $version = self::VERSION_LATEST)
    {
        /** @var EntityRepository $repository */
        $repository = $this->em->getRepository(VersionedConfig::class);

        return $repository->findOneBy([
            'name' => $ref,
            'version' => $version,
        ]);
    }

    /**
     * @param $ref
     * @param null      $version
     * @param $fetchMode
     *
     * @return null|object|VersionedConfig
     */
    private function findOrCreateVersionedConfig($ref, $version = self::VERSION_LATEST, $fetchMode = self::FETCH_CACHED)
    {
        $versionedConfig = ($fetchMode === self::FETCH_CACHED) ? $this->getVersionedConfig($ref, $version) : null;

        if (!($versionedConfig instanceof VersionedConfig)) {
            // Get original config
            $adapter = new ProductLineArrayAdapter($this->productLoader, $ref);
            $config = $adapter->toArray();

            // Compare checksum to see if there any changes to previous versions
            $checksum = md5(serialize($config));

            $versionedConfig = $this->getVersionedConfig($ref, $checksum);

            if (!($versionedConfig instanceof VersionedConfig)) {
                $versionedConfig = new VersionedConfig();
                $versionedConfig->setName($ref);
                $versionedConfig->setVersion($checksum);
                $versionedConfig->setUpdatedAt(new \DateTime());
                $versionedConfig->setData($config);

                $this->em->persist($versionedConfig);
                $this->em->flush();
            }
        }

        return $versionedConfig;
    }
}
