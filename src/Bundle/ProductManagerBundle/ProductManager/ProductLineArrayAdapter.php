<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ProductManagerBundle\ProductManager;

use Klaro\Component\ProductLoader\ProductConfigLoaderInterface;
use Klaro\Component\ProductPhase\PhaseNode;
use Klaro\Component\ProductPhase\ProductLine;

class ProductLineArrayAdapter
{
    /** @var ProductConfigLoaderInterface */
    protected $productConfigLoader;

    /** @var ProductLine */
    protected $productLineId;

    /** @var array */
    protected $products;

    /** @var array */
    protected $phases;

    /**
     * ProductLineArrayAdapter constructor.
     * @param ProductConfigLoaderInterface $productConfigLoader
     * @param $productLineId
     */
    public function __construct(ProductConfigLoaderInterface $productConfigLoader, $productLineId)
    {
        $this->productConfigLoader = $productConfigLoader;
        $this->productLineId = $productLineId;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $this->phases = [];
        $this->products = [];

        $productLineConfig = $this->productConfigLoader->getProductLineConfig($this->productLineId);

        foreach ($productLineConfig['phases'] as $phaseId => $phaseConfig) {
            $this->registerPhaseRefs($phaseConfig);
        }

        return $productLineConfig + [
            'refs' => [
                'phases' => $this->phases,
                'products' => $this->products,
            ],
        ];
    }

    /**
     * @param $ref
     */
    private function addProduct($ref)
    {
        if (!isset($this->phases[$ref])) {
            $productConfig = $this->productConfigLoader->getProductConfig($ref);

            $this->registerSinglePhaseRefs($productConfig['phases']);

            $this->products[$ref] = $productConfig;
        }
    }

    /**
     * @param $ref
     */
    private function addPhase($ref)
    {
        if (!isset($this->phases[$ref])) {
            $this->phases[$ref] = $this->productConfigLoader->getPhaseConfig($ref);
        }
    }

    /**
     * @param $phaseConfig
     */
    private function registerPhaseRefs($phaseConfig)
    {
        switch ($phaseConfig['type']) {
            case PhaseNode::PRODUCT_LIST:
                $this->registerProductPhaseRefs($phaseConfig);
                break;

            case PhaseNode::PRODUCT:
                $this->addProduct($phaseConfig['ref']);
                break;

            case PhaseNode::GROUP:
            case PhaseNode::SINGLE:
                $this->registerSinglePhaseRefs($phaseConfig['ref']);
                break;

            default:
                throw new \UnexpectedValueException('Unknow phase type!');
                break;
        }
    }

    /**
     * @param $productRefs
     */
    private function registerProductPhaseRefs($productRefs)
    {
        foreach ($productRefs['ref'] as $productRef) {
            $this->addProduct($productRef);
        }
    }

    /**
     * @param $phaseRefs
     */
    private function registerSinglePhaseRefs($phaseRefs)
    {
        $phaseRefs = is_array($phaseRefs) ? $phaseRefs : [$phaseRefs];

        foreach ($phaseRefs as $phaseRef) {
            $this->addPhase($phaseRef);
        }
    }
}
