# Phase Input data

```json
phase_data = {
    [string]: single_phase | group_phase | product_list_phase | product_phase
}

group_phase = {
  "data": group_phase_data,
  "title": string | null,
  "locked": boolean | null
}

group_phase_data = {
  [string]: single_phase_data,
  "title": string | null,
  "locked": boolean | null
}

product_list_data = {
  	"data": [product_phase_data],
  	"title": string | null,
  	"locked": boolean | null
}

product_phase_data = {
  "ref": string,
  "data": group_phase,
  "title": string | null,
  "locked": boolean | null
}
single_phase_data = {
  "data": phase_values,
  "locked": boolean | null,
  "title": string | null
}

phase_values = {
  [string]: mixed
}
```

