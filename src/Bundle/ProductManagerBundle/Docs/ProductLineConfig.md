# Product Line Config

### Single Phases

```json
phases = {
  [string]: string
}
```

### Products

```json
products = {
  [string]: product
}
```

```json
product = {
  title: string,
  phases: array
}
```

### Product Lines

```json
product_lines = {
  [string]: product_line
}
```

```json
product_line = {
  "title": string,
  "image": string | null,
  "phases": phases
}
```

```json
phases = {
  [string]: single_phase | phase_group | product_phase | product_list_phase
}
```

```json
single_phase = {
  "type": "single",
  "ref": string,
  "title": string | null
}
```

```json
phase_group = {
  "type": "group",
  "ref": array,
  "title": string
}
```

```json
product_phase = {
  "type": "product",
  "ref": string,
  "title": string | null
}
```

```json
product_list_phase = {
  "type: "product_list",
  "ref": array,
  "title": string
}
```

