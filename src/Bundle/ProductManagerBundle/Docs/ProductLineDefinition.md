# Product Line Definition

![](images/phases.png)

A **product line** is a set of different types of phases that form a questionnaire.

A **single phase** is one form page with questions.

A **group phase** consists of one or more single phases to form a multipage form.

A **product** is similar to a grouped phase in that is also multipage form but it can be repeated in a product list.

A **product list** is a list of one or more different products.

![](images/phase_types.png)