# Product Line Structure

The product line structure is similar to the definition, with the exception product lists.

![](images/form_data.png)

The concrete product type under the product list depends on the user selection. The phase definition is used with the input data to generate the final phase strucutre.

![](images/building_phase_tree.png)

For example if we have to products defined to a product list: 

![](images/product_list_definition.png)

Then, if the user added two motors and one process machine the structure would look like this:

![](images/product_list.png)

