<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Cache;

use Klaro\Component\Cache\CacheInterface;
use Klaro\QuotationBundle\Api\RevisionPhaseConfigCacheInterface;

/**
 * Adapter to allow using RevisionPhaseConfigCacheInterface with Phase's cache interface.
 *
 * Class RevisionPhaseConfigCacheAdapter
 * @package Klaro\QuotationBundle\Cache
 */
class RevisionPhaseConfigCacheAdapter implements CacheInterface
{
    /** @var RevisionPhaseConfigCacheInterface */
    protected $revisionCache;

    /**
     * @param RevisionPhaseConfigCacheInterface $revisionCache
     */
    public function __construct(RevisionPhaseConfigCacheInterface $revisionCache)
    {
        $this->revisionCache = $revisionCache;
    }

    /**
     * {@inheritDoc}
     */
    function fetch($id)
    {
        return $this->revisionCache->fetch($id);
    }

    /**
     * {@inheritDoc}
     */
    function contains($id)
    {
        return $this->revisionCache->contains($id);
    }

    /**
     * {@inheritDoc}
     */
    function save($id, $data, $lifeTime = 0)
    {
        return $this->revisionCache->save($id, $data);
    }

    /**
     * {@inheritDoc}
     */
    function delete($id)
    {
        return $this->revisionCache->delete($id);
    }

    /**
     * {@inheritDoc}
     */
    function getStats()
    {
        return null;
    }
}
