<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Cache;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Api\RevisionPhaseConfigCacheManagerInterface;

/**
 * Dummy cache manager. To override this service, create a service which implements
 * RevisionPhaseConfigCacheManagerInterface and set its service id as
 * the provider in the `klaro_quotation.phase_config_cache_provider` config parameter.
 *
 * Class NullPhaseConfigCache
 * @package Klaro\QuotationBundle\Cache
 */
class NullPhaseConfigCache implements RevisionPhaseConfigCacheManagerInterface
{
    /**
     * {@inheritDoc}
     */
    public function getRevisionPhaseConfigCache(QuotationRevisionInterface $revision)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function removeRevisionPhaseConfigCache(QuotationRevisionInterface $revision)
    {
    }
}
