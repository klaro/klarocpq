# Debugging

[TOC]

## Force loading of phase configuration

Because revision data is stored whenever the revision is created,, the phase config rules are not updated whenever the file is changed in the disk. To force a reload, add a `reload=1` url parameter to the page query so that for example the url:

``` 
/quotation/1/revision/1/edit?phaseId=general&subId=0
```

Becomes:

``` 
/quotation/1/revision/1/edit?phaseId=general&subId=0&reload=1
```

This will load the phase configuration from file and save it the revision.

## Logging

The quotation bundle has a loggind service with id  `klaro_quotation.logger`. The logger assumes that Monolog is defined, otherwise no information is logged. Messages logged through this service are logged to the `klaro_quotation` channel.

To log messages, inject the service and call the `info()`, `warning()` or `error()` methods on the logger instance:

``` php
<?php
$logger = $this->get('klaro_quotation.logger');

$logger->warning('Warning note');

// Add context data
$logger->error('Service not found!', [
  'name' => $name
]);
```

Referer to the Monolog configuration on how to setup logging to files.

## Web Debug Toolbar

Logged messages that happen during the quotation process can be found by using the [Symfony Web Debug Toolbar](http://symfony.com/doc/current/book/internals.html#configuration). To be able to see the quotation errors and messages, you need to have the following things configured:

* Enable the toolbar in your `congig.yml`. 
* Add the corresponding role to the user groups that can see the toolbar, see [User Roles](UserRoles.md).

It might be good to add the toolbar to your production config as well as the development config because it will be easier to debug things in production mode. To prevent other users from seeing the toolbar, use the security roles to control access to this feature.

The toolbar extension gives you an overview of any errors that may happen during the process (hover mouse over the quotation bundle icon):

![image](images/toolbar.png )

The see further information, click the toolbar button.
