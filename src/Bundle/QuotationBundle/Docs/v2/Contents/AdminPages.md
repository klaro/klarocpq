# Admin Pages

Manager for services is called `klaro_quotation.admin_page_manager` and it can be accessed with:

``` 
$this->container->get('klaro_quotation.admin_page_manager');
```

Array of services registered to this object can be accessed with

``` 
$this->container->get('klaro_quotation.admin_page_manager')->getAdminPages()
```

**NOTE:** The default admin page is the first available one.

Class diagram:

![image](images/admin_page_uml.png)

Service configuration example:

``` yaml
    osc_automation.admin_legal_offices:
        class:  OSC\AutomationBundle\Library\AdminLegalOffices
        arguments:
            - @templating
        tags:
            - { name: klaro_quotation.admin_page_handler, alias: legal_offices }
```

The interface method `render()` is expected to return the rendered HTML content of the admin section for the body section only. The rest of the layout is automatically included. To use templates to render the page, inject the `templating` service.
