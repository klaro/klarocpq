# Bundle Configuration

[TOC]

## Configuration

The quotation bundle is configured under the key `klaro_quotation` in the application's `config.yml`. Below is an example of a configuration.

``` yaml
parameters:
	phase.path:				/path/to/phase/files
    sales_structure:		# structure array
    product_items:			# array of rules
    
klaro_quotation:
    application_title:      Sales Configurator
    application_id:         my_sales_configurator
    default_product_line:   general
    formatter:              default
    product_lines:
        automation:
            title:          Automation
            phases:
                general:                  %phase.path%/quotation/general.yml
                siteInterfaces:           %phase.path%/commercial/technicalDescription.yml
                baseSystem:               %phase.path%/product/baseSystem.yml
                services:                 %phase.path%/product/services.yml
                spareParts:               %phase.path%/product/spareParts.yml
                delivery:                 %phase.path%/product/delivery.yml
            sales_structure:              %sales_structure%
            product_items:                %product_items%
            documents: 
	        	commercial:
		            type:                 tbs_word
            		title:                Commercial Quotation
            		locale:				  en
            		source:               %project.documents.path%/commercial_quotation.docx
		            disabled:             true
		            visible:              { expression: 'quotation.ProductLine == "automation"' }
            summaries:                    [default, charts]

    summary:
        title:              Price Distribution
        template:           DummyProject:Summary:charts.html.twig

    sidebar:
        calculation:
            title:          Technical Values
            phases:         ['siteInterfaces', 'lines']
            template:       DummyProject:Sidebar:technical.html.twig

        summary:
            title:          Offering Summary
            phases:         ['siteInterfaces', 'lines']
```

## Configuration options

| Option                      | Type   | Optional | Description                              |
| --------------------------- | ------ | -------- | ---------------------------------------- |
| application_title           | string | No       | Application title shown in headers and frontpage |
| application_id              | string | No       | Application identifier string.           |
| default_product_line        | string | Yes      | Define which product is the default one. Must be one of the keys defined in option product_lines (by default the first key). |
| formatter                   | string | No       | Entity Formatter service alias, see [Entity Formatter](EntityFormatter.md) for more info. |
| phase_config_cache_provider | string | Yes      | Service id of used phase config cache provider. |
| cache                       | string | Yes      | Set the used cache provider. In-memory array cache is used by default. Use `array` or `apc` for default implementations or give full service id. Cache provider service must implement the Doctrine cache interface. |
| revision                    | array  | Yes      | Revision configuration, see "Revision" below. |
| product_lines               | array  | No       | Product configuration, consists of product definition and form phases. See "Product Lines" below. |
| summary                     | array  | Yes      | Definition of extra summary pages.       |
| sidebar                     | array  | Yes      | Definitions of tabs in the form sidebar. |

### Revision

The `revision` key allows to configure labels that can be applied to any revision.

#### Qualifiers

Add revisions qualifiers under `klaro_quotation.qualifiers` as an array. The array key is the identifier of the qualifier.

``` yaml
klaro_quotation:
	revision:
    	qualifiers:
    		type:
                title:              Type
                options:
                    firm:           Firm
                    budgetary:      Budgetary
```

| Option  | Optional | Description                              |
| ------- | -------- | ---------------------------------------- |
| title   | No       | Qualifier name                           |
| default | Yes      | Default option id (option key)           |
| options | No       | List of qualifier options as key-value pairs |

### Product Lines

Each product for which quotations are made must be configured under `klaro_quotation.quotation_types`. The configuration is an array where the key defines an identifier that is used in URL and referring to the product internally. Additional configuration options are described below.

| Option          | Type         | Optional | Description                              |
| --------------- | ------------ | -------- | ---------------------------------------- |
| title           | string       | No       | Name of the product.                     |
| phases          | array        | No       | List of phases as as key-value pairs where the key defines the phase id and the value is a path to the phase definition file. For product configuration form phases, see [Phase Definitions](PhaseReference.md) |
| image           | string       | Yes      | Path to the image which is used as logo of the product. |
| sales_structure | array/string | Yes      | Product summary structure, can be either an array, or a string in which case it is assumed to be a parameter that contains an array. See [Sales Structure](SalesStructure.md). |
| product_items   | array/string | Yes      | Product items defininition that can be attached to summary structure. Can be either an array, or a string in which case it is assumed to be a parameter that contains an array. See [Product Items](ProductItems.md). |
| documents       | array        | Yes      | List of output document keys that should be shown in the summary page. |
| summaries       | array        | Yes      | List of summary keys that should be shown in the summary page. Keys refer to the keys defined in `klaro_quotation.summary`. |

### Sidebar pages

Sidebar pages are shown when editing quotation phases. Configuration options:

| Option   | Type   | Required | Notes                                    |
| -------- | ------ | -------- | ---------------------------------------- |
| title    | string | yes      | Name of the sidebar page, shown in sidebar navigation. |
| phases   | array  | yes      | List of phases where this sidebar is visible |
| template | string | no       | Template which is used to render the sidebar |

To add data to the sidebar, listen to the `QuotationEvents::QUOTATION_SIDEBAR` event and add it to the event with `$event->addData('some_string', 'I'm a string')`. Note that this event is sent for all sidebar pages - use the `type` variable from event data to determine the the page type. After that you can use it in your template, eg. `Sidebar note: {{ some_string }}`.

### Summary pages

You can add additional pages to the Offering Summary which will be shown as tabs in the Summary page. Configuration options.

| Option   | Type   | Required | Notes                                    |
| -------- | ------ | -------- | ---------------------------------------- |
| type     | string | No       | Summary Page service alias, see [Summary Pages](SummaryPages.md) |
| title    | string | Yes      | Summary page title                       |
| template | string | No       | Template which is used to render the summary |

To add data to the summary page, listen to the `QuotationEvents::QUOTATION_SUMMARY` event add it to the event with `$event->addData('some_string', 'I'm a string')`. Note that this event is sent for all sidebar pages - use the `type` variable from event data to determine the the page type. By default, only summary page type `summary` will automatically have the offering summary structure. To get access to the summary, get the service (`klaro_quotation.offering_summary`) and load it by calling `$summary->fromCurrentQuotation()`.

### Documents

Each document consists of an id (array key) and the following configuration options under `klaro_quotation.documents`:

| Option   | Type                       | Required | Notes                                    |
| -------- | -------------------------- | -------- | ---------------------------------------- |
| type     | string                     | yes      | Type of output document builder, see [Output Documents](OutputDocuments.md). |
| title    | string / expression        | yes      | Name of the document, shown when showing the available documents in the summary phase. |
| source   | string / expression / list | no       | Path to the document template. If the source consists of other documents, add them as an array to, eg. `{ source: { 'items': [...] } }`. |
| locale   | string / expression        | no       | Language code                            |
| visible  | boolean / expression       | no       | Tells if the document is shown           |
| disabled | boolean / expression       | no       | Tells if the document is shown but disabled |
