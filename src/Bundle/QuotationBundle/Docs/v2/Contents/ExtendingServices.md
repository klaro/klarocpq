# Extending Quotation Bundle Services

[TOC]

## Introduction

You can extend the quotation bundle through [Symfony's tagged services](http://symfony.com/doc/current/components/dependency_injection/tags.html). To list some extensions to QuotationBundle, use the following console command:

``` 
php app/console klaro:quotation:services
```

**NOTE:** List might be incomplete!

## Expression Evaluator

Expression evaluator is used in quotation phases when evaluating conditions or values. You can add variables to the default context by tagging a service with the name `klaro_quotation.expression_context` and pass a parameter `variable` that defines the name of the variable in the evaluation context. 

For example, we want to use a helper service with the name `helper` inside [phase condition expressions](PhaseReference.md). Let's add a service for it and tag it with the name `klaro_quotation.expression_context`:

``` yaml
services:
    acme.phase_helper:
        class: Etc\AcmeBundle\PhaseHelper
        arguments: []
        tags:
            - { name: klaro_quotation.expression_context, variable: 'helper' }
```

Now you can use the service and call any methods on it, for example in a phase like this:

``` yaml
phaseItem:
    fieldName:        SomeField
    condition:        'helper.HasFeature(model) == true'
```

See also [Configuration Objects](ConfigurationObjects.md).

## Admin Pages

See [Admin Pages](AdminPages.md).

## Custom Phase Editor

See [Form Phase Definition](PhaseReference.md).

## Custom Phase Items

See [Phase Form Editor](PhaseFormEditor.md).

## Output Documents

See [Output Documents](OutputDocuments.md).

## Summary Pages

See [Summary Pages](SummaryPages.md) and [Offering Summary](OfferingSummary.md).

## Configuration Objects

See [Configuration Objects](ConfigurationObjects).
