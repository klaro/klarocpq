# Offering Summary Pages

[TOC]

## Introduction

Offering Summary pages present a summary of the user input choices. By default, this is a hierarchical tree for the product structure. You can also define additional summary pages that can complement or replace the default implementation. 

Class diagram:

![image](images/summary_page_uml.png)

Below is the process used to load a summary page.

![image](images/summary_page.png)

## New Offering Summary Service

To add a summary page, create a service that implements the interface in `Klaro\QuotationBundle\Api\SummaryPageInterface` (in practice, it is better to extend `Klaro\QuotationBundle\SummaryPage\AbstractSummaryPage`). 

Tag the service with the name `klaro_quotation.summary_page` and define an alias that can be used to reference the summary page. Summaries can then be configured in [the configuration list](Configuration.md): 

List summaries you want to show in the summary page in the order you want to show them. To include the default-page, use `default`.

``` yaml
klaro_quotation:
    product_lines:
        product:
            summaries:  [default, custom_summary_alias]

    summary:
        custom:
            type:       custom_summary_alias
            title:      Custom Summary
```

## Override default / existing template

To override the default (or some other summary) template or title, override the `template` key in the configuration:

``` yaml
    summary:
        default:
            template:   AcmeBundle:Quotation:summary_template.html.twig

        custom:
            type:       custom_summary_alias
            title:      Custom Summary
            template:   OtherBundle:Quotation:custom_summary.html.twig
```

To show an error message before accessing the summary page, see "Quotation Validation" in [Extending Services](ExtendingServices.md).

## Quotation Validation

It is possible to add checks before accessing the summary and showing an error or a notice to the user if the quotation is not in a valid state. By default, this happens when there are lockable phases that are not locked when accessing the summary. To add a validator to check the quotation state before the summary, add a service implementing the interface in `Klaro\QuotationBundle\Api\QuotationValidatorInterface`. Then tag the service with the name `klaro_quotation.validator`. The service will be called just before accessing the summary and it should add a list of violations or notices to the validation context.

The interface has to methods, **processViolations()** and **processNotices()** which are called respectively before and after the loading the summary page. A violation will cause the summary page to show an error and not let the user proceed before correcting that violation. A notice will let the user see the summary but it will be shown as an info message below the navbar. Note that processNotices() is not called if there were any violations while loading the summary.

For example, to add a notice when sales price is above a threshold value, call `addNotice` on the validation context. To add an error, call `addViolation`. Both methods expect a `QuotationViolation` object as parameter. Setting a url on the `QuotationViolation` object will make the title into a link that can take the user to the offending section.

``` php
<?php

class CustomValidation implements QuotationValidatorInterface {
    // ... get & set properties

    // Called before accessing the summary.
    public function processViolations(QuotationInterface $quotation, QuotationValidator $context) {
    }

    // Called after loading the summary successfully.
    public function processNotices(QuotationInterface $quotation, QuotationValidator $context) {
        if($this->summary->getTotalSalesPrice() > 5000000) {
            $violation = QuotationViolation::create()
                ->setTitle('Risk Analysis')
                ->setReason('Risk analysis is needed if price > 5M');

            $context->addNotice($violation);
        }
    }
}
```

**NOTE:** Validation is only for quotations which are editable.
