# Installation

[TOC]

## Install the bundle with Composer

Add the following lines to your `composer.json` to the relevant sections:

``` json
"repositories": [
    {
        "type": "composer",
        "url":  "https://stash.klarocpq.fi/satis/private"
    }
],

...

"require": {
    ...        
    "klaro/quotation-bundle" : "2.*"
    ...
} 
```

## Requirements

Quotation bundle uses the following Symfony packages and 3rd party bundles:

- **ExpressionLanguage** ([GitHub](https://github.com/symfony/expression-language)) for evaluated expressions
- **Doctrine Cache** component ([GitHub](https://github.com/doctrine/cache)) is used as the cache interface
- **MonologBundle** ([GitHub](https://github.com/symfony/MonologBundle)) for logging
- **FOSJsRoutingBundle** ([GitHub](https://github.com/FriendsOfSymfony/FOSJsRoutingBundle)) to expose Symfony routes to JavaScript scripts
- **JsTranslationBundle** ([GitHub](https://github.com/willdurand/BazingaJsTranslationBundle)) to expose Symfony translations to JavaScript scripts

### Recommended bundles

* **KlaroQuotationExtraBundle** ([Stash](https://stash.klarocpq.fi/projects/SHARED/repos/klaroquotationextrabundle/browse)) for easy database integration using Propel or Doctrine ORM.

## Enable the Quotation bundle and dependencies

Enable the bundles in the kernel:

``` php
<?php
// app/AppKernel.php

public function registerBundles() {
    $bundles = array(
        // Quotation bundle dependencies
        new Symfony\Bundle\MonologBundle\MonologBundle(),
        new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
        new Bazinga\Bundle\JsTranslationBundle\BazingaJsTranslationBundle(),

        new Klaro\QuotationBundle\KlaroQuotationBundle()      
    );
}
```

### Add configuration

Include the quotation bundle config to and the [configuration](Configuration.md) to your `config.yml`:

``` yaml
imports:
    - { resource: "@KlaroQuotationBundle/Resources/config/config.yml" }

klaro_quotation:
    # see configuration options
```

### Add routing

Include the quotation routes in `routing.yml`. 

``` yaml
klaro_quotation:
    resource: "@KlaroQuotationBundle/Resources/config/routing.yml"
    prefix:   /
    options:
        expose:   true
```

**Note:** You must use the `expose` option flag to expose the routes to be also used inside the javascript files.

## Inherit the KlaroQuotationBundle

Best way to use quotation bundle is to use bundle inheritance. Add the parent to your bundle code:

``` php
<?php
// src/Etc/MyBundle/EtcMyBundle.php

class MyBundle extends Bundle {
    public function getParent()
    {
        return 'KlaroQuotationBundle';
    }
}
```

This allows you to easily overwrite controller methods, templates and other resources using [Symfony's bundle inheritance system](http://symfony.com/doc/current/cookbook/bundles/inheritance.html).

## Implement the model events

The quotation bundle uses interfaces to interact with any database so you must implement the model events to provide the quotation bundle with some data to work with.

See: [Events](Events.md) and [Model Events](ModelEvents.md).

If you use **KlaroQuotationExtraBundle** ([Stash](https://stash.klarocpq.fi/projects/SHARED/repos/klaroquotationextrabundle/browse)), you don't need to worry about the model events as the bundle provides implementations for Propel and Doctrine integration.

## Install required assets

Quotation bundle UI uses several JavaScript components which are loaded with **Require.js**. The following components are needed:

* **Require.js 2** (http://requirejs.org/) for script loading
* **Marionette 2** (http://marionettejs.com/) for BackBone views
* **Underscore** (http://underscorejs.org/)
* **jQuery 1.8** (http://jquery.com/)
  * **jQuery Treetable** (http://ludo.cubicphuse.nl/jquery-treetable/) used to collapse offering summary
  * **jQuery File Upload** (https://github.com/blueimp/jQuery-File-Upload) for uploading files in admin section.
* **Bootstrap 3** (http://getbootstrap.com/) for layout
  * **Bootbox 4** (http://bootboxjs.com/) for popups
* **Font Awesome 4** (http://fontawesome.io/) for icons
* **jQuery Datatables** (http://www.datatables.net/) used in quotation listings
  * **Datatables Bootstrap Integration** (https://github.com/DataTables/Plugins/tree/master/integration/bootstrap) for matching Bootstrap layout
* **Chosen 1.1** (http://harvesthq.github.io/chosen/) used to wrap editor dropdowns
  * **chosen-bootstrap** (github.com/dbtek/chosen-bootstrap.git) 
  * **bootstrap-datepicker** (https://github.com/eternicode/bootstrap-datepicker)
* **Select2** (http://select2.github.io/)
  - **Select2 Bootstrap 3 CSS** (https://fk.github.io/select2-bootstrap-css/)
* **Image Picker** (http://rvera.github.io/image-picker/) used image choice groups in editor
* **Messenger** (http://github.hubspot.com/messenger/) for displaying notifications
* **Spectrum** (https://bgrins.github.io/spectrum/) used as color picker in editor
* **AmCharts** ([https://www.amcharts.com](https://www.amcharts.com)) for charts.

The easiest way is to add `.bowerrc` and `bower.json` files to your project:

### .bowerrc

``` json
{
  "directory": "web/vendor/"
}
```

### bower.json

``` json
{
  "name": "Project name",
  "version": "0.0.1",
  "dependencies": {
    "requirejs-bower": "~2.1.1",
    "jquery": "~1.8.0",
    "jquery-treetable": "~3.1.0",
    "jquery-file-upload": "~9.5.7",
    "marionette": "~2",
    "underscore": "~1.6.0",
    "bootstrap": "3",
    "bootbox": "~4.2.0",
    "bootstrap-datepicker": "~1.3.0",
    "font-awesome": "4.7.0",
    "datatables": "~1.10.7",
    "datatables-bootstrap3": "DataTables/Plugins",
    "chosen": "https://github.com/harvesthq/chosen/releases/download/v1.1.0/chosen_v1.1.0.zip",
    "chosen-bootstrap": "~1.0.3",
    "messenger": "~1.4.1",
    "image-picker": "*",
    "select2": "~3.5.2",
    "spectrum": "~1.6.0",
    "amcharts": "~3.10.3"
  }
}
```

### Add styles and javascripts to layout

Add the stylesheets to the layout (paths may vary based on your chosen installation):

``` json
{% stylesheets filter="cssrewrite"
        'vendor/bootstrap/dist/css/bootstrap.min.css'
        'vendor/bootstrap/dist/css/bootstrap-theme.min.css'
        'vendor/bootstrap-datepicker/css/datepicker3.css'
        'vendor/chosen-bootstrap/chosen.bootstrap.min.css'
        'vendor/datatables-bootstrap3/integration/bootstrap/3/dataTables.bootstrap.css'
        'vendor/font-awesome/css/font-awesome.min.css'
        'vendor/jquery-treetable/stylesheets/jquery.treetable.css'
        'vendor/messenger/build/css/messenger.css'
        'vendor/messenger/build/css/messenger-theme-flat.css'
        'vendor/image-picker/image-picker/image-picker.css'
        'vendor/select2/select2.css'
        'vendor/select2/select2-bootstrap.css'
        'vendor/spectrum/spectrum.css'

        'bundles/klaroquotation/css/chosen-bootstrap.css'
        'bundles/klaroquotation/css/navbar.css'
        'bundles/klaroquotation/css/sidebar.css'
        'bundles/klaroquotation/css/phase.css'
%}
<link href="{{ asset_url }}" rel="stylesheet" media="screen" />
{% endstylesheets %}
```

Add JavaScript files. Note that translation files must be loaded separately.

``` json
{% javascripts
    'vendor/requirejs-bower/require.js'
    'bundles/bazingajstranslation/js/translator.min.js'
    'js/translations/config.js'
    'js/translations/KlaroQuotationBundle/*.js'
%}
<script src="{{ asset_url }}"></script>
{% endjavascripts %}
```

Add this command to tell the Quotation bundle where the required libs are:

``` json
{{ klaro_quotation_requirejs_config({
    globals : {
	    'translator' : 'Translator'
	},
    paths: {
        'jquery'               : 'vendor/jquery/jquery',
        'underscore'           : 'vendor/underscore/underscore',
        'backbone'             : 'vendor/backbone/backbone',
        'bootbox'              : 'vendor/bootbox/bootbox',
        'bootstrap'            : 'vendor/bootstrap/dist/js/bootstrap',
        'bootstrap-datepicker' : 'vendor/bootstrap-datepicker/js/bootstrap-datepicker',
        'datatables'           : 'vendor/datatables/media/js/jquery.dataTables',
        'datatables-bootstrap' : 'vendor/datatables-bootstrap3/integration/bootstrap/3/dataTables.bootstrap',
        'chosen'               : 'vendor/chosen/chosen.jquery',
        'marionette'           : 'vendor/marionette/lib/backbone.marionette',
        'backbone.wreqr'       : 'vendor/backbone.wreqr/lib/backbone.wreqr',
        'backbone.babysitter'  : 'vendor/backbone.babysitter/lib/backbone.babysitter',
        'jquery-treetable'     : 'vendor/jquery-treetable/javascripts/src/jquery.treetable',
        'messenger-flat'       : 'vendor/messenger/build/js/messenger-theme-flat',
        'messenger'            : 'vendor/messenger/build/js/messenger',
        'image-picker'         : 'vendor/image-picker/image-picker/image-picker',
        'select2'              : 'vendor/select2/select2.min',
        'spectrum'             : 'vendor/spectrum/spectrum',
        'amcharts'             : 'vendor/amcharts/dist/amcharts/amcharts',
        'amcharts.pie'         : 'vendor/amcharts/dist/amcharts/pie',
        'amcharts.serial'      : 'vendor/amcharts/dist/amcharts/serial',
        'amcharts.theme.light' : 'vendor/amcharts/dist/amcharts/themes/light'
    },
    packages: [
  		{ ... }
	]
}) }}
```

The `packages` key contains paths for custom project specific scripts. For example:

``` json
packages: [
    { name: 'myproject', location: './bundles/myproject/js' },
    { name: 'someother', location: './bundles/someother/js' }
]
```

This will allow to load modules from other sources by prefixing JavaScript modules with package name, eg. `myproject/somemodule`  will load the `somemodule` script from the path set for package `myproject`.

If you have some dependencies which need to be global (eg. jQuery) define them separately under the `globals` key:

``` json
{{ klaro_quotation_requirejs_config({
    globals : {
	    'translator' : 'Translator',
        'jquery'     : 'jQuery'
    },
    paths: {
        ...
    }
}) }}
```

To define custom packages that contain your own javascript modules, or any other RequireJS config option, just add a RequireJS config (this will be automatically merged with any other configuration):

``` javascript
<script type="text/javascript">
    requirejs.config({
        paths: {
            ...
        },
        packages: [
            { name: 'mybundle', location: './bundles/mybundle/js' },
            { ... }
        ],
        urlArgs: "..."
    });
```

To read more about JavaScript modules, [read the documentation here](JavaScriptModules.md).

## Enable logging (optional)

[To enable logs](Debugging.md), add the `klaro_quotation` channel to your Monolog configuration:

``` yaml
monolog:
    handlers:
        klaro_quotation:
            type:       rotating_file
            path:       %kernel.logs_dir%/klaro_quotation.log
            channels:   klaro_quotation
            max_files:  10
```

## See if it works

Go to `http://localhost/[your_project]/quotations` - you should see a listing of quotations. 

## Note about user configuration

The Quotation bundle assumes that there is at least one active user since quotations are linked to a specific user. Therefore you must implement the `Klaro\Component\Common\Model\QuotationUserInterface` and return a valid user in the corresponding event.

Note that this is also automatically handle by the KlaroQuotationExtraBundle.
