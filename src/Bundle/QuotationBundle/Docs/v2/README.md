# Getting started with Quotation Bundle

Table of contents:

#### Introduction

- [Basic Concepts](Contents/BasicConcepts.md)
- [User Guide](Contents/UserGuide.md)

#### Installation & Configuration

* [Installation](Contents/Installation.md)
* [Configuration](Contents/Configuration.md)
* [User Roles](Contents/UserRoles.md)
* [Database Configuration](Contents/DatabaseConfiguration.md)

#### Usage

- [Quotation Facade](Contents/QuotationFacade.md)
- [Searching Quotations](Contents/SearchingQuotations.md)
- [Web API](Contents/WebApi.md)

#### Customization

* [Entity Formatter](Contents/EntityFormatter.md)
* [Creating forms](Contents/PhaseReference.md)
* [Translations / Customizing Used Terminology](Contents/Translations.md)
* [Offering Summary](Contents/OfferingSummary.md)
* [Product Items](Contents/ProductItems.md)
* [Output Documents](Contents/OutputDocuments.md)
* [Summary Pages](Contents/SummaryPages.md)

#### Under the hood

* [Configuration Objects](Contents/ConfigurationObjects.md)
* [Event system](Contents/Events.md)
* [Phase Form Editor](Contents/PhaseFormEditor.md)
* [JavaScript Widgets](Contents/JavaScriptModules.md)

#### Development

* [Extending services](Contents/ExtendingServices.md)
* [Running tests](Contents/Tests.md)
* [Debugging](Contents/Debugging.md)