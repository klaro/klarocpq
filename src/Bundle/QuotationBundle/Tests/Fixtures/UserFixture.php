<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Tests\Fixtures;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Klaro\QuotationBundle\Tests\Entity\User;

class LoadUserData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $userNormal = new User();
        $userNormal->setUsername('user');
        $userNormal->setPassword('userpass');
        $userNormal->setRoles(['ROLE_USER']);
        $manager->persist($userNormal);

        $userAdmin = new User();
        $userAdmin->setUsername('admin');
        $userAdmin->setPassword('adminpass');
        $userAdmin->setRoles(['ROLE_ADMIN']);

        $manager->persist($userAdmin);
        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return FixtureOrdering::FIXTURE_USER;
    }
}
