<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Tests\UserProvider;

use Doctrine\ORM\EntityManager;
use Klaro\QuotationBundle\Tests\Entity\User;
use Symfony\Bridge\Doctrine\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class TestCaseUserProvider implements UserProviderInterface
{
    /** @var EntityManager */
    protected $em;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->em = $managerRegistry->getManagerForClass('KlaroQuotationBundle:User');
    }

    /**
     * {@inheritDoc}
     */
    public function loadUserByUsername($username)
    {
        $repository = $this->em->getRepository('KlaroQuotationBundle:User');

        $user = $repository->findOneBy([
            'username' => $username,
        ]);

        if (!($user instanceof User)) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $username));
        }

        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!($user instanceof User)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return $class === 'Klaro\QuotationBundle\Tests\Entity\User';
    }
}
