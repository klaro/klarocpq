<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;

class BaseTestCase extends KernelTestCase
{
    /** @var Client */
    protected static $client;

    /** @var Client */
    protected static $adminClient;

    /** @var ContainerInterface */
    protected static $container;

    /** @var RouterInterface */
    protected static $router;

    /** @var Application */
    protected static $application;

    /**
     * {@inheritDoc}
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();

        static::bootKernel(array('debug' => true));

        static::$client = static::createClient(array(
            'PHP_AUTH_USER' => 'user',
            'PHP_AUTH_PW'   => 'userpass',
        ));

        self::runCommand('doctrine:database:drop', array("--force" => true));
        self::runCommand('doctrine:database:create');
        self::runCommand('doctrine:schema:update', array("--force" => true));
        self::runCommand('doctrine:fixtures:load', array("--fixtures" => __DIR__."/../Fixtures"));

        static::$container = static::$client->getContainer();
        static::$router = static::$container->get('router');

        static::$adminClient = static::createClient(array(
            'PHP_AUTH_USER' => 'admin',
            'PHP_AUTH_PW'   => 'adminpass',
        ));
    }

    /**
     * @param $command
     * @param array   $options
     *
     * @return int
     */
    protected static function runCommand($command, Array $options = array())
    {
        $options["-e"] = "test";
        $options["-q"] = null;
        $options = array_merge($options, array('command' => $command));

        $input = new \Symfony\Component\Console\Input\ArrayInput($options);
        $input->setInteractive(false);

        return self::getApplication()->run($input);
    }

    /**
     * @return Application
     */
    protected static function getApplication()
    {
        if (null === self::$application) {
            self::$application = new Application(static::$client->getKernel());
            self::$application->setAutoExit(false);
        }

        return self::$application;
    }

    /**
     * Creates a Client.
     *
     * @param array $server An array of server parameters
     *
     * @return Client A Client instance
     */
    protected static function createClient(array $server = array())
    {
        $client = static::$kernel->getContainer()->get('test.client');
        $client->setServerParameters($server);

        return $client;
    }
}
