<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;

class QuotationApiSmokeTest extends BaseTestCase
{
    protected $routes;

    public function __construct()
    {
        // Define API routes that we go through.
        $this->routes = [
            [
                'name' => 'klaro_quotation_api_get_quotation_types',
            ],
            [
                'name'   => 'klaro_quotation_api_get_quotations',
                'params' => ['quotationType' => 'testLine'],
            ],
            [
                'name'   => 'klaro_quotation_api_get_quotation',
                'params' => ['quotationId' => 1],
            ],
            [
                'name'   => 'klaro_quotation_api_post_quotation_users',
                'params' => ['quotationId' => 1],
            ],
            [
                'name'   => 'klaro_quotation_api_post_quotation_users',
                'params' => ['quotationId' => 1],
                'method' => 'POST',
            ],
            [
                'name'   => 'klaro_quotation_api_get_quotation_revisions',
                'params' => ['quotationId' => 1],
            ],
            [
                'name'   => 'klaro_quotation_api_get_quotation_revision',
                'params' => ['quotationId' => 1, 'revisionId' => 1],
            ],
            [
                'name'   => 'klaro_quotation_api_post_quotation_revision',
                'params' => ['quotationId' => 1, 'revisionId' => 1],
                'method' => 'POST',
            ],
            [
                'name' => 'klaro_quotation_api_get_quotation_phase_data',
                'params' => ['quotationId' => 1, 'revisionId' => 1, 'phaseId' => 'general'],
            ],
            [
                'name'   => 'klaro_quotation_api_get_revision_summary',
                'params' => ['quotationId' => 1, 'revisionId' => 1],
            ],
            [
                'name'   => 'klaro_quotation_api_get_revision_summary',
                'params' => ['quotationId' => 1, 'revisionId' => 1],
                'method' => 'POST',
            ],
            [
                'name' => 'klaro_quotation_api_get_users',
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();

        $url = static::$router->generate('klaro_quotation_create', array(
            'quotationType' => 'testLine',
        ));

        // Setup one quotation that has summary data.
        $crawler = static::$client->request('GET', $url);

        $url = static::$router->generate('klaro_quotation_info', array(
            'quotationId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('td:contains("Q1")')->count() > 0);

        // Lock the site interfaces phase.
        $url = static::$router->generate('klaro_quotation_edit_revision', array(
            'quotationId' => 1,
            'revisionId'  => 1,
            'phaseId'     => 'siteInterfaces',
            'lock'        => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('html:contains("Unlock")')->count() > 0);

        // Go to summary to make sure revision ready.
        $url = static::$router->generate('klaro_quotation_summary', array(
            'quotationId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('html:contains("Item Code")')->count() > 0);
    }

    /**
     * Goes through API routes and sees that they don't return error response.
     */
    public function testApiRoutes()
    {
        foreach ($this->routes as $route) {
            $name   = $route['name'];
            $params = isset($route['params']) ? $route['params'] : [];
            $method = isset($route['method']) ? $route['method'] : 'GET';
            $data   = isset($route['data']) ? $route['data'] : [];

            $url = static::$router->generate($name, $params);

            $crawler = static::$adminClient->request($method, $url, $data);

            $this->assertNotContains(static::$adminClient->getResponse()->getStatusCode(), [500, 403]);
        }
    }
}
