<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;

class QuotationSmokeTest extends BaseTestCase
{
    /**
     * Test frontpage.
     */
    public function testQuotationIndex()
    {
        // Test own proposals.
        $crawler = static::$client->request(
            'GET',
            static::$router->generate('klaro_quotation_index')
        );

        $this->assertTrue($crawler->filter('html:contains("Product line 1")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Product line 2")')->count() > 0);
    }

    /**
     * Test proposal listing page.
     */
    public function testQuotationList()
    {
        // Test first configured product line.
        $crawler = static::$client->request(
            'GET',
            static::$router->generate('klaro_quotation_list', array(
                'quotationType' => 'testLine',
            ))
        );

        $this->assertTrue($crawler->filter('html:contains("Product line 1")')->count() > 0);

        // Test other product line.
        $crawler = static::$client->request(
            'GET',
            static::$router->generate('klaro_quotation_list', array(
                'quotationType' => 'otherLine',
            ))
        );

        $this->assertTrue($crawler->filter('html:contains("Product line 2")')->count() > 0);

        // Test quotation type which is not configured.
        $crawler = static::$client->request(
            'GET',
            static::$router->generate('klaro_quotation_list', array(
                'quotationType' => 'notExistingLine',
            ))
        );

        $this->assertEquals(500, static::$client->getResponse()->getStatusCode());
    }

    /**
     * Test creating a quotation.
     */
    public function testCreateQuotation()
    {
        $url = static::$router->generate('klaro_quotation_create', array(
            'quotationType' => 'testLine',
        ));

        $crawler = static::$client->request('GET', $url);

        // Model subscriber just copies the first quotation to simulate creating a new one -> assume copied id
        // to be the created quotation id.
        $redirectUrl = static::$router->generate('klaro_quotation_info', array(
            'quotationId' => 1,
        ));

        $this->assertTrue(
            static::$client->getResponse()->isRedirect($redirectUrl)
        );

        // Load quotation which is owned by the user.
        $url = static::$router->generate('klaro_quotation_info', array(
            'quotationId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('td:contains("Q1")')->count() > 0);
        $this->assertTrue($crawler->filter('a.btn:contains("Edit proposal")')->count() > 0);
        $this->assertTrue($crawler->filter('a.btn:contains("Edit original")')->count() == 0);
        $this->assertTrue($crawler->filter('a.btn:contains("Copy under own account")')->count() > 0);
        $this->assertTrue($crawler->filter('a.btn:contains("Delete proposal")')->count() == 0);

        // Check that a revision was created
        $this->assertTrue($crawler->filter('td:contains("Q1_R1")')->count() > 0);
    }

    /**
     * Test revision creation.
     */
    public function testCreateRevision()
    {
        $this->testCreateQuotation();

        $url = static::$router->generate('klaro_quotation_create_revision', array(
            'quotationId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $url = static::$router->generate('klaro_quotation_info', array(
            'quotationId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('td:contains("Q1_R2")')->count() > 0);
    }

    /**
     * Tests loading of of edit page.
     */
    public function testEditQuotation()
    {
        $this->testCreateQuotation();

        // Open default editing page, quotation owned by user
        $url = static::$router->generate('klaro_quotation_edit', array(
            'quotationId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('html:contains("General Information")')->count() > 0);
        $this->assertTrue($crawler->filter('.phase-content:contains("Sub title")')->count() > 0);

        // Not owned by user, should result in error
        $url = static::$router->generate('klaro_quotation_edit', array(
            'quotationId' => 3,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertEquals(500, static::$client->getResponse()->getStatusCode());
    }

    /**
     * Test linking of quotations.
     */
    public function testLinkQuotation()
    {
        $this->testCreateQuotation();

        // Link quotation which is owned by the user (no linking privilege) -> should fail
        $url = static::$router->generate('klaro_quotation_link', array(
            'quotationId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertEquals(403, static::$client->getResponse()->getStatusCode());

        // Link quotation which is not owned by user, linking privilege -> should succeed
        $url = static::$router->generate('klaro_quotation_link', array(
            'quotationId' => 1,
        ));

        $crawler = static::$adminClient->request('GET', $url);

        $redirectUrl = static::$router->generate('klaro_quotation_info', array(
            'quotationId' => 1,
        ));

        $this->assertTrue(
            static::$adminClient->getResponse()->isRedirect($redirectUrl)
        );

        // Check that the quotation is shown in the list after linking.
        $crawler = static::$adminClient->request(
            'GET',
            static::$router->generate('klaro_quotation_list', array(
                'quotationType' => 'testLine',
            ))
        );

        $this->assertTrue($crawler->filter('td:contains("Q1")')->count() > 0);
    }

    /**
     * Test copying of quotations.
     */
    public function testCopyQuotation()
    {
        $this->testCreateRevision();

        $url = static::$router->generate('klaro_quotation_copy', array(
            'quotationId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $redirectUrl = static::$router->generate('klaro_quotation_info', array(
            'quotationId' => 2,
        ));

        $this->assertTrue(
            static::$client->getResponse()->isRedirect($redirectUrl)
        );

        $crawler = static::$client->request('GET', $redirectUrl);

        $this->assertTrue($crawler->filter('td:contains("Q2")')->count() > 0);
        $this->assertTrue($crawler->filter('td:contains("Q2_R1")')->count() > 0);
        $this->assertTrue($crawler->filter('td:contains("Q2_R2")')->count() > 0);
    }

    /**
     * Test deleting of quotations.
     */
    public function testDeleteQuotation()
    {
        $this->testCreateRevision();

        // Test deleting without delete privilege -> should fail
        $url = static::$router->generate('klaro_quotation_delete', array(
            'quotationId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertEquals(403, static::$client->getResponse()->getStatusCode());

        // Test with privilege -> success
        $crawler = static::$adminClient->request('GET', $url);

        $redirectUrl = static::$router->generate('klaro_quotation_list', array(
            'quotationType' => 'testLine',
        ));

        $this->assertTrue(
            static::$adminClient->getResponse()->isRedirect($redirectUrl)
        );

        $crawler = static::$client->request('GET', $redirectUrl);

        $this->assertTrue($crawler->filter('td:contains("Q1")')->count() == 0);
    }

    /**
     * Tests locking of phases
     */
    public function testLockingOfPhases()
    {
        $this->testCreateQuotation();

        // Open phase that is not lockable, should not show lock button.
        $url = static::$router->generate('klaro_quotation_edit_revision', array(
            'quotationId' => 1,
            'revisionId' => 1,
            'phaseId' => 'general',
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('.js-status-text:contains("In Process")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Lock selections")')->count() == 0);

        // Open phase that is lockable, should show lock button.
        $url = static::$router->generate('klaro_quotation_edit_revision', array(
            'quotationId' => 1,
            'revisionId' => 1,
            'phaseId' => 'siteInterfaces',
        ));

        $crawler = static::$client->request('GET', $url);

        // Status is "in_process", no locking available
        $this->assertTrue($crawler->filter('html:contains("Lock selections")')->count() > 0);

        $input = $crawler->filter('input[name="mockField"]');

        $this->assertEquals('', $input->attr('disabled'));

        // Offering summary should be locked when there are unlocked phases and show alert with unlocked phases.
        $url = static::$router->generate('klaro_quotation_summary', array(
            'quotationId' => 1,
            'revisionId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('.alert:contains("Summary is locked")')->count() > 0);

        // Try locking.
        $url = static::$router->generate('klaro_quotation_edit_revision', array(
            'quotationId' => 1,
            'revisionId' => 1,
            'phaseId' => 'siteInterfaces',
            'lock' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('html:contains("Unlock")')->count() > 0);

        // Offering summary should show now.
        $url = static::$router->generate('klaro_quotation_summary', array(
            'quotationId' => 1,
        ));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('html:contains("Item Code")')->count() > 0);

        $statusUrl = static::$router->generate('klaro_quotation_api_post_quotation_revision', array(
            'quotationId' => 1,
            'revisionId' => 1,
        ));

        // Change status to active
        $crawler = static::$client->request('POST', $statusUrl, [], [], [
            'CONTENT_TYPE' => 'application/json',
            ], json_encode([
                'status' => 'active',
            ]));

        $crawler = static::$client->request('GET', $url);

        $this->assertTrue($crawler->filter('.js-status-text:contains("Active")')->count() > 0);
        $this->assertTrue($crawler->filter('html:contains("Lock selections")')->count() == 0);
    }
}
