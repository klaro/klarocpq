<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Tests\Controller;

use Klaro\Component\ProductManager\ProductManagerInterface;
use Klaro\QuotationBundle\Library\Dictionary;
use Klaro\QuotationBundle\Library\ProductDefinitionManager;
use Klaro\QuotationBundle\Phase\PhaseItem\OptionPhaseItem;
use Klaro\QuotationBundle\Phase\Phase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class PhaseTest extends KernelTestCase
{
    /** @var ContainerInterface */
    protected $container;

    public function setUp()
    {
        static::bootKernel([
            'PHP_AUTH_USER' => 'user',
            'PHP_AUTH_PW'   => 'userpass',
        ]);

        $this->container = static::$kernel->getContainer();
    }



    public function testPhase()
    {


        $templating = $this->getMockBuilder('Symfony\Bundle\FrameworkBundle\Templating\EngineInterface')->getMock();
        $factory = $this->getMockBuilder('Klaro\QuotationBundle\Phase\PhaseItemFactory')->disableOriginalConstructor()->getMock();
        $applicationId = $this->container->getParameter('application_id');

        $phase = new Phase($templating, $factory, new Stopwatch());

        $phaseEditorContextInterface = $this->getMockBuilder('Klaro\QuotationBundle\Phase\PhaseEditorContextInterface')->getMock();
    }

    /**
     * @dataProvider getConfiguration
     */
    public function testOptionPhaseItemOverride(array $configurationArray, $itemId)
    {

        $optionPhaseItem = new OptionPhaseItem();
        $phase = $this->getPhase();

        $optionPhaseItem->initialize($phase, 'testDropdown', $configurationArray);

        $optionPhaseItem->postProcess();
        $options = $optionPhaseItem->get('options');

        $this->assertTrue($options instanceof Dictionary);
        $this->assertTrue($options->get($itemId)['text'] === 'Overriden text');
        $this->assertTrue($options->get('none')['text'] === 'Please select');
        $this->assertTrue($optionPhaseItem->getValue() === 'test');
    }

    public function getConfiguration()
    {
        return array(
            array(
                array(
                    'template' => 'dropdown',
                    'text'      => 'Test dropdown',
                    'fieldName' => 'testDropdown',
                    'type'      => 'dropdown',
                    'optionsSourceType' => 'model',
                    'optionsSource' => 'Klaro\QuotationBundle\Tests\Entity\Model',
                    'optionValueSource' => 'Id',
                    'optionTextSource' => 'phaseId',
                    'overrides' => array(
                        array(
                            'optionTexts' => array(
                                '1-1-0-general' => 'Overriden text',
                            ),
                            'options' => array(
                                'none' => 'Please select',
                            ),
                            'value' => 'test',
                        ),
                    ),
                ),
                '1-1-0-general',
            ),
        );
    }

    /**
     * @param bool|true $mocked
     *
     * @return Phase
     */
    private function getPhase($mocked = true)
    {
        return ($mocked) ? $this->getPhaseMock() : $this->getPhaseService();
    }

    /**
     * @return Phase
     *
     * @throws \Exception
     */
    private function getPhaseService()
    {
        $phase = $this->container->get('klaro_quotation.phase_form');

        $quotationFascade = $this->container->get('klaro_quotation.quotation_facade');
        $quotation = $quotationFascade->getQuotation(1);
        $revision = $quotation->getLatestRevision();

        $phaseDefinition = $quotationFascade->getPhaseDefinition($revision, 'general', ProductManagerInterface::FETCH_FROM_SOURCE, true);
        $model = $this->container->get('klaro_quotation.model_manager')->getPhaseModel($revision, $phaseDefinition->getModel());

        $editor = $this->container->get('klaro_quotation.phase_editor_manager')->createEditor($revision, $phaseDefinition, $model);

        $phase->initialize($editor, $phaseDefinition, $model);

        return $phase;
    }

    /**
     * @return Phase
     */
    private function getPhaseMock()
    {
        // mock phase dependencies
        $templating = $this->getMockBuilder('Symfony\Bundle\FrameworkBundle\Templating\EngineInterface')->getMock();
        $factory = $this->getMockBuilder('Klaro\QuotationBundle\Phase\PhaseItemFactory')->disableOriginalConstructor()->getMock();

        $phase = new Phase($templating, $factory, new Stopwatch());

        // mock arguments for phase initialize method
        $editorContext = $this->getMockBuilder('Klaro\QuotationBundle\Phase\PhaseEditorContextInterface')->getMock();
        $quotationModelManager = $this->container->get('klaro_quotation.model_manager');
        $expectedRes = $quotationModelManager->getItemFinder('Klaro\QuotationBundle\Tests\Entity\Model');
        $editorContext->method('getFinder')->willReturn(
            $expectedRes
        );
        $phaseDefinition = $this->getMockBuilder('Klaro\Component\ProductPhase\PhaseDefinition')->getMock();
        $formModelInterface = $this->getMockBuilder('Klaro\Component\Common\Model\FormModelInterface')->getMock();

        $phase->initialize($editorContext, $phaseDefinition, $formModelInterface);

        return $phase;
    }
}
