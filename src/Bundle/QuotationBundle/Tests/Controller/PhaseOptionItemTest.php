<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Tests\Controller;

use Klaro\QuotationBundle\Library\ExpressionEvaluatorFactory;
use Klaro\Component\Phase\PhaseItem\OptionPhaseItem;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PhaseOptionItemTest extends KernelTestCase
{
    protected $container;

    /** @var ExpressionEvaluatorFactory */
    protected $evaluatorFactory;

    protected $quotation;
    protected $revision;
    protected $phase;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        static::bootKernel([
            'PHP_AUTH_USER' => 'user',
            'PHP_AUTH_PW'   => 'userpass',
        ]);

        $this->container = static::$kernel->getContainer();

        $this->evaluatorFactory = $this->container->get('klaro_quotation.expression_evaluator');
        $this->quotation = null;
        $this->revision  = null;
        $this->phase     = null;
    }

    public function testOptionsPhaseItem()
    {
        $optionItem = new OptionPhaseItem();

        $optionItem->initialize($this->getMockPhase(), 'option', [
            'text'    => 'Plaa',
            'range'   => '1..3',
        ]);
    }

    private function getMockQuotation()
    {
        if (!$this->quotation) {
            $this->quotation = $this->getMock('Klaro\Component\Common\Model\QuotationInterface');
        }

        return $this->quotation;
    }

    private function getMockRevision()
    {
        if (!$this->revision) {
            $this->revision = $this->getMock('Klaro\Component\Common\Model\QuotationRevisionInterface');
            $this->revision->expects($this->any())
                ->method('getQuotation')
                ->will($this->returnValue($this->getMockQuotation()));
        }

        return $this->revision;
    }

    private function getMockPhase()
    {
        if (!$this->phase) {
            $this->phase = $this->getMockBuilder('Klaro\QuotationBundle\Phase\Phase')
                ->disableOriginalConstructor()
                ->getMock();

            $this->phase->expects($this->any())
                ->method('evaluateExpression')
                ->will($this->returnCallback(
                    function ($expression, $context = []) {
                        $evaluator = $this->evaluatorFactory->getEvaluator($this->revision);

                        return $evaluator->evaluateExpression($expression, $context);
                    }
                ));
        }

        return $this->phase;
    }
}
