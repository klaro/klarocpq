<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\QuotationContext;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Facade\QuotationFacade;
use Symfony\Component\HttpFoundation\RequestStack;

class QuotationContext
{

    /** @var mixed  */
    protected $quotationId;

    /** @var mixed  */
    protected $revisionId;

    /** @var  QuotationRevisionInterface */
    protected $revision;

    /** @var  QuotationInterface */
    protected $quotation;

    /**
     * QuotationContext constructor.
     *
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack, QuotationFacade $quotationFacade)
    {
        $this->quotation = null;
        $this->revision = null;

        $request = $requestStack->getCurrentRequest();

        $this->quotationId = $request->get('quotationId');
        $this->revisionId = $request->get('revisionId');

        if (!is_null($this->quotationId)) {
            $this->quotation = $quotationFacade->getQuotation($this->quotationId);

            if (is_null($this->revisionId)) {
                $this->revision = $this->quotation->getLatestRevision();
            } else {
                $this->revision = $quotationFacade->getQuotationRevision($this->quotation, $this->revisionId);
            }
        }
    }

    public function getQuotation()
    {
        return $this->quotation;
    }

    public function getRevision()
    {
        return $this->revision;
    }
}
