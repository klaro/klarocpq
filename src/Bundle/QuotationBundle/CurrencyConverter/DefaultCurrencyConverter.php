<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\CurrencyConverter;

class DefaultCurrencyConverter implements CurrencyConverterInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDefaultCurrency()
    {
        return 'EUR';
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrency()
    {
        return 'EUR';
    }

    /**
     * {@inheritdoc}
     */
    public function toCurrency($amount)
    {
        return $amount;
    }

    /**
     * {@inheritdoc}
     */
    public function fromCurrency($amount)
    {
        return $amount;
    }
}
