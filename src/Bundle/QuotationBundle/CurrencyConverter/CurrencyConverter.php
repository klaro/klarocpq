<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\CurrencyConverter;

use Psr\Log\LoggerInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Library\ProductDefinitionManager;

class CurrencyConverter
{
    /** @var CurrencyConverterInterface[] */
    protected $currencyConverters;

    /** @var LoggerInterface */
    protected $logger;

    /** @var ProductDefinitionManager */
    protected $productManager;

    /** @var array */
    protected $quotationTypeConverters;

    /**
     * CurrencyConverter constructor.
     * @param ProductDefinitionManager $productManager
     * @param array                    $quotationTypeConverters
     */
    public function __construct(
        ProductDefinitionManager $productManager,
        LoggerInterface $logger,
        array $quotationTypeConverters = []
    ) {
        $this->logger = $logger;
        $this->productManager = $productManager;
        $this->quotationTypeConverters = $quotationTypeConverters;
    }

    /**
     * @param $alias
     * @param CurrencyConverterInterface $currencyConverter
     */
    public function addCurrencyConverter($alias, CurrencyConverterInterface $currencyConverter): void
    {
        $this->currencyConverters[$alias] = $currencyConverter;
    }

    /**
     * @param QuotationRevisionInterface $revision
     *
     * @return CurrencyConverterInterface
     */
    public function getCurrencyConverterForRevision(QuotationRevisionInterface $revision): CurrencyConverterInterface
    {
        $quotationType = $revision->getQuotation()->getQuotationType();

        $configuredQuotationType = $this->quotationTypeConverters[$quotationType] ?? null;
        if ($configuredQuotationType) {
            $currencyConverterAlias = $configuredQuotationType['currency_converter'] ?? null;
        }

        $converter = $this->getCurrencyConverter($currencyConverterAlias ?? null);
        if ($converter instanceof RevisionAwareCurrencyConverter) {
            $converter->setRevision($revision);
        }

        if ($converter instanceof FormDataAwareCurrencyConverter) {
            $converter->setFormData($this->productManager->getFormDataForRevision($revision));
        }

        return $converter;
    }

    /**
     * @param $alias
     *
     * @return CurrencyConverterInterface
     */
    public function getCurrencyConverter($alias): CurrencyConverterInterface
    {
        if (isset($this->currencyConverters[$alias])) {
            return $this->currencyConverters[$alias];
        }

        $this->logger->error('No currency converter found with alias "'.$alias.'"!');

        return new DefaultCurrencyConverter();
    }
}
