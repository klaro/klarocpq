<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Document\Zip;

use Klaro\QuotationBundle\Document\GenericOutputDocument;

/**
 * Represents zip file.
 *
 * Class ZipOutputDocument
 * @package Klaro\QuotationBundle\Document\Zip
 */
class ZipOutputDocument extends GenericOutputDocument
{
    public function __construct()
    {
        parent::__construct();

        $this->addHeader('Content-Type', 'application/zip');

        $this->setExtension('zip');
    }
}
