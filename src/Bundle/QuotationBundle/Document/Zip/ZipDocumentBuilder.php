<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Document\Zip;

use Klaro\QuotationBundle\Api\DocumentBuilderInterface;
use Klaro\QuotationBundle\Api\DocumentDefinitionInterface;
use Klaro\QuotationBundle\Api\OutputDocumentInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Document\CompositeDocumentDefinition;
use Klaro\QuotationBundle\Document\OutputDocumentManager;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Document builder for generaring zip archives of other output documents.
 *
 * Class ZipDocumentBuilder
 * @package Klaro\QuotationBundle\Document\Zip
 */
class ZipDocumentBuilder implements DocumentBuilderInterface
{

    /** @var OutputDocumentManager */
    protected $context;

    /** @var KernelInterface */
    protected $kernel;

    /**
     * @param KernelInterface $kernel
     */
    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * {@inheritDoc}
     */
    public function initialize(OutputDocumentManager $context)
    {
        $this->context = $context;
    }

    /**
     * {@inheritDoc}
     */
    public function generate(QuotationRevisionInterface $revision, DocumentDefinitionInterface $definition)
    {
        if ($definition instanceof CompositeDocumentDefinition) {
            $definitions = $definition->getDefinitions();
        } else {
            $definitions = [$definition];
        }

        return $this->generateFromOutputDocuments(
            $this->context->generateDocuments($revision, $definitions),
            $definition instanceof CompositeDocumentDefinition ? $revision->getIdentifier() : $definition->getTitle()
        );
    }

    /**
     * @param array|OutputDocumentInterface[] $documents
     * @param string                          $title
     *
     * @return ZipOutputDocument
     */
    public function generateFromOutputDocuments($documents, $title)
    {
        $filePath = $this->context->getTemporaryFileName();

        $zip = new \ZipArchive();
        $zip->open($filePath, \ZipArchive::CREATE);

        /** @var OutputDocumentInterface $document */
        foreach ($documents as $document) {
            if (!$document->isSaved()) {
                $this->context->saveTemporaryFile($document);
            }

            // If the document added to the zip file is another zip output document, extract its contents
            // into a separate folder inside the new zip.
            if ($document instanceof ZipOutputDocument) {
                $tmpZip = new \ZipArchive();
                $tmpZip->open($document->getServerPath());

                for ($i = 0; $i < $tmpZip->numFiles; $i++) {
                    $tmpFileName = $tmpZip->getNameIndex($i);

                    $zip->addFromString($document->getTitle().'/'.$tmpFileName, $tmpZip->getFromIndex($i));
                }

                $tmpZip->close();
            } else {
                // Otherwise just add the file as it is.
                $zip->addFile($document->getServerPath(), $document->getFileNameWithExtension());
            }
        }

        $zip->close();

        $output = ZipOutputDocument::create()
            ->setFileName($title)
            ->setTitle($title)
            ->setServerPath($filePath)
            ->setTemporary(true)
            ->setSaved(true);

        return $output;
    }
}
