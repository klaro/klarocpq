<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Document;

use Klaro\Component\ExpressionEvaluator\ExpressionEvaluatorInterface;
use Klaro\Component\ProductManager\ProductManagerInterface;
use Klaro\QuotationBundle\Api\DocumentBuilderInterface;
use Klaro\QuotationBundle\Api\DocumentDefinitionInterface;
use Klaro\Component\Common\Event\DocumentEvents;
use Klaro\QuotationBundle\Api\OutputDocumentInterface;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Event\QuotationDocumentEvent;
use Klaro\QuotationBundle\Library\ExpressionEvaluatorFactory;
use Klaro\QuotationBundle\Traits\QuotationParameterTraits;
use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Klaro\QuotationBundle\Phase\PhaseVariableFactory;

/**
 * Manager for output document builders and output document lists.
 *
 * Class OutputDocumentManager
 * @package Klaro\QuotationBundle\Document
 */
class OutputDocumentManager
{

    use QuotationParameterTraits, QuotationServiceTraits;
    /** @var array */
    protected $outputDocumentConfigs;

    /** @var string[] */
    protected $builderServiceIds;

    /** @var DocumentBuilderInterface[] */
    protected $builderServices;

    /** @var QuotationInterface */
    protected $quotation;

    /** @var OutputDocumentInterface[] */
    protected $cleanupDocuments;

    /** @var ContainerInterface */
    protected $container;

    /** @var string */
    protected $cacheDir;

    /** @var string */
    protected $rootDir;

    /** @var PhaseVariableFactory */
    protected $phaseVariableFactory;

    /** @var ExpressionEvaluatorFactory */
    protected $evaluatorFactory;

    /** @var ExpressionEvaluatorInterface */
    protected $evaluator;

    /**
     * @param ContainerInterface $container
     * @param $outputDocuments
     * @param $cacheDir
     * @param $rootDir
     */
    public function __construct(
        ContainerInterface $container,
        PhaseVariableFactory $phaseVariableFactory,
        ExpressionEvaluatorFactory $evaluatorFactory,
        $outputDocuments,
        $cacheDir,
        $rootDir
    ) {
        $this->container = $container;

        $this->phaseVariableFactory = $phaseVariableFactory;
        $this->evaluatorFactory = $evaluatorFactory;

        $this->outputDocumentConfigs = $outputDocuments;

        $this->builderServiceIds = [];
        $this->builderServices   = [];
        $this->cleanupDocuments  = [];

        $this->cacheDir = $cacheDir.'/klaro_quotation';
        $this->rootDir  = $rootDir;

        // Create cache dir if it doesn't exist.
        if (!is_dir($this->cacheDir)) {
            mkdir($this->cacheDir, 0775, true);
        }
    }

    /**
     * Return cache directory path.
     *
     * @return string
     */
    public function getCacheDir()
    {
        return $this->cacheDir;
    }

    /**
     * Return root directory path.
     *
     * @return string
     */
    public function getRootDir()
    {
        return $this->rootDir;
    }

    public function initEvaluator(QuotationRevisionInterface $revision)
    {
        $quotationFacade = $this->getQuotationFacade();
        $productLine = $quotationFacade->getFormDataForRevision($revision);

        return $this->evaluator = $this->evaluatorFactory->getEvaluator($revision, [
            'globals' => $this->phaseVariableFactory->createVariables($revision, $productLine),
            'user' => $this->getModelProvider()->getCurrentUser(),
            'product' => $productLine,
        ]);
    }

    /**
     * Get output documents for given revision.
     *
     * @param QuotationInterface $quotation Quotation revision.
     *
     * @return DocumentDefinitionInterface[] List of output documents.
     */
    public function getQuotationDocuments(QuotationRevisionInterface $revision)
    {
        $quotation = $revision->getQuotation();

        $documents = array_keys($this->outputDocumentConfigs[$quotation->getQuotationType()]);

        $documentList = [];

        foreach ($documents as $documentId) {
            $documentList[$documentId] = $this->getDocumentDefinition($revision, $documentId);
        }

        return $documentList;
    }

    /**
     * Get output document configuration array.
     *
     * @param string $documentId Document id
     * @param bool   $draft
     * @param bool   $debug
     *
     * @return mixed|DocumentDefinitionInterface
     *
     * @throws \Exception
     */
    public function getDocumentDefinition(QuotationRevisionInterface $revision, $documentId, $draft = false, $debug = false)
    {
        $config = $this->getDocumentConfig($revision, $documentId);

        if (isset($config['source']['items']) && is_array($config['source']['items'])) {
            /** @var CompositeDocumentDefinition $definition */
            $definition = CompositeDocumentDefinition::create();

            foreach ($config['source']['items'] as $id) {
                // Prevent adding self to list.
                if ($id != $documentId) {
                    $definition->addDefinition($this->getDocumentDefinition($revision, $id, $draft, $debug));
                }
            }

            unset($config['source']);
        } else {
            $definition = DocumentDefinition::create();
        }

        $definition
            ->setId($documentId)
            ->setType(isset($config['type']) ? $this->processExpression($revision, $config['type']) : 'file')
            ->setSource(isset($config['source']) ? $this->processExpression($revision, $config['source']) : null)
            ->setTitle(isset($config['title']) ? $this->processExpression($revision, $config['title']) : null)
            ->setLocale(isset($config['locale']) ? $this->processExpression($revision, $config['locale']) : null)
            ->setIsVisible(isset($config['visible']) ? (bool) $this->processExpression($revision, $config['visible']) : true)
            ->setIsDisabled(isset($config['disabled']) ? (bool) $this->processExpression($revision, $config['disabled']) : false)
            ->setIsDraft($draft)
            ->setIsDebug($debug);

        return $definition;
    }

    /**
     * Create unique filename in cache directory that can be used for a temporary filename.
     *
     * @return string Full path to file
     */
    public function getTemporaryFileName()
    {
        do {
            $filename = $this->cacheDir.'/'.uniqid();
        } while (is_dir($filename) || file_exists($filename));

        return $filename;
    }

    /**
     * Save an output document to a temporary file.
     *
     * @param OutputDocumentInterface $document Output document.
     *
     * @throws \Exception
     */
    public function saveTemporaryFile(OutputDocumentInterface $document)
    {
        $path = $document->getServerPath();

        if (empty($path)) {
            $path = $this->getTemporaryFileName();
            $document->setServerPath($path);
        }

        if (is_dir($path) || file_exists($path)) {
            throw new \Exception('File exists');
        }

        file_put_contents($path, $document->getContent());

        $document->setSaved(true);
        $document->setTemporary(true);
    }

    /**
     * Add document builder service.
     *
     * @param string $type    Builder alias
     * @param string $builder Builder service id
     *
     * @throws \Exception
     */
    public function addBuilderService($type, $builder)
    {
        if (isset($this->builderServiceIds[$type])) {
            throw new \Exception("Builder service '{$builder}' with type '{$type}' already added!");
        }

        if (!$this->container->has($builder)) {
            throw new \Exception("Builder service '{$builder}' with type '{$type}' does not exist!");
        }

        $this->builderServiceIds[$type] = $builder;
    }

    /**
     * Get list of builder services.
     *
     * @return array|\string[]
     */
    public function getBuilderServices()
    {
        return $this->builderServiceIds;
    }

    /**
     * Get builder service with given builder alias.
     *
     * @param string $type Builder alias
     *
     * @return DocumentBuilderInterface
     */
    public function getBuilder($type)
    {
        if (!isset($this->builderServiceIds[$type])) {
            throw new \Exception("Builder service with type '{$type}' does not exist!");
        }

        if (!isset($this->builderServices[$type])) {
            $this->builderServices[$type] = $this->container->get($this->builderServiceIds[$type]);

            $this->builderServices[$type]->initialize($this);
        }

        return $this->builderServices[$type];
    }

    /**
     * Generate & return an output document for the given revision and document id. If more than one document id
     * is requested, the files will be grouped together in a zip file.
     *
     * @param QuotationRevisionInterface $revision   Quotation revision
     * @param string|array               $documentId Document id or an array of document ids.
     * @param bool                       $draft      Flag for the builder service to include a draft marking (eg. watermark)
     * @param bool                       $debug      Flag for the builder service to include debugging information
     *
     * @return OutputDocumentInterface
     *
     * @throws \Exception
     *
     * @see generateDocument()
     */
    public function generate(QuotationRevisionInterface $revision, $documentId, $draft = false, $debug = false)
    {
        if (is_array($documentId)) {
            // Make sure keys start from zero.
            $documentId = array_values($documentId);

            // If there is only one document, pass it without zipping.
            if (count($documentId) >= 2) {
                /** @var CompositeDocumentDefinition $definition */
                $definition = CompositeDocumentDefinition::create()
                    ->setType('zip')
                    ->setTitle($revision->getTitle())
                    ->setIsDraft($draft)
                    ->setIsDebug($debug);

                foreach ($documentId as $id) {
                    $definition->addDefinition($this->getDocumentDefinition($revision, $id, $draft, $debug));
                }

                return $this->generateDocument($revision, $definition);
            } elseif (count($documentId) == 1) {
                $documentId = $documentId[0];
            } else {
                throw new \Exception('No documents to generate.');
            }
        }

        return $this->generateDocument($revision, $this->getDocumentDefinition($revision, $documentId, $draft, $debug));
    }

    /**
     * Add document for cleanup on terminate event.
     *
     * @param OutputDocumentInterface $document
     *
     * @return OutputDocumentInterface
     */
    public function addDocumentForDeletion(OutputDocumentInterface $document)
    {
        $this->cleanupDocuments[] = $document;

        return $document;
    }

    /**
     * Cleanup created document files. This method is called automatically when a kernel.terminate event is received.
     */
    public function onKernelTerminate()
    {
        if (count($this->cleanupDocuments) > 0) {
            foreach ($this->cleanupDocuments as $document) {
                if ($document->isTemporary() && $document->isSaved()) {
                    $serverPath = $document->getServerPath();

                    if (!is_dir($serverPath) && file_exists($serverPath)) {
                        unlink($serverPath);
                    }
                }
            }
        }
    }

    /**
     * Generate one output document for given revision and document id.
     * @param QuotationRevisionInterface $revision
     * @param string                     $documentId Document id
     * @param bool                       $debug      Flag for the builder service to include debugging information
     * @param bool                       $draft      Flag for the builder service to include a draft marking (eg. watermark)
     *
     * @return OutputDocumentInterface
     *
     * @see generate()
     */
    public function generateDocument(QuotationRevisionInterface $revision, DocumentDefinitionInterface $definition)
    {
        $builder = $this->getBuilder($definition->getType());

        // Send event to allow overrides to document.
        $event = new QuotationDocumentEvent($revision, $builder, array(
            'definition' => $definition,
        ));

        $eventDispatcher = $this->getEventDispatcher();
        $eventDispatcher->dispatch(DocumentEvents::QUOTATION_DOCUMENT_GENERATE, $event);

        $definition = $event->getData('definition');

        $document = $builder->generate($revision, $definition);

        $event->addData('document', $document);

        $eventDispatcher->dispatch(DocumentEvents::QUOTATION_DOCUMENT_READY, $event);

        return $this->addDocumentForDeletion($event->getData('document'));
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param array                      $definitions
     *
     * @return OutputDocumentInterface[]
     */
    public function generateDocuments(QuotationRevisionInterface $revision, array $definitions)
    {
        $documents = [];

        foreach ($definitions as $definition) {
            $documents[] = $this->generateDocument($revision, $definition);
        }

        return $documents;
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param $rawValue
     *
     * @return mixed|null|string
     */
    private function processExpression(QuotationRevisionInterface $revision, $rawValue)
    {
        if (!$this->evaluator) {
            $this->evaluator = $this->initEvaluator($revision);
        }
        $value = $rawValue;

        if (is_array($rawValue) && isset($rawValue['expression'])) {
            if (!isset($rawValue['evaluate']) || $rawValue['evaluate'] !== false) {
                $value = $this->evaluator->evaluateExpression($rawValue['expression']);
            } else {
                $value = $rawValue['expression'];
            }
        }

        return $value;
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param $documentId
     *
     * @return array
     *
     * @throws \Exception
     */
    private function getDocumentConfig(QuotationRevisionInterface $revision, $documentId)
    {
        $quotation = $revision->getQuotation();

        $documentConfigs = $this->outputDocumentConfigs[$quotation->getQuotationType()];

        if (!array_key_exists($documentId, $documentConfigs)) {
            throw new \Exception('Document config for document id "'.$documentId.'" not found!');
        }

        return $documentConfigs[$documentId];
    }
}
