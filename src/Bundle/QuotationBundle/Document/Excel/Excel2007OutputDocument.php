<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Document\Excel;

use Klaro\QuotationBundle\Document\GenericOutputDocument;

/**
 * Represents Excel 2007 document file.
 *
 * Class Excel2007OutputDocument
 * @package Klaro\QuotationBundle\Document\Excel
 */
class Excel2007OutputDocument extends GenericOutputDocument
{
    public function __construct()
    {
        parent::__construct();

        $this->setExtension('xlsx');

        $this->addHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    }
}
