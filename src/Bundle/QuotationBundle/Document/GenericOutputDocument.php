<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Document;

use Klaro\QuotationBundle\Api\OutputDocumentInterface;

/**
 * Generic output document, this class can be used as a base class for other output documents.
 *
 * @package Klaro\QuotationBundle\Document
 */
class GenericOutputDocument implements OutputDocumentInterface
{
    protected $title;

    protected $headers;

    protected $fileName;

    protected $extension;

    protected $serverPath;

    protected $content;

    protected $isSaved;

    protected $isTemporary;

    /**
     * Constructor
     */
    protected function __construct()
    {
        $this->title       = '';
        $this->headers     = [];
        $this->fileName    = '';
        $this->extension   = '';
        $this->serverPath  = '';
        $this->content     = null;
        $this->isSaved     = false;
        $this->isTemporary = true;
    }

    /**
     * Create new output document instance.
     *
     * @return OutputDocumentInterface
     */
    public static function create()
    {
        return new static;
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle()
    {
        return !empty($this->title) ? $this->title : $this->getFileName();
    }

    /**
     * {@inheritDoc}
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * {@inheritDoc}
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * {@inheritDoc}
     */
    public function getFileNameWithExtension()
    {
        return $this->getFileName().'.'.$this->getExtension();
    }

    /**
     * {@inheritDoc}
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    public function addHeader($name, $value)
    {
        $this->headers[$name] = $value;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function setServerPath($serverPath)
    {
        $this->serverPath = $serverPath;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getServerPath()
    {
        return $this->serverPath;
    }

    /**
     * {@inheritDoc}
     */
    public function getContent()
    {
        if (!$this->content) {
            $this->content = file_get_contents($this->getServerPath());
        }

        return $this->content;
    }

    /**
     * {@inheritDoc}
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function setSaved($isSaved)
    {
        $this->isSaved = $isSaved;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function isSaved()
    {
        return $this->isSaved;
    }

    /**
     * {@inheritDoc}
     */
    public function setTemporary($isTemporary)
    {
        $this->isTemporary = $isTemporary;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function isTemporary()
    {
        return $this->isTemporary;
    }
}
