<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Document\Word;

use Klaro\QuotationBundle\Document\GenericOutputDocument;

/**
 * Represents Word document file.
 *
 * Class WordOutputDocument
 * @package Klaro\QuotationBundle\Document\Word
 */
class WordOutputDocument extends GenericOutputDocument
{
    public function __construct()
    {
        parent::__construct();

        $this->addHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document');

        $this->setExtension('docx');
    }
}
