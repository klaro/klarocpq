<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Document;

use Klaro\QuotationBundle\Api\DocumentBuilderInterface;
use Klaro\QuotationBundle\Api\DocumentDefinitionInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

/**
 * Document builder that passes the given source document as an output document.
 *
 * Class PassthroughDocumentBuilder
 * @package Klaro\QuotationBundle\Document
 */
class PassthroughDocumentBuilder implements DocumentBuilderInterface
{
    /** @var OutputDocumentManager */
    protected $context;

    /**
     * {@inheritDoc}
     */
    public function initialize(OutputDocumentManager $context)
    {
        $this->context = $context;
    }

    /**
     * {@inheritDoc}
     */
    public function generate(QuotationRevisionInterface $revision, DocumentDefinitionInterface $definition)
    {
        $filePath = $this->context->getRootDir().'/../'.$definition->getSource();

        $title = $definition->getTitle();
        $document = null;

        if (!is_dir($filePath) && file_exists($filePath)) {
            $pathinfo = pathinfo($filePath);

            $fileTitle = !empty($title) ? $title : $pathinfo['filename'];
            $contentType = mime_content_type($filePath);

            $document = GenericOutputDocument::create()
                ->setServerPath($filePath)
                ->setFileName($fileTitle)
                ->addHeader('Content-Type', $contentType)
                ->setTemporary(false)
                ->setExtension($pathinfo['extension'])
                ->setSaved(true);
        }

        return $document;
    }
}
