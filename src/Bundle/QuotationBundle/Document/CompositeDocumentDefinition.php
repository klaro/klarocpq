<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Document;

use Klaro\QuotationBundle\Api\DocumentDefinitionInterface;

/**
 * Class CompositeDocumentDefinition
 * @package Klaro\QuotationBundle\Document
 */
class CompositeDocumentDefinition extends DocumentDefinition
{
    /** @var DocumentDefinitionInterface[] */
    protected $definitions;

    /**
     * CompositeDocumentDefinition constructor.
     */
    protected function __construct()
    {
        parent::__construct();

        $this->definitions = [];
    }

    /**
     * @return DocumentDefinitionInterface[]
     */
    public function getDefinitions()
    {
        return $this->definitions;
    }

    /**
     * @param DocumentDefinitionInterface $definition
     */
    public function addDefinition(DocumentDefinitionInterface $definition)
    {
        $this->definitions[] = $definition;
    }

    /**
     * @param DocumentDefinitionInterface[] $definitions
     */
    public function setDefinitions($definitions)
    {
        $this->definitions = $definitions;
    }
}
