<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Document;

use Klaro\QuotationBundle\Api\DocumentDefinitionInterface;

/**
 * Class DocumentDefinition
 * @package Klaro\QuotationBundle\Document
 */
class DocumentDefinition implements DocumentDefinitionInterface
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $title;

    /** @var string */
    protected $type;

    /** @var string */
    protected $source;

    /** @var string */
    protected $locale;

    /** @var bool */
    protected $isDraft;

    /** @var bool */
    protected $isDebug;

    /** @var bool */
    protected $isVisible;

    /** @var bool */
    protected $isDisabled;

    /**
     * DocumentDefinition constructor.
     */
    protected function __construct()
    {
        $this->id         = null;
        $this->title      = null;
        $this->type       = null;
        $this->source     = null;
        $this->locale     = null;
        $this->isDraft    = false;
        $this->isDebug    = false;
        $this->isVisible  = true;
        $this->isDisabled = false;
    }

    /**
     * @return DocumentDefinition
     */
    public static function create()
    {
        return new static;
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $source
     *
     * @return $this
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function isDraft()
    {
        return $this->isDraft;
    }

    /**
     * @param boolean $isDraft
     *
     * @return $this
     */
    public function setIsDraft($isDraft)
    {
        $this->isDraft = $isDraft;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function isDebug()
    {
        return $this->isDebug;
    }

    /**
     * @param boolean $isDebug
     *
     * @return $this
     */
    public function setIsDebug($isDebug)
    {
        $this->isDebug = $isDebug;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function isVisible()
    {
        return $this->isVisible;
    }

    /**
     * @param boolean $isVisible
     *
     * @return $this
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function isDisabled()
    {
        return $this->isDisabled;
    }

    /**
     * @param boolean $isDisabled
     *
     * @return $this
     */
    public function setIsDisabled($isDisabled)
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }
}
