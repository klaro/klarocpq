<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Facade;

use Klaro\Component\Configurator\ConfiguratorManagerInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorProductDefinitionNode;
use Klaro\Component\Common\Model\FinderInterface;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\ProductManager\ProductManagerInterface;
use Klaro\Component\ProductPhase\ProductLine;
use Klaro\Component\ProductPhase\ProductLineDefinition;
use Klaro\QuotationBundle\Api\QuotationRevisionOrderStatus;
use Klaro\QuotationBundle\Api\QuotationRevisionStatus;
use Klaro\Component\QuotationSearch\Query\QuotationQueryInterface;
use Klaro\Component\Common\Result\ResultSetInterface;
use Klaro\Component\QuotationSearch\Query\UserQueryInterface;
use Klaro\QuotationBundle\Event\QuotationUserEvent;
use Klaro\QuotationBundle\Library\ProductDefinitionManager;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Symfony\Component\EventDispatcher\Event;
use Klaro\QuotationBundle\Api\FormModelProviderInterface;
use Klaro\Component\Common\Event\QuotationFacadeEvents;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\QuotationBundle\Event\QuotationEvent;
use Klaro\QuotationBundle\Event\QuotationRevisionEvent;
use Klaro\Component\Logger\Logger;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * QuotationFacade provides a centralized interface to managing quotations, revisions and their models.
 *
 * Class QuotationFacade
 * @package Klaro\QuotationBundle\Facade
 */
class QuotationFacade
{
    /** @var Logger */
    protected $logger;

    /** @var FormModelProviderInterface */
    protected $modelProvider;

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /** @var ProductDefinitionManager */
    protected $productManager;

    /** @var array */
    protected $qualifierDefaults;

    /**
     * @param Logger                     $logger
     * @param FormModelProviderInterface $modelProvider
     * @param EventDispatcherInterface   $eventDispatcher
     * @param ProductDefinitionManager   $productManager
     * @param array                      $qualifierDefaults
     */
    public function __construct(
        Logger $logger,
        FormModelProviderInterface $modelProvider,
        EventDispatcherInterface $eventDispatcher,
        ProductDefinitionManager $productManager,
        $qualifierDefaults
    ) {
        $this->logger = $logger;
        $this->modelProvider = $modelProvider;
        $this->eventDispatcher = $eventDispatcher;
        $this->productManager = $productManager;
        $this->qualifierDefaults = $qualifierDefaults;
    }

    /**
     * Get model provider instance.
     *
     * @return \Klaro\QuotationBundle\Api\FormModelProviderInterface
     */
    public function getModelProvider()
    {
        return $this->modelProvider;
    }

    /**
     * Get quotation based on id.
     *
     * @param $quotationId
     *
     * @return QuotationInterface
     */
    public function getQuotation($quotationId)
    {
        return $this->modelProvider->getQuotation($quotationId);
    }

    /**
     * Get list of quotations.
     *
     * @param QuotationQueryInterface $query
     *
     * @return ResultSetInterface
     *
     * @since 2.0.8
     */
    public function findQuotations(QuotationQueryInterface $query)
    {
        return $this->modelProvider->findQuotations($query);
    }

    /**
     * Get a revision for the given quotation.
     *
     * @param QuotationInterface $quotation
     * @param $revisionId
     *
     * @return QuotationRevisionInterface
     */
    public function getQuotationRevision(QuotationInterface $quotation, $revisionId)
    {
        return $this->modelProvider->getQuotationRevision($quotation, $revisionId);
    }

    /**
     * Get list of all quotation's revisions
     *
     * @param QuotationInterface $quotation
     *
     * @return QuotationRevisionInterface[]
     */
    public function getQuotationRevisions(QuotationInterface $quotation)
    {
        return $this->modelProvider->getQuotationRevisions($quotation);
    }

    /**
     * Persist quotation.
     *
     * @param QuotationInterface $quotation
     */
    public function persistQuotation(QuotationInterface $quotation)
    {
        $this->modelProvider->persistQuotation($quotation);
    }

    /**
     * Persist quotation revision.
     *
     * @param QuotationRevisionInterface $revision
     */
    public function persistQuotationRevision(QuotationRevisionInterface $revision)
    {
        $this->modelProvider->persistQuotationRevision($revision);
    }

    /**
     * Returns a finder class instance for the given entity name.
     *
     * @param $finderName
     *
     * @return FinderInterface
     */
    public function getItemFinder($finderName)
    {
        return $this->modelProvider->getItemFinder($finderName);
    }

    /**
     * Get the currently logged in user.
     *
     * @return QuotationUserInterface
     */
    public function getCurrentUser()
    {
        return $this->modelProvider->getCurrentUser();
    }

    /**
     * Get list of users.
     *
     * @param array $filters
     *
     * @return QuotationUserInterface[]
     */
    public function getUsers($filters = [])
    {
        return $this->modelProvider->getUsers($filters);
    }

    /**
     * Get list of users.
     *
     * @param UserQueryInterface $query
     *
     * @return mixed
     */
    public function findUsers(UserQueryInterface $query)
    {
        return $this->modelProvider->findUsers($query);
    }

    /**
     * Get a user by id.
     *
     * @param $userId
     *
     * @return QuotationUserInterface
     */
    public function getUser($userId)
    {
        return $this->modelProvider->getUser($userId);
    }

    /**
     * Creates a new quotation. The result is an instance of the created quotation's first revision.
     *
     * @param QuotationUserInterface $user          Owning user
     * @param string                 $quotationType Product line name
     *
     * @return QuotationRevisionInterface
     *
     * @throws \Exception
     */
    public function createQuotation(QuotationUserInterface $user, $quotationType)
    {
        $this->logInfo('Starting new proposal');

        $this->ensureQuotationTypeIsDefined($quotationType);

        $quotation = $this->modelProvider->createQuotation($quotationType);

        // Set basic quotation info.
        $quotation->setOwner($user);
        $quotation->setCreatedAt(new \DateTime());

        $this->modelProvider->persistQuotation($quotation);

        // Send event to allow modification.
        $event = new QuotationEvent($quotation, [
            'quotationType' => $quotationType,
        ]);

        $this->logInfo('Created new proposal', $event->toArray());

        $this->dispatchEvent(QuotationFacadeEvents::QUOTATION_CREATE, $event);

        // Get quotation again from event and save.
        $quotation = $event->getQuotation();

        $this->modelProvider->linkQuotation($quotation, $user);

        // Create a new revision for the new quotation.
        return $this->createQuotationRevision($quotation, $user);
    }

    /**
     * Link a quotation to a user which adds the given user as one of the quotation's handlers.
     *
     * @param QuotationInterface     $quotation
     * @param QuotationUserInterface $user
     */
    public function linkQuotation(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        // Send event to allow modification.
        $event = new QuotationUserEvent($quotation, $user);
        $this->dispatchEvent(QuotationFacadeEvents::QUOTATION_LINK, $event);

        $this->logInfo('Linking quotation', $event->toArray());

        $this->modelProvider->linkQuotation($quotation, $user);
    }

    /**
     * Set a user as the only handler for the quotation.
     *
     * @param QuotationInterface     $quotation
     * @param QuotationUserInterface $user
     */
    public function claimQuotation(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        $event = new QuotationUserEvent($quotation, $user);
        $this->dispatchEvent(QuotationFacadeEvents::QUOTATION_CLAIM, $event);

        $this->logger->info('Claim proposal', $event->toArray());

        $this->modelProvider->removeLinkedUsers($quotation);
        $this->modelProvider->linkQuotation($quotation, $user);
    }

    /**
     * Copies a quotation. Copies all revisions and related data except handlers.
     * The given user is set as the owner and the only handler for the new quotation.
     *
     * @param QuotationInterface     $quotation
     * @param QuotationUserInterface $user
     *
     * @return QuotationInterface
     */
    public function copyQuotation(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        // Remove revisions before removing the actual quotations.
        $newQuotation = $this->modelProvider->copyQuotation($quotation, $user);

        $event = new QuotationEvent($newQuotation, [
            'previous' => $quotation,
        ]);

        $this->dispatchEvent(QuotationFacadeEvents::QUOTATION_COPY, $event);

        $this->logger->info('Copy proposal', $event->toArray());

        return $event->getQuotation();
    }

    /**
     * Removes a quotation and all its revisions.
     *
     * @param QuotationInterface $quotation
     */
    public function removeQuotation(QuotationInterface $quotation)
    {
        $event = new QuotationEvent($quotation);
        $this->dispatchEvent(QuotationFacadeEvents::QUOTATION_REMOVE, $event);

        $this->logger->info('Delete proposal', $event->toArray());

        // Set latest revision id to NULL to prevent errors from foreign key relations when deleting revisions.
        $quotation->setLatestRevision(null);
        $this->modelProvider->persistQuotation($quotation);

        // Remove revisions before removing the actual quotations.
        $revisions = $this->getQuotationRevisions($quotation);

        foreach ($revisions as $revision) {
            $this->removeQuotationRevision($revision);
        }

        $this->modelProvider->removeQuotation($quotation);
    }

    /**
     * Remove a quotation revision.
     *
     * @param QuotationRevisionInterface $revision
     */
    public function removeQuotationRevision(QuotationRevisionInterface $revision)
    {
        $event = new QuotationRevisionEvent($revision);
        $this->dispatchEvent(QuotationFacadeEvents::REVISION_REMOVE, $event);

        $this->logInfo('Remove revision', $event->toArray());

        $this->modelProvider->removeQuotationRevision($revision);
    }

    /**
     * Create a new revision for the given quotation.
     *
     * @param QuotationInterface     $quotation     Quotation
     * @param QuotationUserInterface $user          Owner user of the revision
     * @param int|null               $oldRevisionId Previous revision, used to copy data to the new revision. If null,
     *                                              the latest revision of the given quotation is used.
     *
     * @return QuotationRevisionInterface
     */
    public function createQuotationRevision(QuotationInterface $quotation, QuotationUserInterface $user, $oldRevisionId = null)
    {
        $this->logInfo('Starting new revision');

        $quotationType = $quotation->getQuotationType();

        $this->ensureQuotationTypeIsDefined($quotationType);
        $this->ensureQuotationIsNotFinal($quotation);

        // Get old revision.
        if (!is_null($oldRevisionId)) {
            $oldRevision = $this->getQuotationRevision($quotation, $oldRevisionId);
        } else {
            $oldRevision = $quotation->getLatestRevision();
        }

        $revision = $this->modelProvider->createQuotationRevision($quotation);

        $revision->setCreatedAt(new \DateTime());
        $revision->setUser($user);

        $phaseData = [];

        $revision->setProductLine($this->productManager->getConfiguredProductLine($quotationType));
        $revision->setConfigurator($this->productManager->getConfigurator($quotationType));

        // Set defaults.
        if ($oldRevision instanceof QuotationRevisionInterface) {
            // Set qualifiers from old revision + the default values.
            $revision->setQualifiers($oldRevision->getQualifiers() + $this->qualifierDefaults);
            $revision->setStatus($oldRevision->getStatus());
            $revision->setOrderStatus($oldRevision->getOrderStatus());
            $revision->setTargetSalesPriceWithTax($oldRevision->getTargetSalesPriceWithTax());
            $revision->setMetaDatas($oldRevision->getMetaDatas());

            $phaseData = $oldRevision->getPhaseData();
        } else {
            // Set default qualifiers and status.
            $revision->setQualifiers($this->qualifierDefaults);
            $revision->setStatus(QuotationRevisionStatus::IN_PROCESS);
        }

        // Store phases, sales structure and product items here to save configuration state.
        $configuratorConfig = $this->getConfiguratorConfigForRevision(
            $revision,
            ConfiguratorManagerInterface::FETCH_FROM_SOURCE,
            false,
            ProductManagerInterface::VERSION_LATEST
        );
        $revision->setConfiguratorVersion($configuratorConfig->getVersion());

        $quotationType = $this->getProductLineForRevision(
            $revision,
            ProductManagerInterface::FETCH_FROM_SOURCE,
            false,
            ProductManagerInterface::VERSION_LATEST
        );
        $revision->setProductLineVersion($quotationType->getProductLineDefinition()->getVersion());

        $formDataTree = $this->getFormData($quotationType, $phaseData);

        $this->productManager->validateProductLine($formDataTree);
        $revision->setPhaseData($formDataTree->toArray());

        $revision->setLogMessage(null);
        $revision->setIsValid(false);

        // If quotation is won, set order status to in process.
        $revision->setOrderStatus(
            $revision->getStatus() == QuotationRevisionStatus::WON ? QuotationRevisionOrderStatus::IN_PROCESS : null
        );

        //$revision->setSalesStructure($this->productDefinitionManager->getDefaultSalesStructureConfig($productLine));
        //$revision->setProductItems($this->productDefinitionManager->getDefaultProductItemConfig($productLine));
        //$revision->setPhases($this->productDefinitionManager->getDefaultPhaseDefinitionConfigs($productLine));

        $this->modelProvider->persistQuotationRevision($revision);

        $quotation->setLatestRevision($revision);

        $this->modelProvider->persistQuotation($quotation);

        // Send event to allow modification
        $event = new QuotationRevisionEvent($revision, [
            'previous' => $oldRevision,
        ]);

        $this->logInfo('Created new revision', $event->toArray());

        $this->dispatchEvent(QuotationFacadeEvents::REVISION_CREATE, $event);

        return $event->getRevision();
    }

    /**
     * Get list of handlers for a quotation.
     *
     * @param QuotationInterface $quotation
     *
     * @return QuotationUserInterface[]
     */
    public function getLinkedUsers(QuotationInterface $quotation)
    {
        return $this->modelProvider->getLinkedUsers($quotation);
    }

    /**
     * @return array
     */
    public function getQuotationTypes()
    {
        return $this->productManager->getQuotationTypes();
    }

    /**
     * Get list of phases from a given revisions.
     *
     * @param $productLine
     * @param null        $version
     * @param string      $fetchMode
     *
     * @return ProductLineDefinition
     */
    public function getProductLineDefinition(
        $productLine,
        $version = ProductManagerInterface::VERSION_LATEST,
        $fetchMode = ProductManagerInterface::FETCH_CACHED
    ) {
        return $this->productManager->getProductLineDefinition($productLine, $version, $fetchMode);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return ProductLineDefinition
     */
    public function getProductLineDefinitionForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ProductManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        return $this->productManager->getProductLineDefinitionForRevision($revision, $fetchMode, $saveToRevision, $overrideVersion);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return ProductLine
     */
    public function getProductLineForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ProductManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        return $this->productManager->getProductLineForRevision($revision, $fetchMode, $saveToRevision, $overrideVersion);
    }

    /**
     * @param ProductLine $productLine
     * @param $phaseData
     *
     * @return ProductLineDataNode
     */
    public function getFormData(ProductLine $productLine, $phaseData)
    {
        return $this->productManager->getFormData($productLine, $phaseData);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return ProductLineDataNode
     */
    public function getFormDataForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ProductManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        return $this->productManager->getFormDataForRevision($revision, $fetchMode, $saveToRevision, $overrideVersion);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return ConfiguratorProductDefinitionNode
     */
    public function getConfiguratorStructureForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ConfiguratorManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        return $this->productManager->getConfiguratorStructureForRevision($revision, $fetchMode, $saveToRevision, $overrideVersion);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return \Klaro\Component\Configurator\Configuration\ConfigurationNode
     */
    public function getConfigurationForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ConfiguratorManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        return $this->productManager->getConfigurationForRevision($revision, $fetchMode, $saveToRevision, $overrideVersion);
    }

    /**
     * Convenience for logging.
     *
     * @param $msg
     * @param $context
     */
    private function logInfo($msg, $context = [])
    {
        $this->logger->info('QuotationFacade - '.$msg, $context);
    }

    /**
     * Helper for dispatching events.
     *
     * @param $name
     * @param Event $event
     */
    private function dispatchEvent($name, Event $event)
    {
        $this->eventDispatcher->dispatch($name, $event);
    }

    /**
     * Validator helper to see that given quotation type exists.
     *
     * @param $quotationType
     *
     * @throws \Exception
     */
    private function ensureQuotationTypeIsDefined($quotationType)
    {
        if (!$this->productManager->hasQuotationType($quotationType)) {
            throw new \Exception('Quotation type does not exist.');
        }
    }

    /**
     * Validator helper to see if quotation is finished (no more revisions can be created).
     *
     * @param QuotationInterface $quotation
     *
     * @throws \Exception
     */
    private function ensureQuotationIsNotFinal(QuotationInterface $quotation)
    {
        $latestRevision = $quotation->getLatestRevision();

        if ($latestRevision instanceof QuotationRevisionInterface) {
            if (RevisionStateMachine::create($latestRevision)->isFinal()) {
                throw new \Exception('Quotation is final, no more revisions can be made!');
            }
        }
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return \Klaro\Component\Configurator\Definition\ConfiguratorConfig
     */
    private function getConfiguratorConfigForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ProductManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        return $this->productManager->getConfiguratorConfigForRevision($revision, $fetchMode, $saveToRevision, $overrideVersion);
    }
}
