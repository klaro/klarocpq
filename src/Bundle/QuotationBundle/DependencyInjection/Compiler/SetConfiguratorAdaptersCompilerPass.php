<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SetConfiguratorAdaptersCompilerPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_quotation.configurator_adapter')) {
            return;
        }

        $definition = $container->getDefinition('klaro_quotation.configurator_adapter');

        $taggedServices = $container->findTaggedServiceIds('klaro_quotation.configurator_adapter');

        foreach ($taggedServices as $id => $attributes) {
            $alias = isset($attributes[0]['alias']) ? $attributes[0]['alias'] : null;

            if (empty($alias)) {
                throw new \Exception('Tried to set service "'.$id.'" to configurator adapter list but alias is not defined.');
            }

            $definition->addMethodCall('addAdapter', [$alias, $id]);
        }
    }
}
