<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Compiler pass to register quotation validators.
 *
 * Class SetQuotationValidatorCompilerPass
 * @package Klaro\QuotationBundle\DependencyInjection\Compiler
 */
class SetQuotationValidatorCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_quotation.validator_manager')) {
            return;
        }

        $definition = $container->getDefinition('klaro_quotation.validator_manager');

        foreach ($container->findTaggedServiceIds('klaro_quotation.validator') as $id => $attributes) {
            $alias = isset($attributes[0]['alias']) ? $attributes[0]['alias'] : null;

            if (empty($alias)) {
                throw new \Exception('Tried to set validator service "'.$id.'" but alias is not defined.');
            }

            $definition->addMethodCall('addValidator', [$alias, $container->getDefinition($id)]);
        }
    }
}
