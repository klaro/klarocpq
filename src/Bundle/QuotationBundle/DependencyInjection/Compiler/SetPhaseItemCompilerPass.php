<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Compiler pass to register available phase items.
 *
 * Class SetPhaseItemCompilerPass
 * @package Klaro\QuotationBundle\DependencyInjection\Compiler
 */
class SetPhaseItemCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_quotation.phase_item_factory')) {
            return;
        }

        $definition = $container->getDefinition('klaro_quotation.phase_item_factory');

        foreach ($container->findTaggedServiceIds('klaro_quotation.phase_item') as $id => $attributes) {
            foreach ($attributes as $attribute) {
                $alias = isset($attribute['alias']) ? $attribute['alias'] : null;

                if (empty($alias)) {
                    throw new \Exception('Tried to add service "'.$id.'" as a phase item but service alias is not defined.');
                }

                $definition->addMethodCall('addPhaseItem', [$alias, $id]);
            }
        }
    }
}
