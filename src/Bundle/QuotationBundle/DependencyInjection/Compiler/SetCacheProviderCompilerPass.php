<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Compiler pass to set the configured cache implementation as the cache.
 *
 * Class SetCacheProviderCompilerPass
 * @package Klaro\QuotationBundle\DependencyInjection\Compiler
 */
class SetCacheProviderCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasParameter('klaro_quotation.cache_provider')) {
            return;
        }

        $cacheProvider = $container->getParameter('klaro_quotation.cache_provider');

        if (!$container->hasDefinition($cacheProvider)) {
            throw new \Exception("Cache provider service '{$cacheProvider}' does not exists!");
        }

        // Set the cache provider as the cache implementation.
        $definition = $container->getDefinition('klaro_quotation.cache');

        $definition->setArguments([$container->getDefinition($cacheProvider)]);
    }
}
