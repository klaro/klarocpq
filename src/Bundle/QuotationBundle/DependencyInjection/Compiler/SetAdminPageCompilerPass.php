<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Compiler pass to collect admin pages that are tagged with name `klaro_quotation.admin_page_handler`.
 *
 * Class SetAdminPageCompilerPass
 * @package Klaro\QuotationBundle\DependencyInjection\Compiler
 */
class SetAdminPageCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_quotation.admin_page_manager')) {
            return;
        }

        $definition = $container->getDefinition('klaro_quotation.admin_page_manager');

        $taggedServices = $container->findTaggedServiceIds('klaro_quotation.admin_page_handler');

        foreach ($taggedServices as $id => $attributes) {
            $alias = isset($attributes[0]['alias']) ? $attributes[0]['alias'] : null;

            if (empty($alias)) {
                throw new \Exception('Tried to set service "'.$id.'" to admin page list but alias is not defined.');
            }

            $definition->addMethodCall('addAdminPage', [$alias, $id]);
        }
    }
}
