<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SetIdentifierGeneratorCompilerPass implements CompilerPassInterface
{

    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_quotation.identifier_generator')) {
            return;
        }

        $identifierGeneratorDefinition = $container->getDefinition('klaro_quotation.identifier_generator');

        if ($container->hasParameter('klaro_quotation.identifier_generator.quotation')) {
            $serviceId = $container->getParameter('klaro_quotation.identifier_generator.quotation');

            if (!$container->hasDefinition($serviceId)) {
                throw new \Exception(
                    sprintf(
                        'Tried to set service "%s" as quotation identifier generator but the service does not exist!',
                        $serviceId
                    )
                );
            }

            $identifierGeneratorDefinition->replaceArgument(0, $container->getDefinition($serviceId));
        }

        if ($container->hasParameter('klaro_quotation.identifier_generator.revision')) {
            $serviceId = $container->getParameter('klaro_quotation.identifier_generator.revision');

            if (!$container->hasDefinition($serviceId)) {
                throw new \Exception(
                    sprintf(
                        'Tried to set service "%s" as revision identifier generator but the service does not exist!',
                        $serviceId
                    )
                );
            }

            $identifierGeneratorDefinition->replaceArgument(1, $container->getDefinition($serviceId));
        }
    }
}
