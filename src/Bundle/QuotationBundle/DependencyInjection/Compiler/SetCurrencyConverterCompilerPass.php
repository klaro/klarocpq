<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SetCurrencyConverterCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_quotation.currency_converter')) {
            return;
        }

        $definition = $container->getDefinition('klaro_quotation.currency_converter');

        foreach ($container->findTaggedServiceIds('klaro_quotation.currency_converter') as $id => $attributes) {
            foreach ($attributes as $attribute) {
                $alias = isset($attribute['alias']) ? $attribute['alias'] : null;

                if (empty($alias)) {
                    throw new \Exception('Tried to set service "'.$id.'" to currency converter list but name is not defined.');
                }

                $definition->addMethodCall('addCurrencyConverter', [$alias, $container->getDefinition($id)]);
            }
        }
    }
}
