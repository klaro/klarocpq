<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SetPhaseVariablesCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_quotation.phase_variable_factory')) {
            return;
        }

        $definition = $container->getDefinition('klaro_quotation.phase_variable_factory');

        foreach ($container->findTaggedServiceIds('klaro_quotation.phase_variable') as $id => $attributes) {
            $alias = isset($attributes[0]['alias']) ? $attributes[0]['alias'] : null;

            $variableService = $container->getDefinition($id);

            if (!$variableService || empty($alias)) {
                throw new \Exception('Tried to set service "'.$id.'" to configuration but name is not defined.');
            }

            // Check that configuration service has "prototype scope".
            if ($variableService->isShared()) {
                throw new \Exception('Service "'.$id.'" is a shared service.
                Phase variables cannot be shared since they are generated separately for each revision." ');
            }

            if (!$variableService->isPublic()) {
                throw new \Exception('Service "'.$id.'" must to be public as variables are lazy-loaded.');
            }

            $definition->addMethodCall('addGlobalVariable', [$alias, $id]);
        }
    }
}
