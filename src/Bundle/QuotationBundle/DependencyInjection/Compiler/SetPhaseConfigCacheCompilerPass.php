<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Compiler pass to register phase config cache implementation.
 *
 * Class SetPhaseConfigCacheCompilerPass
 * @package Klaro\QuotationBundle\DependencyInjection\Compiler
 */
class SetPhaseConfigCacheCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        // Set the integration provider either to the default service or use the given one.
        $cacheProviderServiceId = $container->getParameter('klaro_quotation.phase_config_cache_provider');

        if (!is_null($cacheProviderServiceId)) {
            if (!$container->hasDefinition($cacheProviderServiceId)) {
                throw new \Exception('Phase config cache provider "'.$cacheProviderServiceId.'" does not exist.');
            }

            $container->setAlias('klaro_quotation.phase_config_cache', $cacheProviderServiceId);
        }
    }
}
