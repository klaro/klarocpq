<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SetProductDataImporterCompilerPass implements CompilerPassInterface
{

    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_quotation.product_importer')) {
            return;
        }

        if ($container->hasParameter('klaro_quotation.product_importer.custom')) {
            $customImporterService = $container->getParameter('klaro_quotation.product_importer.custom');

            if (!$container->hasDefinition($customImporterService)) {
                throw new \Exception('Custom importer service with id "'.$customImporterService.'" not found!');
            }

            $importerService = $container->getDefinition('klaro_quotation.product_importer');
            $importerService->addArgument($container->getDefinition($customImporterService));
        }
    }
}
