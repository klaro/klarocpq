<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Klaro\QuotationBundle\Validation\PhaseValidatorDefinition;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SetPhaseValidatorCompilerPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_quotation.phase_data_validator_manager')) {
            return;
        }

        $definition = $container->getDefinition('klaro_quotation.phase_data_validator_manager');

        foreach ($container->findTaggedServiceIds('klaro_quotation.phase_validator') as $id => $attributes) {
            $types = [
                PhaseValidatorDefinition::PRODUCT_LINE => [],
                PhaseValidatorDefinition::PRODUCT => [],
                PhaseValidatorDefinition::PHASE => [],
            ];

            foreach ($attributes as $attribute) {
                $type = $attribute['type'];

                $types[$type][] = $attribute['ref'];
            }

            if (!$container->hasDefinition($id)) {
                throw new \Exception('Tried to set service "'.$id.'" to configuration but the service is not found.');
            }

            $validator = $container->getDefinition($id);

            if (!$validator->isPublic()) {
                throw new \Exception('Service "'.$id.'" must to be public as validator services are lazy-loaded.');
            }

            $definition->addMethodCall(
                'addValidatorDefinition',
                [
                    $id,
                    $types[PhaseValidatorDefinition::PRODUCT_LINE],
                    $types[PhaseValidatorDefinition::PRODUCT],
                    $types[PhaseValidatorDefinition::PHASE],
                ]
            );
        }
    }
}
