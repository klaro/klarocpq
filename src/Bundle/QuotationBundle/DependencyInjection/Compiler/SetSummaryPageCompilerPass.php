<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Compiler pass to register summary page services.
 *
 * Class SetSummaryPageCompilerPass
 * @package Klaro\QuotationBundle\DependencyInjection\Compiler
 */
class SetSummaryPageCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritDoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_quotation.summary_page_manager')) {
            return;
        }

        $definition = $container->getDefinition('klaro_quotation.summary_page_manager');

        foreach ($container->findTaggedServiceIds('klaro_quotation.summary_page') as $id => $attributes) {
            $alias = isset($attributes[0]['alias']) ? $attributes[0]['alias'] : null;

            if (empty($alias)) {
                throw new \Exception('Tried to set service "'.$id.'" to summary page list but name is not defined.');
            }

            $definition->addMethodCall('addSummaryService', [$alias, $id]);
        }
    }
}
