<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection;

use Klaro\Component\ProductPhase\PhaseNode;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class KlaroQuotationExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        // Set basic parameters.
        $container->setParameter('klaro_quotation.application_title', $config['application_title']);
        $container->setParameter('klaro_quotation.application_id', $config['application_id']);
        $container->setParameter('klaro_quotation.quotation_types', $config['quotation_types']);

        // Set product line and phase related parameters.
        $quotationDocuments  = [];
        $quotationSummaries  = [];
        $quotationTypes = [];
        $qualifierNames = [];
        $qualifierOptions = [];
        $qualifierDefaults = [];

        // Process sidebar types.
        $sidebarTypes = isset($config['sidebar']) ? $config['sidebar'] : null;

        $defaultSummary = [
            'title'    => 'Offering Summary',
            'phases'   => [],
            'template' => 'KlaroQuotationBundle:Quotation:sidebar.html.twig',
        ];

        if (!isset($sidebarTypes['summary'])) {
            $sidebarTypes['summary'] = $defaultSummary;
        } else {
            $sidebarTypes['summary'] = $sidebarTypes['summary'] + $defaultSummary;
        }

        // Process summary types.
        $summaryPages = isset($config['summary']) ? $config['summary'] : [];

        $summaryPages = $summaryPages + [
            'default' => [
                'title'    => 'Offering Summary',
            ],
        ];

        foreach ($config['quotation_types'] as $quotationType => $quotationTypeDefinition) {
            // Set product line names in separate parameter.
            $quotationTypes[$quotationType] = $quotationTypeDefinition['title'];

            // Set documents available to each product line.
            $documents = [];

            if (isset($quotationTypeDefinition['documents'])) {
                foreach ($quotationTypeDefinition['documents'] as $documentId => $documentData) {
                    $documents[$documentId] = $documentData;
                }
            }

            $summaries = [];

            if (!isset($quotationTypeDefinition['summaries']) || count($quotationTypeDefinition['summaries']) == 0) {
                $quotationTypeDefinition['summaries'] = ['default'];
            }

            // Copy summary definitions for each designated summary page.
            $quotationTypeDefinition['summaries'] = array_unique($quotationTypeDefinition['summaries']);

            foreach ($quotationTypeDefinition['summaries'] as $summaryPageId) {
                if (isset($summaryPages[$summaryPageId])) {
                    $summaries[$summaryPageId] = $summaryPages[$summaryPageId];
                } else {
                    throw new \Exception('Summary page "'.$summaryPageId.'" not found under klaro_quotation.summary!');
                }
            }

            $quotationDocuments[$quotationType]  = $documents;
            $quotationSummaries[$quotationType]  = $summaries;
        }

        // Process qualifiers.
        if (isset($config['revision']['qualifiers'])) {
            foreach ($config['revision']['qualifiers'] as $qualifier => $qualifierData) {
                $qualifierNames[$qualifier]    = $qualifierData['title'];
                $qualifierOptions[$qualifier]  = $qualifierData['options'];

                if (isset($qualifierData['default']) && isset($qualifierData['options'][$qualifierData['default']])) {
                    $qualifierDefaults[$qualifier] = $qualifierData['default'];
                } else {
                    reset($qualifierData['options']);
                    $qualifierDefaults[$qualifier] = key($qualifierData['options']);
                }
            }
        }

        $container->setParameter('klaro_quotation.quotation_type_names', $quotationTypes);
        $container->setParameter('klaro_quotation.quotation_type_documents', $quotationDocuments);
        $container->setParameter('klaro_quotation.quotation_type_summaries', $quotationSummaries);
        $container->setParameter('klaro_quotation.sidebar', $sidebarTypes);
        $container->setParameter('klaro_quotation.summary', $summaryPages);
        $container->setParameter('klaro_quotation.revision_qualifiers', $qualifierNames);
        $container->setParameter('klaro_quotation.revision_qualifier_options', $qualifierOptions);
        $container->setParameter('klaro_quotation.revision_qualifier_defaults', $qualifierDefaults);

        // Set product manager
        $container->setAlias('klaro_quotation.product_manager', $config['product_manager']);

        // Set configurator
        $container->setAlias('klaro_quotation.configurator_manager', $config['configurator_manager']);

        // Product data importer
        if (isset($config['product_data_importer'])) {
            $container->setParameter('klaro_quotation.product_importer.custom', $config['product_data_importer']);
        }

        // Set identifier generator
        if (isset($config['identifier_generator'])) {
            if (isset($config['identifier_generator']['quotation'])) {
                $container->setParameter('klaro_quotation.identifier_generator.quotation', $config['identifier_generator']['quotation']);
            }

            if (isset($config['identifier_generator']['revision'])) {
                $container->setParameter('klaro_quotation.identifier_generator.revision', $config['identifier_generator']['revision']);
            }
        }

        // Resolve cache provider.
        if (empty($config['cache'])) {
            // Use array provider by default (in-memory cache).
            $config['cache'] = 'array';
        }

        $cacheProvider = $config['cache'];

        // If the provider string contains '.', assume it is a full service definition.
        // Otherwise prepend with quotation cache service prefix.
        if (strpos($cacheProvider, '.') === false) {
            $cacheProvider = 'klaro_quotation.cache.'.$cacheProvider;
        }

        $container->setParameter('klaro_quotation.cache_provider', $cacheProvider);

        // Set phase config cache provider.
        $container->setParameter('klaro_quotation.phase_config_cache_provider', $config['phase_config_cache_provider']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
