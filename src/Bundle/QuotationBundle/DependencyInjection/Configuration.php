<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('klaro_quotation');

        $this->addRevisionSection($rootNode);
        $this->addDocumentsSection($rootNode);
        $this->addSidebarSection($rootNode);
        $this->addSummarySection($rootNode);
        $this->addQuotationTypesSection($rootNode);

        $rootNode
            ->ignoreExtraKeys()
            ->children()
                ->scalarNode('application_title')->defaultValue('CPQ Configurator')->end()
                ->scalarNode('application_id')->isRequired()->end()
                ->scalarNode('phase_config_cache_provider')->defaultNull()->end()
                ->scalarNode('cache')->defaultValue('array')->end()
                ->scalarNode('product_manager')->isRequired()->end()
                ->scalarNode('configurator_manager')->isRequired()->end()
                ->scalarNode('product_data_importer')->end()
                ->arrayNode('identifier_generator')
                    ->children()
                        ->scalarNode('quotation')->end()
                        ->scalarNode('revision')->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addQuotationTypesSection(ArrayNodeDefinition $node)
    {
        $node
            ->fixXmlConfig('quotation_type')
            ->children()
                ->arrayNode('quotation_types')
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->prototype('array')
                        ->children()
                            ->scalarNode('title')->isRequired()->end()
                            ->scalarNode('product_line')->isRequired()->end()
                            ->scalarNode('configuration')->isRequired()->end()
                            ->scalarNode('adapter')->isRequired()->end()
                            ->scalarNode('currency_converter')->end()
                            ->arrayNode('documents')
                                ->useAttributeAsKey('document')
                                ->prototype('array')
                                ->children()
                                    ->scalarNode('type')->isRequired()->end()
                                    ->variableNode('title')
                                        ->validate()
                                        ->always(function ($v) {
                                            if (is_string($v) || is_array($v) && isset($v['expression'])) {
                                                return $v;
                                            }
                                            throw new InvalidTypeException('Title should be a string or an expression.');
                                        })
                                        ->end()
                                    ->end()
                                    ->variableNode('source')
                                        ->defaultNull()
                                        ->validate()
                                            ->always(function ($v) {
                                                if (is_string($v) || is_array($v) && (isset($v['expression']) || isset($v['items']) && is_array($v['items']))) {
                                                    return $v;
                                                }
                                                throw new InvalidTypeException('Source must be a string, expression or an object with a list of items.');
                                            })
                                        ->end()
                                    ->end()
                                    ->variableNode('locale')
                                        ->defaultNull()
                                        ->validate()
                                            ->ifTrue(function ($v) {
                                                return !is_string($v) && !is_array($v);
                                            })
                                            ->thenInvalid('Locale must be either string or array.')
                                        ->end()
                                    ->end()
                                    ->variableNode('disabled')
                                        ->defaultFalse()
                                        ->validate()
                                            ->always(function ($v) {
                                                if (is_bool($v) || is_array($v) && isset($v['expression'])) {
                                                    return $v;
                                                }
                                                throw new InvalidTypeException('Disabled condition must either be a boolean value or an expression object.');
                                            })
                                        ->end()
                                    ->end()
                                    ->variableNode('visible')
                                        ->defaultTrue()
                                        ->validate()
                                            ->always(function ($v) {
                                                if (is_bool($v) || is_array($v) && isset($v['expression'])) {
                                                    return $v;
                                                }
                                                throw new InvalidTypeException('Visibility condition must either be a boolean value or an expression object.');
                                            })
                                        ->end()
                                    ->end()
                                ->end()
                                ->end()
                            ->end()
                            ->arrayNode('summaries')
                                ->prototype('scalar')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addSidebarSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('sidebar')
                    ->useAttributeAsKey('sidebar')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('title')->isRequired()->end()
                            ->scalarNode('template')->end()
                            ->arrayNode('phases')
                                ->prototype('scalar')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addSummarySection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('summary')
                    ->useAttributeAsKey('summary')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('type')->defaultValue('default')->end()
                            ->scalarNode('title')->isRequired()->end()
                            ->scalarNode('template')->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addRevisionSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->arrayNode('revision')
                    ->children()
                        ->arrayNode('qualifiers')
                        ->useAttributeAsKey('qualifier')
                        ->prototype('array')
                            ->children()
                                ->scalarNode('title')->isRequired()->end()
                                ->scalarNode('default')->end()
                                ->arrayNode('options')
                                ->useAttributeAsKey('option')
                                ->isRequired()
                                    ->prototype('scalar')->end()
                                ->end()
                            ->end()
                        ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addDocumentsSection(ArrayNodeDefinition $node)
    {
        $node
            ->fixXmlConfig('document')
            ->children()
                ->arrayNode('documents')
                    ->useAttributeAsKey('document')
                    ->prototype('array')
                    ->children()
                        ->scalarNode('type')->isRequired()->end()
                        ->variableNode('title')
                            ->validate()
                            ->always(function ($v) {
                                if (is_string($v) || is_array($v) && isset($v['text'])) {
                                    return $v;
                                }
                                throw new InvalidTypeException();
                            })
                            ->end()
                        ->end()
                        ->variableNode('source')->defaultNull()->end()
                        ->scalarNode('condition')->defaultNull()->end()
                    ->end()
                ->end()
            ->end();
    }
}
