<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Profiler;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestMatcherInterface;

/**
 * Permission matcher for the request toolbar. Used in production mode to show the Symfony developer toolbar
 * if the user has the privileges to see it.
 *
 * Class ProfilerPermissionMatcher
 * @package Klaro\QuotationBundle\Profiler
 */
class ProfilerPermissionMatcher implements RequestMatcherInterface
{
    /** @var AuthorizationCheckerInterface  */
    protected $authorizationChecker;

    /**
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function matches(Request $request)
    {
        return $this->authorizationChecker->isGranted('ROLE_KLARO_QUOTATION_PROFILER');
    }
}
