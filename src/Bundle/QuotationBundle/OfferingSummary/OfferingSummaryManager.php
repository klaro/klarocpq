<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\OfferingSummary;

use Klaro\QuotationBundle\Api\FormModelProviderInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Library\ExpressionEvaluatorFactory;
use Klaro\QuotationBundle\Library\ProductDefinitionManager;
use Klaro\QuotationBundle\Traits\EventDispatcherTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Klaro\Component\Cache\CacheInterface;

/**
 * Class for generating a sales structure which is a hierarchical representation of the product configuration.
 *
 * Class OfferingSummaryManager
 * @package Klaro\QuotationBundle\OfferingSummary
 *
 * @see OfferingSummaryTree
 */
class OfferingSummaryManager
{
    use EventDispatcherTrait;

    /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface */
    protected $dispatcher;

    /** @var ExpressionEvaluatorFactory */
    protected $evaluatorFactory;

    /** @var \Klaro\QuotationBundle\Api\FormModelProviderInterface */
    protected $modelProvider;

    /** @var StateMachineManager */
    protected $stateMachineManager;

    /** @var ProductDefinitionManager */
    protected $productDefinitionManager;

    /** @var CacheInterface */
    protected $cache;

    /** @var OfferingSummaryVisitorInterface[] */
    protected $visitors;

    /**
     * @param EventDispatcherInterface   $dispatcher
     * @param ExpressionEvaluatorFactory $evaluator
     * @param FormModelProviderInterface $modelProvider
     * @param StateMachineManager        $stateMachineManager
     * @param ProductDefinitionManager   $productDefinitionManager
     * @param CacheInterface             $cache
     * @param array                      $visitors
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        ExpressionEvaluatorFactory $evaluatorFactory,
        FormModelProviderInterface $modelProvider,
        StateMachineManager $stateMachineManager,
        ProductDefinitionManager $productDefinitionManager,
        CacheInterface $cache,
        array $visitors = []
    ) {
        $this->dispatcher = $dispatcher;
        $this->evaluatorFactory = $evaluatorFactory;
        $this->modelProvider = $modelProvider;
        $this->stateMachineManager = $stateMachineManager;
        $this->productDefinitionManager = $productDefinitionManager;
        $this->cache = $cache;
        $this->visitors = $visitors;
    }

    /**
     * Evaluate an expression in the given context.
     *
     * @param QuotationRevisionInterface $revision   The revision in whose context the expression is evaluated.
     * @param string                     $expression Evaluated expression.
     * @param array                      $context    Context variables.
     *
     * @return null|string
     */
    public function evaluate(QuotationRevisionInterface $revision, $expression, $context)
    {
        $evaluator = $this->evaluatorFactory->getEvaluator($revision);

        return $evaluator->evaluateExpression($expression, $context);
    }

    /**
     * Add a visitor to apply to created structures.
     *
     * @param OfferingSummaryVisitorInterface $visitor
     *
     * @return OfferingSummaryVisitorInterface
     */
    public function addVisitor(OfferingSummaryVisitorInterface $visitor)
    {
        $this->visitors[] = $visitor;

        return $this;
    }

    /**
     * Returns the list of defined visitors.
     *
     * @return OfferingSummaryVisitorInterface[]
     */
    public function getVisitors()
    {
        return $this->visitors;
    }

    /**
     * Returns the visitor with the given name.
     *
     * @param $name
     *
     * @return null|OfferingSummaryVisitorInterface
     */
    public function getVisitor($name)
    {
        $namedVisitor = null;

        foreach ($this->visitors as $visitor) {
            if ($visitor->getName() === $name) {
                $namedVisitor = $visitor;

                break;
            }
        }

        return $namedVisitor;
    }

    /**
     * Get collected data from all visitors.
     *
     * @return array
     */
    public function getVisitorData()
    {
        $data = [];

        foreach ($this->visitors as $visitor) {
            $data[$visitor->getName()] = $visitor->getData();
        }

        return $data;
    }

    /**
     * Generates a sales structure hierarchy for the given revision. The structure is only rebuilt if the
     * revision is not valid (something changed in the editor) and it is the latest revision in the quotation,
     * or if the summary has not been been previously built.
     *
     * @param QuotationRevisionInterface $revision
     * @param bool                       $refresh
     *
     * @return OfferingSummaryTree
     *
     * @throws \Exception
     */
    public function generate(QuotationRevisionInterface $revision, $refresh = false)
    {
        $quotation = $revision->getQuotation();

        // Create the root node if not created already, or a refresh is forced.
        $summary = $this->getCachedSummary($revision);

        if (!($summary instanceof OfferingSummaryTree) || $refresh === true) {
            $exportedSummary = $revision->getOfferingSummary();

            // If this revision is the latest one, generate it again.
            if (empty($exportedSummary) || $revision->getRevisionId() == $quotation->getLatestRevision()->getRevisionId() && $revision->isValid() != true) {
                // Get structure for this product line and create the sections.
                // See if the structure is provided in the event data.
                $productLineDefinition = $this->productDefinitionManager->getProductLineDefinitionForRevision($revision);
                $structure = $productLineDefinition->getStructure();

                if (!is_array($structure)) {
                    throw new \Exception('Structure is not set!');
                }

                $summary = new OfferingSummaryTree($this, $revision);

                // Send event so listeners can add visitors or do some processing.
                $event = new OfferingSummaryEvent($revision, $summary);

                $this->dispatchEvent(OfferingSummaryEvents::SUMMARY_STRUCTURE_CREATE, $event);

                // Send again for processing.
                $event->setSummary($summary);

                // Apply visitors.
                $summary->build($structure);

                $this->dispatchEvent(OfferingSummaryEvents::SUMMARY_STRUCTURE_READY, $event);

                $summary = $event->getSummary();

                $revision->setOfferingSummary($summary->export());
                $revision->setIsValid(true);
                $revision->setCurrency($summary->getCurrency());

                $this->modelProvider->persistQuotationRevision($revision);
            } else {
                // Otherwise, use the cached structure.
                $summary = new OfferingSummaryTree($this, $revision);
                $summary->setCurrency($revision->getCurrency());
                $summary->import($exportedSummary);
            }

            $summary = $this->finalize($revision, $summary);
            $this->saveCachedSummary($revision, $summary);
        }

        return $summary;
    }

    /**
     * Get cached summary for the given revision.
     *
     * @param QuotationRevisionInterface $revision
     *
     * @return array|false
     */
    private function getCachedSummary(QuotationRevisionInterface $revision)
    {
        return $this->cache->fetch($this->getCacheKey($revision));
    }

    /**
     * Save summary to cache.
     *
     * @param QuotationRevisionInterface $revision
     * @param $summary
     */
    private function saveCachedSummary(QuotationRevisionInterface $revision, $summary)
    {
        $this->cache->save($this->getCacheKey($revision), $summary);
    }

    /**
     * Convenience for generating cache key.
     *
     * @param QuotationRevisionInterface $revision
     *
     * @return string
     */
    private function getCacheKey(QuotationRevisionInterface $revision)
    {
        return 'summary://'.$revision->getQuotation()->getId().'-'.$revision->getRevisionId();
    }

    /**
     * Helper to finalize the summary tree after it has been built, mainly to apply the user given
     * sales price.
     *
     * @param QuotationRevisionInterface $revision
     * @param OfferingSummaryTree        $summary
     *
     * @return OfferingSummaryTree
     */
    private function finalize(QuotationRevisionInterface $revision, OfferingSummaryTree $summary)
    {
        $salesPrice = $summary->getTotalSalesPriceWithTax();

        // If the quotation is in order phase, the price can be set by hand.
        $stateMachine = $this->stateMachineManager->getRevisionStateMachine($revision);

        if ($stateMachine->isOrder()) {
            $targetPrice = $revision->getTargetSalesPriceWithTax();

            // Adjust prices to target price if it is set and differs from the actual sales price.
            if (is_numeric($targetPrice) && $targetPrice > 0 && (int) ($targetPrice - $salesPrice) != 0) {
                $discountFactor = $targetPrice / $salesPrice;

                // Total price values are cached so reset the value fields to get discounted values.
                $summary->reset('SalesPrice');
                $summary->addFactor('SalesPrice', $discountFactor, 'Target Price Adjustment');
            }
        }

        // Save calculated values to revision.
        $revision->setCalculatedCostPrice($summary->getTotalCostPrice());
        $revision->setCalculatedSalesPriceWithTax($salesPrice);
        $this->modelProvider->persistQuotationRevision($revision);

        return $summary;
    }
}
