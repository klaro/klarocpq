<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\Component\Configurator\Definition\ConfiguratorProductDefinitionNode;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

class ConfiguratorDefinitionEvent extends QuotationRevisionEvent
{
    /** @var ConfiguratorProductDefinitionNode */
    protected $structure;

    /**
     * ConfiguratorDefinitionEvent constructor.
     * @param QuotationRevisionInterface|null   $revision
     * @param ConfiguratorProductDefinitionNode $structure
     * @param array                             $data
     */
    public function __construct(QuotationRevisionInterface $revision = null, ConfiguratorProductDefinitionNode $structure, $data = [])
    {
        parent::__construct($revision, $data);

        $this->structure = $structure;
    }

    /**
     * @return ConfiguratorProductDefinitionNode
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @param ConfiguratorProductDefinitionNode $structure
     */
    public function setStructure(ConfiguratorProductDefinitionNode $structure)
    {
        $this->structure = $structure;
    }
}
