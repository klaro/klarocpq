<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\FormData\ProductLineDataNode;

class PhaseEvent extends QuotationRevisionEvent
{
    /** @var ProductLineDataNode */
    protected $formData;

    /**
     * @param QuotationRevisionInterface $revision
     * @param ProductLineDataNode        $formData
     * @param array                      $data
     */
    public function __construct(QuotationRevisionInterface $revision, ProductLineDataNode $formData, $data = [])
    {
        $this->formData = $formData;

        parent::__construct($revision, $data);
    }

    /**
     * @param ProductLineDataNode $formData
     */
    public function setPhase(ProductLineDataNode $formData)
    {
        $this->formData = $formData;
    }

    /**
     * @return ProductLineDataNode
     */
    public function getFormData()
    {
        return $this->formData;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'formData' => $this->formData,
        ] + parent::toArray();
    }
}
