<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\Component\Common\Model\FormModelInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

class FormModelEvent extends QuotationRevisionEvent
{
    /** @var FormModelInterface */
    protected $model;

    /**
     * @param QuotationRevisionInterface $revision
     * @param $model
     * @param array                      $data
     */
    public function __construct(QuotationRevisionInterface $revision, FormModelInterface $model = null, $data = [])
    {
        $this->model = $model;

        parent::__construct($revision, $data);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['model' => $this->model] + parent::toArray();
    }

    /**
     * @param FormModelInterface $model
     */
    public function setModel(FormModelInterface $model)
    {
        $this->model = $model;
    }

    /**
     * @return FormModelInterface
     */
    public function getModel()
    {
        return $this->model;
    }
}
