<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\Component\Configurator\Definition\ConfiguratorConfig;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

class ConfiguratorConfigEvent extends QuotationRevisionEvent
{
    /** @var ConfiguratorConfig */
    protected $config;

    /**
     * ConfiguratorConfigEvent constructor.
     * @param ConfiguratorConfig              $config
     * @param QuotationRevisionInterface|null $revision
     * @param array                           $data
     */
    public function __construct(ConfiguratorConfig $config, QuotationRevisionInterface $revision = null, array $data = [])
    {
        parent::__construct($revision, $data);

        $this->config = $config;
    }

    /**
     * @return ConfiguratorConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param ConfiguratorConfig $config
     */
    public function setConfig(ConfiguratorConfig $config)
    {
        $this->config = $config;
    }
}
