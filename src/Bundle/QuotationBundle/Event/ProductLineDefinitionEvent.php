<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\ProductPhase\ProductLineDefinition;

class ProductLineDefinitionEvent extends QuotationRevisionEvent
{
    /** @var ProductLineDefinition */
    protected $productLineDefinition;

    /**
     * ProductLineDefinitionEvent constructor.
     * @param ProductLineDefinition           $productLineDefinition
     * @param QuotationRevisionInterface|null $revision
     * @param array                           $data
     */
    public function __construct(ProductLineDefinition $productLineDefinition, QuotationRevisionInterface $revision = null, array $data = [])
    {
        parent::__construct($revision, $data);

        $this->productLineDefinition = $productLineDefinition;
    }

    /**
     * @return ProductLineDefinition
     */
    public function getProductLineDefinition()
    {
        return $this->productLineDefinition;
    }

    /**
     * @param ProductLineDefinition $productLineDefinition
     */
    public function setProductLineDefinition($productLineDefinition)
    {
        $this->productLineDefinition = $productLineDefinition;
    }
}
