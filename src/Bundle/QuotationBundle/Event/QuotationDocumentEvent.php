<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\QuotationBundle\Api\DocumentBuilderInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

class QuotationDocumentEvent extends QuotationRevisionEvent
{
    /** @var DocumentBuilderInterface */
    protected $builder;

    /**
     * @param DocumentBuilderInterface $builder
     * @param string                   $section
     */
    public function __construct(QuotationRevisionInterface $revision, DocumentBuilderInterface $builder, $data = [])
    {
        $this->builder = $builder;

        parent::__construct($revision, $data);
    }

    /**
     * @param \Klaro\QuotationBundle\Api\DocumentBuilderInterface $generator
     */
    public function setBuilder(DocumentBuilderInterface $generator)
    {
        $this->builder = $generator;
    }

    /**
     * @return \Klaro\QuotationBundle\Api\DocumentBuilderInterface
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['builder' => $this->builder] + parent::toArray();
    }
}
