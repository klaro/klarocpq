<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\Component\Common\Model\FinderInterface;

class FormItemFinderEvent extends ResponseEvent
{
    /** @var FinderInterface */
    protected $finder;

    /**
     * @param FinderInterface|null $finder
     * @param array                $data
     */
    public function __construct(FinderInterface $finder = null, $data = [])
    {
        $this->finder = $finder;

        parent::__construct($data);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['finder' => $this->finder] + parent::toArray();
    }

    /**
     * @param FinderInterface $finder
     */
    public function setFinder(FinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @return FinderInterface
     */
    public function getFinder()
    {
        return $this->finder;
    }
}
