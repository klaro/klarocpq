<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

class ConfiguratorEvent extends QuotationRevisionEvent
{
    /** @var ConfigurationNode */
    protected $configuration;

    /**
     * ConfiguratorEvent constructor.
     * @param ConfigurationNode               $configuration
     * @param QuotationRevisionInterface|null $revision
     * @param array                           $data
     */
    public function __construct(ConfigurationNode $configuration, QuotationRevisionInterface $revision = null, array $data = [])
    {
        parent::__construct($revision, $data);

        $this->configuration = $configuration;
    }

    /**
     * @return ConfigurationNode
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param ConfigurationNode $configuration
     */
    public function setConfiguration(ConfigurationNode $configuration)
    {
        $this->configuration = $configuration;
    }
}
