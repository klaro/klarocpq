<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\QuotationBundle\Api\QuotationManagerInterface;

class QuotationManagerEvent extends ResponseEvent
{
    /** @var QuotationManagerInterface */
    protected $manager;

    /**
     * @param QuotationManagerInterface|null $manager
     * @param array                          $data
     */
    public function __construct(QuotationManagerInterface $manager = null, $data = [])
    {
        $this->manager = null;

        parent::__construct($data);
    }

    /**
     * @param QuotationManagerInterface $manager
     */
    public function setManager(QuotationManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return QuotationManagerInterface
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['manager' => $this->manager] + parent::toArray();
    }
}
