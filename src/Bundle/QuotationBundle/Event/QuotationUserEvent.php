<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;

class QuotationUserEvent extends QuotationEvent
{
    /** @var QuotationUserInterface $user */
    protected $user;

    /**
     * @param QuotationInterface     $quotation
     * @param QuotationUserInterface $menuItem
     * @param array                  $data
     */
    public function __construct(QuotationInterface $quotation, QuotationUserInterface $user = null, $data = [])
    {
        $this->user = $user;

        parent::__construct($quotation, $data);
    }

    /**
     * @param QuotationUserInterface $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return QuotationUserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['user' => $this->user] + parent::toArray();
    }
}
