<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\Component\Common\Model\QuotationInterface;

class QuotationEvent extends ResponseEvent
{
    /** @var QuotationInterface */
    protected $quotation;

    /**
     * @param QuotationInterface $quotation
     * @param array              $data
     */
    public function __construct(QuotationInterface $quotation = null, $data = [])
    {
        $this->quotation = $quotation;

        parent::__construct($data);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['quotation' => $this->quotation] + parent::toArray();
    }

    /**
     * @param QuotationInterface $quotation
     */
    public function setQuotation(QuotationInterface $quotation)
    {
        $this->quotation = $quotation;
    }

    /**
     * @return QuotationInterface
     */
    public function getQuotation()
    {
        return $this->quotation;
    }
}
