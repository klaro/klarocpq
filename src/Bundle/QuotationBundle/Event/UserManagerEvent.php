<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\QuotationBundle\Api\UserManagerInterface;

class UserManagerEvent extends ResponseEvent
{
    /** @var UserManagerInterface */
    protected $manager;

    /**
     * @param UserManagerInterface|null $manager
     * @param array                     $data
     */
    public function __construct(UserManagerInterface $manager = null, $data = [])
    {
        $this->manager = null;

        parent::__construct($data);
    }

    /**
     * @param UserManagerInterface $manager
     */
    public function setManager(UserManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return UserManagerInterface
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['manager' => $this->manager] + parent::toArray();
    }
}
