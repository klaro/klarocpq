<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

class QuotationRevisionEvent extends QuotationEvent
{
    /** @var QuotationRevisionInterface */
    protected $revision;

    /**
     * @param QuotationInterface         $quotation
     * @param QuotationRevisionInterface $revision
     * @param array                      $data
     */
    public function __construct(QuotationRevisionInterface $revision = null, $data = [])
    {
        $this->revision = $revision;

        $quotation = $revision ? $revision->getQuotation() : null;

        parent::__construct($quotation, $data);
    }

    /**
     * @param QuotationRevisionInterface $revision
     */
    public function setRevision(QuotationRevisionInterface $revision)
    {
        $this->revision = $revision;
    }

    /**
     * @return QuotationRevisionInterface
     */
    public function getRevision()
    {
        return $this->revision;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['revision' => $this->revision] + parent::toArray();
    }
}
