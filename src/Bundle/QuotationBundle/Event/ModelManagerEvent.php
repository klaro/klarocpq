<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Event;

use Klaro\QuotationBundle\Api\ModelManagerInterface;

class ModelManagerEvent extends ResponseEvent
{
    /** @var ModelManagerInterface */
    protected $manager;

    /**
     * @param ModelManagerInterface|null $manager
     * @param array                      $data
     */
    public function __construct(ModelManagerInterface $manager = null, $data = [])
    {
        $this->manager = null;

        parent::__construct($data);
    }

    /**
     * @param ModelManagerInterface $manager
     */
    public function setManager(ModelManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @return ModelManagerInterface
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return ['manager' => $this->manager] + parent::toArray();
    }
}
