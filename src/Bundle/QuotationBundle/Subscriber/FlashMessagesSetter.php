<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Subscriber;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\QuotationBundle\Event\QuotationEvent;
use Klaro\Component\Common\Event\QuotationControllerEvents;
use Klaro\QuotationBundle\Event\QuotationUserEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Event listener which logs events to the session flash message parameter bag.
 * These are then shown on the next page the user enters.
 *
 * Class FlashMessagesSetter
 * @package Klaro\QuotationBundle\Subscriber
 */
class FlashMessagesSetter implements EventSubscriberInterface
{
    /** @var Session */
    protected $session;

    /** @var TranslatorInterface */
    protected $translator;

    /**
     * @param Session             $session
     * @param TranslatorInterface $translator
     */
    public function __construct(
        Session $session,
        TranslatorInterface $translator
    ) {
        $this->session    = $session;
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            QuotationControllerEvents::QUOTATION_CREATE => 'onQuotationCreate',
            QuotationControllerEvents::QUOTATION_LINK   => 'onQuotationLink',
            QuotationControllerEvents::QUOTATION_CLAIM  => 'onQuotationClaim',
            QuotationControllerEvents::QUOTATION_COPY   => 'onQuotationCopy',
            QuotationControllerEvents::QUOTATION_REMOVE => 'onQuotationRemove',
        );
    }

    /**
     * Set flash message after creating a new quotation.
     *
     * @param QuotationEvent $event
     */
    public function onQuotationCreate(QuotationEvent $event)
    {
        $quotation = $event->getQuotation();

        $message = $this->translator->trans(
            'general.flash_messages.create',
            [
                '%title%' => $quotation ? $quotation->getIdentifier() : '',
            ],
            'KlaroQuotationBundle'
        );

        $this->setFlashMessage($message);
    }

    /**
     * Set flash message after linking a quotation.
     *
     * @param QuotationEvent $event
     */
    public function onQuotationLink(QuotationUserEvent $event)
    {
        $user = $event->getUser();
        $quotation = $event->getQuotation();

        $message = $this->translator->trans(
            'general.flash_messages.link',
            [
                '%username%' => $user ? $user->getFullname() : '',
                '%title%'    => $quotation ? $quotation->getIdentifier() : '',
            ],
            'KlaroQuotationBundle'
        );

        $this->setFlashMessage($message);
    }

    /**
     * Set flash message after claiming a quotation.
     *
     * @param QuotationEvent $event
     */
    public function onQuotationClaim(QuotationUserEvent $event)
    {
        $user = $event->getUser();
        $quotation = $event->getQuotation();

        $message = $this->translator->trans(
            'general.flash_messages.claim',
            [
                '%username%' => $user ? $user->getFullname() : '',
                '%title%'    => $quotation ? $quotation->getIdentifier() : '',
            ],
            'KlaroQuotationBundle'
        );

        $this->setFlashMessage($message);
    }

    /**
     * Set flash message after copying a quotation.
     *
     * @param QuotationEvent $event
     */
    public function onQuotationCopy(QuotationUserEvent $event)
    {
        $user = $event->getUser();

        $original  = $event->getData('quotation');
        $quotation = $event->getQuotation();

        $message = $this->translator->trans(
            'general.flash_messages.copy',
            [
                '%username%'  => $user ? $user->getFullname() : '',
                '%fromTitle%' => $original ? $original->getIdentifier() : '',
                '%toTitle%'   => $quotation ? $quotation->getIdentifier() : '',
            ],
            'KlaroQuotationBundle'
        );

        $this->setFlashMessage($message);
    }

    /**
     * Set flash message after removing a quotation.
     *
     * @param QuotationEvent $event
     */
    public function onQuotationRemove(QuotationEvent $event)
    {
        $quotation = $event->getQuotation();

        $message = $this->translator->trans(
            'general.flash_messages.delete',
            [
                '%title%' => $quotation ? $quotation->getIdentifier() : '',
            ],
            'KlaroQuotationBundle'
        );

        $this->setFlashMessage($message);
    }

    /**
     * Sets notification (session flash) message.
     *
     * @param $message
     */
    private function setFlashMessage($message)
    {
        if ($this->session) {
            $this->session->getFlashBag()->add('notice', $message);
        }
    }
}
