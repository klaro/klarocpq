<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Subscriber;

use Klaro\Component\Common\Event\SummaryPageEvents;
use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\ConfiguratorManagerInterface;
use Klaro\Component\ProductManager\ProductManagerInterface;
use Klaro\QuotationBundle\Api\FormModelProviderInterface;
use Klaro\Component\Common\Event\ProductDefinitionManagerEvents;
use Klaro\Component\Common\Event\QuotationControllerEvents;
use Klaro\QuotationBundle\CurrencyConverter\CurrencyConverter;
use Klaro\QuotationBundle\CurrencyConverter\CurrencyConverterInterface;
use Klaro\QuotationBundle\Document\DocumentDefinition;
use Klaro\QuotationBundle\Event\ConfiguratorConfigEvent;
use Klaro\QuotationBundle\Event\ConfiguratorDefinitionEvent;
use Klaro\QuotationBundle\Event\ConfiguratorEvent;
use Klaro\QuotationBundle\Event\ProductLineDefinitionEvent;
use Klaro\QuotationBundle\Event\QuotationEvent;
use Klaro\Component\Common\Event\QuotationFacadeEvents;
use Klaro\QuotationBundle\Event\QuotationRevisionEvent;
use Klaro\QuotationBundle\IdentifierGenerator\IdentifierGenerator;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Klaro\QuotationBundle\Twig\ConfiguratorExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Event subscriber which handles updating the timestamps for quotations and revisions.
 *
 * Class QuotationEventSubscriber
 * @package Klaro\QuotationBundle\Subscriber
 */
class QuotationEventSubscriber implements EventSubscriberInterface
{
    /** @var CurrencyConverter */
    protected $currencyConverter;

    /** @var FormModelProviderInterface */
    protected $modelProvider;

    /** @var IdentifierGenerator */
    protected $identifierGenerator;

    protected $jsonSummaryTree;

    /** @var ConfiguratorExtension */
    protected $configuratorExtension;

    /** @var AuthorizationChecker */
    protected $auth;

    protected $translator;

    /**
     * @param FormModelProviderInterface $modelProvider
     * @param IdentifierGenerator        $identifierGenerator
     */
    public function __construct(
        CurrencyConverter $currencyConverter,
        FormModelProviderInterface $modelProvider,
        IdentifierGenerator $identifierGenerator,
        ConfiguratorExtension $configuratorExtension,
        AuthorizationChecker $auth,
        TranslatorInterface $translator
    ) {
        $this->currencyConverter = $currencyConverter;
        $this->modelProvider = $modelProvider;
        $this->identifierGenerator = $identifierGenerator;
        $this->configuratorExtension = $configuratorExtension;
        $this->auth = $auth;
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            QuotationControllerEvents::QUOTATION_EDIT_SAVE => 'onQuotationSave',
            QuotationFacadeEvents::QUOTATION_CREATE => 'onQuotationCreate',
            QuotationFacadeEvents::REVISION_CREATE => 'onRevisionCreate',
            QuotationFacadeEvents::QUOTATION_COPY => 'onQuotationCopy',
            ProductDefinitionManagerEvents::PRODUCT_LINE_DEFINITION => 'onProductLineDefinition',
            ProductDefinitionManagerEvents::CONFIGURATOR_CONFIG => 'onConfiguratorConfig',
            ProductDefinitionManagerEvents::CONFIGURATION => 'onConfiguration',
            ProductDefinitionManagerEvents::CONFIGURATION_FINALIZE => 'onConfigurationFinalize',
            QuotationControllerEvents::QUOTATION_SUMMARY => 'onSummary',
            QuotationControllerEvents::QUOTATION_SIDEBAR => 'onSummary',
        );
    }

    /**
     * Updates the updatedAt time whenever phase is changed.
     *
     * @param QuotationRevisionEvent $event
     */
    public function onQuotationSave(QuotationRevisionEvent $event)
    {
        $revision  = $event->getRevision();
        $revision->setUpdatedAt(new \DateTime());
        $revision->setIsValid(false);
        $this->modelProvider->persistQuotationRevision($revision);

        $quotation = $revision->getQuotation();
        $quotation->setUpdatedAt(new \DateTime());
        $this->modelProvider->persistQuotation($quotation);
    }

    /**
     * @param QuotationEvent $event
     */
    public function onQuotationCreate(QuotationEvent $event)
    {
        $quotation = $event->getQuotation();

        // Create identifier for this quotation (do it after first save to get access to id)
        $quotation->setIdentifier($this->identifierGenerator->generateQuotationIdentifier($quotation));
        $this->modelProvider->persistQuotation($quotation);
    }

    /**
     * @param QuotationRevisionEvent $event
     */
    public function onRevisionCreate(QuotationRevisionEvent $event)
    {
        $revision = $event->getRevision();

        $revision->setIdentifier($this->identifierGenerator->generateRevisionIdentifier($revision));
        $this->modelProvider->persistQuotationRevision($revision);
    }

    /**
     * Updates the times on a newly copied quotation.
     * @param QuotationEvent $event
     */
    public function onQuotationCopy(QuotationEvent $event)
    {
        $quotation = $event->getQuotation();

        $quotation->setCreatedAt(new \DateTime());
        $quotation->setUpdatedAt(null);
        $quotation->setIdentifier($this->identifierGenerator->generateQuotationIdentifier($quotation));

        // Set identifier to copied revisions
        $revisions = $this->modelProvider->getQuotationRevisions($quotation);

        if (count($revisions) > 0) {
            foreach ($revisions as $revision) {
                $revision->setIdentifier($this->identifierGenerator->generateRevisionIdentifier($revision));
                $this->modelProvider->persistQuotationRevision($revision);
            }
        }

        $this->modelProvider->persistQuotation($quotation);
    }

    /**
     * @param ProductLineDefinitionEvent $event
     */
    public function onProductLineDefinition(ProductLineDefinitionEvent $event)
    {
        $revision = $event->getRevision();
        $definition = $event->getProductLineDefinition();
        $fetchMode = $event->getData('fetchMode');
        $saveToRevision = $event->getData('saveToRevision');

        // If revision was forced to reload from the file, save it back to the revision.
        if ($fetchMode === ProductManagerInterface::FETCH_FROM_SOURCE && $saveToRevision === true) {
            $revision->setProductLineVersion($definition->getVersion());
            $this->modelProvider->persistQuotationRevision($revision);
        }
    }

    /**
     * @param ConfiguratorConfigEvent $event
     */
    public function onConfiguratorConfig(ConfiguratorConfigEvent $event)
    {
        $revision = $event->getRevision();
        $config = $event->getConfig();
        $fetchMode = $event->getData('fetchMode');
        $saveToRevision = $event->getData('saveToRevision');

        // If revision was forced to reload from the file, save it back to the revision.
        if ($fetchMode === ConfiguratorManagerInterface::FETCH_FROM_SOURCE && $saveToRevision === true) {
            $revision->setConfiguratorVersion($config->getVersion());
            $this->modelProvider->persistQuotationRevision($revision);
        }
    }

    /**
     * @param ConfiguratorEvent $event
     */
    public function onConfiguration(ConfiguratorEvent $event)
    {
        $revision = $event->getRevision();
        $configuration = $event->getConfiguration();

        $revision->setOfferingSummary($configuration->export());
        $revision->setIsValid(true);
        $this->modelProvider->persistQuotationRevision($revision);
    }

    /**
     * @param ConfiguratorEvent $event
     */
    public function onConfigurationFinalize(ConfiguratorEvent $event)
    {
        $revision = $event->getRevision();
        $configuration = $event->getConfiguration();

        $salesPrice = $configuration->getTotalSalesPriceWithTax();

        // If the quotation is in order phase, the price can be set by hand.
        $stateMachine = RevisionStateMachine::create($revision);

        if ($stateMachine->isOrder()) {
            $targetPrice = $revision->getTargetSalesPriceWithTax();

            // Backwards compatibility for older revisions
            $currency = $revision->getCurrency();
            if ($currency && $currency !== 'EUR') {
                $currencyConverter = $this->currencyConverter->getCurrencyConverterForRevision($revision);
                if ($currencyConverter) {
                    $targetPrice = $currencyConverter->fromCurrency($targetPrice);
                }
            }

            // Adjust prices to target price if it is set and differs from the actual sales price.
            if (is_numeric($targetPrice) && $targetPrice > 0 && (int) ($targetPrice - $salesPrice) != 0) {
                $discountFactor = $targetPrice / $salesPrice;

                // Total price values are cached so reset the value fields to get discounted values.
                $configuration->reset('SalesPrice');
                $configuration->addFactor('SalesPrice', $discountFactor, 'Target Price Adjustment', true);
            }
        }

        // Save calculated values to revision.
        $revision->setCalculatedCostPrice($configuration->getTotalCostPrice());
        $revision->setCalculatedSalesPriceWithTax($salesPrice);
        $this->modelProvider->persistQuotationRevision($revision);
    }

    /**
     * @param QuotationRevisionEvent $event
     */
    public function onSummary(QuotationRevisionEvent $event)
    {
        if (!$event->getData('request')->query->has('json')) {
            return $event;
        }

        $result = [];
        if ($event->getData('configuration')) {
            // from sidebar
            $conf = $event->getData('configuration');
            $cc = $event->getData('currencyConverter');
            $showTopLevel = false;
        } else {
            // from summary
            $conf = $event->getData('summaryPage')->getData()['configuration'];
            $cc = $event->getData('summaryPage')->getData()['currencyConverter'];
            $showTopLevel = true;

            $docs = $event->getData('outputDocuments');
            foreach ($docs as $doc) {
                /** @var DocumentDefinition $doc */
                if (!$doc->isVisible()) {
                    continue;
                }
                $result['outputDocuments'][$doc->getId()] = [
                    'title' => $doc->getTitle(),
                    'disabled' => $doc->isDisabled(),
                ];
            }
        }
        /** @var ConfigurationNode $conf */
        foreach ($conf->getScopes() as $scope) {
            foreach ($conf->getSections() as $section) {
                if ($scope === 'default' || $section->hasItemRecursive($scope)) {
                    $result['data'][$scope] = $this->jsonConfiguration($conf, $cc, $scope, 0, $showTopLevel);
                }
            }
        }

        if ($additionalData = $event->getData('additional')) {
            foreach ($additionalData as $key => $data) {
                $result['data'][$key] = $data;
            }
        }

        $currencyConverter = $this->currencyConverter->getCurrencyConverterForRevision($event->getRevision());
        $totals = new \stdClass();
        if (RevisionStateMachine::create($event->getRevision())->isOrder()) {
            $title = $this->translator->trans('summary.totals.final_price_with_tax', [], 'KlaroQuotationBundle');
            $totals->{$title} = sprintf("%s %s", number_format(round($currencyConverter->toCurrency($conf->getTotalSalesPriceWithTax()), 2), 2), $currencyConverter->getCurrency());
        }
        if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_TOTAL_SALES_PRICE')) {
            $title = $this->translator->trans('summary.totals.sales_price_with_tax', [], 'KlaroQuotationBundle');
            $totals->{$title} = sprintf("%s %s", number_format(round($currencyConverter->toCurrency($conf->getTotalSalesPriceWithTax()), 2), 2), $currencyConverter->getCurrency());
            $taxes = $conf->getSalesTaxTotals();
            foreach ($taxes as $percentage => $tax) {
                $title = $this->translator->trans('summary.totals.sales_tax', [], 'KlaroQuotationBundle');
                $totals->{$title}[] = sprintf("%s %s %s", number_format(round($currencyConverter->toCurrency($tax), 2), 2), $currencyConverter->getCurrency(), $percentage);
            }
        }
        if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_TOTAL_COST_PRICE')) {
            $title = $this->translator->trans('summary.totals.cost_price', [], 'KlaroQuotationBundle');
            $totals->{$title} = sprintf("%s %s", number_format(round($currencyConverter->toCurrency($conf->getTotalCostPrice()), 2), 2), $currencyConverter->getCurrency());
        }
        if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_TOTAL_PROFIT')) {
            $title = $this->translator->trans('summary.totals.profit', [], 'KlaroQuotationBundle');
            $totals->{$title} = sprintf("%s %s", number_format(round($currencyConverter->toCurrency($conf->getTotalProfit()), 2), 2), $currencyConverter->getCurrency());
        }
        if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_TOTAL_PROFIT_MARGIN')) {
            $title = $this->translator->trans('summary.totals.profit_margin', [], 'KlaroQuotationBundle');
            $totals->{$title} = $conf->getTotalSalesPrice() > 0 ?
                sprintf("%s %%", number_format(round($conf->getTotalProfitMargin(), 2), 2)) : '-';
        }
        foreach ($conf->getScopes() as $scope) {
            if ($scope !== 'default' && $conf->hasItemRecursive($scope)) {
                if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_TOTAL_SALES_PRICE')) {
                    $title = $this->translator->trans(sprintf('summary.totals.%s_final_price_with_tax', $scope), [], 'KlaroQuotationBundle');
                    $totals->{$title} = sprintf("%s %s", number_format(round($currencyConverter->toCurrency($conf->getTotalSalesPriceWithTax($scope)), 2), 2), $currencyConverter->getCurrency());
                }
                if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_TOTAL_PROFIT_MARGIN')) {
                    $title = $this->translator->trans(sprintf('summary.totals.%s_profit_margin', $scope), [], 'KlaroQuotationBundle');
                    $totals->{$title} = $conf->getTotalSalesPrice($scope) > 0 ?
                        sprintf("%s %%", number_format(round($conf->getTotalProfitMargin($scope), 2), 2)) : '-';
                }
            }
        }
        $result['data']['totals'] = $totals;

        if (count($event->getQuotation()->getEntity()->getRevisions()) > 1) {
            $result['data']['compareTool'] = new \stdClass();
        }

        $event->setResponse(JsonResponse::create($result));

        return $event;
    }

    public function jsonConfiguration(ConfigurationNode $conf, ?CurrencyConverterInterface $cc, $scope, $level = 0, $showTopLevel = true)
    {
        if ($scope !== 'default' && !$conf->hasItemRecursive($scope)) {
            return;
        }

        $result = [];
        if ($showTopLevel) {
            if (!$conf->hasItem()) {
                if (($conf->getLevel() === 0 and $scope === 'default') || $conf->hasItemRecursive($scope)) {
                    $section = [
                        'title' => $conf->getTitle(),
                        'level' => $level,
                        'errorCount' => $conf->getErrorCount(),
                    ];
                    if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_COST_PRICE')) {
                        $section['costPrice'] = $cc->toCurrency($conf->getTotalCostPrice($scope));
                    }
                    if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_SALES_PRICE')) {
                        $section['salesPrice'] = $cc->toCurrency($conf->getTotalSalesPriceWithTax($scope));
                    }
                    foreach ($conf->getErrors() as $error) {
                        $section['errors'][] = [
                            'title' => $error->getTitle(),
                            'description' => $error->getDescription(),
                            'context' => $error->getContext(),
                        ];
                    }

                    $this->jsonSummaryTree[$scope][] = $section;
                    $result = $section;
                }
            } elseif ($conf->hasItem() && $conf->isInScope($conf->getItem()->getScope(), $scope)) {
                $item = [
                    'title' => $conf->getTitle(),
                    'level' => $level,
                    'name' => $conf->getItem()->getInternalTitle(),
                    'quantity' => $conf->getItem()->getQuantity(),
                    'errorCount' => $conf->getErrorCount(),
                ];
                if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_ITEM_CODE')) {
                    $item['code'] = $conf->getItem()->getItemCode();
                }
                if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_COST_PRICE')) {
                    $item['itemCostPrice'] = $cc->toCurrency($conf->getItem()->getCostPrice());
                    $item['costPrice'] = $cc->toCurrency($conf->getTotalCostPrice($scope));
                    if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_FACTORS')) {
                        $item['costFactor'] = $this->configuratorExtension->renderPriceFactor($conf->getItem(), $cc, 'CostPrice');
                    }
                }
                if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_SALES_PRICE')) {
                    $item['itemSalesPrice'] = $cc->toCurrency($conf->getItem()->getSalesPriceWithTax());
                    $item['salesPrice'] = $cc->toCurrency($conf->getTotalSalesPriceWithTax($scope));
                    if ($this->auth->isGranted('ROLE_KLARO_QUOTATION_ACCESS_FACTORS')) {
                        $item['salesFactor'] = $this->configuratorExtension->renderPriceFactor($conf->getItem(), $cc, 'SalesPrice');
                    }
                }
                foreach ($conf->getErrors() as $error) {
                    $item['errors'][] = [
                        'title' => $error->getTitle(),
                        'description' => $error->getDescription(),
                        'context' => $error->getContext(),
                    ];
                }

                $this->jsonSummaryTree[$scope][] = $item;
                $result = $item;
            }
        }

        foreach ($conf->getSections() as $section) {
            $child = $this->jsonConfiguration($section, $cc, $scope, $level + 1);
            if ($child) {
                $result['children'][] = $child;
            }
        }

        return $result;
    }
}
