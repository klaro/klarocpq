<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Subscriber;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Exception event lister replaces default exception layout with a custom one.
 *
 * Class ExceptionListener
 * @package Klaro\QuotationBundle\Subscriber
 */
class ExceptionListener
{
    /** @var EngineInterface */
    protected $templating;

    /** @var KernelInterface */
    protected $kernel;

    /**
     * @param EngineInterface $templating
     * @param $kernel
     */
    public function __construct(EngineInterface $templating, KernelInterface $kernel)
    {
        $this->templating = $templating;
        $this->kernel = $kernel;
    }

    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ('prod' == $this->kernel->getEnvironment()) {
            // Exception object
            $exception = $event->getException();

            // Set response content.
            $response = new Response();

            $response->setContent(
                $this->templating->render('KlaroQuotationBundle:Exception:exception.html.twig', array('exception' => $exception))
            );

            // HttpExceptionInterface is a special type of exception that holds status code and header details.
            if ($exception instanceof HttpExceptionInterface) {
                $response->setStatusCode($exception->getStatusCode());
                $response->headers->replace($exception->getHeaders());
            } else {
                $response->setStatusCode(500);
            }

            // Set the new $response object to the $event
            $event->setResponse($response);
        }
    }
}
