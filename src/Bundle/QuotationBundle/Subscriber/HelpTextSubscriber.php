<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Subscriber;

use Doctrine\ORM\EntityManager;
use Klaro\Component\Common\Event\QuotationControllerEvents;
use Klaro\QuotationBundle\Event\QuotationRevisionEvent;
use Klaro\QuotationExtraBundle\Model\HelpText;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class HelpTextSubscriber implements EventSubscriberInterface
{
    /** @var EntityManager */
    protected $em;

    /** @var Request */
    protected $request;

    /** @var AuthorizationCheckerInterface */
    protected $authorizationChecker;

    public function __construct(EntityManager $em, RequestStack $requestStack, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->em = $em;
        $this->request = $requestStack->getCurrentRequest();
        $this->authorizationChecker = $authorizationChecker;
    }

    public static function getSubscribedEvents()
    {
        return [
            QuotationControllerEvents::QUOTATION_EDIT => 'onEdit',
            QuotationControllerEvents::QUOTATION_EDIT_SAVE => 'onEdit',
        ];
    }

    public function onEdit(QuotationRevisionEvent $event)
    {
        if (!$this->request || !$this->request->query->has('json')) {
            return;
        }

        $values = [];

        $application = $event->getQuotation()->getQuotationType();
        $phaseId     = self::parseActualPhaseId($this->request->get('phaseId', ''));

        /** @var HelpText[] $texts */
        $texts = $this->em->getRepository('Klaro\\Component\\Common\\Model\\HelpTextInterface')
            ->findHelpTexts($application, $phaseId);

        foreach ($texts as $text) {
            $values[$text->getFieldname()] = $text->getDescription();
        }

        $event->addData('helpTexts', [
            'values' => $values,
            'editable' => $this->authorizationChecker->isGranted('ROLE_KLAROCPQ_ADMIN'),
        ]);
    }

    /**
     * Strip dynamic part from phaseId when there is text/number/actualPhaseId
     * lines/N/actualPhaseName -> actualPhaseName
     * actualPhaseName2 -> actualPhaseName2
     * @param string $phaseId
     * @return string
     */
    public static function parseActualPhaseId(string $phaseId=""): string
    {
        $res = preg_match("/[a-z]*\/\d*\/(.*)/i", $phaseId, $matches);
        if ($res && isset($matches[1])) {
            $phaseId = $matches[1];
        }

        return $phaseId;
    }
}
