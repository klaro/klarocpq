<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\DataCollector;

use Klaro\Component\DataCollector\DataCollector;

/**
 * Data collector for showing quotation bundle data in the Symfony web toolbar.
 *
 * Class QuotationDataCollector
 * @package Klaro\QuotationBundle\DataCollector
 */
class QuotationDataCollector extends DataCollector
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'klaro_quotation';
    }

    public function reset()
    {
        $this->queries = [];
        $this->data = [
            'nb_queries' => 0,
            'queries' => [],
        ];
    }
}
