<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\IdentifierGenerator;

use Klaro\Component\Common\Model\QuotationRevisionInterface;

interface RevisionIdentifierGeneratorInterface
{
    /**
     * @param QuotationRevisionInterface $revision
     *
     * @return string
     */
    public function generateRevisionIdentifier(QuotationRevisionInterface $revision);
}
