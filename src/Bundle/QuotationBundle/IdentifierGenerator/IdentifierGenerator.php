<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\IdentifierGenerator;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

class IdentifierGenerator implements QuotationIdentifierGeneratorInterface, RevisionIdentifierGeneratorInterface
{
    /** @var QuotationIdentifierGeneratorInterface */
    protected $quotationIdentifierGenerator;

    /** @var RevisionIdentifierGeneratorInterface */
    protected $revisionIdentifierGenerator;

    /**
     * IdentifierGenerator constructor.
     * @param QuotationIdentifierGeneratorInterface $quotationIdentifierGenerator
     * @param RevisionIdentifierGeneratorInterface  $revisionIdentifierGenerator
     */
    public function __construct(
        QuotationIdentifierGeneratorInterface $quotationIdentifierGenerator = null,
        RevisionIdentifierGeneratorInterface $revisionIdentifierGenerator = null
    ) {
        $this->quotationIdentifierGenerator = $quotationIdentifierGenerator;
        $this->revisionIdentifierGenerator = $revisionIdentifierGenerator;
    }

    /**
     * @return QuotationIdentifierGeneratorInterface
     */
    public function getQuotationIdentifierGenerator()
    {
        if (!($this->quotationIdentifierGenerator instanceof QuotationIdentifierGeneratorInterface)) {
            $this->quotationIdentifierGenerator = new DefaultIdentifierGenerator();
        }

        return $this->quotationIdentifierGenerator;
    }

    /**
     * @param QuotationIdentifierGeneratorInterface $quotationIdentifierGenerator
     */
    public function setQuotationIdentifierGenerator(QuotationIdentifierGeneratorInterface $quotationIdentifierGenerator)
    {
        $this->quotationIdentifierGenerator = $quotationIdentifierGenerator;
    }

    /**
     * @return RevisionIdentifierGeneratorInterface
     */
    public function getRevisionIdentifierGenerator()
    {
        if (!($this->revisionIdentifierGenerator instanceof RevisionIdentifierGeneratorInterface)) {
            $this->revisionIdentifierGenerator = new DefaultIdentifierGenerator();
        }

        return $this->revisionIdentifierGenerator;
    }

    /**
     * @param RevisionIdentifierGeneratorInterface $revisionIdentifierGenerator
     */
    public function setRevisionIdentifierGenerator(RevisionIdentifierGeneratorInterface $revisionIdentifierGenerator)
    {
        $this->revisionIdentifierGenerator = $revisionIdentifierGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function generateQuotationIdentifier(QuotationInterface $quotation)
    {
        return $this->getQuotationIdentifierGenerator()->generateQuotationIdentifier($quotation);
    }

    /**
     * {@inheritdoc}
     */
    public function generateRevisionIdentifier(QuotationRevisionInterface $revision)
    {
        return $this->getRevisionIdentifierGenerator()->generateRevisionIdentifier($revision);
    }
}
