<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\IdentifierGenerator;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

class DefaultIdentifierGenerator implements QuotationIdentifierGeneratorInterface, RevisionIdentifierGeneratorInterface
{
    /**
     * {@inheritdoc}
     */
    public function generateQuotationIdentifier(QuotationInterface $quotation)
    {
        return 'Q'.$quotation->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function generateRevisionIdentifier(QuotationRevisionInterface $revision)
    {
        return $revision->getQuotation()->getIdentifier().'R'.$revision->getRevisionId();
    }
}
