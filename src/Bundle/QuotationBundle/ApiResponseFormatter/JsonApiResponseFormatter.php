<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\ApiResponseFormatter;

use Klaro\QuotationBundle\Api\ApiResponseFormatterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Implementation for ApiResponseFormatterInterface that formats the response as JSON.
 *
 * @package Klaro\QuotationBundle\ApiResponseFormatter
 */
class JsonApiResponseFormatter implements ApiResponseFormatterInterface
{
    /**
     * {@inheritDoc}
     */
    public function getSuccessResponse($data, $status = 200, $meta = [])
    {
        $payload = [
            'data' => $data,
        ] + $meta;

        return JsonResponse::create($payload, $status);
    }

    /**
     * {@inheritDoc}
     */
    public function getErrorResponse($message, $code = 0, $status = 500, $meta = [])
    {
        $payload = [
            'error' => [
                'code'    => $code,
                'message' => $message,
            ],
        ] + $meta;

        return JsonResponse::create($payload, $status);
    }
}
