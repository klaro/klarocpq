<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Traits;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\ProductPhase\PhaseNode;
use Klaro\Component\ProductPhase\SinglePhase;
use Klaro\Component\Util\ArrayUtil;
use Klaro\QuotationBundle\Api\QuotationManagerInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Collection of methods for controller validation. Assumes that methods from QuotationServiceTraits are available.
 *
 * Class QuotationValidationTraits
 * @package Klaro\QuotationBundle\Traits
 */
trait QuotationValidationTraits
{
    /**
     * @param $quotationType
     *
     * @throws \Exception
     */
    private function ensureQuotationTypeIsDefined($quotationType)
    {
        // Check that quotation type exists
        if (!$this->getProductDefinitionManager()->hasQuotationType($quotationType)) {
            throw new \Exception('Product line does not exist or access was denied.');
        }
    }

    /**
     * @param QuotationInterface     $quotation
     * @param QuotationUserInterface $user
     *
     * @throws \Exception
     */
    private function ensureUserHasAccessToQuotation(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        /** @var QuotationManagerInterface $manager */
        $manager = $this->getModelProvider()->getQuotationManager();

        $userId = $user->getId();

        // User may have admin access to view all quoations.
        $isOwned = $this->get('security.authorization_checker')->isGranted('ROLE_KLARO_QUOTATION_EDIT_ALL_QUOTATIONS');

        // Otherwise, check ownership.
        if (!$isOwned) {
            $isOwned = ArrayUtil::inArray($userId, $manager->getLinkedUsers($quotation), function ($userId, $user) {
                return $user && $userId === $user->getId();
            });
        }

        if ($isOwned !== true) {
            throw new AccessDeniedException('Access denied.');
        }
    }

    /**
     * @param QuotationInterface     $quotation
     * @param QuotationUserInterface $user
     *
     * @throws \Exception
     */
    private function ensureQuotationIsNotOwnedByUser(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        /** @var QuotationmanagerInterface $manager */
        $manager = $this->getModelProvider()->getQuotationManager();

        $userId = $user->getId();

        $isOwned = ArrayUtil::inArray($userId, $manager->getLinkedUsers($quotation), function ($userId, $user) {
            return $user && $userId === $user->getId();
        });

        if ($isOwned === true) {
            throw new AccessDeniedException('Access denied.');
        }
    }

    /**
     * @param $role
     *
     * @throws \Exception
     */
    private function ensureUserHasRole($role)
    {
        if (!$this->get('security.authorization_checker')->isGranted($role)) {
            throw new AccessDeniedException('Access denied.');
        }
    }

    private function ensurePhaseAccessibility(PhaseNode $phaseNode)
    {
        if (!$phaseNode instanceof SinglePhase) {
            return;
        }

        if (!$phaseNode->getPhaseDefinition()->getDisabled()) {
            return;
        }

        throw new AccessDeniedException('Access denied');
    }
}
