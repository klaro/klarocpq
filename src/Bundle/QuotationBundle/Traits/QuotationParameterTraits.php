<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Traits;

/**
 * Collection of traits to help accessing bundle parameters. The traits assume that the service container
 * is accessible through an attribute called `container`.
 *
 * Class QuotationParameterTraits
 * @package Klaro\QuotationBundle\Traits
 */
trait QuotationParameterTraits
{
    /**
     * @return array
     */
    protected function getProductLineDocuments($quotationType)
    {
        $documents = $this->container->getParameter('klaro_quotation.quotation_type_documents');

        return $documents[$quotationType];
    }

    /**
     * @param $quotationType
     *
     * @return array
     */
    protected function getPhaseSummaries($quotationType)
    {
        $summaries = $this->container->getParameter('klaro_quotation.quotation_type_summaries');

        return $summaries[$quotationType];
    }

    /**
     * @return array
     */
    protected function getSidebars()
    {
        return $this->container->getParameter('klaro_quotation.sidebar');
    }

    /**
     * @param $phasePath
     *
     * @return array
     */
    protected function getSidebarForPhase($phasePath)
    {
        $sections = $this->getSidebars();

        $allowed = [];

        foreach ($sections as $id => $page) {
            $addPage = empty($page['phases']);

            if (!$addPage) {
                foreach ($page['phases'] as $testExpression) {
                    if (preg_match('/'.$testExpression.'/', $phasePath)) {
                        $addPage = true;
                        break;
                    }
                }
            }

            if ($addPage) {
                $allowed[$id] = $page;
            }
        }

        return $allowed;
    }

    /**
     * @return array
     */
    protected function getRevisionTypes()
    {
        return $this->container->getParameter('klaro_quotation.revision_type');
    }

    /**
     * @return array
     */
    protected function getRevisionQualifiers()
    {
        return $this->container->getParameter('klaro_quotation.revision_qualifiers');
    }

    /**
     * @return array
     */
    protected function getRevisionQualifierOptions()
    {
        return $this->container->getParameter('klaro_quotation.revision_qualifier_options');
    }

    /**
     * @return array
     */
    protected function getQuotationTypeNames()
    {
        return $this->container->getParameter('klaro_quotation.quotation_type_names');
    }

    /**
     * @return array
     */
    protected function getQuotationTypes()
    {
        return $this->container->getParameter('klaro_quotation.quotation_types');
    }
}
