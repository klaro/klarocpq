<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Traits;

/**
 * Trait used in events for convenience.
 *
 * Class EventDataTrait
 * @package Klaro\QuotationBundle\Traits
 */
trait EventDataTrait
{
    /** @var array $data */
    protected $data;

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param $name
     * @param $data
     */
    public function addData($name, $data)
    {
        $this->data[$name] = $data;
    }

    /**
     * @return array|mixed
     */
    public function getData($namedItem = null)
    {
        if ($namedItem !== null) {
            return isset($this->data[$namedItem]) ? $this->data[$namedItem] : null;
        }

        return $this->data;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }
}
