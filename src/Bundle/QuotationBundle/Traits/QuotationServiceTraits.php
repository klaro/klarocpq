<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Traits;

use Klaro\Component\Configurator\ConfiguratorManagerInterface;
use Klaro\Component\ProductManager\ProductManagerInterface;
use Klaro\QuotationBundle\Api\ApiResponseFormatterInterface;
use Klaro\QuotationBundle\Api\DocumentBuilderInterface;
use Klaro\QuotationBundle\Configurator\Configurator;
use Klaro\QuotationBundle\CurrencyConverter\CurrencyConverter;
use Klaro\QuotationBundle\Document\OutputDocumentManager;
use Klaro\QuotationBundle\Facade\QuotationFacade;
use Klaro\QuotationBundle\AdminPage\AdminPageManager;
use Klaro\QuotationBundle\Library\ExpressionEvaluatorFactory;
use Klaro\QuotationBundle\Library\ProductDefinitionManager;
use Klaro\QuotationBundle\Library\PhaseEditorManager;
use Klaro\QuotationBundle\SummaryPage\SummaryPageManager;
use Klaro\QuotationBundle\Twig\MenuExtension;
use Klaro\QuotationBundle\Phase\Phase;
use Klaro\QuotationBundle\Library\QuotationModelProvider;
use Klaro\Component\Logger\Logger;
use Klaro\QuotationBundle\Validation\PhaseDataValidatorManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Templating\EngineInterface;

/**
 * Collection of traits to help accessing bundle services. The traits assume that the service container
 * is accessible through an attribute called `container`.
 *
 * Class QuotationServiceTraits
 * @package Klaro\QuotationBundle\Traits
 */
trait QuotationServiceTraits
{
    /**
     * @return QuotationFacade
     */
    public function getQuotationFacade()
    {
        return $this->container->get('klaro_quotation.quotation_facade');
    }

    /**
     * @return ConfiguratorManagerInterface
     */
    public function getConfiguratorManager()
    {
        return $this->container->get('klaro_quotation.configurator_manager');
    }

    /**
     * @return Configurator
     */
    public function getConfigurator()
    {
        return $this->container->get('klaro_quotation.configurator');
    }

    /**
     * @return ProductManagerInterface
     */
    public function getProductManager()
    {
        return $this->container->get('klaro_quotation.product_manager');
    }

    /**
     * @return EventDispatcher
     */
    public function getEventDispatcher()
    {
        return $this->container->get('event_dispatcher');
    }

    /**
     * @return DocumentBuilderInterface
     */
    public function getDocumentBuilder()
    {
        return $this->container->get('klaro_quotation.document_builder');
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->container->get('klaro_quotation.logger');
    }

    /**
     * @return QuotationModelProvider
     */
    protected function getModelProvider()
    {
        return $this->container->get('klaro_quotation.model_manager');
    }

    /**
     * @return ProductDefinitionManager
     */
    protected function getProductDefinitionManager()
    {
        return $this->container->get('klaro_quotation.product_definition_manager');
    }

    /**
     * @return PhaseEditorManager
     */
    protected function getPhaseEditorManager()
    {
        return $this->container->get('klaro_quotation.phase_editor_manager');
    }

    /**
     * @return Phase
     */
    protected function getPhaseForm()
    {
        return $this->container->get('klaro_quotation.phase_form');
    }

    /** @return ExpressionEvaluatorFactory */
    protected function getExpressionEvaluator()
    {
        return $this->container->get('klaro_quotation.expression_evaluator');
    }

    /** @return \Klaro\QuotationBundle\Validation\QuotationValidator */
    protected function getValidator()
    {
        return $this->container->get('klaro_quotation.validator_manager');
    }

    /** @return SummaryPageManager */
    protected function getSummaryPageManager()
    {
        return $this->container->get('klaro_quotation.summary_page_manager');
    }

    /** @return EngineInterface */
    protected function getTemplating()
    {
        return $this->container->get('templating');
    }

    /** @return OutputDocumentManager */
    protected function getOutputDocumentManager()
    {
        return $this->container->get('klaro_quotation.output_document_manager');
    }

    /** @return AdminPageManager */
    protected function getAdminPageManager()
    {
        return $this->container->get('klaro_quotation.admin_page_manager');
    }

    /** @return ApiResponseFormatterInterface */
    protected function getApiResponseFormatter()
    {
        return $this->container->get('klaro_quotation.api_formatter.json');
    }

    /**
     * @return CurrencyConverter
     */
    protected function getCurrencyConverter()
    {
        return $this->container->get('klaro_quotation.currency_converter');
    }

    /**
     * @return QuotationContext
     */
    protected function getQuotationContext()
    {
        return $this->container->get('klaro_quotation.quotation_context');
    }

    /**
     * @return PhaseDataValidatorManager
     */
    protected function getPhaseDataValidationManager()
    {
        return $this->get('klaro_quotation.phase_data_validator_manager');
    }

    /**
     * @return MenuExtension
     */
    protected function getMenuExtension()
    {
        return $this->get('klaro_quotation.menu_extension');
    }
}
