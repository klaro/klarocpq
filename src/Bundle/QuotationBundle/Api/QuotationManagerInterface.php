<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\Common\Result\ResultSetInterface;
use Klaro\Component\QuotationSearch\Query\QuotationQueryInterface;

/**
 * Interface for a quotation manager that is responsible for finding and saving quotations and revisions.
 *
 * Interface QuotationManagerInterface
 * @package Klaro\QuotationBundle\Api
 */
interface QuotationManagerInterface
{
    /**
     * Get single quotation.
     *
     * @param $quotationId
     *
     * @return QuotationInterface
     */
    public function getQuotation($quotationId);

    /**
     * Get list of quotations.
     *
     * @param  QuotationQueryInterface $query
     *
     * @return ResultSetInterface
     *
     * @since 2.0.8
     */
    public function findQuotations(QuotationQueryInterface $query);

    /**
     * Makes a copy of the given quotation, under given user's account. All related phase models should copied and
     * attached to the copied quotation, any linked users of the copied quotation are removed after copying and
     * the owner of the copied quotation is set to be the current user.
     *
     * @param  QuotationInterface $quotation The quotation to copy
     * @param  int                $userId    Id of copied quotation owner
     *
     * @return QuotationInterface Copy of the given quotation.
     */
    public function copyQuotation(QuotationInterface $quotation, QuotationUserInterface $user);

    /**
     * Links quotation under given user's account.
     *
     * @param  QuotationInterface     $quotation The quotation to link
     * @param  QuotationUserInterface $user      User
     */
    public function linkQuotation(QuotationInterface $quotation, QuotationUserInterface $user);

    /**
     * Removes quotation and all related data.
     *
     * @param  QuotationInterface $quotation
     */
    public function removeQuotation(QuotationInterface $quotation);

    /**
     * Removes quotation revision and associated models.
     *
     * @param  QuotationRevisionInterface $revision
     */
    public function removeQuotationRevision(QuotationRevisionInterface $revision);

    /**
     * Create a new quotation.
     *
     * @param $quotationType
     *
     * @return QuotationInterface
     */
    public function createQuotation($quotationType);

    /**
     * Persist quotation to database.
     *
     * @param  QuotationInterface $quotation
     */
    public function persistQuotation(QuotationInterface $quotation);

    /**
     * Persist quotation revision to database.
     *
     * @param  QuotationRevisionInterface $revision
     */
    public function persistQuotationRevision(QuotationRevisionInterface $revision);

    /**
     * Returns true if quotation is linked under given user's account.
     *
     * @param  QuotationInterface     $quotation
     * @param  QuotationUserInterface $user
     *
     * @return mixed
     */
    public function isLinked(QuotationInterface $quotation, QuotationUserInterface $user);

    /**
     * Returns list of users linked to given quotation.
     *
     * @param  QuotationInterface $quotation
     *
     * @return QuotationUserInterface[]
     */
    public function getLinkedUsers(QuotationInterface $quotation);

    /**
     * Remove given quotation from given user's account.
     *
     * @param  QuotationUserInterface $owner
     */
    public function removeLinkedUser(QuotationInterface $quotation, QuotationUserInterface $user);

    /**
     * Remove references to given from all users.
     *
     * @param  QuotationInterface $quotation
     */
    public function removeLinkedUsers(QuotationInterface $quotation);

    /**
     * Get list of revisions associated with the given quotation.
     *
     * @param  QuotationRevisionInterface $quotation
     *
     * @return QuotationRevisionInterface[]
     */
    public function getQuotationRevisions(QuotationInterface $quotation);

    /**
     * Get a quotation revision based on the given id.
     *
     * @param  QuotationRevisionInterface $quotation
     * @param  int                        $revisionId
     *
     * @return QuotationRevisionInterface
     */
    public function getQuotationRevision(QuotationInterface $quotation, $revisionId);

    /**
     * Creates a new revision for the given quotation.
     *
     * @param  QuotationInterface $quotation
     *
     * @return QuotationRevisionInterface
     */
    public function createQuotationRevision(QuotationInterface $quotation);
}
