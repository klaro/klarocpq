<?php

namespace Klaro\QuotationBundle\Api;

final class QuotationQueryEvents
{
    const JOIN = 'klaro_quotation.query_event.join';
}