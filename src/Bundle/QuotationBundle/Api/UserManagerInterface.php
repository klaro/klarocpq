<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\QuotationSearch\Query\UserQueryInterface;
use Klaro\Component\Common\Result\ResultSetInterface;

/**
 * Interface for a user manager.
 *
 * Interface UserManagerInterface
 * @package Klaro\QuotationBundle\Api
 */
interface UserManagerInterface
{
    /**
     * Get current user.
     *
     * @return QuotationUserInterface
     */
    public function getCurrentUser();

    /**
     * Get user with given Id.
     *
     * @param $userId
     *
     * @return QuotationUserInterface
     */
    public function getUser($userId);

    /**
     * Return list of users based on given filters.
     *
     * @param $filters
     *
     * @return QuotationUserInterface[]
     */
    public function getUsers($filters);

    /**
     * Return list of users based on given filters.
     *
     * @param  UserQueryInterface $query
     *
     * @return ResultSetInterface
     *
     * @since 2.0.8
     */
    public function findUsers(UserQueryInterface $query);
}
