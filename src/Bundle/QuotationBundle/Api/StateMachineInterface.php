<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

/**
 * Interface for a status machine that is used to control the flow of the revision process.
 *
 * Interface StateMachineInterface
 * @package Klaro\QuotationBundle\Api
 */
interface StateMachineInterface
{
    /**
     * Tells whether it is possible to move to the given state from the current one.
     *
     * @param  mixed $state New state
     *
     * @return bool
     */
    public function canApply($state);

    /**
     * Moves to the given state.
     *
     * @param  mixed $state New state
     */
    public function apply($state);

    /**
     * Get the current state.
     *
     * @return string
     */
    public function getCurrentState();

    /**
     * Get a list of possible transitions (ie. states to which it is possible to move from the current one)
     *
     * @return array
     */
    public function getAllowedStates();

    /**
     * Revert to the original state.
     */
    public function reset();

    /**
     * Determine if the current state is a final state in the state machine (ie. it is not possible to move
     * to other states from the current state).
     *
     * @return bool
     */
    public function isFinal();

    /**
     * Tells whether the revision should be editable in the current state.
     *
     * @return bool
     */
    public function isEditable();

    /**
     * Tells whether the revision should be printable in the current state.
     *
     * @return bool
     */
    public function isPrintable();
}
