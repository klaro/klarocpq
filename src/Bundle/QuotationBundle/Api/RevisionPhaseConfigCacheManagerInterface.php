<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

use Klaro\Component\Common\Model\QuotationRevisionInterface;

/**
 * Interface for fetching and removing phase config caches.
 *
 * Interface RevisionPhaseConfigCacheManagerInterface
 * @package Klaro\QuotationBundle\Api
 */
interface RevisionPhaseConfigCacheManagerInterface
{
    /**
     * Fetch a cache instance for given revision.
     *
     * @param  QuotationRevisionInterface $revision
     *
     * @return RevisionPhaseConfigCacheInterface
     */
    public function getRevisionPhaseConfigCache(QuotationRevisionInterface $revision);

    /**
     * Remove cache that corresponds to given revision.
     *
     * @param  QuotationRevisionInterface $revision
     */
    public function removeRevisionPhaseConfigCache(QuotationRevisionInterface $revision);
}
