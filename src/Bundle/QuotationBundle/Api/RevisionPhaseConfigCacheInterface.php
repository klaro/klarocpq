<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

/**
 * Cache interface for storing phase configuration.
 *
 * Interface RevisionPhaseConfigCacheInterface
 * @package Klaro\QuotationBundle\Api
 */
interface RevisionPhaseConfigCacheInterface
{
    /**
     * Fetches a phase config entry from the cache.
     *
     * @param  string $phaseId Phase identifier path.
     *
     * @return array|bool The cached phase config or FALSE, if no cache entry exists for the given id.
     */
    public function fetch($phaseId);

    /**
     * Tests if an entry exists in the cache.
     *
     * @param  string $phaseId Phase identifier path.
     *
     * @return bool
     */
    public function contains($phaseId);

    /**
     * Puts phase config data into the cache.
     *
     * If a cache entry with the given id already exists, its data will be replaced.
     *
     * @param  string $phaseId Phase identifier path.
     * @param  array  $data    Cached phase config
     *
     * @return bool TRUE if the entry was successfully stored in the cache, FALSE otherwise.
     */
    public function save($phaseId, array $data);

    /**
     * Deletes a cache entry.
     *
     * @param  string $phaseId Phase identifier path.
     *
     * @return bool TRUE if the cache entry was successfully deleted, FALSE otherwise.
     *              Deleting a non-existing entry is considered successful.
     */
    public function delete($phaseId);
}
