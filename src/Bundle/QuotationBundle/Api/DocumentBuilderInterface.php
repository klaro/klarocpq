<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Document\OutputDocumentManager;

/**
 * Interface for classes which can be used to generate output documents for quotations.
 *
 * Interface DocumentBuilderInterface
 * @package Klaro\QuotationBundle\Api
 */
interface DocumentBuilderInterface
{
    /**
     * Initialize the generation context, called when this builder service is loaded.
     *
     * @param  OutputDocumentManager $context
     */
    public function initialize(OutputDocumentManager $context);

    /**
     * Generate a document.
     *
     * @param  QuotationRevisionInterface  $revision   Current quotation revision
     * @param  DocumentDefinitionInterface $definition Document definition as defined in klaro_quotation.documents parameter
     *
     * @return OutputDocumentInterface
     */
    public function generate(QuotationRevisionInterface $revision, DocumentDefinitionInterface $definition);
}
