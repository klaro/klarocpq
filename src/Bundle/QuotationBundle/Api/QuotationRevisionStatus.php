<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

/**
 * Quotation revision statuses (states).
 *
 * Class QuotationRevisionStatus
 *
 * @package Klaro\QuotationBundle\Api
 */
final class QuotationRevisionStatus
{
    public const IN_PROCESS = 'in_process';
    public const ACTIVE = 'active';
    public const WON = 'won';
    public const LOST = 'lost';
    public const HOLD = 'hold';

    protected static $revisionStatusOptions = [
        self::IN_PROCESS => 'In Process',
        self::ACTIVE     => 'Active',
        self::WON        => 'Won',
        self::LOST       => 'Lost',
        self::HOLD       => 'Hold',
    ];

    /**
     * @return array
     */
    public static function getRevisionStatusOptions(): array
    {
        return self::$revisionStatusOptions;
    }
}
