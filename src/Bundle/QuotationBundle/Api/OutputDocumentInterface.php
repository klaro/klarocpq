<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

/**
 * Interface for output documents. These are documents that can be printed from the revision summary.
 *
 * @package Klaro\QuotationBundle\Api
 */
interface OutputDocumentInterface
{
    /**
     * Document display name.
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set display name.
     *
     * @param  string $title Display name
     *
     * @return OutputDocumentInterface
     */
    public function setTitle($title);

    /**
     * Get filename without path or extension.
     *
     * @return string
     */
    public function getFileName();

    /**
     * Set filename (no path or extension)
     *
     * @param  string $fileName File name
     *
     * @return OutputDocumentInterface
     */
    public function setFileName($fileName);

    /**
     * Get file extension.
     *
     * @return string
     */
    public function getExtension();

    /**
     * Set file extension.
     *
     * @param  string $extension Extension
     *
     * @return OutputDocumentInterface
     */
    public function setExtension($extension);

    /**
     * Return filename with extension.
     *
     * @return string
     */
    public function getFileNameWithExtension();

    /**
     * Return full path to file.
     *
     * @return string
     */
    public function getServerPath();

    /**
     * Set full path to document.
     *
     * @param  string $serverPath Path
     *
     * @return OutputDocumentInterface
     */
    public function setServerPath($serverPath);

    /**
     * Return a list of headers that should be sent with the request.
     *
     * @return array List of headers with name and value, eg. [['Content-Disposition' => '...']]
     */
    public function getHeaders();

    /**
     * Set list of headers to send with the request.
     *
     * @param  array $headers List of headers with name and value, eg. [['Content-Disposition' => '...']]
     *
     * @return OutputDocumentInterface
     */
    public function setHeaders($headers);

    /**
     * Get file contents.
     *
     * @return string
     */
    public function getContent();

    /**
     * Set file contents.
     *
     * @param  string $contents Content
     *
     * @return OutputDocumentInterface
     */
    public function setContent($contents);

    /**
     * Tells whether this document is temporary and should be automatically deleted after generating document.
     *
     * @return bool
     */
    public function isTemporary();

    /**
     * Set temporary status.
     * @param  bool $isTemporary
     *
     * @return OutputDocumentInterface
     *
     * @see isTemporary()
     *
     */
    public function setTemporary($isTemporary);

    /**
     * Tells whether document is already saved to disk.
     *
     * @return bool
     */
    public function isSaved();

    /**
     * Set whether document is saved.
     * @param  bool $isSaved
     *
     * @return OutputDocumentInterface
     *
     * @see isSaved()
     *
     */
    public function setSaved($isSaved);
}
