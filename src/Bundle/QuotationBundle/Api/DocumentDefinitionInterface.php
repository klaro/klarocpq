<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

/**
 * Interface DocumentDefinitionInterface
 * @package Klaro\QuotationBundle\Document
 */
interface DocumentDefinitionInterface
{
    /**
     * @return string Document identifier
     */
    public function getId();

    /**
     * @return string Title
     */
    public function getTitle();

    /**
     * @return string Builder type
     */
    public function getType();

    /**
     * @return string|null Source template
     */
    public function getSource();

    /**
     * @return string|null Language locale, ISO 639-1 two letter code
     */
    public function getLocale();

    /**
     * @return boolean
     */
    public function isDraft();

    /**
     * @return boolean
     */
    public function isDebug();

    /**
     * @return boolean
     */
    public function isVisible();

    /**
     * @return boolean
     */
    public function isDisabled();
}
