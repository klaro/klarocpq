<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

use Symfony\Component\HttpFoundation\Response;

/**
 * Interface for formatting a response to an API request.
 *
 * Interface ApiResponseFormatterInterface
 * @package Klaro\QuotationBundle\Api
 */
interface ApiResponseFormatterInterface
{
    /**
     * Format & return response for successful API request.
     *
     * @param  mixed $data   Payload data
     * @param  int   $status Status code
     * @param  mixed $meta   Optional meta data to send.
     *
     * @return Response Request response
     */
    public function getSuccessResponse($data, $status = 200, $meta = []);

    /**
     * Format & return response for request that failed.
     *
     * @param  string $message Error message
     * @param  int    $code    Error code
     * @param  int    $status  Status code
     * @param  mixed  $meta    Optional meta data to send.
     *
     * @return Response Request response
     */
    public function getErrorResponse($message, $code = 0, $status = 500, $meta = []);
}
