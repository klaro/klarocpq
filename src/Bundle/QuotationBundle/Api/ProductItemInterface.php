<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

use Klaro\Component\Common\Model\ModelInterface;

/**
 * Defines a common interface for product items (used in sales structure).
 *
 * Interface ProductItemInterface
 * @package Klaro\QuotationBundle\Api
 */
interface ProductItemInterface extends ModelInterface
{
    /**
     * Unique item code.
     *
     * @return mixed
     */
    public function getItemCode();

    /**
     * Set item code.
     *
     * @param  mixed $code
     */
    public function setItemCode($code);

    /**
     * Return item code name for internal usage (used eg. in sales structure).
     *
     * @return string
     */
    public function getInternalTitle();

    /**
     * Set name for internal use.
     *
     * @param  string $title
     */
    public function setInternalTitle($title);

    /**
     * Return an external name for the sale item (used in eg. in quotation document)
     * @return string
     */
    public function getExternalTitle();

    /**
     * Set external title.
     *
     * @param $title
     */
    public function setExternalTitle($title);

    /**
     * Set cost price.
     *
     * @param  float $price
     */
    public function setCostPrice($price);

    /**
     * Return cost price.
     *
     * @return float
     */
    public function getCostPrice();

    /**
     * Set sales price.
     *
     * @param  float $salesPrice
     */
    public function setSalesPrice($salesPrice);

    /**
     * Return sales price.
     *
     * @return float
     */
    public function getSalesPrice();

    /**
     * Return values as array.
     *
     * @return array
     */
    public function toArray();
}
