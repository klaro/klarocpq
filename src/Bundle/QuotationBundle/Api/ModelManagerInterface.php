<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

use Klaro\Component\Common\Model\FinderInterface;

/**
 * Defines an interface for a model manager class. A model manager is responsible for fetching, saving models,
 * persisting and removing models. It also returns finders which can be used to query sets of models.
 *
 * Interface ModelManagerInterface
 * @package Klaro\QuotationBundle\Api
 */
interface ModelManagerInterface
{
    /**
     * Return a class that can be used to query items that are used on the form or in the sales structure.
     *
     * @param  string $finderName Finder class name
     *
     * @return FinderInterface
     */
    public function getItemFinder($finderName);
}
