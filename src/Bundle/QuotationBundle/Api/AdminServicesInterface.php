<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface for services implementing admin plugins.
 *
 * @package Klaro\QuotationBundle\Api
 */
interface AdminServicesInterface
{
    /**
     * Returns the page name that is displayed on the navigation bar.
     *
     * @return string Name of the admin page.
     */
    public static function getUserFriendlyName();

    /**
     * Renders the whole page but the base twig is from the Quotation Bundle. Return renderView()
     *
     * @param  Request $request The current request.
     *
     * @return string Rendered content.
     */
    public function render(Request $request);
}
