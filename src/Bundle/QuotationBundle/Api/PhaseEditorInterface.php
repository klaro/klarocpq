<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\FormData\ProductLineDataNode;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Base interface for different form editors.
 *
 * Interface PhaseEditorInterface
 * @package Klaro\QuotationBundle\Api
 */
interface PhaseEditorInterface
{
    /**
     * Initialize the editor with the current phase definition and model.
     *
     * @param  QuotationRevisionInterface $revision
     * @param  ProductLineDataNode        $formData
     * @param $phasePath string
     */
    public function initialize(QuotationRevisionInterface $revision, ProductLineDataNode $formData, $phasePath);

    /**
     * Process user input. Called when editor is request with POST method. If this method returns a response,
     * then that response is used - otherwise the response is the content from render().
     *
     * @param  Request $request
     *
     * @return Response|null
     */
    public function processUserInput(Request $request);

    /**
     * Do post processing before sending a response.
     */
    public function postProcess();

    /**
     * Render a response for the editor.
     *
     * @return string
     */
    public function render();
}
