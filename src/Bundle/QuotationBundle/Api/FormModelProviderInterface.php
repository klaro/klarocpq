<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

/**
 * An interface for providing database actions to manage quotations, revisions, models and users.
 *
 * Interface FormModelProviderInterface
 * @package Klaro\QuotationBundle\Api
 */
interface FormModelProviderInterface extends QuotationManagerInterface, UserManagerInterface, ModelManagerInterface
{
    /**
     * Returns the current user manager instance.
     *
     * @return ModelManagerInterface
     */
    public function getModelManager();

    /**
     * Returns the current quotation manager instance.
     *
     * @return QuotationManagerInterface
     */
    public function getQuotationManager();

    /**
     * Returns the current user manager interface.
     *
     * @return UserManagerInterface
     */
    public function getUserManager();
}
