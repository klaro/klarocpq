<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Api;

/**
 * Order statuses for a quotation revision.
 *
 * Class QuotationRevisionOrderStatus
 *
 * @package Klaro\QuotationBundle\Api
 */
final class QuotationRevisionOrderStatus
{
    public const NONE = 'none';
    public const IN_PROCESS = 'in_process';
    public const REJECTED = 'rejected';
    public const FINISHED = 'finished';

    protected static $revisionOrderStatusOptions = [
        self::NONE       => 'None',
        self::IN_PROCESS => 'In Process',
        self::REJECTED   => 'Rejected',
        self::FINISHED   => 'Finished',
    ];

    /**
     * @return array
     */
    public static function getQuotationRevisionOrderStatusOptions(): array
    {
        return self::$revisionOrderStatusOptions;
    }
}
