<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Validation;

use Klaro\Component\FormData\GroupPhaseDataNode;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\FormData\ProductPhaseDataNode;
use Klaro\Component\FormData\ProductPhaseListDataNode;
use Klaro\Component\FormData\SinglePhaseDataNode;
use Klaro\Component\ProductPhase\PhaseNode;
use Klaro\Component\Validation\BaseValidatorInterface;
use Klaro\Component\Validation\ProductLineValidatorInterface;
use Klaro\Component\Validation\ProductPhaseValidatorInterface;
use Klaro\Component\Validation\SinglePhaseValidatorInterface;
use Klaro\Component\Validation\ValidationResult;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PhaseDataValidatorManager implements
    ProductLineValidatorInterface,
    ProductPhaseValidatorInterface,
    SinglePhaseValidatorInterface
{
    /** @var ContainerInterface */
    protected $container;

    /** @var PhaseValidatorDefinition[] */
    protected $validatorDefinitions;

    /**
     * PhaseDataValidatorManager constructor.
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->validatorDefinitions = [];
    }

    /**
     * @param $serviceId
     * @param $productLines
     * @param $products
     * @param $phases
     */
    public function addValidatorDefinition($serviceId, $productLines = [], $products = [], $phases = [])
    {
        $this->validatorDefinitions[] = new PhaseValidatorDefinition(
            $serviceId,
            $productLines,
            $products,
            $phases
        );
    }

    /**
     * @param ProductLineDataNode $productLine
     * @param ValidationResult    $result
     */
    public function validateProductLine(ProductLineDataNode $productLine, ValidationResult $result)
    {
        if ($productLine->hasChildren()) {
            foreach ($productLine->getChildren() as $phaseId => $dataNode) {
                $phaseNode = $dataNode->getPhaseNode();

                switch ($phaseNode->getType()) {
                    case PhaseNode::PRODUCT_LIST;
                        /** @var ProductPhaseListDataNode $dataNode */
                        $this->validateProductListPhase($dataNode, $result);
                        break;

                    case PhaseNode::PRODUCT:
                        /** @var ProductPhaseDataNode $dataNode */
                        $this->validateProductPhase($dataNode, $result);
                        break;

                    case PhaseNode::GROUP;
                        /** @var GroupPhaseDataNode $dataNode */
                        $this->validateGroupPhase($dataNode, $result);
                        break;
                    case PhaseNode::SINGLE;
                        /** @var SinglePhaseDataNode $dataNode */
                        $this->validateSinglePhase($dataNode, $result);
                        break;
                    default:
                        break;
                }
            }
        }

        $this->getProductLineValidator($productLine->getPhaseNode()->getId())->validateProductLine($productLine, $result);
    }

    /**
     * @param ProductPhaseListDataNode $dataNode
     * @param ValidationResult         $result
     */
    public function validateProductListPhase(ProductPhaseListDataNode $dataNode, ValidationResult $result)
    {
        if ($dataNode->hasChildren()) {
            foreach ($dataNode as $index => $productNode) {
                $this->validateProductPhase($productNode, $result);
            }
        }
    }

    /**
     * @param ProductPhaseDataNode $productNode
     * @param ValidationResult     $result
     */
    public function validateProductPhase(ProductPhaseDataNode $productNode, ValidationResult $result)
    {
        if ($productNode->hasChildren()) {
            foreach ($productNode->getChildren() as $phaseId => $phaseNode) {
                $formData = $phaseNode->getData();
                $changed = $formData->hasAnyChangedFields();
                if ($changed) {
                    /** @var SinglePhaseDataNode $phaseNode */
                    $this->validateSinglePhase($phaseNode, $result);
                }
            }
        }

        $this->getProductValidator($productNode->getPhaseNode()->getRef())->validateProductPhase($productNode, $result);
    }

    /**
     * @param GroupPhaseDataNode $dataNode
     * @param ValidationResult   $result
     */
    public function validateGroupPhase(GroupPhaseDataNode $dataNode, ValidationResult $result)
    {
        if ($dataNode->hasChildren()) {
            foreach ($dataNode->getChildren() as $phaseId => $phaseNode) {
                /** @var SinglePhaseDataNode $phaseNode */
                $this->validateSinglePhase($phaseNode, $result);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function validateSinglePhase(SinglePhaseDataNode $dataNode, ValidationResult $result)
    {
        $changed = $dataNode->getData()->hasAnyChangedFields();
        $this->getSinglePhaseValidator($dataNode->getPhaseNode()->getId())->validateSinglePhase($dataNode, $result);
    }

    /**
     * @param $productLineId
     *
     * @return ProductLineValidatorInterface
     */
    private function getProductLineValidator($productLineId)
    {
        $validators = $this->getValidator(
            $productLineId,
            PhaseValidatorDefinition::PRODUCT_LINE,
            ProductLineValidatorInterface::class
        );

        return new ProductLineValidatorChain($validators);
    }

    /**
     * @param $ref
     *
     * @return ProductPhaseValidatorInterface
     */
    private function getProductValidator($ref)
    {
        $validators = $this->getValidator(
            $ref,
            PhaseValidatorDefinition::PRODUCT,
            ProductPhaseValidatorInterface::class
        );

        return new ProductPhaseValidatorChain($validators);
    }

    /**
     * @param $ref
     *
     * @return SinglePhaseValidatorInterface
     */
    private function getSinglePhaseValidator($ref)
    {
        $validators = $this->getValidator(
            $ref,
            PhaseValidatorDefinition::PHASE,
            SinglePhaseValidatorInterface::class
        );

        return new SinglePhaseValidatorChain($validators);
    }

    /**
     * @param $ref
     * @param $type
     *
     * @return array|BaseValidatorInterface[]
     */
    private function getValidator($ref, $type, $className)
    {
        $validators = [];

        foreach ($this->validatorDefinitions as $validatorDefinition) {
            if ($validatorDefinition->supportsRef($type, $ref)) {
                $validatorService = $this->container->get($validatorDefinition->getServiceId());

                if (!($validatorService instanceof $className)) {
                    throw new \Exception(
                        sprintf('Validator service must implement interface "%s"', $className)
                    );
                }

                $validators[] = $validatorService;
            }
        }

        return $validators;
    }
}
