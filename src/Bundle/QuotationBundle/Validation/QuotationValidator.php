<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Validation;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Event\ValidationEvents;
use Klaro\QuotationBundle\Event\QuotationRevisionEvent;
use Klaro\QuotationBundle\Facade\QuotationFacade;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Manager for validation the quotation state before accessing summary. Gets a list of violations from all
 * validator services which are tagged with the name "klaro_quotation.validator".
 *
 * Class QuotationValidator
 * @package Klaro\QuotationBundle\Validation
 */
class QuotationValidator
{
    /** @var QuotationFacade */
    protected $quotationFacade;

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /** @var QuotationValidatorInterface[] */
    protected $validators;

    /**
     * @param QuotationFacade          $quotationFacade
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(QuotationFacade $quotationFacade, EventDispatcherInterface $eventDispatcher)
    {
        $this->quotationFacade = $quotationFacade;
        $this->eventDispatcher = $eventDispatcher;

        $this->validators = [];
    }

    /**
     * Add a validator service.
     *
     * @param $alias
     * @param QuotationValidatorInterface $service
     *
     * @throws \Exception
     */
    public function addValidator($alias, QuotationValidatorInterface $service)
    {
        if (isset($this->validators[$alias])) {
            throw new \Exception("Tried to add validator service {$alias} but a service with that alias already exists.");
        }

        $this->validators[$alias] = $service;
    }

    /**
     * @param QuotationRevisionInterface $revision
     *
     * @return ValidationResult
     */
    public function validate(QuotationRevisionInterface $revision)
    {
        $validationResult = new ValidationResult();

        $stateMachine = RevisionStateMachine::create($revision);

        // Only validate latest revision.
        if ($stateMachine->isEditable() && $revision->getRevisionId() == $revision->getQuotation()->getLatestRevision()->getRevisionId()) {
            $formData = $this->quotationFacade->getFormDataForRevision($revision);
            $configuration = $this->quotationFacade->getConfigurationForRevision($revision);

            foreach ($this->validators as $validator) {
                if ($validator instanceof FormDataAwareQuotationValidator) {
                    $validator->setFormData($formData);
                }

                if ($validator instanceof ConfigurationAwareQuotationValidator) {
                    $validator->setConfiguration($configuration);
                }

                $validator->validate($revision, $validationResult);
            }

            $event = new QuotationRevisionEvent($revision, [
                'validationResult' => $validationResult,
            ]);

            $this->eventDispatcher->dispatch(ValidationEvents::QUOTATION_SUMMARY_VALIDATE, $event);
        }

        return $validationResult;
    }
}
