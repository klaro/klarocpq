<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Validation;

class ValidationResult
{
    /** @var QuotationViolation[][] */
    protected $violations;

    /** @var QuotationViolation[][] */
    protected $notices;

    /**
     * ValidationResult constructor.
     */
    public function __construct()
    {
        $this->violations = [];
        $this->notices = [];
    }


    /**
     * Get processed violations for the given revision.
     *
     * @return QuotationViolation[]
     */
    public function getViolations()
    {
        return $this->violations;
    }

    /**
     * @return bool
     */
    public function hasViolations()
    {
        return count($this->violations) > 0;
    }

    /**
     * Add a violation for the given revision.
     *
     * @param QuotationViolation $violation
     *
     * @return $this
     */
    public function addViolation(QuotationViolation $violation)
    {
        $this->violations[] = $violation;

        return $this;
    }

    /**
     * Get notices for the given revision.
     *
     * @return QuotationViolation[]
     */
    public function getNotices()
    {
        return $this->notices;
    }

    /**
     * @return bool
     */
    public function hasNotices()
    {
        return count($this->notices) > 0;
    }

    /**
     * Add a notice for the given revision.
     *
     * @param QuotationViolation $notice
     *
     * @return $this
     */
    public function addNotice(QuotationViolation $notice)
    {
        $this->notices[] = $notice;

        return $this;
    }
}
