<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Validation;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\ExpressionEvaluator\ExpressionEvaluatorInterface;
use Klaro\Component\ProductPhase\GroupPhase;
use Klaro\Component\ProductPhase\PhaseNode;
use Klaro\Component\ProductPhase\ProductLine;
use Klaro\Component\ProductPhase\ProductListPhase;
use Klaro\Component\ProductPhase\ProductPhase;
use Klaro\Component\ProductPhase\SinglePhase;
use Klaro\QuotationBundle\Api\FormModelProviderInterface;
use Klaro\QuotationBundle\Library\ExpressionEvaluatorFactory;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class PhaseAccessibilityValidator
{
    /** @var TokenStorageInterface */
    private $tokenStorage;

    /** @var AuthorizationCheckerInterface */
    private $authorizationChecker;

    /** @var ExpressionEvaluatorFactory */
    private $evaluatorFactory;

    /** @var FormModelProviderInterface */
    private $modelProvider;

    /** @var ExpressionEvaluatorInterface */
    private $evaluator;

    /**
     * PhaseAccessibilityValidator constructor.
     *
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param ExpressionEvaluatorFactory    $evaluatorFactory
     * @param FormModelProviderInterface    $modelProvider
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker,
        ExpressionEvaluatorFactory $evaluatorFactory,
        FormModelProviderInterface $modelProvider
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->evaluatorFactory = $evaluatorFactory;
        $this->modelProvider = $modelProvider;
    }

    public function validate(ProductLine $productLine, QuotationRevisionInterface $revision)
    {
        $this->evaluator = $this->evaluatorFactory->getEvaluator(
            $revision,
            [
                'user' => $this->modelProvider->getCurrentUser(),
            ]
        );

        foreach ($productLine->getPhases() as $phase) {
            switch ($phase->getType()) {
                case PhaseNode::SINGLE:
                    /** @var SinglePhase $phase */
                    $this->validateSinglePhaseDataNode($phase);
                    break;
                case PhaseNode::GROUP:
                    /** @var GroupPhase $phase */
                    $this->validateMixedPhaseDataNode($phase);
                    break;
                case PhaseNode::PRODUCT_LIST:
                    /** @var ProductListPhase $phase */
                    $this->validateProductListPhaseDataNode($phase);
                    break;
                case PhaseNode::PRODUCT:
                    /** @var ProductPhase $phase */
                    $this->validateMixedPhaseDataNode($phase);
                    break;
            }
        }
    }

    private function validateSinglePhaseDataNode(SinglePhase $singlePhase)
    {
        if (!$singlePhase instanceof SinglePhase) {
            return;
        }

        $this->validateAccessibility($singlePhase);
    }

    /**
     * @param SinglePhase|GroupPhase|ProductPhase|ProductListPhase $phaseNode
     */
    private function validateAccessibility($phaseNode)
    {
        $parent = $phaseNode->getParent();
        if ($parent && $parent->getDisabled()) {
            $phaseNode->setDisabled(true);

            return;
        }

        $disabled = $phaseNode->getDisabled();
        if (is_string($disabled)) {
            $disabled = $this->evaluator->evaluateExpression($disabled);
        }

        $roles = $phaseNode->getRoles();
        if (false === $disabled && $roles && $this->tokenStorage->getToken()) {
            $disabled = false === $this->authorizationChecker->isGranted($roles);
        }

        $phaseNode->setDisabled($disabled);
    }

    private function validateMixedPhaseDataNode($mixedPhase)
    {
        if (!($mixedPhase instanceof GroupPhase || $mixedPhase instanceof ProductPhase)) {
            return;
        }

        $this->validateAccessibility($mixedPhase);

        foreach ($mixedPhase->getPhases() as $phase) {
            $this->validateSinglePhaseDataNode($phase);
        }
    }

    private function validateProductListPhaseDataNode(ProductListPhase $productListPhase)
    {
        if (!$productListPhase instanceof ProductListPhase) {
            return;
        }

        $this->validateAccessibility($productListPhase);

        foreach ($productListPhase->getProducts() as $product) {
            $this->validateMixedPhaseDataNode($product);
        }
    }
}
