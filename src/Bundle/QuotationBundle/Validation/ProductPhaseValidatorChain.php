<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Validation;

use Klaro\Component\FormData\ProductPhaseDataNode;
use Klaro\Component\Validation\ProductPhaseValidatorInterface;
use Klaro\Component\Validation\ValidationResult;

class ProductPhaseValidatorChain implements ProductPhaseValidatorInterface
{
    /** @var ProductPhaseValidatorInterface[] */
    protected $validators;

    /**
     * ProductPhaseValidatorChain constructor.
     * @param ProductPhaseValidatorInterface[] $validators
     */
    public function __construct(array $validators)
    {
        $this->validators = $validators;
    }

    /**
     * @return mixed
     */
    public function getValidators()
    {
        return $this->validators;
    }

    /**
     * @param mixed $validators
     */
    public function setValidators($validators)
    {
        $this->validators = $validators;
    }

    /**
     * @param ProductPhaseDataNode $productNode
     */
    public function validateProductPhase(ProductPhaseDataNode $productNode, ValidationResult $result)
    {
        foreach ($this->validators as $validator) {
            $validator->validateProductPhase($productNode, $result);
        }
    }
}
