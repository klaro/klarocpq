<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Validation;

use Klaro\Component\Common\Model\QuotationRevisionInterface;

/**
 * Interface for validators that validate the quotation state before accessing the summary.
 *
 * Interface QuotationValidatorInterface
 * @package Klaro\QuotationBundle\Api
 */
interface QuotationValidatorInterface
{
    /**
     * Process any notices or violations that should be shown to the user. Use QuotationValidator::addViolation or
     * ValidationResult::addNotice to add a violation to the current context.
     *
     * @param QuotationRevisionInterface $revision
     * @param ValidationResult           $result
     */
    public function validate(QuotationRevisionInterface $revision, ValidationResult $result);
}
