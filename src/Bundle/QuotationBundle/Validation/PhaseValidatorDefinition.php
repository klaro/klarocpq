<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Validation;

class PhaseValidatorDefinition
{
    const PRODUCT_LINE = 'product_line';
    const PRODUCT = 'product';
    const PHASE = 'phase';

    /** @var string */
    protected $serviceId;

    /** @var array */
    protected $types;

    /**
     * PhaseValidatorDefinition constructor.
     * @param $serviceId
     * @param array     $supportedProductLines
     * @param array     $supportedProducts
     * @param array     $supportedPhases
     */
    public function __construct(
        $serviceId,
        $supportedProductLines = [],
        $supportedProducts = [],
        $supportedPhases = []
    ) {
        $this->serviceId = $serviceId;

        $this->types = [
            self::PRODUCT_LINE => $supportedProductLines,
            self::PRODUCT => $supportedProducts,
            self::PHASE => $supportedPhases,
        ];
    }

    /**
     * @param $type
     * @param $ref
     *
     * @return bool
     */
    public function supportsRef($type, $ref)
    {
        return in_array($ref, $this->types[$type]);
    }

    /**
     * @return string
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }
}
