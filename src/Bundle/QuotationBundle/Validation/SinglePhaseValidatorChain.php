<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Validation;

use Klaro\Component\FormData\SinglePhaseDataNode;
use Klaro\Component\Validation\SinglePhaseValidatorInterface;
use Klaro\Component\Validation\ValidationResult;

class SinglePhaseValidatorChain implements SinglePhaseValidatorInterface
{
    /** @var SinglePhaseValidatorInterface[] */
    protected $validators;

    /**
     * SinglePhaseValidatorChain constructor.
     * @param SinglePhaseValidatorInterface[] $validators
     */
    public function __construct(array $validators)
    {
        $this->validators = $validators;
    }

    /**
     * {@inheritdoc}
     */
    public function validateSinglePhase(SinglePhaseDataNode $dataNode, ValidationResult $result)
    {
        foreach ($this->validators as $validator) {
            $validator->validateSinglePhase($dataNode, $result);
        }
    }
}
