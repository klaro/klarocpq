<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Validation;

use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\Validation\ProductLineValidatorInterface;
use Klaro\Component\Validation\ValidationResult;

class ProductLineValidatorChain implements ProductLineValidatorInterface
{
    /** @var ProductLineValidatorInterface */
    protected $validators;

    /**
     * PhaseValidatorChain constructor.
     * @param array $validators
     */
    public function __construct(array $validators)
    {
        $this->validators = $validators;
    }

    public function validateProductLine(ProductLineDataNode $productLine, ValidationResult $result)
    {
        foreach ($this->validators as $validator) {
            $validator->validateProductLine($productLine, $result);
        }
    }
}
