<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Configurator;

use Klaro\Component\Configurator\ConfigurationContextInterface;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\ConfiguratorBundle\ConfigurationContext\ConfigurationContextInitializerInterface;

class QuotationConfigurationContextInitializer implements ConfigurationContextInitializerInterface
{
    /** @var ProductLineDataNode */
    protected $formData;

    /**
     * QuotationConfigurationContextInitializer constructor.
     * @param ProductLineDataNode $formData
     */
    public function __construct(ProductLineDataNode $formData)
    {
        $this->formData = $formData;
    }

    /**
     * {@inheritdoc}
     */
    public function initialize(ConfigurationContextInterface $context)
    {
        if ($context instanceof FormDataAwareConfigurationContextInterface) {
            $context->setFormData($this->formData);
        }
    }
}
