<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Configurator;

use Klaro\ConfiguratorBundle\ConfigurationContext\ConfigurationContextInitializerChain;
use Klaro\ConfiguratorBundle\ConfigurationContext\ConfigurationContextInitializerInterface;
use Klaro\ConfiguratorBundle\Configurator\Configurator as BaseConfigurator;
use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\ConfiguratorInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\ConfigurationAdapter\ConfigurationAdapterInterface;

class Configurator implements ConfiguratorInterface
{
    /** @var BaseConfigurator */
    protected $configurator;

    /** @var ConfigurationAdapterInterface */
    protected $configuratorAdapter;

    /**
     * Configurator constructor.
     * @param BaseConfigurator              $configurator
     * @param ConfigurationAdapterInterface $configuratorAdapter
     */
    public function __construct(
        BaseConfigurator $configurator,
        ConfigurationAdapterInterface $configuratorAdapter
    ) {
        $this->configurator = $configurator;
        $this->configuratorAdapter = $configuratorAdapter;
    }

    /**
     * @param ConfiguratorDefinitionNode $configuration
     * @param ProductLineDataNode        $formData
     *
     * @return ConfigurationNode
     */
    public function getConfiguration(ConfiguratorDefinitionNode $configuration, ProductLineDataNode $formData)
    {
        $oldContextInitializer = $this->configurator->getContextInitializer();

        // Replace context initializer with a version that also sets the form data to the context.
        //
        $newContextInitializer = new QuotationConfigurationContextInitializer($formData);

        if ($oldContextInitializer instanceof ConfigurationContextInitializerInterface) {
            $newContextInitializer = new ConfigurationContextInitializerChain([
                $newContextInitializer,
                $oldContextInitializer,
            ]);
        }

        $this->configurator->setContextInitializer($newContextInitializer);

        $offeringSummary = $this->configurator->processConfiguration(
            $configuration,
            $this->configuratorAdapter->process($configuration, $formData)
        );

        $this->configurator->setContextInitializer($oldContextInitializer);

        return $offeringSummary;
    }

    /**
     * {@inheritdoc}
     */
    public function processConfigurationByRef(
        $ref,
        $data
    ) {
        return $this->configurator->processConfigurationByRef($ref, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function processConfiguration(
        ConfiguratorDefinitionNode $configuration,
        $data
    ) {
        return $this->configurator->processConfiguration($configuration, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function importConfiguration($exportedConfiguration)
    {
        return $this->configurator->importConfiguration($exportedConfiguration);
    }

    /**
     * @return array
     */
    public function getAllConfiguratorRefs()
    {
        return $this->configurator->getAllConfiguratorRefs();
    }
}
