<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Menu;

class MenuItem implements \JsonSerializable
{
    /** @var string */
    protected $title;

    /** @var string */
    protected $type;

    /** @var bool */
    protected $active;

    /** @var string */
    protected $url;

    /** @var string */
    protected $icon;

    /** @var MenuItem[]|null */
    protected $children;

    /** @var array */
    protected $dataAttributes;

    /** @var string */
    protected $notice;

    /** @var bool */
    protected $disabled;

    /**
     * MenuItem constructor.
     */
    public function __construct()
    {
        $this->title = null;
        $this->type = null;
        $this->active = false;
        $this->url = null;
        $this->icon = null;
        $this->notice = null;
        $this->children = [];
        $this->dataAttributes = [];
        $this->disabled = false;
    }

    /**
     * @return MenuItem
     */
    public static function create()
    {
        return new self;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return $this
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     *
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return null|MenuItem[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param null|MenuItem[] $children
     *
     * @return $this
     */
    public function setChildren($children)
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return array
     */
    public function getDataAttributes()
    {
        return $this->dataAttributes;
    }

    /**
     * @param array $dataAttributes
     *
     * @return $this
     */
    public function setDataAttributes($dataAttributes)
    {
        $this->dataAttributes = $dataAttributes;

        return $this;
    }

    /**
     * @param $attribute
     * @param $value
     *
     * @return $this
     */
    public function addDataAttribute($attribute, $value)
    {
        $this->dataAttributes[$attribute] = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }

    /**
     * @param string $notice
     *
     * @return $this
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * @param bool $disabled
     *
     * @return MenuItem
     */
    public function setDisabled(bool $disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function jsonSerialize()
    {
        $result = [];
        foreach (get_object_vars($this) as $key => $var) {
            $result[$key] = $var;
        }
        return $result;
    }
}
