<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\ProductDataImport;

use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\FormData\ProductPhaseDataNode;
use Klaro\Component\ProductPhase\PhaseNode;

/**
 * Class ProductDataImporter
 * @package Klaro\QuotationBundle\ProductDataImport
 */
class ProductDataImporter
{
    /** @var ProductDataImportInterface */
    protected $matchingImporter;

    /** @var ProductDataImportInterface */
    protected $customImporter;

    /**
     * ProductDataImporter constructor.
     * @param ProductDataImportInterface|null $customImporter
     */
    public function __construct(ProductDataImportInterface $customImporter = null)
    {
        $this->matchingImporter = new MatchingProductDataImporter();
        $this->customImporter = $customImporter;
    }

    /**
     * Get list of mappings for each phase and defined product.
     *
     * @param ProductLineDataNode $productLine
     *
     * @return ProductDataImportMapping
     */
    public function getImporterMappings(ProductLineDataNode $productLine)
    {
        $mappings = ProductDataImportMapping::create();

        /** @var ProductPhaseDataNode[] $productPhases */
        $productPhases = [];

        // Collect product phases.
        foreach ($productLine->getChildren() as $phase) {
            $phaseNode = $phase->getPhaseNode();

            if ($phaseNode->getType() === PhaseNode::PRODUCT_LIST && $phase->hasChildren()) {
                $productPhases = array_merge($productPhases, $phase->getChildren());
            } elseif ($phaseNode->getType() === PhaseNode::PRODUCT) {
                $productPhases[] = $phase;
            }
        }

        // Map product phases and products.
        $productSources = [];

        foreach ($productPhases as $targetProduct) {
            $productRef = $targetProduct->getProductRef();
            $phaseId = $targetProduct->getPath();

            $productInfo = ProductInfo::create()
                ->setPhaseId($phaseId)
                ->setProductRef($productRef)
                ->setTitle($targetProduct->getTitle())
                ->setProductTitle($targetProduct->getPhaseNode()->getTitle());
            $mappings->addProductPhase($phaseId, $productInfo);

            if (!isset($productSources[$productRef])) {
                $productSources[$productRef] = $this->getImporterSource($productRef);
            }

            foreach ($productPhases as $sourceProduct) {
                if ($targetProduct !== $sourceProduct) {
                    if (in_array($sourceProduct->getProductRef(), $productSources[$productRef])) {
                        $mappings->addPhaseMapping($phaseId, $sourceProduct->getPath());
                    }
                }
            }
        }

        $mappings->setProductSources($productSources);

        return $mappings;
    }

    /**
     * @param $productRef
     *
     * @return array
     */
    public function getImporterSource($productRef)
    {
        $sources = $this->matchingImporter->getSources($productRef);

        if ($this->customImporter instanceof ProductDataImportInterface) {
            $sources = array_merge(
                $sources,
                $this->customImporter->getSources($productRef)
            );
        }

        return $sources;
    }

    /**
     * @param ProductPhaseDataNode $target
     * @param ProductPhaseDataNode $source
     */
    public function import(ProductPhaseDataNode $target, ProductPhaseDataNode $source)
    {
        $targetRef = $target->getProductRef();
        $sourceRef = $source->getProductRef();

        if ($targetRef === $sourceRef) {
            $this->matchingImporter->import($target, $source);
        } elseif ($this->customImporter instanceof ProductDataImportInterface) {
            $this->customImporter->import($target, $source);
        }
    }
}
