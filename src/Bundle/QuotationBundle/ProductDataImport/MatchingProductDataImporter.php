<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\ProductDataImport;

use Klaro\Component\FormData\ProductPhaseDataNode;
use Klaro\Component\FormData\SinglePhaseDataNode;

/**
 * Importer for product that have the same type. All data is copied.
 *
 * Class MatchingProductDataImporter
 * @package Klaro\QuotationBundle\ProductDataImport
 */
class MatchingProductDataImporter implements ProductDataImportInterface
{
    /**
     * {@inheritdoc}
     */
    public function getSources($productRef)
    {
        return [$productRef];
    }

    /**
     * {@inheritdoc}
     */
    public function import(ProductPhaseDataNode $target, ProductPhaseDataNode $source)
    {
        /** @var SinglePhaseDataNode[] $targetChildren */
        $targetChildren = $target->getChildren();

        /** @var SinglePhaseDataNode $singlePhase */
        foreach ($source->getChildren() as $phaseId => $singlePhase) {
            if (isset($targetChildren[$phaseId])) {
                $targetChildren[$phaseId]->setFormPhaseData(clone $singlePhase->getFormPhaseData());
            }
        }
    }
}
