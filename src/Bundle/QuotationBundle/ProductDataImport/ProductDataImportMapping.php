<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\ProductDataImport;

/**
 * Class ProductDataImportMapping
 * @package Klaro\QuotationBundle\ProductDataImport
 */
class ProductDataImportMapping
{
    /** @var ProductInfo[] */
    protected $productPhases;

    /** @var array */
    protected $productSources;

    /** @var array */
    protected $phaseMappings;

    /**
     * ProductDataImportMapping constructor.
     */
    public function __construct()
    {
        $this->productPhases = [];
        $this->productSources = [];
        $this->phaseMappings = [];
    }

    /**
     * @return ProductDataImportMapping
     */
    public static function create()
    {
        return new self;
    }

    /**
     * @return mixed
     */
    public function getProductSources()
    {
        return $this->productSources;
    }

    /**
     * @param mixed $productSources
     */
    public function setProductSources($productSources)
    {
        $this->productSources = $productSources;
    }

    /**
     * @return ProductInfo[]
     */
    public function getProductPhases()
    {
        return $this->productPhases;
    }

    /**
     * @param ProductInfo[] $productPhases
     */
    public function setProductPhases($productPhases)
    {
        $this->productPhases = $productPhases;
    }

    /**
     * @param $phaseId
     * @param ProductInfo $phase
     */
    public function addProductPhase($phaseId, ProductInfo $phase)
    {
        $this->productPhases[$phaseId] = $phase;
    }

    /**
     * @return array
     */
    public function getPhaseMappings()
    {
        return $this->phaseMappings;
    }

    /**
     * @param array $phaseMappings
     */
    public function setPhaseMappings($phaseMappings)
    {
        $this->phaseMappings = $phaseMappings;
    }

    /**
     * @param $target
     * @param $source
     */
    public function addPhaseMapping($target, $source)
    {
        if (!isset($this->phaseMappings[$target])) {
            $this->phaseMappings[$target] = [];
        }

        $this->phaseMappings[$target][] = $source;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $productPhases = [];

        foreach ($this->productPhases as $phaseId => $productPhase) {
            $productPhases[$phaseId] = $productPhase->toArray();
        }

        return [
            'productPhases' => $productPhases,
            'productSources' => $this->productSources,
            'phaseMappings' => $this->phaseMappings,
        ];
    }
}
