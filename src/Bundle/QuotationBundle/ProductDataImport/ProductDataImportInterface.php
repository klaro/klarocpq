<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\ProductDataImport;

use Klaro\Component\FormData\ProductPhaseDataNode;

/**
 * Interface ProductDataImportInterface
 * @package Klaro\QuotationBundle\ProductDataImport
 */
interface ProductDataImportInterface
{
    /**
     * Returns an array of product refs from which it is possible to import data.
     *
     * @param string $productRef
     *
     * @return array
     */
    public function getSources($productRef);

    /**
     * Copy data from the source product to the target product.
     *
     * @param ProductPhaseDataNode $target
     * @param ProductPhaseDataNode $source
     */
    public function import(ProductPhaseDataNode $target, ProductPhaseDataNode $source);
}
