<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\ProductDataImport;

/**
 * Class ProductInfo
 * @package Klaro\QuotationBundle\ProductDataImport
 */
class ProductInfo
{
    /** @var string */
    protected $phaseId;

    /** @var string */
    protected $title;

    /** @var string */
    protected $productRef;

    /** @var string */
    protected $productTitle;

    /**
     * @return ProductInfo
     */
    public static function create()
    {
        return new self;
    }

    /**
     * @return string
     */
    public function getPhaseId()
    {
        return $this->phaseId;
    }

    /**
     * @param string $phaseId
     *
     * @return $this
     */
    public function setPhaseId($phaseId)
    {
        $this->phaseId = $phaseId;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductRef()
    {
        return $this->productRef;
    }

    /**
     * @param string $productRef
     *
     * @return $this
     */
    public function setProductRef($productRef)
    {
        $this->productRef = $productRef;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductTitle()
    {
        return $this->productTitle;
    }

    /**
     * @param string $productTitle
     *
     * @return $this
     */
    public function setProductTitle($productTitle)
    {
        $this->productTitle = $productTitle;

        return $this;
    }

    public function toArray()
    {
        return [
            'phaseId' => $this->phaseId,
            'title' => $this->title,
            'productRef' => $this->productRef,
            'productTitle' => $this->productTitle,
        ];
    }
}
