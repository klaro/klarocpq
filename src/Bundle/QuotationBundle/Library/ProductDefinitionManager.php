<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Library;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\ConfiguratorManagerInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorConfig;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;
use Klaro\Component\Configurator\Definition\ConfiguratorProductDefinitionNode;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\FormData\FormDataNodeFactory;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\ProductManager\ProductManagerInterface;
use Klaro\Component\ProductPhase\ProductLine;
use Klaro\Component\ProductPhase\ProductLineDefinition;
use Klaro\Component\Validation\ValidationResult;
use Klaro\Component\Common\Event\ProductDefinitionManagerEvents;
use Klaro\QuotationBundle\Configurator\Configurator;
use Klaro\QuotationBundle\Event\ConfiguratorConfigEvent;
use Klaro\QuotationBundle\Event\ConfiguratorDefinitionEvent;
use Klaro\QuotationBundle\Event\ConfiguratorEvent;
use Klaro\QuotationBundle\Event\ProductLineDefinitionEvent;
use Klaro\QuotationBundle\Validation\PhaseAccessibilityValidator;
use Klaro\QuotationBundle\Validation\PhaseDataValidatorManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Service for getting product phases, sales structure and product item filter definitions.
 *
 * Class ProductDefinitionManager
 * @package Klaro\QuotationBundle\Library
 */
class ProductDefinitionManager
{
    /** @var ProductManagerInterface */
    protected $productManager;

    /** @var ConfiguratorManagerInterface */
    protected $configuratorManager;

    /** @var Configurator */
    protected $configurator;

    /** @var PhaseDataValidatorManager */
    protected $validationManager;

    /** @var PhaseAccessibilityValidator */
    protected $phaseAccessibilityValidator;

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /** @var array */
    protected $quotationTypes;

    /** @var ConfigurationNode[] */
    protected $configurations;

    /** @var ProductLineDataNode[] */
    protected $formData;

    /**
     * ProductDefinitionManager constructor.
     * @param ProductManagerInterface      $productManager
     * @param ConfiguratorManagerInterface $configuratorManager
     * @param Configurator                 $configurator
     * @param PhaseDataValidatorManager    $validationManager
     * @param PhaseAccessibilityValidator  $phaseAccessibilityValidator
     * @param EventDispatcherInterface     $eventDispatcher
     * @param array                        $quotationTypes
     */
    public function __construct(
        ProductManagerInterface $productManager,
        ConfiguratorManagerInterface $configuratorManager,
        Configurator $configurator,
        PhaseDataValidatorManager $validationManager,
        PhaseAccessibilityValidator $phaseAccessibilityValidator,
        EventDispatcherInterface $eventDispatcher,
        $quotationTypes
    ) {
        $this->productManager = $productManager;
        $this->configuratorManager = $configuratorManager;
        $this->configurator = $configurator;
        $this->validationManager = $validationManager;
        $this->phaseAccessibilityValidator = $phaseAccessibilityValidator;
        $this->eventDispatcher = $eventDispatcher;
        $this->quotationTypes = $quotationTypes;

        $this->configurations = [];
        $this->formData = [];
    }

    /**
     * Get list of quotation types.
     *
     * @return array
     */
    public function getQuotationTypes()
    {
        $names = [];

        foreach ($this->quotationTypes as $quotationType => $quotationTypeData) {
            $names[$quotationType] = $quotationTypeData['title'];
        }

        return $names;
    }

    /**
     * @param $quotationType
     *
     * @return bool
     */
    public function hasQuotationType($quotationType)
    {
        return array_key_exists($quotationType, $this->quotationTypes);
    }

    /**
     * @param $productLine
     *
     * @return bool
     */
    public function hasProductLine($productLine)
    {
        return $this->productManager->hasProductLine($productLine);
    }

    /**
     * @param $quotationType
     *
     * @return string
     */
    public function getConfiguredProductLine($quotationType)
    {
        return $this->quotationTypes[$quotationType]['product_line'];
    }

    /**
     * @param $quotationType
     *
     * @return string
     */
    public function getConfigurator($quotationType)
    {
        return $this->quotationTypes[$quotationType]['configuration'];
    }

    /**
     * @param $quotationType
     * @param int           $version
     * @param string        $fetchMode
     *
     * @return ProductLineDefinition
     */
    public function getProductLineDefinitionForQuotationType(
        $quotationType,
        $version = ProductManagerInterface::VERSION_LATEST,
        $fetchMode = ProductManagerInterface::FETCH_CACHED
    ) {
        return $this->getProductLineDefinition(
            $this->getConfiguredProductLine($quotationType),
            $version,
            $fetchMode
        );
    }

    /**
     * Get list of phases from a given revisions.
     *
     * @param string     $productLine
     * @param string|int $version
     * @param string     $fetchMode
     *
     * @return ProductLineDefinition
     */
    public function getProductLineDefinition(
        $productLine,
        $version = ProductManagerInterface::VERSION_LATEST,
        $fetchMode = ProductManagerInterface::FETCH_CACHED
    ) {
        return $this->productManager->getProductLineDefinition($productLine, $version, $fetchMode);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return ProductLineDefinition
     */
    public function getProductLineDefinitionForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ProductManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        $productLineId = $revision->getProductLine();
        $version = !is_null($overrideVersion) ? $overrideVersion : $revision->getProductLineVersion();

        $definition = $this->getProductLineDefinition($productLineId, $version, $fetchMode);

        $event = new ProductLineDefinitionEvent($definition, $revision, [
            'fetchMode' => $fetchMode,
            'saveToRevision' => $saveToRevision,
        ]);

        $this->eventDispatcher->dispatch(ProductDefinitionManagerEvents::PRODUCT_LINE_DEFINITION, $event);

        return $event->getProductLineDefinition();
    }

    /**
     * @param ProductLineDefinition $productLineDefinition
     *
     * @return ProductLine
     */
    public function getProductLine(ProductLineDefinition $productLineDefinition)
    {
        return $this->productManager->getProductLine($productLineDefinition);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return ProductLine
     */
    public function getProductLineForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ProductManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        $definition = $this->getProductLineDefinitionForRevision($revision, $fetchMode, $saveToRevision, $overrideVersion);

        return $this->getProductLine($definition);
    }

    /**
     * @param ProductLine $productLine
     * @param $phaseData
     *
     * @return ProductLineDataNode
     */
    public function getFormData(ProductLine $productLine, $phaseData)
    {
        return FormDataNodeFactory::create($productLine)->createProductLineDataNode($phaseData);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return ProductLineDataNode
     */
    public function getFormDataForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ProductManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        $productLine = $this->getProductLineForRevision($revision, $fetchMode, $saveToRevision, $overrideVersion);

        $cacheKey = $revision->getQuotation()->getId().'-'.$revision->getRevisionId();
        if ($fetchMode !== ProductManagerInterface::FETCH_CACHED || !isset($this->formData[$cacheKey])) {
            $this->formData[$cacheKey] = $this->getFormData($productLine, $revision->getPhaseData());

            if ($this->phaseAccessibilityValidator) {
                $this->phaseAccessibilityValidator->validate($productLine, $revision);
            }
        }

        return $this->formData[$cacheKey];
    }

    /**
     * @param $formDataTree
     */
    public function validateProductLine($formDataTree)
    {
        $validationResult = new ValidationResult();

        $this->validationManager->validateProductLine($formDataTree, $validationResult);
    }

    /**
     * @param $configuratorId
     * @param int            $version
     * @param string         $fetchMode
     *
     * @return ConfiguratorConfig
     */
    public function getConfiguratorConfig(
        $configuratorId,
        $version = ConfiguratorManagerInterface::VERSION_LATEST,
        $fetchMode = ConfiguratorManagerInterface::FETCH_CACHED
    ) {
        return $this->configuratorManager->getConfiguratorConfig($configuratorId, $version, $fetchMode);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return ConfiguratorConfig
     */
    public function getConfiguratorConfigForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ConfiguratorManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        $configuratorId = $revision->getConfigurator();
        $version = !is_null($overrideVersion) ? $overrideVersion : $revision->getConfiguratorVersion();

        $config = $this->getConfiguratorConfig($configuratorId, $version, $fetchMode);

        $event = new ConfiguratorConfigEvent($config, $revision, [
            'fetchMode' => $fetchMode,
            'saveToRevision' => $saveToRevision,
        ]);

        $this->eventDispatcher->dispatch(ProductDefinitionManagerEvents::CONFIGURATOR_CONFIG, $event);

        return $config;
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return ConfiguratorProductDefinitionNode
     */
    public function getConfiguratorStructureForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ConfiguratorManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        $structure = $this->configuratorManager->getConfiguratorStructure(
            $this->getConfiguratorConfigForRevision($revision, $fetchMode, $saveToRevision, $overrideVersion)
        );

        $event = new ConfiguratorDefinitionEvent($revision, $structure);

        $this->eventDispatcher->dispatch(ProductDefinitionManagerEvents::CONFIGURATOR_STRUCTURE, $event);

        return $event->getStructure();
    }

    /**
     * @param ConfiguratorDefinitionNode $structure
     * @param ProductLineDataNode        $formData
     *
     * @return \Klaro\Component\Configurator\Configuration\ConfigurationNode
     */
    public function getConfiguration(ConfiguratorDefinitionNode $structure, ProductLineDataNode $formData)
    {
        return $this->configurator->getConfiguration($structure, $formData);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string                     $fetchMode
     * @param bool                       $saveToRevision
     * @param null                       $overrideVersion
     *
     * @return \Klaro\Component\Configurator\Configuration\ConfigurationNode
     */
    public function getConfigurationForRevision(
        QuotationRevisionInterface $revision,
        $fetchMode = ConfiguratorManagerInterface::FETCH_CACHED,
        $saveToRevision = false,
        $overrideVersion = null
    ) {
        $cacheId = $revision->getQuotation()->getId().'-'.$revision->getRevisionId();

        if ($fetchMode !== ConfiguratorManagerInterface::FETCH_CACHED || !isset($this->configurations[$cacheId])) {
            $exportedSummary = $revision->getOfferingSummary();

            if (empty($exportedSummary) || $fetchMode === ConfiguratorManagerInterface::FETCH_FROM_SOURCE ||
                $revision->getRevisionId() == $revision->getQuotation()->getLatestRevision()->getRevisionId() && $revision->isValid() != true) {
                $structure = $this->getConfiguratorStructureForRevision($revision, $fetchMode, $saveToRevision, $overrideVersion);
                $formData = $this->getFormDataForRevision($revision);
                $configuration = $this->getConfiguration($structure, $formData);

                $event = new ConfiguratorEvent($configuration, $revision);
                $this->eventDispatcher->dispatch(ProductDefinitionManagerEvents::CONFIGURATION, $event);
            } else {
                $configuration = $this->importConfiguration($exportedSummary);
            }

            // Finalize configuration.
            $event = new ConfiguratorEvent($configuration, $revision);
            $this->eventDispatcher->dispatch(ProductDefinitionManagerEvents::CONFIGURATION_FINALIZE, $event);

            $this->configurations[$cacheId] = $event->getConfiguration();
        }

        return $this->configurations[$cacheId];
    }

    /**
     * @param $exportedSummary
     *
     * @return mixed|\Klaro\Component\Configurator\Configuration\ConfigurationNode
     */
    public function importConfiguration($exportedSummary)
    {
        return $this->configurator->importConfiguration($exportedSummary);
    }

    /**
     * @param QuotationRevisionInterface $revision
     *
     * @return mixed|\Klaro\Component\Configurator\Configuration\ConfigurationNode
     */
    public function importConfigurationFromRevision(QuotationRevisionInterface $revision)
    {
        return $this->importConfiguration($revision->getOfferingSummary());
    }
}
