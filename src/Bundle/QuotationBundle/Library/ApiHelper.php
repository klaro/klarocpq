<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Library;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\Common\Query\QueryCriteria;
use Klaro\Component\Common\Query\QueryFields;
use Klaro\Component\Common\Query\SortParam;
use Klaro\Component\QuotationSearch\Query\QueryInterface;
use Klaro\Component\Common\Result\ResultSetInterface;
use Klaro\QuotationBundle\CurrencyConverter\CurrencyConverterInterface;
use Klaro\QuotationBundle\CurrencyConverter\DefaultCurrencyConverter;
use Symfony\Component\HttpFoundation\Request;

/**
 * Helper class used to format data for display in API results.
 *
 * Class ApiHelper
 * @package Klaro\QuotationBundle\Library
 */
class ApiHelper
{
    /**
     * Adds constraints and sort params to a quotation query from the request object.
     *
     * @param QueryInterface $query          Query object.
     * @param Request        $request        Request object
     * @param array          $searchParams   Array of search parameters that map query field values to request variables, format:
     *                                       [ Comparison => [ Query Field => Request variable ] ] For between comparison, the
     *                                       field value has to be an array of the start and end value. eg. [
     *                                       QueryCriteria::EQUALS => [ QuotationQueryFields::ID => 'id' ], QueryCriteria::LIKE
     *                                       => [ QuotationQueryFields::PRODUCT_LINE => 'product_line' ], QueryCriteria::BETWEEN
     *                                       => [ QuotationQueryFields::CREATED_AT => ['created_start', 'created_end'] ] ] This
     *                                       would use variables from the following query:
     *                                       ?id=2&product_line=%test%&created_start=2015-01-01&created_end=2016-12-31
     *
     * @param array          $sortableFields Array of sort parameters that map query field values to sort values, format
     *                                       [ Query Field => Sort variable ] Sort variable is either a query string or
     *                                       a query array that list sort fields. You can use '-' in front of the
     *                                       variable to reverse the sort. Examples of query values: - sort=date -
     *                                       sort=-date - sort[]=title&sort[]=-date
     */
    public static function addQueryConstraints(QueryInterface $query, Request $request, array $searchParams = [], array $sortableFields = [])
    {
        // Add offset and size to query.
        $searchParams[QueryCriteria::EQUALS][QueryFields::FIRST_RESULT] = 'offset';
        $searchParams[QueryCriteria::EQUALS][QueryFields::MAX_RESULTS]  = 'size';

        // Loop given search parameters
        if (count($searchParams) > 0) {
            foreach ($searchParams as $criteria => $criteriaFieldMappings) {
                // Map query fields to request variable values.
                foreach ($criteriaFieldMappings as $criteriaField => $urlParams) {
                    $value = null;

                    if ($criteria === QueryCriteria::BETWEEN) {
                        list($startParam, $endParam) = $urlParams;

                        $startValue = $request->query->get($startParam);
                        $endValue = $request->query->get($endParam);

                        if (!is_null($startValue) && !is_null($endValue)) {
                            $value    = [$startValue, $endValue];
                            $criteria = QueryCriteria::BETWEEN;
                        } elseif (!is_null($startValue)) {
                            $value    = $startValue;
                            $criteria = QueryCriteria::GREATER_EQUAL;
                        } elseif (!is_null($endValue)) {
                            $value    = $endValue;
                            $criteria = QueryCriteria::LESS_EQUAL;
                        }
                    } else {
                        $value = $request->query->get($urlParams);
                    }

                    if (!is_null($value)) {
                        $query->{'set'.$criteriaField}($value, $criteria);
                    }
                }
            }
        }

        // Add sort params.
        $sortParams = $request->query->get('sort');

        if (!is_null($sortParams) && count($sortableFields) > 0) {
            $sortableFields = array_flip($sortableFields);

            // Sort request variable can be a single string or array.
            if (!is_array($sortParams)) {
                $sortParams = [$sortParams];
            }

            foreach ($sortParams as $sortParam) {
                $sortDirection = SortParam::ASCENDING;

                // A minus sign in front of the sort variable name signifies descending sort.
                if (strpos($sortParam, '-') === 0) {
                    $sortParam = substr($sortParam, 1);
                    $sortDirection = SortParam::DESCENDING;
                }

                if (array_key_exists($sortParam, $sortableFields)) {
                    $query->addSortParam($sortableFields[$sortParam], $sortDirection);
                }
            }
        }
    }

    /**
     * Format quotation data as array.
     *
     * @param QuotationInterface $quotation
     *
     * @return array
     */
    public static function getQuotationDataArray(QuotationInterface $quotation)
    {
        $owner = $quotation->getOwner();

        $createdAt = $quotation->getCreatedAt();
        $updatedAt = $quotation->getUpdatedAt();

        $revision = $quotation->getLatestRevision();

        return [
            'quotation_id'    => $quotation->getId(),
            'identifier'     => $revision->getIdentifier(),
            'product_line'    => $quotation->getQuotationType(),
            'owner'           => $owner ? $owner->getId() : null,
            'latest_revision' => $revision ? $revision->getRevisionId() : null,
            'created_at'      => $createdAt ? $createdAt->getTimestamp() : null,
            'updated_at'      => $updatedAt ? $updatedAt->getTimestamp() : null,
        ];
    }

    /**
     * Format revision data as array.
     *
     * @param QuotationRevisionInterface $revision
     *
     * @return array
     */
    public static function getRevisionDataArray(QuotationRevisionInterface $revision)
    {
        $quotation = $revision->getQuotation();

        $user = $revision->getUser();

        $createdAt = $revision->getCreatedAt();
        $updatedAt = $revision->getUpdatedAt();

        return [
            'quotation_id'   => $quotation->getId(),
            'revision_id'    => $revision->getRevisionId(),
            'identifier'     => $revision->getIdentifier(),
            'user_id'        => $user ? $user->getId() : null,
            'user_fullname'  => $user ? $user->getFullname() : null,
            'qualifiers'     => $revision->getQualifiers(),
            'status'         => $revision->getStatus(),
            'title'          => $revision->getTitle(),
            'order_status'   => $revision->getOrderStatus(),
            'log_message'    => $revision->getLogMessage(),
            'created_at'     => $createdAt ? $createdAt->getTimestamp() : null,
            'updated_at'     => $updatedAt ? $updatedAt->getTimestamp() : null,
        ];
    }

    /**
     * Format user data as array.
     *
     * @param QuotationUserInterface $user
     *
     * @return array
     */
    public static function getUserDataArray(QuotationUserInterface $user)
    {
        return [
            'id'       => $user->getId(),
            'fullname' => $user->getFullname(),
            'username' => $user->getUsername(),
            'email'    => $user->getEmail(),
        ];
    }

    /**
     * Format offering summary data as array.
     *
     * @param ConfigurationNode          $summary
     * @param QuotationRevisionInterface $revision
     * @param CurrencyConverterInterface $converter
     *
     * @return array
     */
    public static function getSummaryDataArray(
        ConfigurationNode $summary,
        QuotationRevisionInterface $revision,
        CurrencyConverterInterface $converter = null
    ) {
        if (!($converter instanceof CurrencyConverterInterface)) {
            $converter = new DefaultCurrencyConverter();
        }

        $salesTaxTotals = [];

        foreach ($summary->getSalesTaxTotals() as $percentage => $amount) {
            $salesTaxTotals[$percentage] = $converter->toCurrency($amount);
        }

        return [
            'quotation_id'         => $revision->getQuotation()->getId(),
            'revision_id'          => $revision->getRevisionId(),
            'user_fullname'        => $revision->getUser()->getFullname(),
            'currency'             => $converter->getCurrency(),
            'original_sales_price_with_tax' => $converter->toCurrency($revision->getCalculatedSalesPriceWithTax()), //$converter->toCurrency($revision->getCalculatedSalesPriceWithTax()),
            'sales_price_with_tax' => $converter->toCurrency($summary->getTotalSalesPriceWithTax()),
            'sales_price'          => $converter->toCurrency($summary->getTotalSalesPrice()),
            'sales_tax'            => $salesTaxTotals,
            'cost_price'           => $converter->toCurrency($summary->getTotalCostPrice()),
            'profit'               => $converter->toCurrency($summary->getTotalProfit()),
            'margin'               => $summary->getTotalProfitMargin(),
        ];
    }

    /**
     * @param QueryInterface     $query
     * @param ResultSetInterface $results
     *
     * @return array
     */
    public static function getPaginationInfo(QueryInterface $query, ResultSetInterface $results)
    {
        return [
            'size'        => $results->count(),
            'pageSize'    => $results->getPageSize(),
            'currentPage' => $results->getCurrentPage(),
            'offset'      => $query->getFirstResult() ? $query->getFirstResult()->getValue() : 0,
            'total'       => $results->getTotalNumberOfResults(),
            'pages'       => $results->getNumberOfPages(),
        ];
    }
}
