<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Library;

use Klaro\Component\Cache\CacheInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\ExpressionEvaluator\ExpressionEvaluator;
use Klaro\Component\ExpressionEvaluator\ExpressionEvaluatorInterface;
use Klaro\Component\Cache\ParserCache;
use Klaro\Component\Logger\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service for evaluating expressions based on Symfony Expression Evaluator.
 *
 * Class ExpressionEvaluator
 * @package Klaro\QuotationBundle\Library
 */
class ExpressionEvaluatorFactory
{
    /** @var Logger */
    protected $logger;

    /** @var ParserCache */
    protected $parserCache;

    /** @var RequestStack */
    protected $requestStack;

    /** @var CacheInterface */
    protected $cache;

    /**
     * ExpressionEvaluatorFactory constructor.
     * @param Logger              $logger
     * @param ParserCache         $parserCache
     * @param RequestStack        $requestStack
     * @param CacheInterface|null $cache
     */
    public function __construct(
        Logger $logger,
        ParserCache $parserCache,
        RequestStack $requestStack,
        CacheInterface $cache = null
    ) {
        $this->logger = $logger;
        $this->parserCache = $parserCache;
        $this->requestStack = $requestStack;
        $this->cache = $cache;
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param array                      $context
     *
     * @return ExpressionEvaluatorInterface
     */
    public function getEvaluator(QuotationRevisionInterface $revision, $context = [])
    {
        $cacheId = sprintf(
            'Expression//%s/%s/%s',
            $revision->getQuotation()->getId(),
            $revision->getRevisionId(),
            join(',', array_keys($context))
        );

        if ($this->cache && $this->cache->contains($cacheId)) {
            $evaluator = $this->cache->fetch($cacheId);
        } else {
            $evaluator = new ExpressionEvaluator(
                $this->parserCache,
                $this->logger
            );

            $this->cache->save($cacheId, $evaluator);
        }

        $evaluator->setContext($context + [
            'revision' => $revision,
            'quotation' => $revision->getQuotation(),
            'request' => $this->requestStack->getCurrentRequest(),
        ]);

        return $evaluator;
    }
}
