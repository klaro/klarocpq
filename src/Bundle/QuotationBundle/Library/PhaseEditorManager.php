<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Library;

use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\ProductPhase\PhaseNode;
use Klaro\Component\ProductPhase\SinglePhase;
use Klaro\QuotationBundle\Api\PhaseEditorInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manager service for editors (services tagged with name `klaro_quotation.phase_editor`).
 *
 * Class PhaseEditorManager
 * @package Klaro\QuotationBundle\Library
 */
class PhaseEditorManager
{
    /** @var ContainerInterface */
    protected $container;

    /** @var string[] */
    protected $editorServices;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->editorServices = [];
    }

    /**
     * Add an editor service definition.
     *
     * @param $name
     * @param PhaseEditorInterface $editor
     *
     * @throws \Exception
     */
    public function addEditorService($name, $editorService)
    {
        if (isset($this->editorServices[$name])) {
            throw new \Exception("A phase editor with the name '{$name}' already exists!'");
        }

        $this->editorServices[$name] = $editorService;
    }

    /**
     * Get defined editor services.
     *
     * @return array|\string[]
     */
    public function getEditorServices()
    {
        return $this->editorServices;
    }

    /**
     * Get an editor for the given phase.
     *
     * @param $name
     *
     * @return PhaseEditorInterface
     *
     * @throws \Exception
     */
    public function createEditor(
        QuotationRevisionInterface $revision,
        ProductLineDataNode $formData,
        $phasePath
    ) {
        $singlePhaseNode = $formData->getSinglePhaseByPath($phasePath);

        /** @var SinglePhase $singlePhase */
        $singlePhase = $singlePhaseNode->getPhaseNode();

        if ($singlePhase->getType() === PhaseNode::PRODUCT) {
            $singlePhaseNode = $singlePhaseNode->current();
            $singlePhase = $singlePhaseNode->getPhaseNode();
        }

        $phaseDefinition = $singlePhase->getPhaseDefinition();
        $editorName = $phaseDefinition->getEditor();

        if (!isset($this->editorServices[$editorName])) {
            throw new \Exception("A phase editor with the name '{$editorName}' does not exist!'");
        }

        /** @var PhaseEditorInterface $editor */
        $editor = $this->container->get($this->editorServices[$editorName]);
        $editor->initialize($revision, $formData, $phasePath);

        return $editor;
    }
}
