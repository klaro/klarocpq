<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Library;

use Klaro\Component\Common\Model\FinderInterface;
use Klaro\Component\Common\Model\FormModelInterface;
use Klaro\QuotationBundle\Api\FormModelProviderInterface;
use Klaro\Component\Common\Model\ModelInterface;
use Klaro\QuotationBundle\Api\ModelManagerInterface;
use Klaro\QuotationBundle\Api\QuotationManagerInterface;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Event\QuotationModelEvents;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\Cache\CacheInterface;
use Klaro\Component\Common\Query\QueryCriteria;
use Klaro\Component\QuotationSearch\Query\QuotationQuery;
use Klaro\Component\QuotationSearch\Query\QuotationQueryFields;
use Klaro\Component\QuotationSearch\Query\QuotationQueryInterface;
use Klaro\Component\Common\Result\ResultSetInterface;
use Klaro\Component\QuotationSearch\Query\RevisionQueryFields;
use Klaro\Component\QuotationSearch\Query\UserQueryFields;
use Klaro\Component\QuotationSearch\Query\UserQueryInterface;
use Klaro\Component\Common\Event\UserEvents;
use Klaro\QuotationBundle\Api\UserManagerInterface;
use Klaro\QuotationBundle\Event\FormModelEvent;
use Klaro\QuotationBundle\Event\ModelManagerEvent;
use Klaro\QuotationBundle\Event\QuotationEvent;
use Klaro\QuotationBundle\Event\FormItemFinderEvent;
use Klaro\QuotationBundle\Event\QuotationManagerEvent;
use Klaro\QuotationBundle\Event\QuotationRevisionEvent;
use Klaro\QuotationBundle\Event\QuotationUserEvent;
use Klaro\QuotationBundle\Event\UserEvent;
use Klaro\QuotationBundle\Event\UserManagerEvent;
use Klaro\QuotationBundle\Traits\EventDispatcherTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 *
 * Class QuotationModelProvider
 * @package Klaro\QuotationBundle\Library
 */
class QuotationModelProvider implements FormModelProviderInterface
{

    use EventDispatcherTrait;

    const CACHE_PREFIX = 'models//';
    /** @var EventDispatcherInterface */
    protected $dispatcher;

    /** @var CacheInterface */
    protected $cache;

    /** @var QuotationManagerInterface */
    protected $quotationManager;

    /** @var ModelManagerInterface */
    protected $modelManager;

    /** @var UserManagerInterface */
    protected $userManager;

    /**
     * @param EventDispatcherInterface $dispatcher
     * @param CacheInterface           $cache
     * @param $phases
     */
    public function __construct(EventDispatcherInterface $dispatcher, CacheInterface $cache)
    {
        $this->dispatcher = $dispatcher;
        $this->cache      = $cache;
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotationManager()
    {
        if (!$this->quotationManager) {
            $this->quotationManager = $this->loadQuotationManager();
        }

        return $this->quotationManager;
    }

    /**
     * Load quotation maanager.
     *
     * @return QuotationManagerInterface
     */
    public function loadQuotationManager()
    {
        $modelFromCache = $this->getFromCache('QuotationManager');

        if ($modelFromCache) {
            return $modelFromCache;
        }

        $event = new QuotationManagerEvent();

        $this->dispatchEvent(QuotationModelEvents::LOAD_QUOTATION_MANAGER, $event);

        return $this->storeToCache(
            'QuotationManager',
            $this->validateQuotationManager($event->getManager())
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getModelManager()
    {
        if (!$this->modelManager) {
            $this->modelManager = $this->loadModelManager();
        }

        return $this->modelManager;
    }

    /**
     * Load model manager.
     *
     * @return ModelManagerInterface
     */
    public function loadModelManager()
    {
        $modelFromCache = $this->getFromCache('ModelManager');

        if ($modelFromCache) {
            return $modelFromCache;
        }

        $event = new ModelManagerEvent();

        $this->dispatchEvent(QuotationModelEvents::LOAD_MODEL_MANAGER, $event);

        return $this->storeToCache(
            'ModelManager',
            $this->validateModelManager($event->getManager())
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getUserManager()
    {
        if (!$this->userManager) {
            $this->userManager = $this->loadUserManager();
        }

        return $this->userManager;
    }

    /**
     * Load user manager.
     *
     * @return UserManagerInterface
     */
    public function loadUserManager()
    {
        $modelFromCache = $this->getFromCache('UserManager');

        if ($modelFromCache) {
            return $modelFromCache;
        }

        $event = new UserManagerEvent();

        $this->dispatchEvent(QuotationModelEvents::LOAD_USER_MANAGER, $event);

        return $this->storeToCache(
            'UserManager',
            $this->validateUserManager($event->getManager())
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotation($quotationId)
    {
        $cacheName = 'Quotation//'.$quotationId;

        $modelFromCache = $this->getFromCache($cacheName);

        if ($modelFromCache) {
            return $modelFromCache;
        }

        $quotation = $this->getQuotationManager()->getQuotation($quotationId);

        $event = new QuotationEvent($quotation, array(
            'quotationId' => $quotationId,
        ));

        $this->dispatchEvent(QuotationModelEvents::LOAD_QUOTATION, $event);

        return $this->storeToCache(
            $cacheName,
            $this->validateQuotation($event->getQuotation())
        );
    }

    /**
     * {@inheritDoc}
     */
    public function createQuotation($quotationType)
    {
        $quotationManager = $this->getQuotationManager();

        $quotation = $quotationManager->createQuotation($quotationType);
        $quotation->setQuotationType($quotationType);

        $event = new QuotationEvent($quotation, array(
            'quotationType' => $quotationType,
        ));

        $this->dispatchEvent(QuotationModelEvents::CREATE_QUOTATION, $event);

        $quotation = $this->validateQuotation($event->getQuotation());

        $quotationManager->persistQuotation($quotation);

        $cacheName = 'Quotation//'.$quotation->getId();

        return $this->storeToCache($cacheName, $quotation);
    }

    /**
     * {@inheritDoc}
     */
    public function linkQuotation(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        $event = new QuotationUserEvent($quotation, $user);

        $this->dispatchEvent(QuotationModelEvents::LINK_QUOTATION_TO_USER, $event);

        $this->getQuotationManager()->linkQuotation($quotation, $user);
    }

    /**
     * {@inheritDoc}
     */
    public function copyQuotation(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        $event = new QuotationUserEvent($quotation, $user);

        $this->dispatchEvent(QuotationModelEvents::COPY_QUOTATION, $event);

        $quotationManager = $this->getQuotationManager();

        $newQuotation = $quotationManager->copyQuotation($quotation, $user);

        $this->validateQuotation($newQuotation);

        $newQuotation->setOwner($user);

        $quotationManager->removeLinkedUsers($newQuotation);
        $quotationManager->linkQuotation($newQuotation, $user);

        return $newQuotation;
    }

    /**
     * {@inheritDoc}
     */
    public function isLinked(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        return $this->getQuotationManager()->isLinked($quotation, $user);
    }

    /**
     * {@inheritDoc}
     */
    public function removeQuotation(QuotationInterface $quotation)
    {
        $event = new QuotationEvent($quotation);

        $this->dispatchEvent(QuotationModelEvents::REMOVE_QUOTATION, $event);

        $this->removeLinkedUsers($quotation);

        $this->getQuotationManager()->removeQuotation($quotation);
    }

    /**
     * {@inheritDoc}
     */
    public function removeQuotationRevision(QuotationRevisionInterface $revision)
    {
        $event = new QuotationRevisionEvent($revision);

        $this->dispatchEvent(QuotationModelEvents::REMOVE_REVISION, $event);

        $this->getQuotationManager()->removeQuotationRevision($revision);
    }

    /**
     * {@inheritDoc}
     */
    public function getItemFinder($finderName)
    {
        $modelFromCache = $this->getFromCache($finderName);

        if ($modelFromCache) {
            return $modelFromCache;
        }

        $finder = $this->getModelManager()->getItemFinder($finderName);

        $event = new FormItemFinderEvent($finder, array(
            'finderName' => $finderName,
        ));

        $this->dispatchEvent(QuotationModelEvents::LOAD_ITEM_FINDER, $event);

        return $this->storeToCache(
            $finderName,
            $this->validateFinder($event->getFinder())
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrentUser()
    {
        $user = $this->getUserManager()->getCurrentUser();

        $event = new UserEvent($user);

        $this->dispatchEvent(UserEvents::CURRENT_USER, $event);

        $user = $event->getUser();

        return $user ? $this->validateUser($user) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function getUsers($filters)
    {
        return $this->getUserManager()->getUsers($filters);
    }

    /**
     * {@inheritDoc}
     */
    public function findUsers(UserQueryInterface $query)
    {
        return $this->getUserManager()->findUsers($query);
    }

    /**
     * {@inheritDoc}
     */
    public function getUser($userId)
    {
        $cacheName = 'User//'.$userId;

        $modelFromCache = $this->getFromCache($cacheName);

        if ($modelFromCache) {
            return $modelFromCache;
        }

        $user = $this->getUserManager()->getUser($userId);

        $event = new UserEvent($user, array(
            'userId' => $userId,
        ));

        $this->dispatchEvent(UserEvents::LOAD_USER, $event);

        return $this->storeToCache(
            $cacheName,
            $this->validateUser($event->getUser())
        );
    }

    /**
     * {@inheritDoc}
     */
    public function findQuotations(QuotationQueryInterface $query)
    {
        $result = $this->getQuotationManager()->findQuotations($query);

        if (!is_null($result) && !($result instanceof ResultSetInterface)) {
            throw new \Exception('ResultSetInterface expected.');
        }

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public function getLinkedUsers(QuotationInterface $quotation)
    {
        return $this->getQuotationManager()->getLinkedUsers($quotation);
    }

    /**
     * {@inheritDoc}
     */
    public function removeLinkedUser(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        return $this->getQuotationManager()->removeLinkedUser($quotation, $user);
    }

    /**
     * {@inheritDoc}
     */
    public function removeLinkedUsers(QuotationInterface $quotation)
    {
        $this->getQuotationManager()->removeLinkedUsers($quotation);
    }

    /**
     * {@inheritDoc}
     */
    public function persistQuotation(QuotationInterface $quotation)
    {
        $this->getQuotationManager()->persistQuotation($quotation);
    }

    /**
     * {@inheritDoc}
     */
    public function persistQuotationRevision(QuotationRevisionInterface $revision)
    {
        $this->getQuotationManager()->persistQuotationRevision($revision);
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotationRevision(QuotationInterface $quotation, $revisionId)
    {
        $cacheName = 'Revision//'.$quotation->getId().'-'.$revisionId;

        $modelFromCache = $this->getFromCache($cacheName);

        if ($modelFromCache) {
            return $modelFromCache;
        }

        $revision = $this->getQuotationManager()->getQuotationRevision($quotation, $revisionId);

        $event = new QuotationRevisionEvent($revision, array(
            'revisionId' => $revisionId,
        ));

        $this->dispatchEvent(QuotationModelEvents::LOAD_REVISION, $event);

        return $this->storeToCache(
            $cacheName,
            $this->validateQuotationRevision($event->getRevision())
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotationRevisions(QuotationInterface $quotation)
    {
        return $this->getQuotationManager()->getQuotationRevisions($quotation);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuotationRevision(QuotationInterface $quotation)
    {
        $quotationManager = $this->getQuotationManager();

        $revision = $quotationManager->createQuotationRevision($quotation);

        $revision->setQuotation($quotation);

        $latestRevision   = $quotation->getLatestRevision();
        $latestRevisionId = $latestRevision ? $latestRevision->getRevisionId() : 0;

        $revision->setRevisionId($latestRevisionId + 1);

        $event = new QuotationRevisionEvent($revision);

        $this->dispatchEvent(QuotationModelEvents::CREATE_REVISION, $event);

        $revision = $this->validateQuotationRevision($event->getRevision());

        $quotationManager->persistQuotationRevision($revision);

        $cacheName = 'Revision//'.$quotation->getId().'_'.$revision->getRevisionId();

        return $this->storeToCache($cacheName, $revision);
    }

    /**
     * @param $quotation
     *
     * @return QuotationInterface
     *
     * @throws \Exception
     */
    protected function validateQuotation($quotation)
    {
        if (!$quotation) {
            throw new \Exception("Quotation not found");
        }

        if (!($quotation instanceof QuotationInterface)) {
            throw new \Exception("Quotation must be an implementation of QuotationInterface");
        }

        return $quotation;
    }

    /**
     * @param $revision
     *
     * @return QuotationRevisionInterface
     *
     * @throws \Exception
     */
    protected function validateQuotationRevision($revision)
    {
        if (!$revision) {
            throw new \Exception("Quotation Revision not found");
        }

        if (!($revision instanceof QuotationRevisionInterface)) {
            throw new \Exception("Quotation Revision must be an implementation of QuotationRevisionInterface");
        }

        return $revision;
    }

    /**
     * @param $manager
     *
     * @return QuotationManagerInterface
     *
     * @throws \Exception
     */
    protected function validateQuotationManager($manager)
    {
        if (!$manager) {
            throw new \Exception("Quotation Manager not found");
        }

        if (!($manager instanceof QuotationManagerInterface)) {
            throw new \Exception("Quotation Manager must be an implementation of QuotationManagerInterface");
        }

        return $manager;
    }

    /**
     * @param $manager
     *
     * @return ModelManagerInterface
     *
     * @throws \Exception
     */
    protected function validateModelManager($manager)
    {
        if (!$manager) {
            throw new \Exception("Model Manager not found");
        }

        if (!($manager instanceof ModelManagerInterface)) {
            throw new \Exception("Model Manager must be an implementation of ModelManagerInterface");
        }

        return $manager;
    }

    /**
     * @param $manager
     *
     * @return UserManagerInterface
     *
     * @throws \Exception
     */
    protected function validateUserManager($manager)
    {
        if (!$manager) {
            throw new \Exception("User Manager not found");
        }

        if (!($manager instanceof UserManagerInterface)) {
            throw new \Exception("User Manager must be an implementation of UserManagerInterface");
        }

        return $manager;
    }

    /**
     * @param $finder
     *
     * @return FinderInterface
     *
     * @throws \Exception
     */
    protected function validateFinder($finder)
    {
        if (!$finder) {
            throw new \Exception("Finder not found");
        }

        if (!($finder instanceof FinderInterface)) {
            throw new \Exception("Finder must be an implementation of FinderInterface");
        }

        return $finder;
    }

    /**
     * @param $user
     *
     * @return mixed
     *
     * @throws \Exception
     */
    protected function validateUser($user)
    {
        if (!$user) {
            throw new \Exception("User not found");
        }

        if (!($user instanceof QuotationUserInterface)) {
            throw new \Exception("User must be an implementation of QuotationUserInterface");
        }

        return $user;
    }

    /**
     * Get item from cache.
     *
     * @param $name
     *
     * @return null
     */
    private function getFromCache($name)
    {
        $cachedName = static::CACHE_PREFIX.$name;

        return $this->cache->fetch($cachedName);
    }

    /**
     * Store an item to cache.
     *
     * @param $name
     * @param $model
     *
     * @return mixed
     */
    private function storeToCache($name, $model)
    {
        $cachedName = static::CACHE_PREFIX.$name;

        $this->cache->save($cachedName, $model);

        return $model;
    }
}
