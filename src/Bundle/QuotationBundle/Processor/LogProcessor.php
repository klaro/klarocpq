<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Processor;

use Klaro\QuotationBundle\Api\FormModelProviderInterface;
use Symfony\Bridge\Monolog\Processor\WebProcessor;

/**
 * Service to add extra information to monolog log entries.
 *
 * Class LogProcessor
 * @package Klaro\QuotationBundle\Processor
 */
class LogProcessor extends WebProcessor
{
    /** @var FormModelProviderInterface */
    protected $modelProvider;

    /**
     * @param FormModelProviderInterface $modelProvider
     */
    public function __construct(FormModelProviderInterface $modelProvider)
    {
        parent::__construct();

        $this->modelProvider = $modelProvider;
    }

    /**
     * @param array $record
     *
     * @return array
     */
    public function processRecord(array $record)
    {
        // Add current user
        $user = $this->modelProvider->getCurrentUser();

        if ($user) {
            $record['extra']['user'] = $user;
        }

        return $record;
    }
}
