<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Controller\WebApi;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Configurator\ConfiguratorManagerInterface;
use Klaro\Component\FormData\ProductPhaseDataNode;
use Klaro\Component\Validation\ValidationResult;
use Klaro\QuotationBundle\Controller\QuotationController as Controller;
use Klaro\QuotationBundle\Facade\QuotationFacade;
use Klaro\QuotationBundle\Library\ApiHelper;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Klaro\QuotationBundle\Traits\QuotationParameterTraits;
use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Klaro\QuotationBundle\Traits\QuotationValidationTraits;
use Klaro\QuotationBundle\Validation\PhaseDataValidatorManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Api controller routes for managing revisions.
 *
 * Class RevisionApiController
 * @package Klaro\QuotationBundle\Controller\WebApi
 */
class RevisionApiController extends Controller
{
    use QuotationServiceTraits, QuotationParameterTraits, QuotationValidationTraits;

    /**
     * Get list of quotaion revisions.
     *
     * @Route("/quotation/{quotationId}/revisions", name="klaro_quotation_api_get_quotation_revisions", methods={"GET"})
     */
    public function getQuotationRevisionsAction($quotationId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);

            $data = [];

            $latestRevision   = $quotation->getLatestRevision();
            $latestRevisionId = $latestRevision ? $latestRevision->getRevisionId() : 0;

            for ($revId = $latestRevisionId; $revId > 0; --$revId) {
                $revision = $quotationFacade->getQuotationRevision($quotation, $revId);

                $data[] = ApiHelper::getRevisionDataArray($revision);
            }

            return $responseFormatter->getSuccessResponse($data);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Get detailed revision data.
     *
     * @Route("/quotation/{quotationId}/revision/{revisionId}", name="klaro_quotation_api_get_quotation_revision", methods={"GET"})
     *
     * @param Request     $request
     * @param $quotationId
     * @param $revisionId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getQuotationRevisionAction(Request $request, $quotationId, $revisionId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);
            $revision  = $quotationFacade->getQuotationRevision($quotation, $revisionId);

            $showState = (bool) ($request->query->get('state') === 'true');

            $data = $this->getRevisionDataArray($revision, $showState);

            return $responseFormatter->getSuccessResponse($data);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Set revision data.
     *
     * @Route("/quotation/{quotationId}/revisions", name="klaro_quotation_api_post_quotation_revision", methods={"POST"})
     */
    public function postQuotationRevisionAction(Request $request, $quotationId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $revisionId = $request->query->get('revisionId');
        $content = $request->getContent();

        $revisionData = [];

        if (!empty($content)) {
            $revisionData = json_decode($content, true); // 2nd param to get as array
        }

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);
            $currentUser = $quotationFacade->getCurrentUser();

            $this->ensureUserHasAccessToQuotation($quotation, $currentUser);

            $revision = $quotationFacade->createQuotationRevision($quotation, $currentUser, $revisionId);

            $this->updateRevisionData($revision, $revisionData, !is_null($revisionId));

            $showState = (bool) ($request->query->get('state') === 'true');

            $data = $this->getRevisionDataArray($revision, $showState);

            return $responseFormatter->getSuccessResponse($data);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Set revision data.
     *
     * @Route("/quotation/{quotationId}/revision/{revisionId}", name="klaro_quotation_api_put_quotation_revision", methods={"PUT"})
     */
    public function putQuotationRevisionAction(Request $request, $quotationId, $revisionId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $content = $request->getContent();

        $revisionData = [];

        if (!empty($content)) {
            $revisionData = json_decode($content, true); // 2nd param to get as array
        }

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);
            $revision  = $quotationFacade->getQuotationRevision($quotation, $revisionId);
            $this->ensureUserHasAccessToQuotation($quotation, $quotationFacade->getCurrentUser());

            $this->updateRevisionData($revision, $revisionData);

            $showState = (bool) ($request->query->get('state') === 'true');

            $data = $this->getRevisionDataArray($revision, $showState);

            return $responseFormatter->getSuccessResponse($data);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @Route("/quotation/{quotationId}/revision/{revisionId}/formdata", name="klaro_quotation_api_get_quotation_formdata", methods={"GET"})
     *
     * @param Request     $request
     * @param $quotationId
     * @param $revisionId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getQuotationRevisionFormDataAction(Request $request, $quotationId, $revisionId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);
            $revision  = $quotationFacade->getQuotationRevision($quotation, $revisionId);

            $data = $quotationFacade->getFormDataForRevision($revision);

            return $responseFormatter->getSuccessResponse($data->toArray());
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @Route("/quotation/{quotationId}/revision/{revisionId}/menu", name="klaro_quotation_api_get_quotation_menu", methods={"GET"})
     *
     * @param $quotationId
     * @param $revisionId

     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getQuotationRevisionMenuAction($quotationId, $revisionId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);
            $revision  = $quotationFacade->getQuotationRevision($quotation, $revisionId);

            $formData = $quotationFacade->getFormDataForRevision($revision);

            return $responseFormatter->getSuccessResponse([
                'menu' => $this->getMenuExtension()->collectMenuData($revision, $formData, false),
                'summaries' => $this->getMenuExtension()->collectSummariesData($revision),
            ]);
        } catch(\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Get revision summary totals.
     *
     * @Route("/quotation/{quotationId}/revision/{revisionId}/summary", name="klaro_quotation_api_get_revision_summary", methods={"GET"})
     */
    public function getRevisionSummaryAction(Request $request, $quotationId, $revisionId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);
            $revision  = $quotationFacade->getQuotationRevision($quotation, $revisionId);

            // Allow to force reloading of the phase definition in dev mode.
            $refresh = false;

            if ($this->container->getParameter('kernel.environment') == 'dev') {
                $refresh = (bool) ($request->query->get('reload') === '1');
            }

            $fetchMode = $refresh ? ConfiguratorManagerInterface::FETCH_FROM_SOURCE : ConfiguratorManagerInterface::FETCH_CACHED;

            $offeringSummary = $quotationFacade->getConfigurationForRevision($revision, $fetchMode, $refresh);
            $currencyConverter = $this->getCurrencyConverter()->getCurrencyConverterForRevision($revision);

            $data = ApiHelper::getSummaryDataArray($offeringSummary, $revision, $currencyConverter);

            return $responseFormatter->getSuccessResponse($data);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Get revision phase data.
     *
     * @Route("/quotation/{quotationId}/revision/{revisionId}/{phaseId}", name="klaro_quotation_api_get_quotation_phase_data", methods={"GET"})
     *
     * @param Request     $request
     * @param $quotationId
     * @param $revisionId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getQuotationRevisionPhaseDataAction(Request $request, $quotationId, $revisionId, $phaseId)
    {
        $request->query->add([
            'phaseId' => $phaseId,
            'json' => true,
            'menu' => false,
        ]);

        return parent::editAction($request, $quotationId, $revisionId);
    }

    /**
     * Set user defined revision sales price.
     *
     * @Route("/quotation/{quotationId}/revision/{revisionId}/summary", name="klaro_quotation_api_post_revision_summary", methods={"POST", "PUT"})
     */
    public function postRevisionSummaryAction(Request $request, $quotationId, $revisionId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        $content = $request->getContent();

        if (!empty($content)) {
            $summaryData = json_decode($content, true); // 2nd param to get as array
        }

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);
            $revision  = $quotationFacade->getQuotationRevision($quotation, $revisionId);

            $currencyConverter = $this->getCurrencyConverter()->getCurrencyConverterForRevision($revision);

            if (isset($summaryData['custom_sales_price']) && $this->isGranted('ROLE_KLARO_QUOTATION_OVERRIDE_ORDER_PRICE')) {
                $customSalesPrice = $currencyConverter->fromCurrency($summaryData['custom_sales_price']);

                $revision->setTargetSalesPriceWithTax($customSalesPrice);
                $revision->setCurrency(null);
                $quotationFacade->persistQuotationRevision($revision);
            }

            $offeringSummary = $quotationFacade->getConfigurationForRevision($revision);
            $data = ApiHelper::getSummaryDataArray($offeringSummary, $revision, $currencyConverter);

            return $responseFormatter->getSuccessResponse($data);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @Route("/quotation/{quotationId}/revision/{revisionId}/product_list", name="klaro_quotation_api_post_revision_modify_product_list", methods={"PUT"})
     */
    public function modifyProductList(Request $request, $quotationId, $revisionId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        $phaseId = $request->query->get('phaseId');
        $content = json_decode($request->getContent());

        $products = [];

        if (is_object($content) && isset($content->products) && is_array($content->products)) {
            $products = array_map(function ($product) {
                $data = (object) [
                    'source_id' => (int) $product->source_id,
                    'title' => (string) $product->title,
                ];

                return $data;
            }, $content->products);
        }

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);
            $revision  = $quotationFacade->getQuotationRevision($quotation, $revisionId);
            $this->ensureRevisionIsEditableByCurrentUser($quotationFacade, $quotation, $revision);

            $formData = $quotationFacade->getFormDataForRevision($revision);
            $formData->reorderProducts($phaseId, $products);

            $returnNode = $formData->getSinglePhaseByPath($phaseId);

            /** @var ValidationResult $result */
            $result = new ValidationResult();
            /** @var PhaseDataValidatorManager $phaseDataValidationManager */
            $phaseDataValidationManager = $this->getPhaseDataValidationManager();
            $phaseDataValidationManager->validateProductLine($formData, $result);

            $revision->setPhaseData($formData->toArray());
            $revision->setIsValid(false);
            $quotationFacade->persistQuotationRevision($revision);

            $urlParams = [
                'quotationId' => $quotationId,
                'revisionId' => $revisionId,
                'phaseId' => count($products) > 0 ? $returnNode->getPath() : null,
            ];

            return $responseFormatter->getSuccessResponse($urlParams + [
                'url' => $this->generateUrl('klaro_quotation_edit_revision', $urlParams),
            ]);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @Route("/quotation/{quotationId}/revision/{revisionId}/product/product_list", name="klaro_quotation_api_post_revision_add_to_product_list", methods={"POST"})
     */
    public function addProductToProductList(Request $request, $quotationId, $revisionId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        $content = json_decode($request->getContent());

        try {
            if (!is_object($content)) {
                throw new \Exception('Missing request body!');
            }

            if (!isset($content->product)) {
                throw new \Exception('Product object not found in body');
            }

            if (!is_object($content->product)) {
                throw new \Exception('Product must be an object!');
            }

            $productData = $content->product;

            if (!isset($productData->product) || empty($productData->product)) {
                throw new \Exception('Product type missing');
            }

            $phaseId = $request->query->get('phaseId');

            $quotation = $quotationFacade->getQuotation($quotationId);
            $revision  = $quotationFacade->getQuotationRevision($quotation, $revisionId);
            $this->ensureRevisionIsEditableByCurrentUser($quotationFacade, $quotation, $revision);

            $formData = $quotationFacade->getFormDataForRevision($revision);
            $newNode = $formData->addProduct(
                $phaseId,
                $productData->product,
                ['title' => $productData->title]
            );

            // Copy data from existing product
            if (!is_null($productData->source)) {
                /** @var ProductPhaseDataNode $sourceProduct */
                $sourceProduct = $formData->getSinglePhaseByPath($productData->source)->getTopLevelNode();

                $productImporter = $this->get('klaro_quotation.product_importer');
                $productImporter->import($newNode, $sourceProduct);
            }

            /** @var ValidationResult $result */
            $result = new ValidationResult();
            /** @var PhaseDataValidatorManager $phaseDataValidationManager */
            $phaseDataValidationManager = $this->getPhaseDataValidationManager();
            $phaseDataValidationManager->validateProductLine($formData, $result);

            $revision->setPhaseData($formData->toArray());
            $revision->setIsValid(false);
            $quotationFacade->persistQuotationRevision($revision);

            $urlParams = [
                'quotationId' => $quotationId,
                'revisionId' => $revisionId,
                'phaseId' => $newNode ? $newNode->getPath() : null,
            ];

            return $responseFormatter->getSuccessResponse($urlParams + [
                'url' => $this->generateUrl('klaro_quotation_edit_revision', $urlParams),
                'product' => [
                    'id' => $newNode->getId(),
                    'title' => $newNode->getTitle(),
                    'ref' => $newNode->getProductRef(),
                ],
            ]);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @Route("/quotation/{quotationId}/revision/{revisionId}/product/import", name="klaro_quotation_api_post_revision_import_product_data", methods={"PUT"})
     */
    public function importProductData(Request $request, $quotationId, $revisionId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        $content = json_decode($request->getContent());

        try {
            if (!is_object($content)) {
                throw new \Exception('Missing request body!');
            }

            if (!isset($content->source) || empty($content->source)) {
                throw new \Exception('Missing source');
            }

            $quotation = $quotationFacade->getQuotation($quotationId);
            $revision  = $quotationFacade->getQuotationRevision($quotation, $revisionId);
            $this->ensureRevisionIsEditableByCurrentUser($quotationFacade, $quotation, $revision);

            $formData = $quotationFacade->getFormDataForRevision($revision);

            $phaseId = $request->query->get('phaseId');

            /** @var ProductPhaseDataNode $targetProduct */
            $targetProduct = $formData->getSinglePhaseByPath($phaseId)->getTopLevelNode();

            /** @var ProductPhaseDataNode $sourceProduct */
            $sourceProduct = $formData->getSinglePhaseByPath($content->source)->getTopLevelNode();

            $productImporter = $this->get('klaro_quotation.product_importer');
            $productImporter->import($targetProduct, $sourceProduct);

            $revision->setPhaseData($formData->toArray());
            $quotationFacade->persistQuotationRevision($revision);

            return $responseFormatter->getSuccessResponse(null, Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param array                      $revisionData
     * @param bool                       $overrideStatus
     */
    private function updateRevisionData(
        QuotationRevisionInterface $revision,
        $revisionData = [],
        $overrideStatus = false
    ) {
        // Set title
        if (isset($revisionData['title'])) {
            $revision->setTitle((string) $revisionData['title']);
        }

        // Set qualifiers from request.
        if (isset($revisionData['qualifiers'])) {
            $qualifierOptions = $this->getRevisionQualifierOptions();

            foreach ($revisionData['qualifiers'] as $qualifier => $qualifierValue) {
                if (isset($qualifierOptions[$qualifier]) && isset($qualifierOptions[$qualifier][$qualifierValue])) {
                    $revision->setQualifier($qualifier, $qualifierValue);
                }
            }
        }

        // Set status & order status from request.
        $status = $revisionData['status'] ?? null;
        if (null !== $status) {
            $stateMachine = null;

            if ($overrideStatus) {
                $revision->setStatus($status);
            } else {
                $stateMachine = RevisionStateMachine::create($revision);
                $stateMachine->apply($status);
            }

            $orderStatus = $revisionData['order_status'] ?? null;
            if (null !== $orderStatus) {
                if ($stateMachine && $stateMachine->hasOrderStatus()) {
                    $stateMachine->setOrderStatus($orderStatus);
                } elseif ($overrideStatus) {
                    $revision->setOrderStatus($orderStatus);
                }
            }
        }

        // Set log message.
        if (isset($revisionData['log_message'])) {
            $revision->setLogMessage($revisionData['log_message']);
        }

        $this->getModelProvider()->persistQuotationRevision($revision);
    }

    /**
     * Helper to get revision data as array.
     *
     * @param QuotationRevisionInterface $revision
     * @param bool                       $showState
     *
     * @return array
     */
    private function getRevisionDataArray(QuotationRevisionInterface $revision, $showState = false)
    {
        $data = ApiHelper::getRevisionDataArray($revision);

        if ($showState) {
            $stateMachine = RevisionStateMachine::create($revision);

            $data += [
                'editable'  => $stateMachine->isEditable(),
                'printable' => $stateMachine->isPrintable(),
                'won'       => $stateMachine->isWon(),
                'final'     => $stateMachine->isFinal(),
                'order'     => $stateMachine->isOrder(),
                'latest'    => $stateMachine->isLatest(),
                'states'    => $stateMachine->getAllowedStates(),
                'order_states' => $stateMachine->getAllowedOrderStates(),
            ];
        }

        return $data;
    }

    /**
     * @param QuotationFacade $quotationFacade
     * @param QuotationInterface $quotation
     * @param QuotationRevisionInterface $revision
     * @return void
     * @throws \Exception
     */
    public function ensureRevisionIsEditableByCurrentUser(QuotationFacade $quotationFacade, QuotationInterface $quotation, QuotationRevisionInterface $revision): void
    {
        $currentUser = $quotationFacade->getCurrentUser();
        $this->ensureUserHasAccessToQuotation($quotation, $currentUser);
        // Prevent editing of other than latest non-draft revision
        $stateMachine = RevisionStateMachine::create($revision);
        if (!$stateMachine->isEditable()) {
            throw new \Exception('Only the latest revision can be edited!');
        }
    }
}
