<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Controller\WebApi;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\Common\Query\QueryCriteria;
use Klaro\Component\QuotationSearch\Query\QuotationQueryFields;
use Klaro\Component\QuotationSearch\Query\QuotationQuery;
use Klaro\Component\QuotationSearch\Query\RevisionQueryFields;
use Klaro\Component\QuotationSearch\Query\UserQueryFields;
use Klaro\QuotationBundle\Library\ApiHelper;
use Klaro\QuotationBundle\Library\Helper;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Klaro\QuotationBundle\Traits\QuotationParameterTraits;
use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Klaro\QuotationBundle\Traits\QuotationValidationTraits;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Api controller methods related to quotations.
 *
 * Class QuotationApiController
 * @package Klaro\QuotationBundle\Controller\WebApi
 */
class QuotationApiController extends Controller
{
    use QuotationServiceTraits, QuotationParameterTraits, QuotationValidationTraits;

    /**
     * Get list of quotation types.
     *
     * @Route("/quotation_types", name="klaro_quotation_api_get_quotation_types", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getQuotationTypesAction()
    {
        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $data = $this->getProductDefinitionManager()->getQuotationTypes();

            return $responseFormatter->getSuccessResponse($data);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * List quotations.
     *
     * @Route("/quotations/{quotationType}", name="klaro_quotation_api_get_quotations", methods={"GET"})
     *
     * @param Request       $request
     * @param $quotationType
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getQuotationsAction(Request $request, $quotationType)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $this->ensureQuotationTypeIsDefined($quotationType);

            $searchParams = [
                QueryCriteria::EQUALS => [
                    QuotationQueryFields::HANDLER => 'userId',
                ],
                QueryCriteria::BETWEEN => [
                    QuotationQueryFields::UPDATED_AT => ['updated_start', 'updated_end'],
                    QuotationQueryFields::CREATED_AT => ['created_start', 'created_end'],
                ],
            ];

            $sortableFields = [
                QuotationQueryFields::OWNER      => 'owner',
                QuotationQueryFields::CREATED_AT => 'created_at',
                QuotationQueryFields::UPDATED_AT => 'updated_at',
            ];

            $query = QuotationQuery::create()
                ->setQuotationType($quotationType)
                ->setQuotationFields(QuotationQueryFields::getAllFields())
                ->setUserFields([
                    UserQueryFields::ID,
                ])
                ->setRevisionFields([
                    RevisionQueryFields::REVISION_ID,
                ]);

            ApiHelper::addQueryConstraints($query, $request, $searchParams, $sortableFields);

            $quotations = $quotationFacade->findQuotations($query);

            $data = $quotations->map(function (QuotationInterface $quotation) {
                return ApiHelper::getQuotationDataArray($quotation);
            });

            return $responseFormatter->getSuccessResponse($data, 200, ApiHelper::getPaginationInfo($query, $quotations));
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Get quotation data.
     *
     * @Route("/quotation/{quotationId}", name="klaro_quotation_api_get_quotation", methods={"GET"})
     *
     * @param $quotationId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getQuotationAction(Request $request, $quotationId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);

            $data = ApiHelper::getQuotationDataArray($quotation);

            $showState = (bool) ($request->query->get('state') === 'true');

            if ($showState) {
                $stateMachine = RevisionStateMachine::create($quotation->getLatestRevision());

                $data += [
                    'won'   => $stateMachine->isWon(),
                    'final' => $stateMachine->isFinal(),
                    'order' => $stateMachine->isOrder(),
                ];
            }

            return $responseFormatter->getSuccessResponse($data);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Save handlers to quotation.
     *
     * @Route("/quotation/{quotationId}/users", name="klaro_quotation_api_post_quotation_users", methods={"POST"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postQuotationUsersAction(Request $request, $quotationId)
    {
        $userIds = $request->request->get('userIds', []);

        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $this->ensureUserHasRole('ROLE_KLARO_QUOTATION_MANAGE_QUOTATION_USERS');

            if (is_array($userIds) && count($userIds) > 0) {
                $quotation = $quotationFacade->getQuotation($quotationId);

                // Add those users which are not already linked to this quotation.
                $linkedUserIds = array_map(function ($user) {
                    /** @var QuotationUserInterface $user */

                    return $user->getId();
                }, $quotationFacade->getLinkedUsers($quotation));

                foreach ($userIds as $userId) {
                    if (!in_array($userId, $linkedUserIds)) {
                        $user = $this->getModelProvider()->getUser($userId);

                        $quotationFacade->linkQuotation($quotation, $user);
                    }
                }
            }

            return $responseFormatter->getSuccessResponse(null, 204);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Get list of handlers.
     *
     * @Route("/quotation/{quotationId}/users", name="klaro_quotation_api_get_quotation_users", methods={"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getQuotationUsersAction(Request $request, $quotationId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $quotation = $quotationFacade->getQuotation($quotationId);

            $userData = [];

            foreach ($quotationFacade->getLinkedUsers($quotation) as $user) {
                $userData[] = ApiHelper::getUserDataArray($user);
            }

            $data = $userData;

            return $responseFormatter->getSuccessResponse($data);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }
}
