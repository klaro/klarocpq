<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Controller\WebApi;

use Klaro\QuotationBundle\Traits\QuotationParameterTraits;
use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Klaro\QuotationBundle\Traits\QuotationValidationTraits;
use stdClass;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Api controller for user info & handling.
 *
 * Class UserApiController
 * @package Klaro\QuotationBundle\Controller\WebApi
 */
class AdminApiController extends Controller
{
    use QuotationServiceTraits, QuotationParameterTraits, QuotationValidationTraits;

    /**
     * @Route("/admin/list", name="klaro_admin_api_get_data", methods={"GET"})
     */
    public function getAction(Request $request) {
        $this->ensureUserHasRole('ROLE_KLARO_QUOTATION_ACCESS_ADMINISTRATION');

        $page = $request->get('page');
        $adminServices = $this->getAdminPageManager();
        $adminPages = $adminServices->getAdminPages();

        $menuArray = [];
        $itemArray = [];
        foreach ($adminPages as $key => $value) {
            $newName = substr($value, strpos($value, '.') + 1);
            $menuTitle = ucwords(str_replace("_", " ", $newName));
            $menuObj = new stdClass();
            $menuObj->title = $menuTitle;
            $menuObj->active = false;
            $menuObj->url = '/' . $key;
            $menuArray[$key] = $menuObj;
        }

        $itemObj = new stdClass();
        $itemObj->text = "";
        $itemObj->fieldName = "";
        $itemObj->value = null;
        $itemObj->disabled = false;
        $itemObj->colWidth = null;
        $itemObj->hideLabel = false;

        $page = isset($adminPages[$page]) ? $page : key($adminPages);

        $menuArray[$page]->active = true;
        $itemObj->type = $page;
        array_push($itemArray, $itemObj);

        $response['menu'] = $menuArray;
        $response['items'] = $itemArray;
        return $this->getApiResponseFormatter()->getSuccessResponse($response);
    }

    /**
     * @Route("/admin/documents/list", name="klaro_admin_api_get_history_list", methods={"GET"})
     */
    public function getDocumentManager(Request $request) {
        $adminServices = $this->getAdminPageManager();
        $adminView = $adminServices->getAdminPage('documentManager');

        return $adminView->renderAdminTemplatesTable(true);
    }

    /**
     * @Route("/admin/documents/history", name="klaro_admin_api_get_history", methods={"GET"})
     */
    public function getHistoryList(Request $request) {
        $groupId = $request->get('group_id', 0);
        $adminServices = $this->getAdminPageManager();
        $adminView = $adminServices->getAdminPage('documentManager');

        return $this->getApiResponseFormatter()->getSuccessResponse($adminView->getHistoryJson($groupId));
    }
}
