<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Controller\WebApi;

use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\Common\Query\QueryCriteria;
use Klaro\Component\QuotationSearch\Query\UserQuery;
use Klaro\Component\QuotationSearch\Query\UserQueryFields;
use Klaro\QuotationBundle\Library\ApiHelper;
use Klaro\QuotationBundle\Traits\QuotationParameterTraits;
use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Klaro\QuotationBundle\Traits\QuotationValidationTraits;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Api controller for user info & handling.
 *
 * Class UserApiController
 * @package Klaro\QuotationBundle\Controller\WebApi
 */
class UserApiController extends Controller
{
    use QuotationServiceTraits, QuotationParameterTraits, QuotationValidationTraits;

    /**
     * @Route("/users", name="klaro_quotation_api_get_users", methods={"GET"})
     */
    public function getUsersAction(Request $request)
    {
        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $this->ensureUserHasRole('ROLE_KLARO_QUOTATION_ACCESS_USERS');

            $query = UserQuery::create()
                ->setUserFields(UserQueryFields::getAllFields());

            $searchParams = [
                QueryCriteria::LIKE => [
                    UserQueryFields::FIRST_NAME => 'first_name',
                    UserQueryFields::LAST_NAME  => 'last_name',
                    UserQueryFields::EMAIL      => 'email',
                ],
            ];

            ApiHelper::addQueryConstraints($query, $request, $searchParams, $searchParams[QueryCriteria::LIKE]);

            $users = $this->getQuotationFacade()->findUsers($query);

            $userData = $users->map(function (QuotationUserInterface $user) {
                return ApiHelper::getUserDataArray($user);
            });

            return $responseFormatter->getSuccessResponse($userData, 200, ApiHelper::getPaginationInfo($query, $users));
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }
}
