<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Controller;

use Klaro\Component\Common\Event\QuotationControllerEvents;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\Common\Query\QueryCriteria;
use Klaro\Component\Configurator\ConfiguratorManagerInterface;
use Klaro\Component\DataTable\DataTable;
use Klaro\Component\ProductManager\ProductManagerInterface;
use Klaro\Component\QuotationSearch\Query\QuotationQuery;
use Klaro\Component\QuotationSearch\Query\QuotationQueryFields;
use Klaro\Component\QuotationSearch\Query\RevisionQueryFields;
use Klaro\Component\QuotationSearch\Query\UserQueryFields;
use Klaro\QuotationBundle\Api\QuotationRevisionStatus;
use Klaro\QuotationBundle\Event\QuotationEvent;
use Klaro\QuotationBundle\Event\QuotationRevisionEvent;
use Klaro\QuotationBundle\Event\QuotationUserEvent;
use Klaro\QuotationBundle\Library\ApiHelper;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Klaro\QuotationBundle\Traits\QuotationParameterTraits;
use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Klaro\QuotationBundle\Traits\QuotationValidationTraits;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller routes for quotation handling.
 *
 * Class QuotationController
 * @package Klaro\QuotationBundle\Controller
 */
class QuotationController extends Controller
{
    use QuotationServiceTraits, QuotationParameterTraits, QuotationValidationTraits;

    // default mapping
    protected $fieldMapping = [
        'Proposal Type' => 'proposalType:type',
        'Owner' => 'CONCAT(o.firstName, \' \', o.lastName)',
    ];

    /**
     * Route handler for proposals front page.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $quotationTypeNames = $this->getQuotationTypeNames();
        $quotationTypes = [];

        $productDefinitionManager = $this->getProductDefinitionManager();

        foreach ($quotationTypeNames as $quotationType => $quotationTypeName) {
            $quotationTypes[$quotationType] = $productDefinitionManager->getProductLineDefinitionForQuotationType($quotationType);
        }

        $event = new QuotationEvent(null, array(
            'quotationTypes' => $quotationTypes,
        ));

        $this->logInfo('Quotation index', $event->toArray());

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_INDEX, $event);

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        if ($request->query->has('json')) {
            return $this->getApiResponseFormatter()->getSuccessResponse($event->getData()['quotationTypes']);
        }

        return $this->renderIndex($event->toArray());
    }

    /**
     * Route handler for proposal listing.
     *
     * @param Request       $request
     * @param $quotationType
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function listAction(Request $request, $quotationType)
    {
        $this->ensureQuotationTypeIsDefined($quotationType);

        $owner = $request->query->get('owner', 'user');

        $dataTable = $this->defaultDataColumns();
        // Pass the data along so subscribers can modify the response if needed.
        $event = new QuotationEvent(null, array(
            'quotationType'    => $quotationType,
            'dataTable'        => $dataTable,
            'owner'            => $owner
        ));
        $this->logInfo('Listing quotations', $event->toArray());
        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_LIST, $event);
        $headers = [];
        foreach ($dataTable->getColumns() as $column) {
            $headerItem = $column->getHeaderItem();
            $headers[] = $headerItem->getText();
        }

        return $this->render('KlaroQuotationBundle:Quotation:list.html.twig',
            [
                'quotationType' => $quotationType,
                'owner' => $owner,
                'headers' => $headers,
            ]);
    }

    /**
     * Route handler for proposal info.
     *
     * @param $quotationId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function infoAction(Request $request, $quotationId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $quotation = $quotationFacade->getQuotation($quotationId);
        $this->ensureQuotationTypeIsDefined($quotation->getQuotationType());

        // Make sure a revision exists.
        $revision = $quotation->getLatestRevision();
        if (!($revision instanceof QuotationRevisionInterface)) {
            $revision = $quotationFacade->createQuotationRevision($quotation, $quotationFacade->getCurrentUser());
        }

        $currencyConverter = $this->getCurrencyConverter();
        $stateMachine = RevisionStateMachine::create($quotation->getLatestRevision());

        $event = new QuotationRevisionEvent($revision, [
            'quotationType' => $quotation->getQuotationType(),
            'currencyConverter' => $currencyConverter,
            'configuredProductLine' => $this->getProductDefinitionManager()->getProductLineDefinitionForRevision($revision),
            'revisions' => $this->getModelProvider()->getQuotationRevisions($quotation),
            'stateMachine' => $stateMachine,
        ]);

        $this->logInfo('Quotation info', $event->toArray());

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_INFO, $event);

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        if ($request->query->has('json')) {
            $data = $event->toArray();

            $users = [];
            foreach ($quotationFacade->getLinkedUsers($quotation) as $user) {
                $users[] = ApiHelper::getUserDataArray($user);
            }
            $userRolesBase = array_map(function($a) { return new Role($a); }, $this->getUser()->getRoles());
            $userRoleHierarchy = $this->get('security.role_hierarchy')->getReachableRoles($userRolesBase);
            $userRoles = array_map(function($z) { return $z->getRole(); }, $userRoleHierarchy);

            $quotationManager = $this->getModelProvider();
            $isLinked = $quotationManager->isLinked($quotation, $quotationFacade->getCurrentUser());
            $isFinal = $stateMachine->isFinal();
            $data['users'] = $users;

            /** @var TranslatorInterface $translator */
            $translator = $this->get('translator');
            $value = $translator->trans('info.status', [], 'KlaroQuotationBundle');
            $value = str_ireplace("%createdAt%", $data['quotation']->createdAt->format('Y-m-d H:m'), $value);
            $value = str_ireplace("%owner%", $data['quotation']->owner->getEmail(), $value);
            $logo = [
                "text" => "KlaroCPQ",
                "fieldName" => "",
                "type" => "logo",
                "class" => "",
                "icon" => "",
                "modal" => false,
                "url" => "/bundles/klarocpq/images/klarocpq.png",
                "colWidth" => ["xs" => 12, "sm" => 12, "md" => 4, "lg" => 3],
                "disabled" => false,
                "value" => $value,
                ];

            $titleTable = [
                "text" => "",
                "fieldName" => "",
                "type" => "titleTable",
                "class" => "",
                "icon" => "",
                "modal" => false,
                "url" => "",
                "value" => [
                    "title" =>
                    [
                        "text" => $translator->trans('info.table.title', [], 'KlaroQuotationBundle'),
                        "value" => $data['quotation']->identifier . ' ' . $data['quotation']->title,
                    ],
                    "productLine" => [
                        "text" => $translator->trans('info.table.product_line', [], 'KlaroQuotationBundle'),
                        "value" => $this->getProductDefinitionManager()->getProductLineDefinitionForRevision($revision)->getTitle(),
                    ],
                    "owner" => [
                        "text" => $translator->trans('info.table.owner', [], 'KlaroQuotationBundle'),
                        "value" => $data['quotation']->owner->getEmail(),
                    ],
                    "handlers" => [
                        "text" => $translator->trans('info.table.handlers', [], 'KlaroQuotationBundle'),
                        "value" => "",
                        "items" => [
                            "0" => [
                                "text" => $translator->trans('info.table.add_handler', [], 'KlaroQuotationBundle'),
                                "fieldName" => "",
                                "type" => "buttonHandlers",
                                "class" => "btn-success",
                                "icon" => "plus",
                                "modal" => true,
                                "url" => false,
                                "value" => "",
                                "disabled" => false,
                            ],
                        ]
                    ],
                    "createdAt" => [
                        "text" => $translator->trans('info.table.created', [], 'KlaroQuotationBundle'),
                        "value" => $data['quotation']->createdAt ? $data['quotation']->createdAt->format('Y-m-d H:m') : '',
                    ],
                    "updatedAt" => [
                        "text" => $translator->trans('info.table.updated', [], 'KlaroQuotationBundle'),
                        "value" => $data['quotation']->updatedAt ? $data['quotation']->updatedAt->format('Y-m-d H:m') : '',
                    ],
                ],
                "disabled" => false,
                "colWidth" => ["xs" => 12, "sm" => 12, "md" => 8, "lg" => 9],
                ];
            $buttons = [];

            if ($isLinked) {
                $buttons[] = [
                    "text" => $translator->trans('info.buttons.create_revision.title', [], 'KlaroQuotationBundle'),
                    "fieldName" => "",
                    "type" => "buttonNew",
                    "class" => "btn-success",
                    "icon" => "plus",
                    "modal" => true,
                    "url" => false,
                    "value" => $data['revision']->revisionId,
                    "disabled" => $isFinal,
                ];

                $buttons[] = [
                    "text" => $translator->trans('info.buttons.edit.title', [], 'KlaroQuotationBundle'),
                    "fieldName" => "",
                    "type" => "buttonEdit",
                    "class" => "btn-primary",
                    "icon" => "pencil",
                    "modal" => false,
                    "url" => true,
                    "value" => $data['revision']->revisionId,
                    "disabled" => false,
                ];
            } elseif (!$isLinked && in_array('ROLE_KLARO_QUOTATION_LINK_QUOTATIONS', $userRoles)) {
                $buttons[] = [
                    "text" => $translator->trans('info.buttons.link.title', [], 'KlaroQuotationBundle'),
                    "fieldName" => "",
                    "type" => "buttonLink",
                    "class" => "btn-primary",
                    "icon" => "link",
                    "modal" => false,
                    "url" => false,
                    "value" => "link",
                    "disabled" => false,
                ];
            }

            if (in_array('ROLE_KLARO_QUOTATION_CLAIM_QUOTATIONS', $userRoles) && (count($quotationManager->getLinkedUsers($quotation)) > 1 || !$isLinked)) {
                $buttons[] = [
                    "text" => $translator->trans('info.buttons.claim.title', [], 'KlaroQuotationBundle'),
                    "fieldName" => "",
                    "type" => "buttonClaim",
                    "class" => "btn-outline-dark",
                    "icon" => "external-link",
                    "modal" => false,
                    "url" => false,
                    "value" => $data['revision']->revisionId,
                    "disabled" => false,
                ];
            }

            if (in_array('ROLE_KLARO_QUOTATION_CREATE_QUOTATION', $userRoles)) {
                $buttons[] = [
                    "text" => $translator->trans('info.buttons.copy.title', [], 'KlaroQuotationBundle'),
                    "fieldName" => "",
                    "type" => "buttonCopy",
                    "class" => "btn-outline-dark",
                    "icon" => "clone",
                    "modal" => false,
                    "url" => false,
                    "value" => $data['revision']->revisionId,
                    "disabled" => false,
                ];
            }

            if (in_array('ROLE_KLARO_QUOTATION_DELETE_QUOTATIONS', $userRoles)) {
                $buttons[] = [
                    "text" => $translator->trans('info.buttons.delete.title', [], 'KlaroQuotationBundle'),
                    "fieldName" => "",
                    "type" => "buttonDelete",
                    "class" => "btn-danger",
                    "icon" => "trash",
                    "modal" => true,
                    "url" => false,
                    "value" => $data['revision']->revisionId,
                    "disabled" => false,
                ];
            }
            $revisionTableItems = [];
            if($isLinked && !$isFinal){
                if (strpos($revision->getStatus(), 'WON') &&
                    ($revision->getOrderStatus() == null || strpos($revision->getOrderStatus(), 'NONE'))) {
                    $text = $translator->trans('info.buttons.copy_to_order.title', [], 'KlaroQuotationBundle');
                } else {
                    $text = $translator->trans('info.buttons.create_revision.title', [], 'KlaroQuotationBundle');
                }

                $revisionTableItems[] = [
                    "text" => $text,
                    "fieldName" => "",
                    "type" => "buttonNew",
                    "class" => "btn-success",
                    "icon" => "plus",
                    "modal" => true,
                    "url" => false,
                    "value" => "",
                    "disabled" => false,
                ];
            }
            for ($i = 0; $i < count($data['revisions']); $i++) {
                $revItem[$i] = $data['revisions'][$i]->toArray();
                $revItem[$i]['items'] = $revisionTableItems;
                $revItem[$i]['price'] = array_key_exists('CalculatedSalesPriceWithTax', $revItem[$i]['metadatas']) ? $revItem[$i]['metadatas']['CalculatedSalesPriceWithTax'] : '';
                $revItem[$i]['author'] = $data['revisions'][0]->getUser() ? $data['revisions'][0]->getUser()->fullName : '-';
                for ($y = 0; $y < count($revisionTableItems); $y++) {
                    $revItem[$i]['items'][$y]['value'] = $revItem[$i]['revisionId'];
                }
            }
            $revisionTable = [
                "text" => "",
                "fieldName" => "",
                "type" => "revisionTable",
                "class" => "",
                "icon" => "none",
                "modal" => false,
                "url" => false,
                "value" => "",
                "disabled" => false,
                "colWidth" => ["xs" => 12, "sm" => 12, "md" => 12, "lg" => 12],
                "columns" => [
                    [
                        "field" => "identifier",
                        "header" => $translator->trans('revision.title', [], 'KlaroQuotationBundle')
                    ],
                    [
                        "field" => "status",
                        "header" =>$translator->trans('revision.status', [], 'KlaroQuotationBundle')
                    ],
                    [
                        "field" => "price",
                        "header" => $translator->trans('revision.sales_price', [], 'KlaroQuotationBundle')
                    ],
                    [
                        "field" => "author",
                        "header" => $translator->trans('revision.author', [], 'KlaroQuotationBundle')
                    ],
                    [
                        "field" => "createdAt",
                        "header" => $translator->trans('revision.created_at', [], 'KlaroQuotationBundle')
                    ],
                    [
                        "field" => "updatedAt",
                        "header" => $translator->trans('revision.updated_at', [], 'KlaroQuotationBundle')
                    ],
                ],
                "items" => $revItem,
            ];
            if (count($revisionTableItems) > 0) {
                $newCol = [
                    "field" => "items",
                    "header" => ""
                ];
                array_push($revisionTable['columns'], $newCol);
            }

            $data['items'][0] = [];
            $data['items'][1] = [];
            array_push($data['items'][0], $logo);
            array_push($data['items'][0], $titleTable);
            array_push($data['items'][0], $buttons);
            array_push($data['items'][1], $revisionTable);
            $data['model'] = [];
            return $this->getApiResponseFormatter()->getSuccessResponse($data);
        }

        return $this->renderInfo($event->toArray());
    }

    /**
     * Route handler for creating a new proposal.
     *
     * @param $quotationType
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createAction($quotationType)
    {
        $quotationFacade = $this->getQuotationFacade();

        $revision  = $quotationFacade->createQuotation($quotationFacade->getCurrentUser(), $quotationType);
        $quotation = $revision->getQuotation();

        $this->ensureUserHasRole('ROLE_KLARO_QUOTATION_CREATE_QUOTATION');

        $event = new QuotationRevisionEvent($revision, [
            'quotationType' => $quotationType,
        ]);

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_CREATE, $event);

        $this->logInfo('Create quotation', $event->toArray());

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        return $this->redirect($this->generateUrl('klaro_quotation_info', [
            'quotationId' => $quotation->getId(),
        ]));
    }

    /**
     * Route handler for creating a new revision.
     *
     * @param Request     $request
     * @param $quotationId
     * @param null        $revisionId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createRevisionAction(Request $request, $quotationId, $revisionId = null)
    {
        $quotationFacade = $this->getQuotationFacade();

        $quotation = $quotationFacade->getQuotation($quotationId);
        $currentUser = $quotationFacade->getCurrentUser();

        $this->ensureUserHasAccessToQuotation($quotation, $currentUser);

        $revision = $quotationFacade->createQuotationRevision($quotation, $currentUser, $revisionId);

        // Set status & order status from request.
        $status = $request->query->get('status');

        $doUpdate = false;

        if (!empty($status)) {
            $revision->setStatus($status);
            $doUpdate = true;
        }

        $orderStatus = $request->query->get('order_status');

        if (!empty($orderStatus)) {
            $revision->setOrderStatus($revision->getStatus() == QuotationRevisionStatus::WON ? $orderStatus : null);
            $doUpdate = true;
        }

        if ($doUpdate === true) {
            $this->getModelProvider()->persistQuotationRevision($revision);
        }

        $event = new QuotationRevisionEvent($revision);

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::REVISION_CREATE, $event);

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        return $this->redirect($this->generateUrl('klaro_quotation_edit_revision', [
            'quotationId' => $quotation->getId(),
            'revisionId'  => $revision->getRevisionId(),
        ]));
    }

    /**
     * Route handler for editing a proposal phase.
     *
     * @param Request     $request
     * @param $quotationId
     * @param $revisionId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function editAction(Request $request, $quotationId, $revisionId = null)
    {
        $quotationFacade = $this->getQuotationFacade();
        $quotation = $quotationFacade->getQuotation($quotationId);

        // Load the given or default phase
        $quotationType = $quotation->getQuotationType();

        $this->ensureQuotationTypeIsDefined($quotationType);
        $this->ensureUserHasAccessToQuotation($quotation, $quotationFacade->getCurrentUser());

        if (is_null($revisionId)) {
            return $this->redirect(sprintf("%s%s",
                $this->generateUrl('klaro_quotation_edit_revision', [
                    'quotationId' => $quotationId,
                    'revisionId' => $quotation->getLatestRevision()->getRevisionId(),
                ]),
                $request->query->has('json') ? '?json' : ''));
        } else {
            $revision = $quotationFacade->getQuotationRevision($quotation, $revisionId);
        }

        $stateMachine = RevisionStateMachine::create($revision);

        $phasePath = $request->query->get('phaseId');

        // Allow to force reloading of the phase definition in dev mode.
        $refresh = false;
        if ($this->container->getParameter('kernel.environment') == 'dev') {
            $refresh = (bool) ($request->query->get('reload') === '1');
        }

        $fetchMode = $refresh ? ProductManagerInterface::FETCH_FROM_SOURCE : ProductManagerInterface::FETCH_CACHED;

        $productLine = $quotationFacade->getFormDataForRevision($revision, $fetchMode, $refresh);
        $editablePhase = $productLine->getSinglePhaseByPath($phasePath);
        $mainPhase = $editablePhase->getTopLevelNode();

        $this->ensurePhaseAccessibility($editablePhase->getPhaseNode());

        $editor = $this->getPhaseEditorManager()->createEditor($revision, $productLine, $phasePath);

        $importerSources = $this->get('klaro_quotation.product_importer')->getImporterMappings($productLine);

        $event = new QuotationRevisionEvent($revision, [
            'productLine' => $productLine->getPhaseNode(),
            'phase' => $mainPhase,
            'editablePhase' => $editablePhase,
            'editor' => $editor,
            'sidebarPages' => $this->getSidebarForPhase($phasePath),
            'stateMachine' => $stateMachine,
            'importerSources' => $importerSources,
        ]);

        $dispatcher = $this->getEventDispatcher();
        $dispatcher->dispatch(QuotationControllerEvents::QUOTATION_EDIT_CREATE_PHASE, $event);

        $this->logInfo('Edit quotation phase', $event->toArray());

        // Send event before saving the form
        if ($request->getMethod() === 'POST') {
            // Prevent editing of other than latest non-draft revision
            if (!$stateMachine->isEditable()) {
                throw new \Exception('Only the latest revision can be edited!');
            }

            $customResponse = $editor->processUserInput($request);

            $this->logInfo('Saving proposal phase', $event->toArray());

            $dispatcher->dispatch(QuotationControllerEvents::QUOTATION_EDIT_SAVE, $event);

            if ($event->hasResponse()) {
                return $event->getResponse();
            }

            if ($customResponse instanceof Response) {
                return $customResponse;
            }

            // Post processing processes the form errors.
            $editor->postProcess();

            if ($request->query->has('json')) {
                $response = $editor->getJson($event->getData('helpTexts'));

                if ($mainPhase->hasChildren()) {
                    foreach ($mainPhase->getChildren() as $child) {
                        $response['tabs'][] = [
                            'id' => $child->getId(),
                            'title' => $child->getPhaseNode()->getTitle(),
                            'active' => $editablePhase === $child,
                        ];
                    }
                }

                if (!$request->query->has('menu') || $request->query->get('menu')) {
                    $response['menu'] = $this->getMenuExtension()->collectMenuData($revision, $productLine, true);
                    $response['summaries'] = $this->getMenuExtension()->collectSummariesData($revision);
                }

                return $this->getApiResponseFormatter()->getSuccessResponse($response);
            } else {
                return new Response($editor->render());
            }
        }

        // Post processing processes the form errors.
        $editor->postProcess();

        // Otherwise render the full page.
        $dispatcher->dispatch(QuotationControllerEvents::QUOTATION_EDIT, $event);

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        if ($request->query->has('json')) {
            $response = $editor->getJson($event->getData('helpTexts'));

            if ($mainPhase->hasChildren()) {
                foreach ($mainPhase->getChildren() as $child) {
                    $response['tabs'][] = [
                        'id' => $child->getId(),
                        'title' => $child->getPhaseNode()->getTitle(),
                        'active' => $editablePhase === $child,
                    ];
                }
            }

            if (!$request->query->has('menu') || $request->query->get('menu')) {
                $response['menu'] = $this->getMenuExtension()->collectMenuData($revision, $productLine, true);
                $response['summaries'] = $this->getMenuExtension()->collectSummariesData($revision);
            }

            return $this->getApiResponseFormatter()->getSuccessResponse($response);
        }

        return $this->renderEdit($event->toArray());
    }

    /**
     * Route handler for linking proposal to current user's account.
     *
     * @param $quotationId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function linkAction($quotationId)
    {
        $quotationFacade = $this->getQuotationFacade();
        $quotation = $quotationFacade->getQuotation($quotationId);

        $user = $quotationFacade->getCurrentUser();

        $this->ensureQuotationTypeIsDefined($quotation->getQuotationType());
        $this->ensureUserHasRole('ROLE_KLARO_QUOTATION_LINK_QUOTATIONS');
        $this->ensureQuotationIsNotOwnedByUser($quotation, $user);

        $quotationFacade->linkQuotation($quotation, $user);

        $event = new QuotationUserEvent($quotation, $user);

        $this->logInfo('Link quotation', $event->toArray());

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_LINK, $event);

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        return $this->redirect($this->generateUrl('klaro_quotation_info', array(
            'quotationId' => $quotation->getId(),
        )));
    }

    /**
     * Route handler for claiming a quotation. Claiming links a quotation to current user and drops other handlers.
     *
     * @param $quotationId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function claimAction($quotationId)
    {
        $quotationFacade = $this->getQuotationFacade();
        $quotation = $quotationFacade->getQuotation($quotationId);

        $user = $quotationFacade->getCurrentUser();

        $this->ensureQuotationTypeIsDefined($quotation->getQuotationType());
        $this->ensureUserHasRole('ROLE_KLARO_QUOTATION_CLAIM_QUOTATIONS');

        $quotationFacade->claimQuotation($quotation, $user);

        $event = new QuotationUserEvent($quotation, $user);

        $this->logInfo('Claim quotation', $event->toArray());

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_CLAIM, $event);

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        return $this->redirect($this->generateUrl('klaro_quotation_info', array(
            'quotationId' => $quotation->getId(),
        )));
    }

    /**
     * Route handler for copying proposal and associated phases to new proposal and linking it to current user.
     *
     * @param $quotationId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function copyAction($quotationId)
    {
        $quotationFacade = $this->getQuotationFacade();
        $quotation = $quotationFacade->getQuotation($quotationId);

        $this->ensureQuotationTypeIsDefined($quotation->getQuotationType());

        $user = $quotationFacade->getCurrentUser();

        $newQuotation = $quotationFacade->copyQuotation($quotation, $user);

        $event = new QuotationUserEvent($newQuotation, $user, [
            'previous' => $quotation,
        ]);

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_COPY, $event);

        $this->logInfo('Copy proposal', $event->toArray());

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        return $this->redirect($this->generateUrl('klaro_quotation_info', array(
            'quotationId' => $newQuotation->getId(),
        )));
    }

    /**
     * Route handler for deleting a proposal, it's associated phase data and removing links to it from users' accounts.
     *
     * @param $quotationId
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($quotationId)
    {
        $quotationFacade = $this->getQuotationFacade();
        $quotation = $quotationFacade->getQuotation($quotationId);

        $quotationType = $quotation->getQuotationType();

        $this->ensureQuotationTypeIsDefined($quotationType);
        $this->ensureUserHasRole('ROLE_KLARO_QUOTATION_DELETE_QUOTATIONS');

        $event = new QuotationEvent($quotation, [
            'quotationId' => $quotationId,
        ]);

        $quotationFacade->removeQuotation($quotation);

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_REMOVE, $event);

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        return $this->redirect($this->generateUrl('klaro_quotation_list', array(
            'quotationType' => $quotationType,
        )));
    }

    /**
     * Route handler for proposal summary page.
     *
     * @param $quotationId
     * @param $revisionId
     * @param $subPage
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function summaryAction(Request $request, $quotationId, $revisionId, $subPage)
    {
        $quotationFacade = $this->getQuotationFacade();

        $quotation = $quotationFacade->getQuotation($quotationId);

        if (is_null($revisionId)) {
            $revision = $quotation->getLatestRevision();
        } else {
            $revision = $quotationFacade->getQuotationRevision($quotation, $revisionId);
        }

        $quotationType = $quotation->getQuotationType();

        $this->ensureQuotationTypeIsDefined($quotationType);
        $this->ensureUserHasAccessToQuotation($quotation, $quotationFacade->getCurrentUser());

        // Check that summary is allowed for this product line.
        $summaryPages = $this->getPhaseSummaries($quotationType);

        // If no page given, use the first key of product line summaries as the default.
        if (is_null($subPage) && count($summaryPages) > 0) {
            reset($summaryPages);
            $subPage = key($summaryPages);
        }

        if (!array_key_exists($subPage, $summaryPages)) {
            throw new \Exception('Unknown summary type.');
        }

        // Allow to force reloading of the phase definition in dev mode.
        $refresh = false;

        if ($this->container->getParameter('kernel.environment') == 'dev') {
            $refresh = (bool) ($request->query->get('reload') === '1');
        }

        // Get offering summary here just to reload it, result is not used.
        if ($refresh) {
            $quotationFacade->getConfigurationForRevision(
                $revision,
                ConfiguratorManagerInterface::FETCH_FROM_SOURCE,
                $refresh
            );
        }

        $validationResult = $this->getValidator()->validate($revision);
        $outputDocuments = [];
        $summaryPage = null;

        if (!$validationResult->hasViolations()) {
            $summaryPageManager = $this->getSummaryPageManager();

            $summaryPage = $summaryPageManager->getSummaryPage($revision, $subPage);

            $outputDocumentManager = $this->getOutputDocumentManager();
            $outputDocuments = $outputDocumentManager->getQuotationDocuments($revision);
        }

        // Send event to allow listeners to add error messages before processing the summary page.
        $event = new QuotationRevisionEvent($revision, array(
            'subPage' => $subPage,
            'validationResult' => $validationResult,
            'outputDocuments' => $outputDocuments,
            'summaryPages' => $summaryPages,
            'summaryPage' => $summaryPage,
            'stateMachine' => RevisionStateMachine::create($revision),
            'request' => $request,
        ));

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_SUMMARY, $event);

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        return $this->renderSummary($event->toArray());
    }

    /**
     * Route handler for sidebar data.
     *
     * @param Request     $request
     * @param $quotationId
     * @param $revisionId
     * @param $type
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function sidebarAction(Request $request, $quotationId, $revisionId, $type)
    {
        $quotationFacade = $this->getQuotationFacade();

        $quotation = $quotationFacade->getQuotation($quotationId);
        $revision  = $quotationFacade->getQuotationRevision($quotation, $revisionId);

        $quotationType = $quotation->getQuotationType();

        $this->ensureQuotationTypeIsDefined($quotationType);
        $this->ensureUserHasAccessToQuotation($quotation, $quotationFacade->getCurrentUser());

        // Check that sidebar config is found.
        $sidebars = $this->getSidebars();

        if (!array_key_exists($type, $sidebars)) {
            throw new \Exception('Unknown sidebar type.');
        }

        $phasePath = $request->query->get('phaseId');
        $subId = null;

        if (preg_match('/lines\/(\d+)?+/', $phasePath, $matches) === 1) {
            $subId = (int) $matches[1];
        }

        // Summary is special so get offering summary structure.
        $configuration = null;
        $currencyConverter = $this->getCurrencyConverter();

        if ($type === 'summary') {
            $configuration = $quotationFacade->getConfigurationForRevision($revision);
        }

        $event = new QuotationRevisionEvent($revision, [
            'configuration' => $configuration,
            'currencyConverter' => $currencyConverter->getCurrencyConverterForRevision($revision),
            'type'          => $type,
            'sidebars'      => $sidebars,
            'phaseId'       => $phasePath,
            'subId'         => $subId,
            'request'       => $request,
        ]);

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_SIDEBAR, $event);

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        if (!isset($sidebars[$type]['template'])) {
            throw new \Exception('No template is defined. Either return a response in the summary event or provide a template.');
        }

        return $this->renderSidebar($sidebars[$type]['template'], $event->toArray());
    }

    /**
     * Render a response for proposals frontpage.
     *
     * @param $options
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderIndex($options)
    {
        return $this->render('KlaroQuotationBundle:Quotation:index.html.twig', $options);
    }

    /**
     * Render a response for proposal list.
     *
     * @param $options
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderList($options)
    {
        return $this->render('KlaroQuotationBundle:Quotation:list.html.twig', $options);
    }

    /**
     * Render response for proposal info.
     *
     * @param $options
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderInfo($options)
    {
        return $this->render('KlaroQuotationBundle:Quotation:info.html.twig', $options);
    }

    /**
     * Render response for proposal phase edit form.
     *
     * @param $options
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderEdit($options)
    {
        return $this->render('KlaroQuotationBundle:Quotation:edit.html.twig', $options);
    }

    /**
     * Render response for proposal summary.
     *
     * @param $options
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function renderSummary($options)
    {
        return $this->render('KlaroQuotationBundle:Quotation:summary.html.twig', $options);
    }

    /**
     * Render sidebar.
     *
     * @param $template
     * @param $options
     *
     * @return Response
     */
    protected function renderSidebar($template, $options)
    {
        return $this->render($template, $options);
    }

    /**
     * Convenience for logging.
     *
     * @param $message
     * @param array   $context
     */
    private function logInfo($message, $context = [])
    {
        $this->getLogger()->info('QuotationController - '.$message, $context);
    }

    /**
     * @param Request $request
     * @param $quotationType
     * @return Response
     */
    public function listJsonAction(Request $request, $quotationType)
    {
        $quotationFacade = $this->getQuotationFacade();

        $owner = $request->query->get('owner', 'user');
        $ownerComparison = QueryCriteria::EQUALS;
        if (isset($owner) && $owner === 'other') {
            $ownerComparison = QueryCriteria::NOT_EQUALS;
        }

        $length = 10;
        if ($request->query->get('length')) {
            $length = $request->query->get('length');
        }

        $start = 0;
        if ($request->query->get('start')) {
            $start = $request->query->get('start');
        }

        $dataTable = $this->defaultDataColumns();
        // Pass the data along so subscribers can modify the response if needed.
        $event = new QuotationEvent(null, array(
            'quotationType'    => $quotationType,
            'dataTable'        => $dataTable,
            'owner'            => $owner,
            'fieldMapping'     => $this->fieldMapping,
        ));
        $this->logInfo('Listing quotations', $event->toArray());
        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_LIST, $event);

        $headers = [];
        foreach ($dataTable->getColumns() as $column) {
            $headers[] = $column->getDataFormat()['id'];
        }

        $query = QuotationQuery::create()
            ->setQuotationType($quotationType)
            ->setHandler($quotationFacade->getCurrentUser()->getId(), $ownerComparison)
            ->setQuotationFields([
                QuotationQueryFields::ID,
                QuotationQueryFields::IDENTIFIER,
                QuotationQueryFields::TITLE,
                QuotationQueryFields::QUOTATION_TYPE,
                QuotationQueryFields::CREATED_AT,
                QuotationQueryFields::UPDATED_AT,
            ])
            ->setRevisionFields([
                RevisionQueryFields::REVISION_ID,
                RevisionQueryFields::QUOTATION_ID,
                RevisionQueryFields::METADATA,
                RevisionQueryFields::STATUS,
                RevisionQueryFields::IDENTIFIER,
                RevisionQueryFields::TITLE,
                RevisionQueryFields::ORDER_STATUS,
                RevisionQueryFields::QUALIFIERS,
            ])
            ->setUserFields([
                UserQueryFields::EMAIL,
                UserQueryFields::FIRST_NAME,
                UserQueryFields::LAST_NAME
            ])
            ->setFirstResult($start)
            ->setMaxResults($length);

        // Individual column filtering
        if ($request->query->get('columns')) {
            for ($i = 0, $len = count($request->query->get('columns')); $i < $len; $i++) {
                $requestColumn = $request->query->get('columns')[$i];
                $str = $requestColumn['search']['value'];

                if ($requestColumn['searchable'] == 'true' && $str !== '') {
                        $text = $headers[$requestColumn['data']];
                    if (array_key_exists($text, $event->getData('fieldMapping'))) {
                        $text = $event->getData('fieldMapping')[$text];
                    }
                    $field = strtok($text, ':');
                    if ($text === $field) {
                        if (method_exists($query, 'set' . $text)) {
                            call_user_func([$query, 'set' . $text], '%' . $str . '%', QueryCriteria::LIKE);
                        } else {
                            $query->addRawCriteria(sprintf('%s LIKE \'%%%s%%\'', $text, $str));
                        }
                    } else {
                        call_user_func([$query, 'set' . ucfirst($field)], ['$.' . strtok(':'), $str], QueryCriteria::JSON_EXTRACT);
                    }
                }
            }
        }
        if ($order = $request->query->get('order')) {
            for ($i = 0, $len = count($request->query->get('order')); $i < $len; $i++) {
                $text = $headers[$order[$i]['column']];
                if (array_key_exists($text, $event->getData('fieldMapping'))) {
                    $query->addSortParam($event->getData('fieldMapping')[$text], strtoupper($order[$i]['dir']));
                } else {
                    $query->addSortParam($text, strtoupper($order[$i]['dir']));
                }
            }
        }
        $quotations = $quotationFacade->findQuotations($query);

        $total = $quotations->getTotalNumberOfResults();
        $totalPages = ceil($total / $length);

        $quotationsArray = [];
        foreach ($quotations as $quotation) {
            $item = $dataTable->processRow($quotation);
            $quotationsArrayItem = [];
            foreach ($dataTable->getColumns() as $key => $column) {
                $quotationsArrayItem[] = $item[$key]->getText();
            }
            $quotationsArray[] = $quotationsArrayItem;
        }

        return new JsonResponse([
                'data' => $quotationsArray,
                'start' => $start,
                'length' => $length,
                'total' => $total,
                'totalPages' => $totalPages,
                "draw" => $request->query->get('draw'),
                "recordsTotal" => $total,
                "recordsFiltered" => $total,
                'headers' => $headers,
            ]
        );
    }

    protected function defaultDataColumns()
    {
        $dataTable = new DataTable();

        /** @var TranslatorInterface $translator */
        $translator = $this->get('translator');

        $iconUrl = $this->get('assets.packages')->getUrl('bundles/klaroquotation/images/icons/edit.png');

        $urlGenerator = function($value, $data) {
            return $this->generateUrl('klaro_quotation_info', [
                'quotationId' => $value
            ]);
        };

        // Date formatting is set in the config.
        $dateFormat = $this->container->getParameter('klaro_quotation.date_format');

        // Build the listing.
        $dataTable->createColumn(QuotationQueryFields::ID, $translator->trans('list.columns.title', [], 'KlaroQuotationBundle'))
            ->setFixed(true)
            ->setDataFormat(array(
                'id' => QuotationQueryFields::IDENTIFIER,
                'icon' => $iconUrl,
                'url' => $urlGenerator,
                'text' => function($id, QuotationInterface $quotation) {
                    return $quotation->getIdentifier() . ' ' . $quotation->getTitle();
                }
            ));

        $dataTable->createColumn(QuotationQueryFields::LATEST_REVISION_ID, $translator->trans('list.columns.latest_revision', [], 'KlaroQuotationBundle'))
            ->setFixed(true)
            ->setDataFormat(array(
                    'id' => RevisionQueryFields::IDENTIFIER,
                    'text' => function(QuotationRevisionInterface $revision) {
                        return $revision->getIdentifier() . ' ' . $revision->getTitle();
                    })
            );

        $dataTable->createColumn(QuotationQueryFields::LATEST_REVISION_ID, $translator->trans('list.columns.status', [], 'KlaroQuotationBundle'))
            ->setFixed(true)
            ->setSortable(true)
            ->setDataFormat(array(
                    'id' => RevisionQueryFields::STATUS,
                    'text' => function(QuotationRevisionInterface $revision) {
                        return $revision->getStatus();
                    })
            );

        $dataTable->createColumn(QuotationQueryFields::OWNER, $translator->trans('list.columns.owner', [], 'KlaroQuotationBundle'))
            ->setSortable(true)
            ->setDataFormat(array(
                    'id' => QuotationQueryFields::OWNER,
                    'text' => function(QuotationUserInterface $owner) {
                        return is_object($owner) ? $owner->getFullname() : '';
                    })
            );

        $dataTable->createColumn(QuotationQueryFields::CREATED_AT, $translator->trans('list.columns.created', [], 'KlaroQuotationBundle'))
            ->setFixed(true)
            ->setSortable(true)
            ->setDataFormat(array(
                    'id' => QuotationQueryFields::CREATED_AT,
                    'text' => function($date) use ($dateFormat) {
                        return $date ? $date->format($dateFormat) : '';
                    })
            );

        $dataTable->createColumn(QuotationQueryFields::UPDATED_AT, $translator->trans('list.columns.updated', [], 'KlaroQuotationBundle'))
            ->setFixed(true)
            ->setSortable(true)
            ->setDataFormat(array(
                    'id' => QuotationQueryFields::UPDATED_AT,
                    'text' => function($date) use ($dateFormat) {
                        return $date ? $date->format($dateFormat) : '';
                    })
            );

        return $dataTable;
    }

    /**
     * Route create a new proposal.
     *
     * @Route("/quotation/create/{quotationType}", name="klaro_quotation_api_post_create_quotation", methods={"POST"})
     *
     * @param $quotationType
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createQuotation($quotationType)
    {
        $quotationFacade = $this->getQuotationFacade();

        $responseFormatter = $this->getApiResponseFormatter();

        try {
            $revision = $quotationFacade->createQuotation($quotationFacade->getCurrentUser(), $quotationType);
            $quotation = $revision->getQuotation();

            $this->ensureUserHasRole('ROLE_KLARO_QUOTATION_CREATE_QUOTATION');

            $event = new QuotationRevisionEvent($revision, [
                'quotationType' => $quotationType
            ]);

            $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_CREATE, $event);

            if ($event->hasResponse()) {
                return $event->getResponse();
            }

            $data = [
                'quotationId' => $quotation->getId(),
            ];

            return $responseFormatter->getSuccessResponse($data);
        } catch (\Exception $e) {
            return $responseFormatter->getErrorResponse($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $quotationType
     * @return Response
     */
    public function listJsonPrimeAction(Request $request, $quotationType) {
        $dataTable = $this->defaultDataColumns();
        $headers = [];
        foreach ($dataTable->getColumns() as $column) {
            $headers[] = $column->getDataFormat()['id'];
        }
        if ($request->query->get('lazyEvent')) {
            $params = json_decode($request->query->get('lazyEvent'));
            $quotationFacade = $this->getQuotationFacade();
            $owner = 'user';
            $owner = $params->owner;
            $ownerComparison = QueryCriteria::EQUALS;
            if (isset($owner) && $owner === 'other') {
                $ownerComparison = QueryCriteria::NOT_EQUALS;
            }

            $length = 10;
            if (property_exists($params, 'rows')) {
                $length = $params->rows;
            }

            $start = 0;
            if (property_exists($params, 'first')) {
                $start = $params->first;
            }


            // Pass the data along so subscribers can modify the response if needed.
            $event = new QuotationEvent(null, array(
                'quotationType' => $quotationType,
                'dataTable' => $dataTable,
                'owner' => $owner,
                'fieldMapping' => $this->fieldMapping,
            ));
            $this->logInfo('Listing quotations', $event->toArray());
            $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_LIST, $event);

            $query = QuotationQuery::create()
                ->setQuotationType($quotationType)
                ->setHandler($quotationFacade->getCurrentUser()->getId(), $ownerComparison)
                ->setQuotationFields([
                    QuotationQueryFields::ID,
                    QuotationQueryFields::IDENTIFIER,
                    QuotationQueryFields::TITLE,
                    QuotationQueryFields::QUOTATION_TYPE,
                    QuotationQueryFields::CREATED_AT,
                    QuotationQueryFields::UPDATED_AT,
                ])
                ->setRevisionFields([
                    RevisionQueryFields::REVISION_ID,
                    RevisionQueryFields::QUOTATION_ID,
                    RevisionQueryFields::METADATA,
                    RevisionQueryFields::STATUS,
                    RevisionQueryFields::IDENTIFIER,
                    RevisionQueryFields::TITLE,
                    RevisionQueryFields::ORDER_STATUS,
                    RevisionQueryFields::QUALIFIERS,
                ])
                ->setUserFields([
                    UserQueryFields::EMAIL,
                    UserQueryFields::FIRST_NAME,
                    UserQueryFields::LAST_NAME
                ])
                ->setFirstResult($start)
                ->setMaxResults($length);

            if (property_exists($params, 'filters')) {
                $filters = $params->filters;
                foreach ($filters as $key => $value) {
                    $str = $filters->$key->value;
                    if ($str !== '') {
                        $text = $key;
                        if (method_exists($query, 'set' . $text)) {
                            call_user_func([$query, 'set' . $text], '%' . $str . '%', QueryCriteria::LIKE);
                        } else {
                            $query->addRawCriteria(sprintf('%s LIKE \'%%%s%%\'', $text, $str));
                        }
                    }
                }
            }

            if (property_exists($params, 'sortField')) {
                if ($params->sortField !== null) {
                    $order = 'asc';
                    if ($params->sortOrder == -1) {
                        $order = 'desc';
                    }
                    if (array_key_exists($params->sortField, $event->getData('fieldMapping'))) {
                        $query->addSortParam($event->getData('fieldMapping')[$params->sortField], strtoupper($order));
                    } else {
                        $query->addSortParam($params->sortField, strtoupper($order));
                    }
                }
            }

            $quotations = $quotationFacade->findQuotations($query);

            $total = $quotations->getTotalNumberOfResults();
            $totalPages = ceil($total / $length);

            $quotationsArray = [];
            foreach ($quotations as $quotation) {
                $item = $dataTable->processRow($quotation);
                $quotationsArrayItem = [];
                foreach ($dataTable->getColumns() as $key => $column) {
                    $quotationsArrayItem[] = $item[$key]->getText();
                }
                $quotationsArray[] = $quotationsArrayItem;
            }
            return new JsonResponse([
                    'data' => $quotationsArray,
                    'start' => $start,
                    'length' => $length,
                    'total' => $total,
                    'totalPages' => $totalPages,
                    "draw" => $request->query->get('draw'),
                    "recordsTotal" => $total,
                    "recordsFiltered" => $total,
                    'headers' => $headers,
                ]
            );
        }
        return new JsonResponse([
                'headers' => $headers,
            ]
        );
    }
}
