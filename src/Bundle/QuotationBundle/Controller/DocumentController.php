<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Controller;

use Klaro\QuotationBundle\Api\OutputDocumentInterface;
use Klaro\Component\Common\Event\QuotationControllerEvents;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Event\QuotationRevisionEvent;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Klaro\QuotationBundle\Traits\QuotationParameterTraits;
use Klaro\QuotationBundle\Traits\QuotationValidationTraits;
use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller routes for document downloads.
 *
 * Class DocumentController
 * @package Klaro\QuotationBundle\Controller
 */
class DocumentController extends Controller
{
    use QuotationServiceTraits, QuotationParameterTraits, QuotationValidationTraits;

    /**
     * Generate a zip package of selected documents.
     *
     * @param Request     $request
     * @param $quotationId
     * @param $revisionId
     *
     * @return Response
     */
    public function generateMultipleAction(Request $request, $quotationId, $revisionId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $quotation = $quotationFacade->getQuotation($quotationId);

        if (is_null($revisionId)) {
            $revision = $quotation->getLatestRevision();
        } else {
            $revision = $quotationFacade->getQuotationRevision($quotation, $revisionId);
        }

        // Prevent download if not printable.
        $stateMachine = RevisionStateMachine::create($revision);

        if (!$stateMachine->isPrintable()) {
            throw new \Exception('Print options not available.');
        }

        //$quotation   = $modelProvider->loadQuotation($quotationId);
        $quotationType = $quotation->getQuotationType();

        $this->ensureQuotationTypeIsDefined($quotationType);
        $this->ensureUserHasAccessToQuotation($quotation, $quotationFacade->getCurrentUser());

        $documentManager = $this->getOutputDocumentManager();

        // If no list is provided in query parameter, zip all available documents.
        $selected = $request->query->get('documents', []);

        $outputDocuments = [];

        foreach ($documentManager->getQuotationDocuments($revision) as $documentId => $definition) {
            if ($definition->isVisible() && !$definition->isDisabled()) {
                $outputDocuments[$documentId] = $definition;
            }
        }

        if (!is_array($selected) || count($selected) < 1) {
            $selected = array_keys($outputDocuments);
        } else {
            $selected = array_intersect($selected, array_keys($outputDocuments));
        }

        $debugMode = (int) $request->query->get('debug', 0) === 1;

        return $this->downloadDocument(
            $revision,
            $documentManager->generate($revision, $selected, $stateMachine->isDraft(), $debugMode)
        );
    }

    /**
     * Generate a single document.
     *
     * @param $quotationId
     * @param $documentId
     *
     * @return Response
     */
    public function generateAction(Request $request, $quotationId, $revisionId, $documentId)
    {
        $quotationFacade = $this->getQuotationFacade();

        $quotation = $quotationFacade->getQuotation($quotationId);

        if (is_null($revisionId)) {
            $revision = $quotation->getLatestRevision();
        } else {
            $revision = $quotationFacade->getQuotationRevision($quotation, $revisionId);
        }

        // Prevent download if not printable.
        $stateMachine = RevisionStateMachine::create($revision);

        if (!$stateMachine->isPrintable()) {
            throw new \Exception('Print options not available.');
        }

        $quotationType = $quotation->getQuotationType();

        $this->ensureQuotationTypeIsDefined($quotationType);
        $this->ensureUserHasAccessToQuotation($quotation, $quotationFacade->getCurrentUser());

        $debugMode = (int) $request->query->get('debug', 0) === 1;

        $documentManager = $this->getOutputDocumentManager();

        // Get print in draft mode if state is printable but still editable.
        $document = $documentManager->generate($revision, $documentId, $stateMachine->isDraft(), $debugMode);

        return $this->downloadDocument($revision, $document);
    }

    /**
     * Return response for downloading a document.
     *
     * @param OutputDocumentInterface $document
     *
     * @return Response
     */
    private function downloadDocument(QuotationRevisionInterface $revision, OutputDocumentInterface $document)
    {
        // Send event to allow overrides to document.
        $event = new QuotationRevisionEvent($revision, array(
            'document' => $document,
        ));

        $this->getEventDispatcher()->dispatch(QuotationControllerEvents::QUOTATION_DOCUMENT, $event);

        $document = $event->getData('document');

        if ($event->hasResponse()) {
            return $event->getResponse();
        }

        $headers = $document->getHeaders() + [
            'Content-Disposition' => 'attachment; filename="'.$document->getFileNameWithExtension().'"',
            'Set-Cookie' => 'fileDownload=true; path=/',
            'Pragma' => 'cache',
            'Cache-Control' => 'private',
        ];

        return new Response($document->getContent(), 200, $headers);
    }
}
