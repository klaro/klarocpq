<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Controller;

use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ConfiguratorController extends Controller
{
    use QuotationServiceTraits;

    public function testAction(Request $request)
    {
        $configurator = $this->getConfigurator();

        $products = $configurator->getAllConfiguratorRefs();
        $configuration = null;
        $ref = null;
        $data = (object) [];

        if ($request->getMethod() === 'POST') {
            $ref = $request->request->get('product');
            $data = json_decode($request->request->get('data'));

            $configuration = $configurator->processConfigurationByRef($ref, $data);
        }

        return $this->render('KlaroQuotationBundle:Configurator:test.html.twig', [
            'product' => $ref,
            'data' => $data,
            'products' => $products,
            'configuration' => $configuration,
        ]);
    }
}
