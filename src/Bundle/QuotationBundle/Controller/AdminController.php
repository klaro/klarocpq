<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Controller;

use Exception;
use Klaro\QuotationBundle\Api\AdminServicesInterface;
use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Klaro\QuotationBundle\Traits\QuotationValidationTraits;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller routes for admin section.
 *
 * Class AdminController
 * @package Klaro\QuotationBundle\Controller
 */
class AdminController extends Controller
{
    use QuotationServiceTraits, QuotationValidationTraits;

    /**
     * Show admin page.
     *
     * @param Request $request
     * @param null $page
     *
     * @return Response
     *
     * @throws Exception
     */
    public function indexAction(Request $request, $page = null) {
        // Check that user has correct rights before rendering the page
        $this->ensureUserHasRole('ROLE_KLARO_QUOTATION_ACCESS_ADMINISTRATION');

        $adminServices = $this->getAdminPageManager();

        $adminView = null;

        $adminPages = $adminServices->getAdminPages();

        if ($page === null) {
            if ($this->container->hasParameter('default_admin_page')) {
                $page = $this->container->getParameter('default_admin_page');
            } else {
                $page = key($adminPages);
            }
        }

        $page = isset($adminPages[$page]) ? $page : key($adminPages);

        $adminView = $adminServices->getAdminPage($page);

        if (!($adminView instanceof AdminServicesInterface)) {
            throw new Exception("Admin page not found!");
        }

        return $this->render('KlaroQuotationBundle:Admin:adminPageBase.html.twig', [
            'page' => $page,
            'renderedContents' => $adminView->render($request),
        ]);
    }

}
