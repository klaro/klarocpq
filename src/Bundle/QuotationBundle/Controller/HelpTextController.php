<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Controller;

use Klaro\QuotationBundle\Subscriber\HelpTextSubscriber;
use Klaro\QuotationExtraBundle\Model\HelpText;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class HelpTextController extends Controller
{
    /**
     * Add or update new help text by identifier (phaseId-fieldName)
     *
     * @Route("/add-or-update-help-text/", name="klaro_help_texts_add_or_update", methods={"POST"})
     * @IsGranted("ROLE_ADMIN")
     *
     * @return JsonResponse
     */
    public function addOrUpdateHelpText(Request $request)
    {
        $application = $request->request->get('application', '');
        $phaseId     = $request->request->get('phaseId', '');
        $fieldname   = $request->request->get('fieldname', '');
        $description = $request->request->get('description', '');

        $phaseId = HelpTextSubscriber::parseActualPhaseId($phaseId);

        try {
            /** @var HelpText $text */
            $text = $this->getDoctrine()
                ->getRepository('Klaro\\Component\\Common\\Model\\HelpTextInterface')
                ->findOneBy([
                    'application' => $application,
                    'phaseId'     => $phaseId,
                    'fieldname'   => $fieldname
                ]);

            if (is_null($text)) {
                $text = new HelpText();

                $text->setApplication($application);
                $text->setPhaseId($phaseId);
                $text->setFieldname($fieldname);
            }

            $text->setDescription($description);

            $this->getDoctrine()->getManager()->persist($text);
            $this->getDoctrine()->getManager()->flush();

            return $this->json($text->toArray());
        } catch(\Exception $e) {
            return $this->json(['error' => $e->getMessage()], 500);
        }
    }

    /**
     * Get array of help texts for phase
     *
     * @Route("/get-phase-help-texts/", name="klaro_help_texts_get_phase", methods={"GET"})
     * @IsGranted("ROLE_USER")
     *
     * @param $identifier
     * @return JsonResponse
     */
    public function getPhaseHelpTexts(Request $request)
    {
        $result = null;

        $application = $request->get('application', '');
        $phaseId     = HelpTextSubscriber::parseActualPhaseId($request->get('phaseId', ''));

        /** @var HelpText[] $texts */
        $texts = $this
            ->getDoctrine()
            ->getRepository('Klaro\\Component\\Common\\Model\\HelpTextInterface')
            ->findHelpTexts($application, $phaseId);

        foreach ($texts as $text) {
            $result[$text->getFieldname()] = $text->toArray();
        }

        return $this->json($result);
    }

    private function getHelpTextClassDefinition()
    {
        return $this->getParameter('klaro_quotation_extra.classes')['help_text'];
    }
}
