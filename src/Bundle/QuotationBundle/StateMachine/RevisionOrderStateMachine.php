<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\StateMachine;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Api\QuotationRevisionOrderStatus;
use Klaro\QuotationBundle\Api\StateMachineInterface;

/**
 * Simple state machine to manage the revision order status after the revision has been won.
 *
 * Class RevisionOrderStateMachine
 * @package Klaro\QuotationBundle\StateMachine
 */
class RevisionOrderStateMachine implements StateMachineInterface
{
    const TYPE_INITIAL = 'initial';
    const TYPE_NORMAL  = 'normal';
    const TYPE_FINAL   = 'final';

    /** @var QuotationRevisionInterface */
    protected $revision;

    /** @var RevisionStateMachine */
    protected $revisionStateMachine;

    protected $states = [
        QuotationRevisionOrderStatus::NONE => [
            'title'     => 'Not Ready',
            'type'      => self::TYPE_INITIAL,
            'editable'  => false,
            'printable' => true,
        ],
        QuotationRevisionOrderStatus::IN_PROCESS => [
            'title'     => 'In Process',
            'type'      => self::TYPE_NORMAL,
            'editable'  => true,
            'printable' => true,
        ],
        QuotationRevisionOrderStatus::REJECTED => [
            'title'     => 'Rejected',
            'type'      => self::TYPE_NORMAL,
            'editable'  => false,
            'printable' => true,
        ],
        QuotationRevisionOrderStatus::FINISHED => [
            'title'     => 'Finished',
            'type'      => self::TYPE_FINAL,
            'editable'  => false,
            'printable' => true,
        ],
    ];

    protected $transitions = [
        QuotationRevisionOrderStatus::IN_PROCESS => [
            'from' => [
                QuotationRevisionOrderStatus::NONE,
                QuotationRevisionOrderStatus::FINISHED,
            ],
            'to' => QuotationRevisionOrderStatus::IN_PROCESS,
        ],
        QuotationRevisionOrderStatus::REJECTED => [
            'from' => [
                QuotationRevisionOrderStatus::IN_PROCESS,
                QuotationRevisionOrderStatus::FINISHED,
            ],
            'to' => QuotationRevisionOrderStatus::REJECTED,
        ],
        QuotationRevisionOrderStatus::FINISHED => [
            'from' => [
                QuotationRevisionOrderStatus::IN_PROCESS,
            ],
            'to' => QuotationRevisionOrderStatus::FINISHED,
        ],
    ];

    /**
     * @param QuotationRevisionInterface $revision
     * @param RevisionStateMachine       $revisionStateMachine
     */
    public function __construct(QuotationRevisionInterface $revision, RevisionStateMachine $revisionStateMachine)
    {
        $this->revision = $revision;
        $this->revisionStateMachine = $revisionStateMachine;
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param RevisionStateMachine       $revisionStateMachine
     *
     * @return RevisionOrderStateMachine
     */
    public static function create(QuotationRevisionInterface $revision, RevisionStateMachine $revisionStateMachine)
    {
        return new self($revision, $revisionStateMachine);
    }

    /**
     * {@inheritDoc}
     */
    public function canApply($state)
    {
        $allowedStates = $this->getAllowedStates();

        return isset($allowedStates[$state]);
    }

    /**
     * {@inheritDoc}
     */
    public function apply($state)
    {
        if ($this->canApply($state)) {
            $this->revision->setOrderStatus($state);
        } else {
            throw new \Exception('Invalid order state!');
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAllowedStates()
    {
        $currentState = $this->getCurrentState();

        $states = array_reduce($this->transitions, function ($carry, $item) use ($currentState) {
            if (in_array($currentState, $item['from'])) {
                $state = $item['to'];

                if ($state == QuotationRevisionOrderStatus::NONE ||
                   $state == QuotationRevisionOrderStatus::IN_PROCESS && $this->revisionStateMachine->isWon() ||
                   $state == QuotationRevisionOrderStatus::REJECTED && $this->revisionStateMachine->isWon() ||
                   $state == QuotationRevisionOrderStatus::FINISHED && $this->revisionStateMachine->isLatest() === true) {
                    $carry[$state] = $this->states[$state]['title'];
                }
            }

            return $carry;
        }, [$currentState => $this->states[$currentState]['title']]);

        return $states;
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrentState()
    {
        $state = $this->revision->getOrderStatus();

        return $this->revisionStateMachine->isWon() && isset($this->states[$state]) ? $state : QuotationRevisionOrderStatus::NONE;
    }

    /**
     * {@inheritDoc}
     */
    public function reset()
    {
        $this->revision->setOrderStatus(QuotationRevisionOrderStatus::NONE);
    }

    /**
     * @return bool
     */
    public function isOrder()
    {
        return $this->isAttributeSet('type', self::TYPE_INITIAL) !== true;
    }

    /**
     * {@inheritDoc}
     */
    public function isEditable()
    {
        return $this->isAttributeSet('editable', true);
    }

    /**
     * {@inheritDoc}
     */
    public function isPrintable()
    {
        return $this->isAttributeSet('printable', true);
    }

    /**
     * {@inheritDoc}
     */
    public function isFinal()
    {
        return $this->isAttributeSet('type', self::TYPE_FINAL);
    }

    /**
     * @param $attribute
     *
     * @return bool
     */
    private function isAttributeSet($attribute, $test)
    {
        $currentState = $this->getCurrentState();

        if (isset($this->states[$currentState])) {
            return $this->states[$currentState][$attribute] === $test;
        }

        return false;
    }
}
