<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\StateMachine;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Api\QuotationRevisionOrderStatus;
use Klaro\QuotationBundle\Api\QuotationRevisionStatus;
use Klaro\QuotationBundle\Api\StateMachineInterface;

/**
 * Simple state machine to handle transitions between revision statuses. After the revision
 * is ready for order, the order status is also taken into account.
 *
 * Class RevisionStateMachine
 * @package Klaro\QuotationBundle\StateMachine
 */
class RevisionStateMachine implements StateMachineInterface
{
    const TYPE_INITIAL = 'initial';     // Initial state (start of graph)
    const TYPE_NORMAL  = 'normal';      // Normal state (graph node)
    const TYPE_FINAL   = 'final';       // Final state (end of graph)

    // Define which actions are possible in each state.
    protected $states = [
        QuotationRevisionStatus::IN_PROCESS => [
            'title'     => 'In Process',
            'type'      => self::TYPE_INITIAL,
            'editable'  => true,        // Is editor editable?
            'printable' => true,        // Is quotation printable
            'order'     => false,        // Is revision ready for order.
        ],
        QuotationRevisionStatus::ACTIVE => [
            'title'     => 'Active',
            'type'      => self::TYPE_NORMAL,
            'editable'  => false,
            'printable' => true,
            'order'     => false,
        ],
        QuotationRevisionStatus::WON => [
            'title'     => 'Won',
            'type'      => self::TYPE_FINAL,
            'editable'  => true,
            'printable' => true,
            'order'     => true,
        ],
        QuotationRevisionStatus::LOST => [
            'title'     => 'Lost',
            'type'      => self::TYPE_FINAL,
            'editable'  => false,
            'printable' => true,
            'order'     => false,
        ],
        QuotationRevisionStatus::HOLD => [
            'title'     => 'Hold',
            'type'      => self::TYPE_NORMAL,
            'editable'  => false,
            'printable' => true,
            'order'     => false,
        ],
    ];

    // For each status, these transitions define from which states it is possible to move to the current state.
    protected $transitions = [
        // Initial state, there no states from which to move into this one.
        QuotationRevisionStatus::IN_PROCESS => [
            'from' => [
            ],
            'to' => QuotationRevisionStatus::IN_PROCESS,
        ],
        // Active status can be reached from "In process" and "Hold" states.
        QuotationRevisionStatus::ACTIVE => [
            'from' => [
                QuotationRevisionStatus::IN_PROCESS,
                QuotationRevisionStatus::HOLD,
            ],
            'to' => QuotationRevisionStatus::ACTIVE,
        ],
        // Won status can be reached from "Active" and "Hold" states.
        QuotationRevisionStatus::WON => [
            'from' => [
                QuotationRevisionStatus::ACTIVE,
                QuotationRevisionStatus::HOLD,
            ],
            'to' => QuotationRevisionStatus::WON,
        ],
        // Lost status can be reached from "Active" and "Hold" states.
        QuotationRevisionStatus::LOST => [
            'from' => [
                QuotationRevisionStatus::ACTIVE,
                QuotationRevisionStatus::HOLD,
            ],
            'to' => QuotationRevisionStatus::LOST,
        ],
        // Hold status can be reached from "In process" and "Active" states.
        QuotationRevisionStatus::HOLD => [
            'from' => [
                QuotationRevisionStatus::IN_PROCESS,
                QuotationRevisionStatus::ACTIVE,
            ],
            'to' => QuotationRevisionStatus::HOLD,
        ],
    ];

    /** @var QuotationRevisionInterface */
    protected $revision;

    /** @var RevisionOrderStateMachine */
    protected $orderStateMachine;

    /** @var bool */
    protected $isLatest;

    /**
     * @param StateMachineManager        $stateMachineManager
     * @param QuotationRevisionInterface $revision
     */
    public function __construct(QuotationRevisionInterface $revision)
    {
        $this->revision = $revision;

        $this->orderStateMachine = RevisionOrderStateMachine::create($this->revision, $this);

        $quotation = $this->revision->getQuotation();
        $latestRevision = $quotation->getLatestRevision();

        $this->isLatest = (bool) ($this->revision->getRevisionId() === $latestRevision->getRevisionId());
    }

    /**
     * @param QuotationRevisionInterface $revision
     *
     * @return RevisionStateMachine
     */
    public static function create(QuotationRevisionInterface $revision)
    {
        return new self($revision);
    }

    /**
     * Is the revision latest for the quotation?
     *
     * @return mixed
     */
    public function isLatest()
    {
        return $this->isLatest;
    }

    /**
     * Get next possible states that can be moved into form the current one.
     *
     * @return array
     */
    public function getAllowedStates()
    {
        $currentState = $this->getCurrentState();

        $states = array_reduce($this->transitions, function ($carry, $item) use ($currentState) {
            if (in_array($currentState, $item['from'])) {
                $state = $item['to'];

                $carry[$state] = $this->states[$state]['title'];
            }

            return $carry;
        }, [$currentState => $this->states[$currentState]['title']]);

        return $states;
    }

    /**
     * Get allowed order statuses that can be moved into from the current one.
     *
     * @return array
     */
    public function getAllowedOrderStates()
    {
        return $this->hasOrderStatus() ? $this->orderStateMachine->getAllowedStates() : [];
    }

    /**
     * Get the current status.
     *
     * @return string
     */
    public function getCurrentState()
    {
        $status = $this->revision->getStatus();

        return isset($this->states[$status]) ? $status : QuotationRevisionStatus::IN_PROCESS;
    }

    /**
     * Get the current order status.
     *
     * @return string
     */
    public function getCurrentOrderStatus()
    {
        return $this->hasOrderStatus() ? $this->orderStateMachine->getCurrentState() : null;
    }

    /**
     * Test if it is possible to move into the given state from the current one.
     *
     * @param $state
     *
     * @return bool
     */
    public function canApply($state)
    {
        $allowedStates = $this->getAllowedStates();

        return isset($allowedStates[$state]);
    }

    /**
     * Move into the given state.
     *
     * @param $state
     *
     * @throws \Exception
     */
    public function apply($state)
    {
        if ($this->canApply($state)) {
            $statusChanged = (bool) ($state != $this->revision->getStatus());

            $this->revision->setStatus($state);

            if (!$this->hasOrderStatus() || $statusChanged) {
                $this->orderStateMachine->reset();
            }
        } else {
            throw new \Exception('Invalid state!');
        }
    }

    /**
     * Set the order status.
     *
     * @param $orderStatus
     *
     * @throws \Exception
     */
    public function setOrderStatus($orderStatus)
    {
        if ($this->hasOrderStatus() || $orderStatus == QuotationRevisionOrderStatus::NONE) {
            $this->orderStateMachine->apply($orderStatus);
        } else {
            throw new \Exception('Not possible to set order status for this state!');
        }
    }

    /**
     * Test if revision is editable in the current state.
     *
     * @return bool
     */
    public function isEditable()
    {
        return $this->isLatest() && $this->isDraft();
    }

    /**
     * Test if revision is editable (= print should be draft mode).
     *
     * @return bool
     */
    public function isDraft()
    {
        return $this->isAttributeSet('editable', true) && (!$this->hasOrderStatus() || $this->orderStateMachine->isEditable());
    }

    /**
     * Test if the revision is printable in the current state.
     *
     * @return bool
     */
    public function isPrintable()
    {
        return $this->isAttributeSet('printable', true) && (!$this->hasOrderStatus() || $this->orderStateMachine->isPrintable());
    }

    /**
     * Test if the current state is a final state (cannot move into any other state)
     * @return bool
     */
    public function isFinal()
    {
        return $this->isAttributeSet('type', self::TYPE_FINAL) && (!$this->hasOrderStatus() || $this->orderStateMachine->isFinal());
    }

    /**
     * Test if the revision has been copied to order.
     *
     * @return bool
     */
    public function isOrder()
    {
        return $this->hasOrderStatus() ? $this->orderStateMachine->isOrder() : false;
    }

    /**
     * Test if the revision can be copied to order.
     * @return bool
     */
    public function hasOrderStatus()
    {
        return $this->isAttributeSet('order', true);
    }

    /**
     * Test if the revision has been won.
     *
     * @return bool
     */
    public function isWon()
    {
        return $this->getCurrentState() === QuotationRevisionStatus::WON;
    }

    /**
     * Reset to the initial state.
     */
    public function reset()
    {
        $this->revision->setStatus(QuotationRevisionOrderStatus::IN_PROCESS);

        $this->orderStateMachine->reset();
    }

    /**
     * Helper to test if a given attribute is allowed in its current state.
     *
     * @param $attribute
     *
     * @return bool
     */
    private function isAttributeSet($attribute, $test)
    {
        $currentState = $this->getCurrentState();

        if (isset($this->states[$currentState])) {
            return $this->states[$currentState][$attribute] === $test;
        }

        return false;
    }
}
