<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\SummaryPage;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

/**
 * Interface for summary pages.
 *
 * Interface SummaryPageInterface
 * @package Klaro\QuotationBundle\Api
 */
interface SummaryPageInterface
{

    /**
     * Get the summary page content.
     *
     * @return string
     */
    public function __toString();
    /**
     * Initiliazes the context for the summary page.
     *
     * @param QuotationRevisionInterface $revision
     * @param $summaryPageId
     * @param $config
     */
    public function initialize(
        QuotationRevisionInterface $revision,
        $summaryPageId,
        $config
    );

    /**
     * Render the summary page content.
     *
     * @return string
     */
    public function render();
}
