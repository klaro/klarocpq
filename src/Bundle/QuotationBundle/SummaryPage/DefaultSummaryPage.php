<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\SummaryPage;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Event\SummaryPageEvents;
use Klaro\QuotationBundle\CurrencyConverter\CurrencyConverter;
use Klaro\QuotationBundle\Event\QuotationEvent;
use Klaro\QuotationBundle\Event\QuotationRevisionEvent;
use Klaro\QuotationBundle\Facade\QuotationFacade;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Templating\EngineInterface;

/**
 * Default summary page implementation that displays the offering sumary.
 *
 * Class DefaultSummaryPage
 * @package Klaro\QuotationBundle\SummaryPage
 */
class DefaultSummaryPage extends AbstractSummaryPage
{
    /** @var QuotationFacade */
    protected $quotationFacade;

    /** @var EngineInterface */
    protected $templating;

    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /** @var CurrencyConverter */
    protected $currencyConverter;

    /** @var QuotationEvent */
    protected $event;

    /**
     * @oaram QuotationFacade $quotationFacade
     *
     * @param EngineInterface          $templating
     * @param EventDispatcherInterface $eventDispatcher
     * @param CurrencyConverter        $currencyConverter
     */
    public function __construct(
        QuotationFacade $quotationFacade,
        EngineInterface $templating,
        EventDispatcherInterface $eventDispatcher,
        CurrencyConverter $currencyConverter
    ) {
        $this->quotationFacade = $quotationFacade;
        $this->templating = $templating;
        $this->eventDispatcher = $eventDispatcher;
        $this->currencyConverter = $currencyConverter;
    }

    /**
     * {@inheritDoc}
     */
    public function initialize(QuotationRevisionInterface $revision, $summaryPageId, $config)
    {
        parent::initialize($revision, $summaryPageId, $config);

        $offeringSummary = $this->quotationFacade->getConfigurationForRevision($revision);

        $currencyConverter = $this->currencyConverter->getCurrencyConverterForRevision($revision);

        $this->event = new QuotationRevisionEvent($revision, [
            'subPage' => $summaryPageId,
            'stateMachine' => RevisionStateMachine::create($this->revision),
            'configuration' => $offeringSummary,
            'currencyConverter' => $currencyConverter,
        ]);

        $this->eventDispatcher->dispatch(SummaryPageEvents::QUOTATION_SUMMARY_DEFAULT, $this->event);
    }

    /**
     * {@inheritDoc}
     */
    public function getData()
    {
        return $this->event->toArray() + parent::getData();
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        return $this->templating->render(
            $this->getTemplate('KlaroQuotationBundle:Quotation:summary/default.html.twig'),
            $this->getData()
        );
    }
}
