<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\SummaryPage;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\FormData\ProductLineDataNode;

/**
 * Base class for summary page implementations.
 *
 * Class AbstractSummaryPage
 * @package Klaro\QuotationBundle\SummaryPage
 */
abstract class AbstractSummaryPage implements SummaryPageInterface
{
    /** @var QuotationInterface */
    protected $quotation;

    /** @var QuotationRevisionInterface */
    protected $revision;

    /** @var string */
    protected $summaryPageId;

    /** @var array */
    protected $config;

    /** @var ProductLineDataNode */
    protected $formData;

    /** @var ConfigurationNode */
    protected $configuration;

    /**
     * @return string
     */
    public function __toString()
    {
        try {
            return $this->render();
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function initialize(
        QuotationRevisionInterface $revision,
        $summaryPageId,
        $config
    ) {
        $this->revision      = $revision;
        $this->quotation     = $revision->getQuotation();
        $this->summaryPageId = $summaryPageId;
        $this->config        = $config;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'quotation'     => $this->quotation,
            'revision'      => $this->revision,
            'summaryPageId' => $this->summaryPageId,
            'config'        => $this->config,
        ];
    }

    /**
     * @param $defaultTemplate
     *
     * @return string
     */
    public function getTemplate($defaultTemplate)
    {
        // Allow overriding of template from config
        return isset($this->config['template']) && !empty($this->config['template']) ? $this->config['template'] : $defaultTemplate;
    }

    /**
     * @return ConfigurationNode
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param ConfigurationNode $configuration
     */
    public function setConfiguration(ConfigurationNode $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return ProductLineDataNode
     */
    public function getFormData()
    {
        return $this->formData;
    }

    /**
     * @param ProductLineDataNode $formData
     */
    public function setFormData(ProductLineDataNode $formData)
    {
        $this->formData = $formData;
    }
}
