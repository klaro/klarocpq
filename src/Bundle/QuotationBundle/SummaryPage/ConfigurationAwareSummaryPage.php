<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\SummaryPage;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;

interface ConfigurationAwareSummaryPage
{
    /**
     * @param ConfigurationNode $configuration
     */
    public function setConfiguration(ConfigurationNode $configuration);
}
