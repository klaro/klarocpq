<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\SummaryPage;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Facade\QuotationFacade;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Klaro\QuotationBundle\Traits\QuotationParameterTraits;
use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Klaro\QuotationBundle\Validation\QuotationViolation;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manager for summary page services.
 *
 * Class SummaryPageManager
 * @package Klaro\QuotationBundle\SummaryPage
 */
class SummaryPageManager
{
    /** @var string[] */
    protected $summaryServices;

    /** @var ContainerInterface */
    protected $container;

    /** @var QuotationFacade */
    protected $quotationFacade;

    /** @var array */
    protected $summaryPages;

    /**
     * SummaryPageManager constructor.
     * @param ContainerInterface $container
     * @param QuotationFacade    $quotationFacade
     * @param $summaryPages
     */
    public function __construct(ContainerInterface $container, QuotationFacade $quotationFacade, $summaryPages)
    {
        $this->summaryServices = [];
        $this->container = $container;
        $this->quotationFacade = $quotationFacade;
        $this->summaryPages = $summaryPages;
    }

    /**
     * Add a summary page definition.
     *
     * @param $alias
     * @param $serviceId
     *
     * @throws \Exception
     */
    public function addSummaryService($alias, $serviceId)
    {
        if (isset($this->summaryServices[$alias])) {
            throw new \Exception('A summary with this alias already exists.');
        } elseif (!$this->container->has($serviceId)) {
            throw new \Exception("The '{$alias}' summary page service '{$serviceId}' does not exist!");
        }

        $this->summaryServices[$alias] = $serviceId;
    }

    /**
     * Get list of summary page service definitions.
     *
     * @return array|\string[]
     */
    public function getSummaryServices()
    {
        return $this->summaryServices;
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param $summaryPageId
     *
     * @return SummaryPageInterface
     */
    public function getSummaryPage(QuotationRevisionInterface $revision, $summaryPageId)
    {
        $quotationType = $revision->getQuotation()->getQuotationType();
        $summaryPage = $this->getSummaryService($revision, $summaryPageId, $this->summaryPages[$quotationType][$summaryPageId]);

        return $summaryPage;
    }

    /**
     * Get list of summary pages for the loaded revision.
     *
     * @return array
     */
    public function getSummaryPages()
    {
        return $this->summaryPages;
    }

    /**
     * Get locked summary page service.
     *
     * @param QuotationRevisionInterface $revision
     * @param $violations
     *
     * @return LockedSummaryPage
     */
    protected function getLockedSummary(QuotationRevisionInterface $revision, $violations)
    {
        /** @var LockedSummaryPage $lockedPage */
        $lockedPage = $this->container->get('klaro_quotation.summary_page.locked');

        $lockedPage->initialize($revision, null, $violations);

        return $lockedPage;
    }

    /**
     * Loads a summary page service.
     *
     * @param QuotationRevisionInterface $revision
     * @param $type
     * @param $config
     *
     * @return SummaryPageInterface
     *
     * @throws \Exception
     */
    private function getSummaryService(QuotationRevisionInterface $revision, $summaryPageId, $config)
    {
        $serviceType = isset($config['type']) ? $config['type'] : 'default';

        if (!isset($this->summaryServices[$serviceType]) || !$this->container->has($this->summaryServices[$serviceType])) {
            throw new \Exception("The '{$summaryPageId}' summary page service with type '{$serviceType}' does not exist!");
        }

        $service = $this->container->get($this->summaryServices[$serviceType]);

        if (!($service instanceof SummaryPageInterface)) {
            throw new \Exception("The '{$summaryPageId}' summary page service '".$this->summaryServices[$serviceType]."' must implement SummaryPageInterface!");
        }

        if ($service instanceof FormDataAwareSummaryPage) {
            $service->setFormData($this->quotationFacade->getFormDataForRevision($revision));
        }

        if ($service instanceof ConfigurationAwareSummaryPage) {
            $service->setConfiguration($this->quotationFacade->getConfigurationForRevision($revision));
        }

        $service->initialize($revision, $summaryPageId, $config);

        return $service;
    }
}
