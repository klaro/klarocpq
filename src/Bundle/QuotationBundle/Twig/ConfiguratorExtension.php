<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Twig;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\ConfItem;
use Klaro\QuotationBundle\CurrencyConverter\CurrencyConverterInterface;
use Klaro\QuotationBundle\CurrencyConverter\DefaultCurrencyConverter;

class ConfiguratorExtension extends \Twig_Extension
{
    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'klaro_quotation_configuration',
                [$this, 'renderConfiguration'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new \Twig_SimpleFunction(
                'klaro_quotation_configuration_price_factor',
                [$this, 'renderPriceFactor'],
                ['is_safe' => ['html']]
            ),
        ];
    }

    /**
     * @param \Twig_Environment               $environment
     * @param ConfigurationNode               $configuration
     * @param CurrencyConverterInterface|null $currencyConverter
     *
     * @return string
     */
    public function renderConfiguration(
        \Twig_Environment $environment,
        ConfigurationNode $configuration,
        CurrencyConverterInterface $currencyConverter = null,
        $scope = 'default',
        $print_header = true,
        $print_first_level = true,
        $prefix = 'root',
        $start_index = 0
    ) {
        if (!($currencyConverter instanceof CurrencyConverterInterface)) {
            $currencyConverter = new DefaultCurrencyConverter();
        }

        return $environment->render('KlaroQuotationBundle:Configurator:table.html.twig', [
            'configuration' => $configuration,
            'currencyConverter' => $currencyConverter,
            'scope' => $scope,
            'print_header' => $print_header,
            'print_first_level' => $print_first_level,
            'prefix' => $prefix,
            'start_index' => $start_index,
        ]);
    }

    /**
     * @param ConfItem                   $confItem
     * @param CurrencyConverterInterface $currencyConverter
     * @param $field
     *
     * @return string
     */
    public function renderPriceFactor(ConfItem $confItem, CurrencyConverterInterface $currencyConverter, $field)
    {
        $res = '';

        $originalValue = $confItem->getOriginalValue($field);

        if (!is_null($originalValue)) {
            $factors = $confItem->getFactors($field);

            $salesTax = $confItem->getSalesTaxPercentage();
            $hasTax   = (bool) ($field === 'SalesPrice' && !empty($salesTax));
            $hasFactors = count($factors) > 0;
            $defaultCurrency = $currencyConverter->getDefaultCurrency();
            $currency = $currencyConverter->getCurrency();
            $hasDifferentCurrency = $defaultCurrency != $currency;

            if ($hasFactors || $hasTax || $hasDifferentCurrency) {
                $value = $confItem->get($field);
                $taxAmount = 0;

                $res .= $originalValue;

                if ($hasFactors) {
                    foreach ($factors as $factor) {
                        $res .= ' * '.$factor['factor'].(!empty($factor['title']) ? ' <sub>'.$factor['title'].'</sub>' : '');
                    }

                    $res .= ' = '.$value;
                }

                if ($hasTax) {
                    $taxAmount = $confItem->getSalesTax();

                    $res .= ' + '.$taxAmount.' <sub>VAT '.$salesTax.'%</sub> = '.($value + $taxAmount);
                }

                $res .= ' '.$defaultCurrency;

                if ($hasDifferentCurrency) {
                    $res .= ' * '.$currencyConverter->toCurrency(1).' '.$currency.'/'.$defaultCurrency.
                        ' = '.$currencyConverter->toCurrency($value + $taxAmount).' '.$currency;
                }
            }
        }

        return $res;
    }
}
