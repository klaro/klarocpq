<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Twig;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\FormData\DataNode;
use Klaro\Component\FormData\DataNodeInterface;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\FormData\ProductPhaseDataNode;
use Klaro\Component\FormData\ProductPhaseListDataNode;
use Klaro\Component\ProductPhase\PhaseNode;
use Klaro\Component\ProductPhase\ProductListPhase;
use Klaro\QuotationBundle\AdminPage\AdminPageManager;
use Klaro\QuotationBundle\Facade\QuotationFacade;
use Klaro\QuotationBundle\Menu\MenuItem;
use Klaro\QuotationBundle\ProductDataImport\ProductDataImporter;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Translation\TranslatorInterface;

class MenuExtension extends \Twig_Extension
{
    /** @var QuotationFacade */
    protected $quotationFacade;

    /** @var RequestStack */
    protected $requestStack;

    /** @var RouterInterface */
    protected $router;

    /** @var TranslatorInterface */
    protected $translator;

    /** @var AdminPageManager */
    protected $adminPages;

    /** @var ProductDataImporter */
    protected $productImporter;

    /** @var array */
    protected $quotationTypes;

    /** @var array */
    protected $summaryPages;

    /**
     * MenuExtension constructor.
     * @param QuotationFacade     $quotationFacade
     * @param RequestStack        $requestStack
     * @param RouterInterface     $router
     * @param TranslatorInterface $translator
     * @param AdminPageManager    $adminPages
     * @param ProductDataImporter $productImporter
     * @param array               $quotationTypes
     * @param array               $summaryPages
     */
    public function __construct(
        QuotationFacade $quotationFacade,
        RequestStack $requestStack,
        RouterInterface $router,
        TranslatorInterface $translator,
        AdminPageManager $adminPages,
        ProductDataImporter $productImporter,
        $quotationTypes,
        $summaryPages
    ) {
        $this->quotationFacade = $quotationFacade;
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->translator = $translator;
        $this->adminPages = $adminPages;
        $this->productImporter = $productImporter;
        $this->quotationTypes = $quotationTypes;
        $this->summaryPages = $summaryPages;
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'klaro_quotation_menu_quotation_types',
                [$this, 'renderQuotationTypesMenu'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new \Twig_SimpleFunction(
                'klaro_quotation_menu_phase_inputdata',
                [$this, 'renderPhaseMenu'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new \Twig_SimpleFunction(
                'klaro_quotation_menu_phase_back',
                [$this, 'renderPhaseBackMenu'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new \Twig_SimpleFunction(
                'klaro_quotation_menu_offering_summary',
                [$this, 'renderPhaseOfferingMenu'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
            new \Twig_SimpleFunction(
                'klaro_quotation_menu_admin_pages',
                [$this, 'renderAdminPagesMenu'],
                ['is_safe' => ['html'], 'needs_environment' => true]
            ),
        ];
    }

    /**
     * @param \Twig_Environment $environment
     * @param null              $activeType
     *
     * @return mixed
     */
    public function renderQuotationTypesMenu(\Twig_Environment $environment, $activeType = null)
    {
        $menu = [];

        foreach ($this->quotationTypes as $id => $title) {
            $menuItem = MenuItem::create()
                ->setTitle($title);

            $active = ($id === $activeType);
            $menuItem->setActive($active);

            if ($active) {
                $menuItem->setIcon('chevron-right');
            }

            $menuItem->setUrl($this->router->generate('klaro_quotation_list', [
                'quotationType' => $id,
            ]));

            $menu[] = $menuItem;
        }

        $title = $this->translator->trans('menu.main.quotations', [], 'KlaroQuotationBundle');

        return $this->renderMenu($environment, $menu, $title);
    }

    /**
     * @param \Twig_Environment          $environment
     * @param QuotationRevisionInterface $revision
     *
     * @return string
     */
    public function renderPhaseBackMenu(\Twig_Environment $environment, QuotationRevisionInterface $revision)
    {
        $title = $this->translator->trans('edit.menu.back.quotation_type', [
            '%quotationType%' => $this->quotationTypes[$revision->getQuotation()->getQuotationType()],
        ], 'KlaroQuotationBundle');

        $quotationList = MenuItem::create()
            ->setTitle($title)
            ->setIcon('chevron-left')
            ->setUrl($this->router->generate('klaro_quotation_list', [
                'quotationType' => $revision->getQuotation()->getQuotationType(),
            ]));

        $title = $this->translator->trans('edit.menu.back.quotation', [
            '%title%' => $revision->getIdentifier(),
        ], 'KlaroQuotationBundle');

        $infoPage = MenuItem::create()
            ->setTitle($title)
            ->setIcon('chevron-left')
            ->setUrl($this->router->generate('klaro_quotation_info', [
                'quotationId' => $revision->getQuotation()->getId(),
            ]));

        $menu = [
            $quotationList, $infoPage,
        ];

        return $this->renderMenu($environment, $menu, $revision->getIdentifier());
    }

    /**
     * @param \Twig_Environment          $environment
     * @param QuotationRevisionInterface $revision
     * @param string                     $activePage
     *
     * @return string
     */
    public function renderPhaseOfferingMenu(
        \Twig_Environment $environment,
        QuotationRevisionInterface $revision,
        $activePage = null
    ) {
        $menu = $this->collectSummariesData($revision, $activePage);

        return $this->renderMenu(
            $environment,
            $menu,
            $this->translator->trans('edit.menu.summary', [], 'KlaroQuotationBundle')
        );
    }

    /**
     * @param \Twig_Environment          $environment
     * @param QuotationRevisionInterface $revision
     * @param boolean                    $showActive
     *
     * @return string
     */
    public function renderPhaseMenu(\Twig_Environment $environment, QuotationRevisionInterface $revision, $showActive = false)
    {
        $formData = $this->quotationFacade->getFormDataForRevision($revision);

        return $this->renderMenu(
            $environment,
            $this->collectMenuData($revision, $formData, $showActive),
            $this->translator->trans('edit.menu.input_data', [], 'KlaroQuotationBundle')
        );
    }

    /**
     * @param \Twig_Environment $environment
     * @param null              $activePage
     *
     * @return string
     */
    public function renderAdminPagesMenu(\Twig_Environment $environment, $activePage = null)
    {
        $menu = [];

        foreach ($this->adminPages->getAdminPages() as $key => $item) {
            $adminService = $this->adminPages->getAdminPage($key);

            $menuItem = MenuItem::create()
                ->setTitle($adminService->getUserFriendlyName());

            $active = ($key === $activePage);
            $menuItem->setActive($active);

            if ($active) {
                $menuItem->setIcon('chevron-right');
            }

            $menuItem->setUrl($this->router->generate('klaro_quotation_admin', [
                'page' => $key,
            ]));

            $menu[] = $menuItem;
        }

        return $this->renderMenu(
            $environment,
            $menu,
            $this->translator->trans('admin.menu.admin_pages', [], 'KlaroQuotationBundle')
        );
    }

    /**
     * @param \Twig_Environment $environment
     * @param array             $menu
     * @param null              $title
     *
     * @return string
     */
    private function renderMenu(\Twig_Environment $environment, array $menu, $title = null)
    {
        return $environment->render('KlaroQuotationBundle:Menu:menu.html.twig', [
            'title' => $title,
            'menu' => $menu,
        ]);
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param ProductLineDataNode        $formData
     * @param boolean                    $showActive
     *
     * @return array
     */
    public function collectMenuData(QuotationRevisionInterface $revision, ProductLineDataNode $formData, $showActive)
    {
        $request = $this->requestStack->getCurrentRequest();
        $phasePath = $request->query->get('phaseId');

        $editablePhase = $formData->getSinglePhaseByPath($phasePath);
        $mainPhase = $editablePhase->getTopLevelNode();
        $mainPhaseParent = $mainPhase->getParent();

        if ($mainPhaseParent instanceof ProductPhaseListDataNode) {
            $mainPhase = $mainPhaseParent;
        }

        foreach ($formData->getChildren() as $dataNode) {
            $phaseNode = $dataNode->getPhaseNode();

            $active = ($showActive && $dataNode->getPath() == $mainPhase->getPath());
            list($attributes, $children, $disabled, $icon, $type, $url) = $this->getDataNodeMenuItem(
                $dataNode,
                $editablePhase,
                $formData,
                $revision
            );

            if ($active && !$icon) {
                $icon = 'chevron-right';
            }

            $menu[] = MenuItem::create()
                ->setType($type)
                ->setTitle($phaseNode->getTitle())
                ->setActive($active)
                ->setDisabled($disabled)
                ->setIcon($icon)
                ->setUrl($url)
                ->setChildren($children)
                ->setDataAttributes($attributes);
        }

        return $menu ?? [];
    }

    /**
     * @param DataNodeInterface          $dataNode
     * @param ProductListPhase           $phaseNode
     * @param $editablePhase
     * @param ProductLineDataNode        $formData
     * @param QuotationRevisionInterface $revision
     *
     * @return array
     */
    private function getProductListPhaseNodeMenuItem(
        DataNodeInterface $dataNode,
        ProductListPhase $phaseNode,
        $editablePhase,
        ProductLineDataNode $formData,
        QuotationRevisionInterface $revision
    ): array {
        $productSources = $this->productImporter->getImporterMappings($formData)->getProductSources();

        /** @var ProductListPhase $phaseNode */
        foreach ($phaseNode->getProducts() as $ref => $product) {
            $productNames[$ref] = $product->getTitle();
        }

        $attributes = [
            'app' => 'add-product',
            'quotation-id' => $revision->getQuotation()->getId(),
            'revision-id' => $revision->getRevisionId(),
            'phase-id' => $dataNode->getPath(),
            'section-name' => $phaseNode->getTitle(),
            'product-names' => $productNames ?? [],
            'product-sources' => $productSources,
            'disabled' => !RevisionStateMachine::create($revision)->isEditable(),
        ];

        /** @var ProductPhaseListDataNode $dataNode */
        $children = $this->collectProducts($editablePhase, $dataNode, $revision);

        return [$attributes, $children, $phaseNode->getDisabled(), 'plus', PhaseNode::PRODUCT_LIST, null];
    }

    /**
     * @param DataNodeInterface          $dataNode
     * @param $editablePhase
     * @param ProductLineDataNode        $formData
     * @param QuotationRevisionInterface $revision
     *
     * @return array
     */
    private function getDataNodeMenuItem(
        DataNodeInterface $dataNode,
        $editablePhase,
        ProductLineDataNode $formData,
        QuotationRevisionInterface $revision
    ): array {
        $phaseNode = $dataNode->getPhaseNode();
        if ($phaseNode->getType() === PhaseNode::PRODUCT_LIST) {
            /** @var ProductListPhase $phaseNode */

            return $this->getProductListPhaseNodeMenuItem($dataNode, $phaseNode, $editablePhase, $formData, $revision);
        }

        if (!$phaseNode->getDisabled()) {
            $nestedChild = $this->getNestedDefaultChild($dataNode);

            $url = $this->router->generate(
                'klaro_quotation_edit_revision',
                [
                    'quotationId' => $revision->getQuotation()->getId(),
                    'revisionId' => $revision->getRevisionId(),
                    'phaseId' => $nestedChild->getPath(),
                ]
            );
        }

        return [[], [], $phaseNode->getDisabled(), null, null, $url ?? null];
    }

    /**
     * @param DataNode                   $editablePhase
     * @param ProductPhaseListDataNode   $productList
     * @param QuotationRevisionInterface $revision
     *
     * @return MenuItem[]
     */
    private function collectProducts(
        DataNode $editablePhase,
        ProductPhaseListDataNode $productList,
        QuotationRevisionInterface $revision
    ) {
        $mainPhase = $editablePhase->getTopLevelNode();
        $productListTitle = $productList->getPhaseNode()->getTitle();

        if (!$productList->hasChildren()) {
            return [];
        }

        /** @var ProductPhaseDataNode $product */
        foreach ($productList as $product) {
            $active = $mainPhase->getPath() == $product->getPath();
            $disabled = $product->getPhaseNode()->getDisabled();
            $icon = $active ? 'chevron-right' : null;
            $title = $product->getTitle() ?: $productListTitle;

            if (!$disabled) {
                $nestedChild = $this->getNestedDefaultChild($product);

                $url = $this->router->generate(
                    'klaro_quotation_edit_revision',
                    [
                        'quotationId' => $revision->getQuotation()->getId(),
                        'revisionId' => $revision->getRevisionId(),
                        'phaseId' => $nestedChild->getPath(),
                    ]
                );
            }

            $products[] = MenuItem::create()
                ->setType($product->getProductRef())
                ->setTitle($title)
                ->setActive($active)
                ->setDisabled($disabled)
                ->setIcon($icon)
                ->setUrl($url ?? null)
                ->setNotice($product->getPhaseNode()->getTitle())
                ->addDataAttribute('phase-id', $product->getPath());
        }

        return $products ?? [];
    }

    /**
     * @param DataNodeInterface $node
     *
     * @return DataNodeInterface
     */
    private function getNestedDefaultChild(DataNodeInterface $node): DataNodeInterface
    {
        $children = $node->getChildren();

        return (empty($children) ? $node : $this->getNestedDefaultChild(reset($children)));
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param string|null $activePage
     * @return array
     */
    public function collectSummariesData(QuotationRevisionInterface $revision, $activePage = null): array
    {
        $menu = [];

        $quotationType = $revision->getQuotation()->getQuotationType();

        if (isset($this->summaryPages[$quotationType])) {
            foreach ($this->summaryPages[$quotationType] as $summaryPageId => $summaryPageData) {
                $active = ($summaryPageId === $activePage);
                $icon = null;

                if ($active) {
                    $icon = 'chevron-right';
                } else if (isset($summaryPageData['icon'])) {
                    $icon = $summaryPageData['icon'];
                }

                $menu[] = MenuItem::create()
                    ->setTitle($summaryPageData['title'])
                    ->setUrl($this->router->generate('klaro_quotation_revision_summary', [
                        'quotationId' => $revision->getQuotation()->getId(),
                        'revisionId' => $revision->getRevisionId(),
                        'subPage' => $summaryPageId
                    ]))
                    ->setIcon($icon)
                    ->setActive($active);
            }
        }
        return $menu;
    }
}
