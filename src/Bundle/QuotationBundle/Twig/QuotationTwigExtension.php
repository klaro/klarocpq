<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Twig;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Api\QuotationRevisionOrderStatus;
use Klaro\QuotationBundle\Api\QuotationRevisionStatus;
use Klaro\Component\Logger\Logger;
use Klaro\QuotationBundle\CurrencyConverter\CurrencyConverterInterface;
use Klaro\QuotationBundle\Traits\QuotationServiceTraits;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig_SimpleFilter;

/**
 * Twig extension that provides quotation bundle data to templates.
 *
 * Class QuotationTwigExtension
 * @package Klaro\QuotationBundle\Twig
 */
class QuotationTwigExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    use QuotationServiceTraits;

    /** @var ContainerInterface */
    protected $container;

    /** @var RouterInterface $routing */
    protected $router;

    /** @var Logger */
    protected $logger;

    /** @var array|string[] */
    protected $requiredJsLibs;

    /** @var array|string[] */
    protected $revisionQualifiers;

    /** @var array|string[] */
    protected $revisionQualifierOptions;

    /** @var array|string[] */
    protected $revisionStatus;

    /** @var array|string[] */
    protected $revisionOrderStatus;

    /**
     * @param ContainerInterface|null  $container
     * @param $requiredJsLibs
     * @param $revisionQualifiers
     * @param $revisionQualifierOptions
     */
    public function __construct(ContainerInterface $container = null, $requiredJsLibs, $revisionQualifiers, $revisionQualifierOptions)
    {
        $this->container = $container;
        $this->logger = $this->container->get('klaro_quotation.logger');
        $this->router = $this->container->get('router');
        $this->requiredJsLibs = $requiredJsLibs;

        $this->revisionQualifiers       = $revisionQualifiers;
        $this->revisionQualifierOptions = $revisionQualifierOptions;

        $this->revisionStatus      = QuotationRevisionStatus::getRevisionStatusOptions();
        $this->revisionOrderStatus = QuotationRevisionOrderStatus::getQuotationRevisionOrderStatusOptions();
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('klaro_quotation_load_modules', [$this, 'renderLoadModules'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('klaro_quotation_requirejs_config', [$this, 'renderRequireJsConfig'], ['is_safe' => ['html']]),
            new \Twig_SimpleFunction('klaro_quotation_main_menu', [$this, 'renderMainMenu'], ['is_safe' => ['html']]),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('repeat', 'str_repeat'),
            new Twig_SimpleFilter('klaro_quotation_revision_data', ['Klaro\\QuotationBundle\\Library\\ApiHelper', 'getRevisionDataArray']),
            new Twig_SimpleFilter('klaro_quotation_summary_data', ['Klaro\\QuotationBundle\\Library\\ApiHelper', 'getSummaryDataArray']),
        ];
    }

    /**
     * @return array
     */
    public function getGlobals()
    {
        return array(
            'klaro_quotation_application_title' => $this->container->getParameter('klaro_quotation.application_title'),
            'klaro_quotation_type_names' => $this->container->getParameter('klaro_quotation.quotation_type_names'),
            'klaro_quotation_flash_messages' => $this->getFlashMessages(),
            'klaro_quotation_revision_status' => $this->revisionStatus,
            'klaro_quotation_revision_order_status' => $this->revisionOrderStatus,
            'klaro_quotation_revision_qualifiers' => $this->revisionQualifiers,
            'klaro_quotation_revision_qualifier_options' => $this->revisionQualifierOptions,
        );
    }

    /**
     * Get script for loading modules.
     *
     * @param array $config
     *
     * @return mixed
     */
    public function renderLoadModules($config = array())
    {
        return $this->container->get('templating')->render('KlaroQuotationBundle:TwigExtension:modules.html.twig', [
            'config' => $config,
        ]);
    }

    /**
     * Render script for require.js configuration.
     *
     * @param $config
     * @param bool   $checkRequired
     *
     * @return mixed
     */
    public function renderRequireJsConfig($config, $checkRequired = true)
    {
        // Go throught the required libs and see that all requirements are met.
        if ($checkRequired === true) {
            $missingLibs = [];

            foreach ($this->requiredJsLibs as $lib) {
                if ((!isset($config['paths']) || !array_key_exists($lib, $config['paths'])) &&
                   (!isset($config['globals']) || !array_key_exists($lib, $config['globals']))) {
                    $missingLibs[] = $lib;
                }
            }

            // Report error for missing libs.
            if (count($missingLibs) > 0) {
                $this->logger->error('Required JS libs are missing!', [
                    'missing_libs'  => $missingLibs,
                    'required_libs' => $this->requiredJsLibs,
                    'config'        => $config,
                ]);
            }
        }

        // Check paths, add defaults
        $paths = $config['paths'] ?? [];

        $paths['fos_routing']      = $paths['fos_routing']      ?? 'bundles/fosjsrouting/js/router';
        $paths['fos_routing_data'] = $paths['fos_routing_data'] ?? 'js/fos_js_routes';
        $paths['klaro_routing']    = $paths['klaro_routing']    ?? 'bundles/klaroquotation/js/routing';

        // browsing cache problem solving
        $webDirPath = sprintf('%s/web', $this->container->getParameter('kernel.project_dir'));
        foreach ($paths as $library => $path) {
            // checking if path ends with ".js" to check file's mtime later
            if (substr('.js', -2) !== '.js') {
                $path = sprintf('%s.js', $path);
            }
            $fileName = sprintf('%s/%s', $webDirPath, $path);
            $timestamp = @filemtime($fileName) ?: false;
            if ($timestamp) {
                $paths[$library] = sprintf('%s?%s', $path, $timestamp);
            }
        }

        // Check packages, add defaults.
        $packages = isset($config['packages']) ? $config['packages'] : [];

        if (array_search('klaro_quotation', array_column($packages, 'name')) === false) {
            $packages['klaro_quotation'] = [
                'name'     => 'klaro_quotation',
                'location' => './bundles/klaroquotation/js',
                'main'     => 'app',
            ];
        }

        return $this->container->get('templating')->render('KlaroQuotationBundle:TwigExtension:requireJsConfig.html.twig', [
            'globals'  => isset($config['globals'])  ? $config['globals']  : [],
            'paths'    => $paths,
            'packages' => $packages,
        ]);
    }

    /**
     * Render the main menu.
     *
     * @return mixed
     */
    public function renderMainMenu()
    {
        return $this->container->get('templating')->render('KlaroQuotationBundle:TwigExtension:mainMenu.html.twig', [
            'adminPages' => $this->getAdminPageManager(),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'klaro_quotation_extension';
    }

    /**
     * Get array of flash messages stored in the session.
     *
     * @return array
     */
    private function getFlashMessages()
    {
        $messages = [];

        $session = $this->container->get('session');

        if ($session) {
            // retrieve messages
            foreach ($session->getFlashBag()->get('notice', []) as $message) {
                $messages[] = $message;
            }
        }

        return $messages;
    }
}
