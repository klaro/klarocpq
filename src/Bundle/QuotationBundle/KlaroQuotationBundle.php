<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle;

use Klaro\QuotationBundle\DependencyInjection\Compiler\SetAdminPageCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetCacheProviderCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetConfiguratorAdaptersCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetCurrencyConverterCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetDocumentBuilderCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetIdentifierGeneratorCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetPhaseConfigCacheCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetPhaseEditorCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetPhaseItemCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetPhaseValidatorCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetPhaseVariablesCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetProductDataImporterCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetQuotationValidatorCompilerPass;
use Klaro\QuotationBundle\DependencyInjection\Compiler\SetSummaryPageCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class KlaroQuotationBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new SetCacheProviderCompilerPass());
        $container->addCompilerPass(new SetPhaseEditorCompilerPass());
        $container->addCompilerPass(new SetPhaseItemCompilerPass());
        $container->addCompilerPass(new SetAdminPageCompilerPass());
        $container->addCompilerPass(new SetQuotationValidatorCompilerPass());
        $container->addCompilerPass(new SetPhaseValidatorCompilerPass());
        $container->addCompilerPass(new SetSummaryPageCompilerPass());
        $container->addCompilerPass(new SetDocumentBuilderCompilerPass());
        $container->addCompilerPass(new SetPhaseConfigCacheCompilerPass());
        $container->addCompilerPass(new SetPhaseVariablesCompilerPass());
        $container->addCompilerPass(new SetConfiguratorAdaptersCompilerPass());
        $container->addCompilerPass(new SetIdentifierGeneratorCompilerPass());
        $container->addCompilerPass(new SetProductDataImporterCompilerPass());
        $container->addCompilerPass(new SetCurrencyConverterCompilerPass());
    }
}
