define(['backbone', 'marionette', './app'], function(Backbone, Marionette, app) {
    'use strict';

    // Helper which finds the closest descendant selector but does not match anything below the given selector.
    // Implementation adapted from: https://github.com/tlindig/jquery-closest-descendant
    var getNearest = function(node, selector, findAll) {
        if (!selector || selector === '') {
            return $();
        }

        findAll = findAll ? true : false;

        var resultSet = $();

        node.each(function() {
            var $this = $(this);

            // breadth first search for every matched node,
            // go deeper, until a child was found in the current subtree or the leave was reached.
            var queue = [];
            queue.push($this);
            while (queue.length > 0) {
                var node = queue.shift();
                var children = node.children();
                for (var i = 0; i < children.length; ++i) {
                    var $child = $(children[i]);
                    if ($child.is(selector)) {
                        resultSet.push($child[0]); //well, we found one
                        if (!findAll) {
                            return false; //stop processing
                        }
                    } else {
                        queue.push($child); //go deeper
                    }
                }
            }
        });

        return resultSet;
    },

    FormGroupView = Backbone.Marionette.ItemView.extend({
        checksum : null,
        groupId : null,
        isHidden : null,
        inputViews : [],

        initialize: function(options) {
            this.checksum = options.checksum;
            this.groupId  = options.groupId;
            this.isHidden = options.isHidden;

            this._initViews();
        },

        _initViews: function() {
            var $widgets = getNearest(this.$el, '[data-form-input]', true),
                me = this;

            this._closeInputViews();

            this._showErrors();

            _.each($widgets, function(widget) {
                var $widget = $(widget),
                    moduleName = $widget.data('form-input'),
                    moduleOptions =  $widget.data() || {};

                moduleOptions = _.extend({
                    el : $widget
                }, moduleOptions);

                app.loadModule(moduleName, moduleOptions, function(module) {
                    me.inputViews.push(module);

                    me._bindEvents(module);
                });
            });
        },

        _showErrors: function() {
            var messages = this.$el.data('validation-msgs'),
                msgFilter,
                errorMsgs,
                warningMsgs,
                infoMsgs,
                errorMessage = '';

            msgFilter = function(collection, type) {
                return _.filter(collection, function(msg) {
                    return msg.type === type;
                });
            };

            errorMsgs   = msgFilter(messages, 'error');
            warningMsgs = msgFilter(messages, 'warning');
            infoMsgs    = msgFilter(messages, 'info');

            if(messages && messages.length > 0) {
                this.$el.addClass('has-feedback');

                if(errorMsgs.length > 0) {
                    this.$el.addClass('has-error');
                }
                else if(warningMsgs.length > 0) {
                    this.$el.addClass('has-warning');
                }

                _.each(messages, function(error) {
                    errorMessage += '<div><i class="fa fa-' + (error.type === 'info' ? 'info-circle' : 'exclamation-triangle') + '"></i> ' + error.message + '</div>';
                });

                this.$el.find('.phase-item-validation-msgs').html(errorMessage).show();
            }
        },

        _closeInputViews: function() {
            _.each(this.inputViews, function(module) {
                this._unbindEvents(module);
            }, this);

            this.inputViews = [];
        },

        replace: function($el, checksum, originator) {
            if(this.checksum !== checksum) {
                this.$el.hide();
                $el.hide();
                this._closeInputViews();

                this.$el.replaceWith($el);
                this.$el = $el;
                this.checksum = checksum;

                this.isHidden = this.$el.data('hidden');

                if(!this.isHidden) {
                    this._initViews();

                    // If the submit event started from this widget, just show the element right away.
                    if(originator && this.groupId !== originator) {
                        this.$el.fadeIn();
                    }
                    else {
                        this.$el.show();
                    }
                }
            }
        },

        getValues: function() {
            var values = [];

            _.each(this.inputViews, function(module) {
                var value = null;

                if(_.isFunction(module.getValue)) {
                    value = module.getValue();
                }

                if(_.isObject(value)) {
                    values.push(value);
                }
            }, this);

            return values;
        },

        _bindEvents: function(module) {
            module.on('change', _.bind(function() {
                this.trigger('change', {
                    originator : this.groupId
                });
            }, this));

            // Bubble replace events up to parent.
            module.on('replace', _.bind(function(htmlData, originator) {
                this.trigger('replace', htmlData, originator);
            }, this));
        },

        _unbindEvents: function(module) {
            module.off();
            module.destroy();
        }
    });

    return Backbone.Marionette.ItemView.extend({
        formGroups : null,

        formKey : null,
        parentFormKey : null,

        events: {
            "submit" : "onSubmit"
        },

        onSubmit: function(e){
            e.preventDefault();
        },

        initialize: function(options) {
            this.formKey  = typeof options.formKey !== 'undefined' ? options.formKey : null;
            this.parentFormKey = options.parentFormKey || null;

            this._readFormFields();

            this._validateForm();

            // Listen to events from outside to refresh form when needed.
            app.vent.on('form:submit', _.bind(this._doSubmit, this));
        },

        _readFormFields: function() {
            this.formGroups = {};

            getNearest(this.$el, '.form-group', true).each(_.bind(function(index, group) {
                var $groupEl = $(group),
                    groupId = $groupEl.data('update-token'),
                    checksum = $groupEl.data('checksum'),
                    hidden = $groupEl.data('hidden');

                if(groupId) {
                    this.formGroups[groupId] = new FormGroupView({
                        el : $groupEl,
                        groupId : groupId,
                        checksum : checksum,
                        isHidden : hidden
                    });

                    this._bindEvents(this.formGroups[groupId]);
                }
            }, this));
        },

        _doSubmit: function(options) {
            var me = this,
                data = this._getData(),
                originator = options && options.originator || null;

            app.vent.trigger('notification', {
                message: "Updating...",
                id: 'phase-save'
            });

            // By default, the phase system makes an XHR to the server and updates the view.
            $.ajax({
                url: window.location.pathname + window.location.search,
                data: {
                    phaseData: JSON.stringify(data)
                },
                dataType: 'html',
                type: 'POST',
                success: function(data) {
                    me._replaceFormFields(data, originator);

                    // TODO: move to subscriber.
                    app.vent.trigger('notification', {
                        message: "Saved.",
                        type : 'success',
                        id: 'phase-save'
                    }, 800);

                    app.vent.trigger('form:submit:success');

                    me.$el.data('validation-msgs', $(data).data('validation-msgs'));

                    me._validateForm();
                },
                error: function() {
                    // TODO: move to subscriber.
                    app.vent.trigger('notification', {
                        message: 'There was an error while processing your request.',
                        type: 'error',
                        showCloseButton: true,
                        id: 'phase-save',
                        actions: {
                            cancel: {
                                label: 'Refresh',
                                action: function() {
                                    window.location.href = window.location.pathname;
                                }
                            }
                        }
                    });

                    app.vent.trigger('form:submit:error');
                },
                complete: function() {
                    app.vent.trigger('log', 'Phase form ajax complete');
                }
            });
        },

        _validateForm: function() {
            if(!this.parentFormKey) {
                app.vent.trigger('form:validation', this.$el.data('validation-msgs'));
            }
        },

        _replaceFormFields: function(htmlData, originator) {
            $(htmlData).find('.form-group').each(_.bind(function(index, group) {
                var $groupEl = $(group),
                    groupId = $groupEl.data('update-token'),
                    checksum = $groupEl.data('checksum'),
                    hidden = $groupEl.data('hidden');

                if(this.formGroups[groupId]) {
                    this.formGroups[groupId].replace($groupEl, checksum, originator);
                }
            }, this));

            // Trigger replace to allow parent to update as well.
            this.trigger('replace', htmlData, originator);
        },

        _bindEvents: function(module) {
            module.on('change', _.bind(this._doSubmit, this));

            // If contents change from sub form, do replace.
            module.on('replace', _.bind(this._replaceFormFields, this));
        },

        _getData: function() {
            var data = {},
                res = {};

            data[this.formKey] = {};

            _.each(this._serializeInputs(), _.bind(function(obj) {
                data[this.formKey][obj.name] = obj.value;
            }, this));

            if(this.parentFormKey) {
                res[this.parentFormKey.parentForm] = {};

                if(this.parentFormKey.dataKey) {
                    res[this.parentFormKey.parentForm][this.parentFormKey.fieldName] = {};
                    res[this.parentFormKey.parentForm][this.parentFormKey.fieldName][this.parentFormKey.dataKey] = data
                }
                else {
                    res[this.parentFormKey.parentForm][this.parentFormKey.fieldName] = data;
                }
            }

            return this.parentFormKey ? res : data;
        },

        _serializeInputs : function() {
            var values = [];

            _.each(this.formGroups, function(formGroup) {
                _.each(formGroup.getValues(), function(value) {
                    values.push(value);
                });
            });

            return values;
        }
    });
});