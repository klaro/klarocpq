/* global define */
define(['jquery', 'underscore', 'backbone', 'marionette'], function($, _, Backbone, Marionette) {
    /**
     * Main application module.
     * @module app
     */
    'use strict';

    // EVENTS:

    /**
     * Event for when module is loaded.
     *
     * @event module:app~moduleLoad
     * @property {string} moduleName Module name
     */

    /**
     * Event for when a module fails to load.
     *
     * @event module:app~moduleLoadError
     * @property {string} moduleName Module name
     */

    /**
     * Event to log messages.
     *
     * @event module:app~log
     * @property {string} moduleName Module name
     */

    /**
     * @global
     */
    var app = undefined;
    var MainApp = Marionette.Application.extend({
        defaultPackage: 'klaro_quotation',
        debugMode: false,
        isStarted: false,
        modules: [],
        pendingModules: [],

        loadModules: function (modules) {
            if (app.isStarted) {
                app._loadModules(modules);
            } else {
                app._registerModules(modules);
            }
        },

        /**
         * Loads a module.
         *
         * @param {string} module Module name
         * @param {Object} options Configuration options for the loaded module.
         * @param {function} readyCallback Callback to call when module is loaded.
         * @fires module:app~moduleLoad
         * @fires module:app~moduleLoadError
         */
        loadModule: function(module, options, readyCallback) {
            var moduleName = module,
                packageName = app.defaultPackage,
                pos = moduleName.lastIndexOf('/');

            if(pos > 0) {
                packageName = moduleName.substring(0, pos);
                moduleName = moduleName.substring(pos + 1);
            }

            // Require the module and start it.
            require([packageName + '/' + moduleName + '.module'], function(ModuleDef) {
                if(typeof ModuleDef === 'function') {
                    var module = new ModuleDef(options);

                    app.vent.trigger('module:load', moduleName);
                    readyCallback(module);
                }
                else {
                    app.vent.trigger('module:load:error', moduleName);
                }
            });
        },

        _loadModules: function (modules) {
            _.each(modules, function(data) {
                app.loadModule(data.module, data, function(module) {
                    app.modules.push(module);
                });
            });
        },

        _registerModules: function (modules) {
            _.each(modules, function(module) {
                app.pendingModules.push(module);
            });
        },

        _start: function (options) {
            app.debugMode = options.debugMode || false;
            app.isStarted = true;
        }
    });

    // Main app that takes care of loading modules.
    app = new MainApp();

    // Setup, loading of modules.
    app.addInitializer(function(options) {
        var modules = options.modules || [];

        app._start(options);

        // Load pending modules first
        app._loadModules(app.pendingModules);
        app._loadModules(modules);

        // Reset pending modules
        app.pendingModules = [];
    });

    return app;
});
