define(['backbone', 'marionette', './app', 'spectrum'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        ui : {
            input   : 'input',
            preview : '.js-preview'
        },

        $colorInput : null,

        fieldName : null,
        format : null,

        disabled : true,

        initialize: function(options) {
            this.bindUIElements();

            this.fieldName = options.fieldName || null;
            this.format = options.format || null;

            this.disabled = (this.ui.input.prop('disabled') === true);

            if(!this.disabled) {
                this.$colorInput = this.$el.spectrum({
                    showInput: false,
                    color: this.ui.input.val(),
                    showPalette: (options.config && options.config.palette),
                    palette: [
                        (options.config && options.config.palette) ? options.config.palette : null
                    ],
                    showInitial: true,
                    clickoutFiresChange: true
                });

                this._bindEvents();
            }
        },

        _bindEvents: function() {
            if(!this.disabled) {
                var format = this.format[0].toUpperCase() + this.format.slice(1),
                    formatMethod = 'to' + format + 'String',
                    value;

                this.$colorInput.on('change', _.bind(function(e, color) {
                    if(color) {
                        value = _.isFunction(color[formatMethod]) ? color[formatMethod]() : color.toHexString();

                        this.ui.input.val(value);
                        this.ui.preview.css('background-color', value);

                        this.trigger('change');
                    }
                }, this));

                this.ui.input.keyup(_.debounce(_.bind(function() {
                    this.ui.preview.css('background-color', this.ui.input.val());
                    this.$colorInput.spectrum('set', this.ui.input.val());
                }, this), 500));

                this.ui.input.change(_.bind(function() {
                    //this.$colorInput.spectrum('hide');
                    this.trigger('change');
                }, this));
            }
        },

        _unbindEvents: function() {
            if(!this.disabled) {
                this.ui.input.off();
                this.$colorInput.off();
            }
        },

        onDestroy: function() {
            this._unbindEvents();
        },

        getValue: function() {
            var value = null;

            if(this.fieldName) {
                value = {
                    name  : this.fieldName,
                    value : this.ui.input.val()
                };
            }

            return value;
        }
    });
});