define(['backbone', 'marionette', './app'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        ui : {
            inputs : 'input[type="checkbox"]'
        },

        fieldName : null,

        initialize: function(options) {
            this.bindUIElements();

            this.fieldName = options.fieldName || null;

            this._bindEvents();
        },

        _bindEvents: function() {
            this.ui.inputs.on('change', _.bind(function() {
                this.trigger('change');
            }, this));
        },

        _unbindEvents: function() {
            this.ui.inputs.off();
        },

        onDestroy: function(){
            this._unbindEvents();
        },

        getValue: function() {
            var value = null;

            if(this.fieldName) {
                value = {
                    name  : this.fieldName,
                    value : this.ui.inputs.is(':checked')
                };
            }

            return value;
        }
    });
});