/* global define */
define([
    'backbone',
    'marionette',
    './app',
    'bootstrap'
], function(Backbone, Marionette, app) {
    /**
     * Module that handles wrapping bootstrap widgets on load.
     * @module bootstrap
     */
    'use strict';

    /**
     * @constructor
     */
    return Backbone.Marionette.ItemView.extend({
        initialize: function(options) {
            // Wrap modals links.
            if($.fn.modal) {
                this.$el.find('.modal').modal({ show : false });
            }

            // Wrap popovers.
            if($.fn.popover) {
                this.$el.find('[rel~="popover"]').popover();

                this.listenTo(app.vent, 'form:submit:success', this.onFormSubmitSuccess);
            }

            // Wrap popovers.
            if($.fn.tooltip) {
                this.$el.find('[rel~="tooltip"]').tooltip();
            }

            if($.fn.dropdown) {
                this.$el.find('.dropdown-toggle').dropdown();
            }
        },

        onFormSubmitSuccess: function(options) {
            this.$el.find('[rel~="popover"]').popover();
        },

        onDestroy: function() {
            this.stopListening();
        }
    });
});
