/* global define */
define(['backbone', 'marionette', './app'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        messages : null,

        initialize: function(options) {
            this.messages = options.messages || [];

            // When notification module has loaded, show flash messages from session.
            app.vent.on('module:load:notification', _.bind(function() {
                this._showMessages();
            }, this));
        },

        _showMessages: function() {
            if(this.messages.length > 0) {
                _.each(this.messages, function(message) {
                    app.vent.trigger('notification', message);
                });
            }
        }
    });
});
