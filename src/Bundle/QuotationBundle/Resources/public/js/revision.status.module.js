/* global define */
define([
    'backbone', 'marionette', 'underscore', 'klaro_routing', './helpers/revision.modal.status', './helpers/revision.modal.create', './models/revision.model', './models/constants', './templates/revision.status', './app', 'bootstrap'
], function(
    Backbone, Marionette, _, Routing, StatusModal, CreateModal, RevisionModel, Constants, PopoverTemplate, app
) {
    'use strict';

    var StatusView;

    StatusView = Backbone.Marionette.ItemView.extend({
        ui : {
            statusText      : '.js-status-text',
            orderStatusText : '.js-order-status-text'
        },

        qualifiers: null,
        qualifierOptions: null,
        status : null,
        orderStatus : null,

        events: {
            'click' : 'launchModal'
        },

        modelEvents: {
            'change' : 'render'
        },

        launchModal: function(e) {
            e.preventDefault();

            StatusModal.launchModal(this.qualifiers, this.qualifierOptions)
                .done(_.bind(function(result, oldData, startNew) {
                    var newStatus = result.status,
                        oldStatus = oldData.status,
                        newTitle = result.title,
                        newOrderStatus = result.order_status,
                        oldOrderStatus = oldData.order_status,
                        oldTitle = oldData.title,

                        doRefresh = _.bind(function() {
                            if(oldStatus == 'in_process' && oldStatus != newStatus ||
                                oldStatus != 'in_process' && newStatus == 'in_process' ||
                                newStatus == 'won' && newOrderStatus != oldOrderStatus ||
                                newTitle !== oldTitle) {
                                window.location.href = window.location.pathname + window.location.search;
                            }
                        }, this);

                    this.model.set(result);

                    app.vent.trigger('revision:status:change', result);

                    if(startNew) {
                        CreateModal.launchModal(this.model.get('quotation_id'), this.model.get('revision_id'))
                            .done(function(result) {
                                window.location.href = Routing.generate('klaro_quotation_edit_revision', {
                                    quotationId : result.quotation_id,
                                    revisionId : result.revision_id
                                });
                            })
                            .fail(doRefresh);
                    }
                    else {
                        doRefresh();
                    }
                }, this))
                .fail(function() {
                });
        },

        initialize: function(options) {
            this.qualifiers = options.qualifiers;
            this.qualifierOptions = options.qualifierOptions;
            this.status = options.status;
            this.orderStatus = options.orderStatus;

            this.bindUIElements();

            this.$el.attr('disabled', false);
        },

        render: function() {
            var status = this.model.get('status'),
                option = status in Constants.status ? Constants.status[status] : null,
                orderStatus = this.model.get('order_status'),
                orderStatusText = '-';

            if(!option) {
                option = Constants.status[_.first(_.keys(Constants.status))];
            }

            if(Constants.order_status[orderStatus]) {
                orderStatusText = Constants.order_status[orderStatus];
            }

            this.ui.statusText.text(option);

            this.ui.orderStatusText.text('(Order: ' + orderStatusText + ')');

            if(status == 'won') {
                this.ui.orderStatusText.show();
            }
            else {
                this.ui.orderStatusText.hide();
            }

            this.$el.popover('destroy').popover({
                trigger: 'hover',
                content: PopoverTemplate.popover({
                    model: this.model.toJSON(),
                    qualifiers: this.qualifiers,
                    qualifierOptions: this.qualifierOptions,
                    status: this.status,
                    orderStatus: this.orderStatus
                }),
                placement:'bottom',
                html: true
            });
        }
    });

    return Backbone.Marionette.ItemView.extend({
        revision : null,
        qualifiers: null,
        qualifierOptions: null,
        status : null,
        orderStatus : null,

        statusView : null,

        initialize: function(options) {
            this.revision = RevisionModel.getInstance().set(options.revision);
            this.qualifiers = options.qualifiers;
            this.qualifierOptions = options.qualifierOptions;
            this.status = options.status;
            this.orderStatus = options.orderStatus;

            this.statusView = new StatusView({
                el : this.$el,
                model : this.revision,
                qualifiers : this.qualifiers,
                qualifierOptions : this.qualifierOptions,
                status : this.status,
                orderStatus : this.orderStatus
            }).render();
        }
    });
});
