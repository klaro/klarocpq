define(['backbone', 'marionette', './app'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        ui : {
            inputs : 'input[type="radio"]'
        },

        fieldName : null,

        initialize: function(options) {
            this.bindUIElements();

            this.fieldName = options.fieldName || null;

            this._bindEvents();
        },

        _bindEvents: function() {
            this.ui.inputs.on('change', _.bind(function() {
                this.trigger('change');
            }, this));
        },

        _unbindEvents: function() {
            this.ui.inputs.off();
        },

        onDestroy: function() {
            this._unbindEvents();
        },

        getValue: function() {
            var value = null,
                $checked;

            if(this.fieldName) {
                $checked = this.$el.find('input[type="radio"]:checked');

                value = {
                    name  : this.fieldName,
                    value : $checked.length > 0 ? $checked.val() : null
                };
            }

            return value;
        }
    });
});