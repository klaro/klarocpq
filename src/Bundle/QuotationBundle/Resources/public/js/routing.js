/* global define */
define(['fos_routing_data'], function() {
    // Wrapper for FOS Routing. After this point, paths data should be set in the
    // fos_routing_data dependency so just return an instance to it.
    return fos.Router.getInstance();
});