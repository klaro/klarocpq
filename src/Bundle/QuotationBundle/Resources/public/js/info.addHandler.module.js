define([
    'backbone',
    'marionette',
    './app',
    'klaro_routing',
    './templates/info.handlers',
    'select2',
    'bootstrap'
], function(
    Backbone,
    Marionette,
    app,
    Routing,
    Template
) {
    'use strict';

    var ModalView,
        HandlerModel,
        HandlerCollection,
        CurrentHandlerView,
        CurrentHandlerCollectionView;

    HandlerModel = Backbone.Model.extend({
        defaults : {
            id       : null,
            title    : '',
            fullname : '',
            email    : ''
        }
    });

    HandlerCollection = Backbone.Collection.extend({
        model : HandlerModel
    });

    ModalView = Backbone.Marionette.ItemView.extend({
        className : 'modal fade',

        template : Template.modal,

        handlerView : null,

        currentHandlers : null,
        quotationId : null,

        ui: {
            saveButton : '.js-save-changes',
            search     : '.js-user-search',
            icon       : '.js-user-icon'
        },

        events: {
            'click @ui.saveButton' : 'saveModal',
            'change @ui.search'    : 'updateSearch'
        },

        initialize: function(options) {
            this.quotationId = options.quotationId;

            this.render();

            this.currentHandlers = this.collection.toJSON();
        },

        onRender: function() {
            this.bindUIElements();

            this.handlerView = new CurrentHandlerCollectionView({
                el : this.$el.find('.js-current-handlers'),
                collection : this.collection
            });

            this.handlerView.render();

            // Get list of users.
            $.ajax({
                url: Routing.generate('klaro_quotation_api_get_users'),
                dataType: 'json',
                type: 'GET',
                success: _.bind(function(result) {
                    var currentHandlerIds = _.pluck(this.currentHandlers, 'id'),
                        mappedData = [];

                    _.each(result.data, function(obj) {
                        if(_.contains(currentHandlerIds, obj.id) !== true) {
                            mappedData.push({
                                id: obj.id,
                                text: obj.fullname + ' (' + obj.email + ')',
                            });
                        }
                    });

                    this.ui.search.select2({
                        multiple: true,
                        data: mappedData
                    });

                    this.ui.icon.removeClass('fa fa-spinner fa-spin').addClass('fa fa-user');
                    this.ui.search.prop('disabled', false);

                    this.ui.search.select2('val', currentHandlerIds);
                }, this),
                error: _.bind(function() {
                    alert('Could not load users!');

                    this.$el.modal('hide');
                }, this)
            });

            this.$el.modal('show');

            // Handle the element deletion after hiding.
            this.$el.on('hidden.bs.modal', _.bind(this.destroyModal, this));

            // Add to DOM.
            this.$el.appendTo(document.body);
        },

        saveModal: function(e) {
            e.preventDefault();

            this.ui.saveButton.button('loading');

            $.ajax({
                url: Routing.generate('klaro_quotation_api_post_quotation_users', {quotationId : this.quotationId}),
                data: {
                    userIds: _.pluck(this.collection.toJSON(), 'id')
                },
                dataType: 'html',
                type: 'POST',
                success: _.bind(function(data) {
                    window.location.reload();
                }, this),
                error: _.bind(function() {
                    alert('Saving failed!');
                }, this),
                complete: _.bind(function() {
                    this.$el.modal('hide');
                }, this)
            });
        },

        updateSearch: function(e) {
            if(e.added) {
                this.collection.add(e.added);
            }

            if(e.removed) {
                var found = _.find(this.currentHandlers, function(obj) {
                    return e.removed.id === obj.id;
                });

                if(!found) {
                    this.collection.remove(e.removed);
                }
            }
        },

        destroyModal: function() {
            // Remove element from DOM after the modal is hidden.
            this.destroy();
        },

        onDestroy: function() {
            this.$el.off();

            this.handlerView.destroy();
        }
    });

    CurrentHandlerView = Backbone.Marionette.ItemView.extend({
        tagName : 'li',
        template : _.template('<%= model.title %>'),

        serializeData: function() {
            return {
                model : this.model.toJSON()
            };
        }
    });

    CurrentHandlerCollectionView = Backbone.Marionette.CollectionView.extend({
        childView : CurrentHandlerView
    });

    return Backbone.Marionette.ItemView.extend({
        currentHandlers : null,

        quotationId : null,

        ui : {
            button   : '.js-add-handler-button',
            template : '.js-add-handler-modal-template'
        },

        events: {
            'click @ui.button' : 'launchModal'
        },

        initialize: function(options) {
            this.bindUIElements();

            this.quotationId = options.quotationId;

            this.currentHandlers = options.currentHandlers;
        },

        launchModal: function(e) {
            e.preventDefault();

            this.openModal();
        },

        openModal: function() {
            var collection = new HandlerCollection(this.currentHandlers);

            new ModalView({
                quotationId : this.quotationId,
                collection : collection
            });
        }
    });
});
