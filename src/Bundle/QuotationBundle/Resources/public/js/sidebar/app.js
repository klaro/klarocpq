define(['backbone', 'underscore', 'klaro_routing', 'klaro_quotation'], function(Backbone, _, Routing, QuotationApp) {
    "use strict";

    var SidebarClosedView = Backbone.View.extend({
        events: {
            'click .js-open-sidebar-link' : 'openSidebar'
        },

        openSidebar: function(e) {
            e.preventDefault();

            app.vent.trigger('open');
        },

        render: function() {
            this.$el.addClass('klaro-sidebar-closed');
            this.$el.html(this.template({
                config : app.Defaults.config
            }));

            return this;
        },

        remove: function() {
            this.$el.removeClass('klaro-sidebar-closed');

            this.$el.empty();
            this.stopListening();

            return this;
        },

        initialize: function() {
            this.template = _.template('<div class="title"><a href="#" class="js-open-sidebar-link"><i class="fa fa-th"></i> ' + app.Defaults.title + '</a></div>');

            return this;
        }
    }),

    SidebarContentView = Backbone.View.extend({
        events: {
        },

        render: function() {
            this.$el.html(this.cachedData);

            return this;
        },

        renderLoading: function() {
            this.$el.html(this.loadingTemplate());
        },

        remove: function() {
            this.$el.empty();
            this.stopListening();

            return this;
        },

        refresh: function(needsRefresh) {
            if(needsRefresh) {
                var self = this,
                    url = Routing.generate(app.Defaults.route, _.extend(app.Defaults.routeParams, {type : app.Defaults.activeTab}));

                $.get(url, null, function(data) {
                    self.cachedData = data;
                    self.render();
                }, 'html');
            }
            else {
                this.render();
            }

            this.listenTo(app.vent, 'collapse', this.toggleExpanded);
        },

        toggleExpanded: function(expanded, maxHeight) {
            if(expanded) {
                this.$el.height(maxHeight);
                this.$el.addClass('expanded');
            }
            else {
                this.$el.css('height', '');
                this.$el.removeClass('expanded');
            }
        },

        changePage: function() {
            this.renderLoading();

            this.refresh(true);
        },

        initialize: function() {
            this.loadingTemplate = _.template('<i class="fa fa-spinner"></i> Loading...');

            this.cachedData = '';

            this.renderLoading();

            this.listenTo(app.vent, 'tab', this.changePage);

            return this;
        }
    }),

    SidebarFooterView = Backbone.View.extend({
        events: {
        },

        render: function(expanded) {
            this.$el.html(this.template({
                expanded : expanded
            }));

            return this;
        },

        remove: function() {
            this.$el.empty();
            this.stopListening();

            return this;
        },

        initialize: function() {
            this.template = _.template('<a href="#" class="js-expand-sidebar-link">' +
                '<% if(!expanded) { %>' +
                '<i class="fa fa-caret-down"></i> Expand' +
                '<% } else { %>' +
                '<i class="fa fa-caret-up"></i> Collapse' +
                '<% } %>' +
                '</a>');

            this.listenTo(app.vent, 'collapse', this.render);

            return this;
        }
    }),

    SideBarTabView = Backbone.View.extend({
        events: {
            'click .js-open-tab' : 'openContentTab'
        },

        initialize: function(options) {
            this.template = _.template('<% _.each(config, function(item, key){ %><li<% if(active === key) { %> class="active"<% }%>>' +
                ' <a href="#" class="js-open-tab" data-target="<%= key %>"><%= item.title %></a>' +
                '</li><% }); %>');
        },

        openContentTab: function(e) {
            e.preventDefault();

            var target = $(e.target).data('target');

            if(target !== app.Defaults.activeTab) {
                this.setActiveTab(target);
            }
        },

        setActiveTab: function(active) {
            app.Defaults.activeTab = active;

            app.vent.trigger('tab');

            this.render();
        },

        render: function() {
            this.$el.html(this.template({
                config : app.Defaults.config,
                active : app.Defaults.activeTab
            }));
        }
    }),

    SidebarOpenView = Backbone.View.extend({
        events: {
            'click .js-expand-sidebar-link' : 'expandSidebar',
            'click .js-close-sidebar-link'  : 'closeSidebar'
        },

        closeSidebar: function(e) {
            e.preventDefault();

            app.vent.trigger('close');
        },

        expandSidebar: function(e) {
            e.preventDefault();

            this.expanded = !this.expanded;

            app.Helpers.createCookie(app.Defaults.expandedCookie, (this.expanded ? '1' : '0'));

            app.vent.trigger('collapse', this.expanded, this.$el.parent().height() - 100);
        },

        render: function(needsRefresh) {
            this.$el
                .addClass('klaro-sidebar-open')
                .addClass('col-sm-3')
                .addClass('col-md-3')
                .addClass('col-lg-3');

            this.$el.html(this.template({
                config : app.Defaults.config,
                active : app.Defaults.activeTab
            }));

            if(!this.tabView) {
                this.tabView = new SideBarTabView();
            }

            if(!this.contentView) {
                this.contentView = new SidebarContentView();
            }

            if(!this.footerView) {
                this.footerView = new SidebarFooterView();
            }

            this.tabView.setElement(this.$('.sidebar-tabs')).render();
            this.contentView.setElement(this.$('.content-area')).refresh(needsRefresh);
            this.footerView.setElement(this.$('.content-footer'));

            app.vent.trigger('collapse', this.expanded, this.$el.parent().height() - 100);

            this.listenTo(app.vent, 'refresh', this.refresh);

            return this;
        },

        remove: function() {
            this.$el
                .removeClass('klaro-sidebar-open')
                .removeClass('col-sm-3')
                .removeClass('col-md-3')
                .removeClass('col-lg-3');

            this.$el.empty();
            this.stopListening();

            if(this.tabView) {
                this.tabView.remove();
                this.tabView = null;
            }

            if(this.contentView) {
                this.contentView.remove();
                this.contentView = null;
            }

            if(this.footerView) {
                this.footerView.remove();
                this.footerView = null;
            }

            return this;
        },

        refresh: function() {
            if(this.contentView) {
                this.contentView.refresh(true);
            }
        },

        initialize: function(options) {
            this.template = _.template('<div class="content-wrapper">' +
                '<div class="content-header">' +
                '<a href="#" class="close-link js-close-sidebar-link"><i class="fa fa-times"></i></a>' +
                '<ul class="nav nav-tabs sidebar-tabs">' +
                '</ul>' +
                '</div>' +

                '<div class="content-area">' +
                '</div>' +

                '<div class="content-footer">' +
                '</div>'
            );

            this.tabView = null;
            this.contentView = null;
            this.footerView = null;

            this.expanded = options.expanded || false;

            return this;
        }
    }),

    SidebarView = Backbone.View.extend({
        events: {
        },

        render: function() {
            this.adjustMainContainerSize();

            if(this.closed) {
                this.openView.remove();
                this.closedView.setElement(this.el).render();
                this.needsRefresh = true;
            }
            else {
                this.closedView.remove();
                this.openView.setElement(this.el).render(this.needsRefresh);
                //this.firstTime = false;
            }

            return this;
        },

        adjustMainContainerSize: function() {
            if(this.closed) {
                this.$mainContainer
                    .removeClass('col-sm-6')
                    .removeClass('col-md-6')
                    .removeClass('col-lg-7')
                    .addClass('col-sm-9')
                    .addClass('col-md-9')
                    .addClass('col-lg-10');
            }
            else {
                this.$mainContainer
                    .addClass('col-sm-6')
                    .addClass('col-md-6')
                    .addClass('col-lg-7')
                    .removeClass('col-sm-9')
                    .removeClass('col-md-9')
                    .removeClass('col-lg-10');
            }
        },

        remove: function() {
            this.$el.empty();
            this.stopListening();

            this.closedView.remove();
            this.openView.remove();

            return this;
        },

        openSidebar: function() {
            this.closed = false;
            this.render();

            app.Helpers.createCookie(app.Defaults.closedCookie, '0');
        },

        closeSidebar: function() {
            this.closed = true;
            this.render();

            app.Helpers.createCookie(app.Defaults.closedCookie, '1');
        },

        refresh: function() {
            this.needsRefresh = true;
        },

        initialize: function(options) {
            this.$mainContainer = options.mainContainer;
            this.closed = (app.Helpers.readCookie(app.Defaults.closedCookie) !== '0');
            this.expanded = (app.Helpers.readCookie(app.Defaults.expandedCookie) !== '0');

            //this.firstTime = true;
            this.needsRefresh = true;

            this.closedView = new SidebarClosedView({
                el : this.el
            });
            this.openView = new SidebarOpenView({
                el : this.el,
                expanded : this.expanded
            });

            this.listenTo(app.vent, 'refresh', this.refresh);
            this.listenTo(app.vent, 'open', this.openSidebar);
            this.listenTo(app.vent, 'close', this.closeSidebar);

            return this;
        }
    }),

    app = {
        Models: {},
        Collections: {},
        Views: {},
        Routers: {},
        Templates: {},
        Defaults: {
            config : null,
            activeTab : null,
            route : null,
            routeParams : null,
            closedCookie : 'klaro.sidebar.closed',
            expandedCookie : 'klaro.sidebar.expanded',
            title: 'Sidebar'
        },
        Helpers: {
            // Cookie helpers taken from http://www.quirksmode.org/js/cookies.html

            createCookie: function(name, value, days) {
                var expires = "",
                    date;
                days = days || 1;
                if (days) {
                    date = new Date();
                    date.setTime(date.getTime()+(days*24*60*60*1000));
                    expires = "; expires="+date.toGMTString();
                }

                document.cookie = name+"="+value+expires+"; path=/";
            },

            readCookie: function readCookie(name) {
                var nameEQ = name + "=",
                    ca = document.cookie.split(';'),
                    i, c;
                for(i=0;i < ca.length;i++) {
                    c = ca[i];
                    while (c.charAt(0)===' ') {
                        c = c.substring(1,c.length);
                    }
                    if (c.indexOf(nameEQ) === 0) {
                        return c.substring(nameEQ.length,c.length);
                    }
                }
                return null;
            },

            eraseCookie: function (name) {
                this.Helpers.createCookie(name,"",-1);
            }
        },
        vent : null,
        init : function(options) {
            app.vent = QuotationApp.vent;

            options = options || {};

            _.extend(app.Defaults, options);

            if(_.keys(options.config).length > 0) {
                app.Defaults.activeTab = _.first(_.keys(app.Defaults.config));

                app.Views.main = new SidebarView({
                    mainContainer : $(options.el)
                });

                app.Views.main.$el.insertAfter(options.el);
                app.Views.main.render();

                QuotationApp.vent.on('form:submit:success', function() {
                    app.vent.trigger('refresh');
                });
            }
        }
    };

    return app;
});
