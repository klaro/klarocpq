/* global define */
define([
    'backbone', 'marionette', 'underscore', 'klaro_routing', './models/revision.collection', './models/constants', './templates/revision.compare', 'chosen', 'amcharts.serial', './app'
], function(
    Backbone, Marionette, _, Routing, RevisionCollection, Constants, Template, chosen, amChart, app
    ) {
    'use strict';

    var SummaryModel,
        RevisionListView,
        RevisionListOptionView,
        CompareChartView;

    SummaryModel = Backbone.Model.extend({
        defaults : {
            "quotation_id":        null,
            "revision_id":         null,
            "currency":            "EUR",
            "original_sales_price_with_tax": 0,
            "sales_price_with_tax": 0,
            "sales_price":          0,
            "sales_tax":            [],
            "cost_price":           0,
            "profit":               0,
            "margin":               0
        },

        url: function() {
            return Routing.generate('klaro_quotation_api_get_revision_summary', {
                quotationId : this.get('quotation_id'),
                revisionId  : this.get('revision_id')
            });
        },

        parse: function(response) {
            return response.data;
        }
    });

    RevisionListOptionView = Backbone.Marionette.ItemView.extend({
        tagName : 'option',

        template : _.template('Q<%= model.quotation_id %>_R<%= model.revision_id %> Quotation (<%= status %>)'),

        serializeData: function() {
            return {
                model : this.model.toJSON(),
                status : Constants.status[this.model.get('status')] || ''
            };
        },

        onRender: function() {
            this.$el.attr('value', this.model.get('revision_id'));
        }
    });

    RevisionListView = Backbone.Marionette.CompositeView.extend({
        childView : RevisionListOptionView,

        template : Template.revisionList,
        currentRevisionId : null,

        ui: {
            list   : '.js-revision-input-list',
            button : '.js-compare-button',
            text   : '.js-help-text'
        },

        events : {
            'click @ui.button' : 'onCompareClick'
        },

        initialize: function(options) {
            this.currentRevisionId = options.currentRevisionId;

            this.collection.fetch().done(_.bind(function() {
                // Remove current revision from list (no point to compare to self).
                var newCollection = this.collection.reject(_.bind(function(obj) {
                    return obj.get('revision_id') === this.currentRevisionId;
                }, this));

                this.collection.reset(newCollection);

                this.render();
            }, this));
        },

        onCompareClick: function(e) {
            e.preventDefault();

            this.triggerMethod('updateChart', this.ui.list.val());
        },

        attachHtml: function(collectionView, childView) {
            this.ui.list.append(childView.el);
        },

        onRender: function() {
            var disabled = this.collection.length < 1;

            this.ui.list.prop('disabled', disabled);
            this.ui.button.prop('disabled', disabled);

            if(disabled) {
                this.ui.text.text('No other revisions available!');
            }

            this.ui.list.chosen();
        }
    });

    CompareChartView = Backbone.Marionette.ItemView.extend({
        quotationId : null,
        currentRevisionId : null,

        initialize: function(options) {
            this.quotationId = options.quotationId;
            this.currentRevisionId = options.currentRevisionId;
        },

        compareTo: function(revisionId) {
            var summary1 = new SummaryModel({
                quotation_id : this.quotationId,
                revision_id  : this.currentRevisionId
            }),
                summary2 = new SummaryModel({
                quotation_id : this.quotationId,
                revision_id  : revisionId
            });

            Backbone.$.when(summary1.fetch(), summary2.fetch())
            .then(_.bind(function() {
                var chartData = [
                    { label: 'Sales Price', value0: summary1.get('sales_price_with_tax'), value1: summary2.get('sales_price_with_tax') },
                    { label: 'Cost Price',  value0: summary1.get('cost_price'), value1: summary2.get('cost_price') },
                    { label: 'Profit',      value0: summary1.get('profit'), value1: summary2.get('profit') }
                ];

                this.renderChart(chartData, summary1.get('currency'), this.currentRevisionId, revisionId);
            }, this), function() {
                alert('Error loading comparison data.');
            });
        },

        renderChart: function(chartData, currency, originalRevisionId, comparedRevisionId) {
            this.$el.css('height', '300px');

            var chartOptions = {
                "type": "serial",
                "dataProvider": chartData,
                "valueAxes": [{
                    "unit": " " + currency,
                    "position": "left"
                }],
                "startDuration": 1,
                marginBottom: 0,
                "graphs": [{
                    "balloonText": "[[category]] (R" + originalRevisionId + "): <b>[[value]] " + currency + "</b>",
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "title": "R" + originalRevisionId + " (current)",
                    "type": "column",
                    "valueField": "value0"
                }, {
                    "balloonText": "[[category]] (R" + comparedRevisionId + "): <b>[[value]] " + currency + "</b>",
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "title": "R" + comparedRevisionId,
                    "type": "column",
                    "valueField": "value1"
                }],
                "plotAreaFillAlphas": 0.1,
                "categoryField": "label",
                "categoryAxis": {
                    "gridPosition": "start"
                },
                "legend": {
                    "useGraphSettings": true
                },
                "numberFormatter": {
                    "precision": 0,
                    "decimalSeparator": ",",
                    "thousandsSeparator": " "
                }
            };

            this.chart = amChart.makeChart(this.$el[0], chartOptions);
        }
    });

    return Backbone.Marionette.LayoutView.extend({
        regions: {
            revisionList : '.js-revision-list',
            compareChart : '.js-compare-chart'
        },

        childEvents: {
            'updateChart' : 'onUpdateChart'
        },

        quotationId : null,
        currentRevisionId : null,
        previousRevisionId : null,

        initialize: function(options) {
            this.collection = new RevisionCollection({
                quotationId : options.quotationId
            });

            this.quotationId = options.quotationId;
            this.currentRevisionId = options.revisionId;

            this.getRegion('revisionList').attachView(new RevisionListView({
                el : this.getRegion('revisionList').$el,
                currentRevisionId : this.currentRevisionId,
                collection : this.collection
            }));

            this.chartView = new CompareChartView({
                el : this.getRegion('compareChart').$el,
                quotationId : this.quotationId,
                currentRevisionId : this.currentRevisionId
            });
            this.getRegion('compareChart').attachView(this.chartView);

            this.listenTo(app.vent, 'priceupdater:change', _.bind(this.onPriceUpdaterChange, this));
        },

        onUpdateChart: function(view, revisionId) {
            this.chartView.compareTo(revisionId);

            this.previousRevisionId = revisionId;
        },

        onPriceUpdaterChange: function() {
            if(this.previousRevisionId) {

                this.chartView.compareTo(this.previousRevisionId);
            }
        }
    });
});
