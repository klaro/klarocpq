/* global define */
define(['underscore', 'translator'], function(_, Translator) {
    'use strict';

    var modalTemplate,
        bodyView;

    modalTemplate = [
        '<div class="modal-dialog">',
            '<div class="modal-content">',
                '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                    '<h4 class="modal-title">' + Translator.trans('modal.create_revision.title', {}, 'KlaroQuotationBundle') + '</h4>',
                '</div>',
                '<div class="modal-body js-modal-body">',

                '</div>',
                '<div class="modal-footer">',
                    '<button type="button" class="btn btn-default" data-dismiss="modal">',
                        Translator.trans('modal.create_revision.buttons.cancel', {}, 'KlaroQuotationBundle'),
                    '</button>',
                    '<button type="button" class="btn btn-primary js-save-changes" data-loading-text="',
                        Translator.trans('modal.create_revision.buttons.save_loading', {}, 'KlaroQuotationBundle'),
                    '">',
                        Translator.trans('modal.create_revision.buttons.save', {}, 'KlaroQuotationBundle'),
                    '</button>',
                '</div>',
            '</div><!-- /.modal-content -->',
        '</div><!-- /.modal-dialog -->'
    ].join('');

    bodyView = [
        '<div class="form-group">',
            '<label>',
                Translator.trans('modal.create_revision.label.title', {}, 'KlaroQuotationBundle'),
            '</label>',

            '<input class="form-control js-revision-title" value="<%= model.title %>" />',
        '</div>',

        '<div class="form-group">',
            '<label>',
                Translator.trans('modal.create_revision.label.source', {}, 'KlaroQuotationBundle'),
            '</label>',

            '<select class="form-control js-revision-list">',
            '</select>',

        '</div>',

        '<hr />',

        '<div class="form-group">',
            '<label>',
                Translator.trans('modal.create_revision.label.status', {}, 'KlaroQuotationBundle'),
            '</label>',

            '<select class="form-control js-revision-status">',
                '<% _.each(statusOptions, function(status, id) { %>',
                    '<option value="<%= id %>"><%= status %></option>',
                '<% }); %>',
            '</select>',
        '</div>',

        '<div class="form-group">',
            '<label>',
                Translator.trans('modal.create_revision.label.order_status', {}, 'KlaroQuotationBundle'),
            '</label>',

            '<select class="form-control js-revision-order-status">',
                '<% _.each(orderStatusOptions, function(status, id) { %>',
                    '<option value="<%= id %>"><%= status %></option>',
                '<% }); %>',
            '</select>',
        '</div>'
    ].join('');

    return {
        modal : _.template(modalTemplate),
        body  : _.template(bodyView)
    };
});
