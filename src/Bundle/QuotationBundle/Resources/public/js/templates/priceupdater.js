/* global define */
define(['underscore'], function(_) {
    'use strict';

    var priceView,
        editView;

    priceView = [
        '<% if(Number(model.original_sales_price_with_tax).toFixed(0) == Number(model.sales_price_with_tax).toFixed(0)) { %>',
            '<%= model.original_price_formatted %> <%= model.currency %> ',
        '<% } else { %>',
            '<%= model.sales_price_formatted %> <%= model.currency %> ',
            '<span style="color: #990">(<%= model.diff_formatted %>%)</span>',
        '<% } %>'
    ].join('');

    editView = [
        '<div class="input-group">',
            '<input type="number" class="form-control input-sm js-price-input" min="0" step="10" style="font-size: 13px;"' +
                ' value="<%= Number(!model.custom_sales_price ? model.sales_price_with_tax : model.custom_sales_price).toFixed(0) %>" placeholder="Sales Price">',
            '<span class="input-group-btn">',
                '<button class="btn btn-primary btn-sm js-save-button" type="button"><i class="fa fa-check"></i></button>',
            '</span>',
        '</div>',
        '<p class="help-block"><small><a href="#" class="js-restore-button"><i class="fa fa-undo"></i> Restore original value</a></small></p>'
    ].join('');

    return {
        price : _.template(priceView),
        edit  : _.template(editView)
    };
});