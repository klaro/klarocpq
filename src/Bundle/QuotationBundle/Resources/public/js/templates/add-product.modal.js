/* global define */
define(['underscore', 'translator'], function(_, Translator) {
    'use strict';

    var modalTemplate,
        newProductView,
        orderProductsView,
        productInListView;

    modalTemplate = [
        '<div class="modal-dialog modal-lg">',
            '<div class="modal-content">',
                '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                    '<h4 class="modal-title"><%= title %></h4>',
                '</div>',

                '<div class="modal-body js-modal-body">',

                '</div>',

                '<div class="modal-footer">',
                    '<div class="row">',
                        '<div class="col-md-6" style="text-align: left;">',
                            '<button type="button" class="btn btn-default js-change-order"><i class="fa fa-reorder"></i> ',
                                Translator.trans('modal.modify_product.buttons.manage_products', {}, 'KlaroQuotationBundle'),
                            '</button>',
                            '<button type="button" class="btn btn-default js-add-product"><i class="fa fa-plus-circle"></i> ',
                                Translator.trans('modal.modify_product.buttons.add_product', {}, 'KlaroQuotationBundle'),
                            '</button>',
                        '</div>',
                        '<div class="col-md-6">',
                            '<button type="button" class="btn btn-default" data-dismiss="modal">',
                                Translator.trans('modal.modify_product.buttons.cancel', {}, 'KlaroQuotationBundle'),
                            '</button>',
                            '<button type="button" class="btn btn-primary js-save-changes" data-loading-text="',
                                Translator.trans('modal.modify_product.buttons.save_loading', {}, 'KlaroQuotationBundle'),
                                '"',
                                '<% if(disabled) { %>',
                                    ' disabled="true"',
                                '<% } %>',
                            '>',
                                Translator.trans('modal.modify_product.buttons.save', {}, 'KlaroQuotationBundle'),
                            '</button>',
                        '</div>',
                    '</div>',
                '</div>',
            '</div><!-- /.modal-content -->',
        '</div><!-- /.modal-dialog -->'
    ].join('');

    newProductView = [
        '<h4>' + Translator.trans('modal.modify_product.new_product.title', {}, 'KlaroQuotationBundle') + '</h4>',
        '<p>' + Translator.trans('modal.modify_product.new_product.description', {}, 'KlaroQuotationBundle') + '</p>',
        '<hr />',
        '<div class="form-group">',
            '<label>' + Translator.trans('modal.modify_product.new_product.label.title', {}, 'KlaroQuotationBundle') + '</label>',
                '<input class="form-control js-new-product-title" name="productTitle" value="<%= model.title %>">',
            '</input>',
        '</div>',

        '<div class="form-group">',
            '<label>' + Translator.trans('modal.modify_product.new_product.label.type', {}, 'KlaroQuotationBundle') + '</label>',

            '<select class="form-control js-new-product" name="productType">',
                '<% _.each(productNames, function(productName, ref) { %>',
                    '<option value="<%= ref %>"><%= productName %></option>',
                '<% }); %>',
            '</select>',
        '</div>',

        '<div class="form-group js-use-source-group">',
            '<div class="checkbox">',
                '<label>',
                    '<input type="checkbox" class="js-use-source" name="copyFromExistingProduct"> ',
                    Translator.trans('modal.modify_product.new_product.label.copy_from_existing', {}, 'KlaroQuotationBundle'),
                '</label>',
            '</div>',
        '</div>',

        '<div class="form-group js-other-products-group">',
            '<label>' + Translator.trans('modal.modify_product.new_product.label.source', {}, 'KlaroQuotationBundle') + '</label>',

            '<select class="form-control js-other-products" name="sourceProduct">',
            '</select>',
        '</div>'

    ].join('');

    orderProductsView = [
        '<h4>' + Translator.trans('modal.modify_product.manage_products.title', {}, 'KlaroQuotationBundle') + '</h4>',
        '<p>' + Translator.trans('modal.modify_product.manage_products.description', {}, 'KlaroQuotationBundle') + '</p>',
        '<hr />',
        '<ul class="product-list-sort js-product-list">',
        '</ul>'
    ].join('');

    productInListView = [
        '<span class="product-list-sort-item-handle">',
            '<% if(model.remove) { %>',
                '<i class="fa fa-trash-o"></i>',
            '<% } else { %>',
                '<i class="fa fa-bars"></i>',
            '<% } %>',
        '</span> ',
        '<span class="product-list-sort-item-title js-title">',
            '<% if(model.remove || editing !== true) { %>',
                '<%= model.title %>',
                '<% if(model.is_new) { %>',
                    '<br/><small class="source"><%= sourceTitle %></small>',
                '<% } %>',
            '<% } else { %>',
                '<input type="text" class="form-control js-title-input" value="<%= model.title %>" />',
            '<% } %>',
        '</span>',
        '<% if(model.type) { %>',
            ' <span class="product-list-sort-item-type"><%= model.type %></span>',
        '<% } %>',
        '<span class="product-list-sort-buttons">',
            '<a href="#" class="btn btn-success btn-xs product-list-sort-button js-edit-save">',
                '<i class="fa fa-check"></i>',
            '</a>',
            '<a href="#" class="btn btn-primary btn-xs product-list-sort-button js-edit-title">',
                '<i class="fa fa-pencil"></i>',
            '</a>',
            '<a href="#" class="btn btn-default btn-xs product-list-sort-button js-edit-clone">',
                '<i class="fa fa-clone"></i>',
            '</a>',
            '<a href="#" class="btn <% if(model.remove) { %>btn-danger<% } else { %>btn-default<% } %> btn-xs product-list-sort-button js-remove">',
                '<i class="fa fa-trash-o"></i>',
            '</a>',
        '</span>'
    ].join('');

    return {
        modal : _.template(modalTemplate),
        newProduct  : _.template(newProductView),
        orderProducts: _.template(orderProductsView),
        productInList : _.template(productInListView)
    };
});
