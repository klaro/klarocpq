/* global define */
define(['underscore', 'translator'], function(_, Translator) {
    'use strict';

    var modalTemplate,
        bodyTemplate;

    modalTemplate = [
        '<div class="modal-dialog">',
            '<div class="modal-content">',
                '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                    '<h4 class="modal-title">' + Translator.trans('modal.import_product_data.title', {}, 'KlaroQuotationBundle') + '</h4>',
                '</div>',

                '<div class="modal-body js-modal-body">',

                '</div>',

                '<div class="modal-footer">',
                    '<div class="row">',
                        '<div class="col-md-6" style="text-align: left;">',
                        '</div>',
                        '<div class="col-md-6">',
                            '<button type="button" class="btn btn-default" data-dismiss="modal">',
                                Translator.trans('modal.import_product_data.buttons.cancel', {}, 'KlaroQuotationBundle'),
                            '</button>',
                            '<button type="button" class="btn btn-primary js-save-changes" data-loading-text="',
                                Translator.trans('modal.import_product_data.buttons.save_loading', {}, 'KlaroQuotationBundle'),
                            '">',
                                Translator.trans('modal.import_product_data.buttons.save', {}, 'KlaroQuotationBundle'),
                            '</button>',
                        '</div>',
                    '</div>',
                '</div>',
            '</div><!-- /.modal-content -->',
        '</div><!-- /.modal-dialog -->'
    ].join('');

    bodyTemplate = [
        '<div class="form-group">',
            '<label>' + Translator.trans('modal.import_product_data.label.source') + '</label>',

            '<select class="form-control js-import-sources">',
            '</select>',
            '<p class="help-block">' + Translator.trans('modal.import_product_data.description') + '</p>',
        '</div>'
    ].join('');

    return {
        modal: _.template(modalTemplate),
        bodyView: _.template(bodyTemplate)
    };
});
