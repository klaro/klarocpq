/* global define */
define(['underscore', 'translator'], function(_, Translator) {
    'use strict';

    var modalTemplate,
        bodyView;

    modalTemplate = [
        '<div class="modal-dialog modal-lg">',
            '<div class="modal-content">',
                '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                    '<h4 class="modal-title">' + Translator.trans('modal.revision_status.title', {}, 'KlaroQuotationBundle') + '</h4>',
                '</div>',

                '<div class="modal-body js-modal-body">',
                    '<p style="text-align: center;"><i class="fa fa-spinner"></i> ',
                    Translator.trans('modal.revision_status.loading_body', {}, 'KlaroQuotationBundle'),
                '</p></div>',

                '<div class="modal-footer">',
                    '<button type="button" class="btn btn-default" data-dismiss="modal">',
                        Translator.trans('modal.revision_status.buttons.cancel', {}, 'KlaroQuotationBundle'),
                    '</button>',
                    '<button type="button" class="btn btn-primary js-save-changes" data-loading-text="',
                        Translator.trans('modal.revision_status.buttons.save_loading', {}, 'KlaroQuotationBundle'),
                    '">',
                        Translator.trans('modal.revision_status.buttons.save', {}, 'KlaroQuotationBundle'),
                    '</button>',
                '</div>',
            '</div><!-- /.modal-content -->',
        '</div><!-- /.modal-dialog -->'
    ].join('');

    bodyView = [
        '<form class="form-horizontal">',
            '<div class="form-group">',
                '<label class="col-sm-2 control-label">',
                    Translator.trans('modal.revision_status.label.title', {}, 'KlaroQuotationBundle'),
                '</label>',
                '<div class="col-sm-10">',
                    '<div class="input-group">',
                        '<div class="input-group-addon"><%= model.identifier %></div>',
                        '<input class="form-control js-revision-title" value="<%= model.title %>"/>',
                    '</div>',
                '</div>',
            '</div>',

            '<div class="form-group">',
                '<label class="col-sm-2 control-label">',
                    Translator.trans('modal.revision_status.label.status', {}, 'KlaroQuotationBundle'),
                '</label>',

                '<div class="col-sm-10">',
                    '<select class="form-control js-revision-status">',
                        '<% _.each(model.states, function(status, id) { %>',
                            '<option value="<%= id %>"><%= status %></option>',
                        '<% }); %>',
                    '</select>',
                '</div>',
            '</div>',

            '<% if(model.won) { %>',
                '<div class="form-group">',
                    '<label class="col-sm-2 control-label">',
                        Translator.trans('modal.revision_status.label.order_status', {}, 'KlaroQuotationBundle'),
                    '</label>',

                    '<div class="col-sm-10">',
                        '<select class="form-control js-revision-order-status">',
                            '<% _.each(model.order_states, function(status, id) { %>',
                                '<option value="<%= id %>"><%= status %></option>',
                            '<% }); %>',
                        '</select>',
                    '</div>',
                '</div>',
            '<% } %>',

            '<% if(qualifiers && qualifierOptions) { %>',
                '<% _.each(qualifiers, function(qualifierName, qualifierId) { %>',
                    '<div class="form-group">',
                        '<label class="col-sm-2 control-label"><%= qualifierName %></label>',
                        '<div class="col-sm-10">',
                            '<select name="<%= qualifierId %>" class="form-control js-revision-qualifier js-revision-qualifier-<%= qualifierId %>">',
                                '<% _.each(qualifierOptions[qualifierId], function(optionName, optionId) { %>',
                                    '<option value="<%= optionId %>"><%= optionName %></option>',
                                '<% }); %>',
                            '</select>',
                        '</div>',
                    '</div>',
                '<% }); %>',
            '<% } %>',

            '<div class="form-group">',
                '<label class="col-sm-2 control-label">',
                    Translator.trans('modal.revision_status.label.log_message', {}, 'KlaroQuotationBundle'),
                '</label>',
                '<div class="col-sm-10">',
                    '<textarea class="form-control js-revision-text" placeholder="Log message" rows="4"></textarea>',
                '</div>',
            '</div>',

            '<div class="form-group">',
                '<div class="col-sm-offset-2 col-sm-10">',
                    '<div class="checkbox">',
                        '<label>',
                            '<input type="checkbox" class="js-create-new-revision"> ',
                            Translator.trans('modal.revision_status.label.create_new_revision', {}, 'KlaroQuotationBundle'),
                        '</label>',
                    '</div>',
                '</div>',
            '</div>',

        '</form>'
    ].join('');

    return {
        modal : _.template(modalTemplate),
        body  : _.template(bodyView)
    };
});
