/* global define */
define(['underscore', 'translator'], function(_, Translator) {
    'use strict';

    var modalView;

    modalView = [
        '<div class="modal-dialog modal-lg">',
            '<div class="modal-content">',
                '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                    '<h4 class="modal-title">' + Translator.trans('info.add_handler_modal.title', {}, 'KlaroQuotationBundle') + '</h4>',
                '</div>',
                '<div class="modal-body">',
                    '<div class="row">',
                        '<div class="col-xs-4">',
                            '<h4>' + Translator.trans('info.add_handler_modal.current_handlers', {}, 'KlaroQuotationBundle') + '</h4>',

                            '<ul class="list-unstyled js-current-handlers">',
                            '</ul>',
                        '</div>',

                        '<div class="col-xs-8">',
                            '<h4>' + Translator.trans('info.add_handler_modal.new_handlers', {}, 'KlaroQuotationBundle') + '</h4>',

                            '<div class="form-group">',
                                '<div class="input-group select2-bootstrap-prepend">',
                                    '<div class="input-group-addon"><i class="js-user-icon fa fa-spinner"></i></div>',

                                    '<input type="text" class="form-control js-user-search" placeholder="' + Translator.trans('info.add_handler_modal.search_users', {}, 'KlaroQuotationBundle') + '" disabled="true">',
                                '</div>',
                            '</div>',
                        '</div>',
                    '</div>',
                '</div>',
                '<div class="modal-footer">',
                    '<button type="button" class="btn btn-default" data-dismiss="modal">',
                        Translator.trans('info.add_handler_modal.cancel', {}, 'KlaroQuotationBundle'),
                    '</button>',
                    '<button type="button" class="btn btn-primary js-save-changes" data-loading-text="' + Translator.trans('info.add_handler_modal.saving', {}, 'KlaroQuotationBundle') + '">',
                        Translator.trans('info.add_handler_modal.save', {}, 'KlaroQuotationBundle'),
                    '</button>',
                '</div>',
            '</div>',
        '</div>',
    ].join('');

    return {
        modal  : _.template(modalView)
    };
});
