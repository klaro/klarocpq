/* global define */
define(['underscore'], function(_) {
    'use strict';

    var modalView,
        optionView;

    modalView = [
        '<div class="modal-dialog">',
            '<div class="modal-content">',
                '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                    '<h4 class="modal-title"><%= title %></h4>',
                '</div>',
                '<div class="modal-body js-modal-body">',
                    '<ul class="nav nav-pills nav-stacked js-option-list">',
                    '</ul>',
                '</div>',
                '<div class="modal-footer">',
                    '<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>',
                '</div>',
            '</div>',
        '</div>'
    ].join('');

    optionView = [
        '<a href="#" class="js-link" data-id="<%= id %>">',
            '<i class="fa fa-angle-right"></i> <%= title %>',
        '</a>'
    ].join('');

    return {
        modal  : _.template(modalView),
        option : _.template(optionView)
    };
});