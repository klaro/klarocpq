/* global define */
define(['underscore'], function(_){
    'use strict';

    var buttonView,
        optionView;

    buttonView = [
        '<a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">',
            '<i class="fa fa-tags"></i> <strong><%= value %></strong>',
            '<span class="caret"></span>',
        '</a>',
        '<ul class="dropdown-menu js-revision-button-options">',
        '</ul>'
    ].join('');

    optionView = [
        '<a href="#"><%= title %></a>'
    ].join('');

    return {
        buttonView : _.template(buttonView),
        optionView : _.template(optionView)
    };
});