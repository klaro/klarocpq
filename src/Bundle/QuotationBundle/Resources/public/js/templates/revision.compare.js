/* global define */
define(['underscore'], function(_){
    'use strict';

    var revisionList = [
        '<div class="input-group">',
            '<span class="input-group-addon"><i class="fa fa-bar-chart"></i></span>',
            '<select class="form-control js-revision-input-list">',
            '</select>',
            '<span class="input-group-btn">',
                '<button class="btn btn-default js-compare-button" type="button">Compare</button>',
            '</span>',
        '</div>',
        '<p class="help-block js-help-text">Choose a revision from the list to compare to.</p>'
    ].join('');

    return {
        revisionList : _.template(revisionList)
    };
});