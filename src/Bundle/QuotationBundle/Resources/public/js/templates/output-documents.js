/* global define */
define(['underscore', 'translator'], function(_, Translator) {
    'use strict';

    var modal;

    modal = [
        '<div class="modal-dialog">',
            '<div class="modal-content">',
                '<div class="modal-header">',
                    '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>',
                    '<h4 class="modal-title">' + Translator.trans('summary.output_documents.popup.title', {}, 'KlaroQuotationBundle') + '</h4>',
                '</div>',
                '<div class="modal-body js-blower-modal-content">',
                    '<% _.each(collection, function(model) { %>',
                    '<div class="checkbox">',
                        '<label>',
                            '<input type="checkbox" checked="checked" class="js-selected-documents" value="<%= model.id %>"> <%= model.title %>',
                        '</label>',
                    '</div>',
                    '<% }); %>',
                '</div>',
                '<div class="modal-footer">',
                    '<button type="button" class="btn btn-default" data-dismiss="modal">' + Translator.trans('summary.output_documents.popup.cancel', {}, 'KlaroQuotationBundle') + '</button>',
                    '<button type="button" class="btn btn-primary js-save-changes" data-loading-text="' + Translator.trans('summary.output_documents.popup.saving', {}, 'KlaroQuotationBundle') + '">',
                        '<i class="fa fa-download"></i> ' + Translator.trans('summary.output_documents.popup.save', {}, 'KlaroQuotationBundle'),
                    '</button>',
                '</div>',
            '</div><!-- /.modal-content -->',
        '</div><!-- /.modal-dialog -->'
    ].join('');

    return {
        modal : _.template(modal)
    };
});
