/* global define */
define(['underscore'], function(_) {
    'use strict';

    var popoverTemplate;

    popoverTemplate = [
        '<ul class="revision-popover-leaders">',
            '<li>',
                '<span>Id</span>',
                '<span><%= model.identifier %></span>',
            '</li>',
        '<ul>',
        '<li>',
            '<span>Title</span>',
            '<span><%= model.title %></span>',
        '</li>',
        '<li>',
            '<span>Status</span>',
            '<span><%= status[model.status] %></span>',
        '</li>',

        '<% if(model.status == "won") { %>',
            '<li>',
                '<span>Order status</span>',
                '<span><%= orderStatus[model.order_status] %></span>',
            '</li>',
        '<% } %>',

        '<% if(qualifiers && qualifierOptions) { %>',
            '<% _.each(qualifiers, function(qualifierName, qualifier) { %>',
                '<li>',
                    '<span><%= qualifierName %></span>',
                    '<span><%= qualifierOptions[qualifier][model.qualifiers[qualifier]] %></span>',
                '</li>',
            '<% }); %>',
        '<% } %>',

    '</ul>'
    ].join('');

    return {
        popover : _.template(popoverTemplate)
    };
});