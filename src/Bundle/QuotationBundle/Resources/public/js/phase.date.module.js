define(['backbone', 'marionette', './app', 'bootstrap-datepicker'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        ui : {
            input : 'input'
        },

        $dateInput : null,

        fieldName : null,

        disabled : true,

        initialize: function(options) {
            this.bindUIElements();

            this.fieldName = options.fieldName || null;

            this.disabled = (this.ui.input.prop('disabled') === true);

            if(!this.disabled) {
                this.$dateInput = this.$el.datepicker({
                    format: 'dd.mm.yyyy',
                    todayBtn: "linked",
                    todayHighlight: true,
                    autoclose: true
                });

                this._bindEvents();
            }
        },

        _bindEvents: function() {
            if(!this.disabled) {
                this.$dateInput.on('changeDate', _.bind(function() {
                    this.trigger('change');
                }, this));
            }
        },

        _unbindEvents: function() {
            if(!this.disabled) {
                this.$dateInput.off();
            }
        },

        onDestroy: function() {
            this._unbindEvents();
        },

        getValue: function() {
            var value = null;

            if(this.fieldName) {
                value = {
                    name  : this.fieldName,
                    value : this.ui.input.val()
                };
            }

            return value;
        }
    });
});