/* global define */
define(['backbone', 'marionette', 'jquery-treetable'], function(Backbone, Marionette) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        ui: {
            expandButton: '.js-expand-all',
            collapseButton: '.js-collapse-all'
        },

        events: {
            'click tr' : 'toggleRow',
            'click @ui.expandButton' : 'expandAll',
            'click @ui.collapseButton' : 'collapseAll'
        },

        initialize: function() {
            this.$el.treetable({
                expandable: true,
                clickableNodeNames : false,
                columnElType : 'td'
            });
        },

        toggleRow: function(e) {
            var $target = $(e.currentTarget),
                nodeId = $target.data('tt-id');

            if(nodeId) {
                if($target.hasClass('collapsed')) {
                    this.$el.treetable('expandNode', nodeId);
                }
                else {
                    this.$el.treetable('collapseNode', nodeId);
                }
            }
        },

        expandAll: function (e) {
            e.preventDefault();

            this.$el.treetable('expandAll');
        },

        collapseAll: function (e) {
            e.preventDefault();

            this.$el.treetable('collapseAll');
        }
    });
});