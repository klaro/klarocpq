/* global define */
define(['backbone', 'marionette', './helpers/confirm.modal'], function(Backbone, Marionette, ConfirmModal) {
    /**
     * Module meant to be attached to be attached to a link that shows a modal before continuing.
     *
     * @module confirm
     * @see module:'helpers/confirm.modal'
     */
    'use strict';

    /**
     * @constructor
     */
    var ConfirmView = Backbone.Marionette.ItemView.extend({
        confirmText : null,

        events : {
            'click' : 'handleClick'
        },

        /**
         * @param {Object} options Configuration options
         * @param {string} options.confirmText Text shown in the popup.
         */
        initialize: function(options) {
            this.confirmText = options.confirmText || 'Are you sure?';
        },

        handleClick: function(e) {
            e.preventDefault();

            var location = this.$el.attr('href');

            ConfirmModal.launchModal(this.confirmText).done(function() {
                window.location.replace(location);
            });
        }
    });

    return ConfirmView;
});
