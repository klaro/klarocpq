define(['backbone', 'marionette', './app'], function(Backbone, Marionette, app) {
    'use strict';

    var ItemView,
        EmptyView,
        RowModel,
        RowCollection,
        CollectionView,
        configDefaults,
        saveAfterLoadTriggered = false;

    RowModel = Backbone.Model.extend({
        defaults : {},

        initialize : function(options) {
            // Set data from the "values" property to the model itself.
            _.each(options.values, function(value, field) {
                this.set(field, value);
            }, this);
        }
    });

    RowCollection = Backbone.Collection.extend({
        model : RowModel
    });

    ItemView = Backbone.Marionette.ItemView.extend({
        tagName : 'tr',

        parent : null,

        jsHandlers: null,

        ui : {
            removeButton : '.js-remove-row',
            inputs       : '.js-array-input'
        },

        events: {
            'click @ui.removeButton' : 'removeRow',
            'change @ui.inputs'      : 'onChange'
        },

        initialize: function(options) {
            this.bindUIElements();

            this.parent = options.parent;
            this.jsHandlers = options.jsHandlers;

        },

        removeRow: function(e) {
            e.preventDefault();

            if (this.parent.itemConfig.allowRemove === true && confirm('Are you sure?')) {
                // Calling destroy() on the model causes DELETE request so just remove it
                // from the collection and trigger change to update view.
                // this.model.destroy();
                var collection = this.model.collection;

                collection.remove(this.model);
                collection.trigger('change');
            }
        },

        onRender: function(e) {
            // Disable inputs if the array is disabled in config.
            if(this.parent.itemConfig.disabled) {
                this.ui.removeButton.attr('disabled', true);
                this.ui.inputs.attr('disabled', true);
            }

            if (this.jsHandlers !== null) {
                this.jsHandlers.forEach(function(item, i) {
                        // iterate fields
                        for (let key in item) {
                            if (key == 'onRender') {
                                // get function data
                                let handler = JSON.parse(item[key]);
                                // create handler funciton
                                let onRenderHandler = new Function(handler.function.arguments, handler.function.body);
                                onRenderHandler(this);
                            }
                        }
                    }, this
                );
            }
        },

        onChange: function(e) {
            var $target = $(e.currentTarget),
                name = $target.data('input-name'),
                value = $target.attr('type') == 'checkbox' ? $target.prop('checked') : $target.val();

            this.model.set(name, value);

            if (this.jsHandlers !== null) {
                this.jsHandlers.forEach(function(item, i) {
                        // iterate fields
                        for (let key in item) {
                            if (key == 'onChange') {
                                // get function data
                                let handler = JSON.parse(item[key]);
                                // create handler funciton
                                let onChangeHandler = new Function(handler.function.arguments, handler.function.body);
                                onChangeHandler(this);
                            }
                        }
                    }, this
                );
            }
        },

        serializeData: function() {
            return {
                items       : this.parent.itemConfig.items,
                showTitle   : this.parent.itemConfig.showTitle,
                allowAdd    : this.parent.itemConfig.allowAdd,
                allowRemove : this.parent.itemConfig.allowRemove,
                model       : this.model.toJSON()
            };
        }
    });

    EmptyView = Backbone.Marionette.ItemView.extend({
        tagName  : 'tr',

        parent : null,

        initialize: function(options) {
            this.parent = options.parent;
        },

        serializeData: function() {
            var columns = _.keys(this.parent.itemConfig.items).length;

            if(this.parent.itemConfig.showTitle) {
                columns += 1;
            }

            if(this.parent.itemConfig.allowRemove) {
                columns += 1;
            }

            return {
                columns : columns
            };
        }
    });

    CollectionView =  Backbone.Marionette.CollectionView.extend({
        childView : ItemView,

        emptyView : EmptyView,

        childViewTemplate : null,

        itemConfig : null,

        initialize: function(options) {
            this.itemConfig = options.itemConfig;
            this.childViewTemplate = options.childViewTemplate;
        },

        buildChildView: function(item, ItemView, itemViewOptions) {
            return new ItemView(_.extend(itemViewOptions, {
                parent : this,
                model : item,
                jsHandlers: this.itemConfig.jsHandlers
            }));
        }
    });

    configDefaults = {
        items         : null,
        rows          : 0,
        showHeaders   : false,
        showTitle     : false,
        allowAdd      : false,
        allowRemove   : false,
        saveAfterLoad : false
    };

    return Backbone.Marionette.ItemView.extend({
        ui: {
            valueInput    : '.js-array-value',
            table         : '.js-array-contents',
            button        : '.js-add-row',
            template      : '.js-array-template',
            emptyTemplate : '.js-array-empty-template'
        },

        collectionView : null,

        fieldName : null,
        itemConfig : null,
        defaultValues : null,
        jsHandlers : null,

        initialize: function(options) {
            this.bindUIElements();

            this.fieldName = options.fieldName || null;
            this.itemConfig = _.defaults(options.config, configDefaults);

            this.defaultValues = {};

            // Set empty model defaults here based on the item config.
            _.each(this.itemConfig.items, function(obj, index) {
                this.defaultValues[index] = this.itemConfig.defaultValues && this.itemConfig.defaultValues[index];
            }, this);

            this.collection = new RowCollection(this.itemConfig.value);

            this.collectionView = new CollectionView({
                el : this.ui.table,
                collection : this.collection,
                itemConfig : this.itemConfig,
                childViewOptions : {
                    template: _.template(this.ui.template.html())
                },
                emptyViewOptions: {
                    template: _.template(this.ui.emptyTemplate.html())
                }
            });

            this.collectionView.render();

            // Update the form value after load just in case the form is sent from another field.
            this.updateValue();

            // Sync data to server after load if requested because the backend might be out of date
            // even though UI might show right values. This happens when a value is not set to a key
            // that is defined in the default value. A global flag is used to prevent many autosaves from
            // triggering if there many array elements on the page.
            if(this.itemConfig.saveAfterLoad && saveAfterLoadTriggered !== true) {
                setTimeout(_.bind(function() {
                    this.trigger("change");
                }, this), 500);

                saveAfterLoadTriggered = true;
            }

            // Add button is hidden the array is disabled in config.
            if(!this.itemConfig.disabled) {
                this.ui.button.click(_.bind(this.addRow, this));
            }

            this.listenTo(this.collection, 'change', _.debounce(this.handleCollectionUpdate, 50));
            this.listenTo(this.collection, 'destroy', _.debounce(this.handleCollectionUpdate, 50));
        },

        addRow: function(e) {
            e.preventDefault();

            if(this.itemConfig.allowAdd === true) {
                this.collection.add(new RowModel({values : this.defaultValues}));
            }
        },

        updateValue: function() {
            this.ui.valueInput.val(JSON.stringify(this.getArrayValue()));
        },

        getArrayValue: function() {
            // If adding is allowed, assume array with numerical index.
            var result = this.itemConfig.allowAdd ? [] : {},
                data;

            this.collection.each(function(model) {
                data = {};

                _.each(this.itemConfig.items, function(field, index) {
                    data[index] = model.get(index);
                }, this);

                // If adding is allowed, assume numerical index.
                if(this.itemConfig.allowAdd === true) {
                    result.push(data);
                }
                else {
                    result[model.id] = data;
                }
            }, this);

            return result;
        },

        handleCollectionUpdate: function() {
            this.updateValue();
            this.trigger("change");
        },

        getValue: function() {
            var value = null;

            if(this.fieldName) {
                value = {
                    name  : this.fieldName,
                    value : this.getArrayValue()
                };
            }

            return value;
        }
    });
});
