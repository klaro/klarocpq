require(['jquery', 'underscore', 'klaro_quotation'], function ($, _, app) {
    $(function () {
        var modules = [];

        // Read dom for additional modules (ones not provided in the starting config)).
        // Modules will receive the element that they are placed on as a parameter option.
        $('[data-app]').each(function(i, element) {
            var $el = $(element);
            var options = $el.data();
            var data = _.extend({
                el : $el,
                module : options.app
            }, options);

            modules.push(data);
        });

        app.loadModules(modules);
    });
});
