define(['backbone', 'marionette', 'klaro_routing', './templates/output-documents', './app'], function(Backbone, Marionette, Router, Template, app) {
    'use strict';

    var DocumentModel,
        DocumentCollection,
        DocumentModalView,
        quotationId;

    DocumentModel = Backbone.Model.extend({
        defaults: {
            title : ''
        }
    });

    DocumentCollection = Backbone.Collection.extend({
        model : DocumentModel
    });

    DocumentModalView = Marionette.ItemView.extend({
        className : 'modal fade',
        template : Template.modal,

        collection : null,

        ui: {
            saveChanges  : '.js-save-changes',
            checkboxes   : '.js-selected-documents'
        },

        events : {
            'click @ui.saveChanges' : 'onSaveChanges',
            'change @ui.checkboxes' : 'onCheckboxChange'
        },

        initialize: function(options) {
            this.collection = options.collection;

            this.render();
        },

        onRender: function() {
            this.bindUIElements();

            this.$el.modal('show');

            // Handle the element deletion after hiding.
            this.$el.on('hidden.bs.modal', _.bind(this.destroyModal, this));

            // Add to DOM.
            this.$el.appendTo(document.body);
        },

        serializeData: function() {
            return {
                collection : this.collection.toJSON()
            };
        },

        getSelectedDocuments: function() {
            return _.reduce(this.ui.checkboxes, function(memo, checkbox) {
                var box = $(checkbox);

                if(box.prop('checked') === true) {
                    memo.push(box.val());
                }

                return memo;
            }, []);
        },

        onCheckboxChange: function() {
            this.ui.saveChanges.prop('disabled', this.getSelectedDocuments().length <= 0);
        },

        onSaveChanges: function(e) {
            e.preventDefault();

            var selected = this.getSelectedDocuments(),
                route;

            if(selected.length > 0) {
                route = Router.generate('klaro_quotation_get_multiple', {quotationId : quotationId, documents : selected});

                window.location.replace(route);

                app.vent.trigger('notification', {
                    message: "Download for " + selected.length + " document" + (selected.length > 1 ? 's' : '') + " started...",
                    id: 'download'
                });

                this.ui.saveChanges.button('loading');

                this.$el.modal('hide');
            }
        },

        destroyModal: function() {
            // Remove element from DOM after the modal is hidden.
            this.destroy();
        },

        onDestroy: function() {
            this.$el.off();
        }
    });

    return Backbone.Marionette.ItemView.extend({
        collection : null,

        ui: {
            downloadLink : '.js-download-link',
            chooseOutput : '.js-choose-ouput-documents'
        },

        events: {
            'click @ui.downloadLink' : 'onDownloadLink',
            'click @ui.chooseOutput' : 'onChooseOutput'
        },

        initialize: function(options) {
            this.bindUIElements();

            quotationId = options.quotationId;

            var documents = [];

            _.each(this.ui.downloadLink, function(link) {
                var $link = $(link),
                    documentId = $link.data('document-id');

                if(documentId) {
                    documents.push({
                        id       : documentId,
                        title    : $link.text()
                    });
                }
            });

            this.collection = new DocumentCollection(documents);
        },

        onDownloadLink: function(e) {
            var $target = $(e.target),
                title  = $target.text();

            if($target.is('[disabled]')) {
                e.preventDefault();
                e.stopPropagation();
                return;
            }

            app.vent.trigger('notification', {
                message: "Download for '" + title.trim() + "' started...",
                id: 'download'
            });
        },

        onChooseOutput: function(e) {
            e.preventDefault();

            // Launch modal.
            var modal = new DocumentModalView({
                collection : this.collection
            });
        }
    });
});
