define(['backbone', 'marionette', 'bootbox', './app'], function(Backbone, Marionette, bootbox, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        inputElement: null,

        restoreButton: null,

        fieldName : null,

        /**
         * Whether the widget is an <input /> or <textarea>.
         */
        isInput: false,

        initialize: function(options) {
            this.fieldName = options.fieldName || null;

            if(this.$el.find('input').length) {
                this.inputElement = this.$el.find('input');
                this.isInput = true;
            } else {
                this.inputElement = this.$el.find('textarea');
                this.isInput = false;
            }

            var restoreButton = this.$el.find('button.default-value-button');

            if(restoreButton.length > 0) {
                this.restoreButton = restoreButton;
            }

            this._buildWidget();
            this._bindEvents();
        },

        /**
         * Builds the widget.
         */
        _buildWidget: function() {
            if(this.restoreButton) {
                this.restoreButton.removeClass('disabled').attr('disabled', false);

                if(this.isInput) {
                    this.restoreButton.parent().css('display', 'inline-block');
                }
            }
        },

        _bindEvents: function() {
            var me = this,
                $el = this.$el,
                $inputEl = this.inputElement,
                confirmText;

            $el.change(function() {
                me.trigger('change');
            });

            if(this.restoreButton) {
                this.restoreButton.click(function(e) {
                    e.preventDefault();

                    confirmText = 'You are about to replace the contents of this text box with its original value. All changes will be lost. Are you sure?';

                    bootbox.confirm(confirmText, function(result) {
                        if(result) {
                            var text = me.$el.find('div.default-value').text();

                            $inputEl.val(text);
                            me.value = text;
                            me.trigger('change');
                        }
                    });
                });
            }
        },

        _unbindEvents: function() {
            this.$el.off();
        },

        onDestroy: function(){
            this._unbindEvents();
        },

        getValue: function() {
            var value = null;

            if(this.fieldName && this.inputElement.is(':disabled') !== true) {
                value = {
                    name  : this.fieldName,
                    value : this.inputElement.val()
                };
            }

            return value;
        }
    });
});