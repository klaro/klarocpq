define(['backbone', 'marionette', './app', './sidebar/app'], function(Backbone, Marionette, app, SidebarApp) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        initialize: function(options) {
            SidebarApp.init(options);
        }
    });
});