/* global define */
define(['backbone', 'marionette', 'underscore', './app'], function(Backbone, Marionette, _, app) {
    /**
     * Module that listens to messages on the application vent and outputs them to console.
     *
     * @module monitor
     */
    'use strict';

    /**
     * @constructor
     */
    var MonitorView = Backbone.Marionette.ItemView.extend({
        /**
         * @listens module:app~log
         * @listens module:app~moduleLoad
         * @listens module:app~moduleLoadError
         */
        initialize: function() {
            app.vent.on('log', _.bind(this._logMessage, this));

            app.vent.on('module:load', _.bind(function(moduleName) {
                this._logMessage('Loaded module "' + moduleName + '"');

                app.vent.trigger('module:load:' + moduleName);
            }, this));

            app.vent.on('module:load:error', _.bind(function(moduleName) {
                this._logMessage('Tried to load module "' + moduleName + '" but failed!');

                app.vent.trigger('module:load:error:' + moduleName);
            }, this));
        },

        _logMessage: function(message) {
            if(app.debugMode && console) {
                console.log('DEBUG: ' + message);
            }
        }
    });

    return MonitorView;
});
