/* global define */
define([
    'backbone', 'marionette', 'underscore', 'klaro_routing', './helpers/revision.modal.create', './app'
], function(
    Backbone, Marionette, _, Routing, CreateModal, app
) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        quotationId : null,
        revisionId : null,

        events: {
            'click' : 'launchModal'
        },

        initialize: function(options) {
            this.quotationId = options.quotationId;
            this.revisionId = options.revisionId;

            app.vent.on('revision:status:change', _.bind(function(result){
                if(result.status == 'won' && (!result.order_status || result.order_status == 'none')) {
                    this.$el.text('Copy to Order');
                }
            }, this));
        },

        launchModal: function(e) {
            e.preventDefault();

            CreateModal.launchModal(this.quotationId, this.revisionId)
                .done(function(result) {
                    window.location.href = Routing.generate('klaro_quotation_edit_revision', {
                        quotationId : result.quotation_id,
                        revisionId : result.revision_id
                    });
                })
                .fail(function() {
                    //alert('cancelled');
                });
        }
    });
});
