/* global define */
define([
    'backbone', 'marionette', 'underscore', 'klaro_routing', './helpers/option_list.modal'
], function(
    Backbone, Marionette, _, Routing, OptionListModal
) {
    'use strict';

    /**
     *
     */
    var CreateView = Backbone.Marionette.ItemView.extend({
        events: {
            'click' : 'onClick'
        },

        title : '',
        choices : [],

        initialize: function(options) {
            this.title   = options.title || this.$el.text();
            this.choices = options.choices;
        },

        onClick: function(e) {
            e.preventDefault();

            OptionListModal.launchModal(this.title, this.choices)
                .done(_.bind(function(quotationType) {
                    var url = Routing.generate('klaro_quotation_create', {
                        quotationType : quotationType
                    });

                    window.location.replace(url);
                }, this));
        }
    });

    return CreateView;
});
