define(['backbone', 'marionette', './app', 'bootstrap'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        ui: {
            input : 'input'
        },

        events: {
            'blur @ui.input'  : 'onBlur',
            'focus @ui.input' : 'onFocus',
            'keyup @ui.input' : 'onKeyUp'
        },

        min : null,
        max : null,

        thousandsSeparator   : ',',
        decimalSeparator     : '.',
        isCurrency           : false,

        originalValue : null,
        tooltipShown : false,

        fieldName : null,

        initialize: function(options) {
            this.bindUIElements();

            this.fieldName = options.fieldName || null;

            // Get input bounds.
            this.min = parseFloat(this.ui.input.attr('min'));
            this.min = _.isNaN(this.min) !== true ? this.min : null;

            this.max = parseFloat(this.ui.input.attr('max'));
            this.max = _.isNaN(this.max) !== true ? this.max : null;

            // get currency format data
            this.isCurrency = this.ui.input.attr('is-currency');
            this.isCurrency = this.isCurrency === 'true' ? true : false;

            /*
            // This is commented as currently the configuration of thousands-separator and decimal-separator is not done.
            // It might make sense to define them in parameters.yml and pass down here through some other means,
            // or to pass them as parameters to the template, as this example-implementation.
            this.thousandsSeparator = this.ui.input.attr('thousands-separator');
            this.thousandsSeparator = _.isString(this.thousandsSeparator) && this.thousandsSeparator.length === 1 ? this.thousandsSeparator : ',';

            this.decimalSeparator = this.ui.input.attr('decimal-separator');
            this.decimalSeparator = _.isString(this.decimalSeparator) && this.decimalSeparator.length === 1 ? this.decimalSeparator : '.';
            */

            // Construct tooltip.
            var errorStr = "Input is out of bounds or not a number";

            if(this.min !== null || this.max !== null) {
                errorStr += ' (';

                if(this.min !== null) {
                    errorStr += 'min ' + this.min;
                }

                if(this.max !== null) {
                    if(this.min !== null) {
                        errorStr += ', ';
                    }

                    errorStr += 'max ' + this.max;
                }

                errorStr += ')';
            }

            this.ui.input.tooltip({
                trigger: 'manual',
                title: errorStr,
                placement: 'auto'
            });

            if (this.isCurrency) {
                this.ui.input.val(this.formatToCurrency(this.getInputValue()));
            }
        },

        getWholeAndDecimal: function(value) {
            var x, whole, decimal,
                _value = value + '';

            if (isNaN(value)) {
                return value;
            }

            if (_value.indexOf('.') >= 0) {
                x = _value.split('.');
                whole = x[0];
                decimal = x[1].length >= 0 ? x[1] : '00';
            } else {
                whole = value;
                decimal = '00';
            }
            if (whole.length === 0) {
                whole = '0';
            }

            return [whole, decimal];
        },

        // Get 2 decimals.
        formatDecimalPart: function(decimal) {
            if (!_.isString(decimal)) {
                decimal = '00';
            } else if (decimal.length === 0) {
                decimal = '00';
            } else if (decimal.length === 1) {
                decimal = decimal + '0';
            } else if (decimal.length === 2) {
                decimal = decimal;
            } else if (decimal.length > 2) {
                decimal = decimal.substr(0, 2);
            }
            return decimal;
        },

        // format separate whole and decimal parts to a full string. Eg. 1234567 -> "1,234,567"
        getNumberString: function(whole, decimal) {
            // reverse first to make sure the first numbers can be a sequence smaller than 3 numbers.
            var isNegative, parts;

            isNegative = parseInt(whole) < 0 || (whole.length > 0 && whole[0] == '-') ? true : false;
            if (isNegative) {
                whole = whole.substr(1);
            }
            parts = (whole + '').split('').reverse().join('').split(/(\d{1,3})/g);

            // get rid of empty strings;
            parts = _.filter(parts, function(x) { return x.length > 0 ? true : false; });

            // The reversing numbers in the beginning makes for some acrobatics here to get the numbers in the right order.
            parts.reverse();
            _.forEach(parts, function(part, index) {
                parts[index] = part.split('').reverse().join('');
            });

            decimal = this.formatDecimalPart(decimal);
            whole = (isNegative ? '-' : '') + parts.join(this.thousandsSeparator);
            return  whole + this.decimalSeparator + decimal;
        },

        // Converts numbers to display format. For ex. 12345.67 -> 12,345.67
        formatToCurrency: function(value) {
            var parts, output;

            if (isNaN(value)) {
                return value;
            }
            parts = this.getWholeAndDecimal(value);
            output = this.getNumberString(parts[0], parts[1]);

            return output;
        },

        // Converts display format to real number. For ex. 12.345,67 -> 12345.67
        formatFromCurrency: function(inputValue) {
            if (_.isString(inputValue)) {
                inputValue = inputValue.replace(',', '.').replace(this.thousandsSeparator, '').replace( this.decimalSeparator, '.' );
            }

            return inputValue;
        },

        getInputValue: function() {
            return this.cleanInputValueToNumber(this.ui.input.val());
        },

        cleanInputValueToNumber: function(value) {
            // if contains ',' & '.', remove ','. If just ',', convert it to '.'
            if (value.indexOf(',') >= 0) {
                if (value.indexOf('.') < 0) {
                    value = value.replace(',', '.');
                }
                value = value.replace(',', '');
            }

            if (this.isCurrency) {
                value = this.formatFromCurrency(value);
            } else {
                value = value.replace(',', '.');
            }
            return parseFloat(value);
        },

        // Store original value on focus.
        onFocus: function() {
            this.originalValue = this.getInputValue();
            if (this.isCurrency) {
                this.ui.input.val(this.formatFromCurrency(this.originalValue));
            }
        },

        // Set original value back to input if it's out of bounds.
        onBlur: function() {
            var value = this.getInputValue();

            if(this.valueWithinBounds(value) && value != this.originalValue) {
                this.trigger('change');
            }
            else {
                if (this.isCurrency) {
                    this.ui.input.val(this.formatToCurrency(this.originalValue));
                } else {
                    this.ui.input.val(this.originalValue);
                }
            }

            this.hideTooltip();
        },

        // Show tooltip if value is out of bounds when user is typing.
        onKeyUp: _.debounce(function() {
            this.toggleTooltip();
        }, 100),

        showTooltip: function() {
            this.ui.input.tooltip('show');
            this.tooltipShown = true;
        },

        hideTooltip: function() {
            this.ui.input.tooltip('hide');
            this.tooltipShown = false;
        },

        toggleTooltip: function() {
            if(this.valueWithinBounds(this.getInputValue())) {
                this.hideTooltip();
            }
            else if(!this.tooltipShown) {
                this.showTooltip();
            }
        },

        valueWithinBounds: function(value) {
            return (_.isNaN(value) === false && (this.min === null || value >= this.min) && (this.max === null || value <= this.max));
        },

        getValue: function() {
            var value = null;

            if(this.fieldName) {
                value = {
                    name  : this.fieldName,
                    value : this.getInputValue()
                };
            }

            return value;
        }
    });
});