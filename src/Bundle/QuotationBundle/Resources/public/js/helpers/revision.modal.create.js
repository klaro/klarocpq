/* global define */
define([
    'backbone',
    'marionette',
    'underscore',
    '../models/quotation.model',
    '../models/revision.model',
    '../models/revision.collection',
    '../models/constants',
    '../templates/revision.modal.create',
    'translator',
    'chosen'
], function(
    Backbone, Marionette, _, QuotationModel, RevisionModel, RevisionCollection, Constants, Template, Translator, chosen
) {
    /**
     * ...
     *
     * @module 'helpers/revision.modal.create'
     */

    'use strict';

    var RevisionOptionView,
        RevisionOptionCollectionView,
        ModalView,
        MessageView;

    /**
     * @constructor
     */
    RevisionOptionView = Backbone.Marionette.ItemView.extend({
        tagName : 'option',
        template : _.template('<%= model.identifier %> <%= model.title %> (<%= status %>)'),

        serializeData: function() {
            return {
                model  : this.model.toJSON(),
                status : Constants.status[this.model.get('status')] || ''
            };
        },

        onRender: function() {
            this.$el.attr('value', this.model.get('revision_id'));

            this.$el.prop('selected', this.model.collection.revisionId == this.model.get('revision_id'));
        }
    });

    /**
     * @constructor
     */
    RevisionOptionCollectionView = Backbone.Marionette.CompositeView.extend({
        childView : RevisionOptionView,

        template : Template.body,
        titleTouched: false,

        ui: {
            titleInput      : '.js-revision-title',
            revisionList    : '.js-revision-list',
            statusList      : '.js-revision-status',
            orderStatusList : '.js-revision-order-status'
        },

        events : {
            'change @ui.titleInput' : 'titleChanged',
            'change @ui.revisionList' : 'revisionChanged',
            'change @ui.statusList'   : 'statusChanged',
            'change @ui.orderStatusList' : 'orderStatusChanged'
        },

        modelEvents : {
            'change' : 'render'
        },

        initialize: function() {
            this.setDataFromRevision(this.model.get('revision_id'));
        },

        revisionChanged: function() {
            this.setDataFromRevision(parseInt(this.ui.revisionList.val()));
        },

        setDataFromRevision: function(revisionId) {
            var selectedRevision = this.collection.findWhere({ revision_id : revisionId });

            if(selectedRevision) {
                var values = selectedRevision.pick(['revision_id', 'title', 'status', 'order_status']),
                    oldTitle = this.model.get('title');

                if(this.titleTouched && this.ui.titleInput.val().length > 0) {
                    values = _.extend(values, {
                        'title': this.model.get('title')
                    });
                }

                this.model.set(values);
            }
        },

        titleChanged: function () {
            this.titleTouched = true;

            this.model.set({
                title: this.ui.titleInput.val()
            }, { silent: true });
        },

        statusChanged: function() {
            var status = this.ui.statusList.val();

            this.model.set('status', status);

            if(status != 'won') {
                this.model.set('order_status', 'none');
            }
        },

        orderStatusChanged: function() {
            var orderStatus = this.ui.orderStatusList.val();

            this.model.set('order_status', orderStatus);
        },

        attachHtml: function(collectionView, itemView){
            collectionView.$('.js-revision-list').append(itemView.el);
        },

        serializeData: function() {
            return {
                model : this.model.toJSON(),
                statusOptions : Constants.status,
                orderStatusOptions : Constants.order_status
            };
        },

        onRender: function() {
            this.ui.revisionList.val(this.model.get('revision_id'));
            this.ui.statusList.val(this.model.get('status'));
            this.ui.orderStatusList.val(this.model.get('order_status'));

            this.ui.orderStatusList.prop('disabled', this.model.get('status') != 'won');
        }
    });

    /**
     * @constructor
     */
    MessageView = Backbone.Marionette.ItemView.extend({
        template: _.template('<p style="text-align: center;"><%= text %></p>'),

        initialize: function(options) {
            this.text = options.text;
        },

        serializeData: function() {
            return {
                text : this.text
            };
        }
    });

    /**
     * @constructor
     */
    ModalView = Backbone.Marionette.LayoutView.extend({
        className : 'modal fade',

        template : Template.modal,

        quotation   : null,
        collection  : null,
        resultModel : null,

        quotationId : null,
        revisionId  : null,

        deferred : null,

        dismissed : false,

        ui: {
            saveButton : '.js-save-changes'
        },

        events: {
            'click @ui.saveButton' : 'saveModal'
        },

        regions: {
            'body' : '.js-modal-body'
        },

        initialize: function(options) {
            this.quotationId = options.quotationId;
            this.revisionId = options.revisionId;

            this.deferred = options.deferred;

            this.render();

            this.bindUIElements();

            this.getRegion('body').show(new MessageView({
                text : '<i class="fa fa-spinner"></i> ' + Translator.trans('modal.create_revision.loading_body', {}, 'KlaroQuotationBundle')
            }));

            this.quotation = new QuotationModel.getInstance().set('quotation_id', this.quotationId);

            this.collection = new RevisionCollection({
                quotationId : this.quotationId
            });

            this.resultModel = new RevisionModel({
                revision_id  : this.revisionId,
                quotation_id : this.quotationId
            });

            this.dismissed = false;

            this.ui.saveButton.prop('disabled', true);

            $.when(this.quotation.fetch(), this.collection.fetch())
                .then(_.bind(function() {
                    if(this.dismissed) {
                        return;
                    }

                    if(this.quotation.get('final')) {
                        this.getRegion('body').show(new MessageView({
                            text : Translator.trans('modal.create_revision.alerts.quotation_closed', {}, 'KlaroQuotationBundle')
                        }));
                    }
                    else {
                        this.getRegion('body').show(new RevisionOptionCollectionView({
                            quotation : this.quotation,
                            model : this.resultModel,
                            collection : this.collection
                        }));

                        this.ui.saveButton.prop('disabled', false);
                    }
                }, this));

            this.$el.modal('show');

            // Handle the element deletion after hiding.
            this.$el.on('hidden.bs.modal', _.bind(this.destroyModal, this));

            // Add to DOM.
            this.$el.appendTo(document.body);
        },

        saveModal: function(e) {
            e.preventDefault();

            this.ui.saveButton.button('loading');

            this.resultModel.save()
                .done(_.bind(function () {
                    this.deferred.resolve(this.resultModel.toJSON());

                    this.ui.saveButton.button('reset');
                    this.$el.modal('hide');
                }, this))
                .fail(_.bind(function () {
                    alert('Failed to create new revision!');

                    this.ui.saveButton.button('reset');
                }, this))
        },

        destroyModal: function() {
            this.deferred.reject();

            this.dismissed = true;

            // Remove element from DOM after the modal is hidden.
            this.destroy();
        },

        onDestroy: function() {
            this.$el.off();
        }
    });

    return {
        /**
         * Launch modal dialog for choosing a new revision status. Returns promise object that is fulfilled
         * when user clicks the save button. Otherwise, the promise is rejected.
         *
         * @param {int} quotationId Quotation Id
         * @param {int} revisionId Revision Id
         */
        launchModal: function(quotationId, revisionId) {
            var deferred = $.Deferred();

            new ModalView({
                quotationId : quotationId,
                revisionId  : revisionId,
                deferred : deferred
            });

            return deferred.promise();
        }
    };
});
