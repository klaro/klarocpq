/* global define */
define([
    'backbone',
    'marionette',
    'underscore',
    '../templates/product-data.import.modal',
    'translator',
    'chosen'
], function(
    Backbone, Marionette, _, Template, Translator, chosen) {
    'use strict';

    var Product,
        Products,
        ProductOptionView,
        ModalView,
        BodyView;

    /**
     * Backbone model for an option.
     * @constructor
     */
    Product = Backbone.Model.extend({
        defaults : {
            title : '',
            type: ''
        }
    });

    /**
     * Option collection.
     * @constructor
     */
    Products = Backbone.Collection.extend({
        model : Product
    });

    ProductOptionView = Backbone.Marionette.ItemView.extend({
        tagName : 'option',
        template : _.template('<%= model.type %>: <%= model.title %>'),

        attributes: function () {
            return {
                value: this.model.get('id')
            }
        },

        serializeData: function() {
            return {
                model : this.model.toJSON()
            };
        }
    });

    BodyView = Backbone.Marionette.CompositeView.extend({
        template: Template.bodyView,
        childView: ProductOptionView,

        ui: {
            importSources: '.js-import-sources'
        },

        onRender: function () {
            this.ui.importSources.chosen();
        },

        getSelectedSource: function () {
            return this.ui.importSources.val();
        },

        attachHtml: function(collectionView, itemView) {
            this.ui.importSources.append(itemView.el);
        }
    });

    ModalView = Backbone.Marionette.LayoutView.extend({
        className : 'modal fade',

        template : Template.modal,

        deferred : null,
        saveUrl : null,
        sources: null,

        ui: {
            modalBody : '.js-modal-body',
            saveButton : '.js-save-changes'
        },

        regions: {
            'body' : '.js-modal-body'
        },

        events: {
            'click @ui.saveButton' : 'saveModal'
        },

        initialize: function(options) {
            this.saveUrl = options.saveUrl;
            this.sources = options.sources;
            this.deferred = options.deferred;

            this.render();

            this.getRegion('body').show(new BodyView({
                collection: options.collection
            }));
        },

        onRender: function() {
            this.bindUIElements();

            this.$el.modal('show');

            // Handle the element deletion after hiding.
            this.$el.on('hidden.bs.modal', _.bind(this.destroyModal, this));

            // Add to DOM.
            this.$el.appendTo(document.body);
        },

        saveModal: function(e) {
            e.preventDefault();

            var confirmText = Translator.trans('modal.import_product_data.alerts.confirm_import', {}, 'KlaroQuotationBundle');

            if(confirm(confirmText)) {
                this.ui.saveButton.button('loading');

                $.ajax({
                    url: this.saveUrl,
                    data: JSON.stringify({
                        source: this.getRegion('body').currentView.getSelectedSource()
                    }),
                    dataType: 'json',
                    type: 'PUT',
                    success: _.bind(function(response) {
                        this.deferred.resolve();

                        this.$el.modal('hide');
                    }, this),
                    error:  _.bind(function(response) {
                        var errorMsg = Translator.trans('modal.import_product_data.alerts.save_error', {}, 'KlaroQuotationBundle');

                        alert('There was an error duting import!');
                    }, this),
                    complete: _.bind(function () {
                        this.ui.saveButton.button('reset');
                    }, this)
                });
            }
        },

        destroyModal: function() {
            this.deferred.reject();

            // Remove element from DOM after the modal is hidden.
            this.destroy();
        },

        onDestroy: function() {
            this.$el.off();
        }
    });

    return {
        /**
         * Launch modal to set revision status. Revision is fetched from API.
         */
        launchModal: function(choices, saveUrl) {
            var deferred = $.Deferred();

            new ModalView({
                saveUrl: saveUrl,
                collection: new Products(choices),
                deferred : deferred
            });

            return deferred.promise();
        }
    };
});
