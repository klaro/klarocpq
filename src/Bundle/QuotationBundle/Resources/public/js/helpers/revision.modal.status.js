/* global define */
define([
    'backbone',
    'marionette',
    'underscore',
    '../models/constants',
    '../models/revision.model',
    '../templates/revision.modal.status',
    'translator',
    'chosen'
], function(
    Backbone, Marionette, _, Constants, RevisionModel, Template, Translator, chosen)
{
    'use strict';

    var ModalView,
        ModalBodyView;

    ModalBodyView = Backbone.Marionette.ItemView.extend({
        template : Template.body,
        qualifiers: null,
        qualifierOptions: null,

        ui : {
            titleInput          : '.js-revision-title',
            statusInput         : '.js-revision-status',
            qualifierInputs     : '.js-revision-qualifier',
            orderStatusInput    : '.js-revision-order-status',
            messageInput        : '.js-revision-text',
            newRevisionCheckbox : '.js-create-new-revision'
        },

        events: {
            'change @ui.statusInput' : 'statusChanged',
            'change @ui.orderStatusInput' : 'orderStatusChanged'
        },

        initialize: function(options) {
            this.qualifiers = options.qualifiers;
            this.qualifierOptions = options.qualifierOptions;

            this.bindUIElements();
        },

        statusChanged: function() {
            if(this.getStatus() != 'won') {
                this.ui.orderStatusInput.val(null);

                this.orderStatusChanged();
            }

            this.ui.orderStatusInput.prop('disabled', this.getStatus() != 'won');
            this.ui.orderStatusInput.trigger('chosen:updated');

            if(this.ui.newRevisionCheckbox.prop('disabled') !== true) {
                this.ui.newRevisionCheckbox.prop('checked', true);
            }
        },

        orderStatusChanged: function() {
            var orderStatusFinished = this.getStatus() == 'won' && this.getOrderStatus() == 'finished';

            if(orderStatusFinished) {
                this.ui.newRevisionCheckbox.prop('checked', false);
            }

            this.ui.newRevisionCheckbox.prop('disabled', orderStatusFinished);

            if(this.ui.newRevisionCheckbox.prop('disabled') !== true) {
                this.ui.newRevisionCheckbox.prop('checked', true);
            }
        },

        getTitle: function() {
            return this.ui.titleInput.val();
        },

        getStatus: function() {
            return this.ui.statusInput.val();
        },

        getOrderStatus: function() {
            return this.ui.orderStatusInput.val();
        },

        getLogMessage: function() {
            return this.ui.messageInput.val();
        },

        getStartNewRevision: function() {
            return !this.ui.newRevisionCheckbox.prop('disabled') && this.ui.newRevisionCheckbox.prop('checked');
        },

        getQualifiers: function() {
            var qualifiers = this.model.getQualifiers();

            this.ui.qualifierInputs.each(function(index, input) {
                var $inputEl = $(input),
                    qualifierId = $inputEl.attr('name');

                qualifiers[qualifierId] = $inputEl.val();
            });

            return qualifiers;
        },

        serializeData: function() {
            return {
                model              : this.model.toJSON(),
                statusOptions      : Constants.status,
                orderStatusOptions : Constants.order_status,
                qualifiers         : this.qualifiers,
                qualifierOptions   : this.qualifierOptions
            };
        },

        onRender : function() {
            this.ui.statusInput.chosen();
            this.ui.orderStatusInput.chosen();

            var qualifiers = this.model.getQualifiers();

            this.ui.qualifierInputs.each(_.bind(function(index, input) {
                var $inputEl = $(input),
                    qualifierId = $inputEl.attr('name');

                $inputEl
                    .val(qualifiers[qualifierId])
                    .chosen();
            }, this));

            this.ui.statusInput.val(this.model.get('status'));
            this.ui.orderStatusInput.val(this.model.get('order_status'));
            this.ui.messageInput.val(this.model.get('log_message'));

            var orderStatusFinished = this.getStatus() == 'won' && this.getOrderStatus() == 'finished';
            this.ui.newRevisionCheckbox.prop('disabled', orderStatusFinished);

            this.ui.statusInput.trigger('chosen:updated');
            this.ui.orderStatusInput.trigger('chosen:updated');
        }
    });

    ModalView = Backbone.Marionette.LayoutView.extend({
        className : 'modal fade',

        template : Template.modal,

        bodyView : null,
        deferred : null,
        qualifiers: null,
        qualifierOptions: null,

        ui: {
            modalBody    : '.js-modal-body',
            saveButton   : '.js-save-changes',
            search       : '.js-user-search',
            icon         : '.js-user-icon'
        },

        regions: {
            'body' : '.js-modal-body'
        },

        events: {
            'click @ui.saveButton' : 'saveModal'
        },

        dismissed: false,

        initialize: function(options) {
            this.deferred = options.deferred;
            this.qualifiers = options.qualifiers;
            this.qualifierOptions = options.qualifierOptions;

            this.model = RevisionModel.getInstance();

            this.render();

            this.dismissed = false;

            this.model.fetch({
                success: _.bind(function() {
                    if(this.dismissed) {
                        return;
                    }
                    this.bodyView = new ModalBodyView({
                        model : this.model,
                        qualifiers : this.qualifiers,
                        qualifierOptions : this.qualifierOptions
                    });

                    this.getRegion('body').show(this.bodyView);

                }, this)
            });
        },

        onRender: function() {
            this.bindUIElements();

            this.$el.modal('show');

            // Handle the element deletion after hiding.
            this.$el.on('hidden.bs.modal', _.bind(this.destroyModal, this));

            // Add to DOM.
            this.$el.appendTo(document.body);
        },

        saveModal: function(e) {
            e.preventDefault();

            var oldModelData = this.model.toJSON(),
                saveData = {
                    title        : this.bodyView.getTitle(),
                    status       : this.bodyView.getStatus(),
                    order_status : this.bodyView.getOrderStatus(),
                    log_message  : this.bodyView.getLogMessage(),
                    qualifiers   : this.bodyView.getQualifiers()
                },
                startNewRevision = this.bodyView.getStartNewRevision();

            this.ui.saveButton.button('loading');

            if(startNewRevision && saveData.log_message.length < 1) {
                alert(Translator.trans('modal.revision_status.alerts.write_log_message', {}, 'KlaroQuotationBundle'));

                this.ui.saveButton.button('reset');
            }
            else {
                this.model.save(saveData, {
                    success: _.bind(function() {
                        this.deferred.resolve(this.model.toJSON(), oldModelData, startNewRevision);

                        this.$el.modal('hide');
                    }, this),
                    error: _.bind(function() {
                        alert(Translator.trans('modal.revision_status.alerts.error_saving_data', {}, 'KlaroQuotationBundle'));

                        this.deferred.fail();

                        this.ui.saveButton.button('reset');
                    }, this)
                });
            }
        },

        destroyModal: function() {
            this.deferred.reject();

            this.dismissed = true;

            // Remove element from DOM after the modal is hidden.
            this.destroy();
        },

        onDestroy: function() {
            this.$el.off();
        }
    });

    return {
        /**
         * Launch modal to set revision status. Revision is fetched from API.
         */
        launchModal: function(qualifiers, qualifierOptions) {
            var deferred = $.Deferred();

            new ModalView({
                deferred : deferred,
                qualifiers : qualifiers,
                qualifierOptions : qualifierOptions
            });

            return deferred.promise();
        }
    };
});
