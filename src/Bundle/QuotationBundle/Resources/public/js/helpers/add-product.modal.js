/* global define */
define([
    'backbone',
    'marionette',
    'underscore',
    '../templates/add-product.modal',
    'klaro_routing',
    'chosen',
    'html5sortable',
    'translator'
], function(
    Backbone, Marionette, _, Template, Routing, chosen, sortable, Translator) {
    'use strict';

    var ProductModel,
        NewProductModel,
        ProductCollection,
        SortableProductView,
        ModalView,
        NewProductBodyView,
        SourceProductView,
        SortProductsBodyView;

    ProductModel = Backbone.Model.extend({
        defaults: {
            title : '',
            source_id: null,
            phase_id: false,
            remove: false,
            is_new: false
        }
    });

    NewProductModel = Backbone.Model.extend({
        defaults: {
            product : null,
            title : null,
            use_source: false
        }
    });

    ProductCollection = Backbone.Collection.extend({
        model : ProductModel,

        comparator: function(model) {
            return model.get('index');
        },

        getNumberOfDeletedProducts: function () {
            return this.reduce(function (memo, model) {
                if(model.get('remove')) {
                    memo += 1;
                }

                return memo;
            }, 0);
        }
    });

    SortableProductView = Backbone.Marionette.ItemView.extend({
        tagName : 'li',
        className: 'product-list-sort-item',
        template : Template.productInList,

        editing: false,

        ui: {
            titleColumn: '.js-title',
            titleInput: '.js-title-input',
            editButton: '.js-edit-title',
            cloneButton: '.js-edit-clone',
            saveButton: '.js-edit-save',
            removeButton: '.js-remove'
        },

        events: {
            'dblclick @ui.titleColumn': 'editTitle',
            //'blur @ui.titleInput': 'saveTitle',
            'keyup @ui.titleInput': 'onKeyUp',
            'click @ui.editButton': 'onEditTitle',
            'click @ui.saveButton': 'saveTitle',
            'click @ui.cloneButton': 'cloneProduct',
            'click @ui.removeButton': 'removeProduct'
        },

        attributes : function() {
            return {
                'data-original-id' : this.model.get('id')
            };
        },

        onKeyUp: function (e) {
            if(this.editing && e.keyCode == 13){
                this.ui.saveButton.click();
            }
        },

        onEditTitle: function(e) {
            e.preventDefault();

            this.editTitle();
        },

        editTitle: function() {
            if(!this.editing) {
                this.editing = true;
                this.render();
                this.ui.titleInput.focus().select();

                sortable('.js-product-list', 'disable');
            }
        },

        saveTitle: function(e) {
            e.preventDefault();

            if(this.editing) {
                this.editing = false;
                this.model.set({
                    title: this.ui.titleInput.val()
                });
                this.render();

                // Update other view that have this as source
                if(!this.model.get('is_new')) {
                    this.triggerMethod('title:updated', this.model);
                }

                sortable('.js-product-list', 'enable');
            }
        },

        cloneProduct: function(e) {
            e.preventDefault();

            this.triggerMethod('clone:product', this.model);
        },

        removeProduct: function(e) {
            e.preventDefault();

            this.model.set({
                remove: !this.model.get('remove')
            });

            this.render();
        },

        onRender: function () {
            if(this.editing) {
                this.ui.saveButton.show();
                this.ui.editButton.hide();
            }
            else {
                this.ui.saveButton.hide();
                this.ui.editButton.show();
            }

            this.$el
                .removeClass('remove')
                .removeClass('new');

            if(this.model.get('remove')) {
                this.$el.addClass('remove');
            }
            else if(this.model.get('is_new')) {
                this.$el.addClass('new');
            }

            this.ui.editButton.attr('disabled', this.model.get('remove'));
            this.ui.removeButton.attr('disabled', this.editing);
            this.ui.cloneButton.attr('disabled', this.model.get('remove') || this.editing);
        },

        serializeData: function() {
            var source = this.model.get('is_new') ? this.model.collection.get(this.model.get('source_id')) : null,
                sourceTitle = null;

            if(source) {
                sourceTitle = Translator.trans('modal.modify_product.manage_products.source', {
                    source: source.get('title')
                }, 'KlaroQuotationBundle');
            }

            return {
                editing: this.editing,
                model : this.model.toJSON(),
                sourceTitle: sourceTitle
            };
        }
    });

    SortProductsBodyView = Backbone.Marionette.CompositeView.extend({
        saveUrl: null,
        template : Template.orderProducts,
        productListView: null,
        childView : SortableProductView,

        ui : {
            productList: '.js-product-list'
        },

        collectionEvents: {
            'update': 'render'
        },

        childEvents: {
            'clone:product': 'onCloneProduct',
            'title:updated': 'onTitleUpdated'
        },

        initialize: function (options) {
            this.saveUrl = options.saveUrl;
        },

        attachHtml: function(collectionView, itemView) {
            this.ui.productList.append(itemView.el);
        },

        onTitleUpdated: function(childView, model) {
            this.collection.where({ source_id: model.get('id') }).forEach(_.bind(function(dependentModel) {
                if(model.get('id') !== dependentModel.get('id')) {
                    this.children.findByModel(dependentModel).render();
                }
            }, this));
        },

        onRender: function () {
            var sortableInstance = sortable(this.ui.productList.get(), {
                placeholderClass: 'product-list-sort-placeholder',
                placeholder: '<li><i class="fa fa-long-arrow-right"></i> ' +
                    Translator.trans('modal.modify_product.manage_products.placeholder', {}, 'KlaroQuotationBundle') + '</li>'
            });

            sortableInstance[0].addEventListener('sortupdate', _.bind(function(e) {
                var model = this.collection.at(e.detail.oldindex);

                this.collection.remove(model);
                this.collection.add(model, { at: e.detail.index });

                this.collection.forEach(_.bind(function(model, index) {
                    this.collection.at(index).set('index', index);
                }, this));
                this.collection.sort();
            }, this));
        },

        getProductOrder: function() {
            return _.map(this.$el.find('li.product-list-sort-item'), function (listEl) {
                return $(listEl).data('original-id');
            });
        },

        onCloneProduct: function(childView, model) {
            var newModel = new ProductModel(_.extend(model.toJSON(), {
                id: this.collection.length,
                title: Translator.trans('modal.modify_product.manage_products.copy_of', {
                    source: model.get('title')
                }, 'KlaroQuotationBundle'),
                is_new: true
            }));

            this.collection.add(newModel, {
                at: this.collection.indexOf(model) + 1
            });

            this.collection.forEach(_.bind(function(model, index) {
                this.collection.at(index).set('index', index);
            }, this));
            this.collection.sort();

            this.children.findByModel(newModel).editTitle();
        },

        doSave: function (callbacks) {
            if(this.confirmSave()) {
                $.ajax({
                    url: this.saveUrl,
                    data: JSON.stringify({
                        products: this.getResult()
                    }),
                    dataType: 'json',
                    type: 'PUT',
                    success: function(response) {
                        if(_.isFunction(callbacks.success)) {
                            callbacks.success(response.data);
                        }
                    },
                    error:  function(response) {
                        if(_.isFunction(callbacks.error)) {
                            callbacks.error(response.data);
                        }
                    },
                    complete: callbacks.complete
                });
            }
            else if(_.isFunction(callbacks.complete)) {
                callbacks.complete();
            }
        },

        confirmSave: function() {
            var numDeleted = this.collection.getNumberOfDeletedProducts(),
                confirmText = Translator.trans('modal.modify_product.manage_products.alerts.confirm_remove', {
                    deletedLines: numDeleted,
                    product: 'pllaa'
                }, 'KlaroQuotationBundle');

            return numDeleted < 1 || confirm(confirmText);
        },

        getResult: function () {
            return _.reduce(this.$el.find('li.product-list-sort-item'), _.bind(function (memo, listEl) {
                var model = this.collection.get($(listEl).data('original-id'));

                if(!model.get('remove')) {
                    memo.push({
                        source_id: model.get('source_id'),
                        title: model.get('title')
                    });
                }

                return memo;
            }, this), []);
        }
    });

    SourceProductView = Backbone.Marionette.ItemView.extend({
        tagName : 'option',
        template : _.template('<%= model.type %>: <%= model.title %>'),

        attributes: function() {
            return {
                value: this.model.get('phase_id')
            }
        },

        serializeData: function() {
            return {
                model : this.model.toJSON()
            };
        }
    });

    NewProductBodyView = Backbone.Marionette.CompositeView.extend({
        template : Template.newProduct,
        childView: SourceProductView,

        saveUrl: null,
        sectionName: null,
        productNames: null,
        productSources: null,

        ui : {
            productSelect: '.js-new-product',
            otherProductsGroup: '.js-other-products-group',
            otherProducts: '.js-other-products',
            useSourceGroup: '.js-use-source-group',
            useSource: '.js-use-source',
            productTitle: '.js-new-product-title'
        },

        events: {
            'change @ui.productSelect' : 'productChanged',
            'change @ui.useSource' : 'useSourceChanged',
            'change @ui.productTitle': 'titleChanged'
        },

        initialize: function(options) {
            this.bindUIElements();

            this.saveUrl = options.saveUrl;
            this.sectionName = options.sectionName || '';
            this.productNames = options.productNames || {};
            this.productSources = options.productSources;

            this.model = new NewProductModel({
                product: Object.keys(this.productNames)[0]
            });
        },

        attachHtml: function(collectionView, itemView) {
            this.ui.otherProducts.append(itemView.el);
        },

        filter: function (child, index, collection) {
            var selectedType = this.model.get('product'),
                childType = child.get('type_id'),
                sources = this.productSources[selectedType] || [];

            return childType === this.model.get('product') || sources.indexOf(childType) !== -1;
        },

        productChanged: function (e) {
            this.model.set({
                product: this.getSelectedProduct(),
                use_source: false
            });
            this.render();
        },

        useSourceChanged: function () {
            this.model.set({
                use_source: this.ui.useSource.prop('checked')
            });
            this.render();
        },

        titleChanged: function(e) {
            this.model.set({
                title: this.ui.productTitle.val()
            }, { silent: true });
        },

        getSelectedProduct: function() {
            return this.ui.productSelect.val();
        },

        getSelectedTitle: function() {
            return this.ui.productTitle.val();
        },

        getSelectedSource: function () {
            return this.ui.otherProducts.val();
        },

        getResult: function() {
            return {
                title: this.getSelectedTitle(),
                product: this.getSelectedProduct(),
                source: this.children.length >= 1 && this.model.get('use_source') ? this.getSelectedSource() : null
            }
        },

        serializeData: function() {
            return {
                sectionName: this.sectionName,
                productNames: this.productNames,
                model: this.model.toJSON()
            };
        },

        doSave: function (callbacks) {
            $.ajax({
                url: this.saveUrl,
                data: JSON.stringify({
                    product: this.getResult()
                }),
                dataType: 'json',
                type: 'POST',
                success: function(response) {
                    if(_.isFunction(callbacks.success)) {
                        callbacks.success(response.data);
                    }
                },
                error:  function(response) {
                    if(_.isFunction(callbacks.error)) {
                        callbacks.error(response.data);
                    }
                },
                complete: callbacks.complete
            });
        },

        onRender : function() {
            this.ui.productSelect.val(this.model.get('product'));

            if(this.children.length < 1) {
                this.ui.otherProductsGroup.hide();
                this.ui.useSourceGroup.hide();
            }
            else {
                if(this.model.get('use_source')) {
                    this.ui.useSource.prop('checked', true);
                    this.ui.otherProducts.chosen();
                }
                else {
                    this.ui.otherProductsGroup.hide();
                }
            }

            this.ui.productSelect.chosen();
        }
    });

    ModalView = Backbone.Marionette.LayoutView.extend({
        className : 'modal fade',

        template : Template.modal,

        deferred : null,
        newProductUrl: null,
        orderProductsUrl: null,
        sectionName: null,
        productNames: null,
        otherProducts: null,
        productSources: null,
        disabled: null,
        newProductView: null,
        orderView: null,

        ui: {
            modalBody : '.js-modal-body',
            saveButton : '.js-save-changes',
            orderButton : '.js-change-order',
            newProductButton : '.js-add-product'
        },

        regions: {
            'body' : '.js-modal-body'
        },

        events: {
            'click @ui.saveButton' : 'saveModal',
            'click @ui.orderButton' : 'changeOrder',
            'click @ui.newProductButton' : 'addProduct'
        },

        initialize: function(options) {
            this.newProductUrl = options.newProductUrl;
            this.orderProductsUrl = options.orderProductsUrl;
            this.sectionName = options.sectionName || '';
            this.productNames = options.productNames || {};
            this.otherProducts = new ProductCollection(options.otherProducts || []);
            this.productSources = options.productSources;
            this.disabled = options.disabled;
            this.deferred = options.deferred;

            this.render();

            this.showNewProductView();
        },

        serializeData: function() {
            return {
                title: Translator.trans('modal.modify_product.title', {
                    'productName' : this.sectionName
                }, 'KlaroQuotationBundle'),
                sectionName: this.sectionName,
                productNames: this.productNames,
                disabled: this.disabled
            };
        },

        onRender: function() {
            this.bindUIElements();

            this.$el.modal('show');

            // Handle the element deletion after hiding.
            this.$el.on('hidden.bs.modal', _.bind(this.destroyModal, this));

            // Add to DOM.
            this.$el.appendTo(document.body);
        },

        addProduct: function(e) {
            e.preventDefault();

            this.showNewProductView();
        },

        changeOrder: function(e) {
            e.preventDefault();

            this.showOrderView();
        },

        showNewProductView: function() {
            this.newProductView = new NewProductBodyView({
                saveUrl: this.newProductUrl,
                sectionName: this.sectionName,
                productNames: this.productNames,
                productSources: this.productSources,
                collection: this.otherProducts
            });
            this.getRegion('body').show(this.newProductView);
            this.orderView = null;

            this.ui.orderButton.show().prop('disabled', (this.otherProducts.length < 1));
            this.ui.newProductButton.hide();
        },

        showOrderView: function() {
            var productData = this.otherProducts.map(function(model, index) {
                return _.extend(model.toJSON(), {
                    source_id: model.get('id'),
                    index: index
                })
            });
            // Pass a copy of the product collection so that name changes are not reflected
            // back to the new product view until they are saved.
            this.orderView = new SortProductsBodyView({
                saveUrl: this.orderProductsUrl,
                collection: new ProductCollection(productData)
            });
            this.getRegion('body').show(this.orderView);
            this.newProductView = null;

            this.ui.newProductButton.show();
            this.ui.orderButton.hide();
        },

        saveModal: function(e) {
            e.preventDefault();

            this.ui.saveButton.button('loading');

            this.getRegion('body').currentView.doSave({
                success: _.bind(function(data) {
                    this.deferred.resolve(data.url);
                }, this),
                error: _.bind(function() {
                    //console.log('error');
                }, this),
                complete: _.bind(function() {
                    this.ui.saveButton.button('reset');
                }, this)
            });
        },

        destroyModal: function() {
            this.deferred.reject();

            // Remove element from DOM after the modal is hidden.
            this.destroy();
        },

        onDestroy: function() {
            this.$el.off();
        }
    });

    return {
        /**
         * Launch modal to set revision status. Revision is fetched from API.
         */
        launchModal: function(newProductUrl, orderProductsUrl, sectionName, productNames, otherProducts, productSources, disabled) {
            var deferred = $.Deferred();

            new ModalView({
                newProductUrl: newProductUrl,
                orderProductsUrl: orderProductsUrl,
                sectionName: sectionName,
                productNames: productNames,
                otherProducts: otherProducts,
                productSources: productSources,
                disabled: disabled,
                deferred : deferred
            });

            return deferred.promise();
        }
    };
});
