/* global define */
define([
    'backbone', 'marionette', 'underscore', '../templates/option_list.modal'
], function(
    Backbone, Marionette, _, Template
) {
    /**
     * Helper to show a modal popup with list of choices. The module returns an object with one static method
     * called 'launchModal' which takes in the title and a list of choices (key-value array). Calling this method
     * shows the the modal and returns a promise that has the selected id when fulfilled.
     *
     * @example
     * // Load module and launch
     * require(['./helpers/option_list.modal'], function(OptionModal) {
     *     var choices = {'a' : 'First', 'b' : 'Second', 'c' : 'Third'};
     *
     *     OptionModal.launchModal('Which one?', choices)
     *     .done(function(selected) {
     *         alert('You selected: ' + selected);
     *     });
     * });
     *
     * @module 'helpers/option_list.modal'
     */

    'use strict';

    var ModalOption,
        ModalOptionCollection,
        ModalView,
        ModalBodyOptionView;

    /**
     * Backbone model for an option.
     * @constructor
     */
    ModalOption = Backbone.Model.extend({
        defaults : {
            title : ''
        }
    });

    /**
     * Option collection.
     * @constructor
     */
    ModalOptionCollection = Backbone.Collection.extend({
        model : ModalOption
    });

    /**
     * Model view for one option. Triggers 'optionSelect' event on when clicked.
     * @constructor
     */
    ModalBodyOptionView = Backbone.Marionette.ItemView.extend({
        template : Template.option,

        tagName : 'li',

        ui: {
            link : '.js-link'
        },

        events : {
            'click @ui.link' : 'linkClicked'
        },

        /**
         * Triggers event when user clicks this option.
         *
         * @fires optionSelect
         * @param e
         */
        linkClicked: function(e) {
            e.preventDefault();

            /**
             * @event optionSelect
             * @property {string} selectedId - Id of the clicked choice.
             */
            this.triggerMethod('optionSelect', this.model.get('id'));
        }
    });

    /**
     * Renders Bootstrap modal popup. Attaches modal to the document body and resolves the promise if
     * an options is selected or the modal is dismissed.
     * @constructor
     */
    ModalView = Backbone.Marionette.CompositeView.extend({
        childView : ModalBodyOptionView,

        className : 'modal fade',

        template : Template.modal,

        ui: {
            body : '.js-modal-body',
            list : '.js-option-list'
        },

        title : '',

        childEvents: {
            'optionSelect' : 'saveModal'
        },

        /**
         * Initialize the modal.
         *
         * @param {Object} options - Configurations
         * @param {Deferred} options.deferred - Deferred object for resolving modal.
         * @param {string} options.title - Modal title
         */
        initialize: function(options) {
            this.deferred = options.deferred;
            this.title    = options.title;

            this.render();

            this.bindUIElements();

            this.$el.modal('show');

            // Handle the element deletion after hiding.
            this.$el.on('hidden.bs.modal', _.bind(this.destroyModal, this));

            // Add to DOM.
            this.$el.appendTo(document.body);
        },

        attachHtml: function(collectionView, itemView){
            this.ui.list.append(itemView.el);
        },

        serializeData: function() {
            return {
                title : this.title
            };
        },

        /**
         * Handler for the user selecting an option. Resolves promise with the selected choice id.
         *
         * @param childView
         * @param {optionSelect} selectedId - The id of the selected choice.
         * @listens optionSelect
         */
        saveModal: function(childView, selectedId) {
            this.deferred.resolve(selectedId);

            this.$el.modal('hide');
        },

        /**
         * Handler for dismissing the modal, rejects the promise.
         */
        destroyModal: function() {
            this.deferred.reject();

            // Remove element from DOM after the modal is hidden.
            this.destroy();
        },

        onDestroy: function() {
            this.$el.off();
        }
    });

    return {
        /**
         * Launch modal and show given options. Returns a promise object that resolves with the id of the
         * selected choice when successful. Promise is rejected when user dismisses the modal.
         *
         * @param {string} title Modal title
         * @param {Array} choices List of choices (key-value array)
         * @returns {Promise} Promise object
         */
        launchModal: function(title, choices) {
            var deferred = $.Deferred(),
                choicesObj;

            choicesObj = _.map(choices, function(val, key) {
                return { id : key, title: val };
            });

            new ModalView({
                title      : title,
                collection : new ModalOptionCollection(choicesObj),
                deferred   : deferred
            });

            return deferred.promise();
        }
    };
});