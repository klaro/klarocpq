define(['bootbox'], function(bootbox) {
    /**
     * Helper to show a modal box confirming user action. The module returns an object with one static method
     * called 'launchModal' which takes in the text to show in the popup. Calling this method
     * shows the the modal and resolves the promise if user clicks OK - otherwise promise is rejected.
     *
     * @example
     * // Load module and show modal
     * require(['./helpers/confirm.modal', function(ConfirmModal) {
     *     ConfirmModal.launchModal('Are you sure?')
     *     .done(function() {
     *         alert('Action confirmed!');
     *     })
     *     .fail(function() {
     *         alert('Action cancelled.');
     *     });
     * }]);
     * @module 'helpers/confirm.modal'
     */
    'use strict';

    var showModal = function(deferred, confirmText) {
        bootbox.confirm(confirmText, function(result) {
            if(result) {
                deferred.resolve();
            }
            else {
                deferred.reject();
            }
        });
    };

    return {
        /**
         * Shows a modal confirm dialog with the given text.
         *
         * @param {string} confirmText Text to show in the popup.
         * @returns {Promise}
         */
        launchModal: function(confirmText) {
            var deferred = $.Deferred();

            showModal(deferred, confirmText);

            return deferred.promise();
        }
    };
});