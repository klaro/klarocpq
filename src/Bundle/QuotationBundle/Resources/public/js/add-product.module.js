/* global define */
define(['backbone', 'marionette', './helpers/add-product.modal'], function(Backbone, Marionette, AddProductModal) {
    /**
     * Module meant to be attached to be attached to a link that shows a modal before continuing.
     *
     * @module confirm
     * @see module:'helpers/confirm.modal'
     */
    'use strict';

    /**
     * @constructor
     */
    var AddProductLinkView = Backbone.Marionette.ItemView.extend({
        sectionName : null,
        productNames : null,
        quotationId: null,
        revisionId: null,
        phaseId: null,
        disabled: null,

        events : {
            'click' : 'handleClick'
        },

        /**
         * @param {Object} options Configuration options
         * @param {string} options.sectionName Product section name
         * @param {string} options.productNames Array of product names with ids as keys
         * @param {string} options.quotationId Quotation id
         * @param {string} options.revisionId Revision id
         * @param {string} options.phaseId Phase id
         */
        initialize: function(options) {
            this.sectionName = options.sectionName || '';
            this.productNames = options.productNames || {};
            this.quotationId = options.quotationId;
            this.revisionId = options.revisionId;
            this.phaseId = options.phaseId;
            this.productSources = options.productSources;
            this.disabled = !!options.disabled; // boolean
        },

        handleClick: function(e) {
            e.preventDefault();

            // Find other products under this menu.
            var products,
                newProductUrl,
                orderProductsUrl;

            products = _.map(this.$el.parent().find('ul').find('[rel="phase-link"]'), _.bind(function(el, key) {
                var $el = $(el),
                    title = $el.data('title'),
                    type = $el.data('type'),
                    phaseId = $el.data('phase-id');

                return {
                    id: key,
                    title: title,
                    type_id: type,
                    phase_id: phaseId,
                    type: this.productNames[type] || null
                }
            }, this));

            newProductUrl = Routing.generate('klaro_quotation_api_post_revision_add_to_product_list', {
                quotationId : this.quotationId,
                revisionId: this.revisionId,
                phaseId: this.phaseId
            });

            orderProductsUrl = Routing.generate('klaro_quotation_api_post_revision_modify_product_list', {
                quotationId : this.quotationId,
                revisionId: this.revisionId,
                phaseId: this.phaseId
            });

            AddProductModal.launchModal(newProductUrl, orderProductsUrl, this.sectionName, this.productNames, products, this.productSources, this.disabled)
                .done(_.bind(function(redirectUrl) {
                    if(!redirectUrl) {
                        redirectUrl = window.location.pathname + window.location.search
                    }

                    window.location.replace(redirectUrl);
                }, this))
        }
    });

    return AddProductLinkView;
});
