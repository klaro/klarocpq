/* global define */
define(['backbone', 'klaro_routing', './revision.model'], function(Backbone, Routing, RevisionModel) {
    /**
     * Defines Backbone collection for revisions.
     *
     * @module 'models/revision.collection'
     * @see module:'models/revision.model'
     */
    'use strict';

    /**
     * @constructor
     */
    var Collection = Backbone.Collection.extend({
        model: RevisionModel,

        quotationId : null,

        /**
         *
         * @param {Object} options Configuration options
         * @param {int} options.quotationId Quotation Id
         */
        initialize: function(options) {
            this.quotationId = options.quotationId;
        },

        /**
         * Collection url.
         *
         * @returns {string}
         */
        url: function() {
            return Routing.generate('klaro_quotation_api_get_quotation_revisions', {
                quotationId : this.quotationId
            });
        },

        /**
         * @param response
         * @returns {data|*|xhr.data|info.data|.xhr.data|purl.data}
         */
        parse: function(response) {
            return response.data;
        },

        /**
         * Get revision model with given id.
         *
         * @param {int} revisionId Revision Id.
         * @returns {null|RevisionModel} Revision model instance.
         */
        getRevision: function(revisionId) {
            return this.findWhere({ revision_id : revisionId });
        }
    });

    return Collection;
});
