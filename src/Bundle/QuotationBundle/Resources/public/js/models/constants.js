/* global define */
define([], function() {
    'use strict';

    return {
        status: {
            in_process : 'In Process',
            active     : 'Active',
            won        : 'Won',
            lost       : 'Lost',
            hold       : 'Hold'
        },

        order_status: {
            none       : 'Not Ready',
            in_process : 'In Process',
            rejected   : 'Rejected',
            finished   : 'Finished'
        }
    };
});