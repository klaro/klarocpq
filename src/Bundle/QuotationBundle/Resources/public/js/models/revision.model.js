/* global define */
define(['backbone', 'underscore', 'klaro_routing'], function(Backbone, _, Routing) {
    /**
     * Defines Backbone model for a revision.
     *
     * @module 'models/revision.model'
     */
    'use strict';

    /**
     * @constructor
     */
    var RevisionModel = Backbone.Model.extend({
        defaults: {
            quotation_id   : null,
            revision_id    : null,
            user_id        : null,
            type_id        : null,
            qualifiers     : {},
            status         : null,
            identifier     : null,
            title          : null,
            order_status   : null,
            log_message    : null,
            created_at     : null,
            updated_at     : null,
            editable       : false,
            printable      : false,
            won            : false,
            final          : false,
            order          : false,
            latest         : false,
            states         : {},
            order_states   : {}
        },

        parse: function(response) {
            var data = response.data || response;

            data.id = data.quotation_id + '-' + data.revision_id;

            return data;
        },

        url: function() {
            return Routing.generate('klaro_quotation_api_get_quotation_revision', {
                quotationId : this.get('quotation_id'),
                revisionId  : this.get('revision_id'),
                state       : true
            });
        },

        save: function(attrs, options) {
            var route = this.isNew() ? 'klaro_quotation_api_post_quotation_revision' : 'klaro_quotation_api_put_quotation_revision';

            options = _.defaults(options || {}, {
                url: Routing.generate(route, {
                    quotationId : this.get('quotation_id'),
                    revisionId  : this.get('revision_id'),
                    state       : true
                })
            });

            return Backbone.Model.prototype.save.call(this, attrs, options);
        },

        getQualifiers: function() {
            var qualifiers = this.get('qualifiers');

            // If qualifiers are empty, the response may contain an empty array but we need an object.
            return _.isObject(qualifiers) && !_.isArray(qualifiers) ? qualifiers : {};
        },

        getQualifier: function(type) {
            var qualifiers = this.getQualifiers();

            return type in qualifiers ? qualifiers[type] : null;
        },

        setQualifier: function(type, value) {
            // Clone array to fire changes.
            var qualifiers = _.clone(this.getQualifiers());

            qualifiers[type] = value;

            this.set('qualifiers', qualifiers);
        }
    }, {
        singleton: null,

        /**
         * Static helper to return the model as a singleton.
         * @returns {RevisionModel}
         */
        getInstance: function() {
            RevisionModel.singleton = RevisionModel.singleton || new RevisionModel();

            return RevisionModel.singleton;
        }
    });

    return RevisionModel;
});
