/* global define */
define(['backbone', 'klaro_routing'], function(Backbone, Routing) {
    /**
     * Defines Backbone model for quotation.
     *
     * @module 'models/quotation.model'
     */
    'use strict';

    /**
     * @constructor
     */
    var QuotationModel = Backbone.Model.extend({
        idAttribute : 'quotation_id',

        defaults: {
            identifier   : null,
            title        : null,
            quotation_id : null,
            product_line : null,
            created_at   : null,
            updated_at   : null,
            owner        : null,
            won          : false,
            final        : false,
            order        : false
        },

        /**
         * @returns {string}
         */
        url: function() {
            return Routing.generate('klaro_quotation_api_get_quotation', {
                quotationId : this.get('quotation_id'),
                state       : true
            });
        },

        parse: function(response) {
            return response.data;
        }
    }, {
        singleton: null,

        /**
         * Static helper to return the model as a singleton.
         * @returns {QuotationModel}
         */
        getInstance: function() {
            QuotationModel.singleton = QuotationModel.singleton || new QuotationModel();

            return QuotationModel.singleton;
        }
    });

    return QuotationModel;
});
