define(['backbone', 'marionette', './app', 'chosen', 'image-picker'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        options:{
            images: true
        },

        initialize: function(options) {
            this.options.images = options.images || false;

            this._buildWidget();
            this._bindEvents();
        },

        getValue: function() {
            var value = null;
            var inputArray = [];
            _.each(this.$el.find(':selected'), function(el){
                inputArray.push($(el).val())
            });

            var multiple = this.$el.attr('multiple');

            if (typeof multiple !== typeof undefined && multiple !== false) {
                inputArray = JSON.stringify(inputArray);
            }
            else {
                inputArray = inputArray[0];
            }
            if(this.$el.attr('name')) {
                value = {
                    name  : this.$el.attr('name'),
                    value : inputArray
                };
            }

            return value;
        },

        /**
         * Builds the widget.
         */
        _buildWidget: function() {
            this.$el.imagepicker({
                //'hide_select' : false,
                'show_label'  : true
            });
        },

        serializeData: function() {
            return {
                model : this.model.toJSON()
            };
        },
        _bindEvents: function() {
            this.$el.on('change', _.bind(function() {
                this.trigger('change');
            }, this));
        },


        _unbindEvents: function() {
            this.$el.off();
        },

        onClose: function(){
            this._unbindEvents();
        }
    });
});