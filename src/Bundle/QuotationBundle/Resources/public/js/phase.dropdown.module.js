define(['backbone', 'marionette', './app', 'chosen'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        $input : null,

        disabledOnLoad : null,

        options:{
            ajaxUrl: null,      // Load option list from this URL.
            ajaxPreload: false  // If true, loads the options from ajaxUrl right after init, instead of loading on autocomplete.
            // NOTE: use an ajaxUrl that loads the full option list (not keyword-filtered) when using this option.
        },

        initialize: function(options) {
            this.fieldName = options.fieldName || null;
            this.$input = this.$el;

            this.options.ajaxUrl = options.ajaxUrl || null;
            this.ajaxPreload = options.ajaxPreload || null;

            this.disabledOnLoad = (this.$el.prop('disabled') === true);

            this._buildWidget();
            this._bindEvents();
        },

        /**
         * Builds the widget.
         */
        _buildWidget: function() {
            var options = {};
            this.$el.chosen(options);
        },

        _bindEvents: function() {
            var $chosen = this.$el.next('.chosen-container'),
                $el = this.$el,
                me = this;

            this.$input.on('change', _.bind(function() {
                this.trigger('change');
            }, this));

            // Remove "href" -attribute from anchors to prevent IE's "onbeforeunload" from firing.
            $chosen.find('a').removeAttr('href');

            if(this.options.ajaxUrl) {
                var $input = $chosen.find('input');
                var searchKeyword = '';
                var currentKeyword = null;
                var currentlyRunning = 0;
                var xhr = false;

                /** Loads option list from options.ajaxUrl, appends options to HTML and updates Chosen widget.
                 * To auto-select a specific option on load, specify the option value in the "data-value" attribute of the select element.
                 */
                var loadOptions = _.bind(function() {
                    xhr = $.ajax({
                        url: this.options.ajaxUrl,
                        data: {
                            keyword: $.trim(searchKeyword)
                        },
                        dataType: 'json',
                        success: function(data) {
                            $el.find('option').each(function(option) {
                                if (!$(this).is(':selected')) {
                                    $(this).remove();
                                }
                            });

                            for (var key in data) {
                                var value = data[key];
                                var $option = $('<option />').attr('value', key).html(value);
                                $el.append($option);
                            }

                            var selectedValue = $el.data('value');
                            if (selectedValue) $el.val(selectedValue);

                            $el.trigger('chosen:updated');

                            $input.attr('value', searchKeyword);

                            if(!me.disabledOnLoad) {
                                me.enable();
                            }
                        }
                    });
                }, this);

                if(this.options.ajaxPreload) {
                    // Disable select until option list is loaded.
                    this.disable();
                    loadOptions();
                }
                else {
                    // Default functionality (no preload): load matching options after at least 3 chars entered in search input.
                    $input.keyup(function() {
                        clearTimeout(currentlyRunning);

                        if (xhr) {
                            xhr.abort();
                        }

                        currentlyRunning = setTimeout(function() {
                            searchKeyword = $input.val();

                            if (searchKeyword.length < 3) {
                                return true;
                            }

                            // If the search term did not change at all, quit.
                            if (currentKeyword === searchKeyword) {
                                return true;
                            }

                            currentKeyword = searchKeyword;

                            loadOptions();
                        }, 500);

                        return false;
                    });
                }
            }
        },

        disable: function() {
            var $el = this.$el;
            $el.prop('disabled', true);
            $el.trigger('chosen:updated');
        },

        enable: function() {
            var $el = this.$el;
            $el.prop('disabled', false);
            $el.trigger('chosen:updated');
        },

        _unbindEvents: function() {
            this.$input.off();
        },

        onDestroy: function(){
            this._unbindEvents();
        },

        getValue: function() {
            var value = null;

            if(this.fieldName) {
                value = {
                    name  : this.fieldName,
                    value : this.$input.val()
                };
            }

            return value;
        }
    });
});