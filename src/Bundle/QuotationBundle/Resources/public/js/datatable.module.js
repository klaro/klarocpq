define(['backbone', 'marionette', './app', 'datatables-bootstrap'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        ui: {
            headers : "thead th"
        },

        initialize: function(options) {
            this.bindUIElements();

            var iqHeaders = [],
                oLanguage = {};

            this.ui.headers.each(function (i, e) {
                if($(this).hasClass('sortable')) {
                    iqHeaders[i] = { bSortable: true };
                } else {
                    iqHeaders[i] = { bSortable: false };
                }
            });

            oLanguage = options.olanguage || {};

            $(this.$el).find('tfoot th').each( function () {
                var title = $('#example thead th').eq( $(this).index() ).text();
                $(this).html( '<input type="text" class="form-control input-sm" style="width:100%;" placeholder="Search '+title+'" />' );
            } );

            var table = this.$el.dataTable({
                "sDom": "t<'row'<'col-xs-6 col-md-4 col-lg-3'l><'hidden-xs hidden-sm col-md-4 col-lg-3'i><'col-xs-6 col-md-4 col-lg-6'p>>",
                aoColumns: iqHeaders,
                aaSorting: [],
                oLanguage: oLanguage
            });

            table.DataTable().columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    that
                        .search( this.value )
                        .draw();
                } );
            } );

            var r =  $(this.$el).find('tfoot tr');
            r.find('th').each(function(){
                $(this).css('padding', 8);
            });
            $(this.$el).find('thead').append(r);


        }
    });
});
