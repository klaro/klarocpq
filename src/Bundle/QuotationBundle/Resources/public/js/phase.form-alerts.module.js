define(['backbone', 'marionette', './app'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        initialize: function() {
            this._bindEvents();
        },

        _bindEvents: function() {
            app.vent.on('form:validation', _.bind(this._onValidation, this));
        },

        _onValidation: function(messages) {
            // Show alert if there are other than info messages on the form.
            var msgsWithoutInfo = _.filter(messages, function(msg){
                return msg.type !== 'info';
            });

            if(msgsWithoutInfo.length > 0) {
                this.$el.show();
            }
            else {
                this.$el.hide();
            }
        },

        _unbindEvents: function() {
            app.vent.off('form:validation');
        },

        onDestroy: function() {
            this._unbindEvents();
        }
    });
});