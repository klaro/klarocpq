/* global define */
define([
    'backbone', 'marionette', 'underscore', 'klaro_routing', './templates/priceupdater', './app'
], function(
    Backbone, Marionette, _, Routing, Template, app
) {
    'use strict';

    var PriceModel,
        OrderPriceView,
        OrderPriceDisplayView,
        EditOrderPriceView,
        SalesPriceView,
        SalesTaxView,
        ProfitView,
        ProfitMarginView,
        toMoney, percentageDiscount;

    /*
     * Helper to format number to decimal string.
     */
    toMoney = function(n, decimals, decimal_sep, thousands_sep)
    {
        var c = isNaN(decimals) ? 0 : Math.abs(decimals), //if decimal is zero we must take it, it means user does not want to show any decimal
            d = decimal_sep || ',', //if no decimal separator is passed we use the dot as default decimal separator (we MUST use a decimal separator)

        /*
         according to [http://stackoverflow.com/questions/411352/how-best-to-determine-if-an-argument-is-not-sent-to-the-javascript-function]
         the fastest way to check for not defined parameter is to use typeof value === 'undefined'
         rather than doing value === undefined.
         */
            t = (typeof thousands_sep === 'undefined') ? ' ' : thousands_sep, //if you don't want to use a thousands separator you can pass empty string as thousands_sep value

            sign = (n < 0) ? '-' : '',

        //extracting the absolute value of the integer part of the number and converting to string
            i = parseInt(n = Math.abs(n).toFixed(c)) + '',
            j;
        j = ((j = i.length) > 3) ? j % 3 : 0;
        return sign + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
    };

    /**
     * Calculate percentage difference between two numbers.
     *
     * @param a
     * @param b
     * @returns {number}
     */
    percentageDiscount = function(a, b) {
        return ((a - b) / b) * 100;
    };

    /**
     *
     * @type {*|void}
     */
    PriceModel = Backbone.Model.extend({
        defaults: {
            quotation_id : null,
            revision_id : null,
            sales_price_with_tax : 0,
            sales_tax : [],
            custom_sales_price : 0,
            original_sales_price_with_tax : 0,
            cost_price : 0,
            profit : 0,
            margin : 0,
            currency : 'EUR'
        },

        toJSON: function() {
            return _.extend(this.attributes, {
                original_price_formatted : this.getFormattedOriginalSalesPrice(),
                sales_price_formatted    : this.getFormattedSalesPrice(),
                diff_formatted           : toMoney(percentageDiscount(this.getSalesPrice(), this.getOriginalSalesPrice()), 2)
            });
        },

        getSalesPrice: function() {
            return this.get('sales_price_with_tax');
        },

        getFormattedSalesPrice: function() {
            return toMoney(this.getSalesPrice());
        },

        getOriginalSalesPrice: function() {
            return this.get('original_sales_price_with_tax');
        },

        getFormattedOriginalSalesPrice: function() {
            return toMoney(this.getOriginalSalesPrice());
        },

        getProfitMargin: function() {
            return this.get('margin');
        },

        getFormattedProfit: function() {
            return toMoney(this.get('profit'));
        },

        getFormattedProfitMargin: function() {
            return toMoney(this.getProfitMargin(), 2);
        },

        getCurrency: function() {
            return this.get('currency');
        },

        url: function() {
            return Routing.generate('klaro_quotation_api_post_revision_summary', {
                quotationId : this.get('quotation_id'),
                revisionId : this.get('revision_id')
            });
        },

        parse: function(response) {
            return response.data;
        }
    });

    /**
     *
     * @type {*|void}
     */
    OrderPriceDisplayView = Backbone.Marionette.ItemView.extend({
        template : Template.price,

        serializeData: function() {
            return {
                model : this.model.toJSON()
            };
        }
    });

    /**
     *
     * @type {*|void}
     */
    EditOrderPriceView = Backbone.Marionette.ItemView.extend({
        template : Template.edit,

        ui: {
            priceInput    : '.js-price-input',
            saveButton    : '.js-save-button',
            restoreButton : '.js-restore-button'
        },

        events: {
            'click @ui.saveButton'    : 'saveClicked',
            'click @ui.restoreButton' : 'restoreClicked'
        },

        saveClicked: function(e) {
            e.preventDefault();

            this.ui.saveButton.prop('disabled', true).html('<i class="fa fa-spinner"></i>');

            app.vent.trigger('notification', {
                message: 'Updating...',
                id: 'price-save'
            });

            this.model.save({
                custom_sales_price : Number(this.ui.priceInput.val())
            })
            .done(_.bind(function() {
                app.vent.trigger('notification', {
                    message: 'Saved.',
                    type : 'success',
                    id: 'price-save'
                }, 800);

                this.triggerMethod('price:save');
            }, this))
            .fail(_.bind(function() {
                app.vent.trigger('notification', {
                    message: 'There was an error while processing your request.',
                    type: 'error',
                    showCloseButton: true,
                    id: 'price-save',
                    actions: {
                        cancel: {
                            label: 'Refresh',
                            action: function() {
                                window.location.href = window.location.pathname + window.location.search;
                            }
                        }
                    }
                });

                this.ui.saveButton.prop('disabled', false).html('<i class="fa fa-check"></i>');
            }, this));
        },

        restoreClicked: function(e) {
            e.preventDefault();

            this.ui.priceInput.val(Number(this.model.getOriginalSalesPrice()).toFixed(0));
        },

        onShow: function() {
            this.ui.priceInput.focus();
        },

        serializeData: function() {
            return {
                model : this.model.toJSON()
            };
        }
    });

    /**
     *
     * @type {*|void}
     */
    OrderPriceView = Backbone.Marionette.LayoutView.extend({
        ui : {
            button : '.js-edit-save-button'
        },

        regions : {
            content : '.js-sales-price-content'
        },

        events : {
            'click @ui.button' : 'buttonClicked'
        },

        childEvents: {
            'price:save' : 'priceSaved'
        },

        editing : false,

        initialize: function() {
            this.bindUIElements();

            this.getRegion('content').show(new OrderPriceDisplayView({
                model : this.model
            }));

            this.editing = false;
        },

        priceSaved: function() {
            this.editing = false;

            app.vent.trigger('priceupdater:change');

            this.updateView();
        },

        buttonClicked: function(e) {
            e.preventDefault();

            this.editing = !this.editing;
            this.updateView();
        },

        updateView: function() {
            if(this.editing) {
                this.getRegion('content').show(new EditOrderPriceView({
                    model : this.model
                }));

                this.ui.button.html('<i class="fa fa-minus"></i>');
            }
            else {
                this.getRegion('content').show(new OrderPriceDisplayView({
                    model : this.model
                }));

                this.ui.button.html('<i class="fa fa-pencil"></i>');
            }
        }
    });

    /**
     *
     * @type {*|void}
     */
    SalesPriceView = Backbone.Marionette.ItemView.extend({
        ui: {
            title : '.js-sales-price-title',
            value : '.js-sales-price-value'
        },

        modelEvents: {
            'change' : 'render'
        },

        initialize: function() {
            this.bindUIElements();

            this.render();
        },

        render: function() {
            var originalPrice = Number(this.model.getOriginalSalesPrice()).toFixed(0),
                salesPrice = Number(this.model.getSalesPrice()).toFixed(0);

            if(salesPrice == originalPrice) {
                this.ui.value.html(this.model.getFormattedSalesPrice() + ' ' + this.model.getCurrency());
            }
            else {
                this.ui.value.html(
                    '<span style="text-decoration: line-through;">' +
                        this.model.getFormattedOriginalSalesPrice() + ' ' + this.model.getCurrency() +
                    '</span>'
                );
            }
        }
    });

    /**
     *
     * @type {*|void}
     */
    SalesTaxView = Backbone.Marionette.ItemView.extend({
        ui: {
            value : '.js-sales-tax-value'
        },

        modelEvents: {
            'change' : 'render'
        },

        initialize: function() {
            this.bindUIElements();
        },

        render: function() {
            var content = _.reduceRight(this.model.get('sales_tax'), _.bind(function(memo, amount, percentage) {
                memo += toMoney(amount) + ' ' + this.model.getCurrency() + ' (' + percentage + ' %)' + '<br />';

                return memo;
            }, this), '');

            this.ui.value.html(content);
        }
    });

    /**
     *
     * @type {*|void}
     */
    ProfitView = Backbone.Marionette.ItemView.extend({
        ui: {
            value : '.js-profit-value'
        },

        modelEvents: {
            'change' : 'render'
        },

        initialize: function() {
            this.bindUIElements();
        },

        render: function() {
            this.ui.value.text(this.model.getFormattedProfit() + ' ' + this.model.getCurrency());
        }
    });

    /**
     *
     * @type {*|void}
     */
    ProfitMarginView = Backbone.Marionette.ItemView.extend({
        ui: {
            value : '.js-profit-margin-value'
        },

        modelEvents: {
            'change' : 'render'
        },

        initialize: function() {
            this.bindUIElements();
        },

        render: function() {
            this.ui.value.text(this.model.getFormattedProfitMargin() + ' %');
        }
    });

    /**
     *
     */
    return Backbone.Marionette.LayoutView.extend({
        regions: {
            orderPrice : '.js-order-price-row',
            salesPrice : '.js-sales-price-row',
            salesTax   : '.js-sales-tax-row',
            profit     : '.js-profit-row',
            margin     : '.js-profit-margin-row'
        },

        initialize: function(options) {
            this.bindUIElements();

            this.model = new PriceModel(options.prices);

            if(this.getRegion('orderPrice').$el.length > 0) {
                this.getRegion('orderPrice').attachView(new OrderPriceView({
                    el : this.getRegion('orderPrice').$el,
                    model : this.model
                }));
            }

            if(this.getRegion('salesPrice').$el.length > 0) {
                this.getRegion('salesPrice').attachView(new SalesPriceView({
                    el : this.getRegion('salesPrice').$el,
                    model : this.model
                }));
            }

            if(this.getRegion('salesTax').$el.length > 0) {
                this.getRegion('salesTax').attachView(new SalesTaxView({
                    el : this.getRegion('salesTax').$el,
                    model : this.model
                }));
            }

            if(this.getRegion('profit').$el.length > 0) {
                this.getRegion('profit').attachView(new ProfitView({
                    el : this.getRegion('profit').$el,
                    model : this.model
                }));
            }

            if(this.getRegion('margin').$el.length > 0) {
                this.getRegion('margin').attachView(new ProfitMarginView({
                    el : this.getRegion('margin').$el,
                    model : this.model
                }));
            }
        }
    });
});
