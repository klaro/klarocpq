require(['klaro_quotation', 'klaro_routing', 'jquery', 'underscore', 'messenger', 'velocity', 'velocity-ui'],
    function (app, Routing, $, _, messengers, velocity, velocityui) {
        "use strict";

        var messenger = new Messenger();
        var helpTexts = {};

        $(document).ajaxComplete(function() {
            putTexts();
        });

        // show modal on dblclick
        $(document).on('dblclick', 'span.help', function() {
            var content     = $(this).attr('data-content');
            var fieldname   = $(this).data('fieldname');
            var update      = false;

            if (!_.isUndefined(content) && content.length) {
                update = true;
            }
            else {
                content = '';
            }

            var compiled = _.template($('script#add-edit-help-template').html());
            $('#help-text-modal').html(compiled({'update': update, 'content': content, 'fieldname': fieldname}));

            $('#add-edit-help-modal').css('left', '0px');
            $('#add-edit-help-modal').velocity('transition.slideLeftBigIn').modal('show');
            $('#add-edit-help-textarea').focus();
        });

        //  remove disabled button on any kind of change in textarea
        $(document).on('keyup', 'div#add-edit-help-modal textarea', function() {
            $('#save-button').prop('disabled', false);
        });

        $(document).on('click', 'div#add-edit-help-modal #save-button', function() {
            // save ajax
            var fieldname  = $('div#add-edit-help-modal #fieldname').val();
            var phaseId    = $('form#phase').data('form-key');
            var content    = $('div#add-edit-help-modal textarea').val();

            var path       = Routing.generate('klaro_help_texts_add_or_update');

            // hide and try to update
            $('#add-edit-help-modal').velocity('transition.bounceRightOut', function() {
                $('#add-edit-help-modal').modal('hide');
            });

            var message = messenger.post({
                type: 'success',
                message: 'Saving...'
            });

            $.post(path, {'application' : application, 'phaseId' : phaseId, 'fieldname': fieldname, 'description': content}, function() {
                updateHelpSpan(fieldname, content);

                // show notification
                message.update({
                    type: 'success',
                    message: 'Help text saved'
                });

                helpTexts[fieldname] = {'fieldname': fieldname, 'description': content};
            })
            .fail(function() {
                // show notification
                message.update({
                    type: 'error',
                    message: 'Failed to update help text'
                });
            });
        });

        var putTexts = function() {
            $('span[id^=help-text-for-]').filter(
                function() {
                    var fieldname = this.id.match(/help-text-for-(.*)/)[1];

                    if ((fieldname in helpTexts) && (helpTexts[fieldname].description.length)) {
                        updateHelpSpan(fieldname, helpTexts[fieldname].description);
                    }
                    else {
                        var fieldHelp = $(this);

                        if (!fieldHelp.hasClass('keep-help-text')) {
                            fieldHelp.hide();

                            var fieldInfo = $('span#info-text-for-' + fieldname);

                            if (fieldInfo.length) {
                                fieldInfo.show();
                            }
                        }
                    }
                });
        };

        var updateHelpSpan = function(fieldname, content) {
            var field = $('span#help-text-for-' + fieldname);

            field.css('color', content.length > 0 ? '' : 'lightgray');
            field.attr('data-content', content.length > 0 ? content : null);
        };

        $(function() {
            var phaseId = $('form#phase').data('form-key');

            if (phaseId !== undefined) {
                var path = Routing.generate('klaro_help_texts_get_phase');

                $.get(path, {'application' : application, 'phaseId' : phaseId}, function (data) {
                    helpTexts = data;
                    putTexts(data);
                })
                .fail(function () {
                    messenger.post({
                        type: 'error',
                        message: 'Failed to get help texts'
                    });
                });
            }
        });
});
