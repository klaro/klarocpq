/* global define */
define([
    'backbone', 'marionette', 'underscore', './models/revision.model', './templates/revision.qualifiers', './app'
], function(
    Backbone, Marionette, _, RevisionModel, Template, app
) {
    'use strict';

    var ButtonOptionModel,
        ButtonOptionCollection,
        ButtonOptionView,
        QualifierButtonView;

    ButtonOptionModel = Backbone.Model.extend({
        defaults: {
            title : ''
        }
    });

    ButtonOptionCollection = Backbone.Collection.extend({
        model : ButtonOptionModel
    });

    ButtonOptionView = Backbone.Marionette.ItemView.extend({
        tagName  : 'li',
        template : Template.optionView,

        events : {
            'click a' : 'linkClicked'
        },

        linkClicked: function(e) {
            e.preventDefault();

            this.triggerMethod('setValue', this.model.get('id'));
        }
    });

    QualifierButtonView = Backbone.Marionette.CompositeView.extend({
        childView : ButtonOptionView,

        tagName : 'ul',
        template : Template.buttonView,

        viewTitle : '',
        qualifierType : '',

        modelEvents: {
            'change' : 'render'
        },

        childEvents: {
            'setValue' : 'onSetValue'
        },

        ui: {
            optionList : '.js-revision-button-options'
        },

        serializeData : function() {
            var option = this.collection.get(this.model.getQualifier(this.qualifierType));

            if(!option) {
                option = this.collection.first();
            }

            return {
                title : this.viewTitle,
                value : option ? option.get('title') : 'n/a',
                model : this.model.toJSON()
            };
        },

        onSetValue: function(view, value) {
            var previousValue = this.model.getQualifier(this.qualifierType);

            if(value == previousValue) {
                return;
            }

            this.model.setQualifier(this.qualifierType, value);

            app.vent.trigger('notification', {
                message: 'Updating...',
                id: 'revision-save'
            });

            this.model.save({}, {
                success : function() {
                    // TODO: move to subscriber.
                    app.vent.trigger('notification', {
                        message: 'Saved.',
                        type : 'success',
                        id: 'revision-save'
                    }, 800);

                    // Trigger phase form update.
                    app.vent.trigger('form:submit');
                },
                error: function() {
                    app.vent.trigger('notification', {
                        message: 'There was an error while processing your request.',
                        type: 'error',
                        showCloseButton: true,
                        id: 'revision-save',
                        actions: {
                            cancel: {
                                label: 'Refresh',
                                action: function() {
                                    window.location.href = window.location.pathname + window.location.search;
                                }
                            }
                        }
                    });
                }
            });
        },

        attachHtml: function(collectionView, itemView){
            this.ui.optionList.append(itemView.el);
        },

        initialize: function(options) {
            this.viewTitle     = options.viewTitle;
            this.qualifierType = options.qualifierType;
        },

        onRender: function() {
            this.$el.find('.dropdown-toggle').dropdown();
        }
    });

    return Backbone.Marionette.ItemView.extend({
        ui: {
            qualifiers : '.js-revision-qualifier'
        },

        revision : null,

        initialize: function(options) {
            this.bindUIElements();

            this.revision = RevisionModel.getInstance();
            this.revision.set(options.revision);

            // Make view for each qualifier selection.
            _.each(this.ui.qualifiers, _.bind(function(qualifier) {
                var $el        = Backbone.$(qualifier),
                    options    = _.map($el.data('options'), function(val, key) {
                        return { id : key, title: val };
                    }),
                    type       = $el.data('qualifier'),
                    title      = $el.data('title'),
                    collection = new ButtonOptionCollection(options);

                new QualifierButtonView({
                    viewTitle     : title,
                    qualifierType : type,
                    el            : $el,
                    model         : this.revision,
                    collection    : collection
                }).render();
            }, this));
        }
    });
});
