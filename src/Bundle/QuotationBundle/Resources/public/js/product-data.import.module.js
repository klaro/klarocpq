/* global define */
define([
    'backbone', 'marionette', 'klaro_routing', './helpers/product-data.import.modal'
], function(
    Backbone, Marionette, ROuting, importModal
) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        sources : null,
        quotationId: null,
        revisionId: null,
        phaseId : null,

        events: {
            'click' : 'doImport'
        },

        initialize: function(options) {
            this.sources = options.sources;
            this.quotationId = options.quotationId;
            this.revisionId = options.revisionId;
            this.phaseId = options.phaseId;

            this.bindUIElements();

            var importSources = this.sources.phaseMappings[this.phaseId] || [];

            if(importSources.length < 1) {
                this.$el.attr('disabled', true);
                this.$el.parent().addClass('disabled');
            }
        },

        doImport: function(e) {
            e.preventDefault();

            if(!this.$el.attr('disabled')) {
                var saveUrl = Routing.generate('klaro_quotation_api_post_revision_import_product_data', {
                        quotationId: this.quotationId,
                        revisionId: this.revisionId,
                        phaseId: this.phaseId
                    }),
                    choices = _.map(this.sources.phaseMappings[this.phaseId], _.bind(function(value) {
                    var values = this.sources.productPhases[value] || {};

                    return {
                        id: value,
                        type: values.productTitle || '',
                        title: values.title || ''
                    }
                }, this));

                importModal.launchModal(choices, saveUrl)
                    .done(function () {
                        window.location.replace(window.location.pathname + window.location.search);
                    });
            }
        }
    });
});
