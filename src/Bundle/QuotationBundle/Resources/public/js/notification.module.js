define(['backbone', 'marionette', './app', 'messenger-flat'], function(Backbone, Marionette, app) {
    'use strict';

    return Backbone.Marionette.ItemView.extend({
        messenger: null,
        initialize: function (options) {
            Messenger.options = {
                extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
                theme: 'flat'
            };

            this.messenger = Messenger();

            app.vent.on('notification', _.bind(this._showMessage, this));
            app.vent.on('notification-async', _.bind(this._showAsyncMessage, this));
        },
        notificationOptions: {
            error: {
                type: 'error',
            },
            progress: {
                hideAfter: 0, // Disable hiding toast while AJAX request is being executed
                type: 'progress',
            },
            success: {
                hideAfer: 2,
                type: 'success',
            }
        },

        _getNotificationOptions(options, type) {
            const opts = Object.assign({}, this.notificationOptions[type] || {}, options[type]);

            return Object.assign({}, {
                id: options.id || 'notification.async-message'
            }, opts);
        },

        _showMessage: function (options, timeout) {
            var msg = this.messenger.post(options);

            if (timeout) {
                setTimeout(function () {
                    msg.hide();
                }, 800);
            }
        },

        /**
         * Generate asyncyhronous messages for the ajax operation
         *
         * @param ajaxOptions jQuery $.ajax([settings])
         * @param notificationOptions
         * @private
         */
        _showAsyncMessage: function (ajaxOptions, notificationOptions) {
            const _this = this;

            // Show up "progress" toast
            this.messenger.post(this._getNotificationOptions(notificationOptions, 'progress'));

            // Perform the AJAX Operation
            $
                .ajax(ajaxOptions)
                .done(function () {
                    _this.messenger.post(_this._getNotificationOptions(notificationOptions, 'success'));
                })
                .fail(function () {
                    _this.messenger.post(_this._getNotificationOptions(notificationOptions, 'error'));
                });
        }
    });
});
