<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\ConfiguratorAdapter;

use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\ConfigurationAdapter\ConfigurationAdapterInterface;
use Klaro\Component\ProductPhase\ProductLine;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConfiguratorAdapter implements ConfigurationAdapterInterface
{
    /** @var array */
    protected $adapters;

    /** @var ConfigurationAdapterInterface[] */
    protected $loadedServices;

    /** @var ContainerInterface */
    protected $container;

    /** @var array */
    protected $productLines;

    /**
     * ConfiguratorAdapterManager constructor.
     */
    public function __construct(ContainerInterface $container, $productLines)
    {
        $this->container = $container;
        $this->productLines = $productLines;
    }

    /**
     * @return array
     */
    public function getAdapters()
    {
        return $this->adapters;
    }

    /**
     * @param  array $adapters
     */
    public function setAdapters($adapters)
    {
        $this->adapters = $adapters;
    }

    /**
     * @param $alias
     * @param $serviceId
     */
    public function addAdapter($alias, $serviceId)
    {
        $this->adapters[$alias] = $serviceId;
    }

    /**
     * @param  ConfiguratorDefinitionNode $configuration
     * @param  ProductLineDataNode        $productLine
     *
     * @return mixed
     */
    public function process(ConfiguratorDefinitionNode $configuration, ProductLineDataNode $productLine)
    {
        /** @var ProductLine $productLinePhase */
        $productLinePhase = $productLine->getPhaseNode();
        $productLineId = $productLinePhase->getProductLineDefinition()->getId();

        $adapterName = $this->productLines[$productLineId]['adapter'];

        return $this->getAdapter($adapterName)->process($configuration, $productLine);
    }

    /**
     * @param $adapterName
     *
     * @return ConfigurationAdapterInterface
     */
    public function getAdapter($adapterName)
    {
        if (!isset($this->loadedServices[$adapterName])) {
            if (!isset($this->adapters[$adapterName])) {
                throw new \Exception('Adapter not found!');
            }

            $this->loadedServices[$adapterName] = $this->container->get($this->adapters[$adapterName]);
        }

        return $this->loadedServices[$adapterName];
    }
}
