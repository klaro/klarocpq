<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\AdminPage;

use Klaro\QuotationBundle\Api\AdminServicesInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manager service for admin pages, ie. services tagged with name `klaro_quotation.admin_page_handler`.
 * Admin pages are lazy loaded only when accessing the admin section.
 *
 * Class AdminPageManager
 * @package Klaro\QuotationBundle\AdminPage
 */
class AdminPageManager
{
    /** @var string[] */
    protected $serviceNames;

    /** @var ContainerInterface */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->serviceNames = [];
    }

    /**
     * @param $alias
     * @param $serviceName
     */
    public function addAdminPage($alias, $serviceName)
    {
        $this->serviceNames[$alias] = $serviceName;
    }

    /**
     * @param $alias
     *
     * @return AdminServicesInterface
     */
    public function getAdminPage($alias)
    {
        if (!isset($this->serviceNames[$alias])) {
            throw new \Exception('Admin page with name "'.$alias.'" not found!');
        }

        $service = $this->container->get($this->serviceNames[$alias]);

        if (!($service instanceof AdminServicesInterface)) {
            throw new \Exception('Admin page service "'.$this->serviceNames[$alias].'" with name "'.$alias.'" does not implement AdminServicesInterface.');
        }

        return $service;
    }

    /**
     * @return array|\string[]
     */
    public function getAdminPages()
    {
        return $this->serviceNames;
    }

    /**
     * @return int
     */
    public function getAdminPagesCount()
    {
        return count($this->serviceNames);
    }
}
