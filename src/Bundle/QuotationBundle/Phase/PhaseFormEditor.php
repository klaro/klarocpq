<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase;

use Klaro\Component\ExpressionEvaluator\ExpressionEvaluatorInterface;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\FormData\SinglePhaseDataNode;
use Klaro\Component\ProductPhase\PhaseDefinition;
use Klaro\Component\Common\Model\FormModelInterface;
use Klaro\Component\ProductPhase\PhaseNode;
use Klaro\Component\ProductPhase\SinglePhase;
use Klaro\Component\Validation\ValidationResult;
use Klaro\QuotationBundle\Api\FormModelProviderInterface;
use Klaro\Component\Common\Model\ModelInterface;
use Klaro\QuotationBundle\Api\PhaseEditorInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationBundle\Api\RevisionPhaseConfigCacheInterface;
use Klaro\QuotationBundle\Api\RevisionPhaseConfigCacheManagerInterface;
use Klaro\QuotationBundle\Cache\RevisionPhaseConfigCacheAdapter;
use Klaro\QuotationBundle\Event\PhaseEvent;
use Klaro\QuotationBundle\Library\ExpressionEvaluatorFactory;
use Klaro\QuotationBundle\StateMachine\RevisionStateMachine;
use Klaro\QuotationBundle\Validation\PhaseDataValidatorManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Editor for phase forms.
 *
 * Class PhaseFormEditor
 * @package Klaro\QuotationBundle\Library
 */
class PhaseFormEditor implements PhaseEditorInterface, PhaseEditorContextInterface
{
    /** @var EventDispatcherInterface */
    protected $dispatcher;

    /** @var FormModelProviderInterface */
    protected $modelProvider;

    /** @var ExpressionEvaluatorFactory */
    protected $evaluatorFactory;

    /** @var ExpressionEvaluatorInterface */
    protected $evaluator;

    /** @var Phase */
    protected $phase;

    /** @var RevisionStateMachine */
    protected $stateMachine;

    /** @var PhaseVariableFactory */
    protected $phaseVariableFactory;

    /** @var PhaseDataValidatorManager */
    protected $dataValidator;

    /** @var QuotationRevisionInterface */
    protected $revision;

    /** @var ProductLineDataNode */
    protected $phaseData;

    /** @var SinglePhaseDataNode */
    protected $currentPhase;

    /** @var PhaseDefinition */
    protected $phaseDefinition;

    /** @var bool */
    protected $userInputProcessed;

    /** @var FormModelInterface */
    protected $model;

    /** @var RevisionPhaseConfigCacheManagerInterface */
    protected $cacheManager;

    /**
     * PhaseFormEditor constructor.
     * @param EventDispatcherInterface                      $dispatcher
     * @param FormModelProviderInterface                    $modelProvider
     * @param ExpressionEvaluatorFactory                    $evaluatorFactory
     * @param Phase                                         $phase
     * @param PhaseVariableFactory                          $phaseVariableFactory
     * @param PhaseDataValidatorManager                     $dataValidator
     * @param RevisionPhaseConfigCacheManagerInterface|null $cacheManager
     */
    public function __construct(
        EventDispatcherInterface $dispatcher,
        FormModelProviderInterface $modelProvider,
        ExpressionEvaluatorFactory $evaluatorFactory,
        Phase $phase,
        PhaseVariableFactory $phaseVariableFactory,
        PhaseDataValidatorManager $dataValidator,
        RevisionPhaseConfigCacheManagerInterface $cacheManager = null
    ) {
        $this->dispatcher          = $dispatcher;
        $this->modelProvider       = $modelProvider;
        $this->evaluatorFactory = $evaluatorFactory;
        $this->phase               = $phase;
        $this->phaseVariableFactory = $phaseVariableFactory;
        $this->dataValidator = $dataValidator;
        $this->cacheManager        = $cacheManager;
        $this->evaluator = null;
        $this->userInputProcessed = false;
    }

    public function getProductPhaseData($phase)
    {
        $productPhase = $this->currentPhase->getParent()->getChild($phase);

        return $productPhase ? $productPhase->getData() : null;
    }

    public function getProductPhaseDataItemsDefinition($phase)
    {
        $productPhase = $this->currentPhase->getParent()->getChild($phase);

        $phaseDefinition = $productPhase ? $productPhase->getPhaseNode()->getPhaseDefinition() : null;

        $phaseItemsDefinition = $phaseDefinition->getOriginalConfig();

        return $phaseItemsDefinition['items'] ?? null;
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param ProductLineDataNode        $formData
     * @param $phasePath
     */
    public function initialize(QuotationRevisionInterface $revision, ProductLineDataNode $formData, $phasePath)
    {
        $this->revision = $revision;
        $this->phaseData = $formData;

        $this->currentPhase = $formData->getSinglePhaseByPath($phasePath);
        $phaseNode = $this->currentPhase->getPhaseNode();

        if ($phaseNode instanceof SinglePhase) {
            $this->phaseDefinition = $phaseNode->getPhaseDefinition();
            $this->phaseDefinition->setPhaseId($this->currentPhase->getPath());
        }

        $parentPhase = $this->currentPhase->getParent();

        $this->evaluator = $this->evaluatorFactory->getEvaluator($revision, [
            'globals' => $this->phaseVariableFactory->createVariables($revision, $formData),
            'user' => $this->modelProvider->getCurrentUser(),
            'product' => ($parentPhase->getPhaseNode()->getType() === PhaseNode::PRODUCT) ? $parentPhase : null,
        ]);

        $this->model = $this->currentPhase->getData();

        $this->stateMachine = RevisionStateMachine::create($this->revision);

        $cache = null;

        // Create cache class to store phase configs when editor is in read-only state.
        if ($this->cacheManager instanceof RevisionPhaseConfigCacheManagerInterface) {
            $configCache = $this->cacheManager->getRevisionPhaseConfigCache($revision);

            if ($configCache instanceof RevisionPhaseConfigCacheInterface) {
                $cache = new RevisionPhaseConfigCacheAdapter($configCache);
            }
        }

        $this->phase->initialize($this, $this->phaseDefinition, $this->model, $cache);
    }

    /**
     * {@inheritDoc}
     */
    public function processUserInput(Request $request)
    {
        $params = (array) json_decode($request->request->get('phaseData'), true);

        $this->phase->processUserInput($params);
        $this->userInputProcessed = true;
    }

    /**
     * {@inheritDoc}
     */
    public function postProcess()
    {
        $this->phase->postProcess();
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        return $this->phase->render();
    }

    public function getJson($help)
    {
        $items = [];
        foreach ($this->phase->getItems() as $item) {
            if (!$item->doPrerequisitesPass()) {
                continue;
            }

            $currentItem = $this->getItemJson($item, $help ?: []);
            if ($item->hasItems()) {
                $subItems = [];
                foreach ($item->getItems() as $subItem) {
                    if (!$subItem->doPrerequisitesPass()) {
                        continue;
                    }

                    $subItemJson = $this->getItemJson($subItem, $help ?: []);
                    $subItems[] = $subItemJson;
                }
                $currentItem['items'] = $subItems;
            }

            if (!empty($item->getMessages())) {
                $currentItem['messages'] = $item->getMessages();
            }
            $items[] = $currentItem;
        }

        $data = array(
            'model' => $this->phase->getModel(),
            'items' => $items,
        );

        return $data;
    }

    // TODO: ability to return templates
    protected function getItemJson(PhaseItemInterface $item, array $help)
    {
        $fieldName = $item->getFieldName();
        $json = [
            'text' => $item->getText(),
            'fieldName' => $fieldName,

            'type' => $item->getConfig()['type'],
            'value' => $item->getConfig()['value'],
            'disabled' => $item->getConfig()['disabled'],

            'colWidth' => $item->getConfig()['colWidth'],
            'hideLabel' => $item->getConfig()['hideLabel'],
        ];

        if ($item->getConfig()['unit']) {
            $json['unit'] = $item->getConfig()['unit'];
        }

        if (isset($item->getConfig()['options'])) {
            $json['options'] = $item->getConfig()['options'];
        }
        if (isset($item->getConfig()['optionsSourceType']) && $item->getConfig()['optionsSourceType'] === 'url') {
            $json['optionsSource'] = $item->getConfig()['optionsSource'];
        }

        if ($item->getConfig()['type'] === 'text' && isset($item->getConfig()['size'])) {
            $json['type'] = 'textArea';
            $json['size'] = $item->getConfig()['size'];
        }
        if ($item->getConfig()['type'] === 'inlineForm') {
            $json['showHeaders'] = $item->getConfig()['showHeaders'];
        }
        if ($item->getConfig()['type'] === 'picture') {
            $json['url'] = $item->getConfig()['url'];
        }
        if ($item->getConfig()['type'] === 'title') {
            $json['titleLevel'] = $item->getConfig()['titleLevel'];
        }

        if (array_key_exists('editable', $help)) {
            $json['helpText']['editable'] = $help['editable'];

            if (array_key_exists($fieldName, $help['values'])) {
                $json['helpText']['value'] = $help['values'][$fieldName];
            } else {
                $json['helpText']['value'] = null;
            }
        }

        return $json;
    }

    /**
     * {@inheritDoc}
     */
    public function persistModel(ModelInterface $model)
    {
        $this->notify(PhaseEvents::BEFORE_PHASE_SAVE);

        $result = new ValidationResult();

        if ($this->userInputProcessed) {
            $this->dataValidator->validateProductLine($this->phaseData, $result);
        }

        $validationResult = $model->commit();

        $this->revision->setPhaseData($this->phaseData->toArray());
        $this->modelProvider->persistQuotationRevision($this->revision);

        $validationResult->mergeValidationIssues($result);

        $this->notify(PhaseEvents::AFTER_PHASE_SAVE, [
            'validationResult' => $validationResult,
        ]);

        return $validationResult;
    }

    /**
     * {@inheritDoc}
     */
    public function evaluateExpression($expression, $context = [])
    {
        return $this->evaluator->evaluateExpression($expression, $context);
    }

    /**
     * {@inheritDoc}
     */
    public function notify($eventName, $data = [])
    {
        $event = new PhaseEvent($this->revision, $this->phaseData, $data);

        $this->dispatcher->dispatch($eventName, $event);
    }

    /**
     * {@inheritDoc}
     */
    public function getFinder($finderName)
    {
        return $this->modelProvider->getItemFinder($this->phaseDefinition->getAlias($finderName));
    }

    /**
     * {@inheritDoc}
     */
    public function isReadOnly()
    {
        return $this->stateMachine->isEditable() !== true;
    }
}
