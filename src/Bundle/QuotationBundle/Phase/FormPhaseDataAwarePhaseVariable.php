<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase;

use Klaro\Component\FormData\ProductLineDataNode;

interface FormPhaseDataAwarePhaseVariable
{
    public function setFormPhaseData(ProductLineDataNode $formData);
}
