<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase;

/**
 * Defines events generated during different form phases.
 *
 * Class PhaseEvents
 * @package Klaro\Component\Phase
 */
final class PhaseEvents
{
    /**
     * Triggered while saving the form and right before setting the value of a field.
     */
    const BEFORE_ITEM_SAVE = 'klaro_component.phase.item.presave';

    /**
     * Triggered while saving the form and after setting the value of a form field.
     */
    const AFTER_ITEM_SAVE = 'klaro_component.phase.item.postsave';

    /**
     * Triggered while saving the form and after all the form fields' values
     * have been set but before persisting the form field model.
     */
    const BEFORE_PHASE_SAVE = 'klaro_component.phase.presave';

    /**
     * Triggered while saving the form and after persisting the phase model.
     */
    const AFTER_PHASE_SAVE = 'klaro_component.phase.postsave';

    /**
     * Triggered while loading the form and before starting to load values from a form field model to the form.
     */
    const BEFORE_PHASE_LOAD = 'klaro_component.phase.preload';

    /**
     * Triggered while loading the form and after values have been set on the form.
     */
    const AFTER_PHASE_LOAD = 'klaro_component.phase.postload';

    /**
     * Triggered when the form is loading and setting the value of a field from the form field model.
     */
    const AFTER_ITEM_LOAD = 'klaro_component.phase.item.postload';
}
