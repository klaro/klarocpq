<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\PhaseVariable\AbstractPhaseVariable;
use Klaro\Component\PhaseVariable\GlobalPhaseVariableInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PhaseVariableServiceLoader extends AbstractPhaseVariable
{
    /** @var array */
    protected $services;

    /** @var ContainerInterface */
    protected $container;

    /** @var QuotationRevisionInterface */
    protected $revision;

    /** @var ProductLineDataNode */
    protected $formData;

    /** @var GlobalPhaseVariableInterface[] */
    protected $loadedInstances;

    /**
     * PhaseVariableServiceLoader constructor.
     * @param ContainerInterface         $container
     * @param array                      $services
     * @param QuotationRevisionInterface $revision
     * @param ProductLineDataNode        $formData
     */
    public function __construct(
        ContainerInterface $container,
        array $services,
        QuotationRevisionInterface $revision,
        ProductLineDataNode $formData
    ) {
        $this->services = $services;
        $this->container = $container;
        $this->revision = $revision;
        $this->formData = $formData;

        $this->loadedInstances = [];
    }

    /**
     * {@inheritdoc}
     */
    public function has($property)
    {
        return $this->container->has($this->services[$property]);
    }

    /**
     * {@inheritdoc}
     */
    public function get($property, $arguments = [])
    {
        return $this->getVariable($property)->getValue($arguments);
    }

    /**
     * @param $name
     *
     * @return GlobalPhaseVariableInterface
     */
    private function getVariable($name)
    {
        if (!isset($this->loadedInstances[$name])) {
            /** @var GlobalPhaseVariableInterface $provider */
            $provider = $this->container->get($this->services[$name]);

            if ($provider instanceof RevisionAwarePhaseVariable) {
                $provider->setRevision($this->revision);
            }

            if ($provider instanceof FormPhaseDataAwarePhaseVariable) {
                $provider->setFormPhaseData($this->formData);
            }

            $this->loadedInstances[$name] = $provider;
        }

        return $this->loadedInstances[$name];
    }
}
