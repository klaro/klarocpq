<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase;

use Klaro\Component\Dictionary\Dictionary;
use Klaro\Component\Common\Model\ModelInterface;
use Klaro\Component\Logger\Logger;
use Klaro\Component\ProductPhase\PhaseDefinition;
use Klaro\Component\Validation\ValidationIssue;
use Klaro\Component\Validation\ValidationResult;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Klaro\Component\Cache\CacheInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * This class represents a single phase form and takes responsibility for parsing and understanding phase YAML files.
 */
class Phase implements \Serializable, \JsonSerializable
{

    /** @var Stopwatch */
    protected $stopwatch;

    /** @var CacheInterface */
    protected $cache;
    /**
     * @var PhaseItemInterface[]
     */
    private $items = array();

    /** @var PhaseDefinition */
    private $phaseDefinition;

    /**
     * Contains the phase definition as an associative array.
     *
     * @var array
     */
    private $originalConfig;

    /**
     * @var \Symfony\Bundle\TwigBundle\TwigEngine
     */
    private $templating;

    /**
     * The model instance that holds the data for items.
     *
     * @var ModelInterface
     */
    private $model;

    /**
     * Holds messages to be shown in UI (errors, alerts, info).
     *
     * @var array
     */
    private $errorLogger;

    private $postProcessDone = false;

    private $phaseId;

    /** @var PhaseItemFactoryInterface */
    private $factory;

    /** @var PhaseEditorContextInterface */
    private $editorContext;

    /** @var Phase */
    private $parentPhase;

    /**
     * Phase constructor.
     * @param EngineInterface           $templating
     * @param PhaseItemFactoryInterface $factory
     * @param Stopwatch                 $stopwatch
     */
    public function __construct(
        EngineInterface $templating,
        PhaseItemFactoryInterface $factory,
        Stopwatch $stopwatch
    ) {
        $this->templating = $templating;
        $this->factory = $factory;
        $this->stopwatch = $stopwatch;

        // TODO: Get this from service?
        $this->errorLogger = new Logger();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @param PhaseEditorContextInterface $editorContext
     * @param PhaseDefinition             $phaseDefinition
     * @param ModelInterface              $model
     * @param CacheInterface|null         $cache
     *
     * @throws \Exception
     */
    public function initialize(
        PhaseEditorContextInterface $editorContext,
        PhaseDefinition $phaseDefinition,
        ModelInterface $model,
        CacheInterface $cache = null
    ) {
        $this->editorContext = $editorContext;
        $this->phaseDefinition = $phaseDefinition;
        $this->parentPhase = null;

        $this->startTimer('init');

        $this->setCache($cache);

        // Try to get cached config when editor is in read only state.
        $phaseId = $phaseDefinition->getPhaseId();

        if ($editorContext->isReadOnly() && $this->cache && $this->cache->contains($phaseId)) {
            $phaseConfig = $this->cache->fetch($phaseId);
        } else {
            $phaseConfig = $phaseDefinition->getOriginalConfig();
        }

        $this->setPhaseConfigData($phaseConfig, $phaseId);
        $this->setModel($model);
        $this->loadCurrentDataToPhaseItems();

        $this->stopTimer();
    }

    /**
     * @return Phase
     */
    public function getParentPhase()
    {
        return $this->parentPhase;
    }

    /**
     * @param Phase $parentPhase
     */
    public function setParentPhase($parentPhase)
    {
        $this->parentPhase = $parentPhase;
    }

    /**
     * @return CacheInterface
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * @param CacheInterface $cache
     */
    public function setCache($cache)
    {
        $this->cache = $cache;
    }

    /**
     * @return PhaseEditorContextInterface
     */
    public function getEditorContext()
    {
        return $this->editorContext;
    }

    /**
     * @param PhaseEditorContextInterface $editorContext
     */
    public function setEditorContext(PhaseEditorContextInterface $editorContext)
    {
        $this->editorContext = $editorContext;
    }

    /**
     * @param mixed $phaseId
     */
    public function setPhaseId($phaseId)
    {
        $this->phaseId = $phaseId;
    }

    /**
     * @return mixed
     */
    public function getPhaseId()
    {
        return $this->phaseId;
    }

    /**
     * @return array
     */
    public function getOriginalConfig()
    {
        return $this->originalConfig;
    }

    /**
     * Adds a message to be shown in the UI.
     *
     * @param string $type        Message type (error, alert, info, etc.)
     * @param string $message     Message shown to user.
     * @param string $phaseItemId Phase item id or fieldname, if given the message is attached also to the field itself.
     * @param array  $context     Optional context values.
     */
    public function addMessage($type, $message, $phaseItemId = null, $context = array())
    {
        if (method_exists($this->errorLogger, $type)) {
            $this->errorLogger->{$type}($message, ['fieldName' => $phaseItemId] + $context);
        }
    }

    /**
     * Get messages to be shown in the UI.
     *
     * @param string $type Message type (error, alert, info, etc.)
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->errorLogger->getMessages();
    }

    /**
     * @param $originalConfig
     * @param $phaseId
     *
     * @throws \Exception
     */
    public function setPhaseConfigData($originalConfig, $phaseId)
    {
        $this->phaseId = $phaseId;

        $this->originalConfig = $originalConfig;

        // Clear items and recreate the new ones.
        $this->items = array();
        $this->createItems();
    }

    /**
     * Post processes the phase. This is mainly used for automatic selection of single-item radios.
     * @param bool $commit
     */
    public function postProcess($commit = true)
    {
        if ($this->postProcessDone === true) {
            return;
        }

        $this->startTimer('postprocess');

        $alertTypes = [
            ValidationIssue::ERROR,
            ValidationIssue::WARNING,
            ValidationIssue::INFO,
        ];

        // Save model and show validation errors
        if ($commit === true) {
            // process calculations to save recalculated data
            // should build some kind of graph here to fetch fields dependencies and recalculate fields from lowers to uppers
            /** @var PhaseItemInterface $item */
            foreach ($this->getItems() as $item) {
                if ('calculated' == $item->get('type')) {
                    $formulaContext = $this->calculateField($item, true);
                    $item->addResolvedInputs($formulaContext);
                    $item->calculate();
                    $this->model->set($item->getFieldName(), $item->getValue());
                }
            }

            /** @var ValidationResult $validationResult */
            $validationResult = $this->editorContext->persistModel($this->model);

            $currentProductId = null;
            $res = preg_match("[lines/(\d)/.*]Uis", $this->phaseId, $matches);
            if ($res && isset($matches[1])) {
                $currentProductId = $matches[1];
            }

            foreach ($validationResult->getValidationIssues() as $issue) {
                $issueProductId = $issue->getProductLineId();
                if (!is_null($issueProductId) && $issueProductId != $currentProductId) {
                    continue;
                }

                $fieldName = $issue->getKey();
                $phaseItem = $this->getItemByFieldName($fieldName);

                if ($phaseItem) {
                    $message = $issue->format();
                    $alertType = $issue->getType();

                    // Add error to list of processable alerts.
                    $phaseItem->set('alerts', array_merge($phaseItem->get('alerts', []), [[$alertType => $message]]));
                }
            }
        }

        // The data is now in the database, but not yet in the phase items.
        // Save results to current phase items.
        $this->loadCurrentDataToPhaseItems();

        // Helper for processing alerts.
        $alertProcessor = function (PhaseItemInterface $phaseItem) use (&$alertProcessor, &$phase, &$alertTypes) {
            $config = $phaseItem->getConfig();

            $processedAlerts = [];

            // Process alerts
            if (isset($config['alerts']) && is_array($config['alerts']) && count($config['alerts']) > 0) {
                foreach ($config['alerts'] as $alert) {
                    if (!isset($alert['condition']) || $this->evaluateExpression($alert['condition'], $config) === true) {
                        foreach ($alertTypes as $alertType) {
                            if (isset($alert[$alertType])) {
                                // Message is defined by the alert type (info/warning/error)
                                $message = $alert[$alertType];

                                // If evaluate flag is true, treat the message as an expression.
                                if (isset($alert['evaluate']) && $alert['evaluate'] === true) {
                                    $message = $this->evaluateExpression($message, $phaseItem->getConfig());
                                }

                                // Add message to phase item and to the form itself
                                $phaseItem->addMessage($alertType, $message);

                                $fieldName = !empty($config['fieldName']) ? $config['fieldName'] : $phaseItem->getId();

                                $this->addMessage($alertType, $message, $fieldName);

                                $processedAlerts[] = [$alertType => $message];
                            }
                        }
                    }
                }

                $phaseItem->set('alerts', $processedAlerts);
            }
        };

        /** @var PhaseItemInterface $item */
        foreach ($this->getItems() as $item) {
            // need to process calculations with new values
            if ('calculated' == $item->get('type')) {
                $formulaContext = $this->calculateField($item);
                $item->addResolvedInputs($formulaContext);
            }

            // Process alerts
            if ($item->doPrerequisitesPass()) {
                $item->postProcess();

                $alertProcessor($item);

                if ($item->hasItems()) {
                    foreach ($item->getItems() as $subItem) {
                        if ($subItem->doPrerequisitesPass()) {
                            $alertProcessor($subItem);
                        }
                    }
                }
            }
        }

        // Save config to cache if not in read only state.
        if ($this->cache && (!$this->editorContext->isReadOnly() || !$this->cache->contains($this->phaseId))) {
            $this->cache->save($this->phaseId, $this->exportConfig());
        }

        $this->postProcessDone = true;
        $this->stopTimer();
    }

    /**
     * @param PhaseItemInterface     $item
     * @param bool                   $forceCalculations
     *
     * @return array
     */
    private function calculateField($item, $forceCalculations = false)
    {
        $formulaContext = [];
        $inputs = $item->get('inputs');
        if (is_array($inputs)) {
            foreach ($inputs as $name => $value) {
                $inputVal = null;
                // get input value from phase specified
                if (strpos($value, '/')) {
                    [$phase, $inputName] = explode('/', $value);
                    // get field definition
                    $itemsDefinitions = $this->editorContext->getProductPhaseDataItemsDefinition($phase);
                    $inputDef = $this->getFieldDefinition($itemsDefinitions, $inputName);
                    if (null != $inputDef) {
                        // go deeper with calculated fields types
                        if ($inputDef->get('type') == 'calculated') {
                            $inputVal = $this->calculateField($inputDef);
                            if (is_array($inputVal)) {
                                $inputVal = $this->evaluateExpression($inputDef->get('formula'), $inputVal);
                            }
                        }
                        // get value from model for other types
                        else {
                            /** @var FormPhaseData $phaseData */
                            $phaseData = $this->editorContext->getProductPhaseData($phase);
                            $inputVal = $phaseData->get($inputName);
                        }
                    }
                } // get input value from this phase model
                else {
                    $inputName = $value;
                    $inputDef = $this->getItemByFieldName($inputName);
                    if (null !== $inputDef && 'calculated' == $inputDef->get('type')) {
                        $inputVal = $this->calculateField($inputDef);
                        if (is_array($inputVal)) {
                            $inputVal = $this->evaluateExpression($inputDef->get('formula'), $inputVal);
                        }
                    } else {
                        $inputVal = $this->getModel()->get($inputName);
                    }
                }
                $formulaContext[$name] = round($inputVal, 4);
            }
        }

        return $formulaContext;
    }

    /**
     * @param array  $itemsDefinitions
     * @param string $inputName
     *
     * @return PhaseItemInterface|null
     */
    private function getFieldDefinition($itemsDefinitions, $inputName)
    {
        foreach ($itemsDefinitions as $n => $v) {
            $inputDef = null;
            if ($v['type'] == 'inlineForm') {
                $inputDef = $this->getFieldDefinition($v['items'], $inputName);
                if ($inputDef) {
                    return $inputDef;
                }
            } else {
                if (isset($v['fieldName']) && $v['fieldName'] == $inputName) {
                    return $this->createPhaseItem($inputName, $v);
                }
            }
        }

        return null;
    }

    /**
     *
     */
    public function processUserInput($params)
    {
        if (isset($params[$this->getPhaseId()])) {
            $params = $params[$this->getPhaseId()];
        } else {
            return;
        }

        $this->editorContext->notify(PhaseEvents::BEFORE_PHASE_SAVE);

        foreach ($this->items as $phaseItem) {
            $phaseItem->processUserInput($params);
        }
    }

    /**
     * A generic method to retrieve the value for a phase item field.
     *
     * @param $fieldName
     *
     * @return mixed
     */
    public function getPhaseFieldValue($fieldName)
    {
        return $this->model->get($fieldName);
    }

    /**
     * A generic method to set the value for a phase item field.
     *
     * @param $fieldName
     *
     * @return mixed
     */
    public function setPhaseFieldValue($fieldName, $value, $sanitize = true)
    {
        $this->model->set($fieldName, $value, $sanitize);
    }

    /**
     * Returns all phase items.
     *
     * @return PhaseItemInterface[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Returns the item with the specified field name. You may also use the tableKey to find it.
     *
     * @param $fieldName
     *
     * @return PhaseItemInterface
     */
    public function getItemByFieldName($fieldName)
    {
        $eachItem = function ($items) use ($fieldName, &$eachItem) {
            /** @var PhaseItemInterface $item */
            foreach ($items as $item) {
                $config = $item->getConfig();
                if ($item->has('fieldName') && strtolower($config['fieldName']) === strtolower($fieldName)) {
                    return $item;
                }

                if ($item->hasItems()) {
                    $result = $eachItem($item->getItems());

                    if ($result !== null) {
                        return $result;
                    }
                }
            }

            return null;
        };

        return $eachItem($this->items);
    }

    /**
     * Returns the item of the given name.
     *
     * The name is the key that appears in the phase YAML.
     *
     * @param string $name
     *
     * @return PhaseItemInterface|null
     */
    public function getItemByPhaseKey($name)
    {
        foreach ($this->items as $item) {
            if ($item->getId() === $name) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @param $itemName
     * @param $itemConfig
     * @param PhaseItemInterface $parent
     *
     * @return null|PhaseItemInterface
     */
    public function createPhaseItem($itemName, $itemConfig, PhaseItemInterface $parent = null)
    {
        return $this->factory->createPhaseItem($this, $itemName, $itemConfig, $this->editorContext->isReadOnly(), $parent);
    }

    /**
     * @param $expression
     * @param array      $extra
     *
     * @return null|string
     */
    public function evaluateExpression($expression, $extra = array())
    {
        $context = [
            'this'  => (object) [
                'phaseId' => $this->getPhaseId(),
            ],
            'model' => $this->model,
        ];

        return $this->editorContext->evaluateExpression($expression, $context + $extra);
    }

    /**
     * Evaluates the given prerequisite string.
     *
     * Returns a boolean value indicating whether the prerequisite passed.
     *
     * @deprecated Prefer evaluateExpression instead.
     *
     * @param string $prerequisite
     *
     * @throws \Exception
     *
     * @return
     */
    public function evalPrerequisite($prerequisite)
    {
        trigger_error('Deprecated, use evaluateExpression() instead', E_USER_DEPRECATED);
    }

    /**
     * Loads the current data (from the database/source) to all phase items.
     *
     * @param $model object The model instance to use.
     */
    public function loadCurrentDataToPhaseItems()
    {
        $this->postProcessDone = false;

        $this->editorContext->notify(PhaseEvents::BEFORE_PHASE_LOAD);

        foreach ($this->getItems() as $phaseItem) {
            $phaseItem->loadItemData();

            $this->editorContext->notify(PhaseEvents::AFTER_ITEM_LOAD, [
                'phaseItem' => $phaseItem,
                'value' => null,
            ]);
        }

        $this->editorContext->notify(PhaseEvents::AFTER_PHASE_LOAD);
    }

    /**
     * @return string|null
     */
    public function getTemplate()
    {
        return isset($this->originalConfig['template']) ? $this->originalConfig['template'] : null;
    }

    /**
     * @param $template
     */
    public function setTemplate($template)
    {
        $this->originalConfig['template'] = $template;
    }

    /**
     * Returns the render of the phase using the default phase template
     * unless otherwise specified in config.
     *
     * @param array $templateData Custom data that is given to the template.
     */
    public function render($templateData = array())
    {
        if (isset($this->originalConfig['template'])) {
            $template = $this->originalConfig['template'].'.html.twig';
        } else {
            $template = "KlaroQuotationBundle:Phase:phase.html.twig";
        }

        try {
            $templateData += array(
                'model' => $this->getModel(),
                'items' => $this->getItems(),
                'phase' => $this,
                'messages' => $this->getMessages(),
            );

            return $this->renderTemplate($template, $templateData);
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param $template
     * @param array    $data
     *
     * @return string
     */
    public function renderTemplate($template, $data = [])
    {
        return $this->templating->render($template, $data);
    }

    /**
     * Automatically generates options for the given item.
     * The options source, defined by 'optionsSource' option, can be a model, a configuration parameter or a URL.
     *
     * TODO: Move/refactor this under OptionPhaseItem!
     *
     * @param PhaseItemInterface $phaseItem
     *
     * @return Dictionary
     */
    public function generateOptions(PhaseItemInterface $phaseItem)
    {
        $config = $phaseItem->getConfig();

        // Find out what column to use for the text of these options.
        // This is by default "materialRemark", but sometimes "itemName" is useful.
        $optionTextSource = $config['optionTextSource'];

        $optionValueSource = $config['optionValueSource'];

        // Find matching items.

        $filters = array();

        if (isset($config['filterOptions']) && is_array($config['filterOptions']) && count($config['filterOptions']) > 0) {
            foreach ($config['filterOptions'] as $filterKey => $filterOption) {
                if (isset($filterOption['condition']) && $this->evaluateExpression($filterOption['condition'], $config) !== true) {
                    continue;
                }

                $value = $this->evaluateExpression($filterOption['value']);

                $filters[$filterKey] = $value;
            }
        }

        $generator = 'generateOptionsFrom'.ucfirst($config['optionsSourceType']);

        /** @var Dictionary $options */
        $options = $this->$generator($config['optionsSource'], $optionValueSource, $optionTextSource, $filters);

        $this->handleOptionOverrides($options, $config);

        return $options;
    }

    /**
     * @return array
     */
    public function exportConfig()
    {
        $itemConfig = [];

        /** @var PhaseItemInterface $item */
        foreach ($this->getItems() as $item) {
            // Process alerts
            if ($item->doPrerequisitesPass()) {
                $itemConfig[$item->getId()] = $item->exportConfig();
            }
        }

        return [
            'items'    => $itemConfig,
        ] + $this->originalConfig;
    }

    /**
     * Returns the model instance for this phase. This is the primary model.
     *
     * @return ModelInterface
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param ModelInterface $model
     */
    public function setModel(ModelInterface $model)
    {
        $this->model = $model;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return array(
            'phase' => $this->originalConfig,
            'model' => $this->model,
        );
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {
        return serialize($this->getData());
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        if (isset($data['phase'])) {
            $this->originalConfig = $data['phase'];
        }

        if (isset($data['model'])) {
            $this->model = $data['model'];
        }
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return $this->getData();
    }

    /**
     * @param string $category
     */
    private function startTimer($category = 'form')
    {
        if ($this->stopwatch) {
            $this->stopwatch->start('klaro_quotation', $category);
        }
    }

    /**
     *
     */
    private function stopTimer()
    {
        if ($this->stopwatch) {
            $this->stopwatch->stop('klaro_quotation');
        }
    }

    /**
     * Creates all phase item objects according to phase definition.
     */
    private function createItems()
    {
        if (empty($this->originalConfig['items'])) {
            return;
        }

        foreach ($this->originalConfig['items'] as $itemName => $itemConfig) {
            if (is_array($itemConfig)) {
                $item = $this->createPhaseItem($itemName, $itemConfig);
                if ($item) {
                    $this->items[] = $item;
                }
            }
        }
    }

    /**
     * @param Dictionary $options
     * @param array      $config
     */
    private function handleOptionOverrides(Dictionary $options, array $config)
    {
        if (is_array($config['overrides'])) {
            foreach ($config['overrides'] as $override) {
                if (isset($override['options'])) {
                    $condition = isset($override['condition']) ?: null;

                    if (!$condition || $this->evaluateExpression($condition, $config) === true) {
                        $options->override($override['options']);
                    }
                }

                if (isset($override['optionTexts'])) {
                    $condition = isset($override['condition']) ?: null;

                    if (!$condition || $this->evaluateExpression($condition, $config) === true) {
                        $options->overrideOptionTexts($override['optionTexts']);
                    }
                }
            }
        }
    }

    /**
     * @param $expression
     * @param $optionValueSource
     * @param $optionTextSource
     * @param array             $filters
     *
     * @return Dictionary
     */
    private function generateOptionsFromExpression($expression, $optionValueSource, $optionTextSource, $filters = array())
    {
        $items = $this->evaluateExpression($expression);

        return new Dictionary($items);
    }

    /** Generates options from item model.
     * @param string $itemModel
     * @param string $optionValueSource
     * @param string $optionTextSource
     * @param array  $filters
     *
     * @return Dictionary
     *
     * @throws \Exception
     */
    private function generateOptionsFromModel($itemModel, $optionValueSource, $optionTextSource, $filters = array())
    {
        // If no item model found, try one that is defined for the whole phase.
        if (!$itemModel && isset($this->originalConfig['itemModel'])) {
            $itemModel = $this->originalConfig['itemModel'];
        }

        // Create the Query object.
        if (!$itemModel) {
            throw new \Exception("Item model must be defined!");
        }
        $query = $this->editorContext->getFinder($itemModel);
        $items = $query->getItems($filters);

        // Generate options.
        $options = new Dictionary();
        foreach ($items as $item) {
            if (!($item instanceof ModelInterface)) {
                $item = $query->translateModel($item);
            }

            if (empty($optionValueSource) || empty($optionTextSource)) {
                throw new \Exception('Option value and text source must be defined for fields with generated options.');
            }

            $subB = $item->get($optionValueSource);

            $details = array('text' => $item->get($optionTextSource));

            $options->add($subB, $details);
        }

        return $options;
    }

    /**
     * Generates options from a configuration parameter.
     * @param string $parameterName     Name of the parameter in configuration file.
     * @param string $optionValueSource TODO: NOT SUPPORTED YET - values are always keys from the parameter array.
     * @param string $optionTextSource  TODO: NOT SUPPORTED YET - texts are always values from the parameter array.
     * @param array  $filters           TODO: NOT SUPPORTED YET
     *
     * @return Dictionary
     *
     * @throws \Exception
     *
     * @deprecated
     */
    private function generateOptionsFromParameter($parameterName, $optionValueSource, $optionTextSource, $filters = array())
    {
        @trigger_error("Generation options from optionsSourceType = 'parameter' is deprecated, use optionsSourceType = 'expression' instead.", E_USER_DEPRECATED);

        throw new \Exception("Generation options from optionsSourceType = 'parameter' is deprecated, use optionsSourceType = 'expression' instead.");
    }
}
