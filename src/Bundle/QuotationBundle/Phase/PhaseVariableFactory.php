<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase;

use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\PhaseVariable\PhaseVariableInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PhaseVariableFactory
{
    /** @var ContainerInterface */
    protected $container;

    /** @var string[] */
    protected $globalVariables;

    /** @var PhaseVariableInterface[] */
    protected $loaders;

    /**
     * PhaseVariableFactory constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->globalVariables = [];
        $this->loaders = [];
    }

    /**
     * @return \string[]
     */
    public function getGlobalVariables()
    {
        return $this->globalVariables;
    }

    /**
     * @param \string[] $globalVariables
     */
    public function setGlobalVariables($globalVariables)
    {
        $this->globalVariables = $globalVariables;
    }

    /**
     * @param $name
     * @param $serviceId
     */
    public function addGlobalVariable($name, $serviceId)
    {
        if (isset($this->globalVariables[$name])) {
            throw new \UnexpectedValueException(
                sprintf(
                    'A global variable already exists with the name "%s" (existing service id is %s)',
                    $name,
                    $this->globalVariables[$name]
                )
            );
        }

        $this->globalVariables[$name] = $serviceId;
    }

    /**
     * @param QuotationRevisionInterface $revision
     * @param ProductLineDataNode        $formData
     *
     * @return PhaseVariableInterface
     */
    public function createVariables(QuotationRevisionInterface $revision, ProductLineDataNode $formData)
    {
        $cacheKey = $revision->getQuotation()->getId().'-'.$revision->getRevisionId();

        if (!isset($this->loaders[$cacheKey])) {
            $this->loaders[$cacheKey] = new PhaseVariableServiceLoader(
                $this->container,
                $this->globalVariables,
                $revision,
                $formData
            );
        }

        return $this->loaders[$cacheKey];
    }
}
