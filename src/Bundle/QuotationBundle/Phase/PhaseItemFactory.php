<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase;

use Klaro\QuotationBundle\Phase\PhaseItem\ArrayPhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\BasicPhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\ColorPhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\DatePhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\GridPhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\ImagesPhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\InlineFormPhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\NumberPhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\OptionPhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\RadioPhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\TextPhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\TitlePhaseItem;
use Klaro\QuotationBundle\Phase\PhaseItem\CalculatedPhaseItem;
use Klaro\Component\Logger\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manager class for defined phase items, responsible for creating new phase items.
 *
 * Class PhaseItemFactory
 * @package Klaro\QuotationBundle\Phase
 */
class PhaseItemFactory implements PhaseItemFactoryInterface
{
    /** @var ContainerInterface */
    protected $container;

    /** @var Logger */
    protected $logger;

    /** @var array */
    protected $phaseItemServices;

    /**
     * @param ContainerInterface $container
     * @param Logger             $logger
     */
    public function __construct(ContainerInterface $container, Logger $logger)
    {
        $this->container = $container;
        $this->logger    = $logger;

        $this->phaseItemServices = [];
    }

    /**
     * Add phase item service.
     *
     * @param $name
     * @param $serviceId
     *
     * @throws \Exception
     */
    public function addPhaseItem($name, $serviceId)
    {
        if (isset($this->phaseItemServices[$name])) {
            throw new \Exception("Phase item with type '{$name}' already exists!'");
        }

        $this->phaseItemServices[$name] = $serviceId;
    }

    /**
     * Creates a phase item.
     *
     * @param Phase                   $phase
     * @param $id
     * @param $config
     * @param bool|false              $readOnly
     * @param PhaseItemInterface|null $parent
     *
     * @return null|PhaseItemInterface
     *
     * @throws \Exception
     */
    public function createPhaseItem(Phase $phase, $id, $config, $readOnly = false, PhaseItemInterface $parent = null)
    {
        $type = isset($config['type']) ? $config['type'] : $this->getDefaultType($config);

        $item = null;

        if ($readOnly === true) {
            $config['disabled'] = $readOnly;
        }

        $this->logger->info("Requested phase item type '$type'", [
            'id'     => $id,
            'config' => $config,
        ]);

        if (!isset($this->phaseItemServices[$type]) && (!isset($config['dontBreak']))) {
            throw new \Exception("Phase item with type '{$type}' does not exist!'");
        }

        /** @var PhaseItemInterface $item */
        if (array_key_exists($type, $this->phaseItemServices)) {
            $item = $this->container->get($this->phaseItemServices[$type]);
        } else {
            $item = new BasicPhaseItem();
        }

        if ($item) {
            $item->initialize($phase, $id, $config, $parent);
        }

        return $item;
    }

    /**
     * Creates a basic phase item.
     *
     * @return BasicPhaseItem
     */
    public function createBasicPhaseItem()
    {
        return new BasicPhaseItem();
    }

    /**
     * Creates a grid phase item.
     *
     * @return GridPhaseItem
     */
    public function createGridPhaseItem()
    {
        return new GridPhaseItem($this->container);
    }

    /**
     * Creates a number phase item.
     *
     * @return NumberPhaseItem
     */
    public function createNumberPhaseItem()
    {
        return new NumberPhaseItem();
    }

    /**
     * Creates an option phase item.
     *
     * @return OptionPhaseItem
     */
    public function createOptionPhaseItem()
    {
        return new OptionPhaseItem();
    }

    /**
     * Creates an array phase item.
     *
     * @return ArrayPhaseItem
     */
    public function createArrayPhaseItem()
    {
        return new ArrayPhaseItem();
    }

    /**
     * Creates a radio phase item.
     *
     * @return RadioPhaseItem
     */
    public function createRadioPhaseItem()
    {
        return new RadioPhaseItem();
    }

    /**
     * Creates a title phase item.
     *
     * @return TitlePhaseItem
     */
    public function createTitlePhaseItem()
    {
        return new TitlePhaseItem();
    }

    /**
     * Creates a date phase item.
     *
     * @return DatePhaseItem
     */
    public function createDatePhaseItem()
    {
        return new DatePhaseItem();
    }

    /**
     * Creates a text phase item.
     *
     * @return TextPhaseItem
     */
    public function createTextPhaseItem()
    {
        return new TextPhaseItem();
    }

    /**
     * Creates an image phase item.
     *
     * @return ImagesPhaseItem
     */
    public function createImagesPhaseItem()
    {
        return new ImagesPhaseItem();
    }

    /**
     * Creates an inline phase item.
     * @return InlineFormPhaseItem
     */
    public function createInlineFormPhaseItem()
    {
        return new InlineFormPhaseItem();
    }

    /**
     * Creates a color selection phase item.
     *
     * @return ColorPhaseItem
     */
    public function createColorPhaseItem()
    {
        return new ColorPhaseItem();
    }

    /**
     * Creates a color selection phase item.
     *
     * @return CalculatedPhaseItem
     */
    public function createCalculatedPhaseItem()
    {
        return new CalculatedPhaseItem();
    }

    /**
     * Get default phase item type.
     *
     * @param $config
     *
     * @return string
     */
    private function getDefaultType($config)
    {
        $defaultType = 'text';

        if (isset($config['options'])) {
            $defaultType = 'radio';
        }

        return $defaultType;
    }
}
