<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase;

use Klaro\Component\Common\Model\FinderInterface;
use Klaro\Component\Common\Model\ModelInterface;

/**
 * Interface that enables the phase form service to interact with the editor and outside world,
 * eg. providing access to the current model when editing a form phase.
 *
 * Interface PhaseEditorContextInterface
 * @package Klaro\Component\Phase
 */
interface PhaseEditorContextInterface
{
    /**
     * Get model with given name and filters.
     *
     * @param $modelName
     *
     * @return ModelInterface
     */
    //public function getModel($modelName, $filters = [], $deferSave = false);

    /**
     * Save model to database.
     *
     * @param ModelInterface $model
     */
    public function persistModel(ModelInterface $model);

    /**
     * Get item finder with the given name.
     *
     * @param $itemModel
     *
     * @return FinderInterface
     */
    public function getFinder($itemModel);

    /**
     * Evaluate an expression string.
     *
     * @param $expression
     * @param array      $context
     *
     * @return mixed
     */
    public function evaluateExpression($expression, $context = []);

    /**
     * Send an event with the given name and data.
     *
     * @param $eventName
     * @param array     $data
     */
    public function notify($eventName, $data = []);

    /**
     * Tells if the editor should be read-only.
     *
     * @return boolean
     */
    public function isReadOnly();
}
