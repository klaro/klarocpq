<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase;

/**
 * Interface for phase form field classes. Each form item is instantiated into a for item class that corresponds
 * to this interface.
 *
 * Interface PhaseItemInterface
 * @package Klaro\Component\Phase
 */
interface PhaseItemInterface
{

    /**
     * Renders this item as a string.
     *
     * @return string
     */
    public function __toString();
    /**
     * @param Phase              $phase
     * @param $id
     * @param $config
     * @param PhaseItemInterface $parent
     *
     * @return mixed
     */
    public function initialize(Phase $phase, $id, $config, PhaseItemInterface $parent = null);

    /**
     * @param PhaseItemInterface $parent
     *
     * @return mixed
     */
    public function setParent(PhaseItemInterface $parent);

    /**
     * @return PhaseItemInterface
     */
    public function getParent();

    /**
     * Returns the unique identifier for this phase item.
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getFullIdPath();

    /**
     * Export item config so that it can be loaded from cache. Conditions should be evaluated and if item is not
     * visible, don't include its config. Exported config should also include evaluated and dynamic options in plain values.
     *
     * @return array
     */
    public function exportConfig();

    /**
     * Returns current configuration array.
     *
     * @return array
     */
    public function getConfig();

    /**
     * Get default values for fields which are not set in the phase definition config. Array value can be scalar
     * or a function which returns a value (function gets original $config as parameter), eg.
     *
     * return [
     *    'default' => 1,
     *    'template' => function($config) {
     *           return $config['type'];
     *       }
     * ]
     *
     * @return array
     */
    public function getDefaults();

    /**
     * Returns the default value for this phase item if no value is yet set.
     *
     * @return mixed
     */
    public function getDefaultValue();

    /**
     * Returns current value of the item.
     *
     * @return mixed
     */
    public function getValue();

    /**
     * Sets the value of the item to $value.
     * @param $value
     */
    public function setValue($value);

    /**
     * Returns the text label of the item.
     *
     * @return string
     */
    public function getText();

    /**
     * Returns the fieldname of this item.
     * @return string
     */
    public function getFieldName();

    /**
     * If this item is a single column item (such as "title" or "button"), ie. it has no label then this should return true.
     *
     * @return boolean
     */
    public function isSingleColumn();

    /**
     * Returns a boolean value indicating if the phase item is empty (nothing to select).
     *
     * @return bool
     */
    public function isEmpty();

    /**
     * Returns true if this item has subitems.
     * @return boolean
     */
    public function hasItems();

    /**
     * Returns list of subitems.
     *
     * @return PhaseItemInterface[]
     */
    public function getItems();

    /**
     * Returns a list of variables that are passed on to the phase item template. By default,
     * this includes the phase and the item itself.
     *
     * @return array
     */
    public function getData();

    /**
     * Returns true if the given configuration field exists.
     *
     * @param string $field Configuration field name
     *
     * @return boolean
     */
    public function has($field);

    /**
     * Returns the given configuration variable if it exists or a default value (null if none given).
     *
     * @param string $field   Configuration field name
     * @param mixed  $default Default value if field is not found.
     *
     * @return mixed
     */
    public function get($field, $default = null);

    /**
     * Sets configuration variable name.
     *
     * @param string $field Configuration field name
     * @param mixed  $value Configuration variable value
     */
    public function set($field, $value);

    /**
     * Called when rendering the item to a string.
     */
    public function render();

    /**
     * Phase item preprocessing, called when constructing the phase item. Sets default values in case some did not exist.
     * Normalizes options to be associative and makes sure prerequisites are applied.
     */
    public function preProcess();

    /**
     * Handle postprocessing the phase item data. Post processing occurs just before rendering the phase item.
     */
    public function postProcess();

    /**
     * Returns a boolean value indicating if this phase item should be shown (its prerequisites pass).
     *
     * @return boolean
     */
    public function doPrerequisitesPass();

    /**
     * Returns a unique string which is used handle updating of for item goups.
     *
     * @return string
     */
    public function getUpdateToken();

    /**
     * Return a checksum of the item's data which is used to check if the phase item was updated or not.
     *
     * @return string
     */
    public function getChecksum();

    /**
     * Returns an array of error/information strings that are set for this item.
     *
     * @return array
     */
    public function getMessages();

    /**
     * Attach an information message to this item.
     *
     * @param string $type    Message type (eg. 'error', 'warning' or 'info')
     * @param string $message Message text
     * @param array  $context Additional context variables
     */
    public function addMessage($type, $message, $context = []);

    /**
     * Process user input parameters. Called when editor is requested with POST method.
     *
     * @param $params
     */
    public function processUserInput($params);

    /**
     * Handle loading of item data from current model.
     */
    public function loadItemData();
}
