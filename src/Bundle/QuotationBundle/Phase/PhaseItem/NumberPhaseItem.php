<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase\PhaseItem;

class NumberPhaseItem extends BasicPhaseItem
{
    /**
     * {@inheritDoc}
     */
    public function getDefaults()
    {
        return [
            'step' => null,
            'min'  => null,
            'max'  => null,
        ] + parent::getDefaults();
    }

    /**
     * {@inheritDoc}
     */
    public function postProcess()
    {
        parent::postProcess();

        if ($this->has('min')) {
            $min = $this->get('min');

            if (is_string($min)) {
                $min = $this->phase->evaluateExpression($min, $this->config);
            }

            $this->set('min', $min);
        }

        if ($this->has('max')) {
            $max = $this->get('max');

            if (is_string($max)) {
                $max = $this->phase->evaluateExpression($max, $this->config);
            }

            $this->set('max', $max);
        }
    }
}
