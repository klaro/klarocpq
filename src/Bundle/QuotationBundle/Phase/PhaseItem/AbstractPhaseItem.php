<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase\PhaseItem;

use Klaro\QuotationBundle\Phase\Phase;
use Klaro\QuotationBundle\Phase\PhaseItemInterface;

abstract class AbstractPhaseItem implements PhaseItemInterface
{
    /** @var Phase */
    protected $phase;

    /** @var PhaseItemInterface */
    protected $parent;

    /** @var string */
    protected $id;

    /** @var array */
    protected $config;

    /** @var bool */
    protected $prerequisitesPassResult;

    /**
     * {@inheritDoc}
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @param Phase              $phase
     * @param string             $id
     * @param array              $config
     * @param PhaseItemInterface $parent
     */
    public function initialize(Phase $phase, $id, $config, PhaseItemInterface $parent = null)
    {
        $this->phase  = $phase;
        $this->parent = $parent;
        $this->id     = $id;
        $this->config = $config;

        $this->preProcess();
    }

    /**
     * {@inheritDoc}
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * {@inheritDoc}
     */
    public function setParent(PhaseItemInterface $parent)
    {
        $this->parent = $parent;
    }

    /**
     * {@inheritDoc}
     */
    public function getFullIdPath()
    {
        return ($this->parent instanceof PhaseItemInterface) ?
            $this->parent->getFullIdPath().'/'.$this->id :
            $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function exportConfig()
    {
        $config = $this->config;

        unset($config['condition']);
        unset($config['overrides']);
        unset($config['prerequisite']);
        unset($config['overrides']);
        unset($config['disabled']);

        return $config;
    }

    /**
     * {@inheritDoc}
     */
    public function preProcess()
    {
        $this->setDefaultValues();
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaults()
    {
        return [
            'default' => null,
            'defaults' => array(),
            'value' => function ($config) {
                    $default = isset($config['default']) ? $config['default'] : null;

                    return isset($config['value']) ? $config['value'] : $default;
            },
            'hasChanged' => false,
            'disabled' => false,
            'fieldName' => '',
            'hideLabel' => false,
            'indent' => function ($config) {
                if ($config['hideLabel']) {
                    return true;
                }

                    return false;
            },
            'optional' => false,
            'text' => $this->id,
            'infoText' => function ($config) {
                    return (isset($config['infoText']) ? $config['infoText'] : '');
            },
            'type' => function ($config) {
                    return (isset($config['type']) ? $config['type'] : 'text');
            },
            'overrides' => null,
            'template' => function ($config) {
                    return $config['type'];
            },
            'unit' => '',
            'placeholder' => null,
            'width' => null,
            'colWidth' => null,
            'offsetWidth' => null,
            'vars' => null,
            'jsHandlers' => null,
            'cssClasses' => null,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * {@inheritDoc}
     */
    public function getData()
    {
        return [
            'id' => $this->id,
            'config' => $this->config,
            'item' => $this,
            'phase' => $this->phase,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getText()
    {
        return $this->get('text');
    }

    /**
     * {@inheritDoc}
     */
    public function getFieldName()
    {
        return $this->get('fieldName');
    }

    /**
     * {@inheritDoc}
     */
    public function isSingleColumn()
    {
        return ($this->config['hideLabel'] === true);
    }

    /**
     * {@inheritDoc}
     */
    public function isEmpty()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaultValue()
    {
        $default = null;

        // Check for overrides
        $defaultOverridden = $this->processOverride('default', function ($original, $overridden) use (&$default) {
            $default = $overridden;

            return true;
        });

        if (!$defaultOverridden) {
            $default = $this->config['default'];
        }

        return $default;
    }

    /**
     * {@inheritDoc}
     */
    public function getValue()
    {
        return $this->get('value');
    }

    /**
     * {@inheritDoc}
     */
    public function setValue($value, $sanitize = true)
    {
        $fieldName = $this->get('fieldName');

        if (!empty($fieldName)) {
            $this->config['value'] = $value;

            $this->processOverride('value', function ($original, $overridden) {
                $this->config['value'] = $overridden;
            });

            $this->phase->setPhaseFieldValue($this->config['fieldName'], $this->config['value'], $sanitize);

            // If the originalValue has not yet been set, do so now.
            if (!array_key_exists('originalValue', $this->config)) {
                $this->config['originalValue'] = $value;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function has($field)
    {
        return isset($this->config[$field]);
    }

    /**
     * {@inheritDoc}
     */
    public function get($field, $default = null)
    {
        return isset($this->config[$field]) ? $this->config[$field] : $default;
    }

    /**
     * {@inheritDoc}
     */
    public function set($field, $value)
    {
        $this->config[$field] = $value;
    }

    /**
     * {@inheritDoc}
     */
    public function loadItemData()
    {
        $value = null;
        $config = $this->getConfig();

        $fieldName = $config['fieldName'];

        if (!empty($fieldName)) {
            // Get old value.
            if ($fieldName) {
                $value = $this->phase->getPhaseFieldValue($fieldName);
            }

            $default = $this->getDefaultValue();

            // Set default value when the model is new or when value is null.
            if (($value === null || (is_array($value) && empty($value))) && $default !== null) {
                $value = $default;
            }

            $this->setValue($value, false);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdateToken()
    {
        return $this->get('type').'-'.$this->get('fieldName');
    }

    /**
     * {@inheritDoc}
     */
    public function doPrerequisitesPass(array $extra = [])
    {
        if (isset($this->config['condition'])) {
            // Process main prerequisites.
            if ($this->prerequisitesPassResult === null) {
                $this->prerequisitesPassResult = $this->phase->evaluateExpression(
                    $this->config['condition'],
                    array_merge($this->config, $extra)
                );
            }

            if ($this->prerequisitesPassResult === false) {
                return false;
            }
        }
        // TODO: Remove prerequisites
        elseif (isset($this->config['prerequisite'])) {
            $this->phase->evalPrerequisite(null);
        }

        return true;
    }

    /**
     * Helper to process overrides defined in the configuration. There may be multiple overrides for the same field.
     * For every override whose condition is truthy, the callback is called. If no condition is defined,
     * then the callback is always called. Callback function receives the old value and overridden value as parameters.
     * The callback should return true if the overridden value was used and false otherwise.
     *
     * @param string   $configField Name of the config field that is overridden.
     * @param callable $callback    Callback function, receives the old value and the overriden one as parameters.
     *
     * @return bool
     */
    public function processOverride($configField, callable $callback)
    {
        $overrideFound = false;

        if (isset($this->config['overrides'])) {
            foreach ($this->config['overrides'] as $override) {
                if (isset($override[$configField])) {
                    $condition = isset($override['condition']) ? $override['condition'] : null;

                    if (!$condition || $this->phase->evaluateExpression($condition, $this->config) === true) {
                        $oldValue = $this->config[$configField];
                        $newValue = $override[$configField];

                        // If evaluate flag is true, treat the message as an expression.
                        if (isset($override['evaluate']) && $override['evaluate'] === true) {
                            $newValue = $this->phase->evaluateExpression($newValue, $this->config);
                        }

                        $overrideFound = (call_user_func($callback, $oldValue, $newValue) === true) || $overrideFound;
                    }
                }
            }
        }

        return $overrideFound;
    }

    /**
     * {@inheritDoc}
     */
    public function processUserInput($params)
    {
        $fieldName = $this->get('fieldName');

        if (empty($fieldName) || !isset($params[$fieldName])) {
            return;
        }

        $newValue = $params[$fieldName];

        $this->setValue($newValue);
    }

    /**
     * {@inheritDoc}
     */
    private function setDefaultValues()
    {
        $config = &$this->config;

        // Default field values. If the specified key is not specified in the phase, use these defaults.
        // Go through the previously declared fields and set defaults.
        foreach ($this->getDefaults() as $field => $value) {
            if (!isset($config[$field])) {
                // If it's a function, call it to receive the value.
                // (is_object() is needed to check that the function is a closure.)
                if (is_callable($value) && is_object($value)) {
                    $value = $value($config);
                }

                $config[$field] = $value;
            }
        }
    }
}
