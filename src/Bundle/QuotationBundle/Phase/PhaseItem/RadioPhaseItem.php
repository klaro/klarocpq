<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase\PhaseItem;

class RadioPhaseItem extends OptionPhaseItem
{
    /**
     * {@inheritDoc}
     */
    public function getDefaults()
    {
        return [
            'template' => function ($config) {
                $template = (!isset($config['type']) || $config['type'] == "radio") ? 'radio-vertical' : $config['type'];

                if (isset($config['template'])) {
                    $template = $config['template'];
                }

                return $template;
            },
        ] + parent::getDefaults();
    }
}
