<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase\PhaseItem;

use Klaro\Component\Logger\Logger;

class BasicPhaseItem extends AbstractPhaseItem
{
    /**
     * Error logger for messages
     */
    protected $errorLogger;

    /**
     *
     */
    public function __construct()
    {
        // TODO: Get this from service?
        $this->errorLogger = new Logger();
    }

    /**
     * {@inheritDoc}
     */
    public function hasItems()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getItems()
    {
        return [];
    }

    /**
     * {@inheritDoc}
     */
    public function getData()
    {
        return parent::getData() + [
            'messages' => $this->getMessages(),
            'visible'  => $this->doPrerequisitesPass(),
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        try {
            $template = $this->config['template'];

            // Make it possible to define custom templates.
            if (strpos($template, ':') === false) {
                $template = "KlaroQuotationBundle:Phase:$template";
            }

            $template = $this->phase->renderTemplate($template.'.html.twig', $this->getData());

            return $template;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function preProcess()
    {
        parent::preProcess();

        // Some specific defaults.
        $default = isset($this->config['default']) ? $this->config['default'] : null;

        $this->config['value'] = isset($this->config['value']) ? $this->config['value'] : $default;

        // If width is given, assume unit is pixels if none given
        if ($this->has('width')) {
            $width = $this->get('width');

            if (is_numeric($width)) {
                $this->set('width', $width.'px');
            }
        }

        if ($this->has('colWidth')) {
            $this->set('colWidth', $this->processColumnWidths($this->get('colWidth'), 1, 12));
        }

        if ($this->has('offsetWidth')) {
            $this->set('offsetWidth', $this->processColumnWidths($this->get('offsetWidth'), 0, 12));
        }

        if ($this->config['placeholder'] === true) {
            $this->config['placeholder'] = $this->config['text'];
        }
    }

    /**
     * {@inheritDoc}
     */
    public function postProcess()
    {
        // Check for overrides.
        $overrideFields = ['text', 'unit', 'url'];

        foreach ($overrideFields as $configField) {
            $this->processOverride($configField, function ($original, $overridden) use ($configField) {
                $this->set($configField, $overridden);

                return true;
            });
        }

        // Try to determine if the value was changed.
        if (array_key_exists('originalValue', $this->config)) {
            $value = $this->config['value'];

            if (is_array($value)) {
                $changed = (bool) ($this->config['originalValue'] !== $value);
            } elseif (is_float($value)) {
                $changed = (bool) ((string) $this->config['originalValue'] !== (string) $value);
            } else {
                $changed = (bool) ($this->config['originalValue'] != $value);
            }

            $this->config['hasChanged'] = $changed;
        }

        // If 'disabled' option is a string, treat it as an expression.
        if (is_string($this->config['disabled'])) {
            $this->config['disabled'] = $this->phase->evaluateExpression($this->config['disabled'], $this->config);
        }

        // Extra variables to be used in template.
        $evaluatedVars = $this->get('evaluatedVars', []);

        if (is_array($this->config['vars'])) {
            foreach ($this->config['vars'] as $var => $value) {
                if (!isset($evaluatedVars[$var])) {
                    if (is_array($value)) {
                        $evaluate = isset($value['evaluate']) ? $value['evaluate'] === true : false;

                        $value = isset($value['value']) ? $value['value'] : $value;

                        if (is_string($value) && $evaluate === true) {
                            $value = $this->phase->evaluateExpression($value, $this->config);
                        }
                    }

                    $evaluatedVars[$var] = $value;
                }

                $this->config['vars'][$var] = $evaluatedVars[$var];
            }
        }

        $this->set('evaluatedVars', $evaluatedVars);

        if ($this->has('jsHandlers')) {
            $this->set('jsHandlers', $this->get('jsHandlers', []));
        }

        if (is_array($this->config['text'])) {
            $text = $this->config['text'];
            $evaluate = isset($text['evaluate']) ? $text['evaluate'] === true : false;
            $value = isset($text['value']) ? $text['value'] : $text;

            if (is_string($value) && $evaluate === true) {
                $value = $this->phase->evaluateExpression($value, $this->config);
            }

            $this->config['text'] = $value;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getChecksum()
    {
        $data = [
            'config' => $this->config,
            'messages' => $this->getMessages(),
            'visible' => $this->doPrerequisitesPass(),
        ];

        // Remove values to prevent for field update if value changes.
        if (isset($data['config']['hasChanged'])) {
            unset($data['config']['hasChanged']);
        }

        if (isset($data['config']['originalValue'])) {
            unset($data['config']['originalValue']);
        }

        return crc32(serialize($data));
    }

    /**
     * {@inheritDoc}
     */
    public function getMessages()
    {
        return $this->errorLogger->getMessages();
    }

    /**
     * {@inheritDoc}
     */
    public function addMessage($type, $message, $context = array())
    {
        if ($this->doPrerequisitesPass()) {
            if (method_exists($this->errorLogger, $type)) {
                $this->errorLogger->{$type}($message, $context);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    private function processColumnWidths($colWidthValue, $min, $max)
    {
        $colWidths = [];

        if (is_string($colWidthValue)) {
            $colWidthValue = $this->phase->evaluateExpression($colWidthValue, $this->config);
        }

        if (is_array($colWidthValue)) {
            foreach ($colWidthValue as $size => $width) {
                $width = min($max, $width);
                $width = max($min, $width);

                $colWidths[$size] = $width;
            }
        } else {
            $colWidthValue = min($max, $colWidthValue);
            $colWidthValue = max($min, $colWidthValue);

            $colWidths = [
                'xs' => $colWidthValue,
                'sm' => $colWidthValue,
                'md' => $colWidthValue,
                'lg' => $colWidthValue,
            ];
        }

        return $colWidths;
    }
}
