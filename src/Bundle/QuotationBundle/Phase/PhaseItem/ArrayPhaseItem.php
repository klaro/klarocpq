<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase\PhaseItem;

use Klaro\QuotationBundle\Phase\PhaseItemInterface;

class ArrayPhaseItem extends BasicPhaseItem
{
    /** @var PhaseItemInterface[] */
    protected $items;

    protected $values;

    /**
     * {@inheritDoc}
     */
    public function preProcess()
    {
        parent::preProcess();

        if (is_array($this->config['items']) && count($this->config['items']) > 0) {
            foreach ($this->config['items'] as $itemName => $itemConfig) {
                $defaults = isset($this->config['defaults']) ? $this->config['defaults'] : array();
                $itemConfig = isset($itemConfig) ? $itemConfig : array();

                $config = array_merge($defaults, $itemConfig);

                $item = $this->phase->createPhaseItem($itemName, $config, $this);

                if ($item) {
                    $item->set('fieldName', $item->getId());
                    $this->items[] = $item;
                }
            }
        }

        // Disable adding and removing if the array is disabled.
        $this->set('allowAdd', $this->get('allowAdd') === true && $this->get('disabled') !== true);
        $this->set('allowRemove', $this->get('allowRemove') === true && $this->get('disabled') !== true);
    }

    /**
     * {@inheritDoc}
     */
    public function postProcess()
    {
        parent::postProcess();

        /** @var PhaseItemInterface $item */
        foreach ($this->items as $item) {
            $item->postProcess();
        }

        if (!is_array($this->config['value'])) {
            $this->config['value'] = [];
        }

        $rowsArray = [];

        $rows = isset($this->config['rows']) ? $this->config['rows'] : null;

        // Figure out how many rows we should have from the "rows" config variable:
        // If array, use the keys and values for id
        if (is_array($rows)) {
            $rowsArray = $rows;
            $rows = count($rowsArray);
        }
        // If string this value should be evaluated, assume an array is returned.
        // TODO: accept number?
        elseif (is_string($rows)) {
            $rowsArray = $this->phase->evaluateExpression($rows, $this->config);
            $rows = count($rowsArray);
        }
        // If number, use that as row count
        elseif (is_numeric($rows)) {
            $rows = (int) $rows;
            $rowsArray = range(0, $rows);
        }
        // Otherwise set as many rows as the value (if value is not set, default value is stored here).
        else {
            $rowsArray = $this->config['value'];
            $rows = count($rowsArray);
        }

        $this->config['rows'] = $rows;

        $values = [];

        $this->config['items'] = [];
        $this->config['defaultValues'] = [];

        foreach ($this->items as $item) {
            if ($item->doPrerequisitesPass()) {
                $this->config['items'][$item->getId()] = $item->getConfig();
                $this->config['defaultValues'][$item->getId()] = $item->getDefaultValue();
            }
        }

        for ($i = 0; $i < $rows; ++$i) {
            list($key, $value) = each($rowsArray);

            if (!is_string($value)) {
                $value = $key;
            }

            $row = [];

            foreach ($this->items as $item) {
                if ($item->doPrerequisitesPass()) {
                    $id = $item->getId();

                    if (isset($this->config['value'][$key][$id])) {
                        $row[$id] = $this->config['value'][$key][$id];
                    } elseif (isset($this->config['default'][$key][$id])) {
                        $row[$id] = $this->config['default'][$key][$id];
                    } else {
                        $row[$id] = $item->getDefaultValue();
                    }
                }
            }

            $values[] = [
                'id' => $key,
                'title' => $value,
                'values' => $row,
            ];
        }

        $this->config['value'] = $values;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return [
            'items'  => $this->items,
            'values' => $this->values,
        ] + parent::getData();
    }

    /**
     * @return array
     */
    public function getDefaults()
    {
        return [
            'showHeaders' => false,
            'showTitle' => false,
            'rows' => null,
            'allowAdd' => false,
            'allowRemove' => false,
            'saveAfterLoad' => false,
        ] + parent::getDefaults();
    }

    /**
     * @return bool
     */
    public function hasItems()
    {
        return count($this->items) > 0;
    }

    /**
     * @return PhaseItemInterface[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * {@inheritDoc}
     */
    public function getChecksum()
    {
        $checksum = parent::getChecksum();

        if ($this->hasItems()) {
            foreach ($this->getItems() as $item) {
                $checksum .= $item->getChecksum();
            }

            $checksum = crc32($checksum);
        }

        return $checksum;
    }
}
