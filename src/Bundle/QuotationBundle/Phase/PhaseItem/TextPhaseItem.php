<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase\PhaseItem;

class TextPhaseItem extends BasicPhaseItem
{
    /**
     * {@inheritDoc}
     */
    public function preProcess()
    {
        parent::preProcess();

        // Parse "text" size configuration.
        if (isset($this->config['size'])) {
            $size = $this->config['size'];

            // Define new size properties -- this makes it easier to make the template/view.
            if (strpos($size, 'x') !== false) { // <textarea></textarea>
                list($columns, $rows) = explode('x', $size);
                $this->config['sizeColumns'] = $columns;
                $this->config['sizeRows'] = $rows;
            } else { // <input />
                $this->config['sizeWidth'] = $size.'px';
                $this->config['sizeColumns'] = $size;
                $this->config['sizeRows'] = 1;
            }
        } elseif ($this->config['type'] === 'text') {
            $this->config['sizeRows'] = 1;
        }

        // Override width from size config if not defined.
        if (!$this->has('width') && $this->has('sizeWidth')) {
            $this->set('width', $this->get('sizeWidth'));
        }
    }
}
