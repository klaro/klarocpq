<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase\PhaseItem;

class ImagesPhaseItem extends OptionPhaseItem
{
    public function getDefaults()
    {
        return [
            'images' => false,
            'multiple' => false,
        ] + parent::getDefaults();
    }

    /**
     * @param $key
     * @param null|array $optionConfig
     *
     * @return array
     */
    protected function getOptionValues($key, $optionConfig = null)
    {
        $data = parent::getOptionValues($key, $optionConfig);

        $data['image'] = isset($optionConfig['image']) ? $optionConfig['image'] : null;

        return $data;
    }
}
