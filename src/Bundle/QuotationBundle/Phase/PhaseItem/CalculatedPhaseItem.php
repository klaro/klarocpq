<?php

namespace Klaro\QuotationBundle\Phase\PhaseItem;

/**
 * Class CalculatedPhaseItem
 *
 * @package Klaro\QuotationBundle\Phase\PhaseItem
 */
class CalculatedPhaseItem extends BasicPhaseItem {
    /**
     * {@inheritDoc}
     */
    public function getDefaults() {
        return [
                'formula' => null,
                'inputs' => [],
                'resolvedInputs' => []
            ] + parent::getDefaults();
    }

    /**
     * {@inheritDoc}
     */
    public function preProcess()
    {
        parent::preProcess();
    }

    /**
     * {@inheritDoc}
     */
    public function postProcess()
    {
        parent::postProcess();

        $this->calculate();
    }

    /**
     * Evaliuates field value using pre set 'resolvedInputs' and defined 'formula'
     */
    public function calculate()
    {
        $value = null;

        // todo: compare $this->get('resolvedInputs') and $this->inputs and add alerts if no enough inputs are set
        // todo:  think what to do with zero divisions? Now they are catched and send to log

        if ($this->has('formula')) {
            $formula = $this->get('formula');

            if (is_string($formula)) {
                foreach ($this->get('resolvedInputs') as $name => $val) {
                    if (is_string($val)) {
                        $this->set('value', 0);
                        return;
                    }
                }
                $value = $this->phase->evaluateExpression($formula, array_merge($this->config, $this->get('resolvedInputs')));
                if (is_nan($value) || is_infinite($value)) {
                    $value = 0;
                }
            }
        }

        $this->set('value', $value);
    }

    public function addResolvedInputs($value)
    {
        $this->set('resolvedInputs', $value);
    }
}
