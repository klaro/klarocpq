<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase\PhaseItem;

use Klaro\Component\Common\Model\FinderInterface;
use Klaro\Component\Common\Model\ModelInterface;
use Klaro\Component\FormData\FormPhaseData;
use Klaro\QuotationBundle\Phase\Phase;
use Klaro\QuotationBundle\Phase\PhaseEditorContextInterface;
use Klaro\QuotationBundle\Phase\PhaseItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GridPhaseItem extends AbstractPhaseItem implements PhaseEditorContextInterface
{
    /** @var ContainerInterface */
    protected $container;

    /** @var Phase[] */
    protected $phases;

    /** @var bool */
    protected $userInputProcessed;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function initialize(Phase $phase, $id, $config, PhaseItemInterface $parent = null)
    {
        parent::initialize($phase, $id, $config, $parent);

        $this->userInputProcessed = false;
    }

    /**
     * {@inheritDoc}
     */
    public function getDefaults()
    {
        return [
            'showHeaders' => false,
            'panel'       => false,
            'rows'        => [],
            'model'       => null,
            'filters'     => [],
            'dataKey'     => null,
            'emptyText'   => 'No options found.',
        ] + parent::getDefaults();
    }

    /**
     * {@inheritDoc}
     */
    public function hasItems()
    {
        // TODO: Implement hasItems() method.
    }

    /**
     * {@inheritDoc}
     */
    public function getItems()
    {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function getData()
    {
        return [
            'phases' => $this->phases,
            'dataKey' => $this->get('dataKey'),
        ] + parent::getData();
    }

    /**
     * {@inheritDoc}
     */
    public function render()
    {
        try {
            $template = $this->config['template'];

            // Make it possible to define custom templates.
            if (strpos($template, ':') === false) {
                $template = "KlaroQuotationBundle:Phase:$template";
            }

            $template = $this->phase->renderTemplate($template.'.html.twig', $this->getData());

            return $template;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function preProcess()
    {
        parent::preProcess();

        $showHeaders = (bool) ($this->get('showHeaders') === true);

        if (!$this->has('showRowHeaders')) {
            $this->set('showRowHeaders', $showHeaders);
        }

        if (!$this->has('showColumnHeaders')) {
            $this->set('showColumnHeaders', $showHeaders);
        }

        $panelHeader = (bool) ($this->get('panel') === true);

        if (!$this->has('panelHeader')) {
            $this->set('panelHeader', $panelHeader);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function postProcess()
    {
        // Save subphase configs for caching.
        $exportedConfigs = [];

        if (count($this->phases) > 0) {
            foreach ($this->phases as $index => $phase) {
                $phase->postProcess(false);

                $exportedConfigs[$index] = $phase->exportConfig();
            }
        }

        $this->set('phases', $exportedConfigs);

        $columns = [];

        if (!$this->has('columns')) {
            if (count($this->phases) > 0) {
                /** @var PhaseItemInterface $phaseItem */
                foreach (reset($this->phases)->getItems() as $phaseItem) {
                    if ($phaseItem->doPrerequisitesPass()) {
                        $columns[] = $phaseItem->getText();
                    }
                }
            }
        } else {
            $columnDefinition = $this->get('columns');

            foreach ($columnDefinition as $column) {
                $columnText = null;

                if (is_string($column)) {
                    $columns[] = $column;
                } elseif (is_array($column)) {
                    if (!isset($column['condition']) || $this->phase->evaluateExpression($column['condition'], $this->getConfig())) {
                        $columns[] = $column['text'];
                    }
                }
            }
        }

        $this->set('columns', $columns);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdateToken()
    {
        return $this->get('type');
    }

    /**
     * {@inheritDoc}
     */
    public function getChecksum()
    {
        $data = [];

        if (count($this->phases) > 0) {
            foreach ($this->phases as $phase) {
                $checksums = [];

                // Get list of visible fields and use their names to calculate checksum (getting value or checksum
                // from each item would cause the whole grid to reload if one value changes).
                foreach ($phase->getItems() as $item) {
                    if ($item->doPrerequisitesPass()) {
                        $checksums[] = $item->getId();
                    }
                }

                $data[] = $checksums;
            }
        }

        return crc32(serialize($data));
    }

    /**
     * {@inheritDoc}
     */
    public function getMessages()
    {
        // TODO: Implement getMessages() method.
    }

    /**
     * {@inheritDoc}
     */
    public function addMessage($type, $message, $context = [])
    {
        // TODO: Implement addMessage() method.
    }

    /**
     * {@inheritDoc}
     */
    public function processUserInput($params)
    {
        $this->userInputProcessed = true;

        $fieldName = $this->get('fieldName');

        if (empty($fieldName) || !isset($params[$fieldName])) {
            return;
        }

        $params = $params[$fieldName];

        $dataKey = $this->get('dataKey');

        if (!empty($dataKey)) {
            $params = $params[$dataKey];
        }

        $combinedData = $this->phase->getPhaseFieldValue($fieldName);

        if (count($this->phases) > 0) {
            foreach ($this->phases as $phase) {
                $phase->processUserInput($params);

                $phaseId = $phase->getPhaseId();
                $phaseData = $phase->getModel()->toArray();

                if (!empty($dataKey)) {
                    $combinedData[$dataKey][$phaseId] = $phaseData;
                } else {
                    $combinedData[$phaseId] = $phaseData;
                }
            }
        }

        $this->phase->setPhaseFieldValue($fieldName, $combinedData);
    }

    /**
     * {@inheritDoc}
     */
    public function loadItemData()
    {
        parent::loadItemData();

        $this->processSubPhases();
    }

    /**
     * {@inheritdoc}
     */
    public function persistModel(ModelInterface $model)
    {
        $this->phase->getEditorContext()->persistModel($model);
    }

    /**
     * {@inheritdoc}
     */
    public function getFinder($itemModel)
    {
        return $this->phase->getEditorContext()->getFinder($itemModel);
    }

    /**
     * {@inheritdoc}
     */
    public function evaluateExpression($expression, $context = [])
    {
        $extra = [
            'parent' => (object) [
                'model' => $this->phase->getModel(),
                'dataKey' => $this->get('dataKey'),
                'vars' => (object) $this->get('vars'),
            ],
        ];

        return $this->phase->getEditorContext()->evaluateExpression($expression, $context + $extra);
    }

    /**
     * {@inheritdoc}
     */
    public function notify($eventName, $data = [])
    {
        $this->phase->getEditorContext()->notify($eventName, $data);
    }

    /**
     * {@inheritdoc}
     */
    public function isReadOnly()
    {
        return $this->phase->getEditorContext()->isReadOnly();
    }

    /**
     * Creates sub phase forms based on the given rows.
     */
    private function processSubPhases()
    {
        $subPhases = $this->get('rows');

        if (is_string($subPhases)) {
            $subPhases = $this->phase->evaluateExpression($subPhases);
        }

        if (is_numeric($subPhases)) {
            $tmpArray = [];

            for ($i = 0; $i < $subPhases; ++$i) {
                $tmpArray[] = $i;
            }

            $subPhases = $tmpArray;
        }

        if (!is_array($subPhases)) {
            $subPhases = [];
        }

        $this->set('rows', $subPhases);

        $this->phases = [];

        // Figure out data key
        $fieldName = $this->get('fieldName');

        if (empty($fieldName)) {
            throw new \Exception('Fieldname cannot be empty!');
        }

        $gridItemData = $this->phase->getPhaseFieldValue($fieldName);

        // Filters are needed to get a specific model for each subphase row in the grid.
        // By default, filters are evaluated but when loading cached configs there might be cases where
        // where evaluation is always needed, such as access to the request object. These are the filters
        // defined in the yml file:
        $dataKey = $this->get('dataKey');

        // Evaluate or fetch cached filters.
        if (is_array($dataKey)) {
            $evaluate = isset($dataKey['evaluate']) ? $dataKey['evaluate'] === true : false;

            $dataKey = isset($dataKey['value']) ? $dataKey['value'] : null;

            if (is_string($dataKey) && $evaluate === true) {
                $dataKey = $this->phase->evaluateExpression($dataKey);
            }
        }

        // Store for caching.
        $this->set('dataKey', $dataKey);

        if (!empty($dataKey)) {
            $rowData = isset($gridItemData[$dataKey]) ? $gridItemData[$dataKey] : [];
        } else {
            $rowData = $gridItemData;
        }

        // If config has been cached, this will contain the phase configs for each sub phase.
        $importedPhases = $this->get('phases', []);

        foreach ($subPhases as $index => $title) {
            $data = isset($rowData[$index]) ? $rowData[$index] : [];

            /** @var Phase $phase */
            $phase = $this->container->get('klaro_quotation.phase_form');

            if (!isset($importedPhases[$index])) {
                $phaseDefinition = [
                    'items'      => $this->get('items'),
                    'model'      => null,
                    'definition' => null,
                ];

                if (empty($phaseDefinition['template'])) {
                    $phaseDefinition['template'] = 'KlaroQuotationBundle:Phase:grid_row';
                }
            } else {
                $phaseDefinition = $importedPhases[$index];
            }

            $phase->setEditorContext($this);
            $phase->setPhaseConfigData($phaseDefinition, $index);
            $phase->setParentPhase($this->phase);

            $model = new FormPhaseData($index, $data);
            $phase->setModel($model);

            $phase->loadCurrentDataToPhaseItems();

            $this->phases[$index] = $phase;

            // Set data
            if (!empty($dataKey)) {
                $gridItemData[$dataKey][$index] = $model->toArray();
            } else {
                $gridItemData[$index] = $model->toArray();
            }
        }

        $this->phase->setPhaseFieldValue($fieldName, $gridItemData);
    }
}
