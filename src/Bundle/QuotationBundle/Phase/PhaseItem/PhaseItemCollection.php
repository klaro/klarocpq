<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase\PhaseItem;

use Klaro\QuotationBundle\Phase\PhaseItemInterface;

class PhaseItemCollection extends BasicPhaseItem
{
    /** @var PhaseItemInterface[] */
    protected $items;

    /**
     * {@inheritDoc}
     */
    public function exportConfig()
    {
        $itemConfig = [];

        if ($this->hasItems()) {
            foreach ($this->getItems() as $item) {
                if ($item->doPrerequisitesPass()) {
                    $itemConfig[$item->getId()] = $item->exportConfig();
                }
            }
        }

        return [
            'items' => $itemConfig,
        ] + parent::exportConfig();
    }

    /**
     * {@inheritDoc}
     */
    public function preProcess()
    {
        parent::preProcess();

        if (is_array($this->config['items']) && count($this->config['items']) > 0) {
            foreach ($this->config['items'] as $itemName => $itemConfig) {
                $defaults = isset($this->config['defaults']) ? $this->config['defaults'] : array();
                $itemConfig = isset($itemConfig) ? $itemConfig : array();

                $config = array_merge($defaults, $itemConfig);

                $item = $this->phase->createPhaseItem($itemName, $config, $this);

                if ($item) {
                    $this->items[] = $item;
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function postProcess()
    {
        parent::postProcess();

        if ($this->hasItems()) {
            /** @var PhaseItemInterface $item */
            foreach ($this->getItems() as $item) {
                $item->postProcess();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getData()
    {
        return [
            'items' => $this->items,
        ] + parent::getData();
    }

    /**
     * @return bool
     */
    public function hasItems()
    {
        return count($this->items) > 0;
    }

    /**
     * @return PhaseItemInterface[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * {@inheritDoc}
     */
    public function getChecksum()
    {
        $checksum = parent::getChecksum();

        if ($this->hasItems()) {
            foreach ($this->getItems() as $item) {
                $checksum .= $item->getChecksum();
            }

            $checksum = crc32($checksum);
        }

        return $checksum;
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdateToken()
    {
        $token = parent::getUpdateToken().'-collection';

        if ($this->hasItems()) {
            foreach ($this->getItems() as $item) {
                $token .= '-'.$item->get('type').'-'.$item->get('fieldName');
            }
        }

        return $token;
    }

    /**
     * {@inheritDoc}
     */
    public function getMessages()
    {
        $messages = $this->errorLogger->getMessages();

        if ($this->hasItems()) {
            foreach ($this->getItems() as $item) {
                $messages = array_merge($messages, $item->getMessages());
            }
        }

        return $messages;
    }

    /**
     * {@inheritDoc}
     */
    public function processUserInput($params)
    {
        if ($this->hasItems()) {
            foreach ($this->getItems() as $item) {
                $item->processUserInput($params);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    public function loadItemData()
    {
        $doSave = false;

        // Phase items with type subTemplate have children.
        if ($this->hasItems()) {
            foreach ($this->getItems() as $item) {
                $doSave = $item->loadItemData() || $doSave;
            }
        }

        return $doSave;
    }
}
