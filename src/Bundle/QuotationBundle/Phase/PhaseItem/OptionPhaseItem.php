<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase\PhaseItem;

use Klaro\Component\Dictionary\Dictionary;

class OptionPhaseItem extends BasicPhaseItem
{
    protected static $validSourceTypes = ['model', 'parameter', 'expression', 'url'];

    /**
     * {@inheritDoc}
     */
    public function getDefaults()
    {
        return [
            'options' => null,
            'range' => false,
            'step' => 1,
            'optionTextSource' => null, // Specifies where to fetch the text for auto-generated options.
            'optionValueSource' => null,
            'filterOptions' => null,
            'generateOptions' => false,     // DEPRECATED: use optionsSource and optionsSourceType instead
            'optionsSource' => null,        // model namespace, config parameter name or url
            'optionsSourceType' => 'model', // 'model', 'parameter' or 'url'
            'ajaxPreload' => true,          // If optionsSourceType is 'url', load options from url right after the page is initialized
        ] + parent::getDefaults();
    }

    /**
     * {@inheritDoc}
     */
    public function exportConfig()
    {
        $exportedOptions = [];

        foreach ($this->get('options') as $key => $option) {
            unset($option['condition']);
            unset($option['prerequisite']);

            $exportedOptions[$key] = $option;
        }

        $parentConfig = parent::exportConfig();

        unset($parentConfig['range']);

        if ($this->has('optionsSource') && $this->get('optionsSourceType') != 'url') {
            unset($parentConfig['generateOptions']);
            unset($parentConfig['optionsSource']);
            unset($parentConfig['optionsSourceType']);
            unset($parentConfig['filterOptions']);
            unset($parentConfig['optionTextSource']);
            unset($parentConfig['optionValueSource']);
        }

        return [
            'options' => $exportedOptions,
        ] + $parentConfig;
    }

    /**
     * {@inheritDoc}
     */
    public function preProcess()
    {
        parent::preProcess();

        // Validate option optionsSourceType.
        $optionsSourceType = $this->config['optionsSourceType'];

        if ($optionsSourceType && !in_array($optionsSourceType, static::$validSourceTypes)) {
            throw new \Exception("Unsupported value \"$optionsSourceType\" for option optionsSourceType. Supported values are: ".implode(', ', static::$validSourceTypes));
        }

        // Process the options. (i.e. normalize key-values)
        if ($this->has('options')) {
            $options = $this->get('options');

            // Convert regular numeric arrays to associative arrays unless otherwise asked.
            $useNumericKeys = $this->has('useNumericKeys') ? $this->get('useNumericKeys') : true;

            if ($this->isAssociative($options) === false && !$useNumericKeys) {
                $options = $this->makeAssociative($options);
            }

            // If there is an option with "default" set to "true", then specify this option as the default value.
            foreach ($options as $key => $value) {
                if (isset($value['default']) && $value['default'] === true) {
                    $this->set('default', $key);
                }
            }

            $this->set('options', new Dictionary($options));
        }

        // Range and step support.
        if ($this->get('range') !== false) {
            $options = $this->phase->evaluateExpression($this->get('range'));

            $step = $this->get('step');

            if (is_array($options) && ($step != 1)) {
                $index = 0;

                foreach ($options as $key => $value) {
                    if ($index % $step != 0) {
                        unset($options[$key]);
                    }

                    $index++;
                }
            }

            $this->set('options', $options);

            // Make sure the key is always the same as the value.
            $options = array();
            foreach ($this->get('options') as $option) {
                $options[$option] = $option;
            }

            $this->set('options', new Dictionary($options));
        }
    }

    /**
     * {@inheritDoc}
     */
    public function postProcess()
    {
        parent::postProcess();

        // Generate options from model/parameter sources. Options from URL are fetched via ajax and not generated here.
        if ($this->has('optionsSource') && $this->get('optionsSourceType') != 'url') {
            $options = $this->phase->generateOptions($this);

            $this->set('options', $options);

            if ($options->count() < 1) {
                $this->addMessage('error', 'No options found for "'.$this->get('text').'".');
            }
        }

        // Process "options" -prerequisites.
        if ($this->has('options')) {
            $options = $this->processOptionsPrerequisites($this->get('options'));

            $this->set('options', $options);
        }

        /** @var Dictionary $options */
        $options = $this->get('options');

        // If there's a single item, select it.
        if ($this->get('optionsSourceType') != 'url') {
            $keys  = $options->getKeys();

            if ($options->count() === 1) {
                $this->setValue($keys[0]);
            }

            // If the value is not one of the options left available, select the default one.
            if (!in_array($this->getValue(), $keys)) {
                $default = $this->getDefaultValue();

                $this->setValue($default);
            }
        }

        $disabled = $this->has('disabled') && $this->get('disabled') == true;

        // Lock if needed.
        if ($disabled) {
            // Set every option as disabled if this item is disabled.
            $options = new Dictionary();
            foreach ($this->get('options') as $key => $option) {
                $option['disabled'] = true;
                $options->add($key, $option);
            }

            $this->set('options', $options);
        }
    }

    public function isEmpty()
    {
        // Irrelevant if options are loaded via ajax.
        if ($this->get('optionsSourceType') == 'url') {
            return false;
        }

        return ($this->has('options') && is_array($this->get('options')) && count($this->get('options')) === 0);
    }

    /**
     * Return values from option config or default values if config is not set.
     *
     * @param $key
     * @param null|array $optionConfig
     *
     * @return array
     */
    protected function getOptionValues($key, $optionConfig = null)
    {
        $data = [
            'text' => '',
            'disabled' => false,
        ];

        if (isset($optionConfig['text'])) {
            $data['text'] = $optionConfig['text'];
        }

        $data['disabled'] = isset($optionConfig['disabled']) ? $optionConfig['disabled'] : $this->config['disabled'];

        return $data;
    }

    /**
     * @param $originalOptions
     *
     * @return Dictionary
     */
    private function processOptionsPrerequisites($originalOptions)
    {
        $options = new Dictionary();

        // Go through all current options.
        foreach ($originalOptions as $key => $option) {
            // The option is an array (and likely to have 'text' element.)
            if (is_array($option)) {
                $condition = isset($option['condition']) ? $option['condition'] : false;

                $optionConfig = $this->getOptionValues($key, $option) + ['text' => $option['text']];

                // If the option's prerequisite evaluates to true, we add it to new "options" array.
                if ($condition) {
                    $result = $this->phase->evaluateExpression($condition, [
                        'key' => $key,
                        'option' => $optionConfig,
                    ]);

                    if ($result === false) {
                        continue;
                    }
                }
                // TODO: Remove
                elseif (isset($option['prerequisite'])) {
                    $this->phase->evalPrerequisite(null);
                }

                $options->add($key, $optionConfig);
            } else {
                $options->add($key, ['text' => $option] + $this->getOptionValues($key, null));
            }
        }

        // Save the new processed "options" array.
        return $options;
    }

    /**
     * Utility function.
     *
     * Returns whether the given variable is an associative array.
     *
     * @param array $array
     *
     * @return bool
     */
    private function isAssociative($array)
    {
        return is_array($array) && count(array_filter(array_keys($array), 'is_string')) === count($array);
    }

    /**
     * Utility function.
     *
     * Converts the given array to associative.
     *
     * @param array $array
     *
     * @return array
     */
    private function makeAssociative($array)
    {
        $newArray = array();
        array_walk($array, function ($item) use (&$newArray) {

            if (is_array($item)) {
                $key = $item['text'];
            } else {
                $key = $item;
            }

            $newArray[$key] = $item;
        });

        return $newArray;
    }
}
