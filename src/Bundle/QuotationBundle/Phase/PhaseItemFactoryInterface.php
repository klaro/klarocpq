<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationBundle\Phase;

interface PhaseItemFactoryInterface
{
    /**
     * Creates a phase item.
     *
     * @param Phase                   $phase
     * @param $id
     * @param $config
     * @param bool|false              $readOnly
     * @param PhaseItemInterface|null $parent
     *
     * @return null|PhaseItemInterface
     *
     * @throws \Exception
     */
    public function createPhaseItem(Phase $phase, $id, $config, $readOnly = false, PhaseItemInterface $parent = null);
}
