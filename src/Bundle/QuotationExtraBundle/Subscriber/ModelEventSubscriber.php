<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Subscriber;

use Klaro\QuotationBundle\Api\ModelManagerInterface;
use Klaro\QuotationBundle\Api\QuotationManagerInterface;
use Klaro\QuotationBundle\Api\UserManagerInterface;
use Klaro\QuotationBundle\Event\ModelManagerEvent;
use Klaro\QuotationBundle\Event\UserManagerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Klaro\Component\Common\Event\QuotationModelEvents;
use Klaro\QuotationBundle\Event\QuotationManagerEvent;
use Symfony\Component\Security\Core\SecurityContextInterface;

class ModelEventSubscriber implements EventSubscriberInterface
{
    /** @var QuotationManagerInterface */
    protected $quotationManager;

    /** @var ModelManagerInterface */
    protected $modelManager;

    /** @var UserManagerInterface */
    protected $userManager;

    public function __construct(QuotationManagerInterface $quotationManager, ModelManagerInterface $modelManager, UserManagerInterface $userManager)
    {
        $this->quotationManager = $quotationManager;
        $this->modelManager     = $modelManager;
        $this->userManager      = $userManager;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            QuotationModelEvents::LOAD_QUOTATION_MANAGER => 'onQuotationManagerLoad',
            QuotationModelEvents::LOAD_USER_MANAGER      => 'onUserManagerLoad',
            QuotationModelEvents::LOAD_MODEL_MANAGER     => 'onModelManagerLoad',
        );
    }

    /**
     * @param QuotationManagerEvent $event
     */
    public function onQuotationManagerLoad(QuotationManagerEvent $event)
    {
        $event->setManager($this->quotationManager);
    }

    public function onUserManagerLoad(UserManagerEvent $event)
    {
        $event->setManager($this->userManager);
    }

    public function onModelManagerLoad(ModelManagerEvent $event)
    {
        $event->setManager($this->modelManager);
    }
}
