<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Library;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Klaro\Component\QuotationSearch\Query\UserQueryInterface;
use Klaro\QuotationBundle\Api\UserManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserManager implements UserManagerInterface
{
    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /** @var ManagerRegistry */
    protected $managerRegistry;

    /**
     * @var string
     */
    protected $userClass;

    /** @var QueryHelperInterface */
    protected $queryHelper;

    /**
     * @param TokenStorageInterface $tokenStorage
     * @param ManagerRegistry       $managerRegistry
     * @param QueryHelperInterface  $queryHelper
     * @param $quotationClasses
     */
    public function __construct(TokenStorageInterface $tokenStorage, ManagerRegistry $managerRegistry, QueryHelperInterface $queryHelper, $quotationClasses)
    {
        $this->tokenStorage = $tokenStorage;
        $this->managerRegistry = $managerRegistry;
        $this->queryHelper = $queryHelper;
        $this->userClass = $quotationClasses['user'];
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrentUser()
    {
        $user = $this->getCurrentUserFromToken();

        return $user ? $this->getUser($user->getId()) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function getUsers($filters)
    {
        $currentUser = $this->getCurrentUserFromToken();

        $userClass = EntityHelper::getClassNameForObject($currentUser);

        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($userClass);

        $repository = $em->getRepository($userClass);

        $users = $repository->findBy($filters);

        return array_map(function ($user) {
            return EntityHelper::translateUser($user);
        }, $users);
    }

    /**
     * {@inheritDoc}
     */
    public function findUsers(UserQueryInterface $query)
    {
        return $this->queryHelper->findUsers($query);
    }

    /**
     * {@inheritDoc}
     */
    public function getUser($userId)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->userClass);

        $user = $em->getReference($this->userClass, $userId);

        return $user ? EntityHelper::translateUser($user) : null;
    }

    /**
     * @return mixed|null
     */
    protected function getCurrentUserFromToken()
    {
        $token = $this->tokenStorage->getToken();

        return $token ? $token->getUser() : null;
    }
}
