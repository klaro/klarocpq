<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Library;

use Doctrine\Common\Persistence\ObjectManager;
use Klaro\Component\Util\ArrayUtil;
use Klaro\QuotationBundle\Api\QuotationManagerInterface;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\QuotationSearch\Query\QuotationQueryInterface;
use Symfony\Bridge\Doctrine\ManagerRegistry;

/**
 * Class QuotationManager
 *
 * @package Klaro\QuotationExtraBundle\Adapters\Doctrine\Library
 */
class QuotationManager implements QuotationManagerInterface
{
    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;

    /**
     * @var array
     */
    protected $quotationClasses;

    /** @var QueryHelperInterface */
    protected $queryHelper;

    /**
     * @param ManagerRegistry      $managerRegistry
     * @param QueryHelperInterface $queryHelper
     * @param $quotationClasses
     */
    public function __construct(ManagerRegistry $managerRegistry, QueryHelperInterface $queryHelper, $quotationClasses)
    {
        $this->managerRegistry  = $managerRegistry;
        $this->queryHelper = $queryHelper;
        $this->quotationClasses = $quotationClasses;
    }

    /**
     * {@inheritDoc}
     */
    public function findQuotations(QuotationQueryInterface $query)
    {
        return $this->queryHelper->findQuotations($query);
    }

    /**
     * {@inheritDoc}
     */
    public function createQuotation($quotationType)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation']);

        $modelClassName = $em->getClassMetadata($this->quotationClasses['quotation'])->getName();

        $quotation = new $modelClassName();

        return EntityHelper::translateQuotation($em, $quotation);
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotation($quotationId)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation']);

        $repository = $em->getRepository($this->quotationClasses['quotation']);

        $quotation = $repository->findOneBy(['id' => $quotationId]);

        return $quotation ? EntityHelper::translateQuotation($em, $quotation) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function linkQuotation(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation']);

        $q = EntityHelper::getQuotation($quotation, $em, $this->quotationClasses['quotation']);
        $u = EntityHelper::getUser(
            $user,
            $this->managerRegistry->getManagerForClass($this->quotationClasses['user']),
            $this->quotationClasses['user']
        );

        $q->addLinkedUser($u);

        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getLinkedUsers(QuotationInterface $quotation)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation']);

        $q = EntityHelper::getQuotation($quotation, $em, $this->quotationClasses['quotation']);

        return array_map(function ($user) {
            return EntityHelper::translateUser($user);
        }, $q->getLinkedUsers());
    }

    /**
     * {@inheritDoc}
     */
    public function copyQuotation(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        $oldQuotation = EntityHelper::getQuotation($quotation);
        $newQuotation = clone $oldQuotation;

        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation']);
        $em->persist($newQuotation);
        $em->flush($newQuotation);

        // Copy revisions and models.
        $clonedRevisions = EntityHelper::cloneCollection($newQuotation, null, $oldQuotation->getRevisions());

        $latestRevision = $oldQuotation->getLatestRevision();

        if ($latestRevision && $clonedRevisions->count() > 0) {
            $lastRevisionId = $latestRevision->getRevisionId();

            $newLastRevision = $clonedRevisions->filter(function ($r) use ($lastRevisionId) {
                return $r->getRevisionId() == $lastRevisionId;
            })->first();

            $newQuotation->setLatestRevision($newLastRevision);
        }

        $newQuotation->setRevisions($clonedRevisions);

        $em->flush();

        return EntityHelper::translateQuotation($em, $newQuotation);
    }

    /**
     * {@inheritDoc}
     */
    public function removeLinkedUser(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation']);

        $q = EntityHelper::getQuotation($quotation, $em, $this->quotationClasses['quotation']);
        $u = EntityHelper::getUser(
            $user,
            $this->managerRegistry->getManagerForClass($this->quotationClasses['user']),
            $this->quotationClasses['user']
        );

        $q->removeLinkedUser($u);

        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function removeLinkedUsers(QuotationInterface $quotation)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation']);

        $q = EntityHelper::getQuotation($quotation, $em, $this->quotationClasses['quotation']);
        $q->removeLinkedUsers();

        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function removeQuotation(QuotationInterface $quotation)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation']);

        $q = EntityHelper::getQuotation($quotation, $em, $this->quotationClasses['quotation']);

        $em->remove($q);
        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function isLinked(QuotationInterface $quotation, QuotationUserInterface $user)
    {
        return ArrayUtil::inArray($user->getId(), $this->getLinkedUsers($quotation), function ($userId, $user) {
            /** @var QuotationUserInterface $user */

            return $user && $userId === $user->getId();
        });
    }

    /**
     * {@inheritDoc}
     */
    public function persistQuotation(QuotationInterface $quotation)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation']);

        $quotation = EntityHelper::getQuotation($quotation);

        if (!$em->contains($quotation)) {
            $em->persist($quotation);
        }

        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function removeQuotationRevision(QuotationRevisionInterface $revision)
    {
        $revision = EntityHelper::getQuotationRevision($revision);

        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass(get_class($revision));

        $em->remove($revision);
        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function persistQuotationRevision(QuotationRevisionInterface $revision)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation_revision']);

        $revision = EntityHelper::getQuotationRevision($revision);

        if (!$em->contains($revision)) {
            $em->persist($revision);
        }

        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotationRevisions(QuotationInterface $quotation)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation_revision']);

        $repository = $em->getRepository($this->quotationClasses['quotation_revision']);

        $results = $repository->findBy(
            ['quotation' => EntityHelper::getQuotation($quotation, $em, $this->quotationClasses['quotation'])],
            ['revisionId' => 'DESC']
        );

        $revisions = array_map(function ($result) use (&$em) {
            return ($result instanceof QuotationRevisionInterface) ?
                $result :
                EntityHelper::translateQuotationRevision($em, $result);
        }, $results);

        return $revisions;
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotationRevision(QuotationInterface $quotation, $revisionId)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation_revision']);

        $repository = $em->getRepository($this->quotationClasses['quotation_revision']);

        $revision = $repository->findOneBy([
            'quotation'  => EntityHelper::getQuotation($quotation, $em, $this->quotationClasses['quotation']),
            'revisionId' => (int) $revisionId,
        ]);

        return $revision ? EntityHelper::translateQuotationRevision($em, $revision) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function createQuotationRevision(QuotationInterface $quotation)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation_revision']);

        $modelClassName = $em->getClassMetadata($this->quotationClasses['quotation_revision'])->getName();

        $revision = new $modelClassName();

        return EntityHelper::translateQuotationRevision($em, $revision);
    }
}
