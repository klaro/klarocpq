<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Library;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use Klaro\Component\Common\Model\ModelInterface;
use Klaro\QuotationBundle\Api\ModelManagerInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;

class ModelManager implements ModelManagerInterface
{
    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;

    /**
     * @var string
     */
    protected $genericModelName;

    /**
     * @var
     */
    protected $modelNameSpaces;

    /**
     * @param ManagerRegistry  $managerRegistry
     * @param $modelNameSpaces
     * @param $quotationClasses
     */
    public function __construct(ManagerRegistry $managerRegistry, $modelNameSpaces, $quotationClasses)
    {
        $this->managerRegistry = $managerRegistry;
        $this->modelNameSpaces = $modelNameSpaces;

        $this->genericModelName = $quotationClasses['model'];
    }

    /**
     * {@inheritDoc}
     */
    public function getPhaseModel(QuotationRevisionInterface $revision, $modelName, $subId = null, $filters = [], $deferSave = false)
    {
        $revision  = EntityHelper::getQuotationRevision($revision);

        // Model selection rules:
        //
        // 1.) Model already contains namespace.
        //
        // If finder name contains ':' or slashes, assume that the finder using bundle namespace alias or
        // full namespace to get the finder.
        if (strpos($modelName, ':') !== false || strpos($modelName, '\\') !== false) {
            $criteria = [
                'revision'  => $revision,
            ];

            if ($subId !== null) {
                $criteria += [
                    'subId' => $subId,
                ];
            }

            // Add custom filters
            if (is_array($filters) && count($filters) > 0) {
                $criteria += $filters;
            }

            list($em, $model) = $this->findOrCreateModel($modelName, $criteria, $deferSave);

            return EntityHelper::translateFormModel($em, $model);
        }

        // 2.) Model is found under one of the configured namespaces.
        foreach ($this->modelNameSpaces as $namespace) {
            $modelClassName = $namespace.'\\'.$modelName;

            if (class_exists($modelClassName)) {
                $criteria = [
                    'revision'  => $revision,
                ];

                if ($subId !== null) {
                    $criteria += [
                        'subId' => $subId,
                    ];
                }

                // Add custom filters
                if (is_array($filters) && count($filters) > 0) {
                    $criteria += $filters;
                }

                list($em, $model) = $this->findOrCreateModel($modelClassName, $criteria, $deferSave);

                return EntityHelper::translateFormModel($em, $model);
            }
        }

        // 3.) A generic model is used and modelName is the phaseId
        $modelClassName = $this->genericModelName;

        $model = null;

        if (class_exists($modelClassName)) {
            $criteria = [
                'revision'  => $revision,
                'phaseId'   => $modelName,
            ];

            if ($subId !== null) {
                $criteria += [
                    'subId' => $subId,
                ];
            }

            list($em, $model) = $this->findOrCreateModel($modelClassName, $criteria, $deferSave);

            return EntityHelper::translateFormModel($em, $model);
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getItemFinder($finderName)
    {
        // If finder name contains ':' or slashes, assume that the finder using bundle namespace alias or
        // full namespace to get the finder.
        if (strpos($finderName, ':') !== false || strpos($finderName, '\\') !== false) {
            /** @var ObjectManager $em */
            $em = $this->managerRegistry->getManagerForClass($finderName);

            return $em ? EntityHelper::translateItemFinder($em, $em->getRepository($finderName)) : null;
        }

        foreach ($this->modelNameSpaces as $namespace) {
            $modelClassName = $namespace.'\\'.$finderName;

            if (class_exists($modelClassName)) {
                /** @var ObjectManager $em */
                $em = $this->managerRegistry->getManagerForClass($modelClassName);
                $repository = $em->getRepository($modelClassName);

                return EntityHelper::translateItemFinder($em, $repository);
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function isModelPersisted(ModelInterface $model)
    {
        $model = EntityHelper::getModel($model);

        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass(get_class($model));

        return $em->contains($model);
    }

    /**
     * {@inheritDoc}
     */
    public function persistModel(ModelInterface $model)
    {
        $model = EntityHelper::getModel($model);

        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass(get_class($model));

        if (!$em->contains($model)) {
            $em->persist($model);
        }

        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function removeModel(ModelInterface $model)
    {
        $model = EntityHelper::getModel($model);

        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass(get_class($model));

        $em->remove($model);
        $em->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function removePhaseModels(QuotationRevisionInterface $revision)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->genericModelName);

        $qb = $em->createQueryBuilder();

        $qb->delete($this->genericModelName, 'm')
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->eq('m.revision', ':revision')
                )
            )
            ->setParameter('revision', EntityHelper::getQuotationRevision($revision));

        $qb->getQuery()->execute();
    }

    /**
     * @param string $modelName
     * @param array  $criteria
     * @param bool   $deferSave
     *
     * @return array
     *    - EntityManager $em
     *    - mixed $model
     */
    private function findOrCreateModel($modelName, array $criteria, $deferSave = false)
    {
        /** @var ObjectManager $em */
        $em = $this->managerRegistry->getManagerForClass($modelName);

        $repository = $em->getRepository($modelName);

        // Try to find find an existing model. If not found, create a new one.
        $model = $repository->findOneBy($criteria);

        if (!$model) {
            $modelClassName = $em->getClassMetadata($modelName)->getName();
            $model = new $modelClassName();
        }

        if ($model) {
            // If model is new, set the criteria to the model so that it can be found after saving.
            if (!$em->contains($model)) {
                foreach ($criteria as $filterField => $filterValue) {
                    $model->{'set'.ucfirst($filterField)}($filterValue);
                }

                if ($deferSave !== true) {
                    $em->persist($model);
                    $em->flush();
                }
            }
        }

        return [$em, $model];
    }
}
