<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Library;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ObjectManager;
use Klaro\Component\Common\Model\FinderInterface;
use Klaro\Component\Common\Model\FormModelInterface;
use Klaro\Component\Common\Model\ModelInterface;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\QuotationSearch\Result\QuotationResult;
use Klaro\Component\QuotationSearch\Result\UserResult;
use Klaro\QuotationExtraBundle\Proxy\DoctrineFormModelProxy;
use Klaro\QuotationExtraBundle\Proxy\DoctrineItemFinderProxy;
use Klaro\QuotationExtraBundle\Proxy\DoctrineModelProxy;
use Klaro\QuotationExtraBundle\Proxy\DoctrineQuotationProxy;
use Klaro\QuotationExtraBundle\Proxy\DoctrineQuotationRevisionProxy;
use Klaro\QuotationExtraBundle\Proxy\DoctrineQuotationUserProxy;

class EntityHelper
{
    /**
     * Gets short name of class (without namespace).
     *
     * @param $className
     *
     * @return null|string
     */
    public static function getShortName($className)
    {
        $reflection = new \ReflectionClass($className);

        if ($reflection) {
            return $reflection->getShortName();
        }

        return null;
    }

    /**
     * Get class name (with namespace) for given object.
     *
     * @param $user
     *
     * @return string
     */
    public static function getClassNameForObject($user)
    {
        $reflection = new \ReflectionObject($user);

        return $reflection->getName();
    }

    /**
     * Clone a related form model collection to a given quotation.
     *
     * @param $quotation
     * @param $revision
     * @param Collection $collection
     *
     * @return Collection
     */
    public static function cloneCollection($quotation, $revision, Collection $collection)
    {
        $newCollection = new ArrayCollection();

        foreach ($collection as $obj) {
            $newObj = clone $obj;

            if ($quotation) {
                $newObj->setQuotation($quotation);
            }

            if ($revision) {
                $newObj->setRevision($revision);
            }

            $newCollection->add($newObj);
        }

        return $newCollection;
    }

    /**
     * @param QuotationInterface $quotation
     * @param ObjectManager|null $em
     * @param null               $entityName
     *
     * @return mixed|QuotationInterface
     *
     * @throws \Exception
     */
    public static function getQuotation(QuotationInterface $quotation, ObjectManager $em = null, $entityName = null)
    {
        if ($quotation instanceof DoctrineQuotationProxy) {
            return $quotation->getEntity();
        } elseif ($quotation instanceof QuotationResult) {
            // If the quotation is a QuotationResult value object, try to fetch a reference to the actual entity.
            if ($em instanceof ObjectManager && is_string($entityName)) {
                return $em->getReference($entityName, $quotation->getId());
            } else {
                throw new \Exception('QuotationResult cannot be used as a Doctrine reference!');
            }
        }

        return $quotation;
    }

    /**
     * @param QuotationRevisionInterface $revision
     *
     * @return mixed
     */
    public static function getQuotationRevision(QuotationRevisionInterface $revision)
    {
        if ($revision instanceof DoctrineQuotationRevisionProxy) {
            return $revision->getEntity();
        }

        return $revision;
    }

    /**
     * @param QuotationUserInterface $user
     * @param ObjectManager|null     $em
     * @param null                   $entityName
     *
     * @return mixed|object|QuotationUserInterface
     *
     * @throws \Exception
     */
    public static function getUser(QuotationUserInterface $user, ObjectManager $em = null, $entityName = null)
    {
        if ($user instanceof DoctrineQuotationUserProxy) {
            return $user->getEntity();
        } elseif ($user instanceof UserResult) {
            // If the user is a UserResult value object, try to fetch a reference to the actual entity.
            if ($em instanceof ObjectManager && is_string($entityName)) {
                return $em->getReference($entityName, $user->getId());
            } else {
                throw new \Exception('UserResult cannot be used as a Doctrine reference!');
            }
        }

        return $user;
    }

    /**
     * @param ModelInterface $model
     *
     * @return mixed
     */
    public static function getModel(ModelInterface $model)
    {
        if ($model instanceof DoctrineModelProxy) {
            return $model->getEntity();
        }

        return $model;
    }

    /**
     * @param ObjectManager $em
     * @param $quotation
     *
     * @return QuotationInterface
     */
    public static function translateQuotation(ObjectManager $em, $quotation)
    {
        if (!($quotation instanceof QuotationInterface)) {
            return new DoctrineQuotationProxy($em, $quotation);
        }

        return $quotation;
    }

    /**
     * @param ObjectManager $em
     * @param $revision
     *
     * @return QuotationRevisionInterface
     */
    public static function translateQuotationRevision(ObjectManager $em, $revision)
    {
        if (!($revision instanceof QuotationRevisionInterface)) {
            return new DoctrineQuotationRevisionProxy($em, $revision);
        }

        return $revision;
    }

    /**
     * @param ObjectManager $em
     * @param $model
     *
     * @return FormModelInterface
     */
    public static function translateFormModel(ObjectManager $em, $model)
    {
        if (!($model instanceof FormModelInterface)) {
            return new DoctrineFormModelProxy($em, $model);
        }

        return $model;
    }

    /**
     * @param ObjectManager $em
     * @param $model
     *
     * @return ModelInterface
     */
    public static function translateModel(ObjectManager $em, $model)
    {
        if (!($model instanceof ModelInterface)) {
            return new DoctrineModelProxy($em, $model);
        }

        return $model;
    }

    /**
     * @param $user
     *
     * @return QuotationUserInterface
     */
    public static function translateUser($user)
    {
        if (!($user instanceof QuotationUserInterface)) {
            return new DoctrineQuotationUserProxy($user, 'Id');
        }

        return $user;
    }

    /**
     * @param ObjectManager $em
     * @param $repository
     *
     * @return FinderInterface
     */
    public static function translateItemFinder(ObjectManager $em, $repository)
    {
        if (!($repository instanceof FinderInterface)) {
            return new DoctrineItemFinderProxy($em, $repository);
        }

        return $repository;
    }
}
