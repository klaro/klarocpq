<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Library;

use Klaro\Component\QuotationSearch\Query\QuotationQueryInterface;
use Klaro\Component\QuotationSearch\Query\UserQueryInterface;

interface QueryHelperInterface
{
    public function findQuotations(QuotationQueryInterface $query);

    public function findUsers(UserQueryInterface $query);
}
