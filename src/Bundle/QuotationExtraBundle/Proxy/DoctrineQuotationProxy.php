<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Proxy;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\QuotationExtraBundle\Library\EntityHelper;
use Symfony\Component\Validator\Constraints\DateTime;

class DoctrineQuotationProxy extends DoctrineModelProxy implements QuotationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getIdentifier()
    {
        return $this->entity->getIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    public function setIdentifier($identifier)
    {
        $this->entity->setIdentifier($identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle()
    {
        return $this->entity->getTitle();
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle($value)
    {
        $this->entity->setTitle($value);
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotationType()
    {
        return $this->entity->getQuotationType();
    }

    /**
     * {@inheritDoc}
     */
    public function setQuotationType($value)
    {
        $this->entity->setQuotationType($value);
    }

    /**
     * @return QuotationUserInterface
     */
    public function getOwner()
    {
        $owner = $this->entity->getOwner();

        return $owner ? EntityHelper::translateUser($owner) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function setOwner(QuotationUserInterface $owner)
    {
        $this->entity->setOwner(EntityHelper::getUser($owner));
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->entity->getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($value)
    {
        $this->entity->setCreatedAt($value);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {
        return $this->entity->getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($value)
    {
        $this->entity->setUpdatedAt($value);
    }

    /**
     * {@inheritDoc}
     */
    public function getLatestRevision()
    {
        $latest = $this->entity->getLatestRevision();

        return $latest ? EntityHelper::translateQuotationRevision($this->em, $latest) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function setLatestRevision(QuotationRevisionInterface $revision = null)
    {
        $this->entity->setLatestRevision($revision ? EntityHelper::getQuotationRevision($revision) : null);
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        $result = $this->toArray();
        $result['owner'] = $this->getOwner()->getFullname() == "" ? $this->getOwner()->getFullname() : $this->getOwner()->getEmail();

        return $result;
    }
}
