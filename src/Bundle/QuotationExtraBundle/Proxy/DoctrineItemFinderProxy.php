<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Proxy;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;
use Klaro\Component\Common\Model\FinderInterface;
use Klaro\QuotationExtraBundle\Library\EntityHelper;

class DoctrineItemFinderProxy implements FinderInterface
{
    /** @var ObjectManager */
    protected $em;

    /** @var ObjectRepository */
    protected $query;

    /**
     * @param ObjectManager    $em
     * @param ObjectRepository $query
     */
    public function __construct(ObjectManager $em, ObjectRepository $query)
    {
        $this->em = $em;
        $this->query = $query;
    }

    /**
     * {@inheritDoc}
     */
    public function getItems($filters)
    {
        if ($filters != null) {
            return $this->query->findBy($filters);
        }

        return $this->query->findAll();
    }

    /**
     * {@inheritDoc}
     */
    public function translateModel($model)
    {
        return EntityHelper::translateModel($this->em, $model);
    }
}
