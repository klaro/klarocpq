<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Proxy;

use Klaro\Component\Common\Model\QuotationUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class DoctrineQuotationUserProxy implements QuotationUserInterface
{
    /** @var UserInterface */
    protected $user;

    protected $identifier;

    /**
     * @param UserInterface $user
     * @param string        $identifier
     */
    public function __construct(UserInterface $user, $identifier = 'Username')
    {
        $this->user = $user;
        $this->identifier = $identifier;
    }

    /**
     * {@inheritDoc}
     */
    public function __get($field)
    {
        return $this->get($field);
    }

    /**
     * {@inheritDoc}
     */
    public function __call($name, $arguments)
    {
        if (method_exists($this->user, $name)) {
            return call_user_func_array([$this->user, $name], $arguments);
        }

        throw new \Exception('Method "'.$name.'" not found!');
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->user;
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->user->{'get'.$this->identifier}();
    }

    /**
     * {@inheritDoc}
     */
    public function getProfileTitle()
    {
        return $this->user->getProfileTitle();
    }

    /**
     * {@inheritDoc}
     */
    public function getFullName()
    {
        return $this->user->getFullname();
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {
        return $this->user->getEmail();
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstName()
    {
        return $this->user->getFirstName();
    }

    /**
     * {@inheritDoc}
     */
    public function getLastName()
    {
        return $this->user->getLastName();
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {
        return $this->user->getRoles();
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {
        return $this->user->getPassword();
    }

    /**
     * {@inheritDoc}
     */
    public function getSalt()
    {
        return $this->user->getSalt();
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {
        return $this->user->getUsername();
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {
        $this->user->eraseCredentials();
    }

    /**
     * {@inheritDoc}
     */
    public function toArray()
    {
        return array(
            'Id'       => $this->getId(),
            'FullName' => $this->getFullname(),
            'Roles'    => $this->getRoles(),
            'Username' => $this->getUsername(),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getData()
    {
        return array(
            'identifier' => $this->identifier,
            'user'       => $this->user,
        );
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return $this->getData();
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {
        return serialize($this->getData());
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        $this->identifier = $data['identifier'];
        $this->user = $data['user'];
    }

    /**
     * {@inheritDoc}
     */
    public function get($field, $arguments = null)
    {
        $methodName = 'get'.ucfirst($field);

        return call_user_func_array([$this->user, $methodName], is_array($arguments) ? $arguments : [$arguments]);
    }
}
