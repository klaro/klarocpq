<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Proxy;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Klaro\Component\Common\Model\ModelInterface;

class DoctrineModelProxy implements ModelInterface
{
    /** @var ObjectManager */
    protected $em;

    /* This is the doctrine entity. Type is entity class, and so can't be defined here. */
    protected $entity;

    /** @var array  */
    protected $entityFields;

    /**
     * @param EntityManager $em
     * @param $entity
     */
    public function __construct(ObjectManager $em, $entity)
    {
        $this->em     = $em;
        $this->entity = $entity;

        $this->entityFields = $this->getEntityFields();
    }

    /**
     * {@inheritDoc}
     */
    public function __get($field)
    {
        return $this->get($field);
    }

    /**
     * {@inheritDoc}
     */
    public function __set($field, $value)
    {
        $this->set($field, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function __call($name, $parameters)
    {
        if (method_exists($this->entity, $name)) {
            return call_user_func_array([$this->entity, $name], $parameters);
        }

        throw new \Exception("Method {$name} does not exist!");
    }

    /**
     * @return ObjectManager|null
     */
    public function getEntityManager()
    {
        return $this->em ? $this->em : null;
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->entity->getId();
    }

    /**
     * {@inheritDoc}
     */
    public function get($field, $arguments = null)
    {
        $methodName = 'get'.ucfirst($field);

        if (method_exists($this->entity, $methodName)) {
            return call_user_func_array([$this->entity, $methodName], is_array($arguments) ? $arguments : [$arguments]);
        }

        return $this->entity->get($field, $arguments);
    }

    /**
     * {@inheritDoc}
     */
    public function set($field, $value)
    {
        $setter = 'set'.ucfirst($field);

        if (method_exists($this->entity, $setter)) {
            return $this->entity->$setter($value);
        }

        return $this->entity->set($field, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function toArray()
    {
        if (method_exists($this->entity, 'toArray')) {
            return $this->entity->toArray();
        }

        $data = [];

        foreach ($this->entityFields as $field) {
            try {
                $value = $this->get(ucfirst($field));

                if ($value instanceof \DateTime) {
                    $value = $value->format('Y-m-d H:i:s');
                }

                $data[$field] = $value;
            } catch (\Exception $e) {
                $data[$field] = 'Error getting value for "'.$field.'" field.';
            }
        }

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {
        $data = [];

        foreach ($this->entityFields as $field) {
            try {
                $value = $this->get(ucfirst($field));

                if ($value instanceof \DateTime) {
                    $value = $value->format('Y-m-d H:i:s');
                }

                $data[$field] = $value;
            } catch (\Exception $e) {
                $data[$field] = 'Error serializing value for "'.$field.'" field.';
            }
        }

        return serialize([
            'entity'   => $data,
            'mappings' => $this->entityFields,
            'class'    => get_class($this->entity),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        $class = $data['class'];

        $this->entity = new $class();
        $this->entityFields = $data['mappings'];

        if (is_array($data['entity'])) {
            foreach ($data['entity'] as $field => $value) {
                $this->set($field, $value);
            }
        }
    }

    /**
     * @return array
     */
    public function getEntityFields()
    {
        $className = get_class($this->entity);
        $metaData  = $this->em->getClassMetadata($className);

        return array_keys($metaData->fieldMappings);
    }
}
