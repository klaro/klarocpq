<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Proxy;

use Klaro\Component\Common\Model\FormModelInterface;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\QuotationExtraBundle\Library\EntityHelper;

class DoctrineFormModelProxy extends DoctrineModelProxy implements FormModelInterface
{
    /**
     * {@inheritDoc}
     */
    public function getQuotation()
    {
        $quotation = $this->entity->getQuotation();

        return $quotation ? EntityHelper::translateQuotation($this->em, $quotation) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function setQuotation(QuotationInterface $quotation)
    {
        $this->entity->setQuotation(EntityHelper::getQuotation($quotation));
    }

    /**
     * {@inheritDoc}
     */
    public function getRevision()
    {
        $revision = $this->entity->getRevision();

        return $revision ? EntityHelper::translateQuotationRevision($this->em, $revision) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function setRevision(QuotationRevisionInterface $revision)
    {
        $this->entity->setRevision(EntityHelper::getQuotationRevision($revision));
    }

    /**
     * {@inheritDoc}
     */
    public function getPhaseId()
    {
        return $this->entity->getPhaseId();
    }

    /**
     * {@inheritDoc}
     */
    public function setPhaseId($phaseId)
    {
        $this->entity->setPhaseId($phaseId);
    }

    /**
     * {@inheritDoc}
     */
    public function getSubId()
    {
        return $this->entity->getSubId();
    }

    /**
     * {@inheritDoc}
     */
    public function setSubId($subId)
    {
        $this->entity->setSubId($subId);
    }

    /**
     * {@inheritDoc}
     */
    public function getLocked()
    {
        return $this->entity->getLocked();
    }

    /**
     * {@inheritDoc}
     */
    public function setLocked($locked)
    {
        $this->entity->setLocked($locked);
    }

    /**
     * {@inheritDoc}
     */
    public function getFormData()
    {
        return $this->entity->getFormData();
    }

    /**
     * {@inheritDoc}
     */
    public function setFormData($data)
    {
        $this->entity->setFormData($data);
    }
}
