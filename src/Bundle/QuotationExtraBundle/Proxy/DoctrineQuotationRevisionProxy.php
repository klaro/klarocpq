<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Proxy;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\QuotationExtraBundle\Library\EntityHelper;

class DoctrineQuotationRevisionProxy extends DoctrineModelProxy implements QuotationRevisionInterface
{
    /**
     * {@inheritDoc}
     */
    public function getIdentifier()
    {
        return $this->entity->getIdentifier();
    }

    public function setIdentifier($identifier)
    {
        $this->entity->setIdentifier($identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function getRevisionId()
    {
        return $this->entity->getRevisionId();
    }

    /**
     * {@inheritDoc}
     */
    public function setRevisionId($revisionId)
    {
        $this->entity->setRevisionId($revisionId);
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotation()
    {
        $quotation = $this->entity->getQuotation();

        return $quotation ? EntityHelper::translateQuotation($this->em, $quotation) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function setQuotation(QuotationInterface $quotation)
    {
        $this->entity->setQuotation(EntityHelper::getQuotation($quotation));
    }

    /**
     * {@inheritDoc}
     */
    public function getType()
    {
        return $this->entity->getType();
    }

    /**
     * {@inheritDoc}
     */
    public function setType($type)
    {
        $this->entity->setType($type);
    }

    /**
     * {@inheritDoc}
     */
    public function getClassification()
    {
        return $this->entity->getClassification();
    }

    /**
     * {@inheritDoc}
     */
    public function setClassification($classification)
    {
        $this->entity->setClassification($classification);
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus()
    {
        return $this->entity->getStatus();
    }

    /**
     * {@inheritDoc}
     */
    public function setStatus($status)
    {
        $this->entity->setStatus($status);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrderStatus()
    {
        return $this->entity->getOrderStatus();
    }

    /**
     * {@inheritDoc}
     */
    public function setOrderStatus($status)
    {
        $this->entity->setOrderStatus($status);
    }

    /**
     * {@inheritDoc}
     */
    public function getLogMessage()
    {
        return $this->entity->getLogMessage();
    }

    /**
     * {@inheritDoc}
     */
    public function setLogMessage($message)
    {
        $this->entity->setLogMessage($message);
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle()
    {
        return $this->entity->getTitle();
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle($title)
    {
        $this->entity->setTitle($title);
    }

    /**
     * {@inheritDoc}
     */
    public function isValid()
    {
        return $this->entity->isValid();
    }

    /**
     * {@inheritDoc}
     */
    public function setIsValid($valid)
    {
        $this->entity->setIsValid($valid);
    }

    /**
     * {@inheritDoc}
     */
    public function getOfferingSummary()
    {
        return $this->entity->getOfferingSummary();
    }

    /**
     * {@inheritDoc}
     */
    public function setOfferingSummary($summary)
    {
        $this->entity->setOfferingSummary($summary);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->entity->getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($value)
    {
        $this->entity->setCreatedAt($value);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {
        return $this->entity->getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($value)
    {
        $this->entity->setUpdatedAt($value);
    }

    /**
     * {@inheritDoc}
     */
    public function getUser()
    {
        $user = $this->entity->getUser();

        return $user ? EntityHelper::translateUser($user) : null;
    }

    /**
     * {@inheritDoc}
     */
    public function setUser(QuotationUserInterface $user)
    {
        $this->entity->setUser(EntityHelper::getUser($user));
    }

    /**
     * {@inheritDoc}
     */
    public function getSalesStructure()
    {
        return $this->entity->getSalesStructure();
    }

    /**
     * {@inheritDoc}
     */
    public function setSalesStructure($salesStructure)
    {
        $this->entity->setSalesStructure($salesStructure);
    }

    /**
     * {@inheritDoc}
     */
    public function getProductItems()
    {
        return $this->entity->getProductItems();
    }

    /**
     * {@inheritDoc}
     */
    public function setProductItems($productItems)
    {
        $this->entity->setProductItems($productItems);
    }

    /**
     * {@inheritDoc}
     */
    public function getPhases()
    {
        return $this->entity->getPhases();
    }

    /**
     * {@inheritDoc}
     */
    public function setPhases($phases)
    {
        $this->entity->setPhases($phases);
    }

    /**
     * {@inheritDoc}
     */
    public function getCalculatedSalesPriceWithTax()
    {
        return $this->entity->getCalculatedSalesPriceWithTax();
    }

    /**
     * {@inheritDoc}
     */
    public function setCalculatedSalesPriceWithTax($salesPrice)
    {
        $this->entity->setCalculatedSalesPriceWithTax($salesPrice);
    }

    /**
     * {@inheritDoc}
     */
    public function getTargetSalesPriceWithTax()
    {
        return $this->entity->getTargetSalesPriceWithTax();
    }

    /**
     * {@inheritDoc}
     */
    public function setTargetSalesPriceWithTax($salesPrice)
    {
        $this->entity->setTargetSalesPriceWithTax($salesPrice);
    }

    /**
     * {@inheritDoc}
     */
    public function getCalculatedCostPrice()
    {
        return $this->entity->getCalculatedCostPrice();
    }

    /**
     * {@inheritDoc}
     */
    public function setCalculatedCostPrice($costPrice)
    {
        $this->entity->setCalculatedCostPrice($costPrice);
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrency()
    {
        return $this->entity->getCurrency();
    }

    /**
     * {@inheritDoc}
     */
    public function setCurrency($currency)
    {
        $this->entity->setCurrency($currency);
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaData($name, $value)
    {
        $this->entity->setMetaData($name, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getMetaData($name)
    {
        return $this->entity->getMetaData($name);
    }

    /**
     * {@inheritDoc}
     */
    public function getMetaDatas()
    {
        return $this->entity->getMetaDatas();
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaDatas($metadata)
    {
        $this->entity->setMetaDatas($metadata);
    }

    /**
     * {@inheritDoc}
     */
    public function getQualifiers()
    {
        return $this->entity->getQualifiers();
    }

    /**
     * {@inheritDoc}
     */
    public function setQualifiers($qualifiers)
    {
        $this->entity->setQualifiers($qualifiers);
    }

    /**
     * {@inheritDoc}
     */
    public function getQualifier($name)
    {
        return $this->entity->getQualifier($name);
    }

    /**
     * {@inheritDoc}
     */
    public function setQualifier($name, $value)
    {
        $this->entity->setQualifier($name, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getProductLine()
    {
        return $this->entity->getProductLine();
    }

    /**
     * {@inheritDoc}
     */
    public function setProductLine($productLine)
    {
        $this->entity->setProductLine($productLine);
    }

    /**
     * {@inheritDoc}
     */
    public function getProductLineVersion()
    {
        return $this->entity->getProductLineVersion();
    }

    /**
     * {@inheritDoc}
     */
    public function setProductLineVersion($version)
    {
        $this->entity->setProductLineVersion($version);
    }

    /**
     * {@inheritDoc}
     */
    public function getConfigurator()
    {
        return $this->entity->getConfigurator();
    }

    /**
     * {@inheritDoc}
     */
    public function setConfigurator($configurator)
    {
        $this->entity->setConfigurator($configurator);
    }

    /**
     * {@inheritDoc}
     */
    public function getConfiguratorVersion()
    {
        return $this->entity->getConfiguratorVersion();
    }

    /**
     * {@inheritDoc}
     */
    public function setConfiguratorVersion($version)
    {
        $this->entity->setConfiguratorVersion($version);
    }

    /**
     * @return array
     *
     * @since 2.2.4
     */
    public function getPhaseData()
    {
        return $this->entity->getPhaseData();
    }

    /**
     * @param array $phaseData
     *
     * @since 2.2.4
     */
    public function setPhaseData(array $phaseData)
    {
        $this->entity->setPhaseData($phaseData);
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        $result = $this->toArray();
        unset($result['offeringSummary']);
        unset($result['phaseData']);

        return $result;
    }
}
