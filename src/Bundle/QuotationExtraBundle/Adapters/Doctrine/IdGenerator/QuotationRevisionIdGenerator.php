<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Adapters\Doctrine\IdGenerator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class QuotationRevisionIdGenerator extends AbstractIdGenerator
{
    /**
     * {@inheritDoc}
     */
    public function generate(EntityManager $em, $entity)
    {
        $quotation = $entity->getQuotation();

        if (!$quotation) {
            throw new \Exception('Quotation must be set to generate revision id!');
        }

        $id = $quotation->getId().'-'.$entity->getRevisionId();

        return $id;
    }
}
