<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Adapters\Doctrine\IdGenerator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

class FormModelIdGenerator extends AbstractIdGenerator
{
    /**
     * {@inheritDoc}
     */
    public function generate(EntityManager $em, $entity)
    {
        $revision = $entity->getRevision();

        if (!$revision) {
            throw new \Exception('Revision must be set to generate model id!');
        }

        $id = $revision->getId().'-'.$entity->getSubId().'-'.$entity->getPhaseId();

        return $id;
    }
}
