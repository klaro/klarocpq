<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Adapters\Doctrine;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Klaro\Component\Common\Query\QueryCriteria;
use Klaro\Component\QuotationSearch\Query\QueryInterface;
use Klaro\Component\QuotationSearch\Query\QuotationQueryInterface;
use Klaro\Component\QuotationSearch\Query\QuotationEntities;
use Klaro\Component\QuotationSearch\Query\QuotationQueryFields;
use Klaro\Component\QuotationSearch\Result\QuotationResult;
use Klaro\Component\Common\Result\ResultSet;
use Klaro\Component\QuotationSearch\Query\RevisionQueryFields;
use Klaro\Component\Common\Query\SortParam;
use Klaro\Component\Common\Query\QueryParam;
use Klaro\Component\QuotationSearch\Query\UserQueryFields;
use Klaro\Component\QuotationSearch\Query\UserQueryInterface;
use Klaro\Component\QuotationSearch\Result\UserResult;
use Klaro\QuotationBundle\Api\QuotationQueryEvents;
use Klaro\QuotationBundle\Event\QuotationEvent;
use Klaro\QuotationExtraBundle\Library\QueryHelperInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class QueryHelper implements QueryHelperInterface
{
    /**
     * @var ManagerRegistry
     */
    protected $managerRegistry;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var array
     */
    protected $quotationClasses;

    public function __construct(ManagerRegistry $managerRegistry,
                                EventDispatcherInterface $eventDispatcher,
                                $quotationClasses)
    {
        $this->managerRegistry = $managerRegistry;
        $this->eventDispatcher = $eventDispatcher;
        $this->quotationClasses = $quotationClasses;
    }

    /**
     * {@inheritDoc}
     */
    public function findQuotations(QuotationQueryInterface $query)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['quotation']);

        $dbFields = [
            QuotationEntities::QUOTATION => [
                QuotationQueryFields::ID => 'q.id',
                QuotationQueryFields::IDENTIFIER => 'q.identifier',
                QuotationQueryFields::TITLE => 'q.title',
                QuotationQueryFields::QUOTATION_TYPE => 'q.quotationType',
                QuotationQueryFields::CREATED_AT => 'q.createdAt',
                QuotationQueryFields::UPDATED_AT => 'q.updatedAt',
                QuotationQueryFields::LATEST_REVISION_ID => 'q.latestRevisionId',
            ],
            QuotationEntities::REVISION => [
                RevisionQueryFields::QUOTATION_ID => 'q.id',
                RevisionQueryFields::REVISION_ID => 'r.revisionId',
                RevisionQueryFields::IDENTIFIER => 'r.identifier',
                RevisionQueryFields::TITLE => 'r.title',
                RevisionQueryFields::STATUS => 'r.status',
                RevisionQueryFields::ORDER_STATUS => 'r.orderStatus',
                RevisionQueryFields::METADATA => 'r.metadatas',
                RevisionQueryFields::QUALIFIERS => 'r.qualifiers',
            ],
            QuotationEntities::USER => [
                UserQueryFields::ID         => 'o.id',
                UserQueryFields::FIRST_NAME => 'o.firstName',
                UserQueryFields::LAST_NAME  => 'o.lastName',
                UserQueryFields::EMAIL      => 'o.email',
            ],
        ];

        $qb = $em->createQueryBuilder();

        $neededEntities = $this->addSelectFields($qb, $query, $dbFields);

        $this->addDefaultJoins($qb, $query, $neededEntities);
        $this->addCustomJoins($qb, $query);

        $criteriaFields = [
            QuotationQueryFields::QUOTATION_TYPE => 'q.quotationType',
            QuotationQueryFields::IDENTIFIER     => 'q.identifier',
            QuotationQueryFields::OWNER        => 'o.id',
            QuotationQueryFields::HANDLER      => 'qu.id',
            QuotationQueryFields::CREATED_AT   => 'q.createdAt',
            QuotationQueryFields::UPDATED_AT   => 'q.updatedAt',

            RevisionQueryFields::IDENTIFIER    => 'r.identifier',
            RevisionQueryFields::PROPOSAL_TYPE => 'r.qualifiers',
            RevisionQueryFields::STATUS        => 'r.status',
            RevisionQueryFields::METADATA      => 'r.metadatas',
        ];

        $this->addConditions($qb, $query, $criteriaFields);

        list($results, $totalResults) = $this->getPagedResults($qb);

        $mappingFunction = function ($values) {
            return new QuotationResult(DoctrineORMHelper::mapResultEntities($values));
        };

        return new ResultSet($query, $results, $totalResults, $mappingFunction);
    }

    /**
     * {@inheritDoc}
     */
    public function findUsers(UserQueryInterface $query)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->managerRegistry->getManagerForClass($this->quotationClasses['user']);
        $qb = $em->createQueryBuilder();

        $dbFields = [
            QuotationEntities::USER => [
                UserQueryFields::ID         => 'u.id',
                UserQueryFields::FIRST_NAME => 'u.firstName',
                UserQueryFields::LAST_NAME  => 'u.lastName',
                UserQueryFields::EMAIL      => 'u.email',
            ],
        ];

        $this->addSelectFields($qb, $query, $dbFields);

        $qb->from($this->quotationClasses['user'], 'u');

        $criteriaFields = [
            'FirstName' => 'u.firstName',
            'LastName'  => 'u.lastName',
            'FullName'  => ['u.firstName', 'u.lastName'],
            'Email'     => 'u.email',
        ];

        $this->addConditions($qb, $query, $criteriaFields);

        list($results, $totalResults) = $this->getPagedResults($qb);

        $mappingFunction = function ($values) {
            return new UserResult(DoctrineORMHelper::mapResultEntities($values));
        };

        return new ResultSet($query, $results, $totalResults, $mappingFunction);
    }

    /**
     * Add select fields to the DB query builder based on the desired fields in the quotation query object. Returns
     * a list of entity names that are needed as joins in the actual query.
     *
     * @param QueryBuilder   $qb       Doctrine query builder
     * @param QueryInterface $query    Quotation query object
     * @param array          $dbFields List of mappings that map quotation query fields to actual DB fields in the query. Eg.
     *                                 [ QuotationEntities::QUOTATION => [ QuotationQueryFields::ID => 'q.id',
     *                                 QuotationQueryFields::QUOTATION_TYPE => 'q.quotationType', ... ],
     *                                 QuotationEntities::REVISION => [ QuotationQueryFields::STATUS => 'r.status', ... ] ]
     *
     * @return array List of entities that are needed as joins in the query.
     *
     * @throws \Exception
     *
     * @since 2.0.2
     */
    public function addSelectFields(QueryBuilder $qb, QueryInterface $query, $dbFields)
    {
        // Get fields for quotation entities in the quotation query.
        $resultFields = [
            QuotationEntities::QUOTATION => $query->getQuotationFields(),
            QuotationEntities::REVISION  => $query->getRevisionFields(),
            QuotationEntities::USER      => $query->getUserFields(),
        ];

        // Get mapping for entity columns that can be used as alias in the flat result list.
        $resultMappings = DoctrineORMHelper::getResultMapping();

        $neededEntities = [];

        foreach ($resultFields as $entity => $fields) {
            // Add queried fields as selected columns in the DB query, aliased according to the result mapping.
            if (array_key_exists($entity, $dbFields) && array_key_exists($entity, $resultMappings)) {
                foreach ($fields as $resultField) {
                    if (array_key_exists($resultField, $dbFields[$entity]) && array_key_exists($resultField, $resultMappings[$entity])) {
                        $qb->addSelect($dbFields[$entity][$resultField].' AS '.$resultMappings[$entity][$resultField]);

                        if (!isset($neededEntities[$entity])) {
                            $neededEntities[$entity] = true;
                        }
                    }
                }
            }
        }

        if (count($neededEntities) < 1) {
            throw new \Exception('No entitities selected!');
        }

        return array_keys($neededEntities);
    }

    /**
     * @param QueryBuilder   $qb
     * @param QueryInterface $query
     * @param $neededEntities
     */
    public function addDefaultJoins(QueryBuilder $qb, QueryInterface $query, $neededEntities)
    {
        $qb->from($this->quotationClasses['quotation'], 'q');

        if ($query->hasHandler()) {
            $qb->leftJoin('q.linkedUsers', 'qu')
                ->groupBy('q.id');
        }

        if (in_array(QuotationEntities::REVISION, $neededEntities)) {
            $qb->leftJoin('q.latestRevision', 'r');
        }

        if ($query->hasOwner() || in_array(QuotationEntities::USER, $neededEntities)) {
            $qb->leftJoin('q.owner', 'o');
        }
    }

    /**
     * @param QueryBuilder $qb
     * @param QueryInterface $query
     */
    public function addCustomJoins(QueryBuilder $qb, QueryInterface $query)
    {
        $event = new QuotationEvent(null, array(
            'queryBuilder'    => $qb,
            'query'        => $query,
        ));
        $this->eventDispatcher->dispatch(QuotationQueryEvents::JOIN, $event);
    }

    /**
     * Adds search conditions, sorting and pagination to a Doctrine query builder based on the quotation query.
     *
     * @param QueryBuilder   $qb             Doctrine query builder
     * @param QueryInterface $query          Quotation query
     * @param array          $criteriaFields List of mappings on the query object to actual database fields. Eg.
     *                                       [ QuotationQueryFields::QUOTATION_TYPE => 'q.quotationType' ]
     *                                       Conditions are joined using AND except if the value is an array in
     *                                       which case those fields use OR: [ QuotationUserFields::FIRST_NAME
     *                                       => 'u.firstName', QuotationUserFields::LAST_NAME  => 'u.lastName',
     *                                       QuotationUserFields::FULL_NAME  => ['u.firstName', 'u.lastName'] ]
     *                                       Only fields which are present as criterias can be sorted.
     *
     * @since 2.0.2
     */
    public function addConditions(QueryBuilder $qb, QueryInterface $query, $criteriaFields)
    {
        $this->addQueryCriteria($qb, $query, $criteriaFields);
        $this->addQuerySorting($qb, $query, $criteriaFields);
        $this->addPagination($qb, $query);
    }

    /**
     * Returns the results and the total amount fetched by the query. If the query is limited,
     * then the total will be the actual amount of rows without the limit.
     *
     * @param QueryBuilder $qb
     *
     * @return array
     */
    public function getPagedResults(QueryBuilder $qb)
    {
        $results = new \ArrayIterator($qb->getQuery()->getArrayResult());

        // If the query is limited, reset the limits and query the rows.
        if (!is_null($qb->getMaxResults()) || !is_null($qb->getFirstResult())) {
            $total = $qb->setMaxResults(null)
                ->setFirstResult(null)
                ->select('1')
                ->getQuery()
                ->getScalarResult();
            $total = count($total);
        } else {
            $total = count($results);
        }

        return [$results, $total];
    }

    /**
     * @see addConditions()
     *
     * @param QueryBuilder            $qb
     * @param QuotationQueryInterface $query
     * @param $criteriaFields
     *
     * @since 2.0.2
     */
    public function addQueryCriteria(QueryBuilder $qb, QueryInterface $query, $criteriaFields)
    {
        // Map quotation bundle query criterias to matching doctrine criterias.
        $simpleComparison = [
            QueryCriteria::EQUALS        => 'eq',
            QueryCriteria::NOT_EQUALS    => 'neq',
            QueryCriteria::IN            => 'in',
            QueryCriteria::NOT_IN        => 'notIn',
            QueryCriteria::GREATER_THAN  => 'gt',
            QueryCriteria::GREATER_EQUAL => 'gte',
            QueryCriteria::LESS_THAN     => 'lt',
            QueryCriteria::LESS_EQUAL    => 'lte',
            QueryCriteria::LIKE          => 'like',
        ];

        // Create conditions based on query.
        $conditions = $qb->expr()->andX();
        $parameters = [];

        foreach ($criteriaFields as $criteriaField => $dbField) {
            // By default, one database field maps to a query field. If array is given, assume that
            // multiple columns are searched alternatively.
            if (!is_array($dbField)) {
                $dbField = [$dbField];
            }

            // Get query criteria object from the quotation query.
            $queryCriteria = call_user_func([$query, 'get'.$criteriaField]);

            if ($queryCriteria instanceof QueryCriteria) {
                $comparison = $queryCriteria->getComparison();

                $condition = null;

                // json comparsions
                if ($comparison === QueryCriteria::JSON_EXTRACT) {
                    list($path, $value) = $queryCriteria->getValue();
                    $value = strtolower($value);
                    $condition = $qb->expr()->orX();
                    foreach($dbField as $dbFieldSingle) {
                        $condition->add("LOWER(JSON_EXTRACT(".$dbFieldSingle.", '".$path."')) like '%".$value."%'");
                    }
                }
                if ($comparison === QueryCriteria::JSON_CONTAINS) {
                    list($path, $value) = $queryCriteria->getValue();
                    $condition = $qb->expr()->orX();
                    foreach($dbField as $dbFieldSingle) {
                        $condition->add("JSON_CONTAINS(".$dbFieldSingle.",  ':' . $criteriaField, '".$path."') = 1");
                    }
                    $parameters[':' . $criteriaField] = $value;
                }

                // If comparison is BETWEEN, the value should be an array of start and end values.
                if ($comparison === QueryCriteria::BETWEEN) {
                    list($start, $end) = $queryCriteria->getValue();

                    $condition = $qb->expr()->orX();

                    foreach ($dbField as $dbFieldSingle) {
                        $condition->add(
                            $qb->expr()->between($dbFieldSingle, ":{$criteriaField}_start", ":{$criteriaField}_end")
                        );
                    }

                    $parameters[":{$criteriaField}_start"] = $start;
                    $parameters[":{$criteriaField}_end"]   = $end;
                } elseif (array_key_exists($comparison, $simpleComparison)) {
                    // Add simple comparisons as nested ORs (in most cases there is only one field).
                    $condition = $qb->expr()->orX();

                    foreach ($dbField as $dbFieldSingle) {
                        $condition->add(
                            call_user_func([$qb->expr(), $simpleComparison[$comparison]], $dbFieldSingle, ':'.$criteriaField)
                        );
                    }

                    $parameters[':'.$criteriaField] = $queryCriteria->getValue();
                }

                // Add AND condition.
                if ($condition) {
                    $conditions->add($condition);
                }
            }
        }

        foreach ($query->getRawCriteria() as $criteria) {
            $conditions->add($criteria);
        }

        if ($conditions->count() > 0) {
            $qb->where($conditions);
            $qb->setParameters($parameters);
        }
    }

    /**
     * @see addConditions()
     *
     * @param QueryBuilder            $qb
     * @param QuotationQueryInterface $query
     * @param $criteriaFields
     *
     * @since 2.0.2
     */
    private function addQuerySorting(QueryBuilder $qb, QueryInterface $query, $criteriaFields)
    {
        // Get sorting parameters from the quotation query object.
        $sortParams = $query->getSortParams();

        if (is_array($sortParams) && count($sortParams) > 0) {
            /** @var SortParam $sortParam */
            foreach ($sortParams as $sortParam) {
                $fullField = $sortParam->getValue();
                $field     = $sortParam->getValue();
                $direction = $sortParam->getSortDirection();

                $field = strtok($fullField, ':');
                if (array_key_exists($field, $criteriaFields)) {
                    $qb->addOrderBy($fullField === $field ? $criteriaFields[$field] : sprintf('JSON_EXTRACT(%s, \'$.%s\')', $criteriaFields[$field], strtok(':')),
                        $direction == SortParam::ASCENDING ? 'ASC' : 'DESC');
                } else {
                    // raw 'order by' expression
                    $qb->addOrderBy($fullField, $direction == SortParam::ASCENDING ? 'ASC' : 'DESC');
                }
            }
        }
    }

    /**
     * @see addConditions()
     *
     * @param QueryBuilder   $qb
     * @param QueryInterface $query
     *
     * @since 2.0.2
     */
    private function addPagination(QueryBuilder $qb, QueryInterface $query)
    {
        // Set offset & limit
        $offset = $query->getFirstResult();

        if ($offset instanceof QueryParam) {
            $qb->setFirstResult($offset->getValue());
        }

        $size = $query->getMaxResults();

        if ($size instanceof QueryParam) {
            $qb->setMaxResults($size->getValue());
        }
    }
}
