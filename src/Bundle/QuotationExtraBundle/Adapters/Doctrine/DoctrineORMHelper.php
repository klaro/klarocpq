<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Adapters\Doctrine;

use Klaro\Component\QuotationSearch\Query\QuotationEntities;
use Klaro\Component\QuotationSearch\Query\QuotationQueryFields;
use Klaro\Component\QuotationSearch\Query\RevisionQueryFields;
use Klaro\Component\QuotationSearch\Query\UserQueryFields;

class DoctrineORMHelper
{
    /** @var array|null */
    private static $resultMapping = null;

    /**
     * @return array|null
     */
    public static function getResultMapping()
    {
        if (!self::$resultMapping) {
            self::$resultMapping = [
                QuotationEntities::QUOTATION => [
                    QuotationQueryFields::ID => 'q_id',
                    QuotationQueryFields::IDENTIFIER => 'q_identifier',
                    QuotationQueryFields::TITLE => 'q_title',
                    QuotationQueryFields::QUOTATION_TYPE => 'q_quotationType',
                    QuotationQueryFields::OWNER => 'q_ownerId',
                    QuotationQueryFields::CREATED_AT => 'q_createdAt',
                    QuotationQueryFields::UPDATED_AT => 'q_updatedAt',
                ],
                QuotationEntities::REVISION => [
                    RevisionQueryFields::QUOTATION_ID => 'r_quotationId',
                    RevisionQueryFields::REVISION_ID => 'r_revisionId',
                    RevisionQueryFields::USER_ID => 'r_userId',
                    RevisionQueryFields::STATUS => 'r_status',
                    RevisionQueryFields::IDENTIFIER => 'r_identifier',
                    RevisionQueryFields::TITLE => 'r_title',
                    RevisionQueryFields::ORDER_STATUS => 'r_orderStatus',
                    RevisionQueryFields::METADATA => 'r_metadata',
                    RevisionQueryFields::QUALIFIERS => 'r_qualifiers',
                    RevisionQueryFields::CREATED_AT => 'r_createdAt',
                    RevisionQueryFields::UPDATED_AT => 'r_updatedAt',
                ],
                QuotationEntities::USER => [
                    UserQueryFields::ID         => 'u_id',
                    UserQueryFields::EMAIL      => 'u_email',
                    UserQueryFields::FIRST_NAME => 'u_firstName',
                    UserQueryFields::LAST_NAME  => 'u_lastName',
                ],
            ];
        }

        return self::$resultMapping;
    }

    /**
     * @param $values
     *
     * @return array
     */
    public static function mapResultEntities($values)
    {
        $mappings = [];

        foreach (self::getResultMapping() as $entity => $mapping) {
            $mappings[$entity] = array_flip($mapping);
        }

        $newValues = [];

        foreach ($values as $field => $value) {
            foreach ($mappings as $entity => $mapping) {
                if (array_key_exists($field, $mapping)) {
                    $newValues[$entity][$mapping[$field]] = $value;
                }
            }
        }

        return $newValues;
    }
}
