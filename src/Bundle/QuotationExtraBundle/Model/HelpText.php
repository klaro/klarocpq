<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Klaro\Component\Common\Model\HelpTextInterface;

/**
 * @ORM\Entity(repositoryClass="Klaro\QuotationExtraBundle\Model\HelpTextRepository")
 * @ORM\Table(name="klaro_help_text", options={"engine"="MyISAM"})
 * @ORM\HasLifecycleCallbacks

 */
class HelpText implements HelpTextInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="application", type="string", nullable=true)
     */
    protected $application;

    /**
     * @ORM\Column(name="phase_id", type="string", nullable=true)
     */
    protected $phaseId;

    /**
     * @ORM\Column(name="fieldname", type="string", nullable=true)
     */
    protected $fieldname;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(name="date_updated", type="datetime", nullable=true)
     */
    protected $dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param mixed $application
     */
    public function setApplication($application): void
    {
        $this->application = $application;
    }

    /**
     * @return mixed
     */
    public function getPhaseId()
    {
        return $this->phaseId;
    }

    /**
     * @param mixed $phaseId
     */
    public function setPhaseId($phaseId): void
    {
        $this->phaseId = $phaseId;
    }

    /**
     * @return mixed
     */
    public function getFieldname()
    {
        return $this->fieldname;
    }

    /**
     * @param mixed $fieldname
     */
    public function setFieldname($fieldname): void
    {
        $this->fieldname = $fieldname;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated): void
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdatedFormatted()
    {
        return $this->getDateUpdated()->format('Y-m-d H:i:s');
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamp()
    {
        $this->setDateUpdated(new \DateTime(date('Y-m-d H:i:s')));
    }

    /**
     *
     * Get object in array
     *
     * @return array
     */
    public function toArray() {
        $data = [];

        $data['id']          = $this->getId();
        $data['application'] = $this->getApplication();
        $data['fieldname']   = $this->getFieldname();
        $data['description'] = $this->getDescription();
        $data['date_update'] = $this->getDateUpdatedFormatted();

        return $data;
    }
}
