<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\Util\DateUtil;

class Quotation
{
    /** @var int|string */
    protected $id;

    /** @var string */
    protected $identifier;

    /** @var string */
    protected $title;

    /** @var string */
    protected $quotationType;

    /** @var \DateTime */
    protected $createdAt;

    /** @var \DateTime */
    protected $updatedAt;

    /** @var QuotationUserInterface */
    protected $owner;

    /** @var QuotationRevisionInterface|null */
    protected $latestRevision = null;

    /** @var QuotationRevisionInterface[]|ArrayCollection */
    protected $revisions;

    /** @var QuotationUserInterface[]|ArrayCollection */
    protected $linkedUsers;

    /**
     * Quotation constructor.
     */
    public function __construct()
    {
        $this->revisions = new ArrayCollection();
        $this->linkedUsers = new ArrayCollection();
    }

    /**
     *
     */
    public function __clone()
    {
        if ($this->id) {
            $this->id             = null;
            $this->owner          = null;
            $this->revisions      = null;
            $this->latestRevision = null;
            $this->linkedUsers    = clone $this->linkedUsers;
        }
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getRevisions()
    {
        return $this->revisions;
    }

    /**
     * @param mixed $revisions
     */
    public function setRevisions($revisions)
    {
        $this->revisions = $revisions;
    }

    /**
     * Set quotation type.
     *
     * @param string $quotationType
     *
     * @return Quotation
     */
    public function setQuotationType($quotationType)
    {
        $this->quotationType = $quotationType;

        return $this;
    }

    /**
     * Get quotation type
     *
     * @return string
     */
    public function getQuotationType()
    {
        return $this->quotationType;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Quotation
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = DateUtil::toDateTime($createdAt);

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Quotation
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = DateUtil::toDateTime($updatedAt);

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set owner
     *
     * @param integer $owner
     *
     * @return Quotation
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return QuotationUserInterface
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $latestRevision
     */
    public function setLatestRevision($latestRevision)
    {
        $this->latestRevision = $latestRevision;
    }

    /**
     * @return mixed
     */
    public function getLatestRevision()
    {
        return $this->latestRevision;
    }

    /**
     * @param QuotationUserInterface $linkedUser
     *
     * @return $this
     */
    public function addLinkedUser($linkedUser)
    {
        $this->linkedUsers[] = $linkedUser;

        return $this;
    }

    /**
     * @param QuotationUserInterface $linkedUser
     */
    public function removeLinkedUser($linkedUser)
    {
        if ($this->linkedUsers) {
            $this->linkedUsers->removeElement($linkedUser);
        }
    }

    /**
     *
     */
    public function removeLinkedUsers()
    {
        if ($this->linkedUsers) {
            $this->linkedUsers->clear();
        }
    }

    /**
     * @param $linkedUsers
     */
    public function setLinkedUsers($linkedUsers)
    {
        $this->linkedUsers = $linkedUsers;
    }

    /**
     * @return ArrayCollection|QuotationUserInterface[]
     */
    public function getLinkedUsers()
    {
        return $this->linkedUsers ? $this->linkedUsers->toArray() : [];
    }
}
