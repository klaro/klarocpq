<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Model;

use Klaro\Component\Common\Model\QuotationRevisionInterface;

class FormModel
{
    /** @var string */
    protected $id;

    /** @var QuotationRevisionInterface */
    protected $revision;

    /** @var string */
    protected $phaseId;

    /** @var int */
    protected $subId = 0;

    /** @var bool */
    protected $locked = false;

    /** @var array */
    protected $formData;

    /**
     *
     */
    public function __construct()
    {
        $this->formData = [];
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $revision
     */
    public function setRevision($revision)
    {
        $this->revision = $revision;
    }

    /**
     * @return mixed
     */
    public function getRevision()
    {
        return $this->revision;
    }

    /**
     * @param mixed $phaseId
     */
    public function setPhaseId($phaseId)
    {
        $this->phaseId = $phaseId;
    }

    /**
     * @return mixed
     */
    public function getPhaseId()
    {
        return $this->phaseId;
    }

    /**
     * @param mixed $subId
     */
    public function setSubId($subId)
    {
        $this->subId = $subId;
    }

    /**
     * @return mixed
     */
    public function getSubId()
    {
        return $this->subId;
    }

    /**
     * @param mixed $locked
     */
    public function setLocked($locked)
    {
        $this->locked = (bool) $locked;
    }

    /**
     * @return mixed
     */
    public function getLocked()
    {
        return (bool) $this->locked;
    }

    /**
     * @param mixed $formData
     */
    public function setFormData($formData)
    {
        $this->formData = $formData;
    }

    /**
     * @return mixed
     */
    public function getFormData()
    {
        return is_array($this->formData) ? $this->formData : [];
    }

    /**
     * @param $field
     *
     * @return mixed|null
     */
    public function get($field)
    {
        return isset($this->formData[$field]) ? $this->formData[$field] : null;
    }

    /**
     * @param $field
     * @param $value
     */
    public function set($field, $value)
    {
        $this->formData[$field] = $value;
    }
}
