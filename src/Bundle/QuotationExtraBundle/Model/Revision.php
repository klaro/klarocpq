<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Klaro\Component\Common\Model\FormModelInterface;
use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\QuotationExtraBundle\Library\EntityHelper;

class Revision
{
    /** @var string|int */
    protected $id;

    /** @var QuotationInterface */
    protected $quotation;

    /** @var string|int */
    protected $revisionId;

    /** @var string[]|array */
    protected $qualifiers;

    /** @var string */
    protected $identifier;

    /** @var string */
    protected $title;

    /** @var string */
    protected $status;

    /** @var string */
    protected $orderStatus;

    /** @var string[]|array */
    protected $metadatas;

    /** @var array */
    protected $salesStructure;

    /** @var array */
    protected $productItems;

    /** @var array */
    protected $phases;

    /** @var string */
    protected $productLine;

    /** @var string */
    protected $productLineVersion;

    /** @var string */
    protected $configurator;

    /** @var string */
    protected $configuratorVersion;

    /** @var array */
    protected $phaseData;

    /** @var array */
    protected $offeringSummary;

    /** @var \DateTime */
    protected $createdAt;

    /** @var \DateTime */
    protected $updatedAt;

    /** @var QuotationUserInterface */
    protected $user;

    /** @var FormModelInterface[]|ArrayCollection */
    protected $models;

    /**
     *
     */
    public function __construct()
    {
        $this->models = new ArrayCollection();
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id     = null;
            $this->models = EntityHelper::cloneCollection(null, $this, $this->models);
        }
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $quotation
     */
    public function setQuotation($quotation)
    {
        $this->quotation = $quotation;
    }

    /**
     * @return QuotationInterface
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * @param mixed $revisionId
     */
    public function setRevisionId($revisionId)
    {
        $this->revisionId = $revisionId;
    }

    /**
     * @return mixed
     */
    public function getRevisionId()
    {
        return $this->revisionId;
    }

    /**
     * @return mixed
     */
    public function getQualifiers()
    {
        return is_array($this->qualifiers) ? $this->qualifiers : [];
    }

    /**
     * @param mixed $qualifiers
     */
    public function setQualifiers($qualifiers)
    {
        $this->qualifiers = $qualifiers;
    }

    /**
     * @param mixed $qualifiers
     */
    public function setQualifier($name, $value)
    {
        $this->qualifiers[$name] = $value;
    }

    /**
     * @param $name
     *
     * @return mixed|null|string
     */
    public function getQualifier($name)
    {
        return isset($this->qualifiers[$name]) ? $this->qualifiers[$name] : null;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $orderStatus
     */
    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    /**
     * @return mixed
     */
    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    /**
     * @param mixed $phases
     */
    public function setPhases($phases)
    {
        $this->phases = $phases;
    }

    /**
     * @return mixed
     */
    public function getPhases()
    {
        return $this->phases;
    }

    /**
     * @return string
     */
    public function getProductLine()
    {
        return $this->productLine;
    }

    /**
     * @param string $productLine
     */
    public function setProductLine($productLine)
    {
        $this->productLine = $productLine;
    }

    /**
     * @return string
     */
    public function getProductLineVersion()
    {
        return $this->productLineVersion;
    }

    /**
     * @param string $productLineVersion
     */
    public function setProductLineVersion($productLineVersion)
    {
        $this->productLineVersion = $productLineVersion;
    }

    /**
     * @return string
     */
    public function getConfigurator()
    {
        return $this->configurator;
    }

    /**
     * @param string $configurator
     */
    public function setConfigurator($configurator)
    {
        $this->configurator = $configurator;
    }

    /**
     * @return string
     */
    public function getConfiguratorVersion()
    {
        return $this->configuratorVersion;
    }

    /**
     * @param string $configuratorVersion
     */
    public function setConfiguratorVersion($configuratorVersion)
    {
        $this->configuratorVersion = $configuratorVersion;
    }

    /**
     * @param mixed $salesStructure
     */
    public function setSalesStructure($salesStructure)
    {
        $this->salesStructure = $salesStructure;
    }

    /**
     * @return mixed
     */
    public function getSalesStructure()
    {
        return $this->salesStructure;
    }

    /**
     * @param mixed $productItem
     */
    public function setProductItems($productItems)
    {
        $this->productItems = $productItems;
    }

    /**
     * @return mixed
     */
    public function getProductItems()
    {
        return $this->productItems;
    }

    /**
     * @param mixed $offeringSummary
     */
    public function setOfferingSummary($offeringSummary)
    {
        $this->offeringSummary = $offeringSummary;
    }

    /**
     * @return mixed
     */
    public function getOfferingSummary()
    {
        return $this->offeringSummary;
    }

    /**
     * @return mixed
     */
    public function getPhaseData()
    {
        return $this->phaseData;
    }

    /**
     * @param mixed $phaseData
     */
    public function setPhaseData($phaseData)
    {
        $this->phaseData = $phaseData;
    }

    /**
     * @param array $metadata
     */
    public function setMetadatas($metadata)
    {
        $this->metadatas = (array) $metadata;
    }

    /**
     * @return mixed
     */
    public function getMetadatas()
    {
        return is_array($this->metadatas) ? $this->metadatas : [];
    }

    /**
     * @param $field
     *
     * @return mixed|null
     */
    public function getMetadata($field)
    {
        return isset($this->metadatas[$field]) ? $this->metadatas[$field] : null;
    }

    /**
     * @param $field
     * @param $value
     */
    public function setMetadata($field, $value)
    {
        $this->metadatas[$field] = $value;
    }

    /**
     * @param QuotationUserInterface $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return QuotationUserInterface
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed|null
     */
    public function getLogMessage()
    {
        return $this->getMetadata('LogMessage');
    }

    /**
     * Set revision log message.
     *
     * @param $message
     */
    public function setLogMessage($message)
    {
        $this->setMetadata('LogMessage', $message);
    }

    /**
     * Returns true if the revision's offering summary structure has been generated and cached (no need to generate
     * it again from sales structure definition).
     *
     * @return boolean
     */
    public function isValid()
    {
        return $this->getMetadata('IsValid');
    }

    /**
     * @param $valid
     *
     * @return mixed
     */
    public function setIsValid($valid)
    {
        $this->setMetadata('IsValid', $valid);
    }

    /**
     * @return mixed
     */
    public function getCalculatedSalesPriceWithTax()
    {
        return $this->getMetadata('CalculatedSalesPriceWithTax');
    }

    /**
     * @param $salesPrice
     */
    public function setCalculatedSalesPriceWithTax($salesPrice)
    {
        $this->setMetadata('CalculatedSalesPriceWithTax', $salesPrice);
    }

    /**
     * @return mixed
     */
    public function getTargetSalesPriceWithTax()
    {
        return $this->getMetadata('TargetSalesPriceWithTax');
    }

    /**
     * @param $salesPrice
     */
    public function setTargetSalesPriceWithTax($salesPrice)
    {
        $this->setMetadata('TargetSalesPriceWithTax', $salesPrice);
    }

    /**
     * @return mixed
     */
    public function getCalculatedCostPrice()
    {
        return $this->getMetadata('CalculatedCostPrice');
    }

    /**
     * @param $costPrice
     */
    public function setCalculatedCostPrice($costPrice)
    {
        $this->setMetadata('CalculatedCostPrice', $costPrice);
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->getMetadata('Currency');
    }

    /**
     * @param $currency
     */
    public function setCurrency($currency)
    {
        $this->setMetadata('Currency', $currency);
    }

    /**
     * @param mixed $models
     */
    public function setModels($models)
    {
        $this->models = $models;
    }

    /**
     * @return mixed
     */
    public function getModels()
    {
        return $this->models;
    }
}
