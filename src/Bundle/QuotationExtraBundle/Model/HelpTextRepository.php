<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\Model;

use Doctrine\ORM\EntityRepository;

class HelpTextRepository extends EntityRepository
{
    /**
     * {@inheritDoc}
     */
    public function findHelpTexts($application, $phaseId)
    {
        return $this->createQueryBuilder('o')
                    ->where('o.application = :application AND o.phaseId = :phaseId')
                    ->setParameter('application', $application)
                    ->setParameter('phaseId', $phaseId)
                    ->getQuery()
                    ->getResult();
    }
}
