<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('klaro_quotation_extra');

        $rootNode
            ->children()
                ->enumNode('orm')
                    ->values(array('doctrine'))
                    ->isRequired()
                ->end()
                ->scalarNode('bundle')
                ->end()
                ->arrayNode('namespaces')
                    ->prototype('scalar')->end()
                ->end()
                ->arrayNode('classes')
                    ->children()
                        ->scalarNode('quotation')->isRequired()->end()
                        ->scalarNode('quotation_revision')->isRequired()->end()
                        ->scalarNode('quotation_user')->defaultNull()->end()
                        ->scalarNode('user')->isRequired()->end()
                        ->scalarNode('model')->isRequired()->end()
                        ->scalarNode('help_text')
                            ->defaultValue('Klaro\QuotationExtraBundle\Model\HelpText')
                            ->treatNullLike('Klaro\QuotationExtraBundle\Model\HelpText')
                        ->end()
                    ->end()
                    ->isRequired()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
