<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\QuotationExtraBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class KlaroQuotationExtraExtension extends Extension implements PrependExtensionInterface
{
    private $interfaces;

    public function __construct()
    {
        $this->interfaces = [
            'quotation'          => 'Klaro\\Component\\Common\\Model\\QuotationInterface',
            'quotation_revision' => 'Klaro\\Component\\Common\\Model\\QuotationRevisionInterface',
            'quotation_user'     => 'Klaro\\Component\\Common\\Model\\QuotationUserLinkInterface',
            'user'               => 'Klaro\\Component\\Common\\Model\\QuotationUserInterface',
            'model'              => 'Klaro\\Component\\Common\\Model\\FormModelInterface',
            'help_text'          => 'Klaro\\Component\\Common\\Model\\HelpTextInterface',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter($this->getAlias().'.backend_type_'.$config['orm'], true);

        $container->setParameter('klaro_quotation_extra.orm', $config['orm']);
        $container->setParameter('klaro_quotation_extra.namespaces', $config['namespaces']);

        if (!isset($config['bundle'])) {
            $config['bundle'] = null;
        }

        $container->setParameter('klaro_quotation_extra.bundle', $config['bundle']);
        $container->setParameter('klaro_quotation_extra.classes', $config['classes']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        // Load ORM specific services
        $loader->load('services.yml');
        $loader->load('services_'.$config['orm'].'.yml');
    }

    /**
     * Allow an extension to prepend the extension configurations.
     *
     * @param  ContainerBuilder  $container
     */
    public function prepend(ContainerBuilder $container)
    {
        // Get configuration of our own bundle
        $configs = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration(new Configuration(), $configs);

        // Get all Bundles
        $bundles = $container->getParameter('kernel.bundles');

        // If chosen  ORM is Doctrine, prepend interface mappings to Doctrine config.
        if ($config['orm'] === 'doctrine') {
            foreach ($this->interfaces as $interfaceId => $interface) {
                $implementation = $config['classes'][$interfaceId];

                if ($implementation) {
                    $entities[$interface] = $implementation;
                }
            }

            // Prepend doctrine resolve mapping
            if (isset($bundles['DoctrineBundle']) && $container->hasExtension('doctrine')) {
                // Prepend mapping config.
                $mappingConfig = [
                    'orm' => [
                        'resolve_target_entities' => $entities ?? [],
                    ],
                ];

                $container->prependExtensionConfig('doctrine', $mappingConfig);
            }
        }
    }
}
