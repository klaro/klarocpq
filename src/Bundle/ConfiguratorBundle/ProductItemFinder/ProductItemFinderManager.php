<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\ProductItemFinder;

class ProductItemFinderManager implements ProductItemFinderInterface
{
    /** @var array */
    protected $finderServices;

    /**
     * ProductItemFinderManager constructor.
     */
    public function __construct()
    {
        $this->finderServices = [];
    }

    /**
     * @return ProductItemFinderInterface[]
     */
    public function getFinderServices()
    {
        return $this->finderServices;
    }

    /**
     * @param ProductItemFinderInterface[] $finderServices
     */
    public function setFinderServices($finderServices)
    {
        $this->finderServices = $finderServices;
    }

    /**
     * @param $source
     * @param $serviceId
     */
    public function addFinderService($source, ProductItemFinderInterface $finder)
    {
        $this->finderServices[$source] = $finder;
    }

    /**
     * {@inheritdoc}
     */
    public function getProductItems($source, array $filters, array $fields)
    {
        $finder = $this->getProductItemFinder($source);

        if (!($finder instanceof ProductItemFinderInterface)) {
            throw new \UnexpectedValueException('Product item finder with name "'.$source.'" not found!');
        }

        return $finder->getProductItems($source, $filters, $fields);
    }

    /**
     * @param $source
     *
     * @return ProductItemFinderInterface|null
     */
    private function getProductItemFinder($source)
    {
        return isset($this->finderServices[$source]) ? $this->finderServices[$source] : null;
    }
}
