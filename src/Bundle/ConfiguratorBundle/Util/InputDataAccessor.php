<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\Util;

class InputDataAccessor
{
    /** @var array */
    protected $data;

    /**
     * InputDataAccessor constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @param $name
     *
     * @return null
     */
    public function __get($name)
    {
        return $this->data[$name] ?? null;
    }
}
