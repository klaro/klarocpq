<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle;

use Doctrine\Bundle\DoctrineBundle\DependencyInjection\Compiler\DoctrineOrmMappingsPass;
use Klaro\ConfiguratorBundle\DependencyInjection\Compiler\SetNodeProcessorsCompilerPass;
use Klaro\ConfiguratorBundle\DependencyInjection\Compiler\SetProductItemFinderCompilerPass;
use Klaro\ConfiguratorBundle\DependencyInjection\Compiler\SetConfigurationContextCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class KlaroConfiguratorBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new SetNodeProcessorsCompilerPass());
        $container->addCompilerPass(new SetProductItemFinderCompilerPass());
        $container->addCompilerPass(new SetConfigurationContextCompilerPass());

        $this->addRegisterMappingsPass($container);
    }

    /**
     * @param ContainerBuilder $container
     */
    private function addRegisterMappingsPass(ContainerBuilder $container): void
    {
        $mappings = [
            realpath(__DIR__.'/Resources/config/doctrine-mapping') => 'Klaro\ConfiguratorBundle\Model',
        ];

        if (class_exists(DoctrineOrmMappingsPass::class)) {
            $container->addCompilerPass(DoctrineOrmMappingsPass::createYamlMappingDriver($mappings));
        }
    }
}
