<?php

namespace Klaro\ConfiguratorBundle\Tests\Kernel;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Exception;
use Klaro\ConfiguratorBundle\KlaroConfiguratorBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\Kernel;

class AppKernel extends Kernel
{

    /**
     * Returns an array of bundles to register.
     *
     * @return iterable|BundleInterface[] An iterable of bundle instances
     */
    public function registerBundles()
    {
        return [
            new DoctrineBundle(),
            new KlaroConfiguratorBundle(),
        ];
    }

    /**
     * Loads the container configuration.
     *
     * @param LoaderInterface $loader
     * @throws Exception
     */
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'. $this->getEnvironment() . '.yml');
    }

    public function getCacheDir()
    {
        return '/tmp/cache/' . $this->environment;
    }

    public function getLogDir()
    {
        return '/tmp/logs/' . $this->environment;
    }


}
