<?php

namespace Klaro\ConfiguratorBundle\Tests\NodeProcessor;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Klaro\Component\Configurator\Configuration\NodeProcessorInterface;
use Klaro\ConfiguratorBundle\Exception\InvalidProductNodeProcessorReferenceException;
use Klaro\ConfiguratorBundle\Exception\NodeProcessorNotFoundException;
use Klaro\ConfiguratorBundle\NodeProcessor\NodeProcessorManager;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NodeProcessorManagerTest extends TestCase
{
    public static $nodeProcessors = [
        'FirstNodeProcessor'  => ['first', 0],
        'SecondNodeProcessor' => ['second', 0],
        'ThirdNodeProcessor'  => ['third', 10],
        'FourthNodeProcessor' => ['fourth', 10],
        'FifthNodeProcessor'  => ['fifth', -10],
    ];

    public function testGetGeneralNodeProcessorsSortedByPriority(): void
    {
        $container = $this->createContainer(self::$nodeProcessors);
        $nodeProcessorManager = new NodeProcessorManager($container);
        foreach (self::$nodeProcessors as $nodeProcessorName => $attributes) {
            $nodeProcessorManager->addGeneralProcessor($nodeProcessorName, $attributes[1]);
        }

        $this->assertEquals(
            [
                $container->get('ThirdNodeProcessor'),
                $container->get('FourthNodeProcessor'),
                $container->get('FirstNodeProcessor'),
                $container->get('SecondNodeProcessor'),
                $container->get('FifthNodeProcessor'),
            ],
            $nodeProcessorManager->getGeneralProcessors()
        );
    }

    private function createContainer(array $nodeProcessors): ContainerInterface
    {
        $container = new ContainerBuilder();
        foreach ($nodeProcessors as $nodeProcessorName => $priority) {
            $nodeProcessorMock = $this->createNodeProcessorMock($nodeProcessorName);
            $container->set($nodeProcessorName, $nodeProcessorMock);
        }

        $container->compile();

        return $container;
    }

    private function createNodeProcessorMock(string $name): MockObject
    {
        return $this->getMockBuilder(NodeProcessorInterface::class)
            ->setMockClassName($name)
            ->getMockForAbstractClass();
    }

    public function testGetNodeProcessorsWithoutExceptionsSortedByPriority(): void
    {
        $container = $this->createContainer(self::$nodeProcessors);
        $nodeProcessorManager = new NodeProcessorManager($container);
        foreach (self::$nodeProcessors as $nodeProcessorName => $attributes) {
            $nodeProcessorManager->addProductProcessor($nodeProcessorName, $attributes[0], $attributes[1]);
        }

        $this->assertEquals(
            [
                $container->get('FourthNodeProcessor'),
                $container->get('ThirdNodeProcessor'),
                $container->get('SecondNodeProcessor'),
                $container->get('FirstNodeProcessor'),
                $container->get('FifthNodeProcessor'),
            ],
            $nodeProcessorManager->getNodeProcessors(['fifth', 'fourth', 'third', 'second', 'first'])
        );
    }

    public function testGetNodeProcessorWithoutException(): void
    {
        $container = $this->createContainer(self::$nodeProcessors);
        $nodeProcessorManager = new NodeProcessorManager($container);
        foreach (self::$nodeProcessors as $nodeProcessorName => $attributes) {
            $nodeProcessorManager->addProductProcessor($nodeProcessorName, $attributes[0], $attributes[1]);
        }

        $this->assertEquals($container->get('FirstNodeProcessor'), $nodeProcessorManager->getNodeProcessor('first'));
    }

    public function testGetNodeProcessorWithInvalidReference(): void
    {
        $this->expectException(InvalidProductNodeProcessorReferenceException::class);
        $this->expectExceptionMessage('Reference "test" is invalid.');

        $container = $this->createContainer([]);
        $nodeProcessorManager = new NodeProcessorManager($container);
        $nodeProcessorManager->getNodeProcessor('test');
    }

    public function testGetNodeProcessorWithInvalidService(): void
    {
        $this->expectException(NodeProcessorNotFoundException::class);
        $this->expectExceptionMessage('Node processor "FirstNodeProcessor" could not be found.');

        $container = $this->createContainer([]);
        $nodeProcessorManager = new NodeProcessorManager($container);
        $nodeProcessorManager->addProductProcessor('FirstNodeProcessor', 'test');
        $nodeProcessorManager->getNodeProcessor('test');
    }
}
