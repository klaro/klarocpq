<?php

namespace Klaro\ConfiguratorBundle\Tests\NodeProcessor;

use Exception;
use Klaro\Component\Configurator\Configuration\ConfigurationBuilder;
use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\Configuration\ProductConfigurationProcessor;
use Klaro\ConfiguratorBundle\ConfigurationContext\ConfigurationContextInitializerAdapter;
use Klaro\ConfiguratorBundle\ConfigurationContext\ProductConfigurationContextInitializer;
use Klaro\ConfiguratorBundle\ConfiguratorAdapter\ConfiguratorAdapter;
use Klaro\ConfiguratorBundle\NodeProcessor\GeneralNodeProcessorManagerAdapter;
use Klaro\ConfiguratorBundle\NodeProcessor\NodeProcessorManager;
use Klaro\ConfiguratorBundle\NodeProcessor\VirtualItemNodeProcessor;
use Klaro\ConfiguratorBundle\Tests\Traits\BootsKernelTrait;
use Klaro\ConfiguratorBundle\Tests\Traits\GetsConfigurationDefinitionTrait;
use Klaro\ConfiguratorBundle\Tests\Traits\GetsFormDataTrait;
use Klaro\ConfiguratorBundle\Tests\Traits\ProvidesKernelClassTrait;
use Klaro\ConfiguratorBundle\Tests\Traits\ProvidesTestScenariosTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class VirtualItemNodeProcessorTest extends KernelTestCase
{
    use BootsKernelTrait;
    use GetsConfigurationDefinitionTrait;
    use GetsFormDataTrait;
    use ProvidesKernelClassTrait;
    use ProvidesTestScenariosTrait;

    private const productName = 'test_product';
    private const scenariosSubDirectory = 'NodeProcessor';

    /**
     * @dataProvider  scenariosProvider
     *
     * @param string $configurationPath
     * @param string $phaseDataPath
     * @param string $expectedResultPath
     *
     * @throws Exception
     */
    public function testProcess(string $configurationPath, string $phaseDataPath, string $expectedResultPath): void
    {
        $configurationAdapter = new ConfiguratorAdapter(
            self::$kernel->getContainer()->get('klaro_configurator.expression_evaluator_factory')
        );

        $configuratorDefinition = $this->getConfigurationDefinition($configurationPath, self::productName);

        $nodeProcessorManager = new NodeProcessorManager(self::$kernel->getContainer());
        $nodeProcessorManager->addGeneralProcessor(VirtualItemNodeProcessor::class);

        $configuration = ConfigurationBuilder::create($configuratorDefinition)->build(
            $configurationAdapter->process(
                $configuratorDefinition,
                $this->getFormData(self::productName, json_decode(file_get_contents($phaseDataPath), true))
            )
        );

        ProductConfigurationProcessor::create(
            $configuration,
            new GeneralNodeProcessorManagerAdapter($nodeProcessorManager),
            $this->getContextProvider($configuration)
        )->process($configuration);

        $this->assertEquals(unserialize(file_get_contents($expectedResultPath)), $configuration->export());
    }

    /**
     * @param ConfigurationNode $configuration
     *
     * @return ConfigurationContextInitializerAdapter
     */
    private function getContextProvider(ConfigurationNode $configuration): ConfigurationContextInitializerAdapter
    {
        $contextManager = self::$kernel->getContainer()->get('klaro_configurator.configuration_context.manager');
        $initializer = new ProductConfigurationContextInitializer($configuration);

        return new ConfigurationContextInitializerAdapter($contextManager, $initializer);
    }
}
