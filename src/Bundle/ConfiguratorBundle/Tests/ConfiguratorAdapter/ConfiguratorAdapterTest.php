<?php

namespace Klaro\ConfiguratorBundle\Tests\ConfiguratorAdapter;

use Exception;
use Klaro\ConfiguratorBundle\ConfiguratorAdapter\ConfiguratorAdapter;
use Klaro\ConfiguratorBundle\Tests\Traits\BootsKernelTrait;
use Klaro\ConfiguratorBundle\Tests\Traits\GetsConfigurationDefinitionTrait;
use Klaro\ConfiguratorBundle\Tests\Traits\GetsFormDataTrait;
use Klaro\ConfiguratorBundle\Tests\Traits\ProvidesKernelClassTrait;
use Klaro\ConfiguratorBundle\Tests\Traits\ProvidesTestScenariosTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ConfiguratorAdapterTest extends KernelTestCase
{
    use BootsKernelTrait;
    use GetsConfigurationDefinitionTrait;
    use GetsFormDataTrait;
    use ProvidesKernelClassTrait;
    use ProvidesTestScenariosTrait;

    private const productName = 'test_product';

    /**
     * @dataProvider  scenariosProvider
     *
     * @param string $configurationPath
     * @param string $phaseDataPath
     * @param string $expectedResultPath
     *
     * @throws Exception
     */
    public function testProcess(string $configurationPath, string $phaseDataPath, string $expectedResultPath): void
    {
        $configurationAdapter = new ConfiguratorAdapter(
            self::$kernel->getContainer()->get('klaro_configurator.expression_evaluator_factory')
        );

        $actualResult = $configurationAdapter->process(
            $this->getConfigurationDefinition($configurationPath, self::productName),
            $this->getFormData(self::productName, json_decode(file_get_contents($phaseDataPath), true))
        );

        $this->assertEquals(unserialize(file_get_contents($expectedResultPath)), $actualResult);
    }
}
