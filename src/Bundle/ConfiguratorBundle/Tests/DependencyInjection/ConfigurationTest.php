<?php

namespace Klaro\ConfiguratorBundle\Tests\DependencyInjection;

use Exception;
use Klaro\ConfiguratorBundle\DependencyInjection\Configuration;
use Klaro\ConfiguratorBundle\DependencyInjection\KlaroConfiguratorExtension;
use Klaro\ConfiguratorBundle\Tests\Traits\ProvidesTestScenariosTrait;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionConfigurationTestCase;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

class ConfigurationTest extends AbstractExtensionConfigurationTestCase
{
    use ProvidesTestScenariosTrait;

    private static function getExpectedArguments(): array
    {
        return ['config', 'result'];
    }

    /**
     * @dataProvider  scenariosProvider
     *
     * @param string $configurationPath
     * @param string $expectedResultPath
     *
     * @throws Exception
     */
    public function testGetConfigTreeBuilder(string $configurationPath, string $expectedResultPath): void
    {
        $this->assertProcessedConfigurationEquals(
            json_decode(file_get_contents($expectedResultPath), true),
            [$configurationPath]
        );
    }

    /**
     * Return an instance of the container extension that you are testing.
     *
     * @return ExtensionInterface
     */
    protected function getContainerExtension(): ExtensionInterface
    {
        return new KlaroConfiguratorExtension();
    }

    /**
     * Return an instance of the configuration class that you are testing.
     *
     * @return ConfigurationInterface
     */
    protected function getConfiguration(): ConfigurationInterface
    {
        return new Configuration();
    }
}
