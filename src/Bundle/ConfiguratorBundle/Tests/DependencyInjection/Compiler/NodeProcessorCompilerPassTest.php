<?php

namespace Klaro\ConfiguratorBundle\Tests\DependencyInjection\Compiler;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractCompilerPassTestCase;
use Klaro\ConfiguratorBundle\DependencyInjection\Compiler\SetNodeProcessorsCompilerPass;
use Klaro\ConfiguratorBundle\Exception\InvalidNodeProcessorTypeException;
use Klaro\ConfiguratorBundle\Exception\InvalidProductNodeProcessorReferenceException;
use Klaro\ConfiguratorBundle\NodeProcessor\NodeProcessorManager;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class NodeProcessorCompilerPassTest extends AbstractCompilerPassTestCase
{
    public function validProcessProvider(): array
    {
        return [
            [
                'general.node_processor',
                ['type' => NodeProcessorManager::GENERAL],
                'addGeneralProcessor',
                ['general.node_processor', 0],
            ],
            [
                'general.node_processor',
                ['type' => NodeProcessorManager::GENERAL, 'priority' => 10],
                'addGeneralProcessor',
                ['general.node_processor', 10],
            ],
            [
                'product.node_processor',
                ['type' => NodeProcessorManager::PRODUCT, 'ref' => 'product.processor'],
                'addProductProcessor',
                ['product.node_processor', 'product.processor', 0],
            ],
            [
                'product.node_processor',
                ['type' => NodeProcessorManager::PRODUCT, 'ref' => 'product.processor', 'priority' => 10],
                'addProductProcessor',
                ['product.node_processor', 'product.processor', 10],
            ],
        ];
    }

    public function invalidProcessProvider(): array
    {
        return [
            [
                'missing_type.node_processor',
                [],
                InvalidNodeProcessorTypeException::class,
                'Invalid node processor type.'
            ],
            [
                'invalid_type.node_processor',
                ['type' => 'generic'],
                InvalidNodeProcessorTypeException::class,
                'Node processor of type "generic" is invalid.'
            ],
            [
                'missing_product_reference.node_processor',
                ['type' => NodeProcessorManager::PRODUCT],
                InvalidProductNodeProcessorReferenceException::class,
                'Invalid product node processor reference.'
            ],
        ];
    }

    /**
     * @param  string  $serviceId
     * @param  array  $tags
     * @param  string  $expectedMethodCall
     * @param  array  $expectedArguments
     *
     * @dataProvider validProcessProvider
     */
    public function testProcessDoesNotThrowExceptions(
        string $serviceId,
        array $tags,
        string $expectedMethodCall,
        array $expectedArguments
    ): void {
        // Create  some services that will be registered with node_processor_manager
        $nodeProcessor = new Definition();
        $nodeProcessor->addTag('klaro_configurator.node_processor', $tags);

        // Register NodeProcessorManager definition that compiler pass will be manipulating
        $this->setDefinition('klaro_configurator.node_processor_manager', new Definition());
        $this->setDefinition($serviceId, $nodeProcessor);
        $this->compile();

        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'klaro_configurator.node_processor_manager',
            $expectedMethodCall,
            $expectedArguments
        );
    }

    /**
     * @param  string  $serviceId
     * @param  array  $tags
     * @param  string  $expectedException
     * @param  string  $expectedExceptionMessage
     *
     * @dataProvider invalidProcessProvider
     */
    public function testProcessThrowsException(
        string $serviceId,
        array $tags,
        string $expectedException,
        string $expectedExceptionMessage
    ): void {
        $this->expectException($expectedException);
        $this->expectExceptionMessage($expectedExceptionMessage);

        // Create  some services that will be registered with node_processor_manager
        $nodeProcessor = new Definition();
        $nodeProcessor->addTag('klaro_configurator.node_processor', $tags);

        // Register NodeProcessorManager definition that compiler pass will be manipulating
        $this->setDefinition('klaro_configurator.node_processor_manager', new Definition());
        $this->setDefinition($serviceId, $nodeProcessor);
        $this->compile();
    }

    protected function registerCompilerPass(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new SetNodeProcessorsCompilerPass());
    }
}
