<?php

namespace Klaro\ConfiguratorBundle\Tests\Traits;

use Klaro\Component\Configurator\ArrayConfigLoader;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionFactory;
use Klaro\Component\Configurator\Definition\ConfiguratorProductDefinitionNode;
use Klaro\ConfiguratorBundle\Configurator\Configurator;
use Klaro\ConfiguratorBundle\DependencyInjection\Configuration;
use Symfony\Component\Yaml\Yaml;

trait GetsConfigurationDefinitionTrait
{
    private function getConfigurationDefinition(string $filePath, string $productName): ConfiguratorProductDefinitionNode
    {
        $configurationTree = (new Configuration())->getConfigTreeBuilder()->buildTree();

        // Read yaml file content before pushing it through the configuration builder
        $configuration = Yaml::parse(file_get_contents($filePath));
        $configuration = $configurationTree->normalize($configuration['klaro_configurator']);
        $configuration = $configurationTree->finalize($configuration);

        $configurationLoader = new ArrayConfigLoader($configuration['products'], -1);
        $definitionFactory = ConfiguratorDefinitionFactory::create($configurationLoader);
        $configuratorConfig = $definitionFactory->createConfiguratorConfig($productName);

        return $definitionFactory->createStructure($configuratorConfig);
    }
}
