<?php

namespace Klaro\ConfiguratorBundle\Tests\Traits;

trait GetsClassNameTrait
{
    /**
     * @return string
     */
    private function getClassName(): string
    {
        $path = explode('\\', __CLASS__);

        return str_replace('Test', '', array_pop($path));
    }
}
