<?php

namespace Klaro\ConfiguratorBundle\Tests\Traits;

use Klaro\ConfiguratorBundle\Tests\Kernel\AppKernel;

trait ProvidesKernelClassTrait
{
    protected static function getKernelClass(): string
    {
        return AppKernel::class;
    }
}
