<?php

namespace Klaro\ConfiguratorBundle\Tests\Traits;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

trait LoadsTestScenariosTrait
{
    use GetsClassNameTrait;

    /**
     * @return array
     */
    private function loadTestScenarios(): array
    {
        $testCases = [];
        $files = (new Finder())->files()->in([__DIR__.'/../__tests__/Resources/scenarios']);

        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            [$scenario, $data, $class] = explode('/', $file->getRelativePath());

            if ((null !== $class && $class !== $this->getClassName()) ||
                false === in_array($data, self::getExpectedArguments(), true)
            ) {
                continue;
            }

            $testCases[$scenario][$data] = $file->getRealPath();

            // Sort loaded files alphabetically by folder to ensure same order for attributes
            ksort($testCases[$scenario]);
        }

        ksort($testCases);

        return array_map(
            static function (array $testCase) {
                return array_values($testCase);
            },
            $testCases
        );
    }

    private static function getExpectedArguments(): array
    {
        return ['config', 'data', 'result'];
    }
}
