<?php

namespace Klaro\ConfiguratorBundle\Tests\Traits;

trait BootsKernelTrait
{
    protected function setUp()
    {
        parent::setUp();

        self::bootKernel();
    }
}
