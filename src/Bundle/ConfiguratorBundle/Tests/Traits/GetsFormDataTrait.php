<?php

namespace Klaro\ConfiguratorBundle\Tests\Traits;

use Klaro\Component\FormData\FormDataNodeFactory;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\ProductLoader\ArrayProductConfigLoader;
use Klaro\Component\ProductPhase\PhaseFactory;
use Symfony\Component\Yaml\Yaml;

trait GetsFormDataTrait
{
    private function getFormData(string $productName, array $phaseData): ProductLineDataNode
    {
        $filePath = __DIR__.'/../__tests__/Resources/config/product_line_config.yml';

        $configuration = Yaml::parse(file_get_contents($filePath));
        $productLoader = new ArrayProductConfigLoader(
            $configuration['phases'],
            $configuration['products'],
            $configuration['product_lines'],
            -1
        );

        $definitionFactory = PhaseFactory::create($productLoader);
        $productLineDefinition = $definitionFactory->createProductLineDefinition($productName);
        $productLine = $definitionFactory->createProductLinePhase($productLineDefinition);

        return FormDataNodeFactory::create($productLine)->createProductLineDataNode($phaseData);
    }
}
