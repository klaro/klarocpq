<?php

namespace Klaro\ConfiguratorBundle\Tests\Traits;

trait ProvidesTestScenariosTrait
{
    use LoadsTestScenariosTrait;

    /**
     * @return array
     */
    public function scenariosProvider(): array
    {
        return $this->loadTestScenarios();
    }
}
