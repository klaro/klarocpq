<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\NodeProcessor;

use Klaro\Component\Configurator\Configuration\NodeProcessorInterface;
use Klaro\Component\Configurator\Configuration\NodeProcessorManagerInterface;
use Klaro\ConfiguratorBundle\Exception\InvalidProductNodeProcessorReferenceException;
use Klaro\ConfiguratorBundle\Exception\NodeProcessorNotFoundException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NodeProcessorManager implements NodeProcessorManagerInterface
{
    public const GENERAL = 'general';
    public const PRODUCT = 'product';

    /** @var ContainerInterface */
    protected $container;

    /** @var array */
    protected $processors;

    /**
     * NodeProcessorManager constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->processors[self::GENERAL] = [];
        $this->processors[self::PRODUCT] = [];
    }

    /**
     * @return array
     */
    public function getGeneralProcessors(): array
    {
        $nodeProcessors = [];

        foreach ($this->processors[self::GENERAL] as $priority => $processors) {
            foreach ($processors as $processor) {
                $nodeProcessors[] = $this->getProcessorService($processor);
            }
        }

        return $nodeProcessors;
    }

    /**
     * @param $name
     *
     * @return NodeProcessorInterface
     */
    private function getProcessorService($name): NodeProcessorInterface
    {
        /** @var NodeProcessorInterface $service */
        $service = $this->container->get($name, ContainerInterface::NULL_ON_INVALID_REFERENCE);
        if (null === $service) {
            throw new NodeProcessorNotFoundException($name);
        }

        return $service;
    }

    /**
     * @param $processor
     * @param int $priority
     */
    public function addGeneralProcessor($processor, $priority = 0): void
    {
        $this->processors[self::GENERAL][$priority][] = $processor;

        // Sort processors based on priority after being added
        krsort($this->processors[self::GENERAL]);
    }

    /**
     * @param $processor
     * @param string $reference
     * @param int  $priority
     */
    public function addProductProcessor($processor, $reference, $priority = 0): void
    {
        $this->processors[self::PRODUCT][$priority][$reference] = $processor;

        // Sort processors based on priority after being added
        krsort($this->processors[self::PRODUCT]);
    }

    /**
     * @param array $references
     *
     * @return array
     */
    public function getNodeProcessors($references): array
    {
        $nodeProcessors = [];

        foreach ($this->processors[self::PRODUCT] as $priority => $processors) {
            foreach ($references as $reference) {
                if (isset($processors[$reference])) {
                    $nodeProcessors[] = $this->getProcessorService($processors[$reference]);
                }
            }
        }

        return $nodeProcessors;
    }

    /**
     * @param string $reference
     *
     * @return NodeProcessorInterface
     */
    public function getNodeProcessor($reference): NodeProcessorInterface
    {
        foreach ($this->processors[self::PRODUCT] as $priority => $processorReferences) {
            if (isset($processorReferences[$reference])) {
                return $this->getProcessorService($processorReferences[$reference]);
            }
        }

        throw new InvalidProductNodeProcessorReferenceException($reference);
    }
}
