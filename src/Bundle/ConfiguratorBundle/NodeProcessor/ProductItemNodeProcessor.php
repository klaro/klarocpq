<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\NodeProcessor;

use Exception;
use Klaro\Component\Cache\CacheInterface;
use Klaro\Component\Common\Exception\NotFoundProductItemException;
use Klaro\Component\Common\Exception\TooManyProductItemsException;
use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\Configuration\ConfigurationProcessorInterface;
use Klaro\Component\Configurator\Configuration\NodeProcessorInterface;
use Klaro\Component\Configurator\ConfigurationContextProviderInterface;
use Klaro\Component\Configurator\ConfItemInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorItemsLoaderDefinition;
use Klaro\Component\Configurator\Definition\FilteredItem\ConfigurationFilterTypeDefinition;
use Klaro\Component\ExpressionEvaluator\ExpressionEvaluatorInterface;
use Klaro\ConfiguratorBundle\ExpressionEvaluator\ExpressionEvaluatorFactory;
use Klaro\ConfiguratorBundle\NodeProcessor\Traits\AppliesAdditionalConfItemValuesTrait;
use Klaro\ConfiguratorBundle\NodeProcessor\Traits\CreatesConfItemWithValuesTrait;
use Klaro\ConfiguratorBundle\ProductItemFinder\ProductItemFinderInterface;
use Klaro\ConfiguratorBundle\Util\InputDataAccessor;

class ProductItemNodeProcessor implements NodeProcessorInterface
{
    use CreatesConfItemWithValuesTrait;

    /** @var ExpressionEvaluatorInterface */
    protected $evaluator;

    /** @var ProductItemFinderInterface */
    protected $productItemFinder;

    /** @var CacheInterface */
    protected $cache;

    /** @var ConfigurationNode */
    protected $configuredProduct;

    /**
     * ProductItemNodeProcessor constructor.
     * @param ExpressionEvaluatorFactory $evaluatorFactory
     * @param ProductItemFinderInterface $productItemFinder
     * @param CacheInterface             $cache
     */
    public function __construct(
        ExpressionEvaluatorFactory $evaluatorFactory,
        ProductItemFinderInterface $productItemFinder,
        CacheInterface $cache = null
    ) {
        $this->evaluator = $evaluatorFactory->getEvaluator();
        $this->productItemFinder = $productItemFinder;
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function setContext(
        ConfigurationNode $rootProduct,
        ConfigurationNode $product,
        ConfigurationProcessorInterface $configurationProcessor,
        ConfigurationContextProviderInterface $contextProvider = null
    ): void {
        $this->configuredProduct = $product;
    }

    /**
     * {@inheritdoc}
     */
    public function process(ConfigurationNode $node): void
    {
        $canHaveItems = $node->getDefinition()->canHaveItems();
        $itemRules = $node->getDefinition()->getItemRules();
        if (false === is_array($itemRules) || false === $canHaveItems) {
            return;
        }

        $loader = $itemRules['loader'] ?? null;
        if ($loader !== ConfiguratorItemsLoaderDefinition::FILTERED_ITEM_LOADER) {
            return;
        }

        $this->createProductItems($node, $itemRules['options'] ?? []);
    }

    /**
     * Helper to create product items for a given sections.
     *
     * @param ConfigurationNode $section
     * @param array             $options
     */
    private function createProductItems(ConfigurationNode $section, array $options): void
    {
        $this->findFilteredProductItems($section, $options);
    }

    /**
     * @param ConfigurationNode $section
     * @param $options
     */
    private function findFilteredProductItems(ConfigurationNode $section, $options): void
    {
        $defaultValues = $options['values'] ?? [];
        $filters = $options['filter']['filters'] ?? [];
        $filterType = $options['filter']['type'] ?? null;
        $mapping = $options['mapping'] ?? [];
        $source = $options['source'] ?? null;

        if (null === $filterType) {
            return;
        }

        $transformedFilters = [];
        if ($filterType === ConfigurationFilterTypeDefinition::ITEM_FILTER_SINGLE_TYPE) {
            $transformedFilters[] = $this->transformFilters($section, $filters);
        } else {
            foreach ($filters as $filter) {
                $transformedFilters[] = $this->transformFilters($section, $filter);
            }

            $transformedFilters = array_unique($transformedFilters, SORT_REGULAR);
        }

        try {
            $this->findMultipleFilteredProductItems($section, $source, $transformedFilters, $mapping, $defaultValues);
        } catch (Exception $e) {
            $section->addError('Product item finder error', $e->getMessage(), $transformedFilters);
        }
    }

    /**
     * @param ConfigurationNode $section
     * @param $filters
     *
     * @return array
     */
    private function transformFilters(ConfigurationNode $section, $filters): array
    {
        $transformedFilters = [];

        $context = [
            'product' => $this->configuredProduct->getItem(),
            'inputs' => new InputDataAccessor($section->getInputData()),
        ];

        // Evaluate filter values.
        foreach ($filters as $key => $value) {
            if (is_scalar($value)) {
                $transformedFilters[$key] = $this->evaluator->evaluateExpression($value, $context);
            } elseif (is_array($value)) {
                $condition = $value['condition'] ?? null;
                if (null === $condition || $this->evaluator->evaluateExpression($condition, $context) === true) {
                    $transformedFilters[$key] = $this->evaluator->evaluateExpression($value['value'], $context);
                }
            }
        }

        return $transformedFilters;
    }

    /**
     * Find products depending on multiple filter expressions one by one
     *
     * @param ConfigurationNode $section
     * @param string            $source
     * @param array             $filters
     * @param array             $mapping
     * @param array             $defaultValues
     *
     * @throws NotFoundProductItemException
     * @throws TooManyProductItemsException
     *
     * @todo Upon switching to PHP7.3+ replace end() with array_key_last()
     */
    private function findMultipleFilteredProductItems(
        ConfigurationNode $section,
        string $source,
        array $filters,
        array $mapping,
        array $defaultValues
    ): void {
        foreach ($filters as $filter) {
            try {
                if ($confItem = $this->findProductItems($section, $source, $filter, $mapping, $defaultValues)) {
                    return;
                }
            } catch (NotFoundProductItemException $e) {
                if ($filter === end($filters)) {
                    throw $e;
                }
            }
        }
    }

    /**
     * @param ConfigurationNode $section
     * @param string            $source
     * @param array             $filter
     * @param array             $mapping
     * @param array             $defaultValues
     *
     * @return ConfItemInterface
     *
     * @throws NotFoundProductItemException
     * @throws TooManyProductItemsException
     * @throws Exception
     */
    private function findProductItems(
        ConfigurationNode $section,
        string $source,
        array $filter,
        array $mapping,
        array $defaultValues
    ): ConfItemInterface {
        $productItems = $this->getProductItems($source, $filter, array_keys(array_flip($mapping)));

        $productItemsCount = count($productItems);
        if ($productItemsCount > 1) {
            throw new TooManyProductItemsException();
        }

        if ($productItemsCount < 1) {
            throw new NotFoundProductItemException();
        }

        $confItem = $this->createConfItem($section, $mapping, $productItems[0], $section->getItemData());
        $this->applyAdditionalValues($confItem, $this->evaluateConfItemValues($section, $confItem, $defaultValues));

        return $confItem;
    }

    /**
     * Helper to get product items based on filters, if cache available then use cached values.
     *
     * @param string $source
     * @param array  $filters
     * @param array  $fields
     *
     * @return mixed
     */
    private function getProductItems(string $source, array $filters, array $fields)
    {
        if ($this->cache) {
            // Find product items, cache the items that were gotten with the same filters.
            $filterCacheName = 'item://'.$source.'/'.md5(serialize($filters));

            $productItems = $this->cache->fetch($filterCacheName);
            if ($productItems === false) {
                $productItems = $this->productItemFinder->getProductItems($source, $filters, $fields);

                $this->cache->save($filterCacheName, $productItems);
            }

            return $productItems;
        }

        return $this->productItemFinder->getProductItems($source, $filters, $fields);
    }

    /**
     * @param ConfigurationNode $section
     * @param ConfItemInterface $confItem
     * @param $values
     * @param array             $extra
     *
     * @return array
     */
    private function evaluateConfItemValues(
        ConfigurationNode $section,
        ConfItemInterface $confItem,
        $values,
        $extra = []
    ): array {
        $context = [
            'confItem' => $confItem,
            'inputs' => new InputDataAccessor($section->getInputData()),
            'extra' => new InputDataAccessor($extra),
        ];

        // Fields under 'values' are evaluated if they are strings.
        foreach ($values as $key => $value) {
            if (is_string($value)) {
                $value = $this->evaluator->evaluateExpression($value, $context);
            }

            $evaluatedValues[$key] = $value;
        }

        return $evaluatedValues ?? [];
    }
}
