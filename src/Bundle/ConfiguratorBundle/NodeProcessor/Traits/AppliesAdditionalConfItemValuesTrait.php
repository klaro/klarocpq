<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\NodeProcessor\Traits;

use Klaro\Component\Configurator\ConfItemInterface;

trait AppliesAdditionalConfItemValuesTrait
{
    /**
     * @param ConfItemInterface $confItem
     * @param array             $values
     */
    private function applyAdditionalValues(ConfItemInterface $confItem, array $values): void
    {
        foreach ($values as $key => $value) {
            // Lets apply additional values only to keys that are null, lets not override previous values
            if (null !== $confItem->get($key)) {
                continue;
            }

            $confItem->set($key, $value);
        }
    }
}
