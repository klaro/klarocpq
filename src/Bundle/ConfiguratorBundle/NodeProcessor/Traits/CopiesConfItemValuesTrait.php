<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\NodeProcessor\Traits;

use Exception;
use Klaro\Component\Configurator\ConfItemInterface;

trait CopiesConfItemValuesTrait
{
    /**
     * @param ConfItemInterface $confItem
     * @param array             $fieldMapping
     * @param array             $item
     *
     * @throws Exception
     */
    private function copyConfItemValues(
        ConfItemInterface $confItem,
        array $fieldMapping,
        array $item
    ): void {
        foreach ($fieldMapping as $destination => $source) {
            try {
                // TODO: Implement a default value for source fields in YML
                $confItem->set($destination, $item[$source] ?? null);
            } catch (Exception $e) {
                throw new Exception('Unable to set "' . $destination . '" value from original field "' . $source . '"');
            }
        }
    }
}
