<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\NodeProcessor\Traits;

use Exception;
use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\ConfItemInterface;

trait CreatesConfItemWithValuesTrait
{
    use AppliesAdditionalConfItemValuesTrait;
    use CopiesConfItemValuesTrait;

    /**
     * @param ConfigurationNode $node
     * @param array             $fieldMapping
     * @param array             $item
     *
     * @param array             $additionalValues
     *
     * @return ConfItemInterface
     *
     * @throws Exception
     */
    private function createConfItem(
        ConfigurationNode $node,
        array $fieldMapping,
        array $item,
        array $additionalValues = []
    ): ConfItemInterface {
        $confItem = $node->createConfItem();

        $this->copyConfItemValues($confItem, $fieldMapping, $item);
        $this->applyAdditionalValues($confItem, $additionalValues);

        return $confItem;
    }
}
