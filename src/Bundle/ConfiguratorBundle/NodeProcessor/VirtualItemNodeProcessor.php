<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\NodeProcessor;

use Exception;
use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\Configuration\ConfigurationProcessorInterface;
use Klaro\Component\Configurator\Configuration\NodeProcessorInterface;
use Klaro\Component\Configurator\ConfigurationContextProviderInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorItemsLoaderDefinition;
use Klaro\Component\ExpressionEvaluator\ExpressionEvaluatorInterface;
use Klaro\ConfiguratorBundle\ExpressionEvaluator\ExpressionEvaluatorFactory;
use Klaro\ConfiguratorBundle\NodeProcessor\Traits\CreatesConfItemWithValuesTrait;
use Klaro\ConfiguratorBundle\Util\InputDataAccessor;

class VirtualItemNodeProcessor implements NodeProcessorInterface
{
    use CreatesConfItemWithValuesTrait;

    /** @var ExpressionEvaluatorInterface */
    protected $evaluator;

    /**
     * @param ExpressionEvaluatorFactory $evaluatorFactory
     */
    public function __construct(ExpressionEvaluatorFactory $evaluatorFactory)
    {
        $this->evaluator = $evaluatorFactory->getEvaluator();
    }

    /**
     * {@inheritdoc}
     */
    public function setContext(
        ConfigurationNode $rootProduct,
        ConfigurationNode $product,
        ConfigurationProcessorInterface $configurationProcessor,
        ConfigurationContextProviderInterface $contextProvider = null
    ): void {
        // No use for context here
    }

    /**
     * @param ConfigurationNode $node
     *
     * @throws Exception
     */
    public function process(ConfigurationNode $node): void
    {
        $canHaveItems = $node->getDefinition()->canHaveItems();
        $itemRules = $node->getDefinition()->getItemRules();
        if (false === is_array($itemRules) || false === $canHaveItems) {
            return;
        }

        $loader = $itemRules['loader'] ?? null;
        if ($loader !== ConfiguratorItemsLoaderDefinition::VIRTUAL_ITEM_LOADER) {
            return;
        }

        $sections = $node->getSections();
        if (empty($sections)) {
            $sections[] = $node;
        }

        $options = $itemRules['options'];
        foreach ($sections as $section) {
            $this->createConfItem(
                $section,
                $options['mapping'] ?? [],
                $this->evaluateSourceString($section, $options['source'] ?? null),
                $options['values'] ?? []
            );
        }
    }

    /**
     * Evaluates source, given source or expression should return array
     *
     * @param ConfigurationNode $node
     * @param null|array|string $source
     *
     * @return array
     */
    private function evaluateSourceString(ConfigurationNode $node, $source): array
    {
        $context = [
            'inputs' => new InputDataAccessor($node->getInputData()),
        ];

        if (is_array($source)) {
            return $source;
        }

        if (false === is_string($source)) {
            return $node->getInputData();
        }

        $evaluatedSource = $this->evaluator->evaluateExpression($source, $context);

        return is_array($evaluatedSource) ? $evaluatedSource : [];
    }
}
