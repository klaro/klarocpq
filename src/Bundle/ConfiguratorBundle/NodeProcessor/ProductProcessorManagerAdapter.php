<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\NodeProcessor;

use Klaro\Component\Configurator\Configuration\NodeProcessorInterface;
use Klaro\Component\Configurator\Configuration\NodeProcessorManagerInterface;

class ProductProcessorManagerAdapter implements NodeProcessorManagerInterface
{
    /** @var NodeProcessorManager */
    protected $nodeProcessorManager;

    /**
     * NodeProcessorManagerAdapter constructor.
     * @param NodeProcessorManager $nodeProcessorManager
     */
    public function __construct(NodeProcessorManager $nodeProcessorManager)
    {
        $this->nodeProcessorManager = $nodeProcessorManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getNodeProcessor($reference)
    {
        return $this->nodeProcessorManager->getNodeProcessor($reference);
    }

    /**
     * @param $references
     *
     * @return NodeProcessorInterface[]
     */
    public function getNodeProcessors($references)
    {
        return $this->nodeProcessorManager->getNodeProcessors($references);
    }
}
