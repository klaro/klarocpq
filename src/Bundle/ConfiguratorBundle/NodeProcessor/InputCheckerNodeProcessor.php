<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\NodeProcessor;

use League\JsonGuard\Validator;
use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\Configuration\ConfigurationProcessorInterface;
use Klaro\Component\Configurator\Configuration\NodeProcessorInterface;
use Klaro\Component\Configurator\ConfigurationContextProviderInterface;
use Klaro\Component\Util\ArrayUtil;

class InputCheckerNodeProcessor implements NodeProcessorInterface
{
    /** @var ConfigurationProcessorInterface */
    protected $configurationProcessor;

    /**
     * {@inheritdoc}
     */
    public function setContext(
        ConfigurationNode $rootProduct,
        ConfigurationNode $product,
        ConfigurationProcessorInterface $configurationProcessor,
        ConfigurationContextProviderInterface $contextProvider = null
    ) {
        $this->configurationProcessor = $configurationProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public function process(ConfigurationNode $node)
    {
        $requiredInputs = (array) $node->getDefinition()->getRequiredInputs();

        if (count($requiredInputs) > 0) {
            $inputData = $node->getInputData();

            $schema = (object) [
                'type' => 'object',
                'properties' => (object) $requiredInputs,
                'required' => array_keys($requiredInputs),
            ];

            $validator = new Validator((object) ArrayUtil::arrayToObject($inputData), $schema);

            if ($validator->fails()) {
                foreach ($validator->errors() as $error) {
                    $node->addError(
                        'Input validation failed (path "'.$error->getDataPath().'")',
                        $error->getMessage(),
                        $inputData
                    );
                }

                // Prevent other processors from running for this product:
                // $this->configurationProcessor->stopProcessing();
            }
        }
    }
}
