<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\NodeProcessor;

use Klaro\Component\Configurator\Configuration\NodeProcessorInterface;
use Klaro\Component\Configurator\Configuration\NodeProcessorManagerInterface;

class GeneralNodeProcessorManagerAdapter implements NodeProcessorManagerInterface
{
    /** @var NodeProcessorManager */
    protected $nodeProcessorManager;

    /**
     * GeneralNodeProcessorManagerAdapter constructor.
     *
     * @param NodeProcessorManager $nodeProcessorManager
     */
    public function __construct(NodeProcessorManager $nodeProcessorManager)
    {
        $this->nodeProcessorManager = $nodeProcessorManager;
    }


    /**
     * @param string $reference
     *
     * @return NodeProcessorInterface
     */
    public function getNodeProcessor($reference): ?NodeProcessorInterface
    {
        return $this->nodeProcessorManager->getNodeProcessor($reference);
    }

    /**
     * @param string $references
     *
     * @return NodeProcessorInterface[]
     */
    public function getNodeProcessors($references): array
    {
        return $this->nodeProcessorManager->getGeneralProcessors();
    }
}
