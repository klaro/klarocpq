<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\ConfiguratorAdapter;

use Closure;
use Exception;
use Klaro\Component\ConfigurationAdapter\ArrayTreeNode;
use Klaro\Component\ConfigurationAdapter\ConfigurationAdapterInterface;
use Klaro\Component\ConfigurationAdapter\ObjectTreeNode;
use Klaro\Component\ConfigurationAdapter\ProductTreeNode;
use Klaro\Component\ConfigurationAdapter\TreeBuilder;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;
use Klaro\Component\Configurator\Definition\ConfiguratorProductDefinitionNode;
use Klaro\Component\Configurator\Definition\ConfiguratorProductListDefinitionNode;
use Klaro\Component\ExpressionEvaluator\ExpressionEvaluator;
use Klaro\Component\FormData\ProductLineDataNode;
use Klaro\Component\FormData\ProductPhaseDataNode;
use Klaro\Component\FormData\ProductPhaseListDataNode;
use Klaro\ConfiguratorBundle\Exception\FailedConditionException;
use Klaro\ConfiguratorBundle\ExpressionEvaluator\ExpressionEvaluatorFactory;
use Klaro\ConfiguratorBundle\Util\InputDataAccessor;
use stdClass;
use Symfony\Component\ExpressionLanguage\ExpressionFunction;

class ConfiguratorAdapter implements ConfigurationAdapterInterface
{
    /**
     * @var ExpressionEvaluator
     */
    protected $evaluator;

    /**
     * @param ExpressionEvaluatorFactory $evaluatorFactory
     */
    public function __construct(ExpressionEvaluatorFactory $evaluatorFactory)
    {
        $this->evaluator = $evaluatorFactory->getEvaluator();

        // Register additional functions that do not exist in our default evaluator
        $this->evaluator->addFunction(ExpressionFunction::fromPhp('count'));
    }

    /**
     * @param ConfiguratorDefinitionNode $configuration
     * @param ProductLineDataNode        $productLineDataNode
     *
     * @return bool|stdClass
     *
     * @throws Exception
     */
    public function process(ConfiguratorDefinitionNode $configuration, ProductLineDataNode $productLineDataNode)
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root();

        $this->addSectionInputData($configuration->getInputs(), $rootNode, $productLineDataNode);

        $this->processSections(
            $configuration->getSections(),
            $rootNode,
            Closure::fromCallable([$this, 'processRootSections']),
            $productLineDataNode
        );

        return $treeBuilder->buildTree();
    }

    /**
     * @param array                     $sections
     * @param $rootNode
     * @param Closure                   $callback
     * @param ProductLineDataNode       $productLineData
     * @param ProductPhaseDataNode|null $productPhaseData
     *
     * @throws Exception
     */
    private function processSections(
        array $sections,
        $rootNode,
        Closure $callback,
        ProductLineDataNode $productLineData,
        ProductPhaseDataNode $productPhaseData = null
    ): void {
        foreach ($sections as $sectionName => $section) {
            try {
                $callback($section, $sectionName, $rootNode, $productLineData, $productPhaseData);
            } catch (Exception $exception) {
                if ($exception instanceof FailedConditionException) {
                    continue;
                }

                throw $exception;
            }
        }
    }

    /**
     * @param ConfiguratorDefinitionNode $section
     * @param string                     $sectionName
     * @param $rootNode
     * @param ProductLineDataNode        $productLineDataNode
     *
     * @throws FailedConditionException
     */
    private function processRootSections(
        ConfiguratorDefinitionNode $section,
        string $sectionName,
        $rootNode,
        ProductLineDataNode $productLineDataNode
    ): void {
        if ($section->getType() === ConfiguratorDefinitionNode::PRODUCT_LIST) {
            /** @var ConfiguratorProductListDefinitionNode $section */
            $this->createProductListSection($section, $sectionName, $rootNode, $productLineDataNode);

            return;
        }

        $this->createSingleSection($section, $sectionName, $rootNode, $productLineDataNode);
    }

    /**
     * @param ConfiguratorProductListDefinitionNode $section
     * @param string                                $sectionName
     * @param ObjectTreeNode                        $rootNode
     * @param ProductLineDataNode                   $productLineData
     *
     * @throws FailedConditionException
     * @throws Exception
     */
    private function createProductListSection(
        ConfiguratorProductListDefinitionNode $section,
        string $sectionName,
        ObjectTreeNode $rootNode,
        ProductLineDataNode $productLineData
    ): void {
        if ($section->getType() !== ConfiguratorDefinitionNode::PRODUCT_LIST) {
            return;
        }

        $this->ensureConditionIsTrue($sectionName, $rootNode, $section->getCondition(), $productLineData);

        /** @var ProductPhaseListDataNode $productLines */
        $productLines = $this->evaluateValue($section->getSource(), $productLineData);
        if (false === ($productLines instanceof ProductPhaseListDataNode)) {
            return;
        }

        $productListNode = $rootNode->productListSection($sectionName)->productList()->products();

        /** @var ProductPhaseDataNode $productLine */
        foreach ($productLines as $productLine) {
            $this->processSections(
                $section->getSections(),
                $productListNode,
                Closure::fromCallable([$this, 'createSingleSection']),
                $productLineData,
                $productLine
            );
        }
    }

    /**
     * @param ConfiguratorDefinitionNode                   $section
     * @param string                                       $sectionName
     * @param ArrayTreeNode|ObjectTreeNode|ProductTreeNode $rootNode
     * @param ProductLineDataNode                          $productLineData
     * @param ProductPhaseDataNode|null                    $productPhaseData
     *
     * @throws FailedConditionException
     * @throws Exception
     */
    private function createSingleSection(
        ConfiguratorDefinitionNode $section,
        string $sectionName,
        $rootNode,
        ProductLineDataNode $productLineData,
        ProductPhaseDataNode $productPhaseData = null
    ): void {
        if (false === in_array(
            $section->getType(),
            [ConfiguratorDefinitionNode::SECTION, ConfiguratorDefinitionNode::PRODUCT],
            true
        )) {
            return;
        }

        $this->ensureConditionIsTrue(
            $sectionName,
            $rootNode,
            $section->getCondition(),
            $productLineData,
            $productPhaseData
        );

        $sectionRoot = $this->createSectionRootNode(
            $rootNode,
            $sectionName,
            $this->getSectionTitle($section->getTitle(), $productLineData, $productPhaseData)
        );

        $this->addSectionInputData($section->getInputs(), $sectionRoot, $productLineData, $productPhaseData);

        if ($section instanceof ConfiguratorProductDefinitionNode && $section->getCollectionType()) {
            $this->createProductCollection($section, $sectionRoot, $productLineData, $productPhaseData);

            return;
        }

        $this->processSections(
            $section->getSections(),
            $sectionRoot,
            Closure::fromCallable([$this, 'createSingleSection']),
            $productLineData,
            $productPhaseData
        );
    }

    /**
     * @param string|null               $sectionName
     * @param $rootNode
     * @param $condition
     * @param ProductLineDataNode       $productLineData
     * @param ProductPhaseDataNode|null $productPhaseData
     *
     * @param array                     $context
     *
     * @throws FailedConditionException
     */
    private function ensureConditionIsTrue(
        ?string $sectionName,
        $rootNode,
        $condition,
        ProductLineDataNode $productLineData,
        ProductPhaseDataNode $productPhaseData = null,
        array $context = []
    ): void {
        if (null === $condition) {
            return;
        }

        if ($this->evaluateValue($condition, $productLineData, $productPhaseData, $context)) {
            return;
        }

        if ($rootNode instanceof ObjectTreeNode) {
            $rootNode->section($sectionName)->boolean(false);
        }

        throw new FailedConditionException(sprintf('Failed to evaluate condition \'%s\' to be true.', $condition));
    }

    /**
     * @param null|string               $value
     * @param ProductLineDataNode       $productLineData
     * @param ProductPhaseDataNode|null $productPhaseData
     * @param array                     $context
     *
     * @return mixed|string|null
     */
    private function evaluateValue(
        ?string $value,
        ProductLineDataNode $productLineData,
        ProductPhaseDataNode $productPhaseData = null,
        array $context = []
    ) {
        return $this->evaluator->evaluateExpression(
            $value,
            $this->getEvaluatorContext($productLineData, $productPhaseData, $context)
        );
    }

    /**
     * @param ArrayTreeNode|ObjectTreeNode $rootNode
     * @param string                       $sectionName
     * @param null|string                  $title
     *
     * @return ObjectTreeNode|ProductTreeNode
     */
    private function createSectionRootNode($rootNode, string $sectionName, ?string $title = null)
    {
        if ($rootNode instanceof ArrayTreeNode) {
            return $rootNode->product($sectionName)->title($title);
        }

        /** @var ObjectTreeNode $rootNode */

        return $rootNode->section($sectionName)->object();
    }

    /**
     * @param string|null               $title
     * @param ProductLineDataNode       $productLineData
     * @param ProductPhaseDataNode|null $productPhaseData
     * @param array                     $context
     *
     * @return string|null
     */
    private function getSectionTitle(
        ?string $title,
        ProductLineDataNode $productLineData,
        ProductPhaseDataNode $productPhaseData = null,
        array $context = []
    ): ?string {
        if ((null !== $title) && $title = $this->evaluateValue($title, $productLineData, $productPhaseData, $context)) {
            return $title;
        }

        if ($productPhaseData) {
            return $productPhaseData->getTitle();
        }

        return $productLineData->getTitle();
    }

    /**
     * @param array                     $inputs
     * @param $rootNode
     * @param ProductLineDataNode       $productLineData
     * @param ProductPhaseDataNode|null $productPhaseData
     * @param array                     $context
     */
    private function addSectionInputData(
        array $inputs,
        $rootNode,
        ProductLineDataNode $productLineData,
        ProductPhaseDataNode $productPhaseData = null,
        array $context = []
    ): void {
        if (false === ($rootNode instanceof ObjectTreeNode)) {
            return;
        }

        /** @var ObjectTreeNode $rootNode */
        foreach ($inputs as $key => $value) {
            if ($evaluatedValue = $this->evaluateValue($value, $productLineData, $productPhaseData, $context)) {
                $rootNode->addData($key, $evaluatedValue);
            }
        }
    }

    /**
     * @param ConfiguratorProductDefinitionNode $section
     * @param $productNode
     * @param ProductLineDataNode               $productLineData
     * @param ProductPhaseDataNode|null         $productPhaseData
     */
    private function createProductCollection(
        ConfiguratorProductDefinitionNode $section,
        $productNode,
        ProductLineDataNode $productLineData,
        ProductPhaseDataNode $productPhaseData = null
    ): void {
        if (false === ($section instanceof ConfiguratorProductDefinitionNode)) {
            return;
        }

        $items = $this->evaluateValue($section->getSource(), $productLineData, $productPhaseData);
        if (false === is_array($items)) {
            return;
        }

        $collectionType = $section->getCollectionType();
        $sections = $section->getSections();

        /**
         * In cases where product doesn't define sections and creates items as additional items,
         * transform current section into section array
         */
        if (empty($sections)) {
            $sections[$section->getId()] = $section;
        }

        foreach ($sections as $childSectionName => $childSection) {
            $collectionNode = $this->createCollectionSection($collectionType, $childSectionName, $productNode);

            foreach ($items as $item) {
                try {
                    if (false === is_array($item)) {
                        continue;
                    }

                    $evaluatorContext = ['item' => new InputDataAccessor($item)];

                    $this->ensureConditionIsTrue(
                        $childSectionName,
                        $collectionNode,
                        $childSection->getCondition(),
                        $productLineData,
                        $productPhaseData,
                        $evaluatorContext
                    );

                    // In this specific case item may have been set by expression language
                    $title = $this->evaluateValue(
                        $childSection->getTitle(),
                        $productLineData,
                        $productPhaseData,
                        $evaluatorContext
                    );

                    $collectionObjectItem = $collectionNode->object()->title($title);

                    $this->addSectionInputData(
                        $childSection->getInputs(),
                        $collectionObjectItem,
                        $productLineData,
                        $productPhaseData,
                        $evaluatorContext
                    );
                } catch (FailedConditionException $e) {
                    continue;
                }
            }
        }
    }

    /**
     * @param ProductLineDataNode       $productLineData
     * @param ProductPhaseDataNode|null $productPhaseData
     * @param array                     $context
     *
     * @return array
     */
    private function getEvaluatorContext(
        ProductLineDataNode $productLineData,
        ProductPhaseDataNode $productPhaseData = null,
        array $context = []
    ): array {
        $context = array_merge($context, ['productLine' => $productLineData]);
        if ($productPhaseData) {
            return array_merge($context, ['product' => $productPhaseData]);
        }

        return $context;
    }

    /**
     * @param string                         $type
     * @param string                         $id
     * @param ObjectTreeNode|ProductTreeNode $rootNode
     *
     * @return ArrayTreeNode
     */
    private function createCollectionSection(
        string $type,
        string $id,
        $rootNode
    ): ArrayTreeNode {
        if ($type === 'additional') {
            return $rootNode->additional($id)->array();
        }

        return $rootNode->section($id)->array();
    }
}
