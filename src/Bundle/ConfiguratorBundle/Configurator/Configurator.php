<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\Configurator;

use Klaro\Component\Configurator\Configuration\ConfigurationBuilder;
use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\Configuration\ExportedConfigurationBuilder;
use Klaro\Component\Configurator\Configuration\ProductConfigurationProcessor;
use Klaro\Component\Configurator\ConfigurationContextProviderInterface;
use Klaro\Component\Configurator\ConfiguratorInterface;
use Klaro\Component\Configurator\ConfiguratorManagerInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;
use Klaro\ConfiguratorBundle\ConfigurationContext\ConfigurationContextInitializerAdapter;
use Klaro\ConfiguratorBundle\ConfigurationContext\ConfigurationContextInitializerChain;
use Klaro\ConfiguratorBundle\ConfigurationContext\ConfigurationContextInitializerInterface;
use Klaro\ConfiguratorBundle\ConfigurationContext\ConfigurationContextManager;
use Klaro\ConfiguratorBundle\ConfigurationContext\ProductConfigurationContextInitializer;
use Klaro\ConfiguratorBundle\NodeProcessor\GeneralNodeProcessorManagerAdapter;
use Klaro\ConfiguratorBundle\NodeProcessor\NodeProcessorManager;
use Klaro\ConfiguratorBundle\NodeProcessor\ProductProcessorManagerAdapter;

class Configurator implements ConfiguratorInterface
{
    /** @var ConfiguratorManagerInterface */
    protected $configuratorManager;

    /** @var NodeProcessorManager */
    protected $nodeProcessorManager;

    /** @var ConfigurationContextManager */
    protected $contextManager;

    /** @var ConfigurationContextInitializerInterface */
    protected $contextInitializer;

    /**
     * Configurator constructor.
     * @param ConfiguratorManagerInterface $configuratorManager
     * @param NodeProcessorManager         $nodeProcessorManager
     * @param ConfigurationContextManager  $contextManager
     */
    public function __construct(
        ConfiguratorManagerInterface $configuratorManager,
        NodeProcessorManager $nodeProcessorManager,
        ConfigurationContextManager $contextManager
    ) {
        $this->configuratorManager = $configuratorManager;
        $this->nodeProcessorManager = $nodeProcessorManager;
        $this->contextManager = $contextManager;
    }

    /**
     * {@inheritdoc}
     */
    public function processConfigurationByRef(
        $ref,
        $data
    ) {
        $definition = $this->configuratorManager->getConfiguratorConfig($ref);

        return $this->processConfiguration(
            $this->configuratorManager->getConfiguratorStructure($definition),
            $data
        );
    }

    /**
     * {@inheritdoc}
     */
    public function processConfiguration(
        ConfiguratorDefinitionNode $configuration,
        $data
    ) {
        $configuration = ConfigurationBuilder::create($configuration)->build($data);

        $contextProvider = $this->getContextProvider($configuration);

        // Process general processors first (input validation, product items).
        $preProcessorManager = new GeneralNodeProcessorManagerAdapter($this->nodeProcessorManager);
        ProductConfigurationProcessor::create($configuration, $preProcessorManager, $contextProvider)->process($configuration);

        // Then run product specific processors.
        $productProcessorManager = new ProductProcessorManagerAdapter($this->nodeProcessorManager);
        ProductConfigurationProcessor::create($configuration, $productProcessorManager, $contextProvider)->process($configuration);

        return $configuration;
    }

    /**
     * @return ConfigurationContextInitializerInterface
     */
    public function getContextInitializer()
    {
        return $this->contextInitializer;
    }

    /**
     * @param ConfigurationContextInitializerInterface $contextInitializer
     */
    public function setContextInitializer($contextInitializer)
    {
        $this->contextInitializer = $contextInitializer;
    }

    /**
     * {@inheritdoc}
     */
    public function importConfiguration($exportedConfiguration)
    {
        return ExportedConfigurationBuilder::create($exportedConfiguration)->build();
    }

    /**
     * @return array
     */
    public function getAllConfiguratorRefs()
    {
        return $this->configuratorManager->getAllConfiguratorRefs();
    }

    /**
     * @param ConfigurationNode $configuration
     *
     * @return ConfigurationContextProviderInterface
     */
    private function getContextProvider(ConfigurationNode $configuration)
    {
        $initializer = new ProductConfigurationContextInitializer($configuration);

        if ($this->contextInitializer instanceof ConfigurationContextInitializerInterface) {
            $initializer = new ConfigurationContextInitializerChain([
                $this->contextInitializer,
                $initializer,
            ]);
        }

        $contextProvider = new ConfigurationContextInitializerAdapter(
            $this->contextManager,
            $initializer
        );

        return $contextProvider;
    }
}
