<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\Configurator;

use Klaro\Component\Configurator\ConfigLoaderInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;

class ConfigArrayAdapter
{
    /** @var ConfigLoaderInterface */
    protected $configurationLoader;

    /** @var string */
    protected $ref;

    /** @var array */
    protected $products;

    /**
     * ConfigArrayAdapter constructor.
     * @param ConfigLoaderInterface $configurationLoader
     * @param string                $ref
     */
    public function __construct(ConfigLoaderInterface $configurationLoader, $ref)
    {
        $this->configurationLoader = $configurationLoader;
        $this->ref = $ref;
    }

    public function toArray()
    {
        $this->products = [];

        $this->addProduct($this->ref);

        return $this->products;
    }

    /**
     * @param $ref
     */
    private function addProduct($ref)
    {
        $this->products[$ref] = $this->configurationLoader->getConfig($ref);

        $this->processSections($ref, $this->products[$ref]['ref']);
    }

    /**
     * @param $config
     */
    private function processSection($productRef, $config)
    {
        if ($config['type'] === ConfiguratorDefinitionNode::PRODUCT_LIST) {
            foreach ($config['ref'] as $ref) {
                $this->addProduct($ref);
            }
        } elseif ($config['type'] === ConfiguratorDefinitionNode::PRODUCT) {
            $this->addProduct($config['ref']);
        } elseif (isset($config['ref'])) {
            $this->processSections($productRef, $config['ref']);
        }
    }

    /**
     * @param $productRef
     * @param $sectionRefs
     */
    private function processSections($productRef, $sectionRefs)
    {
        foreach ($sectionRefs as $sectionRef) {
            $this->processSection($productRef, $this->products[$productRef]['sections'][$sectionRef]);
        }
    }
}
