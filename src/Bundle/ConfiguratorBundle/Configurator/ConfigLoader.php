<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\Configurator;

use Klaro\Component\Configurator\ArrayConfigLoader;
use Klaro\Component\Configurator\ConfiguratorManagerInterface;

/**
 * Class ConfigLoader
 * @package Klaro\ConfiguratorBundle\Configurator
 */
class ConfigLoader extends ArrayConfigLoader
{
    /**
     * ConfigLoader constructor.
     * @param array $products
     */
    public function __construct(array $products)
    {
        parent::__construct($products, ConfiguratorManagerInterface::VERSION_LATEST);
    }
}
