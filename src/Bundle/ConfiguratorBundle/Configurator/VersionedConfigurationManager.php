<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\Configurator;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Klaro\Component\Configurator\ArrayConfigLoader;
use Klaro\Component\Configurator\ConfigLoaderInterface;
use Klaro\Component\Configurator\ConfiguratorManagerInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorConfig;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionFactory;
use Klaro\ConfiguratorBundle\Model\VersionedConfig;

class VersionedConfigurationManager implements ConfiguratorManagerInterface
{
    /** @var EntityManager */
    protected $em;

    /** @var ConfigLoaderInterface */
    protected $configurationLoader;

    /** @var ConfigLoaderInterface[] */
    protected $loaders;

    /**
     * VersionedConfigurationManager constructor.
     * @param ManagerRegistry       $managerRegistry
     * @param ConfigLoaderInterface $configurationLoader
     */
    public function __construct(ManagerRegistry $managerRegistry, ConfigLoaderInterface $configurationLoader)
    {
        $this->em = $managerRegistry->getManagerForClass(VersionedConfig::class);
        $this->configurationLoader = $configurationLoader;
        $this->loaders = [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAllConfiguratorRefs()
    {
        return $this->configurationLoader->getAllConfigRefs();
    }

    /**
     * {@inheritdoc}
     */
    public function getConfiguratorConfig($ref, $version = self::VERSION_LATEST, $fetchMode = self::FETCH_CACHED)
    {
        $loader = $this->getLoader($ref, $version, $fetchMode);

        return ConfiguratorDefinitionFactory::create($loader)->createConfiguratorConfig($ref);
    }

    /**
     * @param ConfiguratorConfig $configuratorConfig
     *
     * @return ConfiguratorConfig|ConfiguratorDefinitionNode
     */
    public function getConfiguratorStructure(ConfiguratorConfig $configuratorConfig)
    {
        $loader = $this->getLoader($configuratorConfig->getId(), $configuratorConfig->getVersion());

        return ConfiguratorDefinitionFactory::create($loader)->createStructure($configuratorConfig);
    }

    /**
     * @param $version
     *
     * @return ConfigLoaderInterface
     */
    private function getLoader($ref, $version = self::VERSION_LATEST, $fetchMode = self::FETCH_CACHED)
    {
        $cacheId = $ref.'/'.$version;

        if ($fetchMode !== self::FETCH_CACHED || $version === ConfiguratorManagerInterface::VERSION_LATEST || !isset($this->loaders[$cacheId])) {
            $versionedConfig = $this->findOrCreateVersionedConfig($ref, $version, $fetchMode);

            $cacheId = $ref.'/'.$versionedConfig->getVersion();

            $this->loaders[$cacheId] = new ArrayConfigLoader(
                $versionedConfig->getData(),
                $versionedConfig->getVersion()
            );
        }

        return $this->loaders[$cacheId];
    }

    /**
     * @param $ref
     * @param int    $version
     * @param string $fetchMode
     *
     * @return null|object|VersionedConfig
     */
    private function findOrCreateVersionedConfig($ref, $version = self::VERSION_LATEST, $fetchMode = self::FETCH_CACHED)
    {
        $versionedConfig = ($fetchMode === self::FETCH_CACHED) ? $this->getVersionedConfig($ref, $version) : null;

        if (!($versionedConfig instanceof VersionedConfig)) {
            // Get original config
            $adapter = new ConfigArrayAdapter($this->configurationLoader, $ref);
            $config = $adapter->toArray();

            // Compare checksum to see if there any changes to previous versions
            $checksum = md5(serialize($config));

            $versionedConfig = $this->getVersionedConfig($ref, $checksum);

            if (!($versionedConfig instanceof VersionedConfig)) {
                $versionedConfig = new VersionedConfig();
                $versionedConfig->setName($ref);
                $versionedConfig->setVersion($checksum);
                $versionedConfig->setUpdatedAt(new \DateTime());
                $versionedConfig->setData($config);

                $this->em->persist($versionedConfig);
                $this->em->flush();
            }
        }

        return $versionedConfig;
    }

    /**
     * @param $ref
     * @param $version
     *
     * @return null|object
     */
    private function getVersionedConfig($ref, $version)
    {
        /** @var EntityRepository $repository */
        $repository = $this->em->getRepository(VersionedConfig::class);

        return $repository->findOneBy([
            'name' => $ref,
            'version' => $version,
        ]);
    }
}
