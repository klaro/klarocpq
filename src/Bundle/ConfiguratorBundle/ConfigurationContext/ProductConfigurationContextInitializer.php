<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\ConfigurationContext;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\ConfigurationContextInterface;

class ProductConfigurationContextInitializer implements ConfigurationContextInitializerInterface
{
    /** @var ConfigurationNode */
    protected $configuration;

    /**
     * ConfigurationNodeInitializer constructor.
     * @param ConfigurationNode $configuration
     */
    public function __construct(ConfigurationNode $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param ConfigurationContextInterface $context
     */
    public function initialize(ConfigurationContextInterface $context)
    {
        if ($context instanceof ProductAwareConfigurationContextInterface) {
            $context->setRootProduct($this->configuration);
        }
    }
}
