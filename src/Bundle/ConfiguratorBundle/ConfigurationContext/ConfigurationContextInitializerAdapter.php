<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\ConfigurationContext;

use Klaro\Component\Configurator\ConfigurationContextProviderInterface;

class ConfigurationContextInitializerAdapter implements ConfigurationContextProviderInterface
{
    /** @var ConfigurationContextManager */
    protected $contextManager;

    /** @var  ConfigurationContextInitializerInterface */
    protected $initializer;

    protected $loadedContext;

    /**
     * ConfigurationContextInitializerAdapter constructor.
     * @param ConfigurationContextManager              $contextManager
     * @param ConfigurationContextInitializerInterface $initializer
     */
    public function __construct(ConfigurationContextManager $contextManager, ConfigurationContextInitializerInterface $initializer = null)
    {
        $this->contextManager = $contextManager;
        $this->initializer = $initializer;
        $this->loadedContext = [];
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigurationContext($ref)
    {
        if (!isset($this->loadedContext[$ref])) {
            $context = $this->contextManager->getContext($ref);

            if ($this->initializer instanceof ConfigurationContextInitializerInterface) {
                $this->initializer->initialize($context);
            }

            $context->initialize();

            $this->loadedContext[$ref] = $context;
        }

        return $this->loadedContext[$ref];
    }
}
