<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\ConfigurationContext;

use Klaro\Component\Configurator\ConfigurationContextInterface;

class ConfigurationContextManager
{
    /** @var ConfigurationContextInterface[] */
    protected $contextServices;

    /**
     * ConfigurationContextManager constructor.
     */
    public function __construct()
    {
        $this->contextServices = [];
    }

    /**
     * @param $ref
     * @param ConfigurationContextInterface $context
     */
    public function addConfigurationContext($ref, ConfigurationContextInterface $context)
    {
        $this->contextServices[$ref] = $context;
    }

    /**
     * @param $ref
     *
     * @return ConfigurationContextInterface
     */
    public function getContext($ref)
    {
        return $this->contextServices[$ref];
    }
}
