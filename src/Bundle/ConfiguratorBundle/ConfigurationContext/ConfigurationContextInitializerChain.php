<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\ConfigurationContext;

use Klaro\Component\Configurator\ConfigurationContextInterface;

class ConfigurationContextInitializerChain implements ConfigurationContextInitializerInterface
{
    /** @var ConfigurationContextInitializerInterface[] */
    protected $initializers;

    /**
     * ConfigurationContextInitializerChain constructor.
     * @param ConfigurationContextInitializerInterface[] $initializers
     */
    public function __construct(array $initializers = [])
    {
        $this->initializers = $initializers;
    }

    /**
     * @param ConfigurationContextInitializerInterface[] $initializers
     */
    public function setInitializers($initializers)
    {
        $this->initializers = $initializers;
    }

    /**
     * @param ConfigurationContextInterface $context
     */
    public function initialize(ConfigurationContextInterface $context)
    {
        foreach ($this->initializers as $initializer) {
            if ($initializer instanceof ConfigurationContextInitializerInterface) {
                $initializer->initialize($context);
            }
        }
    }
}
