<?php

namespace Klaro\ConfiguratorBundle\Exception;

use Exception;
use RuntimeException;

/**
 * Exception class thrown when registered node processor was not found in container.
 */
class NodeProcessorNotFoundException extends RuntimeException
{
    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        if (null === $message) {
            $message = 'Node processor could not be found.';
        } else {
            $message = sprintf('Node processor "%s" could not be found.', $message);
        }

        parent::__construct($message, $code, $previous);
    }
}
