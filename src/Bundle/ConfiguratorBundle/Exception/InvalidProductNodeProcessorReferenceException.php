<?php

namespace Klaro\ConfiguratorBundle\Exception;

use Exception;
use RuntimeException;

/**
 * Exception class thrown when registered product node processor reference is invalid.
 */
class InvalidProductNodeProcessorReferenceException extends RuntimeException
{
    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        if (null === $message) {
            $message = 'Invalid product node processor reference.';
        } else {
            $message = sprintf('Reference "%s" is invalid.', $message);
        }

        parent::__construct($message, $code, $previous);
    }
}
