<?php

namespace Klaro\ConfiguratorBundle\Exception;

use Exception;
use RuntimeException;

/**
 * Exception class thrown when registered node processor type is invalid.
 */
class InvalidNodeProcessorTypeException extends RuntimeException
{
    public function __construct($message = null, $code = 0, Exception $previous = null)
    {
        if (null === $message) {
            $message = 'Invalid node processor type.';
        } else {
            $message = sprintf('Node processor of type "%s" is invalid.', $message);
        }

        parent::__construct($message, $code, $previous);
    }
}
