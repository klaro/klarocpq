# Configuration Input

Configuration process

![](images/configurator_process.png)

Configration input should follow the specification below. Default configuration input is an empty object.

```json
configration = {
  "title": string | null,
  "data": input_data | null,
  "sections": sections | null
}
```

Input data is an object or it can be omitted. The object key defines the input variable name.

```json
input_data = {
  [string]: number | string | boolean | array | object
}
```

Sections is an object with keys matching the names of the sections in the configuration config. 

```json
sections = {
  [string]: single_section | multiple_sections | product_list
}
```

```json
single_section = number | boolean | configration
```

Sections can be repeated by giving the input as an array.

```json
multiple_sections = [single_section]
```

If the section is a product list, then you must give a product list object:

```json
product_list = {
  "products": [product]
}
```

Product section is similar to a regular section but the product name must be given in the `ref` key.

```json
product = {
  "ref": string
} + configration
```

