# Product Data

##### config

A product config is defined as an object:

```json
config = {
  "title": string,
  "ref": array,
  "sections": sections,
  "inputs": inputs | null,
  "required": required_inputs | null,
  "items": items | null,
  "defaults": item_defaults | null
}
```

The `ref` key above refers to section ids defined under `sections` and should contain only the ids of the sections that are directly under the main product.

##### inputs, required

Inputs is an object with field names as keys. The value is a JSON schema definition for a value.

```json
inputs = {
  [string]: json_schema_object
}
```

The `required` key refers to input fields which must be present. If not, an error will be given when processing the configuration.

```json
required_inputs = [string]
```

For example, to define a required input that acceps the values `SS` or `MS` use:

```json
{
  "inputs": {
    "Material": { "type": enum: ["MS", "SS"] }
  },
  "required": ["Material"]
}
```

##### sections

The `sections` is an object where the keys define the section ids. 

```json
sections = {
  [string]: section | product | product_list
}
```

If type is `section`, then the `ref` key is an array of section keys that should come directly under this section.

```json
section = {
  "title": string,
  "type": "section",
  "ref": array,
  "default": number | bool | null,
  "items": items | null,
  "inputs": array | null
}
```

The `inputs ` is a list of input fields defined in the product's inputs.

If the type is `product`, then the `ref` key refers to the id of the product that should be added under this section.

```json
product = {
  "title": string,
  "type": "product",
  "ref": string
}
```

If the type is `product_list`, then the `ref` key is a list of products that can be added under this section.

```json
product_list = {
  "title": string,
  "type": "product_list",
  "ref": array
}
```

##### items

The `items` key defines rules to fetch product items to the section.

```json
items = {
  "source": string | null,
  "create": boolean | null,
  "filters": {
      [string]: expression | conditional_filter
  } | null,
  "values": {
      [string]: expression
  } | null
}
```

```json
conditional_filter = {
  "value": expression,
  "condition": expression
}
```

Example:

```json
"drivePackage": {
  "items": {
    "filters": {
      "ModuleId": "'DR'",
      "FrequencyId": { 
        "value": "inputs.ACFrequency", 
        "condition": "inputs.DriveType == 'VB'"
      }
    }
  }
}
```

##### defaults

Defaults are values that are copied to every section if they are not given.

```json
defaults = {
  "items": {
    "source": string | null,
    "copy": {
        [string]: string
    } | null,
    "values": {
        [string]: expression
    }
  }
}
```

