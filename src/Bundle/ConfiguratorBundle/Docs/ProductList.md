# Product List

Plant

```json
{
  "title": "Plant",
  "ref": ["processLines"],
  "sections": {
      "processLines": {
          "title": "Process Lines",
          "ref": ["cleaner", "rougher"]
      }
  }
}
```

Cleaner

```json
{
  "title": "Cleaner",
  "ref": ["tank", "dischargeOutlet"],
  "sections": {
      "tank": {
          "title": "Tank",
          "ref": ["tankLining"]
      },
      "tankLining": {
          "title": "Tank Lining"
      },
      "dischargeOutlet": {
          "title": "Discharge Outlet"
      }
  }
}
```

Rougher

```json
{
  "title": "Rougher",
  "ref": ["launder", "drive"],
  "sections": {
      "launder": {
          "title": "Launder"
      },
      "drive": {
          "title": "Drive Package"
      }
  }
}
```

This forms the structure below (green bars mark the products).

![](images/product_list_1.png)

## Generating the product list configuration

The data for product list under the `processLines` section must contain a `products` entry that consists of an array of data for each product. The product data must also contain a `ref` entry that defines which product is being added to the product list.

```json
{
  "title": "Processing Plant",
  "sections": {
    "processLines": {
      "products": [
        {
            "ref": "cleaner",
          	"title": "First Cleaner"
        },
        {
            "ref": "cleaner",
          	"title": "Second Cleaner"
        },
        {
            "ref": "rougher"
        }
      ]
    }
  }
}
```

The generated structure looks like this:

![](images/product_list_2.png)