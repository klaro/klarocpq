# Configurator

Configurator takes an object and generates the configuration from it based on the configuration rules.

The examples below assume that we have a product configuration for an electric motor:

```json
{
  "title": "Electric Motor",
  "ref": ["protectiveCover", "increasedBearings"]
  "sections": {
      "protectiveCover": {
          "title": "Protective Cover"
      },
      "increasedBearings": {
          "title": "Increased Bearings"
      }
  },
}
```

The structure of the configuration is:

![](images/configuration_1.png)

Default input is an empty object. This generates the configuration with default values as above.

## Override Title

To override the product title, add the `title` key:

```json
{
  "title": "My Motor"
}
```

![](images/configuration_2.png)

## Section data

The data for each section under the `sections` key can be controlled by giving the section a different type of input.

A **boolean value** will hide or show the section with default values.

```json
{
  "sections": {
    "protectiveCover": false
  }
}
```

![](images/configuration_4.png)

An **integer value** will repeat the section the given number of times.

```json
{
  "sections": {
    "increasedBearings": 2
  }
}
```

![](images/configuration_5.png)

An **object value** will override values in the section.

```json
{
  "title": "My Motor",
  "sections": {
    "protectiveCover": {
        "title": "A Nice Cover"
    }
  }
}
```

![](images/configuration_3.png)

An **array of objects** will repeat the sections and override the data.

```json
{
  "title": "My Motor",
  "sections": {
    "protectiveCover": {
        "title": "A Nice Cover"
    },
    "increasedBearings": [
        { "title": "First Bearing" },
        { "title": "Second Bearing" }
    ]
  }
}
```

![](images/configuration_6.png)

### Input data

Input data is inherited for nested sections. For example:

```json
{
  "data": {
    "Material": "MS"
  },
  "sections": {
    "protectiveCover": {
      "data": {
        "Size": 100
      }
    }
  }
}
```

The top level section has access to one input `Material` but the Protective Cover section has two inputs:

- Electric Motor `data = { Material: "MS" }`
  - Protective Cover `data = { Material: "MS", Size: 100 }`
  - Increased Bearings `data = { Material: "MS" }`

Input data can be different for sections, for example

```json
{
  "data": {
    "Material": "MS"
  },
  "sections": {
    "protectiveCover": [{
      "title": "Protective Cover 1",
      "data": {
        "Size": 100
      }
    },
    {
      "title": "Protective Cover 2",
      "data": {
        "Size": 200
      }
    }]
  }
}
```

This results with the sections and their data:

- Electric Motor `data = { Material: "MS" }`
  - Protective Cover 1 `data = { Material: "MS", Size: 100 }`
  - Protective Cover 2 `data = { Material: "MS", Size: 200 }`
  - Increased Bearings `data = { Material: "MS" }`