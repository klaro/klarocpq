<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\ExpressionEvaluator;

use Klaro\Component\Cache\CacheInterface;
use Klaro\Component\Cache\ParserCache;
use Klaro\Component\ExpressionEvaluator\ExpressionEvaluator;
use Klaro\Component\ExpressionEvaluator\ExpressionEvaluatorInterface;
use Klaro\Component\Logger\Logger;

class ExpressionEvaluatorFactory
{
    /** @var ParserCache */
    protected $parserCache;

    /** @var CacheInterface */
    protected $cache;

    /**
     * ExpressionEvaluatorFactory constructor.
     * @param CacheInterface|null $cache
     */
    public function __construct(
        CacheInterface $cache = null
    ) {
        $this->cache = $cache;
        $this->parserCache = new ParserCache($this->cache);
    }

    /**
     * @param array $context
     *
     * @return ExpressionEvaluatorInterface
     */
    public function getEvaluator($context = [])
    {
        $cacheId = sprintf(
            'Expression//%s',
            join(',', array_keys($context))
        );

        if ($this->cache && $this->cache->contains($cacheId)) {
            $evaluator = $this->cache->fetch($cacheId);
        } else {
            $evaluator = new ExpressionEvaluator(
                $this->parserCache,
                new Logger()
            );

            $this->cache->save($cacheId, $evaluator);
        }

        $evaluator->setContext($context);

        return $evaluator;
    }
}
