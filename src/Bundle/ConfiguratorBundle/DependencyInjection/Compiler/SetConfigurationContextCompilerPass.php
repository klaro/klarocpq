<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SetConfigurationContextCompilerPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_configurator.configuration_context.manager')) {
            return;
        }

        $definition = $container->getDefinition('klaro_configurator.configuration_context.manager');

        $taggedServices = $container->findTaggedServiceIds('klaro_configurator.configuration_context');

        foreach ($taggedServices as $id => $attributes) {
            foreach ($attributes as $attribute) {
                $ref = isset($attribute['ref']) ? $attribute['ref'] : null;

                if (empty($ref)) {
                    throw new \Exception('Ref must be given for configuration contexts!');
                }

                $definition->addMethodCall('addConfigurationContext', [$ref, $container->getDefinition($id)]);
            }
        }
    }
}
