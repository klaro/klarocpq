<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\DependencyInjection\Compiler;

use Klaro\ConfiguratorBundle\Exception\InvalidNodeProcessorTypeException;
use Klaro\ConfiguratorBundle\Exception\InvalidProductNodeProcessorReferenceException;
use Klaro\ConfiguratorBundle\NodeProcessor\NodeProcessorManager;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class SetNodeProcessorsCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition('klaro_configurator.node_processor_manager')) {
            return;
        }

        $definition = $container->getDefinition('klaro_configurator.node_processor_manager');
        $taggedServices = $container->findTaggedServiceIds('klaro_configurator.node_processor');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $type = $attributes['type'] ?? null;

                switch ($type) {
                    case NodeProcessorManager::GENERAL:
                        $definition->addMethodCall('addGeneralProcessor', [$id, $attributes['priority'] ?? 0]);
                        break;
                    case NodeProcessorManager::PRODUCT:
                        $this->registerProductProcessor($definition, $id, $attributes);
                        break;
                    default:
                        throw new InvalidNodeProcessorTypeException($type);
                }
            }
        }
    }

    private function registerProductProcessor(Definition $definition, string $processorId, array $attributes): void
    {
        $reference = $attributes['ref'] ?? null;
        if (empty($reference)) {
            throw new InvalidProductNodeProcessorReferenceException();
        }

        $definition->addMethodCall('addProductProcessor', [$processorId, $reference, $attributes['priority'] ?? 0]);
    }
}
