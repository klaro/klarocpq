<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\ConfiguratorBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class SetProductItemFinderCompilerPass implements CompilerPassInterface
{

    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('klaro_configurator.product_item_finder.manager')) {
            return;
        }

        $definition = $container->getDefinition('klaro_configurator.product_item_finder.manager');

        $taggedServices = $container->findTaggedServiceIds('klaro_configurator.product_item_finder');

        foreach ($taggedServices as $id => $attributes) {
            foreach ($attributes as $attribute) {
                $alias = isset($attribute['alias']) ? $attribute['alias'] : null;

                if (empty($alias)) {
                    throw new \Exception('Alias is needed for product item finder services!');
                }

                $definition->addMethodCall('addFinderService', [$alias, $container->getDefinition($id)]);
            }
        }
    }
}
