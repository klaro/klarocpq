<?php

namespace Klaro\ConfiguratorBundle\DependencyInjection;

use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;
use Klaro\Component\Configurator\Definition\ConfiguratorItemsLoaderDefinition;
use Klaro\Component\Configurator\Definition\FilteredItem\ConfigurationFilterTypeDefinition;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('klaro_configurator');

        $this->addProductsSection($rootNode);

        return $treeBuilder;
    }

    /**
     * @param ArrayNodeDefinition $node
     */
    private function addProductsSection(ArrayNodeDefinition $node): void
    {
        /** @noinspection NullPointerExceptionInspection */
        $node
            ->fixXmlConfig('product')
            ->children()
                ->arrayNode('products')
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->useAttributeAsKey('product')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('collection_type')
                                ->validate()
                                    ->ifTrue(static function ($v) {
                                        return !in_array($v, ['section', 'additional']);
                                    })
                                    ->thenInvalid('Invalid collection type defined.')
                                ->end()
                            ->end()
                            ->scalarNode('condition')->end()
                            ->arrayNode('defaults')
                                ->append($this->getItemsNode())
                            ->end()
                            ->arrayNode('inputs')
                                ->children()
                                    ->arrayNode('values')
                                        ->scalarPrototype()->end()
                                    ->end()
                                    ->arrayNode('required')
                                        ->variablePrototype()->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->append($this->getItemsNode())
                            ->arrayNode('processors')
                                ->scalarPrototype()->end()
                            ->end()
                            ->arrayNode('ref')
                                ->scalarPrototype()->end()
                            ->end()
                            ->arrayNode('sections')
                                ->requiresAtLeastOneElement()
                                ->useAttributeAsKey('section')
                                ->arrayPrototype()
                                    ->children()
                                        ->scalarNode('condition')->end()
                                        ->variableNode('default')
                                            ->defaultTrue()
                                        ->end()
                                        ->arrayNode('inputs')
                                            ->children()
                                                ->arrayNode('values')
                                                    ->scalarPrototype()->end()
                                                ->end()
                                                ->arrayNode('required')
                                                    ->variablePrototype()->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                        ->append($this->getItemsNode())
                                        ->variableNode('ref')
                                            ->validate()
                                                ->ifTrue(static function ($v) {
                                                    return !is_string($v) && !is_array($v);
                                                })
                                                ->thenInvalid('Ref must be either string or array.')
                                            ->end()
                                        ->end()
                                        ->scalarNode('source')->end()
                                        ->scalarNode('title')->end()
                                        ->scalarNode('type')
                                            ->defaultValue(ConfiguratorDefinitionNode::SECTION)
                                            ->validate()
                                                ->ifTrue(static function ($v) {
                                                    return !in_array($v, [
                                                        ConfiguratorDefinitionNode::SECTION,
                                                        ConfiguratorDefinitionNode::PRODUCT,
                                                        ConfiguratorDefinitionNode::PRODUCT_LIST,
                                                    ], true);
                                                })
                                                ->thenInvalid('Invalid section type defined.')
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->scalarNode('source')->end()
                            ->scalarNode('title')->isRequired()->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    private function getItemsNode()
    {
        $treeBuilder = new TreeBuilder();
        $node = $treeBuilder->root('items');

        /** @noinspection NullPointerExceptionInspection */
        $node
            ->treatFalseLike(['enabled' => false])
            ->treatNullLike(['enabled' => false])
            ->beforeNormalization()
                ->ifTrue(static function ($v) {
                    return isset($v['loader']) && $v['loader'] !== ConfiguratorItemsLoaderDefinition::FILTERED_ITEM_LOADER;
                })
                ->then(static function ($v) {
                    unset($v['options']['filter']);

                    return $v;
                })
            ->end()
            ->children()
                ->booleanNode('enabled')->defaultTrue()->end()
                ->scalarNode('loader')
                    ->validate()
                        ->ifTrue(static function ($v) {
                            return !in_array($v, [
                                ConfiguratorItemsLoaderDefinition::VIRTUAL_ITEM_LOADER,
                                ConfiguratorItemsLoaderDefinition::FILTERED_ITEM_LOADER,
                            ], true);
                        })
                        ->thenInvalid('Invalid items loader defined.')
                    ->end()
                ->end()
                ->arrayNode('options')
                    ->children()
                        ->arrayNode('filter')
                            ->children()
                                ->arrayNode('filters')
                                    ->variablePrototype()
                                        ->validate()
                                            ->ifTrue(static function ($v) {
                                                return !is_scalar($v) && !is_array($v);
                                            })
                                            ->thenInvalid('Variable must be either scalar or array.')
                                        ->end()
                                    ->end()
                                ->end()
                                ->scalarNode('type')
                                    ->defaultValue(ConfigurationFilterTypeDefinition::ITEM_FILTER_SINGLE_TYPE)
                                    ->validate()
                                        ->ifTrue(static function ($v) {
                                            return !in_array($v, [
                                                ConfigurationFilterTypeDefinition::ITEM_FILTER_SINGLE_TYPE,
                                                ConfigurationFilterTypeDefinition::ITEM_FILTER_MULTI_TYPE,
                                            ], true);
                                        })
                                        ->thenInvalid('Invalid filter type defined.')
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('mapping')
                            ->scalarPrototype()->end()
                        ->end()
                        ->scalarNode('source')->end()
                        ->arrayNode('values')
                            ->scalarPrototype()->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $node;
    }
}
