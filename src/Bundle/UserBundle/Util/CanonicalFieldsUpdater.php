<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\UserBundle\Util;

use Klaro\UserBundle\Entity\UserInterface;

class CanonicalFieldsUpdater
{
    /**
     * Updates user's canonical fields
     *
     * @param  UserInterface $user
     */
    public static function updateCanonicalFields(UserInterface $user): void
    {
        $user->setUsernameCanonical(static::canonicalizeUsername($user->getUsername()));
        $user->setEmailCanonical(static::canonicalizeEmail($user->getEmail()));
    }

    /**
     * Canonicalizes a username.
     *
     * @param  string|null $username
     *
     * @return string|null
     */
    public static function canonicalizeUsername($username): ?string
    {
        return Canonicalizer::canonicalize($username);
    }

    /**
     * Canonicalizes an email.
     *
     * @param  string|null $email
     *
     * @return string|null
     */
    public static function canonicalizeEmail($email): ?string
    {
        return Canonicalizer::canonicalize($email);
    }
}
