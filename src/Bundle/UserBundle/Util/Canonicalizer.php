<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\UserBundle\Util;

class Canonicalizer implements CanonicalizerInterface
{
    /**
     * @inheritDoc
     */
    public static function canonicalize(string $value = null): ?string
    {
        if (null === $value) {
            return null;
        }

        $encoding = mb_detect_encoding($value);

        return $encoding
            ? mb_convert_case($value, MB_CASE_LOWER, $encoding)
            : mb_convert_case($value, MB_CASE_LOWER);
    }
}
