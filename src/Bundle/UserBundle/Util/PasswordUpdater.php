<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\UserBundle\Util;

use Exception;
use Klaro\UserBundle\Entity\UserInterface;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class PasswordUpdater implements PasswordUpdaterInterface
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->encoder = $passwordEncoder;
    }

    /**
     * @param  UserInterface $user
     *
     * @throws Exception
     */
    public function hashPassword(UserInterface $user): void
    {
        $plainPassword = $user->getPlainPassword();
        if ($plainPassword === '') {
            return;
        }

        $hashedPassword = $this->encodePassword($user, $plainPassword);
        $user->setPassword($hashedPassword);
        $user->eraseCredentials();
    }

    /**
     * Updates user's salt
     *
     * @param  UserInterface $user
     *
     * @throws Exception
     */
    private function updateSalt(UserInterface $user): void
    {
        if ($this->encoder instanceof BCryptPasswordEncoder) {
            $user->setSalt(null);

            return;
        }

        $salt = rtrim(str_replace('+', '.', base64_encode(random_bytes(32))), '=');
        $user->setSalt($salt);
    }

    /**
     * @param  UserInterface $user
     * @param  string        $plainPassword
     *
     * @return string
     *
     * @throws Exception
     */
    private function encodePassword(UserInterface $user, string $plainPassword): string
    {
        $this->updateSalt($user);

        return $this->encoder->encodePassword($user, $plainPassword);
    }
}
