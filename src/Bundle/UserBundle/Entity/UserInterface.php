<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\UserBundle\Entity;

use DateTime;
use Serializable;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * Interface UserInterface
 *
 * @todo: Once PHP7.4 is set as dependency update setters return type hint to UserInterface
 */
interface UserInterface extends AdvancedUserInterface, Serializable
{
    public const ROLE_DEFAULT = 'ROLE_USER';

    /**
     * Returns user's unique id.
     *
     * @return mixed
     */
    public function getId();

    /**
     * Sets the username.
     *
     * @param string $username
     *
     * @return UserInterface
     */
    public function setUsername(string $username);

    /**
     * Gets the canonical username in search and sort queries.
     *
     * @return string
     */
    public function getUsernameCanonical(): string;

    /**
     * Sets the canonical username.
     *
     * @param string $usernameCanonical
     *
     * @return UserInterface
     */
    public function setUsernameCanonical(string $usernameCanonical);

    /**
     * @param string|null $salt
     *
     * @return UserInterface
     */
    public function setSalt(?string $salt);

    /**
     * Gets email.
     *
     * @return string
     */
    public function getEmail(): string;

    /**
     * Sets the email.
     *
     * @param string $email
     *
     * @return UserInterface
     */
    public function setEmail(string $email);

    /**
     * Gets the canonical email in search and sort queries.
     *
     * @return string
     */
    public function getEmailCanonical(): string;

    /**
     * Sets the canonical email.
     *
     * @param string $emailCanonical
     *
     * @return UserInterface
     */
    public function setEmailCanonical(string $emailCanonical);

    /**
     * Gets first name.
     *
     * @return string
     */
    public function getFirstName(): string;

    /**
     * Sets first name
     *
     * @param  string $firstName
     *
     * @return UserInterface
     */
    public function setFirstName(string $firstName);

    /**
     * Gets last name.
     *
     * @return string
     */
    public function getLastName(): string;

    /**
     * Sets last name
     *
     * @param  string $lastName
     *
     * @return UserInterface
     */
    public function setLastName(string $lastName);


    /**
     * Gets the plain password.
     *
     * @return string
     */
    public function getPlainPassword(): string;

    /**
     * Sets the plain password.
     *
     * @param string $password
     *
     * @return UserInterface
     */
    public function setPlainPassword(string $password);

    /**
     * Sets the hashed password.
     *
     * @param string $password
     *
     * @return UserInterface
     */
    public function setPassword(string $password);

    /**
     * @param bool $boolean
     *
     * @return UserInterface
     */
    public function setEnabled(bool $boolean);

    /**
     * Sets the last login time.
     *
     * @param DateTime|null $time
     *
     * @return UserInterface
     */
    public function setLastLogin(DateTime $time = null);

    /**
     * @param string $role
     *
     * @return bool
     */
    public function hasRole(string $role): bool;

    /**
     * Sets the roles of the user.
     *
     * This overwrites any previous roles.
     *
     * @param array $roles
     *
     * @return UserInterface
     */
    public function setRoles(array $roles);

    /**
     * Adds a role to the user.
     *
     * @param string $role
     *
     * @return UserInterface
     */
    public function addRole(string $role);

    /**
     * Removes a role to the user.
     *
     * @param string $role
     *
     * @return UserInterface
     */
    public function removeRole(string $role);
}
