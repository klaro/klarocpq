<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\UserBundle\DependencyInjection;

use Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class KlaroUserExtension extends Extension
{
    private static $doctrineDrivers = [
        'orm' => [
            'registry' => 'doctrine',
            'tag'      => 'doctrine.event_subscriber',
        ],
    ];

    /**
     * @param  array            $configs
     * @param  ContainerBuilder $container
     *
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));

        foreach (['model', 'security', 'util'] as $configuration) {
            $loader->load(sprintf('%s.xml', $configuration));
        }

        $this->loadDatabaseDriver($config, $container, $loader);

        $this->remapParametersNamespaces(
            $config,
            $container,
            [
                '' => [
                    'user_class' => 'klaro_user.model.user.class',
                ],
            ]
        );
    }

    /**
     * @param  array            $config
     * @param  ContainerBuilder $container
     * @param  array            $namespaces
     */
    protected function remapParametersNamespaces(array $config, ContainerBuilder $container, array $namespaces): void
    {
        foreach ($namespaces as $ns => $map) {
            if ($ns) {
                if (!array_key_exists($ns, $config)) {
                    continue;
                }
                $namespaceConfig = $config[$ns];
            } else {
                $namespaceConfig = $config;
            }
            if (is_array($map)) {
                $this->remapParameters($namespaceConfig, $container, $map);
            } else {
                foreach ($namespaceConfig as $name => $value) {
                    $container->setParameter(sprintf($map, $name), $value);
                }
            }
        }
    }

    /**
     * @param  array            $config
     * @param  ContainerBuilder $container
     * @param  array            $map
     */
    protected function remapParameters(array $config, ContainerBuilder $container, array $map): void
    {
        foreach ($map as $name => $paramName) {
            if (array_key_exists($name, $config)) {
                $container->setParameter($paramName, $config[$name]);
            }
        }
    }

    /**
     * @param  array            $config
     * @param  ContainerBuilder $container
     * @param  XmlFileLoader    $loader
     *
     * @throws Exception
     */
    private function loadDatabaseDriver(array $config, ContainerBuilder $container, XmlFileLoader $loader): void
    {
        $databaseDriver = $config['db_driver'];
        if (false === isset(self::$doctrineDrivers[$databaseDriver])) {
            return;
        }

        $loader->load('doctrine.xml');

        // Enable orm mapping
        $container->setParameter('klaro_user.backend_type_'.$databaseDriver, true);
    }
}
