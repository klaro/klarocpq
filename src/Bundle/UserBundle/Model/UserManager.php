<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\UserBundle\Model;

use Klaro\UserBundle\Entity\UserInterface;
use Klaro\UserBundle\Util\CanonicalFieldsUpdater;
use Klaro\UserBundle\Util\PasswordUpdaterInterface;

abstract class UserManager implements UserManagerInterface
{
    /**
     * @var PasswordUpdaterInterface
     */
    private $passwordUpdater;

    public function __construct(PasswordUpdaterInterface $passwordUpdater)
    {
        $this->passwordUpdater = $passwordUpdater;
    }

    /**
     * {@inheritdoc}
     */
    public function createUser()
    {
        $class = $this->getClass();

        return new $class();
    }

    /**
     * {@inheritdoc}
     */
    public function findUserByUsernameOrEmail($usernameOrEmail)
    {
        if (preg_match('/^.+\@\S+\.\S+$/', $usernameOrEmail)) {
            $user = $this->findUserByEmail($usernameOrEmail);
            if (null !== $user) {
                return $user;
            }
        }

        return $this->findUserByUsername($usernameOrEmail);
    }

    /**
     * {@inheritdoc}
     */
    public function findUserByEmail($email)
    {
        return $this->findUserBy(['emailCanonical' => CanonicalFieldsUpdater::canonicalizeEmail($email)]);
    }

    /**
     * {@inheritdoc}
     */
    public function findUserByUsername($username)
    {
        return $this->findUserBy(
            ['usernameCanonical' => CanonicalFieldsUpdater::canonicalizeUsername($username)]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function updateCanonicalFields(UserInterface $user): void
    {
        CanonicalFieldsUpdater::updateCanonicalFields($user);
    }

    /**
     * {@inheritdoc}
     */
    public function updatePassword(UserInterface $user)
    {
        $this->passwordUpdater->hashPassword($user);
    }
}
