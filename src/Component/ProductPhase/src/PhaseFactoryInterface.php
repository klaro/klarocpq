<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductPhase;

interface PhaseFactoryInterface
{
    public function createProductListPhase($id, $title, $roles, $disabled, $ref, PhaseNode $parent = null);

    public function createProductPhase($id, $ref, PhaseNode $parent = null);

    public function createGroupPhase($id, $title, $roles, $disabled, $ref, PhaseNode $parent = null);

    public function createSinglePhase($id, $ref, PhaseNode $parent = null);

    public function createPhaseDefinition($ref);

    public function createProductLinePhase(ProductLineDefinition $productLineDefinition);

    public function createProductLinePhaseById($id);

    /**
     * @param $id
     * @param array $phaseData
     * @param null  $version
     *
     * @return ProductLineDefinition
     */
    public function createProductLineDefinition($id);
}
