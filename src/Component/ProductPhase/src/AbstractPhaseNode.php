<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductPhase;

abstract class AbstractPhaseNode implements PhaseNode
{
    /** @var string */
    protected $id;

    /** @var string|array */
    protected $ref;

    /** @var string */
    protected $title;

    /** @var array */
    protected $roles;

    /** @var string|bool */
    protected $disabled;

    /** @var PhaseNode */
    protected $parent;

    /**
     * PhaseNode constructor.
     *
     * @param string       $id
     * @param string|array $ref
     * @param null         $title
     */
    public function __construct($id, $ref, $title = null)
    {
        $this->id = $id;
        $this->ref = $ref;
        $this->title = $title;
        $this->parent = null;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array|string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param array|string $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param array $roles
     */
    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    /**
     * @return bool|string
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * @param bool|string $disabled
     */
    public function setDisabled($disabled): void
    {
        $this->disabled = $disabled;
    }

    /**
     * @return PhaseNode
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param PhaseNode $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }
}
