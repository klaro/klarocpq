<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductPhase;

class GroupPhase extends AbstractPhaseNode
{
    /** @var SinglePhase */
    protected $phases;

    /**
     * GroupPhase constructor.
     * @param string       $id
     * @param array|string $ref
     * @param null         $title
     */
    public function __construct($id, $ref, $title = null)
    {
        parent::__construct($id, $ref, $title);

        $this->phases = [];
    }

    /**
     * @return SinglePhase
     */
    public function getPhases()
    {
        return $this->phases;
    }

    /**
     * @param SinglePhase $phase
     */
    public function addPhase($ref, $phase)
    {
        $this->phases[$ref] = $phase;
    }

    public function getType()
    {
        return PhaseNode::GROUP;
    }
}
