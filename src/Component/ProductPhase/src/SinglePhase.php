<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductPhase;

class SinglePhase extends AbstractPhaseNode
{
    /** @var PhaseDefinition **/
    protected $phaseDefinition;

    /**
     * SinglePhase constructor.
     * @param string       $id
     * @param array|string $ref
     * @param null         $title
     */
    public function __construct($id, $ref, $title = null)
    {
        parent::__construct($id, $ref, $title);

        $this->phaseDefinition = null;
    }

    /**
     * @return PhaseDefinition
     */
    public function getPhaseDefinition()
    {
        return $this->phaseDefinition;
    }

    /**
     * @param PhaseDefinition $phaseDefinition
     */
    public function setPhaseDefinition(PhaseDefinition $phaseDefinition)
    {
        $this->phaseDefinition = $phaseDefinition;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        if (!empty($this->title)) {
            return $this->title;
        }

        return $this->phaseDefinition ? $this->phaseDefinition->getTitle() : null;
    }

    public function getDisabled()
    {
        if ($this->phaseDefinition) {
            return $this->phaseDefinition->getDisabled();
        }

        return parent::getDisabled();
    }

    public function setDisabled($disabled): void
    {
        if ($this->phaseDefinition) {
            $this->phaseDefinition->setDisabled($disabled);

            return;
        }

        parent::setDisabled($disabled);
    }

    public function getRoles(): array
    {
        if ($this->phaseDefinition) {
            return $this->phaseDefinition->getRoles();
        }

        return parent::getRoles();
    }

    public function setRoles(array $roles): void
    {
        if ($this->phaseDefinition) {
            $this->phaseDefinition->setRoles($roles);

            return;
        }

        parent::setRoles($roles);
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return PhaseNode::SINGLE;
    }
}
