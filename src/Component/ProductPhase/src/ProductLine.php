<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductPhase;

class ProductLine implements PhaseNode
{
    /** @var ProductLineDefinition */
    protected $productLineDefinition;

    /** @var PhaseNode[] */
    protected $phases;

    /**
     * ProductLine constructor.
     * @param ProductLineDefinition $productLineDefinition
     */
    public function __construct(ProductLineDefinition $productLineDefinition)
    {
        $this->productLineDefinition = $productLineDefinition;
        $this->phases = [];
    }

    /**
     * @return PhaseNode[]
     */
    public function getPhases()
    {
        return $this->phases;
    }

    /**
     * @param PhaseNode $phase
     */
    public function addPhase($ref, PhaseNode $phase)
    {
        $this->phases[$ref] = $phase;
    }

    /**
     * @return ProductLineDefinition
     */
    public function getProductLineDefinition()
    {
        return $this->productLineDefinition;
    }

    /**
     * @param ProductLineDefinition $productLineDefinition
     */
    public function setProductLineDefinition($productLineDefinition)
    {
        $this->productLineDefinition = $productLineDefinition;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->productLineDefinition->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return $this->productLineDefinition->getTitle();
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getDisabled()
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return PhaseNode::PRODUCT_LINE;
    }

    /**
     * @return string|array
     */
    public function getRef()
    {
        return null;
    }
}
