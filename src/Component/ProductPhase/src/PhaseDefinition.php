<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductPhase;

/**
 * Class representing a phase's configuration.
 *
 * Class PhaseDefinition
 * @package Klaro\Component\Phase
 */
class PhaseDefinition
{
    /** @var array */
    protected $aliases;

    /** @var string */
    protected $phaseId;

    /** @var array */
    protected $originalConfig;

    /** @var array */
    protected $values;

    /** @var array */
    protected $schema;

    /**
     * PhaseDefinition constructor.
     * @param $phaseId
     * @param $phaseConfig
     */
    public function __construct($phaseId, $phaseConfig)
    {
        $this->phaseId = $phaseId;
        $this->originalConfig = $phaseConfig;
        $this->aliases        = [];
        $this->values         = [];
        $this->schema = null;
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function getAlias($name)
    {
        return isset($this->aliases[$name]) ? $this->aliases[$name] : $name;
    }

    /**
     * @param array $use
     */
    public function setAliases($use)
    {
        $this->aliases = $use;
    }

    /**
     * @return array
     */
    public function getAliases()
    {
        return is_array($this->aliases) ? $this->aliases : [];
    }

    /**
     * @param mixed $phaseId
     */
    public function setPhaseId($phaseId)
    {
        $this->phaseId = $phaseId;
    }

    /**
     * @return mixed
     */
    public function getPhaseId()
    {
        return $this->phaseId;
    }

    /**
     * @param string $editor
     */
    public function setEditor($editor)
    {
        $this->setProperty('editor', $editor);
    }

    /**
     * @return string
     */
    public function getEditor()
    {
        return $this->getProperty('editor');
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->setProperty('title', $title);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->getProperty('title');
    }

    /**
     * @param string $icon
     */
    public function setIcon($icon)
    {
        $this->setProperty('icon', $icon);
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->getProperty('icon');
    }

    /**
     * @param boolean $disabled
     */
    public function setDisabled($disabled)
    {
        $this->setProperty('disabled', $disabled);
    }

    /**
     * @return boolean
     */
    public function getDisabled()
    {
        return $this->getProperty('disabled');
    }

    /**
     * @return null
     */
    public function getValidationClass()
    {
        return $this->getProperty('validation');
    }

    /**
     * @param $className
     */
    public function setValidationClass($className)
    {
        $this->setProperty('validation', $className);
    }

    /**
     * @return null
     */
    public function getDecoratorClass()
    {
        return $this->getProperty('decorator');
    }

    /**
     * @param $className
     */
    public function setDecoratorClass($className)
    {
        $this->setProperty('decorator', $className);
    }

    /**
     * @param $roles
     */
    public function setRoles($roles)
    {
        $this->setProperty('roles', $roles);
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->getProperty('roles');
    }

    /**
     * @param mixed $config
     */
    public function setOriginalConfig($config)
    {
        $this->originalConfig = $config;
    }

    /**
     * @return mixed
     */
    public function getOriginalConfig()
    {
        return $this->originalConfig;
    }

    /**
     * @return array
     */
    public function getSchema()
    {
        return $this->schema;
    }

    /**
     * @param array $schema
     */
    public function setSchema($schema)
    {
        $this->schema = $schema;
    }

    /**
     * @param $fieldName
     * @param $value
     */
    private function setProperty($fieldName, $value)
    {
        $this->values[$fieldName] = $value;
    }

    /**
     * @param $fieldName
     *
     * @return null
     */
    private function getProperty($fieldName)
    {
        if (isset($this->values[$fieldName])) {
            return $this->values[$fieldName];
        }

        return null;
    }
}
