<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductPhase;

class ProductListPhase extends AbstractPhaseNode
{
    /** @var ProductPhase[] */
    protected $products;

    /**
     * ProductListPhase constructor.
     * @param string       $id
     * @param array|string $ref
     * @param null         $title
     */
    public function __construct($id, $ref, $title = null)
    {
        parent::__construct($id, $ref, $title);

        $this->products = [];
    }

    /**
     * @return ProductPhase[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param ProductPhase $product
     */
    public function addProduct($ref, ProductPhase $product)
    {
        $this->products[$ref] = $product;
    }

    public function getType()
    {
        return PhaseNode::PRODUCT_LIST;
    }
}
