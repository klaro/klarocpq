<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductPhase;

class ProductLineDefinition implements \JsonSerializable
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $title;

    /** @var string */
    protected $image;

    /** @var array */
    protected $phases;

    /** @var array */
    protected $structure;

    /** @var string */
    protected $version;

    public function __construct($id, $title, $version = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->image = null;
        $this->phases = [];
        $this->structure = [];
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return array
     */
    public function getPhases()
    {
        return $this->phases;
    }

    /**
     * @param array $phases
     */
    public function setPhases($phases)
    {
        $this->phases = $phases;
    }

    /**
     * @return array
     */
    public function getStructure()
    {
        return $this->structure;
    }

    /**
     * @param array $structure
     */
    public function setStructure($structure)
    {
        $this->structure = $structure;
    }

    /**
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'image' => $this->getImage(),
        ];
    }

    public function toJson()
    {
        $json = json_encode([
            'id' => $this->getId(),
            'title' => $this->getTitle(),
            'image' => $this->getImage(),
        ]);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new \UnexpectedValueException(sprintf(
                'Can not encode cache signature to JSON, error: "%s".',
                json_last_error_msg()
            ));
        }

        return $json;
    }
}
