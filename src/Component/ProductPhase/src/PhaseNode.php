<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductPhase;

interface PhaseNode
{
    const PRODUCT_LINE = 'product_line';
    const PRODUCT_LIST = 'product_list';
    const PRODUCT = 'product';
    const GROUP = 'group';
    const SINGLE = 'single';

    public function getId();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @return string|array
     */
    public function getRef();

    /**
     * @return PhaseNode
     */
    public function getParent();

    /**
     * @return string|bool
     */
    public function getDisabled();

    /**
     * @return string
     */
    public function getType();
}
