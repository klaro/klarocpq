<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductPhase;

use Klaro\Component\ProductLoader\ProductConfigLoaderInterface;

class PhaseFactory implements PhaseFactoryInterface
{
    /** @var ProductConfigLoaderInterface */
    protected $productLoader;

    /**
     * PhaseFactory constructor.
     * @param ProductConfigLoaderInterface $productLoader
     */
    public function __construct(ProductConfigLoaderInterface $productLoader)
    {
        $this->productLoader = $productLoader;
    }

    /**
     * @param ProductConfigLoaderInterface $productLoader
     *
     * @return PhaseFactory
     */
    public static function create(ProductConfigLoaderInterface $productLoader)
    {
        return new self($productLoader);
    }

    /**
     * {@inheritdoc}
     */
    public function createProductListPhase(
        $id,
        $title,
        $roles,
        $disabled,
        $refs,
        PhaseNode $parent = null
    ) {
        if (!is_array($refs)) {
            $refs = [$refs];
        }

        $phase = new ProductListPhase($id, $refs, $title);
        $phase->setRoles($roles);
        $phase->setDisabled($disabled);
        $phase->setParent($parent);

        foreach ($refs as $ref) {
            $productPhase = $this->createProductPhase($ref, $ref, $phase);

            $phase->addProduct($ref, $productPhase);
        }

        return $phase;
    }

    /**
     * {@inheritdoc}
     */
    public function createProductPhase(
        $id,
        $ref,
        PhaseNode $parent = null
    ) {
        $productData = $this->productLoader->getProductConfig($ref);

        list($title, $roles, $disabled, $phases) = [
            $productData['title'] ?? null,
            $productData['roles'] ?? [],
            $productData['disabled'] ?? false,
            $productData['phases'],
        ];

        $product = new ProductPhase($id, $ref, $title);
        $product->setRoles($roles);
        $product->setDisabled($disabled);
        $product->setParent($parent);

        foreach ($phases as $phase) {
            $singlePhase = $this->createSinglePhase($phase, $phase, $product);

            $product->addPhase($phase, $singlePhase);
        }

        return $product;
    }

    /**
     * {@inheritdoc}
     */
    public function createGroupPhase(
        $id,
        $title,
        $roles,
        $disabled,
        $refs,
        PhaseNode $parent = null
    ) {
        $phase = new GroupPhase($id, $refs, $title);
        $phase->setRoles($roles);
        $phase->setDisabled($disabled);
        $phase->setParent($parent);

        foreach ($refs as $ref) {
            $singlePhase = $this->createSinglePhase($ref, $ref, $phase);

            $phase->addPhase($ref, $singlePhase);
        }

        return $phase;
    }

    /**
     * {@inheritdoc}
     */
    public function createSinglePhase(
        $id,
        $ref,
        PhaseNode $parent = null
    ) {
        $phase = new SinglePhase($id, $ref);
        $phase->setParent($parent);

        $phaseDefinition = $this->createPhaseDefinition($ref);
        $phase->setPhaseDefinition($phaseDefinition);

        return $phase;
    }

    /**
     * {@inheritdoc}
     */
    public function createPhaseDefinition($ref)
    {
        $phaseConfig = $this->productLoader->getPhaseConfig($ref);

        $phaseDefinition = new PhaseDefinition($ref, $phaseConfig);
        $phaseDefinition->setTitle($phaseConfig['title']);
        $phaseDefinition->setAliases($phaseConfig['aliases'] ?? []);
        $phaseDefinition->setEditor($phaseConfig['editor']);
        $phaseDefinition->setIcon($phaseConfig['icon']);
        $phaseDefinition->setRoles($phaseConfig['roles'] ?? []);
        $phaseDefinition->setDisabled($phaseConfig['disabled'] ?? false);

        if (!empty($phaseConfig['validation'])) {
            $phaseDefinition->setValidationClass($phaseConfig['validation']);
        }

        if (!empty($phaseConfig['decorator'])) {
            $phaseDefinition->setDecoratorClass($phaseConfig['decorator']);
        }

        if (isset($phaseConfig['schema'])) {
            $phaseDefinition->setSchema($phaseConfig['schema']);
        }

        return $phaseDefinition;
    }

    /**
     * {@inheritdoc}
     */
    public function createProductLinePhase(ProductLineDefinition $productLineDefinition)
    {
        $productLine = new ProductLine($productLineDefinition);

        foreach ($productLineDefinition->getPhases() as $phaseId => $phaseData) {
            $phase = $this->createPhaseNode($phaseId, $phaseData, $productLine);
            if ($phase) {
                $productLine->addPhase($phaseId, $phase);
            }
        }

        return $productLine;
    }

    /**
     * {@inheritdoc}
     */
    public function createProductLinePhaseById($id)
    {
        return $this->createProductLinePhase(
            $this->createProductLineDefinition($id)
        );
    }

    /**
     * @param $id
     *
     * @return ProductLineDefinition
     */
    public function createProductLineDefinition($id)
    {
        $phaseData = $this->productLoader->getProductLineConfig($id);

        $title = $phaseData['title'] ?? null;
        $phases = $phaseData['phases'] ?? [];
        $structure = $phaseData['structure'] ?? [];
        $image = $phaseData['image'] ?? null;

        $productLine = new ProductLineDefinition($id, $title, $this->productLoader->getLatestVersion());
        $productLine->setPhases($phases);
        $productLine->setStructure($structure);
        $productLine->setImage($image);

        return $productLine;
    }

    /**
     * Create and return phase based on phaseLineDefinition
     *
     * @param $phaseId
     * @param array       $phaseData
     * @param ProductLine $productLine
     *
     * @return null|GroupPhase|ProductListPhase|ProductPhase|SinglePhase
     */
    private function createPhaseNode($phaseId, array $phaseData, ProductLine $productLine)
    {
        list($type, $title, $roles, $disabled, $ref) = [
            $phaseData['type'],
            $phaseData['title'],
            $phaseData['roles'] ?? [],
            $phaseData['disabled'] ?? false,
            $phaseData['ref'],
        ];

        switch ($type) {
            case PhaseNode::PRODUCT_LIST:
                return $this->createProductListPhase($phaseId, $title, $roles, $disabled, $ref, $productLine);
            case PhaseNode::PRODUCT:
                return $this->createProductPhase($phaseId, $ref, $productLine);
            case PhaseNode::GROUP:
                return $this->createGroupPhase($phaseId, $title, $roles, $disabled, $ref, $productLine);
            case PhaseNode::SINGLE:
                return $this->createSinglePhase($phaseId, $ref, $productLine);
            default:
                // TODO: Add exception in future so we can rely on return without checking it
                return null;
        }
    }
}
