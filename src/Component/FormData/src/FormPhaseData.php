<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\FormData;

use Klaro\Component\Validation\FormPhaseDataSanitizerInterface;
use Klaro\Component\Validation\FormPhaseDataValidatorChain;
use Klaro\Component\Validation\FormPhaseDataValidatorInterface;
use Klaro\Component\Validation\NoOpDataHandler;
use Klaro\Component\Validation\ValidationResult;

class FormPhaseData implements FormPhaseDataInterface
{
    /** @var string */
    protected $id;

    /** @var array */
    protected $values;

    /** @var array */
    protected $changedValues;

    /** @var FormPhaseDataSanitizerInterface */
    protected $sanitizer;

    /** @var FormPhaseDataValidatorInterface */
    protected $validator;

    /**
     * FormPhaseData constructor.
     * @param $id
     * @param array $values
     * @param null  $dataHandler
     * @param null  $schemaValidator
     */
    public function __construct($id, array $values, $dataHandler = null, $schemaValidator = null)
    {
        $this->id = $id;
        $this->values = $values;
        $this->changedValues = $values;

        $noOpHandler = new NoOpDataHandler();

        $this->sanitizer = ($dataHandler instanceof FormPhaseDataSanitizerInterface) ? $dataHandler : $noOpHandler;
        $this->validator = ($dataHandler instanceof FormPhaseDataValidatorInterface) ? $dataHandler : $noOpHandler;

        if ($schemaValidator instanceof FormPhaseDataValidatorInterface) {
            $this->validator = new FormPhaseDataValidatorChain([
                $schemaValidator,
                $this->validator,
            ]);
        }

        $this->commit();
    }

    /**
     * {@inheritdoc}
     */
    public function __get($field)
    {
        return $this->get($field);
    }

    /**
     * {@inheritdoc}
     */
    public function __set($field, $value)
    {
        $this->set($field, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function __call($name, $parameters)
    {
        throw new \Exception('Nothing to call.');
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getChangedValues()
    {
        return $this->changedValues;
    }

    /**
     * @param array $values
     */
    public function setChangedValues($values)
    {
        $this->changedValues = $values;
    }

    /**
     * @return array
     */
    public function getValidatedValues()
    {
        return $this->values;
    }

    /**
     * Get set of values based on requested fields
     *
     * @param array $fields
     *
     * @return array
     */
    public function getValues(array $fields): array
    {
        foreach ($fields as $field) {
            $values[] = $this->get($field);
        }

        return $values ?? [];
    }

    /**
     * Set multiple values at once
     *
     * @param array $values
     * @param bool  $sanitize
     */
    public function setValues(array $values, $sanitize = true): void
    {
        foreach ($values as $key => $value) {
            $this->set($key, $value, $sanitize);
        }
    }

    /**
     * Set multiple values at once, affects only really changed values
     *
     * @param array $values
     * @param bool  $sanitize
     */
    public function setValuesIfChanged(array $values, $sanitize = true) : void
    {

        foreach ($values as $key => $value) {
            $this->setIfChanged($key, $value, $sanitize);
        }
    }

    /**
     *
     */
    public function commit()
    {
        $result = $this->validate();

        $this->values = $result->getValues() + $this->values;
        $this->changedValues = $this->values;

        return $result;
    }

    /**
     * @return ValidationResult
     */
    public function validate()
    {
        $result = new ValidationResult();

        $this->validator->validate(
            $this->sanitizer->sanitize($this->changedValues),
            $result
        );

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function get($field, $arguments = null)
    {
        return isset($this->changedValues[$field]) ? $this->changedValues[$field] : null;
    }

    /**
     * {@inheritdoc}
     */
    public function set($field, $value, $sanitize = true)
    {
        if ($sanitize) {
            $this->changedValues = $this->sanitizer->sanitize(
                [$field => $value] + $this->changedValues
            );
        } else {
            $this->changedValues = [$field => $value] + $this->changedValues;
        }
    }

    /**
     * Set value only if it differs
     *
     * @var String $field
     * @var mixed $value
     * @var bool $sanitize
     *
     * @return boolean True if differs and was set, false otherwise
     */
    public function setIfChanged($field, $value, $sanitize = true) : bool
    {
        if (!isset($this->changedValues[$field]) ||
            (isset($this->changedValues[$field]) && $this->changedValues[$field] !== $value)) {
            if ($sanitize) {
                $this->changedValues = $this->sanitizer->sanitize(
                    [$field => $value] + $this->changedValues
                );
            } else {
                $this->changedValues = [$field => $value] + $this->changedValues;
            }

            return true;
        }

        return false;
    }

    /**
     * Returns list of changed fields names
     *
     * @param bool $hasAppeared
     *
     * @return array
     */
    public function getValuesUpdated($hasAppeared = true): array
    {
        $changedFields = [];
        foreach ($this->values as $field => $value) {
            if ($this->hasChanged($field, $hasAppeared)) {
                $changedFields[] = $field;
            }
        }

        return $changedFields;
    }

    /**
     * Check if given field got initialized during the change
     *
     * @param $field
     *
     * @return bool
     */
    public function hasAppeared($field) : bool
    {
        return false === isset($this->values[$field]) && isset($this->changedValues[$field]);
    }

    /**
     * Check if given field have been changed
     *
     * @param $field
     * @param bool $hasAppeared
     *
     * @return bool
     */
    public function hasChanged($field, $hasAppeared = true) : bool
    {
        if ($hasAppeared && $this->hasAppeared($field)) {
            return true;
        }

        return isset($this->values[$field]) && isset($this->changedValues[$field]) && $this->values[$field] !== $this->changedValues[$field];
    }

    /**
     * Check if given fields have been changed
     *
     * @param array $fields
     * @param bool $hasAppeared
     *
     * @return bool
     */
    public function hasChangedFields(array $fields, $hasAppeared = true): bool
    {
        foreach ($fields as $field) {
            if ($this->hasChanged($field, $hasAppeared)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check if any fields have been changed
     *
     * @param array $fields
     *
     * @return bool
     */
    public function hasAnyChangedFields(): bool
    {
        return $this->values != $this->changedValues;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return $this->changedValues;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize([
            'values' => $this->values,
            'changedValues' => $this->changedValues,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        $this->values = is_array($data['values']) ? $data['values'] : [];
        $this->changedValues = is_array($data['changedValues']) ? $data['changedValues'] : $this->values;
    }

    /**
     * {@inheritdoc}
     */
    function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return isset($this->changedValues[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        unset($this->changedValues[$offset]);
    }
}
