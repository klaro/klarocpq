<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\FormData;

class SinglePhaseDataNode extends LockableDataNode
{
    /** @var FormPhaseDataInterface */
    protected $data;

    /**
     * SinglePhaseData constructor.
     * @param array $values
     */
    public function __construct($id, $title = null)
    {
        parent::__construct($id, $title);

        $this->data = null;
    }

    /**
     * @param null $property
     *
     * @return mixed|null|FormPhaseDataInterface
     */
    public function getData($property = null)
    {
        return !is_null($property) ? $this->data[$property] : $this->data;
    }

    /**
     * @param $property
     * @param $value
     * @param bool     $commit
     */
    public function setData($property, $value, $commit = false)
    {
        $this->data->set($property, $value);

        if ($commit === true) {
            $this->data->commit();
        }
    }

    /**
     * @return null|FormPhaseDataInterface
     */
    public function getFormPhaseData()
    {
        return $this->data;
    }

    /**
     * @param FormPhaseDataInterface $data
     */
    public function setFormPhaseData(FormPhaseDataInterface $data)
    {
        $this->data = $data;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return parent::toArray() + [
            'data' => $this->data->toArray(),
        ];
    }

    public function getSinglePhaseByPath($path)
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return $this->data->current();
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        return $this->data->next();
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return $this->data->key();
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return $this->data->valid();
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        return $this->data->rewind();
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->data[$offset] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return is_array($this->data) ? count($this->data) : 0;
    }

    /**
     * @return DataNodeInterface
     */
    public function getTopLevelNode()
    {
        $parent = $this->getParent();

        if ($parent instanceof ProductPhaseDataNode) {
            return $parent->getTopLevelNode();
        } elseif ($parent instanceof GroupPhaseDataNode) {
            return $parent;
        }

        return parent::getTopLevelNode();
    }
}
