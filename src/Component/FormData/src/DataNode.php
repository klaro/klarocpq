<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\FormData;

use Klaro\Component\ProductPhase\PhaseNode;

abstract class DataNode implements DataNodeInterface
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $title;

    /** @var PhaseNode|null */
    protected $phaseNode;

    /** @var DataNodeInterface|null */
    protected $parent;

    /** @var DataNodeInterface[]|null */
    protected $children;

    /**
     * DataNode constructor.
     * @param $id
     * @param $title
     */
    public function __construct($id, $title = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->children = null;
        $this->parent = null;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return null|DataNodeInterface[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param null|DataNodeInterface[] $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return bool
     */
    public function hasChildren()
    {
        return is_array($this->children) && count($this->children) > 0;
    }

    /**
     * @param mixed|null        $ref
     * @param DataNodeInterface $child
     *
     * @return DataNodeInterface
     */
    public function addChild($ref, DataNodeInterface $child)
    {
        $this->children[$ref] = $child;

        return $child;
    }

    /**
     * @param mixed $ref
     *
     * @return DataNodeInterface
     */
    public function getChild($ref)
    {
        return $this->children[$ref];
    }

    /**
     * {@inheritdoc}
     */
    public function getPhaseNode()
    {
        return $this->phaseNode;
    }

    /**
     * @param PhaseNode $phaseNode
     */
    public function setPhaseNode(PhaseNode $phaseNode)
    {
        $this->phaseNode = $phaseNode;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param DataNodeInterface $parent
     */
    public function setParent(DataNodeInterface $parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        $path = $this->getId();
        $parent = $this->getParent();

        if ($parent && !($parent instanceof ProductLineDataNode)) {
            $path = $parent->getPath().DataNodeInterface::PATH_DELIMITER.$path;
        }

        return $path;
    }

    /**
     * @return DataNodeInterface
     */
    public function getTopLevelNode()
    {
        return $this;
    }

    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->{$name};
        }

        return $this->offsetGet($name);
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            $this->{$name} = $value;
        }

        $this->offsetSet($name, $value);
    }

    public function __isset($name)
    {
        if (property_exists($this, $name)) {
            return true;
        }

        return $this->offsetExists($name);
    }

    /**
     * {@inheritdoc}
     */
    public function current()
    {
        return current($this->children);
    }

    /**
     * {@inheritdoc}
     */
    public function next()
    {
        return next($this->children);
    }

    /**
     * {@inheritdoc}
     */
    public function key()
    {
        return key($this->children);
    }

    /**
     * {@inheritdoc}
     */
    public function valid()
    {
        return is_array($this->children) && key($this->children) !== null;
    }

    /**
     * {@inheritdoc}
     */
    public function rewind()
    {
        return is_array($this->children) ? reset($this->children) : false;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return isset($this->children[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->children[$offset];
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->children[$offset] = $value;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        unset($this->children[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return is_array($this->children) ? count($this->children) : 0;
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return [
            'title' => $this->getTitle(),
        ];
    }
}
