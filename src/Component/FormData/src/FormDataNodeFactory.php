<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\FormData;

use Klaro\Component\ProductPhase\GroupPhase;
use Klaro\Component\ProductPhase\PhaseNode;
use Klaro\Component\ProductPhase\ProductLine;
use Klaro\Component\ProductPhase\ProductListPhase;
use Klaro\Component\ProductPhase\ProductPhase;
use Klaro\Component\ProductPhase\SinglePhase;
use Klaro\Component\Validation\ArraySchemaFormPhaseValidator;

class FormDataNodeFactory
{
    /** @var ProductLine */
    protected $productLine;

    /**
     * FormDataNodeFactory constructor.
     *
     * @param ProductLine $productLine
     */
    public function __construct(ProductLine $productLine)
    {
        $this->productLine = $productLine;
    }

    /**
     * @param ProductLine $productLine
     *
     * @return FormDataNodeFactory
     */
    public static function create(ProductLine $productLine)
    {
        return new self($productLine);
    }

    /**
     * productLine = {
     *
     * }
     *
     * @param array $phaseData
     *
     * @return ProductLineDataNode
     */
    public function createProductLineDataNode(array $phaseData)
    {
        $productLineNode = new ProductLineDataNode($this->productLine->getId());
        $productLineNode->setPhaseNode($this->productLine);

        foreach ($this->productLine->getPhases() as $phaseId => $phase) {
            $node = null;

            $data = isset($phaseData[$phaseId]) ? $phaseData[$phaseId] : [];

            if ($phase->getType() === PhaseNode::SINGLE) {
                /** @var SinglePhase $phase */
                $node = $this->createSinglePhaseDataNode($phase, $phaseId, $data, $productLineNode);
            } elseif ($phase->getType() === PhaseNode::GROUP) {
                /** @var GroupPhase $phase */
                $node = $this->createGroupPhaseDataNode($phase, $phaseId, $data, $productLineNode);
            } elseif ($phase->getType() === PhaseNode::PRODUCT_LIST) {
                /** @var ProductListPhase $phase */
                $node = $this->createProductListPhaseDataNode($phase, $phaseId, $data, $productLineNode);
            } elseif ($phase->getType() === PhaseNode::PRODUCT) {
                /** @var ProductPhase $phase */
                $node = $this->createProductPhaseDataNode($phase, $phaseId, $data, $productLineNode);
            }

            if ($node) {
                $productLineNode->addChild($phaseId, $node);
            }
        }

        return $productLineNode;
    }

    /**
     * single_phase_data = {
     *   "data": phase_values,
     *   "locked": boolean | null,
     *   "title": string | null
     * }
     *
     * phase_values = {
     *   [string]: mixed
     * }
     *
     * @param SinglePhase   $phase
     * @param array         $phaseData
     * @param DataNode|null $parent
     *
     * @return SinglePhaseDataNode
     */
    public function createSinglePhaseDataNode(SinglePhase $phase, $id, array $phaseData, DataNode $parent)
    {
        $data = isset($phaseData['data']) ? $phaseData['data'] : [];
        $locked = isset($phaseData['locked']) ? $phaseData['locked'] : false;
        $title = isset($phaseData['title']) ? $phaseData['title'] : null;

        $singlePhaseData = new SinglePhaseDataNode($id, $title);
        $singlePhaseData->setLocked($locked);
        $singlePhaseData->setPhaseNode($phase);
        $singlePhaseData->setParent($parent);

        // Add input sanitizer
        $validationClass = $phase->getPhaseDefinition()->getValidationClass();

        $validator = null;
        $schemaValidator = null;

        if (class_exists($validationClass)) {
            $validator = new $validationClass();
        }

        // Add schema validator
        $schema = $phase->getPhaseDefinition()->getSchema();
        if (is_array($schema)) {
            $schemaValidator = new ArraySchemaFormPhaseValidator($schema);
        }

        $formData = new FormPhaseData($singlePhaseData->getPath(), $data, $validator, $schemaValidator);

        // Decorator
        $decoratorClass = $phase->getPhaseDefinition()->getDecoratorClass();
        $decorator = null;

        if (class_exists($decoratorClass)) {
            $formData = new $decoratorClass($formData);
        }

        $singlePhaseData->setFormPhaseData($formData);

        return $singlePhaseData;
    }

    /**
     * group_phase_data = {
     *   "data": {
     *     [string]: single_phase_data
     *   },
     *   "title": string | null,
     *   "locked": boolean | null
     * }
     *
     * @param GroupPhase $phase
     * @param array      $phaseData
     * @param DataNode   $parent
     *
     * @return GroupPhaseDataNode
     */
    public function createGroupPhaseDataNode(GroupPhase $phase, $id, array $phaseData, DataNode $parent)
    {
        $data = isset($phaseData['data']) ? $phaseData['data'] : [];
        $locked = isset($phaseData['locked']) ? $phaseData['locked'] : false;
        $title = isset($phaseData['title']) ? $phaseData['title'] : null;

        $groupPhaseNode = new GroupPhaseDataNode($id, $title);
        $groupPhaseNode->setLocked($locked);
        $groupPhaseNode->setPhaseNode($phase);
        $groupPhaseNode->setParent($parent);

        foreach ($phase->getPhases() as $ref => $singlePhase) {
            $singlePhaseData = isset($data[$ref]) ? $data[$ref] : [];

            $groupPhaseNode->addChild($ref, $this->createSinglePhaseDataNode($singlePhase, $ref, $singlePhaseData, $groupPhaseNode));
        }

        return $groupPhaseNode;
    }

    /**
     * product_list_data = {
     *    "data": [product_phase_data],
     *    "title": string | null,
     *    "locked": boolean | null
     * }
     *
     * @param ProductListPhase $phase
     * @param array            $phaseData
     * @param DataNode         $parent
     *
     * @return ProductPhaseListDataNode
     */
    public function createProductListPhaseDataNode(ProductListPhase $phase, $id, array $phaseData, DataNode $parent)
    {
        $data = isset($phaseData['data']) ? $phaseData['data'] : [];

        $productListNode = new ProductPhaseListDataNode($id);
        $productListNode->setPhaseNode($phase);
        $productListNode->setParent($parent);

        $index = 0;

        $productPhases = $phase->getProducts();

        foreach ($data as $productData) {
            $productRef = $productData['ref'];
            $productPhase = $productPhases[$productRef];

            $productNode = $this->createProductPhaseDataNode($productPhase, $index, $productData, $productListNode);
            $productListNode->addChild($index, $productNode);

            $index++;
        }

        return $productListNode;
    }

    /**
     * product_phase_data = {
     *   "data": {
     *     "ref": string,
     *     "data": group_phase_data
     *   }
     * }
     *
     * @param ProductPhase $phase
     * @param array        $phaseData
     * @param DataNode     $parent
     *
     * @return ProductPhaseDataNode
     */
    public function createProductPhaseDataNode(ProductPhase $phase, $id, array $phaseData, DataNode $parent)
    {
        $locked = isset($phaseData['locked']) ? $phaseData['locked'] : false;
        $data = isset($phaseData['data']) ? $phaseData['data'] : [];
        $title = isset($phaseData['title']) ? $phaseData['title'] : null;

        $productNode = new ProductPhaseDataNode($id, $title);

        $productNode->setPhaseNode($phase);
        $productNode->setParent($parent);
        $productNode->setLocked($locked);

        foreach ($phase->getPhases() as $ref => $singlePhase) {
            $singlePhaseData = isset($data[$ref]) ? $data[$ref] : [];

            $productNode->addChild($ref, $this->createSinglePhaseDataNode($singlePhase, $ref, $singlePhaseData, $productNode));
        }

        return $productNode;
    }
}
