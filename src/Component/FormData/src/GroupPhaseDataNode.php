<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\FormData;

use Klaro\Component\ProductPhase\GroupPhase;
use Klaro\Component\ProductPhase\PhaseNode;

class GroupPhaseDataNode extends LockableDataNode
{
    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        $data = [];

        if ($this->hasChildren()) {
            foreach ($this->getChildren() as $ref => $node) {
                $data[$ref] = $node->toArray();
            }
        }

        return parent::toArray() + [
            'data' => $data,
        ];
    }

    public function getSinglePhaseByPath($path)
    {
        $result = $this->getChild($path);

        if (!$result && $this->hasChildren()) {
            list($result) = $this->children;
        }

        return $result;
    }
}
