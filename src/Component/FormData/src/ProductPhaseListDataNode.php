<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\FormData;

use KKlaro\Component\Common\Exception\ExceptionFactory;
use Klaro\Component\ProductPhase\ProductListPhase;
use Klaro\Component\Util\ArrayUtil;

class ProductPhaseListDataNode extends DataNode
{
    /**
     * @param FormDataNodeFactory $factory
     * @param $productRef
     * @param array               $initialData
     *
     * @return ProductPhaseDataNode
     */
    public function addProduct(FormDataNodeFactory $factory, $productRef, $initialData = [])
    {
        $index = is_array($this->children) ? count($this->children) : 0;

        /** @var ProductListPhase $phase */
        $phase = $this->getPhaseNode();
        $products = $phase->getProducts();

        if (!isset($products[$productRef])) {
            throw ExceptionFactory::productNotFound($productRef);
        }

        if (!isset($initialData['title'])|| empty($initialData['title'])) {
            $initialData['title'] = $products[$productRef]->getTitle().' '.($index + 1);
        }

        $node = $factory->createProductPhaseDataNode($products[$productRef], $index, $initialData, $this);

        $this->addChild($index, $node);

        return $node;
    }

    /**
     * Reorder the product
     * @param array $products Array of objects, with shape
     *
     *                        products = [product]
     *                        product = {
     *                        source_id = string,   // required, id of the product to copy data from
     *                        title = string        // optional, new title for the line
     *                        }
     */
    public function reorderProducts(array $products)
    {
        $originalChildren = $this->getChildren();

        $newChildren = [];

        foreach ($products as $newOrderItem) {
            $originalId = $newOrderItem->source_id;

            $newChild = clone $originalChildren[$originalId];

            if (!empty($newOrderItem->title)) {
                $newChild->setTitle($newOrderItem->title);
            }

            $newChildren[] = $newChild;

            $newChild->setId(count($newChildren) - 1);
        }

        $this->setChildren($newChildren);
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        $data = [];

        if ($this->hasChildren()) {
            foreach ($this->getChildren() as $productNode) {
                $data[] = $productNode->toArray();
            }
        }

        return [
            'data' => $data,
        ] + parent::toArray();
    }

    public function getSinglePhaseByPath($path)
    {
        $delimiterPos = strpos($path, DataNodeInterface::PATH_DELIMITER);

        if ($delimiterPos === false) {
            $ref     = (int) $path;
            $child   = $this->getChild($ref);
            $result  = $child ? $child->current() : null;
        } else {
            $subPath = substr($path, $delimiterPos + 1);
            $ref     = (int) substr($path, 0, $delimiterPos);
            $child   = $this->getChild($ref);
            $result  = $child ? $child->getSinglePhaseByPath($subPath) : null;
        }

        return $result;
    }
}
