<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\FormData;

use Klaro\Component\ProductPhase\PhaseNode;

interface DataNodeInterface extends \ArrayAccess, \Iterator, \Countable
{
    const PATH_DELIMITER = '/';

    /**
     * @return bool
     */
    public function hasChildren();

    /**
     * @return DataNodeInterface[]|null
     */
    public function getChildren();

    /**
     * @param DataNodeInterface[]|null $children
     */
    public function setChildren($children);

    /**
     * @param mixed             $ref
     * @param DataNodeInterface $child
     *
     * @return DataNodeInterface
     */
    public function addChild($ref, DataNodeInterface $child);

    /**
     * @param mixed $ref
     *
     * @return DataNodeInterface
     */
    public function getChild($ref);

    /**
     * @return string
     */
    public function getId();

    /**
     * @param $id
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getPath();

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param $title
     */
    public function setTitle($title);

    /**
     * @param PhaseNode $phaseNode
     */
    public function setPhaseNode(PhaseNode $phaseNode);

    /**
     * @return PhaseNode
     */
    public function getPhaseNode();

    /**
     * @param $path
     *
     * @return SinglePhaseDataNode
     */
    public function getSinglePhaseByPath($path);

    /**
     * @return DataNodeInterface
     */
    public function getTopLevelNode();

    /**
     * @return DataNodeInterface|null
     */
    public function getParent();

    /**
     * @return array
     */
    public function toArray();
}
