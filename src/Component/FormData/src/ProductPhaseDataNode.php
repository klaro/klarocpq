<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\FormData;

class ProductPhaseDataNode extends GroupPhaseDataNode
{
    /**
     * @return string
     */
    public function getProductRef()
    {
        return $this->phaseNode->getRef();
    }

    public function toArray()
    {
        return [
            'ref' => $this->getProductRef(),
        ] + parent::toArray();
    }
}
