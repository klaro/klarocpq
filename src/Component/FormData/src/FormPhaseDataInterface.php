<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\FormData;

use Klaro\Component\Common\Model\ModelInterface;

interface FormPhaseDataInterface extends ModelInterface, \ArrayAccess
{
    public function commit();

    public function getValues(array $fields): array;
    public function setValues(array $array): void;
    public function getChangedValues();

    public function hasChanged($field, $hasAppeared = true): bool;
    public function hasChangedFields(array $fields, $hasAppeared = true): bool;
    public function hasAnyChangedFields(): bool;
    public function setIfChanged($field, $value, $sanitize = true): bool;
    public function getValuesUpdated($hasAppeared = true): array;
    public function hasAppeared($field): bool;
}
