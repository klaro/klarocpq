<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\FormData;

use Klaro\Component\Common\Exception\ExceptionFactory;
use Klaro\Component\ProductPhase\PhaseNode;
use Klaro\Component\ProductPhase\ProductLine;
use Klaro\Component\ProductPhase\ProductListPhase;

class ProductLineDataNode extends DataNode
{
    /**
     * @param $path
     *
     * @return DataNode|SinglePhaseDataNode
     */
    public function getSinglePhaseByPath($path)
    {
        $delimiterPos = strpos($path, DataNodeInterface::PATH_DELIMITER);

        if (false === $delimiterPos) {
            return $this->getCurrentDataNode($path);
        }

        $subPath = substr($path, $delimiterPos + 1);
        $ref     = substr($path, 0, $delimiterPos);
        $child   = $this->getChild($ref);
        $result  = $child ? $child->getSinglePhaseByPath($subPath) : null;

        return $result;
    }

    /**
     * @param string|null $path
     *
     * @return DataNode
     */
    private function getCurrentDataNode(string $path = null)
    {
        /** @var DataNode $currentPath */
        $currentPath = $this[$path] ?? $this->current();

        return $currentPath instanceof SinglePhaseDataNode ? $currentPath : $currentPath->current();
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        $result = [];

        if ($this->hasChildren()) {
            foreach ($this->getChildren() as $ref => $node) {
                $result[$ref] = $node->toArray();
            }
        }

        return $result;
    }

    /**
     * @param $phasePath
     * @param FormPhaseData $data
     */
    public function setFormData($phasePath, FormPhaseData $data)
    {
        $singlePhaseNode = $this->getSinglePhaseByPath($phasePath);

        $singlePhaseNode->setFormPhaseData($data);
    }

    /**
     * @param $phasePath
     *
     * @return FormPhaseData
     */
    public function getFormData($phasePath)
    {
        return $this->getSinglePhaseByPath($phasePath)->getData();
    }

    /**
     * @param $phaseId
     * @param $productRef
     * @param array      $initialData
     *
     * @return ProductPhaseDataNode
     */
    public function addProduct($phaseId, $productRef, $initialData = [])
    {
        /** @var ProductPhaseListDataNode $productListNode */
        $productListNode = $this[$phaseId];

        /** @var ProductListPhase $phase */
        $phase = $productListNode->getPhaseNode();

        if (!$productListNode || $phase->getType() !== PhaseNode::PRODUCT_LIST) {
            throw ExceptionFactory::unexpectedPhaseType($phase->getType());
        }

        /** @var ProductLine $productLinePhase */
        $productLinePhase = $this->getPhaseNode();

        return $productListNode->addProduct(
            FormDataNodeFactory::create($productLinePhase),
            $productRef,
            $initialData
        );
    }

    /**
     * @param $phaseId
     * @param array   $newOrder
     *
     * @see ProductPhaseListDataNode::reorderProducts()
     */
    public function reorderProducts($phaseId, array $newOrder)
    {
        /** @var ProductPhaseListDataNode $productListNode */
        $productListNode = $this[$phaseId];

        /** @var ProductListPhase $phase */
        $phase = $productListNode->getPhaseNode();

        if (!$productListNode || $phase->getType() !== PhaseNode::PRODUCT_LIST) {
            throw ExceptionFactory::unexpectedPhaseType($phase->getType());
        }

        $productListNode->reorderProducts($newOrder);
    }
}
