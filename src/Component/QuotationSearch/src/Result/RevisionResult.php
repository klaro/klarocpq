<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Result;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\Common\Result\Result;
use Klaro\Component\QuotationSearch\Query\QuotationEntities;
use Klaro\Component\QuotationSearch\Query\RevisionQueryFields;

/**
 * Class RevisionResult
 *
 * @package Klaro\Component\QuotationSearch
 */
class RevisionResult extends Result implements QuotationRevisionInterface
{
    /**
     * @var QuotationInterface
     */
    protected $quotation;

    /**
     * @param QuotationInterface $quotation
     * @param null               $values
     */
    public function __construct(QuotationInterface $quotation, $values)
    {
        $this->quotation = $quotation;

        parent::__construct($values[QuotationEntities::REVISION], RevisionQueryFields::REVISION_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotation(): QuotationInterface
    {
        return $this->quotation;
    }

    /**
     * {@inheritDoc}
     */
    public function setQuotation(QuotationInterface $quotation)
    {
        $this->quotation = $quotation;
    }

    /**
     * {@inheritDoc}
     */
    public function getIdentifier()
    {
        return $this->get(RevisionQueryFields::IDENTIFIER);
    }

    /**
     * {@inheritDoc}
     */
    public function setIdentifier($identifier)
    {
        $this->set(RevisionQueryFields::IDENTIFIER, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function getRevisionId()
    {
        return $this->get(RevisionQueryFields::REVISION_ID);
    }

    /**
     * {@inheritDoc}
     */
    public function setRevisionId($revisionId)
    {
        $this->set(RevisionQueryFields::REVISION_ID, $revisionId);
    }

    /**
     * {@inheritDoc}
     */
    public function getQualifier($name)
    {
        $qualifiers = $this->getQualifiers();

        return $qualifiers[$name] ?? null;
    }

    /**
     * {@inheritDoc}
     */
    public function getQualifiers()
    {
        return $this->get(RevisionQueryFields::QUALIFIERS);
    }

    /**
     * {@inheritDoc}
     */
    public function setQualifier($name, $value)
    {
        $qualifiers = $this->getQualifiers();

        if (!is_array($qualifiers)) {
            $qualifiers = [];
        }

        $qualifiers[$name] = $value;

        $this->setQualifiers($qualifiers);
    }

    /**
     * {@inheritDoc}
     */
    public function setQualifiers($qualifiers)
    {
        $this->set(RevisionQueryFields::QUALIFIERS, $qualifiers);
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus()
    {
        return $this->get(RevisionQueryFields::STATUS);
    }

    /**
     * {@inheritDoc}
     */
    public function setStatus($status)
    {
        $this->set(RevisionQueryFields::STATUS, $status);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrderStatus()
    {
        return $this->get(RevisionQueryFields::ORDER_STATUS);
    }

    /**
     * {@inheritDoc}
     */
    public function setOrderStatus($status)
    {
        $this->set(RevisionQueryFields::ORDER_STATUS, $status);
    }

    /**
     * {@inheritDoc}
     */
    public function getLogMessage()
    {
        return $this->get(RevisionQueryFields::LOG_MESSAGE);
    }

    /**
     * {@inheritDoc}
     */
    public function setLogMessage($message)
    {
        $this->set(RevisionQueryFields::LOG_MESSAGE, $message);
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle()
    {
        return $this->get(RevisionQueryFields::TITLE);
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle($title)
    {
        $this->set(RevisionQueryFields::TITLE, $title);
    }

    /**
     * {@inheritDoc}
     */
    public function isValid()
    {
        @trigger_error('Deprecated method', E_USER_DEPRECATED);
    }

    /**
     * {@inheritDoc}
     */
    public function setIsValid($valid)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getOfferingSummary()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function setOfferingSummary($summary)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->get(RevisionQueryFields::CREATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($value)
    {
        $this->set(RevisionQueryFields::CREATED_AT, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {
        return $this->get(RevisionQueryFields::UPDATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($value)
    {
        $this->set(RevisionQueryFields::UPDATED_AT, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getUser()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function setUser(QuotationUserInterface $user)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getSalesStructure()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function setSalesStructure($salesStructure)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getProductItems()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function setProductItems($productItems)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getPhases()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function setPhases($phases)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getCalculatedSalesPriceWithTax()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function setCalculatedSalesPriceWithTax($salesPrice)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getTargetSalesPriceWithTax()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function setTargetSalesPriceWithTax($salesPrice)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getCalculatedCostPrice()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function setCalculatedCostPrice($costPrice)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrency()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function setCurrency($currency)
    {
    }

    /**
     * {@inheritDoc}
     */
    public function getMetaData($name)
    {
        $metadata = $this->getMetaDatas();

        return $metadata[$name] ?? null;
    }

    /**
     * {@inheritDoc}
     */
    public function getMetaDatas()
    {
        return $this->get(RevisionQueryFields::METADATA);
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaData($name, $value)
    {
        $metadata = $this->getMetaDatas();

        if (!is_array($metadata)) {
            $metadata = [];
        }

        $metadata[$name] = $value;

        $this->setMetaDatas($metadata);
    }

    /**
     * {@inheritDoc}
     */
    public function setMetaDatas($metadata)
    {
        $this->set(RevisionQueryFields::METADATA, $metadata);
    }

    /**
     * @return null|string
     */
    public function getProductLineVersion(): ?string
    {
        return $this->get(RevisionQueryFields::PRODUCT_LINE_VERSION);
    }

    /**
     * @param string $version
     */
    public function setProductLineVersion($version)
    {
        $this->set(RevisionQueryFields::PRODUCT_LINE_VERSION, $version);
    }

    /**
     * @return null|array
     */
    public function getPhaseData(): ?array
    {
        return null;
    }

    /**
     * @param array $phaseData
     */
    public function setPhaseData(array $phaseData)
    {
    }

    /**
     * @return null|string
     */
    public function getProductLine(): ?string
    {
        return $this->get(RevisionQueryFields::PRODUCT_LINE);
    }

    /**
     * @param string $productLine
     */
    public function setProductLine($productLine)
    {
        $this->set(RevisionQueryFields::PRODUCT_LINE, $productLine);
    }

    /**
     * @return null|string
     */
    public function getConfigurator(): ?string
    {
        return $this->get(RevisionQueryFields::CONFIGURATOR);
    }

    /**
     * @param string $configurator
     */
    public function setConfigurator($configurator)
    {
        $this->set(RevisionQueryFields::CONFIGURATOR, $configurator);
    }

    /**
     * @return null|string
     */
    public function getConfiguratorVersion(): ?string
    {
        return $this->get(RevisionQueryFields::CONFIGURATOR_VERSION);
    }

    /**
     * @param string $version
     */
    public function setConfiguratorVersion($version)
    {
        $this->set(RevisionQueryFields::CONFIGURATOR_VERSION, $version);
    }
}
