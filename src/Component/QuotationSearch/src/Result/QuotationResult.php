<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Result;

use Klaro\Component\Common\Model\QuotationInterface;
use Klaro\Component\Common\Model\QuotationRevisionInterface;
use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\Common\Result\Result;
use Klaro\Component\QuotationSearch\Query\QuotationEntities;
use Klaro\Component\QuotationSearch\Query\QuotationQueryFields;

/**
 * Class QuotationResult
 *
 * @package Klaro\Component\QuotationSearch\Quotation
 */
class QuotationResult extends Result implements QuotationInterface
{
    protected $latestRevision;

    protected $owner;

    /**
     * @param $values
     */
    public function __construct($values)
    {
        if (array_key_exists(QuotationEntities::REVISION, $values)) {
            $this->latestRevision = new RevisionResult($this, $values);
        }

        if (array_key_exists(QuotationEntities::USER, $values)) {
            $this->owner = new UserResult($values);
        }

        parent::__construct($values[QuotationEntities::QUOTATION], QuotationQueryFields::ID);
    }

    /**
     * {@inheritDoc}
     */
    public function getLatestRevision()
    {
        return $this->latestRevision;
    }

    /**
     * {@inheritDoc}
     */
    public function setLatestRevision(QuotationRevisionInterface $revision)
    {
        $this->latestRevision = $revision;
    }

    /**
     * {@inheritDoc}
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * {@inheritDoc}
     */
    public function setOwner(QuotationUserInterface $owner)
    {
        $this->owner = $owner;
    }

    /**
     * {@inheritDoc}
     */
    public function getIdentifier()
    {
        return $this->get(QuotationQueryFields::IDENTIFIER);
    }

    /**
     * {@inheritDoc}
     */
    public function setIdentifier($identifier)
    {
        $this->set(QuotationQueryFields::IDENTIFIER, $identifier);
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle()
    {
        return $this->get(QuotationQueryFields::TITLE);
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle($value)
    {
        $this->set(QuotationQueryFields::TITLE, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotationType()
    {
        return $this->get(QuotationQueryFields::QUOTATION_TYPE);
    }

    /**
     * {@inheritDoc}
     */
    public function setQuotationType($value)
    {
        $this->set(QuotationQueryFields::QUOTATION_TYPE, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {
        return $this->get(QuotationQueryFields::CREATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($value)
    {
        $this->set(QuotationQueryFields::CREATED_AT, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {
        return $this->get(QuotationQueryFields::UPDATED_AT);
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($value)
    {
        $this->set(QuotationQueryFields::UPDATED_AT, $value);
    }
}
