<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Result;

use Klaro\Component\Common\Model\QuotationUserInterface;
use Klaro\Component\Common\Result\Result;
use Klaro\Component\QuotationSearch\Query\QuotationEntities;
use Klaro\Component\QuotationSearch\Query\UserQueryFields;

/**
 * Class UserResult
 *
 * @package Klaro\Component\QuotationSearch
 */
class UserResult extends Result implements QuotationUserInterface
{
    /**
     * @param $values
     */
    public function __construct($values)
    {
        parent::__construct($values[QuotationEntities::USER], UserQueryFields::ID);
    }

    /**
     * {@inheritDoc}
     */
    public function getProfileTitle()
    {
        @trigger_error('Deprecated method', E_USER_DEPRECATED);
    }

    /**
     * {@inheritDoc}
     */
    public function getFullname(): string
    {
        return $this->getFirstName().' '.$this->getLastName();
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstName()
    {
        return $this->get(UserQueryFields::FIRST_NAME);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastName()
    {
        return $this->get(UserQueryFields::LAST_NAME);
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail()
    {
        return $this->get(UserQueryFields::EMAIL);
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {
        return $this->get(UserQueryFields::ROLES);
    }

    /**
     * {@inheritDoc}
     */
    public function getPassword()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {
        return $this->get(UserQueryFields::USERNAME);
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials(): void
    {
        // do nothing.
    }
}
