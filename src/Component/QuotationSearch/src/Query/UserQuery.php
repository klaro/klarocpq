<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Query;

use Klaro\Component\Common\Query\QueryCriteria;

/**
 * Class for querying quotation bundle users.
 *
 * Class UserQuery
 *
 * @package Klaro\Component\QuotationSearch
 */
class UserQuery extends Query implements UserQueryInterface
{
    /**
     * @return UserQuery
     */
    public static function create(): UserQuery
    {
        return new static;
    }

    /**
     * {@inheritDoc}
     */
    public function setFirstName(string $name, string $comparison = QueryCriteria::EQUALS)
    {
        $this->setCriteria('FirstName', $name, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstName(): ?QueryCriteria
    {
        return $this->getCriteria('FirstName');
    }

    /**
     * {@inheritDoc}
     */
    public function setLastName(string $name, string $comparison = QueryCriteria::EQUALS)
    {
        $this->setCriteria('LastName', $name, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getLastName(): ?QueryCriteria
    {
        return $this->getCriteria('LastName');
    }


    /**
     * {@inheritDoc}
     */
    public function setFullName(string $name, string $comparison = QueryCriteria::EQUALS)
    {
        $this->setCriteria('FullName', $name, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getFullName(): ?QueryCriteria
    {
        return $this->getCriteria('FullName');
    }


    /**
     * {@inheritDoc}
     */
    public function setEmail(string $email, string $comparison = QueryCriteria::EQUALS)
    {
        $this->setCriteria('Email', $email, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getEmail(): ?QueryCriteria
    {
        return $this->getCriteria('Email');
    }
}
