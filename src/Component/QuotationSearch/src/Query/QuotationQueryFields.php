<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Query;

/**
 * Class QuotationQueryFields
 *
 * @package Klaro\Component\QuotationSearch
 */
final class QuotationQueryFields
{
    public const ID = 'Id';
    public const IDENTIFIER = 'Identifier';
    public const TITLE = 'Title';
    public const QUOTATION_TYPE = 'QuotationType';
    public const OWNER = 'Owner';
    public const HANDLER = 'Handler';
    public const CREATED_AT = 'CreatedAt';
    public const UPDATED_AT = 'UpdatedAt';
    public const LATEST_REVISION_ID = 'LatestRevision';

    /**
     * @return array
     */
    public static function getAllFields(): array
    {
        return [
            self::ID,
            self::IDENTIFIER,
            self::TITLE,
            self::QUOTATION_TYPE,
            self::CREATED_AT,
            self::UPDATED_AT,
        ];
    }
}
