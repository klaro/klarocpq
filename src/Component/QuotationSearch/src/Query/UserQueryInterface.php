<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Query;

use Klaro\Component\Common\Query\QueryCriteria;

/**
 * Interface for user queries.
 *
 * Interface UserQueryInterface
 *
 * @package Klaro\Component\QuotationSearch
 */
interface UserQueryInterface extends QueryInterface
{
    /**
     * Set first name.
     *
     * @param string $name
     * @param string $comparison
     *
     * @return UserQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setFirstName(string $name, string $comparison = QueryCriteria::EQUALS);

    /**
     * Get first name.
     *
     * @return null|QueryCriteria
     */
    public function getFirstName(): ?QueryCriteria;

    /**
     * Set last name.
     *
     * @param string $name
     * @param string $comparison
     *
     * @return UserQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setLastName(string $name, string $comparison = QueryCriteria::EQUALS);

    /**
     * Get last name.
     *
     * @return null|QueryCriteria
     */
    public function getLastName(): ?QueryCriteria;

    /**
     * Set full name.
     *
     * @param $name
     * @param string $comparison
     *
     * @return UserQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setFullName(string $name, string $comparison = QueryCriteria::EQUALS);

    /**
     * Get full name.
     *
     * @return null|QueryCriteria
     */
    public function getFullName(): ?QueryCriteria;

    /**
     * Set email.
     *
     * @param $email
     * @param string $comparison
     *
     * @return UserQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setEmail(string $email, string $comparison = QueryCriteria::EQUALS);

    /**
     * Get email.
     *
     * @return null|QueryCriteria
     */
    public function getEmail(): ?QueryCriteria;
}
