<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Query;

use Klaro\Component\Common\Query\QueryInterface as BaseQueryInterface;

/**
 * Base interface for quotation content queries.
 *
 * Interface QueryInterface
 *
 * @package Klaro\Component\QuotationSearch
 */
interface QueryInterface extends BaseQueryInterface
{
    /**
     * @param array $fields
     *
     * @return QueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setQuotationFields(array $fields = []);

    /**
     * @return array
     */
    public function getQuotationFields(): array;

    /**
     * @param array $fields
     *
     * @return QueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setRevisionFields(array $fields = []);

    /**
     * @return array
     */
    public function getRevisionFields(): array;

    /**
     * @param array $fields
     *
     * @return QueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setUserFields(array $fields = []);

    /**
     * @return array
     */
    public function getUserFields(): array;
}
