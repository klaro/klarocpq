<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Query;

use Klaro\Component\Common\Query\Query as BaseQuery;

/**
 * Base class for querying quotation content.
 *
 * Class Query
 *
 * @package Klaro\Component\QuotationSearch
 */
class Query extends BaseQuery implements QueryInterface
{
    /**
     * @param array $fields
     *
     * @return Query
     */
    public function setQuotationFields(array $fields = []): Query
    {
        $this->setResultFields(QuotationEntities::QUOTATION, $fields);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotationFields(): array
    {
        return $this->getResultFields(QuotationEntities::QUOTATION);
    }

    /**
     * {@inheritDoc}
     */
    public function setUserFields(array $fields = []): Query
    {
        $this->setResultFields(QuotationEntities::USER, $fields);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getUserFields(): array
    {
        return $this->getResultFields(QuotationEntities::USER);
    }

    /**
     * {@inheritDoc}
     */
    public function setRevisionFields(array $fields = []): Query
    {
        $this->setResultFields(QuotationEntities::REVISION, $fields);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getRevisionFields(): array
    {
        return $this->getResultFields(QuotationEntities::REVISION);
    }
}
