<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Query;

use Klaro\Component\Common\Query\QueryCriteria;

/**
 * Interface for quotation queries.
 *
 * Interface QuotationQueryInterface
 *
 * @package Klaro\Component\QuotationSearch\Quotation
 */
interface QuotationQueryInterface extends QueryInterface
{
    /**
     * Set the owner user id.
     *
     * @param int    $userId
     * @param string $comparison
     *
     * @return QuotationQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setOwner(int $userId, string $comparison = QueryCriteria::EQUALS);

    /**
     * Get the owner user id.
     *
     * @return null|QueryCriteria
     */
    public function getOwner(): ?QueryCriteria;

    /**
     * @return bool
     */
    public function hasOwner(): bool;

    /**
     * Set the handler user id.
     *
     * @param int    $userId
     * @param string $comparison
     *
     * @return QuotationQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setHandler(int $userId, string $comparison = QueryCriteria::EQUALS);

    /**
     * Get the handler user id.
     *
     * @return null|QueryCriteria
     */
    public function getHandler(): ?QueryCriteria;

    /**
     * @return bool
     */
    public function hasHandler(): bool;

    /**
     * @param int    $id
     * @param string $comparison
     *
     * @return QuotationQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setId(int $id, string $comparison = QueryCriteria::EQUALS);

    /**
     * @return null|QueryCriteria
     */
    public function getId(): ?QueryCriteria;

    /**
     * @return bool
     */
    public function hasId(): bool;

    /**
     * @return null|QueryCriteria
     */
    public function getTitle(): ?QueryCriteria;

    /**
     * @param string $title
     * @param string $comparison
     *
     * @return QuotationQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setTitle(string $title, string $comparison = QueryCriteria::EQUALS);

    /**
     * @return bool
     */
    public function hasTitle(): bool;

    /**
     * @return null|QueryCriteria
     */
    public function getIdentifier(): ?QueryCriteria;

    /**
     * @param string $identifier
     * @param string $comparison
     *
     * @return QuotationQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setIdentifier(string $identifier, string $comparison = QueryCriteria::EQUALS);

    /**
     * @return bool
     */
    public function hasIdentifier(): bool;

    /**
     * Set quotation type.
     *
     * @param string $quotationType
     * @param string $comparison
     *
     * @return QuotationQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setQuotationType(string $quotationType, string $comparison = QueryCriteria::EQUALS);

    /**
     * Get quotation type.
     *
     * @return null|QueryCriteria
     */
    public function getQuotationType(): ?QueryCriteria;

    /**
     * @return bool
     */
    public function hasQuotationType(): bool;

    /**
     * Set the created at time.
     *
     * @param string|string[] $createdAt
     * @param string          $comparison
     *
     * @return QuotationQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setCreatedAt($createdAt, string $comparison = QueryCriteria::GREATER_EQUAL);

    /**
     * Get the created at time.
     *
     * @return null|QueryCriteria
     */
    public function getCreatedAt(): ?QueryCriteria;

    /**
     * @return bool
     */
    public function hasCreatedAt(): bool;

    /**
     * Set the updated at time.
     *
     * @param string|string[] $updatedAt
     * @param string          $comparison
     *
     * @return QuotationQueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setUpdatedAt($updatedAt, string $comparison = QueryCriteria::GREATER_EQUAL);

    /**
     * Get the updated at time.
     *
     * @return null|QueryCriteria
     */
    public function getUpdatedAt(): ?QueryCriteria;

    /**
     * @return bool
     */
    public function hasUpdatedAt(): bool;
}
