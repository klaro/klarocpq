<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Query;

use function Complex\sec;

/**
 * Class RevisionQueryFields
 *
 * @package Klaro\Component\QuotationSearch
 */
final class RevisionQueryFields
{
    const QUOTATION_ID         = 'QuotationId';
    const REVISION_ID          = 'RevisionId';
    const USER_ID              = 'UserId';
    const STATUS               = 'Status';
    const IDENTIFIER           = 'Ridentifier';
    const TITLE                = 'Title';
    const ORDER_STATUS         = 'OrderStatus';
    const METADATA             = 'Metadata';
    const QUALIFIERS           = 'Qualifiers';
    const LOG_MESSAGE          = 'LogMessage';
    const PRODUCT_LINE         = 'ProductLine';
    public const PRODUCT_LINE_VERSION = 'ProductLineVersion';
    const CONFIGURATOR         = 'Configurator';
    public const CONFIGURATOR_VERSION = 'ConfiguratorVersion';
    const CREATED_AT           = 'CreatedAt';
    const UPDATED_AT           = 'UpdatedAt';
    const PROPOSAL_TYPE        = 'proposalType';

    /**
     * @return array
     */
    public static function getAllFields(): array
    {
        return [
            self::QUOTATION_ID,
            self::REVISION_ID,
            self::USER_ID,
            self::STATUS,
            self::IDENTIFIER,
            self::TITLE,
            self::ORDER_STATUS,
            self::METADATA,
            self::QUALIFIERS,
            self::LOG_MESSAGE,
            self::PRODUCT_LINE,
            self::PRODUCT_LINE_VERSION,
            self::CONFIGURATOR,
            self::CONFIGURATOR_VERSION,
            self::CREATED_AT,
            self::UPDATED_AT,
            self::PROPOSAL_TYPE,
        ];
    }
}
