<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Query;

/**
 * Mappings for different quotation entities that can be used as aliases in a database query. The values should be
 * unique so that the entities can be separated back to the right entities from a flat result list.
 *
 * Class QuotationEntities
 *
 * @package Klaro\Component\QuotationSearch
 */
final class QuotationEntities
{
    public const QUOTATION = 'q';
    public const REVISION = 'r';
    public const USER = 'u';
}
