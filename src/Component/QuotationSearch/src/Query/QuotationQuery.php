<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Query;

use Klaro\Component\Common\Query\QueryCriteria;
use Klaro\Component\Common\Query\QueryParam;

/**
 * Class for querying quotations.
 *
 * Class QuotationQuery
 *
 * @package Klaro\Component\QuotationSearch
 */
class QuotationQuery extends Query implements QuotationQueryInterface
{
    /** @var QueryCriteria */
    protected $criteria;

    /** @var QueryParam */
    protected $params;

    /**
     *
     */
    protected function __construct()
    {
        parent::__construct();

        $this->criteria = [];
        $this->params = [];
    }

    /**
     * @return QuotationQuery
     */
    public static function create(): QuotationQuery
    {
        return new self;
    }

    /**
     * {@inheritDoc}
     *
     * @return $this
     */
    public function setStatus($val, $comparison = QueryCriteria::EQUALS)
    {
        $this->setCriteria('status', $val, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus() {
        return $this->getCriteria('status');
    }

    /**
     * {@inheritDoc}
     *
     * @return $this
     */
    public function setMetadata($value, $comparison = QueryCriteria::EQUALS)
    {
        $this->setCriteria('metadata', $value, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getMetadata()
    {
        return $this->getCriteria('metadata');
    }

    /**
     * {@inheritDoc}
     *
     * @return QuotationQuery
     */
    public function setOwner(int $userId, string $comparison = QueryCriteria::EQUALS): QuotationQuery
    {
        $this->setCriteria('owner', $userId, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getOwner(): ?QueryCriteria
    {
        return $this->getCriteria('owner');
    }

    /**
     * {@inheritDoc}
     */
    public function hasOwner(): bool
    {
        return $this->hasCriteria('owner');
    }

    /**
     * {@inheritDoc}
     *
     * @return $this
     */
    public function setHandler(int $userId, string $comparison = QueryCriteria::EQUALS): QuotationQuery
    {
        $this->setCriteria('handler', $userId, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getHandler(): ?QueryCriteria
    {
        return $this->getCriteria('handler');
    }

    /**
     * {@inheritDoc}
     */
    public function hasHandler(): bool
    {
        return $this->hasCriteria('handler');
    }

    /**
     * {@inheritDoc}
     */
    public function setId(int $id, string $comparison = QueryCriteria::EQUALS)
    {
        $this->setCriteria('id', $id, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getId(): ?QueryCriteria
    {
        return $this->getCriteria('id');
    }

    /**
     * {@inheritDoc}
     */
    public function hasId(): bool
    {
        return $this->hasCriteria('id');
    }

    /**
     * {@inheritDoc}
     */
    public function getTitle(): ?QueryCriteria
    {
        return $this->getCriteria('title');
    }

    /**
     * {@inheritDoc}
     */
    public function setTitle(string $title, string $comparison = QueryCriteria::EQUALS)
    {
        $this->setCriteria('title', $title, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function hasTitle(): bool
    {
        return $this->hasCriteria('title');
    }

    /**
     * {@inheritDoc}
     */
    public function getIdentifier(): ?QueryCriteria
    {
        return $this->getCriteria('Identifier');
    }

    /**
     * {@inheritDoc}
     */
    public function setIdentifier(string $identifier, string $comparison = QueryCriteria::EQUALS)
    {
        $this->setCriteria('Identifier', $identifier, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function setRIdentifier($identifier, $comparison = QueryCriteria::EQUALS)
    {
        $this->setCriteria('r_identifier', $identifier, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getRIdentifier()
    {
        return $this->getCriteria('r_identifier');
    }

    /**
     * {@inheritDoc}
     */
    public function hasIdentifier(): bool
    {
        return $this->hasCriteria('identifier');
    }

    /**
     * {@inheritDoc}
     */
    public function setProposalType($val, $comparison = QueryCriteria::JSON_EXTRACT)
    {
        if (is_array($val)) {
            $this->setCriteria('qualifiers', $val, $comparison);
        } else {
            $this->setCriteria('qualifiers', ['$.type', $val], $comparison);
        }

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getProposalType()
    {
        return $this->getCriteria('qualifiers');
    }

    /**
     * {@inheritDoc}
     */
    public function setQuotationType(string $quotationType, string $comparison = QueryCriteria::EQUALS): QuotationQuery
    {
        $this->setCriteria('quotationType', $quotationType, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getQuotationType(): ?QueryCriteria
    {
        return $this->getCriteria('quotationType');
    }

    /**
     * {@inheritDoc}
     */
    public function hasQuotationType(): bool
    {
        return $this->hasCriteria('quotationType');
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt($createdAt, string $comparison = QueryCriteria::GREATER_EQUAL): QuotationQuery
    {
        $this->setCriteria('createdAt', $createdAt, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt(): ?QueryCriteria
    {
        return $this->getCriteria('createdAt');
    }

    /**
     * {@inheritDoc}
     */
    public function hasCreatedAt(): bool
    {
        return $this->hasCriteria('createdAt');
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt($updatedAt, string $comparison = QueryCriteria::GREATER_EQUAL): QuotationQuery
    {
        $this->setCriteria('updatedAt', $updatedAt, $comparison);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt(): ?QueryCriteria
    {
        return $this->getCriteria('updatedAt');
    }

    /**
     * {@inheritDoc}
     */
    public function hasUpdatedAt(): bool
    {
        return $this->hasCriteria('updatedAt');
    }
}
