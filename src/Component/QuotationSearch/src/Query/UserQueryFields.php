<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\QuotationSearch\Query;

/**
 * Class UserQueryFields
 *
 * @package Klaro\Component\QuotationSearch
 */
final class UserQueryFields
{
    public const ID = 'Id';
    public const FIRST_NAME = 'FirstName';
    public const LAST_NAME = 'LastName';
    public const FULL_NAME = 'FullName';
    public const EMAIL = 'Email';
    public const ROLES = 'Roles';
    public const USERNAME = 'Username';

    /**
     * @return array
     */
    public static function getAllFields(): array
    {
        return [
            self::ID,
            self::FIRST_NAME,
            self::LAST_NAME,
            self::EMAIL,
            self::USERNAME,
        ];
    }
}
