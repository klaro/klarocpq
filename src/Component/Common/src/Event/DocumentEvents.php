<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Event;

/**
 * Document events are triggered when accessing documents under Offering Summary print options.
 *
 * Class DocumentEvents
 *
 * @package Klaro\Component\Common\Event
 */
final class DocumentEvents
{
    /**
     * Event: Klaro\QuotationBundle\Event/QuotationDocumentEvent
     *
     * Triggered when the requested document is about to be generated.
     * Passed data is the same as the document configuration + debug flag:
     *
     * documentId : Document id
     * type       : Type
     * title      : Document title
     * source     : Source template path
     * debug      : Debug flag
     */
    public const QUOTATION_DOCUMENT_GENERATE = 'klaro_quotation.document.generate';

    /**
     * Event: Klaro\QuotationBundle\Event/QuotationDocumentEvent
     *
     * Triggered when the requested document has been generated and is ready to be set for the response.
     *
     * Data is the same as in QUOTATION_DOCUMENT_GENERATE
     */
    public const QUOTATION_DOCUMENT_READY = 'klaro_quotation.document.ready';
}
