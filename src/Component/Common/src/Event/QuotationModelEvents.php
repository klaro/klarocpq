<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Event;

/**
 * Define events that modify the quotation entities.
 *
 * Class QuotationModelEvents
 *
 * @package Klaro\Component\Common\Event
 */
final class QuotationModelEvents
{
    /**
     * Event: Klaro\QuotationBundle\Event\FormModelEvent
     *
     * Triggered when a model is requested. The event expects the listener to create or load a model that
     * implements the model interface in Klaro\Component\Common\Model\FormModelInterface and then attach it
     * to the event by calling FormModelEvent::setModel().
     *
     * modelName : Name of the model to load
     * subId     : Index of the model, if there are many associated with one quotation
     * filters   : Array of filters used when finding a model
     * deferSave : Flag to tell whether the model should be persisted before returning it (this is
     *             needed when the model does not exist and a new is created - if the model is new,
     *             default values may be set to it).
     */
    public const LOAD_MODEL = 'klaro_quotation.model_event.form_model.load';

    /**
     * Event: Klaro\QuotationBundle\Event\FormItemFinderEvent
     *
     * Triggered when an item finder is requested. The event handler should create or load an item finder
     * that implements the interface in Klaro\Component\Common\Model\FinderInterface and attach it to the event
     * by calling FormItemFinderEvent::setFinder().
     *
     * finderName : Name of the finder
     */
    public const LOAD_ITEM_FINDER = 'klaro_quotation.model_event.item_finder.load';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationManagerEvent
     *
     * Triggered when a quotation manager is requested. Quotation manager implements the interface in
     * Klaro\Component\Common\Event\QuotationManagerInterface and is responsible for finding quotations
     * based on given filters. The event handler should set the handler by calling QuotationManagerEvent::setManager().
     */
    public const LOAD_QUOTATION_MANAGER = 'klaro_quotation.model_event.quotation_manager.load';

    /**
     * Event: Klaro\QuotationBundle\Event\UserManagerEvent
     *
     * Triggered when a user manager is requested. User manager implements the interface in
     * Klaro\Component\Common\Event\UserManagerInterface and is responsible for finding users and especially the current user.
     * The event handler should set the handler by calling UserManagerEvent::setManager().
     */
    public const LOAD_USER_MANAGER = 'klaro_quotation.model_event.user_manager.load';

    /**
     * Event: Klaro\QuotationBundle\Event\ModelManagerEvent
     */
    public const LOAD_MODEL_MANAGER = 'klaro_quotation.model_event.model_manager.load';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationEvent
     *
     * Triggered when a quotation is requested. The event handler should load a quotation that implements
     * the interface in Klaro\Component\Common\Model\QuotationInterface and set it to the event by calling
     * QuotationEvent::setQuotation().
     *
     * quotationId : Id of the quotation to load
     */
    public const LOAD_QUOTATION = 'klaro_quotation.model_event.quotation.load';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationRevisionEvent
     *
     * Triggered when a quotation revision is requested. The event handler should load a revision that implements
     * the interface in Klaro\Component\Common\Model\QuotationRevisionInterface and set it to the event by calling
     * QuotationRevisionEvent::setQuotationRevision().
     *
     * revisionId : Id of the revision to load
     */
    public const LOAD_REVISION = 'klaro_quotation.model_event.revision.load';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationEvent
     *
     * Triggered when a new quotation is created. The event handler should load a quotation that implements
     * the interface in Klaro\Component\Common\Model\QuotationInterface and set it to the event by calling
     * QuotationEvent::setQuotation().
     *
     * quotationType : The quotation type that the quotation should be created in.
     */
    public const CREATE_QUOTATION = 'klaro_quotation.model_event.quotation.create';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationRevisionEvent
     *
     * Triggered when a new revision is created. The event handler should load a revision that implements
     * the interface in Klaro\Component\Common\Model\QuotationRevisionInterface and set it to the event by calling
     * QuotationRevisionEvent::setRevision().
     */
    public const CREATE_REVISION = 'klaro_quotation.model_event.revision.create';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationUserEvent
     *
     * Triggered when the quotation is being linked to the given user.
     */
    public const LINK_QUOTATION_TO_USER = 'klaro_quotation.model_event.quotation.link_to_user';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationEvent
     *
     * Triggered when a quotation is being removed.
     */
    public const REMOVE_QUOTATION = 'klaro_quotation.model_event.quotation.remove';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationRevisionEvent
     *
     * Triggered when removing a revision.
     */
    public const REMOVE_REVISION = 'klaro_quotation.model_event.revision.remove';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationUserEvent
     *
     * Triggered whem a quotation is being copied. Note: the quotation associated with the event is the
     * quotation being copied, not the copy.
     */
    public const COPY_QUOTATION = 'klaro_quotation.model_event.quotation.copy';
}
