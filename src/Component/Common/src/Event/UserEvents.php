<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Event;

/**
 * Events related to user handling.
 *
 * Class UserEvents
 *
 * @package Klaro\Component\Common\Event
 */
final class UserEvents
{
    /**
     * Event: Klaro\QuotationBundle\Event\UserEvent
     *
     * Triggered whenever the current user is requested. The event handler should load the current user implementing
     * the interface in Klaro\Component\Common\Model\QuotationUserInterface and set it to the event by calling UserEvent::setUser().
     */
    public const CURRENT_USER = 'klaro_quotation.user.current';

    /**
     * Event: Klaro\QuotationBundle\Event\UserEvent
     *
     * Triggered when a user with a given id is requested. The event handler should load the current user implementing
     * the interface in Klaro\Component\Common\Model\QuotationUserInterface and set it to the event by calling UserEvent::setUser().
     * The desired user id is in the event data under 'userId'.
     *
     * userId : Id of the user that should be loaded
     */
    public const LOAD_USER = 'klaro_quotation.user.load';
}
