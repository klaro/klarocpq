<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Event;

/**
 * Events which happens when validating the offering summary page.
 *
 * Class ValidationEvents
 *
 * @package Klaro\Component\Common\Event
 */
final class ValidationEvents
{
    /**
     * Event: Klaro\QuotationBundle\Event\QuotationRevisionEvent
     *
     * Triggered when validating the current revision in the summary page.
     *
     * validationResult : Validator result
     */
    public const QUOTATION_SUMMARY_VALIDATE = 'klaro_quotation.validation.validate';
}
