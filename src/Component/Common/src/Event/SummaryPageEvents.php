<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Event;

/**
 * Offering summary page events.
 *
 * Class SummaryPageEvents
 *
 * @package Klaro\Component\Common\Event
 */
final class SummaryPageEvents
{
    /**
     * Event: Klaro\Component\Common\Event\Event\QuotationRevisionEvent
     *
     * Triggered when the default (offering summary structure) summary page is loaded.
     */
    public const QUOTATION_SUMMARY_DEFAULT = 'klaro_quotation.summary_page.default';
}
