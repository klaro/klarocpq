<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Event;

final class ProductDefinitionManagerEvents
{
    /**
     *
     */
    public const PRODUCT_LINE_DEFINITION = 'klaro_quotation.product_definition_manager.event.product_line_definition';

    /**
     *
     */
    public const CONFIGURATOR_CONFIG = 'klaro_quotation.product_definition_manager.event.configurator_config';

    /**
     *
     */
    public const CONFIGURATOR_STRUCTURE = 'klaro_quotation.product_definition_manager.event.configurator_structure';

    /**
     *
     */
    public const CONFIGURATION = 'klaro_quotation.product_definition_manager.event.configuration';

    /**
     *
     */
    public const CONFIGURATION_FINALIZE = 'klaro_quotation.product_definition_manager.event.configuration_finalize';
}
