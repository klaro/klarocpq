<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Event;

/**
 * Defines UI events triggered during the proposal process.
 *
 * Unless otherwise noted, the event listener receives an Klaro\QuotationBundle\Event\QuotationEvent instance
 * which is also an implementation of ResponseEvent. Therefore, it is possible to override the response when
 * catching the event.
 *
 * Class QuotationControllerEvents
 *
 * @package Klaro\Component\Common\Event
 */
final class QuotationControllerEvents
{
    /**
     * Triggered when entering the quotation bundle index, showing all configured product lines. Data:
     *
     * quotationTypes : Array of configured quotation types
     */
    public const QUOTATION_INDEX = 'klaro_quotation.controller_event.index';

    /**
     * Triggered after creating a quotation and before redirecting the quotation info page. Data:
     *
     * quotationType : The quotation type for which this quotation is created.
     */
    public const QUOTATION_CREATE = 'klaro_quotation.controller_event.quotation.create';

    public const REVISION_CREATE = 'klaro_quotation.controller_event.revision.create';

    /**
     * Triggered when listing proposals. Data array includes the following data entries:
     *
     * quotationType : Current quotation type
     * dataTable : Reference to the data table object for proposal listing
     * owner : Identifier for quotation listing, either "user" for user's quotations or "other" for other users' quotations
     */
    public const QUOTATION_LIST = 'klaro_quotation.controller_event.list';

    /**
     * Triggered before rendering the proposal info page. Data array includes:
     *
     * quotationType : Quotation type
     * configuredProductLine : Product definition
     * revisions : List of revisions
     * stateMachine : Revision state
     */
    public const QUOTATION_INFO = 'klaro_quotation.controller_event.info';

    /**
     * Triggered when creating the form and processing input, just before QUOTATION_EDIT_SAVE and QUOTATION_EDIT (data is the same).
     */
    public const QUOTATION_EDIT_CREATE_PHASE = 'klaro_quotation.controller_event.edit.create_phase';

    /**
     * Triggered when saving the form, just before QUOTATION_EDIT (data is the same).
     */
    public const QUOTATION_EDIT_SAVE = 'klaro_quotation.controller_event.edit.save';

    /**
     * Triggered when rendering a proposal phase form. Receives a QuotationEvent instance. Data array includes:
     */
    public const QUOTATION_EDIT = 'klaro_quotation.controller_event.edit';

    /**
     * Triggered when generating an output document. Receives an instance of QuotationDocumentEvent.
     */
    public const QUOTATION_DOCUMENT = 'klaro_quotation.controller_event.document';

    /**
     * Triggered when accessing the offering summary page.
     */
    public const QUOTATION_SUMMARY = 'klaro_quotation.controller_event.summary';

    /**
     * Triggered when linking a quotation to a user's account. Event type is QuotationUserEvent.
     */
    public const QUOTATION_LINK = 'klaro_quotation.controller_event.link';

    /**
     * Triggered when claiming a quotation and dropping other handlers. Event type is QuotationUserEvent.
     */
    public const QUOTATION_CLAIM = 'klaro_quotation.controller_event.claim';

    /**
     * Triggered when copying a quotations. Note: this event is triggered for new quotation, a reference
     * to the old one is available in the event data. Event type is QuotationUserEvent.
     */
    public const QUOTATION_COPY = 'klaro_quotation.controller_event.copy';

    /**
     * Triggered when a quotation is removed.
     */
    public const QUOTATION_REMOVE = 'klaro_quotation.controller_event.remove';

    /**
     * Triggered when sidebar page is loaded.
     */
    public const QUOTATION_SIDEBAR = 'klaro_quotation.controller_event.sidebar.show';
}
