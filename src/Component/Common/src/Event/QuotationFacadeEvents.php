<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Event;

/**
 * Defines events that are triggered during the quotation management process.
 *
 * Class QuotationEvents
 *
 * @package Klaro\Component\Common\Event
 */
final class QuotationFacadeEvents
{
    /**
     * Event: Klaro\QuotationBundle\Event\QuotationEvent
     *
     * Triggered after creating a quotation and before redirecting the quotation info page. Data:
     *
     * productLine : The product line id for which this quotation is created.
     */
    public const QUOTATION_CREATE = 'klaro_quotation.facade_event.quotation.create';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationRevisionEvent
     *
     * Triggered when a new revision is created.
     *
     * previous : The previous revision from which this revision was started from or null if none.
     */
    public const REVISION_CREATE = 'klaro_quotation.facade_event.revision.create';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationRevisionEvent
     *
     * Triggered when a revision is removed.
     */
    public const REVISION_REMOVE = 'klaro_quotation.facade_event.revision.remove';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationUserEvent
     *
     * Triggered when linking a quotation to a user's account. Event type is QuotationUserEvent.
     */
    public const QUOTATION_LINK = 'klaro_quotation.facade_event.link';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationUserEvent
     *
     * Triggered when claiming a quotation and dropping other handlers. Event type is QuotationUserEvent.
     */
    public const QUOTATION_CLAIM = 'klaro_quotation.facade_event.claim';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationEvent
     *
     * Triggered when copying a quotations. Note: this event is triggered for new quotation, a reference
     * to the old one is available in the event data. Event type is QuotationUserEvent.
     *
     * previous : Reference to the copied (old) quotation
     */
    public const QUOTATION_COPY = 'klaro_quotation.facade_event.copy';

    /**
     * Event: Klaro\QuotationBundle\Event\QuotationEvent
     *
     * Triggered when a quotation is removed.
     */
    public const QUOTATION_REMOVE = 'klaro_quotation.facade_event.remove';
}
