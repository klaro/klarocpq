<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Exception;

use Exception;

/**
 * Class TooManyProductItemsException
 *
 * @package Klaro\Component\Common\Exception
 */
class TooManyProductItemsException extends ProductItemException
{
    /**
     * {@inheritdoc}
     */
    public function __construct(
        string $message = 'Too many product items found!',
        int $code = 0,
        Exception $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
