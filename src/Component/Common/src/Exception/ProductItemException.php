<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Exception;

use Exception;

/**
 * Class ProductItemException
 *
 * @package Klaro\Component\Common\Exception
 */
class ProductItemException extends Exception implements ProductItemExceptionInterface
{
    /**
     * @param string    $message  The internal exception message
     * @param null|int  $code     The internal exception code
     * @param Exception $previous The previous exception
     */
    public function __construct(string $message, ?int $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
