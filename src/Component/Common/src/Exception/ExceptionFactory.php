<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Exception;

use Exception;
use UnexpectedValueException;

/**
 * Factory to generate exceptions
 *
 * Class ExceptionFactory
 *
 * @package Klaro\Component\Common\Exception
 *
 * @todo move separate exception classes under factory and use as static calls, also inherit them from SPL exceptions.
 */
class ExceptionFactory
{
    /**
     * @param $message
     *
     * @return Exception
     */
    public static function invalidSchema($message = 'Invalid schema')
    {
        return new UnexpectedValueException($message);
    }

    /**
     * @param $type
     *
     * @return UnexpectedValueException
     */
    public static function unexpectedPhaseType($type)
    {
        return new UnexpectedValueException(
            sprintf('Unexpected phase type "%s"!', $type)
        );
    }

    /**
     * @param $productRef
     *
     * @return UnexpectedValueException
     */
    public static function productNotFound($productRef)
    {
        return new UnexpectedValueException(
            sprintf('Product "%s" not found!', $productRef)
        );
    }

    /**
     * @return Exception
     */
    public static function notSupported()
    {
        return new Exception('Not supported');
    }
}
