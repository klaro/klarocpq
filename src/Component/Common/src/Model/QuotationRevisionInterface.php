<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Model;

use DateTime;
use stdClass;

/**
 * Interface for a quotation revision. The revision is responsible for storing all related phase rules,
 * sales structures, product item rules and metadata. Revision has a status and an order status and can be
 * labelled with qualifiers. The generated offering summary price can be overridden for each revision by the user.
 *
 * Interface QuotationRevisionInterface
 *
 * @package Klaro\Component\Common\Model
 */
interface QuotationRevisionInterface extends BaseModelInterface
{
    /**
     * Return a unique identifier, generated when creating the revision.
     *
     * @return string
     */
    public function getIdentifier();

    /**
     * Set a unique identifier, generated when creating the revision.
     *
     * @param $identifier
     *
     * @return mixed
     */
    public function setIdentifier($identifier);

    /**
     * Get revision id.
     *
     * @return int
     */
    public function getRevisionId();

    /**
     * Set the revision id.
     *
     * @param $revisionId
     */
    public function setRevisionId($revisionId);

    /**
     * Get the related quotation.
     *
     * @return QuotationInterface
     */
    public function getQuotation();

    /**
     * Set the quotation that this revision belongs to.
     *
     * @param QuotationInterface $quotation
     */
    public function setQuotation(QuotationInterface $quotation);

    /**
     * @return string
     */
    public function getProductLine();

    /**
     * @param string $productLine
     */
    public function setProductLine($productLine);

    /**
     * @return string
     */
    public function getProductLineVersion();

    /**
     * @param string $version
     */
    public function setProductLineVersion($version);

    /**
     * @return string
     */
    public function getConfigurator();

    /**
     * @param string $configurator
     */
    public function setConfigurator($configurator);

    /**
     * @return string
     */
    public function getConfiguratorVersion();

    /**
     * @param string $version
     */
    public function setConfiguratorVersion($version);

    /**
     * Qualifiers are an array key-value pairs that can be used to describe the revision, eg. type = {Firm, Budgetary}.
     * Different qualifiers and options can be set in the configuration key Klaro_quotation.revision.qualifiers.
     *
     * @return array Array of qualifier types and their values.
     */
    public function getQualifiers();

    /**
     * Set qualifier array.
     *
     * @param array $qualifiers
     */
    public function setQualifiers($qualifiers);

    /**
     * Get qualifier value for given type. The name must match a key under Klaro_quotation.revision.qualifiers.
     *
     * @param string $name Qualifier name
     *
     * @return string Qualifier value.
     */
    public function getQualifier($name);

    /**
     * Set qualifier value for given type.
     *
     * @param string $name  Qualifier name
     * @param string $value Qualifier value.
     */
    public function setQualifier($name, $value);

    /**
     * Revision status, one of the types defined in class Klaro\QuotationBundle\Api\QuotationRevisionStatus.
     *
     * @return mixed
     */
    public function getStatus();

    /**
     * Set revision status.
     *
     * @param string $status
     */
    public function setStatus($status);

    /**
     * Revision order status for revisions that have 'won' status,
     * one of the types defined in class Klaro\QuotationBundle\Api\QuotationRevisionOrderStatus.
     *
     * @return mixed
     */
    public function getOrderStatus();

    /**
     * Set revision order status.
     *
     * @param $status
     */
    public function setOrderStatus($status);

    /**
     * Revision log message for internal use explaining changes etc.
     *
     * @return string
     */
    public function getLogMessage();

    /**
     * Set revision log message.
     *
     * @param $message
     */
    public function setLogMessage($message);

    /**
     * Get the revision's title.
     *
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     */
    public function setTitle($title);

    /**
     * @return array
     *
     * @since 2.2.4
     */
    public function getPhaseData();

    /**
     * @param array $phaseData
     *
     * @since 2.2.4
     */
    public function setPhaseData(array $phaseData);

    /**
     * Returns true if the revision's offering summary structure has been generated and cached (no need to generate
     * it again from sales structure definition).
     *
     * @return boolean
     */
    public function isValid();

    /**
     * Set revision as invalid. This happens when a data has been changed and the offering summary needs to
     * be generated again.
     *
     * @param $valid
     *
     * @return mixed
     */
    public function setIsValid($valid);

    /**
     * Get cached offering summary structure.
     *
     * @return stdClass
     */
    public function getOfferingSummary();

    /**
     * Set cached offering summary.
     *
     * @param stdClass $summary
     */
    public function setOfferingSummary($summary);

    /**
     * Return the revision create date.
     *
     * @return DateTime
     */
    public function getCreatedAt();

    /**
     * Set revision create date.
     *
     * @param DateTime $value
     */
    public function setCreatedAt($value);

    /**
     * Return date of revision's last modification.
     *
     * @return DateTime
     */
    public function getUpdatedAt();

    /**
     * Set last modified date for revision.
     *
     * @param DateTime $value
     */
    public function setUpdatedAt($value);

    /**
     * Get the user related to this revision.
     *
     * @return QuotationUserInterface
     */
    public function getUser();

    /**
     * Set the user related to this revision.
     *
     * @param QuotationUserInterface $user
     */
    public function setUser(QuotationUserInterface $user);

    /**
     * Get cached sales structure.
     *
     * @return array
     */
    public function getSalesStructure();

    /**
     * Set cache sales structure.
     *
     * @param array $salesStructure
     */
    public function setSalesStructure($salesStructure);

    /**
     * Get cached product item rules.
     *
     * @return array
     */
    public function getProductItems();

    /**
     * Set cached product item rules.
     *
     * @param array $productItems
     */
    public function setProductItems($productItems);

    /**
     * Get cached phase configuration.
     *
     * @return array
     */
    public function getPhases();

    /**
     * Set cached phase configuration.
     *
     * @param $phases
     *
     * @return array
     */
    public function setPhases($phases);

    /**
     * Get calculated sales price.
     *
     * @return mixed
     */
    public function getCalculatedSalesPriceWithTax();

    /**
     * Get the calculated sales price.
     *
     * @param $salesPrice
     */
    public function setCalculatedSalesPriceWithTax($salesPrice);

    /**
     * Get the user defined sales price.
     *
     * @return mixed
     */
    public function getTargetSalesPriceWithTax();

    /**
     * Set the user defined sales price.
     *
     * @param $salesPrice
     */
    public function setTargetSalesPriceWithTax($salesPrice);

    /**
     * Get the calculated cost price.
     *
     * @return mixed
     */
    public function getCalculatedCostPrice();

    /**
     * Set the calculated cost price.
     *
     * @param $costPrice
     */
    public function setCalculatedCostPrice($costPrice);

    /**
     * Get the revision currency.
     *
     * @return mixed
     */
    public function getCurrency();

    /**
     * Set the revision currency.
     *
     * @param $currency
     */
    public function setCurrency($currency);

    /**
     * Get metadata for this revision.
     *
     * @param $name
     *
     * @return mixed
     */
    public function getMetaData($name);

    /**
     * Set metadata for this revision.
     *
     * @param $name
     * @param $value
     */
    public function setMetaData($name, $value);

    /**
     * Get all metadata related to this revision.
     *
     * @return array
     */
    public function getMetaDatas();

    /**
     * Set all metadata related to this revision.
     *
     * @param array $metadata
     */
    public function setMetaDatas($metadata);
}
