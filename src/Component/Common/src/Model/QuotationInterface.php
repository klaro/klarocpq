<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Model;

use DateTime;

/**
 * Interface for a Quotation.
 *
 * Interface QuotationInterface
 *
 * @package Klaro\Component\Common\Model
 */
interface QuotationInterface extends ModelInterface
{
    /**
     * Return a unique identifier for this quotation, generated when creating the quotation.
     *
     * @return string
     */
    public function getIdentifier();

    /**
     * Set a unique identifier for this quotation, generated when creating the quotation.
     * @param $identifier
     */
    public function setIdentifier($identifier);

    /**
     * Return quotation title that can be changed by the user.
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set quotation title.
     *
     * @param $value
     */
    public function setTitle($value);

    /**
     * Return quotation type.
     *
     * @return string
     */
    public function getQuotationType();

    /**
     * Set quotation type.
     *
     * @param $value
     */
    public function setQuotationType($value);

    /**
     * Returns the quotation's owner.
     *
     * @return QuotationUserInterface
     */
    public function getOwner();

    /**
     * Set quotation owner.
     * @param QuotationUserInterface $owner
     */
    public function setOwner(QuotationUserInterface $owner);

    /**
     * Return the quotation create date.
     *
     * @return DateTime
     */
    public function getCreatedAt();

    /**
     * Set quotation create date.
     *
     * @param DateTime $value
     */
    public function setCreatedAt($value);

    /**
     * Return date of quotation's last modification.
     *
     * @return DateTime
     */
    public function getUpdatedAt();

    /**
     * Set last modified date for quotation.
     *
     * @param DateTime $value
     */
    public function setUpdatedAt($value);

    /**
     * Get the latest revision.
     *
     * @return QuotationRevisionInterface
     */
    public function getLatestRevision();

    /**
     * Set the latest quotation revision.
     *
     * @param QuotationRevisionInterface $revision
     */
    public function setLatestRevision(QuotationRevisionInterface $revision);
}
