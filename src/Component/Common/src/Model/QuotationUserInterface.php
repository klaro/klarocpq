<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Model;

use JsonSerializable;
use Serializable;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface for a user that interacts with quotations.
 *
 * Interface QuotationUserInterface
 *
 * @package Klaro\Component\Common\Model
 */
interface QuotationUserInterface extends UserInterface, JsonSerializable, Serializable
{

    /**
     * Magic method for getting properties form the user object. This is needed when accessing data from the user
     * in evaluated expressions using object notation.
     *
     * @param string $field Property name
     *
     * @return mixed
     */
    public function __get($field);

    /**
     * Magic method to allow calling of methods using object notation.
     *
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments);
    /**
     * Return an identifier for the user.
     *
     * @return mixed
     */
    public function getId();

    /**
     * Return a string that is used to represent the user's name in the UI.
     *
     * @return string
     *
     * @deprecated since 2.0.8
     */
    public function getProfileTitle();

    /**
     * Return user's full name.
     *
     * @return string
     */
    public function getFullname();

    /**
     * Return user's first name.
     *
     * @return string
     *
     * @since 2.0.8
     */
    public function getFirstName();

    /**
     * Return user's last name.
     *
     * @return string
     *
     * @since 2.0.8
     */
    public function getLastName();

    /**
     * Return user's email address.
     *
     * @return string
     */
    public function getEmail();

    /**
     * Helper to get a property from the user object.
     *
     * @param string     $field     Propery name
     * @param null|mixed $arguments
     *
     * @return mixed
     */
    public function get($field, $arguments = null);
}
