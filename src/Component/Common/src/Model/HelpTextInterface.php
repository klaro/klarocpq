<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Model;

/**
 * @ORM\Entity(repositoryClass="Klaro\QuotationExtraBundle\Model\HelpTextRepository")
 * @ORM\Table(name="klaro_help_text", options={"engine"="MyISAM"})
 * @ORM\HasLifecycleCallbacks
 */
interface HelpTextInterface
{
    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param mixed $id
     */
    public function setId($id): void;

    /**
     * @return mixed
     */
    public function getApplication();

    /**
     * @param mixed $application
     */
    public function setApplication($application): void;

    /**
     * @return mixed
     */
    public function getPhaseId();

    /**
     * @param mixed $phaseId
     */
    public function setPhaseId($phaseId): void;

    /**
     * @return mixed
     */
    public function getFieldname();

    /**
     * @param mixed $fieldname
     */
    public function setFieldname($fieldname): void;

    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * @param mixed $description
     */
    public function setDescription($description): void;

    /**
     * @return mixed
     */
    public function getDateUpdated();

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated): void;

    /**
     * @return mixed
     */
    public function getDateUpdatedFormatted();

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamp();

    /**
     *
     * Get object in array
     *
     * @return array
     */
    public function toArray();
}