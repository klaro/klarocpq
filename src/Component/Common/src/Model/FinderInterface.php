<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Model;

use ArrayObject;

/**
 * Interface for finding items in the phase forms or in the sales structure.
 *
 * Interface FinderInterface
 *
 * @package Klaro\Component\Common\Model
 */
interface FinderInterface
{
    /**
     * Return items based on the given filters. Filters are an array of key-value pairs that can be used to filter
     * the list of items.
     *
     * @param array $filters
     *
     * @return array|ArrayObject
     */
    public function getItems($filters);

    /**
     * Translate a model returned by FinderInterface::getItems() to be an instance of FormModelInterface.
     * Called when iterating the list of items and the model does not implement ModelInterface.
     *
     * @param mixed $model
     *
     * @return ModelInterface
     */
    public function translateModel($model);
}
