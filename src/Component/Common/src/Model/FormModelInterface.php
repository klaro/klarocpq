<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Model;

/**
 * Interface for a form model.
 *
 * {@inheritdoc}
 *
 * Interface FormModelInterface
 *
 * @package Klaro\Component\Common\Model
 */
interface FormModelInterface extends ModelInterface
{
    /**
     * Return the parent quotation.
     * @return QuotationInterface
     */
    // TODO: remove?
    public function getQuotation();

    /**
     * Set the parent quotation.
     *
     * @param $parentId
     */
    // TODO: remove?
    public function setQuotation(QuotationInterface $quotation);

    /**
     * @return QuotationRevisionInterface
     */
    public function getRevision();

    /**
     * @param QuotationRevisionInterface $revision
     *
     * @return mixed
     */
    public function setRevision(QuotationRevisionInterface $revision);

    /**
     * @return string
     */
    public function getPhaseId();

    /**
     * @param string $phaseId
     */
    public function setPhaseId($phaseId);

    /**
     * Returns the ordinal for repeated phases.
     *
     * @return int
     */
    public function getSubId();

    /**
     * Set ordinal for repeated phases. This is to differentiate between several models linked under one quotation.
     *
     * @param int $subId Ordinal
     */
    public function setSubId($subId);

    /**
     * Return model locking status.
     *
     * @return boolean
     */
    public function getLocked();

    /**
     * Set model locking status.
     *
     * @param boolean $locked True if locked.
     */
    public function setLocked($locked);

    /**
     * Get model data as array.
     *
     * @return array
     */
    public function getFormData();

    /**
     * Set model data from array.
     *
     * @param $data
     */
    public function setFormData($data);
}
