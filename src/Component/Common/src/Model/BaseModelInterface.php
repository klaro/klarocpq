<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Model;

use JsonSerializable;
use Serializable;

/**
 * Base interface for models.
 *
 * @package Klaro\Component\Common\Model
 */
interface BaseModelInterface extends Serializable, JsonSerializable
{

    /**
     * Magic method wrapper for self::get() to allow using object notation in expressions, eg. 'model.SomeField'
     * is the same as $model->get('SomeField').
     * @param string $field Field name
     *
     * @return mixed Value
     *
     * @see get()
     *
     */
    public function __get($field);

    /**
     * Magic method to allow using of this model in expression, see __get().
     * @param string $field Field name
     * @param mixed  $value Value.
     *
     * @see set()
     *
     */
    public function __set($field, $value);

    /**
     * Magic method to allow calling of methods using object notation.
     *
     * @param string $name       Method name
     * @param mixed  $parameters Parameters
     *
     * @return mixed Value
     */
    public function __call($name, $parameters);
    /**
     * Return the given field's value.
     *
     * @param string $field Fieldname
     *
     * @return mixed Value
     */
    public function get($field, $arguments = null);

    /**
     * Set a value to the given field.
     *
     * @param string $field Fieldname
     * @param mixed  $value Value
     */
    public function set($field, $value);

    /**
     * Return the model data as an array.
     *
     * @return array
     */
    public function toArray();
}
