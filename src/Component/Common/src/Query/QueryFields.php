<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Query;

/**
 * Class QueryFields
 *
 * @package Klaro\Component\Common\Query
 */
class QueryFields
{
    public const MAX_RESULTS = 'MaxResults';
    public const FIRST_RESULT = 'FirstResult';
}
