<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Result;

use Countable;
use Iterator;
use Klaro\Component\Common\Query\QueryInterface;

/**
 * Interface for query results.
 *
 * Interface ResultSetInterface
 *
 * @package Klaro\Component\Common\Result
 */
interface ResultSetInterface extends Iterator, Countable
{
    public const MAP_CALLABLE = 'callable';
    public const MAP_STATIC = 'static';

    /**
     * Get the original query.
     *
     * @return QueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function getQuery();

    /**
     * Map results by passing each element through the given callback function.
     *
     * @param callable $callback Callback function
     * @param string   $type     callback type, use MAP_STATIC for static callback.
     *
     * @return mixed
     */
    public function map(callable $callback, string $type = self::MAP_CALLABLE);

    /**
     * Get the result list.
     *
     * @return mixed
     */
    public function getResults();

    /**
     * Get the page size used in the query.
     *
     * @return int
     */
    public function getPageSize(): int;

    /**
     * Get the current page number.
     *
     * @return int
     */
    public function getCurrentPage(): int;

    /**
     * Get total number of pages.
     *
     * @return int
     */
    public function getNumberOfPages(): int;

    /**
     * Get total number of results in the whole query.
     *
     * @return int
     */
    public function getTotalNumberOfResults(): int;
}
