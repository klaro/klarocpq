<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Result;

use Iterator;
use Klaro\Component\Common\Model\ModelInterface;
use Klaro\Component\Common\Query\QueryInterface;

/**
 * Class ResultSet
 *
 * @package Klaro\Component\Common\Result
 */
class ResultSet implements ResultSetInterface
{
    /** @var array Results */
    protected $results;

    /** @var QueryInterface */
    protected $query;

    /** @var int Current position */
    protected $position;

    /** @var int */
    protected $numResults;

    /** @var int */
    protected $totalResults;

    /** @var int */
    protected $pageSize;

    /** @var int */
    protected $currentPage;

    /** @var int */
    protected $numberOfPages;

    /** @var null|callable */
    protected $mappingFunction;

    /**
     * ResultSet constructor.
     * @param QueryInterface $query
     * @param Iterator       $results
     * @param int            $numResults
     * @param null|callable  $mappingFunction
     */
    public function __construct(
        QueryInterface $query,
        Iterator $results,
        int $numResults,
        callable $mappingFunction = null
    ) {
        $this->query = $query;
        $this->results = $results;
        $this->results->rewind();
        $this->totalResults = $numResults;
        $this->mappingFunction = $mappingFunction;
        $this->numResults = count($this->results);

        $maxResults = $this->query->getMaxResults();
        if ($maxResults instanceof QueryParam) {
            $this->pageSize = $maxResults->getValue();
        } else {
            $this->pageSize = $this->numResults;
        }

        $this->pageSize = max(1, $this->pageSize);

        $firstResult = $this->query->getFirstResult();
        if ($firstResult instanceof QueryParam) {
            $this->currentPage = (int) ceil(($firstResult->getValue() + 1) / $this->pageSize);
        } else {
            $this->currentPage = 1;
        }

        $this->numberOfPages = (int) ceil($this->totalResults / $this->pageSize);
    }

    /**
     * {@inheritDoc}
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * {@inheritDoc}
     */
    public function getQuery(): QueryInterface
    {
        return $this->query;
    }

    /**
     * {@inheritDoc}
     */
    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    /**
     * {@inheritDoc}
     */
    public function getCurrentPage(): int
    {
        return $this->currentPage;
    }

    /**
     * {@inheritDoc}
     */
    public function getNumberOfPages(): int
    {
        return $this->numberOfPages;
    }

    /**
     * @return int
     */
    public function getNumberOfResults(): int
    {
        return $this->numResults;
    }

    /**
     * {@inheritDoc}
     */
    public function getTotalNumberOfResults(): int
    {
        return $this->totalResults;
    }

    /**
     * {@inheritDoc}
     */
    public function current()
    {
        if (false === $this->valid()) {
            return false;
        }

        $value = $this->results->current();

        return ($value instanceof Result) ? $value : $this->translateValue($value);
    }

    /**
     * {@inheritDoc}
     */
    public function valid(): bool
    {
        return $this->results->valid();
    }

    /**
     * Transform the current result values into an object that implements ModelInterface.
     *
     * @param mixed $value
     *
     * @return ModelInterface
     */
    public function translateValue($value): ModelInterface
    {
        return is_callable($this->mappingFunction) ? call_user_func($this->mappingFunction, $value) : $value;
    }

    /**
     * {@inheritDoc}
     */
    public function next()
    {
        return $this->results->next();
    }

    /**
     * {@inheritDoc}
     */
    public function key()
    {
        return $this->results->key();
    }

    /**
     * {@inheritDoc}
     */
    public function rewind()
    {
        $this->results->rewind();
    }

    /**
     * {@inheritDoc}
     */
    public function count(): int
    {
        return $this->numResults;
    }

    /**
     * Map result values to an array after passing the result to a callback function.
     *
     * @param callable $callback Callback function
     * @param string   $type     Callback type, use ResultSetInterface::MAP_STATIC if callback should be
     *                           called statically.
     *
     * @return array|ModelInterface[]
     */
    public function map(callable $callback, string $type = self::MAP_CALLABLE): array
    {
        $results = [];

        foreach ($this as $original) {
            $results[] = $callback($original);
        }

        return $results;
    }

    /**
     * Maps values from the raw query values to an array where the values are separated into entity values.
     *
     * @param array $values
     *
     * @return array
     */
    protected function mapValues(array $values): array
    {
        return is_callable($this->mappingFunction) ? call_user_func($this->mappingFunction, $values) : $values;
    }
}
