<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Common\Result;

use Klaro\Component\Common\Model\ModelInterface;

/**
 * Class Result
 *
 * @package Klaro\Component\Common\Result
 *
 * @todo: This class is missing a lot of type hinting because base class does not support it yet.
 */
class Result implements ModelInterface
{
    /** @var array */
    protected $values;

    /** @var string */
    protected $idField;

    /**
     * @param array       $values
     * @param null|string $idField
     */
    public function __construct(array $values, string $idField = null)
    {
        $this->values = $values;
        $this->idField = $idField;
    }

    /**
     * {@inheritDoc}
     */
    public function __get($field)
    {
        return $this->get($field);
    }

    /**
     * {@inheritDoc}
     */
    public function __set($field, $value)
    {
        $this->set($field, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function __call($name, $arguments)
    {
        return $this->get($name);
    }

    /**
     * {@inheritDoc}
     */
    public function get($field, $arguments = null)
    {
        if (array_key_exists($field, $this->values)) {
            $value = $this->values[$field];
        }

        return $value ?? null;
    }

    /**
     * {@inheritDoc}
     */
    public function set($field, $value)
    {
        $this->values[$field] = $value;
    }

    /**
     * {@inheritDoc}
     */
    public function serialize(): string
    {
        return serialize($this->values);
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized): void
    {
        $this->values = unserialize($serialized);
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * {@inheritDoc}
     */
    public function toArray(): array
    {
        return $this->values;
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->get($this->idField);
    }
}
