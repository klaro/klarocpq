<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductManager;

use Klaro\Component\ProductPhase\ProductLine;
use Klaro\Component\ProductPhase\ProductLineDefinition;

interface ProductManagerInterface
{
    const VERSION_LATEST = -1;

    const FETCH_CACHED = 'cached';
    const FETCH_FROM_REVISION = 'revision';
    const FETCH_FROM_SOURCE = 'source';

    /**
     * @return ProductLineDefinition[]
     */
    public function getProductLines();

    /**
     * @param $productLine
     *
     * @return bool
     */
    public function hasProductLine($productLine);

    /**
     * @return string[]
     */
    public function getProductLineNames();

    /**
     * @param $ref
     * @param $version
     * @param $fetchMode
     *
     * @return ProductLineDefinition
     */
    public function getProductLineDefinition($ref, $version = self::VERSION_LATEST, $fetchMode = self::FETCH_CACHED);

    /**
     * @param ProductLineDefinition $productLineDefinition
     *
     * @return ProductLine
     */
    public function getProductLine(ProductLineDefinition $productLineDefinition);
}
