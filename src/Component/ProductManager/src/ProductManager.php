<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductManager;

use Klaro\Component\Cache\CacheInterface;
use Klaro\Component\ProductPhase\ProductLineDefinition;

class ProductManager implements ProductManagerInterface
{
    /** @var ProductManagerInterface */
    protected $concreteManager;

    /** @var CacheInterface */
    protected $cache;

    /**
     * ProductManager constructor.
     * @param ProductManagerInterface $concreteManager
     */
    public function __construct(ProductManagerInterface $concreteManager, CacheInterface $cache = null)
    {
        $this->concreteManager = $concreteManager;
        $this->cache = $cache;
    }

    /**
     * {@inheritdoc}
     */
    public function getProductLineDefinition($ref, $version = self::VERSION_LATEST, $fetchMode = self::FETCH_CACHED)
    {
        $cacheId = 'ProductLineDefinition://'.$ref.'/'.$version;

        if ($fetchMode === self::FETCH_CACHED && $this->cache && $this->cache->contains($cacheId)) {
            $productLineDefinition = $this->cache->fetch($cacheId);
        } else {
            $productLineDefinition = $this->concreteManager->getProductLineDefinition($ref, $version, $fetchMode);

            if ($this->cache) {
                $this->cache->save($cacheId, $productLineDefinition);
            }
        }

        return $productLineDefinition;
    }

    /**
     * {@inheritdoc}
     */
    public function hasProductLine($productLine)
    {
        return $this->concreteManager->hasProductLine($productLine);
    }

    /**
     * {@inheritdoc}
     */
    public function getProductLines()
    {
        return $this->concreteManager->getProductLines();
    }

    /**
     * {@inheritdoc}
     */
    public function getProductLineNames()
    {
        return $this->concreteManager->getProductLineNames();
    }

    /**
     *{@inheritdoc}
     */
    public function getProductLine(ProductLineDefinition $productLineDefinition)
    {
        $ref = $productLineDefinition->getId();
        $version = $productLineDefinition->getVersion();
        $cacheId = 'ProductLine://'.$ref.'/'.$version;

        if ($this->cache && $this->cache->contains($cacheId)) {
            $productLine = $this->cache->fetch($cacheId);
        } else {
            $productLine = $this->concreteManager->getProductLine($productLineDefinition);

            if ($this->cache) {
                $this->cache->save($cacheId, $productLine);
            }
        }

        return $productLine;
    }
}
