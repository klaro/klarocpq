<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductLoader\Tests;

use PHPUnit\Framework\TestCase;
use Klaro\Component\ProductLoader\ArrayProductConfigLoader;
use Klaro\Component\ProductLoader\ProductConfigLoaderInterface;
use Symfony\Component\Yaml\Yaml;

class ArrayProductConfigLoaderTest extends TestCase
{
    /** @var string */
    protected $path;

    /** @var array */
    protected $config;

    /** @var ProductConfigLoaderInterface */
    protected $productLoader;

    /**
     *
     */
    public function setUp()
    {
        $this->path = __DIR__.'/Fixtures';
        $this->config = Yaml::parse(file_get_contents($this->path.'/config.yml'));

        $this->productLoader = new ArrayProductConfigLoader(
            $this->config['phases'],
            $this->config['products'],
            $this->config['product_lines'],
            $this->config['version']
        );
    }

    /**
     *
     */
    public function testGetAllProductLineRefs()
    {
        $expectedTitles = [];

        foreach ($this->config['product_lines'] as $id => $data) {
            $expectedTitles[$id] = $data['title'];
        }

        $actualTitles = $this->productLoader->getAllProductLineRefs();

        $this->assertTrue($expectedTitles === $actualTitles);
    }

    /**
     *
     */
    public function testGetAllPhaseRefs()
    {
        $expectedPhases = [];

        foreach ($this->config['phases'] as $id => $data) {
            $expectedPhases[$id] = $data['title'];
        }

        $actualPhases = $this->productLoader->getAllPhaseRefs();

        $this->assertTrue($expectedPhases === $actualPhases);
    }

    /**
     *
     */
    public function testGetAllProductRefs()
    {
        $expectedProducts = [];

        foreach ($this->config['products'] as $id => $data) {
            $expectedProducts[$id] = $data['title'];
        }

        $actualProducts = $this->productLoader->getAllProductRefs();

        $this->assertTrue($expectedProducts === $actualProducts);
    }

    /**
     *
     */
    public function testGetPhaseConfig()
    {
        foreach ($this->config['phases'] as $id => $expectedConfig) {
            $actualConfig = $this->productLoader->getPhaseConfig($id);

            $this->assertEquals(serialize($expectedConfig), serialize($actualConfig));
        }

        $this->expectException(\UnexpectedValueException::class);
        $this->productLoader->getPhaseConfig('unknownId');
    }

    /**
     *
     */
    public function testGetProductConfig()
    {
        foreach ($this->config['products'] as $id => $expectedConfig) {
            $actualConfig = $this->productLoader->getProductConfig($id);

            $this->assertEquals(serialize($expectedConfig), serialize($actualConfig));
        }

        $this->expectException(\UnexpectedValueException::class);
        $this->productLoader->getProductConfig('unknownId');
    }

    /**
     *
     */
    public function testGetProductLineConfig()
    {
        foreach ($this->config['product_lines'] as $id => $expectedConfig) {
            $actualConfig = $this->productLoader->getProductLineConfig($id);

            $this->assertEquals(serialize($expectedConfig), serialize($actualConfig));
        }

        $this->expectException(\UnexpectedValueException::class);
        $this->productLoader->getProductLineConfig('unknownId');
    }

    /**
     *
     */
    public function testGetLatestVersion()
    {
        $this->assertEquals($this->config['version'], $this->productLoader->getLatestVersion());
    }

    /**
     *
     */
    public function testHasProductLineRef()
    {
        foreach (array_keys($this->config['product_lines']) as $id) {
            $this->assertTrue($this->productLoader->hasProductLineRef($id));
        }

        $this->assertFalse($this->productLoader->hasProductLineRef('unknownId'));
    }
}
