<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductLoader;

class ArrayProductConfigLoader implements ProductConfigLoaderInterface
{
    /** @var array */
    protected $phases;

    /** @var array */
    protected $products;

    /** @var array */
    protected $productLines;

    /** @var string */
    protected $version;

    /**
     * ArrayProductConfigLoader constructor.
     * @param array $phases
     * @param array $products
     * @param array $productLines
     * @param null  $version
     */
    public function __construct(
        array $phases,
        array $products,
        array $productLines,
        $version = null
    ) {
        $this->phases = $phases;
        $this->products = $products;
        $this->productLines = $productLines;
        $this->version = $version;
    }

    /**
     * {@inheritdoc}
     */
    public function getPhaseConfig($ref)
    {
        if (!array_key_exists($ref, $this->phases)) {
            throw new \UnexpectedValueException(
                sprintf('Phase "%s" not found!', $ref)
            );
        }

        return $this->phases[$ref];
    }

    /**
     * {@inheritdoc}
     */
    public function getProductConfig($ref)
    {
        if (!array_key_exists($ref, $this->products)) {
            throw new \UnexpectedValueException(
                sprintf('Product "%s" not found!', $ref)
            );
        }

        return $this->products[$ref];
    }

    /**
     * {@inheritdoc}
     */
    public function getProductLineConfig($ref)
    {
        if (!array_key_exists($ref, $this->productLines)) {
            throw new \UnexpectedValueException(
                sprintf('Productline "%s" not found!', $ref)
            );
        }

        return $this->productLines[$ref];
    }

    /**
     * {@inheritdoc}
     */
    public function getLatestVersion()
    {
        return $this->version;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllProductLineRefs()
    {
        return $this->extractTitles($this->productLines);
    }

    /**
     * {@inheritdoc}
     */
    public function getAllPhaseRefs()
    {
        return $this->extractTitles($this->phases);
    }

    /**
     * {@inheritdoc}
     */
    public function getAllProductRefs()
    {
        return $this->extractTitles($this->products);
    }

    /**
     * {@inheritdoc}
     */
    public function hasProductLineRef($productLine)
    {
        return isset($this->productLines[$productLine]);
    }

    /**
     * @param $source
     *
     * @return string[]
     */
    private function extractTitles($source)
    {
        $names = [];

        foreach ($source as $ref => $data) {
            $names[$ref] = !empty($data['title']) ? $data['title'] : $ref;
        }

        return $names;
    }
}
