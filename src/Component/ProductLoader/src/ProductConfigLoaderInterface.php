<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ProductLoader;

/**
 * Interface for loaders that fetch the product configuration.
 *
 * Interface ProductConfigLoaderInterface
 * @package Klaro\Component\ProductLoader
 */
interface ProductConfigLoaderInterface
{
    /**
     * @return string[]
     */
    public function getAllProductLineRefs();

    /**
     * @return string[]
     */
    public function getAllPhaseRefs();

    /**
     * @return string[]
     */
    public function getAllProductRefs();

    /**
     * @param $ref
     *
     * @return array
     */
    public function getPhaseConfig($ref);

    /**
     * @param $ref
     *
     * @return array
     */
    public function getProductConfig($ref);

    /**
     * @param $ref
     *
     * @return array
     */
    public function getProductLineConfig($ref);

    /**
     * @return string
     */
    public function getLatestVersion();

    /**
     * @param $productLine
     *
     * @return bool
     */
    public function hasProductLineRef($productLine);
}
