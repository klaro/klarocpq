<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\DataTable\Table;

/**
 * Class HeaderItem
 * @package Klaro\DataTable
 */
class HeaderItem extends DataTableItem
{
    protected $sortable;
    protected $columnId;

    public function __construct($text, $columnId, $url = null, $fixed = false, $sortable = false)
    {
        // Set properties.
        $this->sortable = $sortable;
        $this->columnId = $columnId;

        parent::__construct($text, $url, $fixed);
    }

    public function setSortable($sortable)
    {
        $this->sortable = $sortable;
    }

    public function getSortable()
    {
        return $this->sortable;
    }

    public function setColumnId($columnId)
    {
        $this->columnId = $columnId;
    }

    public function getColumnId()
    {
        return $this->columnId;
    }
}
