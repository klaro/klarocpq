<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\DataTable\Table;

/**
 * Class DataTableColumn
 * @package Klaro\DataTable
 */
class DataTableColumn implements \Serializable, \JsonSerializable
{
    /** @var string */
    private $columnId;

    /** @var null|HeaderItem */
    private $headerItem = null;

    /** @var array|null */
    private $dataFormat = null;

    /**
     * DataTableColumn constructor.
     * @param $columnId
     * @param null     $text
     */
    public function __construct($columnId, $text = null)
    {
        $this->columnId = $columnId;

        $this->headerItem = new HeaderItem($text, $columnId);
    }

    /**
     * @param $columnId
     *
     * @return $this
     */
    public function setColumnId($columnId)
    {
        $this->columnId = $columnId;

        return $this;
    }

    /**
     * @return string
     */
    public function getColumnId()
    {
        return $this->columnId;
    }

    /**
     * @param $columnName
     *
     * @return $this
     */
    public function setColumnName($columnName)
    {
        $this->headerItem->setColumnId($columnName);

        return $this;
    }

    /**
     * @param $headerItem
     *
     * @return $this
     */
    public function setHeaderItem($headerItem)
    {
        $this->headerItem = $headerItem;

        return $this;
    }

    /**
     * @return null|HeaderItem
     */
    public function getHeaderItem()
    {
        return $this->headerItem;
    }

    /**
     * @param $fixed
     *
     * @return $this
     */
    public function setFixed($fixed)
    {
        $this->headerItem->setFixed($fixed);

        return $this;
    }

    /**
     * @param $sortable
     *
     * @return $this
     */
    public function setSortable($sortable)
    {
        $this->headerItem->setSortable($sortable);

        return $this;
    }

    /**
     * @param $dataFormat
     *
     * @return $this
     */
    public function setDataFormat($dataFormat)
    {
        $this->dataFormat = $dataFormat;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getDataFormat()
    {
        return $this->dataFormat;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return array(
            'columnId' => $this->columnId,
        );
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {
        return serialize($this->getData());
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        $this->columnId = $data['columnId'];
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return $this->getData();
    }
}
