<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\DataTable\Table;

/**
 * Class DataTableItem
 * @package Klaro\DataTable
 */
class DataTableItem extends LinkItem
{
    protected $fixed;
    protected $sortable;

    public function __construct($text, $url = null, $fixed = false)
    {
        // Set properties.
        $this->fixed = $fixed;

        parent::__construct($text, $url);
    }

    public function setFixed($fixed)
    {
        $this->fixed = $fixed;

        return $this;
    }

    public function getFixed()
    {
        return $this->fixed;
    }

    /**
     * @return mixed
     */
    public function getSortable()
    {
        return $this->sortable;
    }

    /**
     * @param mixed $sortable
     *
     * @return DataTableItem
     */
    public function setSortable($sortable)
    {
        $this->sortable = $sortable;

        return $this;
    }
}
