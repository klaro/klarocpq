<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\DataTable\Table;

/**
 * Class LinkItem
 * @package Klaro\DataTable
 */
class LinkItem
{
    /**
     * @var string
     */
    protected $text;
    /**
     * @var int
     */
    protected $id;
    /**
     * @var string
     */
    protected $url;
    /**
     * @var string
     */
    protected $icon = null;
    /**
     * @var string
     */
    protected $fontIcon = null;
    /**
     * @var string
     */
    protected $class = null;
    /**
     * @var string
     */
    protected $data;

    /**
     * @param string $text
     * @param string $url
     */
    public function __construct($text, $url = null)
    {
        // Set properties.
        $this->text = $text;
        $this->url = $url;
    }

    /**
     * @return string
     *
     * @throws \Exception
     * @throws \Twig_Error
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @param string $text
     * @param string $url
     *
     * @return LinkItem
     */
    public static function create($text, $url = null)
    {
        return new LinkItem($text, $url);
    }

    /**
     * @param string $id
     *
     * @return LinkItem
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Returns the URI.
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $url
     *
     * @return LinkItem
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Returns the URI.
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Sets the text for this menu item.
     *
     * @param string $text
     *
     * @return LinkItem
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return string
     */
    public function render()
    {
        if (empty($this->url)) {
            return $this->text;
        }

        $icon = '';

        if ($this->icon != null) {
            $icon = "<img src=\"{$this->icon}\" alt=\"\" /> ";
        } elseif ($this->fontIcon != null) {
            $icon = "<i class=\"icon-{$this->fontIcon}\"></i> ";
        }

        return "<a href=\"{$this->url}\" class=\"{$this->class}\" data=\"{$this->data}\">{$icon}{$this->text}</a>";
    }

    /**
     * @param string $icon
     *
     * @return LinkItem
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param string $fontIcon
     *
     * @return $this
     */
    public function setFontIcon($fontIcon)
    {
        $this->fontIcon = $fontIcon;

        return $this;
    }

    /**
     * @return string
     */
    public function getFontIcon()
    {
        return $this->fontIcon;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     *
     * @return LinkItem
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @param string $data
     *
     * @return LinkItem
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }
}
