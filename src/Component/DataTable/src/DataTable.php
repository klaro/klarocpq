<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\DataTable;

use Klaro\Component\DataTable\Table\DataTableColumn;
use Klaro\Component\DataTable\Table\HeaderItem;
use Klaro\Component\DataTable\Table\DataTableItem;

/**
 * Utility for rendering an HTML table easily from an iterable object or array.
 *
 * Class DataTable
 * @package Klaro\DataTable
 */
class DataTable implements \Serializable, \JsonSerializable
{
    /** @var DataTableColumn[] */
    private $columns = null;

    /** @var array|null */
    private $rows = null;

    /**
     * DataTable constructor.
     */
    public function __construct()
    {
        $this->rows = array();
    }

    /**
     * @return DataTable
     */
    public static function create()
    {
        return new self;
    }

    /**
     * @param $columnId
     * @param $title
     * @param bool     $addToList
     *
     * @return DataTableColumn
     */
    public function createColumn($columnId, $title, $addToList = true)
    {
        $column = new DataTableColumn($columnId, $title);

        if ($addToList) {
            $this->addColumn($column);
        }

        return $column;
    }

    /**
     * @param DataTableColumn $column
     */
    public function addColumn(DataTableColumn $column)
    {
        $this->columns[] = $column;
    }

    /**
     * @param DataTableColumn|DataTableColumn[] $columns
     */
    public function setColumns()
    {
        if (is_array(func_get_arg(0))) {
            $this->columns = func_get_arg(0);
        } else {
            $this->columns = func_get_args();
        }
    }

    /**
     * @return DataTableColumn[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @return array|null
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * @param array $rows
     */
    public function setRows($rows)
    {
        $this->rows = $rows;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return array(
            //'columns' => $this->columns,
            //'rows'    => $this->rows
        );
    }

    /**
     * @param DataTable   $dataTable
     * @param string      $dataTableId
     * @param string|null $cssClass
     * @param array       $dataAttributes
     *
     * @return null|string
     */
    public function render(
        $dataTableId = 'klaroDataTable',
        $cssClass = null,
        $dataAttributes = array()
    ) {
        $headers = array();
        $rows = array();

        $columns = $this->getColumns();

        if (!is_array($columns) || (count($columns) < 1)) {
            return null;
        }

        // Process column headers.
        foreach ($columns as $column) {
            $headerItem = $column->getHeaderItem();

            $headers[] = $headerItem;
        }

        // Process tables data.
        foreach ($this->getRows() as $data) {
            $rows[] = $this->processRow($data);
        }

        $dataAttributes = is_array($dataAttributes) ? $dataAttributes : [];

        return $this->doRender($dataTableId, $dataAttributes, $cssClass, $headers, $rows);
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {
        return serialize($this->getData());
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);

        $this->columns = isset($data['columns']) ? $data['columns'] : null;
        $this->rows = isset($data['rows']) ? $data['rows'] : null;
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return $this->getData();
    }

    /**
     * @param HeaderItem[]      $headers
     * @param DataTableItem[][] $rows
     *
     * @return string
     */
    private function doRender($dataTableId, array $dataAttributes, $cssClass, $headers, $rows)
    {
        $attributes = [
            'id'    => $dataTableId,
            'class' => trim('datatable '.$cssClass),
        ];

        foreach ($dataAttributes as $key => $value) {
            $attributes['data-'.$key] = (is_array($value) || $value instanceof \Traversable) ? json_encode($value) : $value;
        }

        $attributeList = [];

        foreach ($attributes as $attribute => $value) {
            $attributeList[] = $attribute.'="'.htmlspecialchars($value).'"';
        }

        return
            '<table '.implode(' ', $attributeList).'>'.
                $this->doRenderHeader($headers).
                $this->doRenderFooter($headers).
                $this->doRenderBody($rows).
            '</table>';
    }

    /**
     * @param HeaderItem[] $headers
     *
     * @return string
     */
    private function doRenderHeader($headers)
    {
        $columns = array_reduce($headers, function ($html, HeaderItem $header) {
            $th = '<th class="header';

            if ($header->getFixed()) {
                $th .= ' col-xs-1';
            }

            if ($header->getSortable()) {
                $th .= ' sortable';
            }

            $th .= '">';

            $th .= $header->render().'</th>';

            return $html.$th;
        }, '');

        return  '<thead><tr>'.$columns.'</tr></thead>';
    }

    /**
     * @param DataTableItem[][] $rows
     *
     * @return string
     */
    private function doRenderBody($rows)
    {
        $body = '';

        /** @var DataTableItem[] $rowItem */
        foreach ($rows as $rowItem) {
            $body .= '<tr>';

            /** @var DataTableItem $rowData */
            foreach ($rowItem as $rowData) {
                $body .= '<td>'.$rowData->render().'</td>';
            }

            $body .= '</tr>';
        }

        return '<tbody>'.$body.'</tbody>';
    }

    /**
     * @param HeaderItem[] $headers
     *
     * @return string
     */
    private function doRenderFooter($headers)
    {
        $footers = array_reduce($headers, function ($html, HeaderItem $header) {
            return $html.'<th class="col-xs-1"></th>';
        }, '');

        return '<tfoot><tr>'.$footers.'</tr></tfoot>';
    }

    public function processRow($data)
    {
        $rowData = [];

        /** @var DataTableColumn $column */
        // Go through each column to format the data.
        foreach ($this->getColumns() as $column) {
            $columnId = $column->getColumnId();
            $dataFormat = $column->getDataFormat();

            // Set text for the column. By default, it is fetched from the row array with columnId
            // but it can also be a static text
            $text = null;

            $columnValue = is_callable([$data, 'get'.$columnId]) ? call_user_func(
                [$data, 'get'.$columnId]
            ) : null;

            if (!isset($dataFormat['text'])) {
                $text = $columnValue;
            } else {
                if (is_callable($dataFormat['text']) && is_object($dataFormat['text'])) {
                    $text = $dataFormat['text']($columnValue, $data);
                } else {
                    $text = $dataFormat['text'];
                }
            }

            // Placeholder text.
            if (!$text && isset($dataFormat['emptyText'])) {
                $text = $dataFormat['emptyText'];
            }

            $rowItem = new DataTableItem($text);

            if (isset($dataFormat['icon'])) {
                $rowItem->setIcon($dataFormat['icon']);
            }

            if (isset($dataFormat['fontIcon'])) {
                $rowItem->setFontIcon($dataFormat['fontIcon']);
            }

            if (isset($dataFormat['class'])) {
                $rowItem->setClass($dataFormat['class']);
            }

            // Process url to make text into a link.
            if (isset($dataFormat['url'])) {
                if (is_callable($dataFormat['url']) && is_object($dataFormat['url'])) {
                    $url = call_user_func($dataFormat['url'], $columnValue, $data);
                } else {
                    $url = $dataFormat['url'];
                }

                $rowItem->setUrl($url);
            }

            $rowItem->setFixed($column->getHeaderItem()->getFixed());
            $rowData[] = $rowItem;
        }

        return $rowData;
    }
}
