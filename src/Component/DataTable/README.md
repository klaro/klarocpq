# Data table

Utility for rendering an HTML table easily from an iterable object or array.

Example:

```php
$dataTable = DataTable::create();

$dataTable->createColumn('id', 'Id');
$dataTable->createColumn('name', 'Name');

$dataTable->setRows([
    ['id' => 1, 'name' => 'First'],
    ['id' => 2, 'name' => 'Second'],
    ['id' => 3, 'name' => 'Third']
]);
```

This will output a HTML table:

| Id   | Name   |
| ---- | ------ |
| 1    | First  |
| 2    | Second |
| 3    | Third  |

