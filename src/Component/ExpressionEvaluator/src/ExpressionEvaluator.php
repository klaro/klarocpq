<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ExpressionEvaluator;

use Exception;
use Klaro\Component\Cache\ParserCache;
use Klaro\Component\Logger\Logger;
use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;

/**
 * Service for evaluating expressions based on Symfony Expression Evaluator.
 *
 * Class ExpressionEvaluator
 *
 * @package Klaro\QuotationBundle\Library
 */
class ExpressionEvaluator extends ExpressionLanguage implements ExpressionEvaluatorInterface
{
    /** @var Logger */
    protected $logger;

    /** @var array */
    protected $context;

    /**
     * ExpressionEvaluator constructor.
     *
     * @param ParserCache $cache
     * @param Logger      $logger
     * @param array       $context
     */
    public function __construct(ParserCache $cache, Logger $logger, array $context = [])
    {
        parent::__construct($cache);

        $this->logger = $logger;
        $this->context = $context;
    }

    /**
     * Evaluates the given expression in the quotation context. Additional variables given in the second argument
     * are added to the evaluation context.
     *
     * @param string $expression Evaluated expression.
     * @param array  $context    Context variables.
     *
     * @return null|string
     */
    public function evaluateExpression($expression, $context = [])
    {
        $contextVariables = $context + $this->context;

        $value = null;

        try {
            $value = parent::evaluate($expression, $contextVariables);
        } catch (Exception $e) {
            $this->logger->error(
                'Failed to parse "' . $expression . '"',
                array(
                    'error' => $e->getMessage(),
                    'context' => $contextVariables,
                )
            );
        }

        return $value;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param array $context
     */
    public function setContext($context)
    {
        $this->context = $context;
    }

    /**
     * Register custom functions.
     */
    protected function registerFunctions()
    {
        // Register core functions.
        parent::registerFunctions();

        $this->addFunction(ExpressionFunction::fromPhp('array_combine', 'combine'));
        $this->addFunction(ExpressionFunction::fromPhp('date'));
        $this->addFunction(ExpressionFunction::fromPhp('is_numeric'));
        $this->addFunction(ExpressionFunction::fromPhp('pi'));
        $this->addFunction(ExpressionFunction::fromPhp('strtolower', 'lowercase'));
        $this->addFunction(ExpressionFunction::fromPhp('strtoupper', 'uppercase'));
        $this->addFunction(ExpressionFunction::fromPhp('sqrt'));

        // Register explicit empty because it is not recognized as a function
        $this->register(
            'empty',
            static function ($str) {
                return sprintf('empty(%s)', $str);
            },
            static function ($arguments, $str) {
                return empty($str);
            }
        );
    }
}
