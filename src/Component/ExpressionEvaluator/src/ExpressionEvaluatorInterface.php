<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ExpressionEvaluator;

interface ExpressionEvaluatorInterface
{
    /**
     * Evaluate an expression string.
     *
     * @param $expression
     * @param array      $context
     *
     * @return mixed
     */
    public function evaluateExpression($expression, $context = []);
}
