<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Logger;

use Psr\Log\LoggerInterface;

/**
 * Logger utility.
 *
 * Class Logger
 * @package Klaro\Component\Logger
 */
class Logger implements LoggerInterface
{

    const LOG_TYPE_ERROR   = 'error';
    const LOG_TYPE_WARNING = 'warning';
    const LOG_TYPE_INFO    = 'info';
    /** @var LoggerInterface */
    protected $logger;

    /** @var bool */
    protected $debugMode;

    /** @var array */
    protected $messages;

    /**
     * @param LoggerInterface $logger
     * @param bool            $debugMode
     */
    public function __construct(LoggerInterface $logger = null, $debugMode = true)
    {
        $this->logger = $logger;
        $this->debugMode = $debugMode;

        $this->messages = array();
    }

    /**
     * {@inheritDoc}
     */
    public function info($message, array $context = array())
    {
        if ($this->debugMode) {
            $this->addMessage(static::LOG_TYPE_INFO, $message, $context);
        }

        if ($this->logger) {
            $this->logger->info($message, $context);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function error($message, array $context = array())
    {
        if ($this->debugMode) {
            $this->addMessage(static::LOG_TYPE_ERROR, $message, $context);
        }

        if ($this->logger) {
            $this->logger->error($message, $context);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function warning($message, array $context = array())
    {
        if ($this->debugMode) {
            $this->addMessage(static::LOG_TYPE_WARNING, $message, $context);
        }

        if ($this->logger) {
            $this->logger->warning($message, $context);
        }
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        $errors = array_filter($this->messages, function ($message) {
            return ($message['type'] == static::LOG_TYPE_ERROR);
        });

        return $errors;
    }

    /**
     * @return int
     */
    public function getErrorCount()
    {
        return count($this->getErrors());
    }

    /**
     * @return int
     */
    public function getMessageCount()
    {
        return count($this->messages);
    }

    /**
     * {@inheritDoc}
     */
    public function emergency($message, array $context = array())
    {
        // TODO: Implement emergency() method.
    }

    /**
     * {@inheritDoc}
     */
    public function alert($message, array $context = array())
    {
        // TODO: Implement alert() method.
    }

    /**
     * {@inheritDoc}
     */
    public function critical($message, array $context = array())
    {
        // TODO: Implement critical() method.
    }

    /**
     * {@inheritDoc}
     */
    public function notice($message, array $context = array())
    {
        // TODO: Implement notice() method.
    }

    /**
     * {@inheritDoc}
     */
    public function debug($message, array $context = array())
    {
        // TODO: Implement debug() method.
    }

    /**
     * {@inheritDoc}
     */
    public function log($level, $message, array $context = array())
    {
        // TODO: Implement log() method.
    }

    /**
     * @param string $type    Type of message, one of LOG_TYPE_ERROR, LOG_TYPE_WARNING or LOG_TYPE_INFO
     * @param string $message Message text
     * @param array  $context Additional context values
     */
    private function addMessage($type, $message, $context = array())
    {
        $this->messages[] = array(
            'type' => $type,
            'message' => $message,
            'context' => $context,
        );
    }
}
