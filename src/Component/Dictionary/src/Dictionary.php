<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Dictionary;

use DateTime;
use Countable;
use Iterator;
use JsonSerializable;
use Serializable;

/**
 * This class represents a Dictionary<string, mixed>.
 *
 * Every key is represented as a string.
 */
class Dictionary implements Iterator, Serializable, JsonSerializable, Countable
{
    private $items = array();
    private $position = 0;
    private $keys;

    public function __construct($array = null)
    {
        if ($array) {
            foreach ($array as $key => $value) {
                $this->add($key, $value);
            }
        }
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->items[$this->key()];
    }

    public function key()
    {
        return (string) $this->keys[$this->position];
    }

    /**
     * Returns an array containing all keys.
     *
     * @return mixed
     */
    public function getKeys()
    {
        $keys = array();

        if (is_array($this->keys)) {
            foreach ($this->keys as $key) {
                $keys[] = (string) $key;
            }
        }

        return $keys;
    }

    /**
     * Returns the number of items in the map.
     *
     * @return int
     */
    public function count()
    {
        return is_array($this->keys) ? count($this->keys) : 0;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->keys[$this->position]);
    }

    /**
     * Adds a new item to the options list.
     *
     * @param $key
     * @param $value
     */
    public function add($key, $value)
    {
        $this->items[$key] = $value;
        $this->keys = array_keys($this->items);
    }

    /**
     * Returns the value corresponding the given key.
     *
     * @param $key
     *
     * @return mixed
     */
    public function get($key)
    {
        if ($key instanceof DateTime) {
            return false;
        }

        return isset($this->items[$key]) ? $this->items[$key] : null;
    }

    /**
     * Prepends a new item to the options list.
     *
     * @param $key
     * @param $value
     */
    public function prepend($key, $value)
    {
        $this->items[$key] = $value;
        $this->keys = array_keys($this->items);
    }

    /**
     * Overrides existing options with the specified array.
     *
     * @param array $overrideOptions
     */
    public function override($overrideOptions)
    {
        if ($overrideOptions === null) {
            return;
        }

        $this->items = $overrideOptions + $this->items;
        $this->keys = array_keys($this->items);
    }

    /**
     * Overrides existing options with the specified array.
     *
     * @param array $overrideOptions
     */
    public function overrideOptionTexts($overrideOptions)
    {
        if ($overrideOptions === null) {
            return;
        }

        foreach ($this->items as $key => $item) {
            if (array_key_exists($key, $overrideOptions)) {
                $this->items[$key]['text'] = $overrideOptions[$key];
            }
        }
    }

    public function jsonSerialize()
    {
        return $this->items;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     *
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize($this->items);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     *
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $this->items = unserialize($serialized);
    }
}
