<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\DataCollector;

use Klaro\Component\Logger\Logger;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector as BaseDataCollector;

/**
 * Data collector for showing data in the Symfony web toolbar.
 *
 * Class DataCollector
 * @package Klaro\Component\DataCollector
 */
abstract class DataCollector extends BaseDataCollector
{
    /** @var Logger */
    protected $logger;

    /**
     * @param Logger $logger
     */
    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->data = array(
            'messages' => $this->sanitizeMessages($this->logger->getMessages()),
            'message_count' => $this->logger->getMessageCount(),
            'error_count' => $this->logger->getErrorCount(),
        );
    }

    /**
     * @return mixed
     */
    public function getMessages()
    {
        return $this->data['messages'];
    }

    /**
     * @return mixed
     */
    public function getMessageCount()
    {
        return $this->data['message_count'];
    }

    /**
     * @return mixed
     */
    public function getErrorCount()
    {
        return $this->data['error_count'];
    }

    /**
     * Go through context variables and clean out stuff that cannot be serialized.
     *
     * @param $messages
     *
     * @return mixed
     */
    private function sanitizeMessages($messages)
    {
        foreach ($messages as $i => $message) {
            $messages[$i]['context'] = $this->sanitizeContext($message['context']);
        }

        return $messages;
    }

    /**
     * Prepare context variable for printing, go through arrays recursively.
     *
     * @param $context
     *
     * @return mixed
     */
    private function sanitizeContext($context)
    {
        if (is_array($context)) {
            foreach ($context as $key => $value) {
                $context[$key] = $this->sanitizeContext($value);
            }

            return $context;
        }

        if (is_resource($context)) {
            return sprintf('Resource(%s)', get_resource_type($context));
        }

        if (is_object($context) && !($context instanceof \Serializable)) {
            return sprintf('Object(%s)', get_class($context));
        }

        return $context;
    }
}
