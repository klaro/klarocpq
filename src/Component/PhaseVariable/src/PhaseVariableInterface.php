<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\PhaseVariable;

/**
 * Common interface for querying properties from a configuration object. Configuration objects that are tagged
 * with the name "klaro_quotation.configuration" are available to use in expressions as configuration.[object] where
 * [object] is the alias of the tagged service. Properties can then be accessed using object notation or array access.
 *
 * Interface ConfigurationInterface
 * @package Klaro\QuotationBundle\Api
 */
interface PhaseVariableInterface extends \ArrayAccess
{

    /**
     * Magic method for convenience when used in expressions, should result in a call to ConfigurationInterface::get().
     * Eg. configuration.[object].Property is the same as configuration.[object].get("Property").
     *
     * @param $property
     *
     * @return mixed
     */
    public function __get($property);

    /**
     * Magig method checker, needed for __get() to work properly.
     *
     * @param $property
     *
     * @return mixed
     */
    public function __isset($property);

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments);
    /**
     * Returns if given property is available.
     *
     * @param $property
     *
     * @return bool
     */
    public function has($property);

    /**
     * Get given property value.
     *
     * @param $property
     * @param array    $arguments
     *
     * @return mixed
     */
    public function get($property, $arguments = []);
}
