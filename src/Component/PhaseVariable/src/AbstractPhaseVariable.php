<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\PhaseVariable;

abstract class AbstractPhaseVariable implements PhaseVariableInterface
{
    /**
     * {@inheritDoc}
     */
    public function __get($property)
    {
        return $this->get($property);
    }

    /**
     * @param $property
     *
     * @return mixed
     */
    public function __isset($property)
    {
        return $this->has($property);
    }

    /**
     * @param $name
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->get($name, $arguments);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetExists($offset)
    {
        return $this->has($offset);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetSet($offset, $value)
    {
        // ...
    }

    /**
     * {@inheritDoc}
     */
    public function offsetUnset($offset)
    {
        // ...
    }
}
