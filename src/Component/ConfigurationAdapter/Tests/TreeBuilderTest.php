<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter\Tests;

use Klaro\Component\ConfigurationAdapter\TreeBuilder;
use PHPUnit\Framework\TestCase;

class TreeBuilderTest extends TestCase
{
    public function testEmptyObject(): void
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder->root();

        $this->assertEquals($this->getExpectedResult('testEmptyObject.json'), serialize($treeBuilder->buildTree()));
    }

    public function testTitle(): void
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root();
        $rootNode->title('Node Title');

        $this->assertEquals($this->getExpectedResult('testTitle.json'), serialize($treeBuilder->buildTree()));
    }

    public function testData(): void
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root();

        $rootNode
            ->addData('ModuleId', 'FN')
            ->addData('ManufacturerId', 3);

        $this->assertEquals($this->getExpectedResult('testData.json'), serialize($treeBuilder->buildTree()));
    }

    public function testItemData(): void
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root();

        $rootNode
            ->addItemData('ItemCode', 'ITEM-1')
            ->addItemData('CostPrice', 100)
            ->addItemData('SalesPrice', 200)
            ->addItemData('Quantity', 2);

        $this->assertEquals($this->getExpectedResult('testItemData.json'), serialize($treeBuilder->buildTree()));
    }

    public function testSections(): void
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root();

        $rootNode
            ->section('first')
                ->object()
                    ->title('A single object')
                ->end()
            ->end()
            ->section('second')
                ->array()
                    ->object()
                        ->title('First object in array')
                    ->end()
                    ->object()
                        ->title('Second object in array')
                    ->end()
                ->end()
            ->end()
            ->section('third')
                ->integer(2)->end()
            ->end()
            ->section('fourth')
                ->boolean(true)->end()
            ->end()
            ->section('fifth')
                ->boolean(false)->end()
            ->end();

        $this->assertEquals($this->getExpectedResult('testSections.json'), serialize($treeBuilder->buildTree()));
    }

    public function testAdditional(): void
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root();

        $rootNode
            ->additional('extra')
                ->object()
                    ->title('Additional section')
                ->end()
            ->end()
            ->additional('another')
                ->array()
                    ->object()
                        ->title('First object in array')
                    ->end()
                    ->object()
                        ->title('Second object in array')
                    ->end()
                ->end()
            ->end();

        $this->assertEquals($this->getExpectedResult('testAdditional.json'), serialize($treeBuilder->buildTree()));
    }

    /**
     * Load specified file for test, decode and serialize
     *
     * @param string $filename
     *
     * @return string
     */
    private function getExpectedResult(string $filename): string
    {
        $json = $this->readFile($filename);

        return serialize(json_decode($json, false));
    }

    /**
     * Read file required for test
     *
     * @param string $filename
     *
     * @return false|string
     */
    private function readFile(string $filename)
    {
        return file_get_contents(__DIR__.'/Resources/'.$filename);
    }
}
