<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

class ArrayTreeNode extends ParentTreeNode
{
    /**
     * @var TreeNodeInterface|TreeNode[]
     */
    protected $children;

    /**
     * ArrayTreeNode constructor.
     * @param $name
     * @param TreeNodeInterface|null $parent
     */
    public function __construct($name, TreeNodeInterface $parent = null)
    {
        parent::__construct($name, $parent);

        $this->children = [];
    }

    /**
     * @param TreeNode $node
     *
     * @return $this
     */
    public function append(TreeNode $node)
    {
        $this->children[] = $node->setParent($this);

        return $this;
    }

    /**
     * @return ObjectTreeNode
     */
    public function object()
    {
        return $this->getNodeBuilder()->createObject();
    }

    /**
     * @param $name
     *
     * @return ProductTreeNode
     */
    public function product($name)
    {
        return $this->getNodeBuilder()->createProduct($name);
    }

    /**
     * @param $value
     *
     * @return IntegerTreeNode
     */
    public function integer($value)
    {
        return $this->getNodeBuilder()->createInteger($value);
    }

    /**
     * @param $value
     *
     * @return BooleanTreeNode
     */
    public function boolean($value)
    {
        return $this->getNodeBuilder()->createBoolean($value);
    }

    /**
     * @return array
     */
    public function build()
    {
        $object = [];

        foreach ($this->children as $child) {
            $object[] = $child->build();
        }

        return $object;
    }
}
