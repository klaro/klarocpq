<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

/**
 * Interface ParentTreeNodeInterface
 * @package Klaro\Component\ConfigurationAdapter
 */
interface ParentTreeNodeInterface
{
    public function append(TreeNode $node);

    public function setBuilder(TreeNodeBuilder $builder);
}
