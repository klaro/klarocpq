<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

class ProductListSectionTreeNode extends ParentTreeNode
{
    /** @var ProductListTreeNode */
    protected $productList;

    /**
     * ProductListSectionTreeNode constructor.
     * @param $name
     * @param TreeNodeInterface|null $parent
     */
    public function __construct($name, TreeNodeInterface $parent = null)
    {
        parent::__construct($name, $parent);

        $this->productList = null;
    }

    /**
     * @param TreeNode $node
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function append(TreeNode $node)
    {
        if ($this->productList) {
            throw new \Exception('Can only add one product list');
        }

        $this->productList = $node->setParent($this);

        return $this;
    }

    /**
     * @return ProductListTreeNode
     */
    public function productList()
    {
        return $this->getNodeBuilder()->createProductList();
    }

    /**
     * @return \stdClass
     */
    public function build()
    {
        $object = new \stdClass();

        if ($this->title) {
            $object->title = $this->getTitle();
        }

        if ($this->productList) {
            $object->products = $this->productList->build();
        }

        return $object;
    }
}
