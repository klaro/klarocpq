<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;
use Klaro\Component\FormData\ProductLineDataNode;

interface ConfigurationAdapterInterface
{
    public function process(ConfiguratorDefinitionNode $configuration, ProductLineDataNode $productLineDataNode);
}
