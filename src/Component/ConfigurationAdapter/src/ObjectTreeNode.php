<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

class ObjectTreeNode extends ParentTreeNode
{
    /** @var TreeNode[] */
    protected $children;

    /** @var array */
    protected $inputData;

    protected $itemData;

    /**
     * ObjectTreeNode constructor.
     * @param $name
     * @param TreeNodeInterface|null $parent
     */
    public function __construct($name, TreeNodeInterface $parent = null)
    {
        parent::__construct($name, $parent);

        $this->children = [];
        $this->inputData = [];
        $this->itemData = [];
    }

    /**
     * @param TreeNode $node
     *
     * @return $this
     */
    public function append(TreeNode $node)
    {
        $this->children[$node->getName()] = $node->setParent($this);

        return $this;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->inputData;
    }

    /**
     * @param $inputData
     *
     * @return $this
     */
    public function setData($inputData)
    {
        $this->inputData = $inputData;

        return $this;
    }

    /**
     * @param $field
     * @param $value
     *
     * @return $this
     */
    public function addData($field, $value)
    {
        $this->inputData[$field] = $value;

        return $this;
    }

    /**
     * @return array
     */
    public function getItemData()
    {
        return $this->itemData;
    }

    /**
     * @param array $itemData
     *
     * @return $this
     */
    public function setItemData($itemData)
    {
        $this->itemData = $itemData;

        return $this;
    }

    /**
     * @param $field
     * @param $value
     *
     * @return $this
     */
    public function addItemData($field, $value)
    {
        $this->itemData[$field] = $value;

        return $this;
    }

    /**
     * @param $name
     *
     * @return ProductListSectionTreeNode
     */
    public function productListSection($name)
    {
        return $this->getNodeBuilder()->createProductListSection($name);
    }

    /**
     * @param $name
     *
     * @return SectionTreeNode
     */
    public function section($name)
    {
        return $this->getNodeBuilder()->createSection($name);
    }

    /**
     * @param $name
     *
     * @return AdditionalSectionTreeNode
     */
    public function additional($name)
    {
        return $this->getNodeBuilder()->createAdditionalSection($name);
    }

    /**
     * @return \stdClass
     */
    public function build()
    {
        $object = new \stdClass();

        if ($this->title) {
            $object->title = $this->getTitle();
        }

        if (!empty($this->inputData)) {
            $object->data = (object) $this->getData();
        }

        if (!empty($this->itemData)) {
            $object->item = (object) $this->getItemData();
        }

        if ($this->children) {
            foreach ($this->children as $ref => $section) {
                $type = ($section instanceof AdditionalSectionTreeNode) ? 'additional' : 'sections';

                if (!isset($object->{$type})) {
                    $object->{$type} = new \stdClass();
                }

                $object->{$type}->{$ref} = $section->build();
            }
        }

        return $object;
    }
}
