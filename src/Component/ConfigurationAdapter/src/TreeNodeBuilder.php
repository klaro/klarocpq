<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

class TreeNodeBuilder implements TreeNodeInterface
{
    /** @var ParentTreeNodeInterface */
    protected $parent;
    protected $nodeMapping;

    /**
     * TreeNodeBuilder constructor.
     * @param $parent
     */
    public function __construct()
    {
        $this->nodeMapping = array(
            'productList' => __NAMESPACE__.'\\ProductListTreeNode',
            'product' => __NAMESPACE__.'\\ProductTreeNode',
            'object' => __NAMESPACE__.'\\ObjectTreeNode',
            'array' => __NAMESPACE__.'\\ArrayTreeNode',
            'integer' => __NAMESPACE__.'\\IntegerTreeNode',
            'boolean' => __NAMESPACE__.'\\BooleanTreeNode',
            'productListSection' => __NAMESPACE__.'\\ProductListSectionTreeNode',
            'section' => __NAMESPACE__.'\\SectionTreeNode',
            'additional' => __NAMESPACE__.'\\AdditionalSectionTreeNode',
        );
    }

    /**
     * @param ParentTreeNodeInterface|null $parent
     *
     * @return $this
     */
    public function setParent(ParentTreeNodeInterface $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @param $name
     *
     * @return ProductListSectionTreeNode
     */
    public function createProductListSection($name)
    {
        return $this->node($name, 'productListSection');
    }

    public function createProductList()
    {
        return $this->node(null, 'productList');
    }

    public function createProduct($name)
    {
        return $this->node($name, 'product');
    }

    public function createSection($name)
    {
        return $this->node($name, 'section');
    }

    public function createAdditionalSection($name)
    {
        return $this->node($name, 'additional');
    }

    public function createArray()
    {
        return $this->node(null, 'array');
    }

    public function createObject()
    {
        return $this->node(null, 'object');
    }

    public function createInteger($value)
    {
        return $this->node(null, 'integer')->setValue($value);
    }

    public function createBoolean($value)
    {
        return $this->node(null, 'boolean')->setValue($value);
    }

    /**
     * @param $name
     * @param $type
     *
     * @return mixed
     */
    public function node($name, $type)
    {
        $class = $this->getNodeClass($type);

        $node = new $class($name);

        $this->append($node);

        return $node;
    }

    /**
     * @param TreeNode $node
     *
     * @return $this
     */
    public function append(TreeNode $node)
    {
        if ($node instanceof ParentTreeNodeInterface) {
            $builder = clone $this;
            $builder->setParent(null);
            $node->setBuilder($builder);
        }

        if (null !== $this->parent) {
            $this->parent->append($node);
        }

        return $this;
    }

    /**
     * @param $type
     *
     * @return mixed
     */
    protected function getNodeClass($type)
    {
        if (!isset($this->nodeMapping[$type])) {
            throw new \RuntimeException(sprintf('The node type "%s" is not registered.', $type));
        }

        $class = $this->nodeMapping[$type];

        if (!class_exists($class)) {
            throw new \RuntimeException(sprintf('The node class "%s" does not exist.', $class));
        }

        return $class;
    }
}
