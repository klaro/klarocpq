<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

abstract class ParentTreeNode extends TreeNode implements ParentTreeNodeInterface
{
    /** @var TreeNodeBuilder */
    protected $nodeBuilder;

    /**
     * ParentTreeNode constructor.
     * @param $name
     * @param TreeNodeInterface|null $parent
     */
    public function __construct($name, TreeNodeInterface $parent = null)
    {
        parent::__construct($name, $parent);

        $this->nodeBuilder = null;
    }

    /**
     * @param TreeNodeBuilder $builder
     */
    public function setBuilder(TreeNodeBuilder $builder)
    {
        $this->nodeBuilder = $builder;
    }

    /**
     * @return TreeNodeBuilder
     */
    protected function getNodeBuilder()
    {
        if (null === $this->nodeBuilder) {
            $this->nodeBuilder = new TreeNodeBuilder();
        }

        return $this->nodeBuilder->setParent($this);
    }
}
