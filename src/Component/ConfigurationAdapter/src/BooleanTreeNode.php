<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

class BooleanTreeNode extends TreeNode
{
    /** @var bool */
    protected $value;

    /**
     * BooleanTreeNode constructor.
     * @param $name
     * @param TreeNodeInterface|null $parent
     */
    public function __construct($name, TreeNodeInterface $parent = null)
    {
        parent::__construct($name, $parent);

        $this->value = true;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = (bool) $value;

        return $this;
    }

    /**
     * @return bool
     */
    public function build()
    {
        return $this->value;
    }
}
