<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

class SectionTreeNode extends ParentTreeNode
{
    /** @var TreeNodeInterface|TreeNode */
    protected $child;

    /**
     * SectionTreeNode constructor.
     * @param $name
     * @param TreeNodeInterface|null $parent
     */
    public function __construct($name, TreeNodeInterface $parent = null)
    {
        parent::__construct($name, $parent);

        $this->child = null;
    }

    /**
     * @param TreeNode $node
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function append(TreeNode $node)
    {
        if ($this->child) {
            throw new \Exception('Can only have one type of value!');
        }

        $this->child = $node->setParent($this);

        return $this;
    }

    /**
     * @param $name
     *
     * @return ProductTreeNode
     */
    public function product($name)
    {
        return $this->getNodeBuilder()->createProduct($name);
    }

    /**
     * @return ObjectTreeNode
     */
    public function object()
    {
        return $this->getNodeBuilder()->createObject();
    }

    /**
     * @return ArrayTreeNode
     */
    public function array()
    {
        return $this->getNodeBuilder()->createArray();
    }

    /**
     * @param $value
     *
     * @return IntegerTreeNode
     */
    public function integer($value)
    {
        return $this->getNodeBuilder()->createInteger($value);
    }

    /**
     * @param $value
     *
     * @return BooleanTreeNode
     */
    public function boolean($value)
    {
        return $this->getNodeBuilder()->createBoolean($value);
    }

    /**
     * @return bool|\stdClass
     */
    public function build()
    {
        return $this->child ? $this->child->build() : true;
    }
}
