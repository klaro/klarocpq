<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

abstract class TreeNode implements TreeNodeInterface
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $title;

    /**
     * @var TreeNodeInterface|null
     */
    protected $parent;

    /**
     * TreeNode constructor.
     * @param null|TreeNodeInterface $parent
     */
    public function __construct($name, $parent)
    {
        $this->name = $name;
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     *
     * @return $this
     */
    public function title($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return null|TreeNodeInterface
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param $parent
     *
     * @return $this
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return null|TreeNodeInterface
     */
    public function end()
    {
        return $this->parent;
    }

    /**
     * @return \stdClass|bool
     */
    abstract public function build();
}
