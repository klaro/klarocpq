<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

use RuntimeException;
use stdClass;

class TreeBuilder implements TreeNodeInterface
{
    /** @var stdClass */
    protected $tree;

    /** @var TreeNode */
    protected $root;

    /**
     * @param $name
     * @param string $type
     *
     * @return mixed
     */
    public function root($name = '', $type = 'object')
    {
        $builder = new TreeNodeBuilder();

        $x = $builder->node($name, $type);
        $x->setParent($this);

        $this->root = $x;

        return $this->root;
    }

    /**
     * Builds the tree.
     *
     * @return bool|stdClass
     *
     * @throws RuntimeException
     */
    public function buildTree()
    {
        if (null === $this->root) {
            throw new RuntimeException('The configuration tree has no root node.');
        }
        if (null !== $this->tree) {
            return $this->tree;
        }

        return $this->tree = $this->root->build();
    }
}
