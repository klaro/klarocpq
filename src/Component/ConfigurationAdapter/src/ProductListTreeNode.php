<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\ConfigurationAdapter;

class ProductListTreeNode extends ParentTreeNode
{
    /** @var ArrayTreeNode */
    protected $productArray;

    /**
     * ProductListTreeNode constructor.
     * @param $name
     * @param TreeNodeInterface|null $parent
     */
    public function __construct($name, TreeNodeInterface $parent = null)
    {
        parent::__construct($name, $parent);

        $this->productArray = null;
    }

    /**
     * @param TreeNode $node
     *
     * @return $this
     *
     * @throws \Exception
     */
    public function append(TreeNode $node)
    {
        if ($this->productArray) {
            throw new \Exception('Can only add one productArray');
        }

        $this->productArray = $node->setParent($this);

        return $this;
    }

    /**
     * @return ArrayTreeNode
     */
    public function products()
    {
        return $this->getNodeBuilder()->createArray();
    }

    /**
     * @return array
     */
    public function build()
    {
        return $this->productArray->build();
    }
}
