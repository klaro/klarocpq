<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Util\Test;

use PHPUnit\Framework\TestCase;
use Klaro\Component\Util\ArrayUtil;

class ArrayUtilTest extends TestCase
{
    /**
     *
     */
    public function testInArray()
    {
        $haystack = ['a', 'b', 'c', 'd', 'e', 'f'];

        $this->assertTrue(ArrayUtil::inArray('b', $haystack));
        $this->assertFalse(ArrayUtil::inArray('g', $haystack));

        $haystack = [
            (object) ['id' => 1, 'value' => 'a'],
            (object) ['id' => 2, 'value' => 'b'],
            (object) ['id' => 3, 'value' => 'c'],
            (object) ['id' => 4, 'value' => 'd'],
            (object) ['id' => 5, 'value' => 'e'],
            (object) ['id' => 6, 'value' => 'f'],
        ];

        $this->assertFalse(ArrayUtil::inArray('b', $haystack));
        $this->assertTrue(ArrayUtil::inArray('b', $haystack, function ($needle, $value) {
            return $needle === $value->value;
        }));
    }

    /**
     *
     */
    public function testArrayMergeRecursive()
    {
        $a = [
            'id' => 1,
            'values' => [
                'title' => 'Some Title',
                'description' => 'An appropriate description.',
                'refs' => [1, 5, 7],
            ],
        ];

        $b = [
            'values' => [
                'title' => 'Overridden title',
                'author' => 'Johnny Appleseed',
                'refs' => null,
            ],
            'meta' => [
                'source' => [
                    'url' => 'http://',
                ],
            ],
        ];

        $expectedResult = [
            'id' => 1,
            'values' => [
                'title' => 'Overridden title',
                'description' => 'An appropriate description.',
                'refs' => null,
                'author' => 'Johnny Appleseed',
            ],
            'meta' => [
                'source' => [
                    'url' => 'http://',
                ],
            ],
        ];

        $actualResult = ArrayUtil::arrayMergeRecursive($a, $b);

        $this->assertEquals(serialize($expectedResult), serialize($actualResult));
    }

    /**
     *
     */
    public function testArrayToObject()
    {
        $source = <<<EOT
{
    "id": 1,
    "values": {
        "title": "Some Title",
        "description": "An appropriate description.",
        "refs": [1, 3, 5],
        "author": "Johnny Appleseed"
    },
    "meta": {
        "source": {
            "url": "http://"
        }
    }
}
EOT;

        $expectedResult = json_decode($source, false);
        $actualResult = ArrayUtil::arrayToObject(json_decode($source, true));

        $this->assertEquals(serialize($expectedResult), serialize($actualResult));
    }
}
