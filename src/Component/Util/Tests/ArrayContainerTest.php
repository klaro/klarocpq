<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Util\Test;

use PHPUnit\Framework\TestCase;
use Klaro\Component\Util\ArrayContainer;

class ArrayContainerTest extends TestCase
{
    /**
     *
     */
    public function testArrayContainer()
    {
        $values = [
            'id' => 1,
            'title' => 'Some Title',
            'refs' => [1, 3, 5],
        ];

        $container = new ArrayContainer($values);
        $this->assertEquals(serialize($values), $container->serialize());

        foreach ($values as $key => $value) {
            $this->assertEquals($container->get($key), $value);
        }

        $container->set('meta', 'MetaData');
        $this->assertEquals($container->get('meta'), 'MetaData');
        $this->assertNull($container->get('Unknown value'));
    }
}
