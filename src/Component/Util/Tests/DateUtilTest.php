<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Util\Tests;

use PHPUnit\Framework\TestCase;
use Klaro\Component\Util\DateUtil;

class DateUtilTest extends TestCase
{
    /**
     *
     */
    public function testToDateTime()
    {
        $timeString = '2018-01-10 10:30';

        $date = \DateTime::createFromFormat('Y-m-d H:i', $timeString);
        $timestamp = $date->getTimestamp();

        $this->assertEquals($date, DateUtil::toDateTime($date));
        $this->assertEquals($date, DateUtil::toDateTime($timeString));
        $this->assertEquals($date, DateUtil::toDateTime($timestamp));
    }
}
