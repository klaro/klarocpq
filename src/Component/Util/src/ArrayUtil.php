<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Util;

class ArrayUtil
{
    /**
     * @param $needle
     * @param array    $haystack
     * @param callable $callback
     *
     * @return bool
     */
    public static function inArray($needle, $haystack, callable $callback = null)
    {
        $found = false;

        if (!is_array($haystack)) {
            $found = false;
        } elseif (is_callable($callback)) {
            foreach ($haystack as $value) {
                if (call_user_func_array($callback, [$needle, $value]) === true) {
                    $found = true;
                    break;
                }
            }
        } else {
            $found = in_array($needle, $haystack);
        }

        return $found;
    }

    /**
     * Merge one or more arrays ignoring null values in following arrays if key exists in initial array
     *
     * @param array $array1 Initial array to merge.
     * @param array $array2 [optional]
     * @param array ...$_   [optional]
     *
     * @return array the resulting array.
     */
    public static function array_merge_retain(array $array1, array $array2 = [], array ...$_): array
    {
        if (false === empty($_)) {
            return self::array_merge_retain($array2, ...$_);
        }

        foreach ($array2 as $key => $value) {
            if (array_key_exists($key, $array1) && null === $value) {
                continue;
            }

            $array1[$key] = $value;
        }

        return $array1;
    }

    /**
     * Merge source associative array to target based on keys.
     *
     * @param mixed $to   Target array
     * @param mixed $from Source array
     * @param $skip
     *
     * @return array
     */
    public static function arrayMergeAssociative($to, $from, $skip = [])
    {
        $keys = array_flip(array_flip(array_merge(array_keys($to), array_keys($from))));

        foreach ($keys as $key) {
            if (isset($from[$key])) {
                if (in_array($key, $skip)) {
                    $to[$key] = $from[$key];
                } else {
                    if (!is_array($from[$key])) {
                        if (!isset($to[$key])) {
                            $to[$key] = $from[$key];
                        }
                    } else {
                        if (isset($to[$key])) {
                            $to[$key] = $to[$key] + $from[$key];
                        } else {
                            if (isset($from[$key])) {
                                $to[$key] = $from[$key];
                            }
                        }
                    }
                }
            }
        }

        return $to;
    }

    /**
     * Recursive array merge, taken from http://php.net/manual/en/function.array-merge-recursive.php#92195
     *
     * @param array $array1
     * @param array $array2
     *
     * @return array
     */
    public static function arrayMergeRecursive(array &$array1, array &$array2)
    {
        $merged = $array1;

        foreach ($array2 as $key => &$value) {
            if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
                $merged[$key] = static::arrayMergeRecursive($merged[$key], $value);
            } else {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }

    /**
     * @param array $array
     *
     * @return object
     */
    public static function arrayToObject(array $array)
    {
        return json_decode(json_encode($array));
    }
}
