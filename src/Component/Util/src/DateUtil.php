<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Util;

class DateUtil
{
    /**
     * Convert string or timestamp to PHP's DateTime object.
     *
     * @param mixed $time
     *
     * @return \DateTime
     */
    public static function toDateTime($time)
    {
        if (!($time instanceof \DateTime)) {
            if (is_string($time)) {
                $time = new \DateTime($time);
            } elseif (is_int($time)) {
                $timestamp = $time;

                $time = new \DateTime();
                $time->setTimestamp($timestamp);
            }
        }

        return $time;
    }
}
