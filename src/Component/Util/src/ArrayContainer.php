<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Util;

class ArrayContainer
{
    /** @var array */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct($data = [])
    {
        $this->data = $data;
    }

    /**
     * @param string $field
     *
     * @return mixed
     */
    public function __get($field)
    {
        return $this->get($field);
    }

    /**
     * @param string $field
     * @param mixed  $value
     *
     * @return $this
     */
    public function __set($field, $value)
    {
        $this->set($field, $value);

        return $this;
    }

    /**
     * @param string $name
     * @param mixed  $parameters
     *
     * @return mixed
     */
    public function __call($name, $parameters)
    {
        if (method_exists($this, $name)) {
            return call_user_func_array([$this, $name], $parameters);
        }
    }

    /**
     * Set conf item data.
     *
     * @param $data
     *
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get all data for this item as array.
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Return the given field's value.
     *
     * @param $field
     *
     * @return mixed
     */
    public function get($field, $arguments = null)
    {
        return isset($this->data[$field]) ? $this->data[$field] : null;
    }

    /**
     * Set a value to the given field.
     *
     * @param $field
     * @param $value
     *
     * @return $this
     */
    public function set($field, $value)
    {
        $this->data[$field] = $value;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function toArray()
    {
        return $this->getData();
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {
        return serialize($this->getData());
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized)
    {
        $this->data = unserialize($serialized);
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return $this->getData();
    }
}
