<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Validation;

class FormPhaseDataValidatorChain implements FormPhaseDataValidatorInterface
{
    /** @var FormPhaseDataValidatorInterface */
    protected $validators;

    /**
     * FormPhaseDataValidatorChain constructor.
     * @param FormPhaseDataValidatorInterface[] $validators
     */
    public function __construct(array $validators)
    {
        $this->validators = $validators;
    }

    /**
     * {@inheritdoc}
     */
    public function validate(array $values, ValidationResult $result)
    {
        foreach ($this->validators as $validator) {
            $validator->validate($values, $result);
        }
    }
}
