<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Validation;

class ValidationIssue
{
    const ERROR = 'error';
    const WARNING = 'warning';
    const INFO = 'info';

    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $key;
    /**
     * @var string
     */
    protected $reason;
    /**
     * @var string
     */
    protected $template;

    /**
     * @var string
     */
    protected $productLineId;

    /**
     * @var array
     */
    protected $parameters = [];

    /**
     * @param string $type
     * @param string $key
     * @param string $reason
     * @param string $template
     * @param array  $parameters
     */
    public function __construct($type, $key, $reason, $template, $parameters = [], $productLineId = null)
    {
        $this->type = $type;
        $this->key = $key;
        $this->reason = $reason;
        $this->template = $template;
        $this->parameters = $parameters;
        $this->productLineId = $productLineId;
    }

    /**
     * Formats and returns the error message for this issue.
     *
     * @return string
     */
    public function format()
    {
        $replace = function ($matches) {
            if (array_key_exists($matches[1], $this->parameters)) {
                return $this->parameters[$matches[1]];
            }

            return $matches[0];
        };

        return preg_replace_callback('~{{\s*([^}\s]+)\s*}}~', $replace, $this->template);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the key for this issue.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Returns the reason for issue.
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Overwrites the message template.
     *
     * @param string $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * Get productLineId
     *
     * @return string
     */
    public function getProductLineId()
    {
        return $this->productLineId;
    }

    /**
     * Set productLineId
     *
     * @param string $template
     */
    public function setProductLineId($productLineId)
    {
        $this->productLineId = $productLineId;
    }
}
