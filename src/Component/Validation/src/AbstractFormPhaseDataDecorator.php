<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Validation;

use Klaro\Component\FormData\FormPhaseData;
use Klaro\Component\FormData\FormPhaseDataInterface;

abstract class AbstractFormPhaseDataDecorator implements FormPhaseDataInterface
{
    protected $model;

    /**
     * AbstractFormPhaseDataDecorator constructor.
     *
     * @param $model
     */
    public function __construct(FormPhaseData $model)
    {
        $this->model = $model;
    }

    /**
     * {@inheritdoc}
     */
    public function __get($field)
    {
        return $this->get($field);
    }

    /**
     * {@inheritdoc}
     */
    public function __set($field, $value)
    {
        $this->set($field, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function __call($name, $parameters)
    {
        return $this->get($name, $parameters);
    }

    /**
     * @return ValidationResult
     */
    public function commit()
    {
        return $this->model->commit();
    }

    /**
     * @param $field
     *
     * @return bool
     */
    public function hasAppeared($field) : bool
    {
        return $this->model->hasAppeared($field);
    }

    /**
     * @param $field
     * @param bool $hasAppeared
     *
     * @return bool
     */
    public function hasChanged($field, $hasAppeared = true): bool
    {
        return $this->model->hasChanged($field, $hasAppeared);
    }

    /**
     * Check if given fields have been changed
     *
     * @param array $fields
     * @param bool $hasAppeared
     *
     * @return bool
     */
    public function hasChangedFields(array $fields, $hasAppeared = true): bool
    {
        return $this->model->hasChangedFields($fields, $hasAppeared);
    }

    /**
     * @return array
     */
    public function getChangedValues()
    {
        return $this->model->getChangedValues();
    }

    /**
     * Check if any fields have been changed
     *
     * @param array $fields
     *
     * @return bool
     */
    public function hasAnyChangedFields(): bool
    {
        return $this->model->hasAnyChangedFields();
    }

    /**
     * Returns list of changed fields names
     *
     * @param bool $hasAppeared
     *
     * @return array
     */
    public function getValuesUpdated($hasAppeared = true): array
    {
        return $this->model->getValuesUpdated($hasAppeared);
    }

    /**
     * Set value only if it differs
     *
     * @var String $field
     * @var mixed $value
     * @var bool $sanitize
     *
     * @return boolen True if difers and was set, false otherwise
     */
    public function setIfChanged($field, $value, $sanitize = true) : bool
    {
        return $this->model->setIfChanged($field, $value, $sanitize);
    }


    public function getValues(array $fields): array
    {
        foreach ($fields as $field) {
            $values[] = $this->get($field);
        }

        return $values ?? [];
    }

    public function setValues(array $array): void
    {
        $this->model->setValues($array);
    }

    /**
     * {@inheritdoc}
     */
    public function get($field, $arguments = null)
    {
        if (method_exists($this, $field)) {
            return call_user_func_array([$this, $field], is_array($arguments) ? $arguments : []);
        }

        return $this->model->get($field, $arguments);
    }

    /**
     * {@inheritdoc}
     */
    public function set($field, $value, $sanitize = true)
    {
        $this->model->set($field, $value, $sanitize);
    }

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return $this->model->toArray();
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return method_exists($this, $offset) || isset($this->model[$offset]);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        $this->set($offset, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->model->getId();
    }

    /**
     * {@inheritdoc}
     */
    function jsonSerialize()
    {
        return $this->model->jsonSerialize();
    }
}
