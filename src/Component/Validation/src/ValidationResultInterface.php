<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Validation;

interface ValidationResultInterface
{
    public function isValid();

    public function getValues();

    public function getMessages();

    public function addValidationIssue(ValidationIssue $issue);

    public function getValidationIssues();

    public function mergeValidationIssues(ValidationResultInterface $result);

    public function addValue($key, $value);
}
