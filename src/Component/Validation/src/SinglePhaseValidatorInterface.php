<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Validation;

use Klaro\Component\FormData\SinglePhaseDataNode;

interface SinglePhaseValidatorInterface extends BaseValidatorInterface
{
    public function validateSinglePhase(SinglePhaseDataNode $dataNode, ValidationResult $result);
}
