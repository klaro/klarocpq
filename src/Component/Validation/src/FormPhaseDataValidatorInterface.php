<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Validation;

interface FormPhaseDataValidatorInterface
{
    /**
     * @param array            $values
     * @param ValidationResult $result
     */
    public function validate(array $values, ValidationResult $result);
}
