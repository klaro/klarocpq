<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Validation;

class ValidationResult implements ValidationResultInterface
{
    /** @var array */
    protected $values;

    /** @var ValidationIssue[] */
    protected $validationIssues;

    /** @var array */
    protected $messages;

    /**
     * ValidationResult constructor.
     */
    public function __construct()
    {
        $this->values = [];
        $this->validationIssues = [];
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return count($this->validationIssues) <= 0;
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * @param array $values
     */
    public function setValues($values)
    {
        $this->values = $values;
    }

    /**
     * @param $values
     */
    public function addValues($values)
    {
        $this->values = $values + $this->values;
    }

    /**
     * @param $key
     * @param $value
     */
    public function addValue($key, $value)
    {
        $this->values[$key] = $value;
    }

    /**
     * @return ValidationIssue[]
     */
    public function getValidationIssues()
    {
        return $this->validationIssues;
    }

    /**
     * @param array $validationIssues
     */
    public function setValidationIssues($validationIssues)
    {
        $this->validationIssues = $validationIssues;
    }

    /**
     * @param ValidationIssue $issue
     */
    public function addValidationIssue(ValidationIssue $issue)
    {
        $this->validationIssues[] = $issue;
    }

    /**
     * @param ValidationResult $result
     */
    public function mergeValidationIssues(ValidationResultInterface $result)
    {
        $this->validationIssues = array_merge($this->validationIssues, $result->getValidationIssues());
    }

    /**
     * @return array|null
     */
    public function getMessages()
    {
        if ($this->messages === null) {
            $this->messages = [];
            foreach ($this->validationIssues as $issue) {
                $this->messages[$issue->getKey()][$issue->getReason()] = $issue->format();
            }
        }

        return $this->messages;
    }
}
