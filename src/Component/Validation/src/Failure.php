<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Validation;

class Failure extends ValidationIssue
{
    /**
     * Failure constructor.
     * @param string $key
     * @param string $reason
     * @param string $message
     * @param array  $parameters
     */
    public function __construct($key, $reason, $message, $parameters = [])
    {
        parent::__construct(self::ERROR, $key, $reason, $message, $parameters);
    }
}
