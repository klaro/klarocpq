<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Validation;

use Klaro\Component\Util\ArrayUtil;

class ArraySchemaFormPhaseValidator implements FormPhaseDataValidatorInterface
{
    /** @var array */
    protected $schema;

    /**
     * ArraySchemaFormPhaseValidator constructor.
     * @param array $schema
     */
    public function __construct(array $schema)
    {
        $this->schema = ArrayUtil::arrayToObject($schema);
    }

    /**
     * {@inheritdoc}
     */
    public function validate(array $values, ValidationResult $result)
    {
        $validator = new \League\JsonGuard\Validator(ArrayUtil::arrayToObject($values), $this->schema);

        if ($validator->passes()) {
            $result->addValues($values);
        } else {
            foreach ($validator->errors() as $error) {
                $result->addValidationIssue(new Failure(
                    $error->getDataPath(),
                    $error->getSchemaPath(),
                    $error->getMessage(),
                    $error->getCause()
                ));
            }
        }
    }
}
