<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Validation;

use Klaro\Component\FormData\ProductPhaseDataNode;

interface ProductPhaseValidatorInterface extends BaseValidatorInterface
{
    /**
     * Validate the data for a single phase.
     *
     * @param ProductPhaseDataNode $productNode
     * @param ValidationResult     $result
     *
     * @return mixed
     */
    public function validateProductPhase(ProductPhaseDataNode $productNode, ValidationResult $result);
}
