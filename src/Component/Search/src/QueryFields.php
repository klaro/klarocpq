<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Search;

/**
 * Class QueryFields
 *
 * @package Klaro\Component\Search
 */
class QueryFields
{
    public const MAX_RESULTS = 'MaxResults';
    public const FIRST_RESULT = 'FirstResult';
}
