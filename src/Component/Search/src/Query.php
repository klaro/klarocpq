<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Search;

/**
 * Base class for querying content.
 *
 * Class Query
 *
 * @package Klaro\Component\Search
 */
class Query implements QueryInterface
{
    /** @var QueryCriteria[] */
    protected $criteria;

    /** @var QueryParam[] */
    protected $params;

    /** @var SortParam[] */
    protected $sortParams;

    /** @var array */
    protected $resultFields;

    /**
     *
     */
    protected function __construct()
    {
        $this->criteria = [];
        $this->rawCriteria  = [];
        $this->params = [];
        $this->sortParams = [];
        $this->resultFields = [];
    }

    /**
     * @return Query
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public static function create()
    {
        return new static;
    }

    /**
     * {@inheritDoc}
     */
    public function setFirstResult(int $offset): Query
    {
        $this->setParam('from', $offset);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function setMaxResults(int $size): Query
    {
        $this->setParam('size', $size);

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getMaxResults(): ?QueryParam
    {
        $size = $this->getParam('size');
        $offset = $this->getFirstResult();

        if ($offset instanceof QueryParam && !($size instanceof QueryParam)) {
            return $offset;
        }

        return $size;
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstResult(): ?QueryParam
    {
        return $this->getParam('from');
    }

    /**
     * {@inheritDoc}
     */
    public function hasMaxResults(): bool
    {
        return $this->hasParam('size') || $this->hasFirstResult();
    }

    /**
     * {@inheritDoc}
     */
    public function hasFirstResult(): bool
    {
        return $this->hasParam('from');
    }

    /**
     * {@inheritDoc}
     */
    public function addSortParam(string $field, string $direction = SortParam::ASCENDING): Query
    {
        $this->sortParams[] = SortParam::create($field, $direction);

        return $this;
    }

    /**
     * @return SortParam[]
     */
    public function getSortParams(): array
    {
        return $this->sortParams;
    }

    /**
     * @param string $name
     * @param mixed  $value
     *
     * @return Query
     */
    protected function setParam(string $name, $value): Query
    {
        $this->params[$name] = QueryParam::create($value);

        return $this;
    }

    /**
     * @param string $name
     *
     * @return null|QueryParam
     */
    protected function getParam(string $name): ?QueryParam
    {
        return $this->params[$name] ?? null;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    protected function hasParam(string $name): bool
    {
        return $this->getParam($name) instanceof QueryParam;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    protected function hasCriteria(string $name): bool
    {
        return $this->getCriteria($name) instanceof QueryCriteria;
    }

    /**
     * @param string $name
     *
     * @return null|QueryCriteria
     */
    protected function getCriteria(string $name): ?QueryCriteria
    {
        return $this->criteria[$name] ?? null;
    }

    /**
     * @param string $name
     * @param mixed  $value
     * @param string $comparison
     *
     * @return Query
     */
    public function setCriteria(string $name, $value, string $comparison = QueryCriteria::EQUALS): Query
    {
        $this->criteria[$name] = QueryCriteria::create($value, $comparison);

        return $this;
    }

    /**
     * @param string $name
     *
     * @return array
     */
    protected function getResultFields(string $name): array
    {
        return $this->resultFields[$name] ?? [];
    }

    /**
     * @param string $name
     * @param array  $fields
     *
     * @return Query
     */
    protected function setResultFields(string $name, array $fields = []): Query
    {
        $this->resultFields[$name] = $fields;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function addRawCriteria($val)
    {
        $this->rawCriteria[] = $val;
    }

    /**
     * @inheritDoc
     */
    public function getRawCriteria()
    {
        return $this->rawCriteria;
    }
}
