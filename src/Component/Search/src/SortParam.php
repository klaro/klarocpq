<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Search;

/**
 * Sorting parameter.
 *
 * Class SortParam
 *
 * @package Klaro\Component\Search
 */
class SortParam extends QueryParam
{
    public const ASCENDING = 'ASC';
    public const DESCENDING = 'DESC';

    /** @var string */
    protected $sortDirection;

    /**
     * @param mixed  $value
     * @param string $sortDirection
     */
    protected function __construct($value, string $sortDirection = self::ASCENDING)
    {
        parent::__construct($value);

        $this->sortDirection = $sortDirection;
    }

    /**
     * @param mixed  $value
     * @param string $sortDirection
     *
     * @return SortParam
     */
    public static function create($value, string $sortDirection = self::ASCENDING): SortParam
    {
        return new self($value, $sortDirection);
    }

    /**
     * @return string
     */
    public function getSortDirection(): string
    {
        return $this->sortDirection;
    }

    /**
     * @param string $sortDirection
     *
     * @return SortParam
     */
    public function setSortDirection(string $sortDirection): SortParam
    {
        $this->sortDirection = $sortDirection;

        return $this;
    }
}
