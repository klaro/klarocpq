<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Search;

/**
 * Class QueryCriteria
 *
 * @package Klaro\Component\Search
 */
class QueryCriteria extends QueryParam
{
    public const EQUALS = 'EQ';
    public const NOT_EQUALS = 'NEQ';
    public const IN = 'IN';
    public const NOT_IN = 'NOT_IN';
    public const BETWEEN = 'BETWEEN';
    public const LIKE = 'LIKE';
    public const GREATER_THAN = 'GREATER_THAN';
    public const GREATER_EQUAL = 'GREATER_EQUAL';
    public const LESS_THAN = 'LESS_THAN';
    public const LESS_EQUAL = 'LESS_EQUAL';
    const JSON_EXTRACT  = 'JSON_EXTRACT';
    const JSON_CONTAINS = 'JSON_CONTAINS';

    /** @var string */
    protected $comparison;

    /**
     * @param mixed  $value
     * @param string $comparison
     */
    protected function __construct($value, string $comparison)
    {
        parent::__construct($value);

        $this->comparison = $comparison;
    }

    /**
     * @param mixed  $value
     * @param string $comparison
     *
     * @return QueryCriteria
     */
    public static function create($value, string $comparison = self::EQUALS): QueryCriteria
    {
        return new self($value, $comparison);
    }

    /**
     * @return string
     */
    public function getComparison(): string
    {
        return $this->comparison;
    }

    /**
     * @param string $comparison
     */
    public function setComparison(string $comparison): void
    {
        $this->comparison = $comparison;
    }
}
