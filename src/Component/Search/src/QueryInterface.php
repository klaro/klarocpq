<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Search;

/**
 * Base interface for content queries.
 *
 * Interface QueryInterface
 *
 * @package Klaro\Component\Search
 */
interface QueryInterface
{
    /**
     * Set first result offset.
     *
     * @param int $offset
     *
     * @return QueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setFirstResult(int $offset);

    /**
     * Get first result offset.
     *
     * @return null|QueryParam
     */
    public function getFirstResult(): ?QueryParam;

    /**
     * @return bool
     */
    public function hasFirstResult(): bool;

    /**
     * Set the number of maximum results returned.
     *
     * @param int $size
     *
     * @return QueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function setMaxResults(int $size);

    /**
     * Get the number of maximum results returned.
     *
     * @return null|QueryParam
     */
    public function getMaxResults(): ?QueryParam;

    /**
     * @return bool
     */
    public function hasMaxResults(): bool;

    /**
     * Add a sorting parameter.
     *
     * @param string $field
     * @param string $sortDirection
     *
     * @return QueryInterface
     *
     * @todo: Once our PHP requirement is 7.4 add covariant type hint for this function
     */
    public function addSortParam(string $field, string $sortDirection = SortParam::ASCENDING);

    /**
     * Get all sorting parameters.
     *
     * @return SortParam[]
     */
    public function getSortParams(): array;

    /**
     * @return void
     */
    public function addRawCriteria($val);

    /**
     * @return array
     */
    public function getRawCriteria();
}
