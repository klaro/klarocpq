<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator;

use Klaro\Component\Configurator\Definition\ConfiguratorConfig;

class ConfiguratorManager implements ConfiguratorManagerInterface
{
    /** @var ConfiguratorManagerInterface */
    protected $concreteManager;

    /**
     * ConfiguratorManager constructor.
     * @param ConfiguratorManagerInterface $concreteManager
     */
    public function __construct(ConfiguratorManagerInterface $concreteManager)
    {
        $this->concreteManager = $concreteManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllConfiguratorRefs()
    {
        return $this->concreteManager->getAllConfiguratorRefs();
    }

    /**
     * {@inheritdoc}
     */
    public function getConfiguratorConfig($ref, $version = self::VERSION_LATEST, $fetchMode = self::FETCH_CACHED)
    {
        return $this->concreteManager->getConfiguratorConfig($ref, $version, $fetchMode);
    }

    /**
     * {@inheritdoc}
     */
    public function getConfiguratorStructure(ConfiguratorConfig $configuratorConfig)
    {
        return $this->concreteManager->getConfiguratorStructure($configuratorConfig);
    }
}
