<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator;

/**
 * Conf items represent concrete products that have been added the offering summary.
 * It has the same data as a product item but it differs from product items in that it can have quantity,
 * notices and sales tax applied to it.
 *
 * Interface ConfItemInterface
 * @package Klaro\QuotationBundle\Api
 */
interface ConfItemInterface extends ProductItemInterface
{

    /**
     * Return item quantity.
     *
     * @return int
     */
    public function getQuantity();

    /**
     * Set item quantity.
     *
     * @param int $quantity
     */
    public function setQuantity($quantity);

    /**
     * Get sales tax percentage value.
     *
     * @return float
     */
    public function getSalesTaxPercentage();

    /**
     * Set sales tax percentage value, this is added to the final sales price.
     *
     * @param float $percentage
     */
    public function setSalesTaxPercentage($percentage);

    /**
     * Get sales tax portion of the price.
     *
     * @return float
     */
    public function getSalesTax();

    /**
     * Get total sales price with tax portion included.
     *
     * @return float
     */
    public function getSalesPriceWithTax();

    /**
     * Get notice text.
     *
     * @return string
     */
    public function getNotice();

    /**
     * Set notice text that is displayed along the configuration item.
     *
     * @param string $notice
     */
    public function setNotice($notice);

    /**
     * Returns "default" if item is in main scope.
     *
     * @return string
     */
    public function getScope(): string;

    /**
     * Marks item as optional/required.
     *
     * @param string $scope
     */
    public function setScope(string $scope): void;
}
