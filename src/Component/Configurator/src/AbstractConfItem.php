<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator;

use Klaro\Component\Util\ArrayContainer;

class AbstractConfItem extends ArrayContainer implements ConfItemInterface
{
    /**
     * {@inheritDoc}
     */
    public function getQuantity()
    {
        return $this->get('Quantity');
    }

    /**
     * {@inheritDoc}
     */
    public function setQuantity($quantity)
    {
        return $this->set('Quantity', $quantity);
    }

    /**
     * {@inheritDoc}
     */
    public function getItemCode()
    {
        return $this->get('ItemCode');
    }

    /**
     * {@inheritDoc}
     */
    public function setItemCode($code)
    {
        return $this->set('ItemCode', $code);
    }

    /**
     * {@inheritDoc}
     */
    public function getInternalTitle()
    {
        return $this->get('InternalTitle');
    }

    /**
     * {@inheritDoc}
     */
    public function setInternalTitle($title)
    {
        return $this->set('InternalTitle', $title);
    }

    /**
     * {@inheritDoc}
     */
    public function getExternalTitle()
    {
        return $this->get('ExternalTitle');
    }

    /**
     * {@inheritDoc}
     */
    public function setExternalTitle($title)
    {
        return $this->set('ExternalTitle', $title);
    }

    /**
     * {@inheritDoc}
     */
    public function getCostPrice()
    {
        return $this->get('CostPrice');
    }

    /**
     * {@inheritDoc}
     */
    public function setCostPrice($price)
    {
        return $this->set('CostPrice', $price);
    }

    /**
     * {@inheritDoc}
     */
    public function getSalesPrice()
    {
        return $this->get('SalesPrice');
    }

    /**
     * {@inheritDoc}
     */
    public function getSalesPriceWithTax()
    {
        return $this->get('SalesPrice') + $this->getSalesTax();
    }

    /**
     * {@inheritDoc}
     */
    public function setSalesPrice($salesPrice)
    {
        return $this->set('SalesPrice', $salesPrice);
    }

    /**
     * {@inheritDoc}
     */
    public function getSalesTaxPercentage()
    {
        // Look for VAT if SalesTax is null, backwards compatibility
        return $this->get('SalesTax') ?: $this->get('VAT');
    }

    /**
     * {@inheritDoc}
     */
    public function setSalesTaxPercentage($percentage)
    {
        return $this->set('SalesTax', $percentage);
    }

    /**
     * {@inheritDoc}
     */
    public function getSalesTax()
    {
        return ($this->getSalesTaxPercentage() / 100) * $this->get('SalesPrice');
    }

    /**
     * {@inheritDoc}
     */
    public function getNotice()
    {
        return $this->get('Notice');
    }

    /**
     * {@inheritDoc}
     */
    public function setNotice($notice)
    {
        return $this->set('Notice', $notice);
    }

    /**
     * @inheritDoc
     */
    public function getScope(): string
    {
        return $this->get('Scope') ?? 'default';
    }

    /**
     * @inheritDoc
     */
    public function setScope(string $scope): void
    {
        $this->set('Scope', $scope);
    }
}
