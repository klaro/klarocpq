<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator;

use UnexpectedValueException;

class ArrayConfigLoader implements ConfigLoaderInterface
{
    /** @var array */
    protected $products;

    /** @var string */
    protected $version;

    /**
     * ArrayConfigLoader constructor.
     * @param array  $products
     * @param string $version
     */
    public function __construct(array $products, $version)
    {
        $this->products = $products;
        $this->version = $version;
    }

    /**
     * {@inheritdoc}
     */
    public function getAllConfigRefs()
    {
        $names = [];

        foreach ($this->products as $ref => $productData) {
            $names[$ref] = $productData['title'];
        }

        return $names;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig($ref)
    {
        if (!array_key_exists($ref, $this->products)) {
            throw new UnexpectedValueException(
                sprintf('Config for "%s" not found!', $ref)
            );
        }

        return $this->products[$ref];
    }

    /**
     * {@inheritdoc}
     */
    public function getLatestVersion()
    {
        return $this->version;
    }
}
