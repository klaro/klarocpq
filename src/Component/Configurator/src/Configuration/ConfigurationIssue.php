<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Configuration;

/**
 * Represents a one issue(error) in ConfigurationNode
 *
 * Class ConfigurationIssue
 * @package Klaro\Component\Configurator\Configuration
 */
class ConfigurationIssue
{
    /** @var string */
    protected $title;

    /** @var string */
    protected $description;

    /** @var array */
    protected $context;

    /**
     * ConfigurationIssue constructor.
     * @param $title
     * @param $description
     * @param null        $context
     */
    public function __construct($title, $description, $context = null)
    {
        $this->title = $title;
        $this->description = $description;
        $this->context = $context;
    }

    /**
     * @param $title
     * @param $description
     * @param null        $context
     *
     * @return ConfigurationIssue
     */
    public static function create($title, $description, $context = null)
    {
        return new self($title, $description, $context);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param array $context
     */
    public function setContext($context)
    {
        $this->context = $context;
    }

    /**
     * @return bool
     */
    public function hasContext()
    {
        return isset($this->context);
    }
}
