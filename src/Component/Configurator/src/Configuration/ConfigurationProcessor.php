<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Configuration;

use Klaro\Component\Configurator\ConfigurationContextProviderInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;

class ConfigurationProcessor implements ConfigurationProcessorInterface
{
    /** @var ProductConfigurationProcessor */
    protected $productProcessor;

    /** @var ConfigurationNode */
    protected $rootProduct;

    /** @var NodeProcessorInterface */
    protected $nodeProcessor;

    /** @var ConfigurationContextProviderInterface */
    protected $contextProvider;

    /** @var bool */
    protected $continueTraversing;

    /** @var bool */
    protected $continueProcessing;

    /** @var bool */
    protected $processProducts;

    /**
     * ConfigurationProcessor constructor.
     * @param ProductConfigurationProcessor         $productProcessor
     * @param ConfigurationNode                     $rootProduct
     * @param ConfigurationContextProviderInterface $contextProvider
     * @param NodeProcessorInterface                $nodeProcessor
     * @param bool                                  $processProducts
     */
    public function __construct(
        ProductConfigurationProcessor $productProcessor,
        ConfigurationNode $rootProduct,
        ConfigurationContextProviderInterface $contextProvider = null,
        NodeProcessorInterface $nodeProcessor = null,
        $processProducts = true
    ) {
        $this->productProcessor = $productProcessor;
        $this->rootProduct = $rootProduct;
        $this->contextProvider = $contextProvider;
        $this->nodeProcessor = $nodeProcessor;
        $this->processProducts = $processProducts;
        $this->continueTraversing = true;
        $this->continueProcessing = true;
    }

    /**
     * @param ProductConfigurationProcessor         $productProcessor
     * @param ConfigurationNode                     $rootProduct
     * @param NodeProcessorInterface                $nodeProcessor
     * @param ConfigurationContextProviderInterface $contextProvider
     * @param bool                                  $processProducts
     *
     * @return ConfigurationProcessor
     */
    public static function create(
        ProductConfigurationProcessor $productProcessor,
        ConfigurationNode $rootProduct,
        ConfigurationContextProviderInterface $contextProvider = null,
        NodeProcessorInterface $nodeProcessor = null,
        $processProducts = true
    ) {
        return new self($productProcessor, $rootProduct, $contextProvider, $nodeProcessor, $processProducts);
    }

    /**
     * @param ConfigurationNode $node
     */
    public function process(ConfigurationNode $node)
    {
        if ($this->nodeProcessor instanceof NodeProcessorInterface) {
            $this->nodeProcessor->setContext($this->rootProduct, $node, $this, $this->contextProvider);
        }

        $this->processSection($node);
    }

    /**
     * {@inheritdoc}
     */
    public function processProduct(ConfigurationNode $node)
    {
        if ($this->processProducts) {
            $this->productProcessor->process($node);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function stopTraversingNodes()
    {
        $this->continueTraversing = false;
    }

    /**
     * @return bool
     */
    public function hasStoppedTraversingNodes()
    {
        return !$this->continueTraversing;
    }

    /**
     *
     */
    public function stopProcessing()
    {
        $this->continueProcessing = false;
    }

    /**
     * @return bool
     */
    public function hasStoppedProcessing()
    {
        return !$this->continueProcessing;
    }

    /**
     * @param ConfigurationNode $node
     */
    private function processSection(ConfigurationNode $node)
    {
        if ($this->nodeProcessor instanceof NodeProcessorInterface) {
            $this->nodeProcessor->process($node);
        }

        if ($this->continueTraversing && $node->hasSections()) {
            foreach ($node->getSections() as $section) {
                if ($section->getDefinition()->getType() === ConfiguratorDefinitionNode::PRODUCT) {
                    $this->processProduct($section);
                } else {
                    $this->processSection($section);
                }
            }
        }
    }
}
