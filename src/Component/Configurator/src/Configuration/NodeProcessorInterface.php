<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Configuration;

use Klaro\Component\Configurator\ConfigurationContextProviderInterface;

interface NodeProcessorInterface
{
    public function setContext(
        ConfigurationNode $rootProduct,
        ConfigurationNode $product,
        ConfigurationProcessorInterface $configurationProcessor,
        ConfigurationContextProviderInterface $contextProvider = null
    );

    public function process(ConfigurationNode $node);
}
