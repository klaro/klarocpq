<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Configuration;

use Exception;
use Klaro\Component\Configurator\ConfItem;
use Klaro\Component\Configurator\ConfItemInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;
use stdClass;

class ConfigurationNode
{
    /** @var string */
    protected $id;

    /** @var string */
    protected $title;

    /** @var array */
    protected $inputData;

    /** @var array */
    protected $itemData;

    /** @var ConfiguratorDefinitionNode */
    protected $definition;

    /** @var ConfigurationNode */
    protected $parent;

    /** @var ConfigurationNode[] */
    protected $sections;

    /** @var ConfItemInterface */
    protected $confItem;

    /** @var ConfigurationIssue[] */
    protected $errors;

    protected $values;
    protected $totalValues;
    protected $totalTax;
    protected $aggregateValues;
    protected $factors;
    protected $metadata;

    /** @var array */
    protected $scopes;

    /** @var array */
    protected $appearingRecursiveErrors;

    /**
     * ConfigurationNode constructor.
     * @param $id
     * @param null  $title
     * @param array $inputData
     * @param array $itemData
     */
    public function __construct($id, $title = null, $inputData = [], $itemData = [])
    {
        $this->id = $id;
        $this->title = $title;
        $this->inputData = $inputData;
        $this->itemData = $itemData;
        $this->sections = [];
        $this->definition = null;
        $this->parent = null;
        $this->confItem = null;
        $this->errors = [];

        $this->values = null;
        $this->totalValues = null;
        $this->totalTax = [];
        $this->aggregateValues = null;
        $this->factors  = [];
        $this->metadata = [];

        $this->scopes = [
            'default',
            'options',
        ];

        $this->appearingRecursiveErrors = [
            'Product item finder error'
        ];
    }

    /**
     * @return ConfigurationIssue[]
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param $title
     * @param $description
     * @param $context
     */
    public function addError($title, $description, $context = null)
    {
        $this->errors[] = ConfigurationIssue::create($title, $description, $context);
    }

    /**
     * @param ConfigurationIssue[] $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return int
     */
    public function getErrorCount()
    {
        $errorCount = count($this->errors);

        if ($this->hasSections()) {
            foreach ($this->getSections() as $section) {
                $errorCount += $section->getErrorCount();
            }
        }

        return $errorCount;
    }

    /**
     * Returns stored factor index for specific field and title
     *
     * @param string $field Name of the field.
     * @param string $title Name of the factor shown when debugging.
     *
     * @return int|null
     */
    public function getFactorIndex(string $field, string $title)
    {
        $factors = $this->factors[$field] ?? [];
        foreach ($factors as $index => $factor) {
            if ($title === $factor['title'] ?? null) {
                return $index;
            }
        }

        return null;
    }

    /**
     * Add a factor to this section. Factor affects the display value of the given field.
     *
     * @param string $field  Name of the field.
     * @param float  $factor Factor value that is used to multiply the field value.
     * @param string $title  Name of the factor shown when debugging
     * @param bool   $all    Set for all subsections?
     */
    public function addFactor($field, $factor, $title = '', $all = false)
    {
        $factorData = [
            'factor' => $factor,
            'title'  => $title,
            'all'    => $all,
        ];

        $factorIndex = $this->getFactorIndex($field, $title);
        if (null !== $factorIndex) {
            $this->factors[$field][$factorIndex] = $factorData;
        } else {
            $this->factors[$field][] = $factorData;
        }

        if ($all === true && $this->hasSections()) {
            foreach ($this->getSections() as $section) {
                $section->addFactor($field, $factor, $title, $all);
            }
        }
    }

    /**
     * Get factors applied to the given field.
     *
     * @param $field
     *
     * @return null
     */
    public function getFactors($field)
    {
        return isset($this->factors[$field]) ? $this->factors[$field] : [];
    }

    /**
     * Get all factors applied to this section.
     *
     * @return array
     */
    public function getAllFactors()
    {
        return $this->factors;
    }

    /**
     * Set all factors applied to this section.
     *
     * @param $factors
     */
    public function setAllFactors($factors)
    {
        $this->factors = $factors;
    }

    /**
     * Get all metadata associated with this section.
     *
     * @return array
     */
    public function getAllMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set all metadata associated with this section.
     *
     * @param array $metadata
     */
    public function setAllMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * Get metadata which has been set this section.
     *
     * @param $fieldName
     *
     * @return mixed
     *
     * @see setMetaData()
     */
    public function getMetaData($fieldName)
    {
        return isset($this->metadata[$fieldName]) ? $this->metadata[$fieldName] : null;
    }

    /**
     * Set metadata for this section. Metadata are key value pairs that are not shown to the user
     * but can be used to store or check something on the section.
     *
     * @param $fieldName
     * @param $value
     */
    public function setMetaData($fieldName, $value)
    {
        $this->metadata[$fieldName] = $value;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @param null $field
     *
     * @return array|mixed
     */
    public function getInputData($field = null)
    {
        $inputData = $this->inputData + (($this->parent instanceof ConfigurationNode) ? $this->parent->getInputData() : []);

        if (isset($field)) {
            return isset($inputData[$field]) ? $inputData[$field] : null;
        }

        return $inputData;
    }

    /**
     * @return array
     */
    public function getOriginalInputData()
    {
        return $this->inputData;
    }

    /**
     * @param array $inputData
     */
    public function setInputData($inputData)
    {
        $this->inputData = $inputData;
    }

    /**
     * @return array
     */
    public function getItemData()
    {
        return $this->itemData;
    }

    /**
     * @param array $itemData
     */
    public function setItemData($itemData)
    {
        $this->itemData = $itemData;
    }

    /**
     * @return ConfiguratorDefinitionNode
     */
    public function getDefinition()
    {
        if (!($this->definition instanceof ConfiguratorDefinitionNode)) {
            $this->definition = new ConfiguratorDefinitionNode(null, null);
        }

        return $this->definition;
    }

    /**
     * @param ConfiguratorDefinitionNode $definition
     */
    public function setDefinition($definition)
    {
        $this->definition = $definition;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        if (!$this->parent) {
            return 0;
        }

        return $this->parent->getLevel() + 1;
    }

    /**
     * @return ConfigurationNode
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param ConfigurationNode $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return bool
     */
    public function hasSections()
    {
        return count($this->sections) > 0;
    }

    /**
     * Get section based on the section identifier.
     *
     * @param string $id
     *
     * @return ConfigurationNode
     */
    public function getSection($id)
    {
        $searched = null;

        if ($this->getId() == $id) {
            $searched = $this;
        } elseif ($this->hasSections()) {
            foreach ($this->sections as $section) {
                $searched = $section->getSection($id);

                if ($searched !== null) {
                    break;
                }
            }
        }

        return $searched;
    }

    /**
     * Removes a section based on the section identifier.
     *
     * @param $id
     */
    public function removeSection($id)
    {
        if ($this->hasSections()) {
            $sectionCount = count($this->sections);

            for ($i = $sectionCount - 1; $i >= 0; --$i) {
                if ($this->sections[$i]->getId() == $id) {
                    unset($this->sections[$i]);
                }
            }

            // Reset keys.
            $this->sections = array_values($this->sections);
        }
    }

    /**
     * Find sections that match the given section identifier.
     *
     * @param $id
     *
     * @return ConfigurationNode[]
     */
    public function findSections($id)
    {
        $found = [];

        if ($this->getId() == $id) {
            $found = [$this];
        } elseif ($this->hasSections()) {
            foreach ($this->sections as $section) {
                $found = array_merge($found, $section->findSections($id));
            }
        }

        return $found;
    }

    /**
     * @return ConfigurationNode[]
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * @param ConfigurationNode[] $sections
     */
    public function setSections($sections)
    {
        $this->sections = $sections;
    }

    /**
     * @param ConfigurationNode $section
     */
    public function addSection(ConfigurationNode $section)
    {
        $section->setParent($this);

        $this->sections[] = $section;
    }

    /**
     * Create a conf item for this section.
     *
     * @return ConfItemInterface
     */
    public function createConfItem()
    {
        return $this->addItem(new ConfItem($this));
    }

    /**
     * Add a conf item to this section.
     *
     * @param ConfItemInterface $confItem
     *
     * @return ConfItemInterface
     */
    public function addItem(ConfItemInterface $confItem)
    {
        $this->confItem = $confItem;

        return $confItem;
    }

    /**
     * Test to see if any conf items have been added to this section.
     *
     * @return bool
     */
    public function hasItem()
    {
        return $this->confItem instanceof ConfItemInterface;
    }

    /**
     * Get conf items added to the section.
     *
     * @return ConfItemInterface
     */
    public function getItem()
    {
        return $this->confItem;
    }

    /**
     * Get value for the given field in this section (= conf item values).
     *
     * @param string $field
     * @param string|array $scope
     *
     * @return mixed
     */
    public function get(string $field, $scope = 'default')
    {
        $item = $this->getItem();
        if (null === $item) {
            return 0;
        }

        $value = $item->get($field);
        if (false === is_numeric($value) || false === is_numeric($item->getQuantity())) {
            return 0;
        }

        if (!$this->isInScope($item->getScope(), $scope)) {
            return 0;
        }

        return $item->getQuantity() * $value;
    }

    /**
     * Get total value for given field in this section and it's children.
     *
     * @param $field
     *
     * @return mixed
     */
    public function getTotal($field, $scope = 'default')
    {
        $this->totalValues[$field] = $this->get($field, $scope);

        if ($this->hasSections()) {
            $this->totalValues[$field] += array_reduce($this->sections, function ($carry, ConfigurationNode $section) use ($field, $scope) {
                $carry += $section->getTotal($field, $scope);

                return $carry;
            }, 0);
        }

        return $this->totalValues[$field];
    }

    /**
     * Reset cached values for given field which are used in the calculation.
     *
     * @param $field
     */
    public function reset($field)
    {
        if (isset($this->totalValues[$field])) {
            unset($this->totalValues[$field]);
        }

        if (isset($this->values[$field])) {
            unset($this->values[$field]);
        }

        if ($field == 'SalesPrice') {
            $this->totalTax = [];
        }

        if ($this->hasSections()) {
            foreach ($this->getSections() as $section) {
                $section->reset($field);
            }
        }
    }

    /**
     * Get sum of conf item values grouped by the given column. Returns an array where the values from
     * the group column are the keys and the value is the sum of the value column for that
     * item. Eg. to get a sum of quantities for each product item code:
     *
     *     $values = $section->getAggregateSum('ItemCode', 'Quantity');
     *
     * Result:
     *
     *     [
     *         'ItemCode1' => 1,
     *         'ItemCode2' => 4,
     *         ...
     *     ]
     *
     * @param string $groupColumn Column to group results by.
     * @param string $valueColumn Column name of the value to sum.
     * @param bool   $refresh     Refresh cache
     *
     * @return array
     *
     * @throws Exception
     */
    public function getAggregateSum($groupColumn, $valueColumn, $refresh = false)
    {
        $cacheKey = $groupColumn.'-'.$valueColumn;

        if (isset($this->aggregateValues[$cacheKey]) && $refresh !== true) {
            return $this->aggregateValues[$cacheKey];
        }

        $this->aggregateValues[$cacheKey] = [];

        // Helper to process the sections hierarchically.
        $processor = function (ConfigurationNode $section) use (&$processor, &$groupColumn, &$valueColumn, &$cacheKey) {
            if ($section->hasItem()) {
                $confItem = $section->getItem();
                $groupBy = $confItem->get($groupColumn);

                if (!array_key_exists($groupBy, $this->aggregateValues[$cacheKey])) {
                    $this->aggregateValues[$cacheKey][$groupBy] = 0;
                }

                $value = $confItem->get($valueColumn);

                if ($value === null || is_numeric($value)) {
                    $this->aggregateValues[$cacheKey][$groupBy] += $value;
                } else {
                    // Helper method to get the full path the error section.
                    $nameProcessor = function (ConfigurationNode $section) use (&$nameProcessor) {
                        $parent = $section->getParent();
                        $title  = $section->getTitle();

                        if ($parent instanceof ConfigurationNode && $parent->getLevel() >= 0) {
                            return $nameProcessor($parent).' -> '.$title;
                        }

                        return $title;
                    };

                    throw new Exception(
                        sprintf(
                            'Only numeric values can be aggregated in section "%s", item "%s". Expected number, got %s.',
                            $nameProcessor($section),
                            $confItem->getItemCode(),
                            gettype($value)
                        )
                    );
                }
            }

            if ($section->hasSections()) {
                /** @var ConfigurationNode $subSection */
                foreach ($section->getSections() as $subSection) {
                    $processor($subSection);
                }
            }
        };

        $processor($this);

        return $this->aggregateValues[$cacheKey];
    }

    /**
     * Get sales price sum of conf items from this sections.
     *
     * @return float
     */
    public function getSalesPrice($scope = 'default')
    {
        return $this->get('SalesPrice', $scope);
    }

    /**
     * Get cost price sum of conf items from this sections.
     *
     * @return float
     */
    public function getCostPrice($scope = 'default')
    {
        return $this->get('CostPrice', $scope);
    }

    /**
     * Get cost price sum of conf items and subsections under this section.
     *
     * @return float
     */
    public function getTotalCostPrice($scope = 'default')
    {
        return $this->getTotal('CostPrice', $scope);
    }

    /**
     * Get sales price sum of conf items and subsections under this section.
     *
     * @return float
     */
    public function getTotalSalesPrice($scope = 'default')
    {
        return $this->getTotal('SalesPrice', $scope);
    }

    /**
     * Get sales price sum with sales tax.
     *
     * @return mixed
     */
    public function getTotalSalesPriceWithTax($scope = 'default')
    {
        return $this->getTotal('SalesPrice', $scope) + $this->getTotalSalesTax($scope);
    }

    /**
     * Get total amount of sales tax for the section.
     *
     * @return number
     */
    public function getTotalSalesTax($scope = 'default')
    {
        return array_sum($this->getSalesTaxTotals($scope));
    }

    /**
     * Get an array of totals for each sales tax percentage. For example, if there items with 8% and 14% tax
     * percentages then the result will be a key-value pair:
     *
     * [
     *    8  => 1234,
     *    14 => 4566
     * ]
     *
     * Where the key is the percentage and the value is total amount of that tax percentage under the section.
     *
     * @return array
     */
    public function getSalesTaxTotals($scope = 'default')
    {
        if (!array_key_exists($scope, $this->totalTax)) {
            $totalTax = [];

            if ($this->hasItem() && $this->isInScope($this->getItem()->getScope(), $scope)) {
                $item = $this->getItem();

                $percentage = (int) $item->getSalesTaxPercentage();

                if (!empty($percentage)) {
                    if (!isset($carry[$percentage])) {
                        $totalTax[$percentage] = 0;
                    }

                    $value = $item->getSalesTax();

                    if (is_numeric($value)) {
                        $totalTax[$percentage] += $item->getQuantity() * $value;
                    }
                }
            }

            if ($this->hasSections()) {
                $totalTax = array_reduce($this->sections, function ($carry, ConfigurationNode $section) use ($scope) {
                    $totals = $section->getSalesTaxTotals($scope);

                    foreach ($totals as $percentage => $total) {
                        if (!isset($carry[$percentage])) {
                            $carry[$percentage] = 0;
                        }

                        $carry[$percentage] += $total;
                    }

                    return $carry;
                }, $totalTax);
            }

            $this->totalTax[$scope] = $totalTax;
        }

        return $this->totalTax[$scope];
    }

    /**
     * Get total profit amount.
     *
     * @return float
     */
    public function getTotalProfit($scope = 'default')
    {
        return $this->getTotalSalesPrice($scope) - $this->getTotalCostPrice($scope);
    }

    /**
     * Get the total profit margin.
     *
     * @return float
     */
    public function getTotalProfitMargin($scope = 'default')
    {
        $salesPrice = $this->getTotalSalesPrice($scope);

        return $salesPrice != 0 ? ($this->getTotalProfit($scope) / $salesPrice) * 100 : null;
    }

    /**
     * @return stdClass
     */
    public function export()
    {
        $object = new stdClass();

        $object->id = $this->getId();
        $object->title = $this->getTitle();
        $object->metadata = $this->getAllMetadata();
        $object->factors = $this->getAllFactors();
        $object->inputData = $this->getOriginalInputData();

        if ($this->hasSections()) {
            $object->sections = [];

            /**
             * @var string $sectionRef
             * @var ConfigurationNode[] $sections
             */
            foreach ($this->getSections() as $section) {
                $object->sections[] = $section->export();
            }
        }

        if ($this->hasItem()) {
            $object->item = $this->getItem()->toArray();
        }

        if (count($this->errors) > 0) {
            $object->errors = [];

            foreach ($this->errors as $error) {
                $object->errors[] = (object) [
                    'title' => $error->getTitle(),
                    'description' => $error->getDescription(),
                    'context' => $error->getContext(),
                ];
            }
        }

        return $object;
    }

    /**
     * Test to see if any conf items have been added to this section.
     *
     * @var string|array $scope
     *
     * @return bool
     */
    public function hasItemRecursive($scope = 'default'): bool
    {
        if ($this->hasItem() && $this->isInScope($this->getItem()->getScope(), $scope)) {
            return true;
        }

        foreach ($this->getSections() as $section) {
            if ($section->hasItemRecursive($scope)) {
                return true;
            }
        }

        // to not skip missing items
        // don't forget to make ->addData('Scope', 'default') to parent section
        if (null !== $this->getParent() && count($this->getParent()->getSection($this->getId())->getErrors()) &&
            ($this->getParent()->hasItem() ? $this->isInScope($this->getParent()->getItem()->getScope(), $scope) :
                $this->getParent()->getInputData('Scope') === $scope)) {
            foreach ($this->getParent()->getSection($this->getId())->getErrors() as $error) {
                if (in_array($error->getTitle(), $this->appearingRecursiveErrors)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get all items from section recursively.
     *
     * @var string|array $scope
     * @var array $result
     *
     * @return ConfItem[]
     */
    public function getItemsRecursive($scope = 'default', &$result = []): array
    {
        if (false === $this->hasItemRecursive($scope)) {
            return $result;
        }

        if ($this->hasItem() && $this->isInScope($this->getItem()->getScope(), $scope)) {
            $result[] = $this->getItem();

            return $result;
        }

        foreach ($this->getSections() as $section) {
            $section->getItemsRecursive($scope, $result);
        }

        return $result;
    }

    /**
     * Returns available summary scopes.
     *
     * @return array
     */
    public function getScopes(): array
    {
        return $this->scopes;
    }

    /**
     * Checks if conf item is in selected scope. Use '*' for wildcard.
     *
     * @var string $itemScope
     * @var string|array $matchingScope
     *
     * @return bool
     */
    public function isInScope(string $itemScope, $matchingScope): bool
    {
        if ('*' === $matchingScope) {
            return true;
        }

        if (is_array($matchingScope)) {
            return in_array($itemScope, $matchingScope);
        }

        return $matchingScope === $itemScope;
    }
}
