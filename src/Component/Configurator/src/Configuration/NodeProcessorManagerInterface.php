<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Configuration;

interface NodeProcessorManagerInterface
{
    /**
     * @param $reference
     *
     * @return NodeProcessorInterface
     */
    public function getNodeProcessor($reference);

    /**
     * @param $references
     *
     * @return NodeProcessorInterface[]
     */
    public function getNodeProcessors($references);
}
