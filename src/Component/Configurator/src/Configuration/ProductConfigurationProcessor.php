<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Configuration;

use Exception;
use Klaro\Component\Configurator\ConfigurationContextProviderInterface;
use Klaro\Component\Configurator\Definition\ConfiguratorProductDefinitionNode;

class ProductConfigurationProcessor
{
    /** @var ConfigurationNode */
    protected $rootProduct;

    /** @var NodeProcessorManagerInterface */
    protected $nodeProcessorManager;

    /** @var ConfigurationContextProviderInterface */
    protected $contextProvider;

    /**
     * ProductConfigurationProessor constructor.
     * @param ConfigurationNode                     $rootProduct
     * @param NodeProcessorManagerInterface         $nodeProcessorManager
     * @param ConfigurationContextProviderInterface $contextProvider
     */
    public function __construct(
        ConfigurationNode $rootProduct,
        NodeProcessorManagerInterface $nodeProcessorManager,
        ConfigurationContextProviderInterface $contextProvider = null
    ) {
        $this->rootProduct = $rootProduct;
        $this->nodeProcessorManager = $nodeProcessorManager;
        $this->contextProvider = $contextProvider;
    }

    /**
     * @param ConfigurationNode                     $rootProduct
     * @param NodeProcessorManagerInterface         $nodeProcessorManager
     * @param ConfigurationContextProviderInterface $contextProvider
     *
     * @return ProductConfigurationProcessor
     */
    public static function create(
        ConfigurationNode $rootProduct,
        NodeProcessorManagerInterface $nodeProcessorManager,
        ConfigurationContextProviderInterface $contextProvider = null
    ) {
        return new self($rootProduct, $nodeProcessorManager, $contextProvider);
    }

    /**
     * @param ConfigurationNode $product
     */
    public function process(ConfigurationNode $product)
    {
        /** @var ConfiguratorProductDefinitionNode $definition */
        $definition = $product->getDefinition();
        $nodeProcessors = [];

        try {
            $nodeProcessors = $this->nodeProcessorManager->getNodeProcessors($definition->getProcessors());
        } catch (Exception $e) {
            $product->addError('Error loading product processor', $e->getMessage());
        }

        if (count($nodeProcessors) > 0) {
            // This flag is needed so that the processors are not run again for products but
            // should still be run for the section hierarchy.
            $isFirstProcessor = true;

            foreach ($nodeProcessors as $nodeProcessor) {
                $configurationProcessor = ConfigurationProcessor::create(
                    $this,
                    $this->rootProduct,
                    $this->contextProvider,
                    $nodeProcessor,
                    $isFirstProcessor
                );
                $configurationProcessor->process($product);

                $isFirstProcessor = false;

                if ($configurationProcessor->hasStoppedProcessing()) {
                    break;
                }
            }
        } else {
            ConfigurationProcessor::create($this, $this->rootProduct, $this->contextProvider)->process($product);
        }
    }
}
