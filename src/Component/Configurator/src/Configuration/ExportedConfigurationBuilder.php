<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Configuration;

use Klaro\Component\Configurator\ConfItem;

class ExportedConfigurationBuilder
{
    /** @var array */
    protected $configuration;

    /**
     * ExportedConfigurationBuilder constructor.
     * @param $configuration
     */
    public function __construct($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param $configuration
     *
     * @return ExportedConfigurationBuilder
     */
    public static function create($configuration)
    {
        return new self($configuration);
    }

    /**
     * @return ConfigurationNode
     */
    public function build()
    {
        return $this->createSection($this->configuration);
    }

    /**
     * @param $sectionData
     *
     * @return ConfigurationNode
     */
    private function createSection($sectionData)
    {
        $id = $sectionData->id;
        $title = $sectionData->title;

        $node = new ConfigurationNode($id, $title);

        if (isset($sectionData->sections) && count($sectionData->sections) > 0) {
            foreach ($sectionData->sections as $section) {
                $node->addSection($this->createSection($section));
            }
        }

        if (isset($sectionData->inputData)) {
            $node->setInputData($sectionData->inputData);
        }

        if (isset($sectionData->factors)) {
            $node->setAllFactors($sectionData->factors);
        }

        if (isset($sectionData->metadata)) {
            $node->setAllMetadata($sectionData->metadata);
        }

        if (isset($sectionData->item)) {
            $confItem = new ConfItem($node, $sectionData->item);

            $node->addItem($confItem);
        }

        if (isset($sectionData->errors) && count($sectionData->errors) > 0) {
            foreach ($sectionData->errors as $error) {
                $node->addError($error->title, $error->description, $error->context);
            }
        }

        return $node;
    }
}
