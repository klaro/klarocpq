<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Configuration;

use Exception;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;
use Klaro\Component\Configurator\Definition\ConfiguratorProductListDefinitionNode;
use Klaro\Component\Common\Exception\ExceptionFactory;
use stdClass;

class ConfigurationBuilder
{
    /** @var ConfiguratorDefinitionNode */
    protected $offeringSummaryDefinition;

    /**
     * OfferingSummaryTreeFactory constructor.
     * @param ConfiguratorDefinitionNode $offeringSummaryDefinition
     */
    public function __construct(ConfiguratorDefinitionNode $offeringSummaryDefinition)
    {
        $this->offeringSummaryDefinition = $offeringSummaryDefinition;
    }

    /**
     * @param $data
     *
     * @return ConfigurationNode
     *
     * @throws Exception
     */
    public function build($data = []): ConfigurationNode
    {
        return $this->createSection($this->offeringSummaryDefinition, $data);
    }

    /**
     * @param ConfiguratorDefinitionNode $definition
     * @param null                       $data
     * @param stdClass                   $previousSections
     *
     * @return ConfigurationNode
     *
     * @throws Exception
     */
    private function createSection(
        ConfiguratorDefinitionNode $definition,
        $data = null,
        $previousSections = null
    ): ConfigurationNode {
        $data = $data ?? new stdClass();
        $previousSections = $previousSections ?? new stdClass();

        $title = $data->title ?? $definition->getTitle();
        $inputData = isset($data->data) ? (array) $data->data : [];
        $itemData = isset($data->item) ? (array) $data->item : [];

        $newNode = new ConfigurationNode($definition->getId(), $title, $inputData, $itemData);
        $newNode->setDefinition($definition);

        foreach ($definition->getSections() as $ref => $section) {
            $sectionData = $section->getDefault();

            // Section data can be overridden from parent.
            if (isset($data->sections) && property_exists($data->sections, $ref)) {
                $sectionData = $data->sections->{$ref};
            } elseif (property_exists($previousSections, $ref)) {
                $sectionData = $previousSections->{$ref};
            }

            // Merge current section data with previous.
            $merged = (object) array_merge(
                (array) $previousSections,
                isset($data->sections) ? (array) $data->sections : []
            );

            foreach ($this->createSections($section, $sectionData, $merged) as $node) {
                $node->setId($ref);
                $newNode->addSection($node);
            }
        }

        // Additional sections are not part of the structure and are added to the end.
        $additionalSections = isset($data->additional) ? (array) $data->additional : [];
        foreach ($additionalSections as $sectionId => $sectionData) {
            $definition = new ConfiguratorDefinitionNode($sectionId, null);
            $definition->setItemRules([
                'create' => true,
            ]);

            foreach ($this->createSections($definition, $sectionData) as $node) {
                $newNode->addSection($node);
            }
        }

        return $newNode;
    }

    /**
     * @param ConfiguratorDefinitionNode $definition
     * @param $data
     * @param null                       $previousSections
     *
     * @return array|ConfigurationNode
     *
     * @throws Exception
     */
    private function createSections(ConfiguratorDefinitionNode $definition, $data, $previousSections = null)
    {
        switch ($definition->getType()) {
            case ConfiguratorDefinitionNode::PRODUCT_LIST:
                /** @var ConfiguratorProductListDefinitionNode $definition */

                return $this->createProductListSections($definition, $data);
            case ConfiguratorDefinitionNode::PRODUCT:
            case ConfiguratorDefinitionNode::SECTION:
                /** @var ConfiguratorDefinitionNode $definition */

                return $this->createNormalSections($definition, $data, $previousSections);
            default:
                break;
        }

        throw ExceptionFactory::notSupported();
    }

    /**
     * @param ConfiguratorProductListDefinitionNode $definition
     * @param $data
     *
     * @return array
     *
     * @throws Exception
     */
    private function createProductListSections(ConfiguratorProductListDefinitionNode $definition, $data): array
    {
        $title = $data->title ?? $definition->getTitle();
        $productList = $data->products ?? [];

        $productListNode = new ConfigurationNode($definition->getId(), $title);
        $productListNode->setDefinition($definition);

        if (is_array($productList)) {
            foreach ($productList as $productData) {
                $productRef = $productData->ref;

                $productListNode->addSection(
                    self::create($definition->getProduct($productRef))->build($productData)
                );
            }
        }

        return [$productListNode];
    }

    /**
     * @param ConfiguratorDefinitionNode $offeringSummaryDefinition
     *
     * @return ConfigurationBuilder
     */
    public static function create(ConfiguratorDefinitionNode $offeringSummaryDefinition): ConfigurationBuilder
    {
        return new self($offeringSummaryDefinition);
    }

    /**
     * @param ConfiguratorDefinitionNode $definition
     * @param $data
     * @param $previousSections
     *
     * @return array|ConfigurationNode
     *
     * @throws Exception
     */
    private function createNormalSections(ConfiguratorDefinitionNode $definition, $data, $previousSections = null)
    {
        $sectionDatas = [];

        if (is_bool($data)) {
            if ($data === true) {
                $sectionDatas[] = null;
            }
        } elseif (is_numeric($data)) {
            $repeat = max((int) $data, 0);

            $sectionDatas = array_fill(0, $repeat, null);
        } elseif (is_object($data)) {
            $sectionDatas[] = $data;
        } elseif (is_array($data)) {
            $sectionDatas = $data;
        }

        if (count($sectionDatas) > 0) {
            foreach ($sectionDatas as $sectionData) {
                if ($definition->getType() === ConfiguratorDefinitionNode::PRODUCT) {
                    $sections[] = self::create($definition)->build($sectionData);
                } else {
                    $sections[] = $this->createSection($definition, $sectionData, $previousSections);
                }
            }
        }

        return $sections ?? [];
    }
}
