<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator;

use Klaro\Component\Configurator\Definition\ConfiguratorConfig;

interface ConfiguratorManagerInterface
{
    const VERSION_LATEST = -1;

    const FETCH_CACHED = 'cached';
    const FETCH_FROM_REVISION = 'revision';
    const FETCH_FROM_SOURCE = 'source';

    public function getAllConfiguratorRefs();

    public function getConfiguratorConfig($ref, $version = self::VERSION_LATEST, $fetchMode = self::FETCH_CACHED);

    public function getConfiguratorStructure(ConfiguratorConfig $configuratorConfig);
}
