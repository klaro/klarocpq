<?php

namespace Klaro\Component\Configurator\Definition;

class ConfiguratorItemsLoaderDefinition
{
    public const VIRTUAL_ITEM_LOADER = 'virtual_item';
    public const FILTERED_ITEM_LOADER = 'filtered_item';
}
