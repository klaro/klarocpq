<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Definition;

use Klaro\Component\Util\ArrayUtil;

/**
 * Common config object, used as a bag for all configs in multiple places.
 *
 * Class ConfiguratorConfig
 * @package Klaro\Component\Configurator\Definition
 */
class ConfiguratorConfig
{
    /** @var string|null */
    protected $collectionType;

    /** @var string|null */
    protected $condition;

    /** @var array|null */
    protected $defaults = [];

    /** @var string */
    protected $id;

    /** @var array */
    protected $inputs = [];

    /** @var array|null */
    protected $items = [];

    /** @var array */
    protected $loadedSections = [];

    /** @var array */
    protected $processors = [];

    /** @var array */
    protected $refs = [];

    /** @var array */
    protected $requiredInputs = [];

    /** @var array */
    protected $sections = [];

    /** @var string|null */
    protected $source;

    /** @var string */
    protected $title;

    /** @var string */
    protected $version;

    /**
     * ConfiguratorDefinition constructor.
     * @param string $id
     * @param string $title
     * @param string $version
     */
    public function __construct($id, $title, $version = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->version = $version;
    }

    /**
     * @return array|null
     */
    public function getCollectionType(): ?string
    {
        return $this->collectionType;
    }

    /**
     * @param string|null $type
     */
    public function setCollectionType(?string $type): void
    {
        $this->collectionType = $type;
    }

    /**
     * @return string|null
     */
    public function getCondition(): ?string
    {
        return $this->condition;
    }

    /**
     * @param string|null $condition
     */
    public function setCondition(?string $condition): void
    {
        $this->condition = $condition;
    }

    /**
     * @return array|null
     */
    public function getDefaults(): ?array
    {
        return $this->defaults;
    }

    /**
     * @param array|null $defaults
     */
    public function setDefaults(?array $defaults): void
    {
        $this->defaults = $defaults;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getInputs(): array
    {
        return $this->inputs;
    }

    /**
     * @param array $inputs
     */
    public function setInputs(array $inputs): void
    {
        $this->inputs = $inputs;
    }

    /**
     * @return array|null
     */
    public function getItems(): ?array
    {
        return $this->items;
    }

    /**
     * @param array|null $items
     */
    public function setItems(?array $items): void
    {
        $this->items = $items;
    }

    /**
     * @return array
     */
    public function getProcessors(): array
    {
        return $this->processors;
    }

    /**
     * @param array $processors
     */
    public function setProcessors(array $processors): void
    {
        $this->processors = $processors;
    }

    /**
     * @return array
     */
    public function getRefs(): array
    {
        return $this->refs;
    }

    /**
     * @param array $refs
     */
    public function setRefs(array $refs): void
    {
        $this->refs = $refs;
    }

    /**
     * @return array
     */
    public function getRequiredInputs(): array
    {
        return $this->requiredInputs;
    }

    /**
     * @param array $requiredInputs
     */
    public function setRequiredInputs($requiredInputs): void
    {
        $this->requiredInputs = $requiredInputs;
    }

    /**
     * @return array
     */
    public function getSections(): array
    {
        return $this->sections;
    }

    /**
     * @param array $sections
     */
    public function setSections(array $sections): void
    {
        $this->sections = $sections;
    }

    /**
     * @param string $reference
     *
     * @return array
     */
    public function getSection(string $reference): array
    {
        if (!isset($this->loadedSections[$reference])) {
            $this->loadedSections[$reference] = $this->loadSection($reference);
        }

        return $this->loadedSections[$reference];
    }

    /**
     * @param string $reference
     *
     * @return mixed
     */
    private function loadSection(string $reference)
    {
        $data = $this->sections[$reference];
        if (isset($data['items']) && is_array($data['items'])) {
            $data['items'] = $this->mergeWithItemDefaults($data['items']);
        }

        return $data;
    }

    /**
     * @param $items
     *
     * @return array|mixed
     */
    public function mergeWithItemDefaults($items)
    {
        if (!isset($this->defaults['items'])) {
            $this->defaults['items'] = [];
        }

        if (!is_array($items)) {
            return $this->defaults['items'];
        }

        return ArrayUtil::arrayMergeRecursive($this->defaults['items'], $items);
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string|null $source
     */
    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @param string $version
     */
    public function setVersion($version): void
    {
        $this->version = $version;
    }
}
