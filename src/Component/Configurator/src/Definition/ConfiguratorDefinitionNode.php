<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Definition;

use stdClass;

class ConfiguratorDefinitionNode
{
    public const SECTION = 'section';
    public const PRODUCT_LIST = 'product_list';
    public const PRODUCT = 'product';

    /** @var string|null */
    protected $condition;

    /** @var bool */
    protected $default;

    /** @var string */
    protected $id;

    /** @var array */
    protected $inputs = [];

    /** @var bool  */
    protected $items = true;

    /** @var array|null */
    protected $itemRules;

    /** @var object */
    protected $requiredInputs;

    /** @var ConfiguratorDefinitionNode[] */
    protected $sections = [];

    /** @var string|null */
    protected $source;

    /** @var string|null */
    protected $title;

    /**
     * @param string $id
     * @param string $title
     * @param bool   $default
     */
    public function __construct(string $id, ?string $title, bool $default = true)
    {
        $this->default = $default;
        $this->id = $id;
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getCondition(): ?string
    {
        return $this->condition;
    }

    /**
     * @param string|null $condition
     */
    public function setCondition(?string $condition): void
    {
        $this->condition = $condition;
    }

    /**
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * @param bool $default
     */
    public function setDefault(bool $default): void
    {
        $this->default = $default;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return array
     */
    public function getInputs(): array
    {
        return $this->inputs;
    }

    /**
     * @param array $inputs
     */
    public function setInputs(array $inputs): void
    {
        $this->inputs = $inputs;
    }

    /**
     * @return bool
     */
    public function canHaveItems(): bool
    {
        return $this->items;
    }

    /**
     * @param bool $items
     */
    public function setItems(bool $items): void
    {
        $this->items = $items;
    }

    /**
     * @return null|array
     */
    public function getItemRules(): ?array
    {
        return $this->itemRules;
    }

    /**
     * @param null|array $itemRules
     */
    public function setItemRules(?array $itemRules): void
    {
        $this->itemRules = $itemRules;
    }

    /**
     * @return object
     */
    public function getRequiredInputs()
    {
        return is_object($this->requiredInputs) ? $this->requiredInputs : new stdClass();
    }

    /**
     * @param object $requiredInputs
     */
    public function setRequiredInputs($requiredInputs): void
    {
        $this->requiredInputs = $requiredInputs;
    }

    /**
     * @return ConfiguratorDefinitionNode[]
     */
    public function getSections(): array
    {
        return $this->sections;
    }

    /**
     * @param ConfiguratorDefinitionNode[] $sections
     */
    public function setSections(array $sections): void
    {
        $this->sections = $sections;
    }

    /**
     * @param string $ref
     *
     * @return null|ConfiguratorDefinitionNode
     */
    public function getSection(string $ref): ?ConfiguratorDefinitionNode
    {
        return $this->sections[$ref];
    }

    /**
     * @param $sectionRef
     * @param ConfiguratorDefinitionNode $section
     */
    public function addSection($sectionRef, ConfiguratorDefinitionNode $section): void
    {
        $this->sections[$sectionRef] = $section;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string|null $source
     */
    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return null|string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param null|string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::SECTION;
    }
}
