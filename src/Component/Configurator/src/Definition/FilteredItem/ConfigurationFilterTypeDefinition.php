<?php

namespace Klaro\Component\Configurator\Definition\FilteredItem;

class ConfigurationFilterTypeDefinition
{
    public const ITEM_FILTER_SINGLE_TYPE = 'single';
    public const ITEM_FILTER_MULTI_TYPE = 'multi';
}
