<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Definition;

class ConfiguratorProductListDefinitionNode extends ConfiguratorDefinitionNode
{
    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->getSections();
    }

    /**
     * @param array $products
     */
    public function setProducts(array $products): void
    {
        $this->setSections($products);
    }

    /**
     * @param string $reference
     *
     * @return ConfiguratorDefinitionNode|null
     */
    public function getProduct(string $reference): ?ConfiguratorDefinitionNode
    {
        return $this->getSection($reference);
    }

    /**
     * @param string                     $reference
     * @param ConfiguratorDefinitionNode $product
     */
    public function addProduct(string $reference, ConfiguratorDefinitionNode $product): void
    {
        $this->addSection($reference, $product);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return ConfiguratorDefinitionNode::PRODUCT_LIST;
    }
}
