<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Definition;

use Exception;
use Klaro\Component\Common\Exception\ExceptionFactory;
use Klaro\Component\Configurator\ConfigLoaderInterface;
use Klaro\Component\Util\ArrayUtil;

class ConfiguratorDefinitionFactory
{
    /**
     * @var ConfigLoaderInterface
     */
    protected $productLoader;

    /**
     * @param ConfigLoaderInterface $productLoader
     */
    public function __construct(ConfigLoaderInterface $productLoader)
    {
        $this->productLoader = $productLoader;
    }

    /**
     * @param ConfigLoaderInterface $productLoader
     *
     * @return ConfiguratorDefinitionFactory
     */
    public static function create(ConfigLoaderInterface $productLoader): ConfiguratorDefinitionFactory
    {
        return new self($productLoader);
    }

    /**
     * @param ConfiguratorConfig $definition
     * @param array              $data
     *
     * @return ConfiguratorProductDefinitionNode
     *
     * @throws Exception
     */
    public function createStructure(ConfiguratorConfig $definition, $data = []): ConfiguratorProductDefinitionNode
    {
        $collectionType = $data['collection_type'] ?? $definition->getCollectionType();
        $condition = $data['condition'] ?? $definition->getCondition();
        $default = $data['default'] ?? true;
        $inputs = $data['inputs']['values'] ?? $definition->getInputs();
        $items = $data['items'] ?? $definition->getItems();
        $requiredInputs = $data['inputs']['required'] ?? $definition->getRequiredInputs();
        $source = $data['source'] ?? $definition->getSource();
        $title = $data['title'] ?? $definition->getTitle();

        $node = new ConfiguratorProductDefinitionNode($definition->getId(), $title, $default);

        $node->setConfiguratorConfig($definition);
        $node->setCollectionType($collectionType);
        $node->setCondition($condition);
        $node->setInputs($inputs);
        $node->setItems($items['enabled'] ?? true);
        $node->setItemRules($definition->mergeWithItemDefaults($definition->getItems()));
        $node->setRequiredInputs(ArrayUtil::arrayToObject($requiredInputs));
        $node->setSource($source);

        $this->addStructureReferences($definition, $node, $definition->getRefs());

        return $node;
    }

    /**
     * @param ConfiguratorConfig                $definition
     * @param ConfiguratorProductDefinitionNode $node
     * @param array                             $references
     *
     * @throws Exception
     */
    private function addStructureReferences(
        ConfiguratorConfig $definition,
        ConfiguratorProductDefinitionNode $node,
        array $references
    ): void {
        foreach ($references as $reference) {
            $node->addSection($reference, $this->createSection($definition, $reference));
        }
    }

    /**
     * @param ConfiguratorConfig $definition
     * @param $reference
     *
     * @return ConfiguratorDefinitionNode
     *
     * @throws Exception
     */
    private function createSection(ConfiguratorConfig $definition, $reference): ConfiguratorDefinitionNode
    {
        $data = $definition->getSection($reference);
        $parentDefaults = $definition->getDefaults();

        switch ($data['type'] ?? ConfiguratorDefinitionNode::SECTION) {
            case ConfiguratorDefinitionNode::PRODUCT_LIST:
                return $this->createProductListSection($data, $reference, $parentDefaults);
            case ConfiguratorDefinitionNode::PRODUCT:
                return $this->createProductSection($data['ref'], $data, $parentDefaults);
            case ConfiguratorDefinitionNode::SECTION:
                return $this->createSingleSection($definition, $reference);
            default:
                break;
        }

        throw ExceptionFactory::notSupported();
    }

    /**
     * @param array  $data      Section's data
     * @param string $reference Section's reference
     * @param array  $parentDefaults
     *
     * @return ConfiguratorProductListDefinitionNode
     *
     * @throws Exception
     */
    private function createProductListSection(array $data, string $reference, array $parentDefaults = []): ConfiguratorProductListDefinitionNode
    {
        $inputs = $data['inputs']['values'] ?? [];
        $requiredInputs = $data['inputs']['required'] ?? [];
        $title = $data['title'] ?? null;

        $productListSection = new ConfiguratorProductListDefinitionNode($reference, $title);
        $productListSection->setCondition($data['condition'] ?? null);
        $productListSection->setInputs($inputs);
        $productListSection->setItems($data['items']['enabled'] ?? true);
        $productListSection->setRequiredInputs(ArrayUtil::arrayToObject($requiredInputs));
        $productListSection->setSource($data['source'] ?? null);

        $this->addProductListProducts($productListSection, $data['ref'] ?? [], $parentDefaults);

        return $productListSection;
    }

    /**
     * @param string $reference
     * @param array  $data
     * @param array  $parentDefaults
     *
     * @return ConfiguratorDefinitionNode
     *
     * @throws Exception
     */
    private function createProductSection(string $reference, array $data = [], $parentDefaults = []): ConfiguratorDefinitionNode
    {
        return $this->createStructure($this->createConfiguratorConfig($reference, $parentDefaults), $data);
    }

    /**
     * @param ConfiguratorConfig $definition
     * @param $reference
     *
     * @return ConfiguratorDefinitionNode
     *
     * @throws Exception
     */
    private function createSingleSection(ConfiguratorConfig $definition, $reference): ConfiguratorDefinitionNode
    {
        $data = $definition->getSection($reference);

        $default = $data['default'] ?? true;
        $inputs = $data['inputs']['values'] ?? [];
        $requiredInputs = $data['inputs']['required'] ?? [];
        $structure = $data['ref'] ?? [];
        $title = $data['title'] ?? null;

        $section = new ConfiguratorDefinitionNode($reference, $title, $default);
        $section->setCondition($data['condition'] ?? null);
        $section->setInputs($inputs);
        $section->setItems($data['items']['enabled'] ?? true);
        $section->setItemRules($data['items'] ?? null);
        $section->setRequiredInputs(ArrayUtil::arrayToObject($requiredInputs));
        $section->setSource($data['source'] ?? null);

        foreach ($structure as $ref) {
            $section->addSection($ref, $this->createSection($definition, $ref));
        }

        return $section;
    }

    /**
     * @param ConfiguratorProductListDefinitionNode $productListSection
     * @param array                                 $products
     * @param array                                 $parentDefaults
     *
     * @throws Exception
     */
    private function addProductListProducts(
        ConfiguratorProductListDefinitionNode $productListSection,
        array $products,
        array $parentDefaults = []
    ): void {
        foreach ($products as $product) {
            $productListSection->addProduct($product, $this->createProductSection($product, [], $parentDefaults));
        }
    }

    /**
     * @param string $reference
     * @param array  $parentDefaults
     *
     * @return ConfiguratorConfig
     */
    public function createConfiguratorConfig(string $reference, array $parentDefaults = []): ConfiguratorConfig
    {
        $config = $this->productLoader->getConfig($reference);

        $defaults = $config['defaults'] ?? [];
        $inputs = $config['inputs']['values'] ?? [];
        $requiredInputs = $config['inputs']['required'] ?? [];
        $title = $config['title'] ?? null;

        $definition = new ConfiguratorConfig($reference, $title, $this->productLoader->getLatestVersion());
        $definition->setCollectionType($config['collection_type'] ?? null);
        $definition->setCondition($config['condition'] ?? null);
        $definition->setDefaults(ArrayUtil::arrayMergeRecursive($parentDefaults, $defaults));
        $definition->setInputs($inputs);
        $definition->setItems($config['items'] ?? null);
        $definition->setProcessors($config['processors'] ?? []);
        $definition->setRefs($config['ref'] ?? []);
        $definition->setRequiredInputs($requiredInputs);
        $definition->setSections($config['sections'] ?? []);
        $definition->setSource($config['source'] ?? null);

        return $definition;
    }
}
