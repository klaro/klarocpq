<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Definition;

class ConfiguratorProductDefinitionNode extends ConfiguratorDefinitionNode
{
    /**
     * @var string|null
     */
    protected $collectionType;

    /** @var ConfiguratorConfig */
    protected $configuratorConfig;

    /**
     * @return string|null
     */
    public function getCollectionType(): ?string
    {
        return $this->collectionType;
    }

    /**
     * @param string|null $collectionType
     */
    public function setCollectionType(?string $collectionType): void
    {
        $this->collectionType = $collectionType;
    }

    /**
     * @return ConfiguratorConfig
     */
    public function getConfiguratorConfig(): ConfiguratorConfig
    {
        return $this->configuratorConfig;
    }

    /**
     * @param ConfiguratorConfig $configuratorConfig
     */
    public function setConfiguratorConfig(ConfiguratorConfig $configuratorConfig): void
    {
        $this->configuratorConfig = $configuratorConfig;
    }

    /**
     * @return array
     */
    public function getProcessors(): array
    {
        return $this->configuratorConfig->getProcessors();
    }

    public function getType(): string
    {
        return ConfiguratorDefinitionNode::PRODUCT;
    }
}
