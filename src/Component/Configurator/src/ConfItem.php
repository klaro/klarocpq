<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;

/**
 * Concrete implementation of a conf item which represents a product in an offering summary section.
 *
 * Class ConfItem
 * @package Klaro\QuotationBundle\Library
 */
class ConfItem extends AbstractConfItem
{
    /** @var ConfigurationNode */
    protected $parent;      // TODO: this will cause circular references, remove?

    /**
     * @param ConfigurationNode $context
     * @param array             $data
     */
    public function __construct(ConfigurationNode $context, $data = [])
    {
        parent::__construct($data);

        $this->parent = $context;
    }

    /**
     * @return ConfigurationNode
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param ConfigurationNode $parent
     */
    public function setParent(ConfigurationNode $context)
    {
        $this->parent = $context;
    }

    /**
     * @param $field
     *
     * @return null
     */
    public function getFactors($field)
    {
        return $this->parent->getFactors($field);
    }

    /**
     * @param $field
     *
     * @return null
     */
    public function getOriginalValue($field)
    {
        return isset($this->data[$field]) ? $this->data[$field] : null;
    }

    /**
     * Return the given field's value.
     *
     * @param $field
     *
     * @return mixed
     */
    public function get($field, $arguments = null)
    {
        $data = isset($this->data[$field]) ? $this->data[$field] : null;

        if ($data && is_numeric($data)) {
            foreach ($this->getFactors($field) as $factor) {
                $data *= $factor['factor'];
            }
        }

        return $data;
    }
}
