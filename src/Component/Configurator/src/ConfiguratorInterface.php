<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator;

use Klaro\Component\Configurator\Configuration\ConfigurationNode;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionNode;

interface ConfiguratorInterface
{
    /**
     *
     * @return array
     */
    public function getAllConfiguratorRefs();

    /**
     * @param string $ref
     * @param object $data
     *
     * @return ConfigurationNode
     */
    public function processConfigurationByRef(
        $ref,
        $data
    );

    /**
     * @param ConfiguratorDefinitionNode $configuration
     * @param $data
     *
     * @return ConfigurationNode
     */
    public function processConfiguration(
        ConfiguratorDefinitionNode $configuration,
        $data
    );

    /**
     * @param $exportedConfiguration
     *
     * @return mixed
     */
    public function importConfiguration($exportedConfiguration);
}
