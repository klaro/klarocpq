<?php
/**
 *
 *  This file is part of the KlaroCPQ package.
 *
 *  (c) Klaro Technology <info@klarocpq.fi>
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace Klaro\Component\Configurator\Tests;

use PHPUnit\Framework\TestCase;
use Klaro\Component\Configurator\ArrayConfigLoader;
use Klaro\Component\Configurator\ConfigLoaderInterface;
use Klaro\Component\Configurator\Configuration\ConfigurationBuilder;
use Klaro\Component\Configurator\Definition\ConfiguratorDefinitionFactory;
use Symfony\Component\Yaml\Yaml;

class ConfigurationBuilderTest extends TestCase
{
    /** @var string */
    protected $path;

    /** @var array */
    protected $config;

    /** @var ConfigLoaderInterface */
    protected $configLoader;

    /** @var ConfiguratorDefinitionFactory */
    protected $definitionFactory;

    /**
     *
     */
    public function setUp()
    {
        $this->path = __DIR__.'/Fixtures';
        $this->config = Yaml::parse(file_get_contents($this->path.'/products.yml'));

        $this->configLoader = new ArrayConfigLoader($this->config['products'], $this->config['version']);
        $this->definitionFactory = ConfiguratorDefinitionFactory::create($this->configLoader);
    }

    /**
     *
     */
    public function testEmptyObject()
    {
        $loader = $this->definitionFactory->createConfiguratorConfig('drive');
        $definition = $this->definitionFactory->createStructure($loader);

        $actualConfiguration = ConfigurationBuilder::create($definition)->build();

        $this->assertEquals($actualConfiguration->getId(), "drive");
        $this->assertEquals(count($actualConfiguration->getSections()), 2);
    }

    /**
     *
     */
    public function testTitle()
    {
        $loader = $this->definitionFactory->createConfiguratorConfig('drive');
        $definition = $this->definitionFactory->createStructure($loader);

        $data = <<<EOL
{
    "title": "New Title"
}
EOL;

        $actualConfiguration = ConfigurationBuilder::create($definition)->build(json_decode($data));

        $this->assertEquals($actualConfiguration->getTitle(), "New Title");
    }
}
